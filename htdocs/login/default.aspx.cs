﻿using System;

public partial class ResponseLogin : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        // pass link to correct page (make it so it can only be the length required)
        string access_link = Request.QueryString["u"];
        string access_type = Request.QueryString["t"];
        string access_code = Request.QueryString["c"];

        if (access_link.Length < 50 && access_type == "HAM")
        {

            Session.Timeout = 60;
            Session["YOUR_LANGUAGE"] = "en-gb";
            Session["AccessLink"] = access_link;
            Session["ADMIN_USER"] = "0";
            Response.Redirect("~/core/modules/response/");

        }

        if (access_link.Length < 50 && access_type == "AU")
        {

            Session.Timeout = 60;
            Session["YOUR_LANGUAGE"] = "en-gb";
            Session["AdminLink"] = access_link;
            Response.Redirect("~/core/modules/response/admin/");

        }

        if (access_link.Length < 50 && access_type == "SV")
        {

            Session.Timeout = 60;
            Session["YOUR_LANGUAGE"] = "en-gb";
            Session["SETUP_CONTINUE"] = "Y";
            Session["setupCode"] = access_code;
            Session["setupReference"] = access_link;
            Response.Redirect("~/core/modules/response/setup/");

        }





    }


}

