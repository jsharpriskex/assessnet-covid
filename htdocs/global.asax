
<%@ Application Language="VB" %>
<%@ Import namespace="system.net" %>
<%@ Import Namespace="system.net.mail" %>
<SCRIPT LANGUAGE="VBScript" RUNAT="Server">    

    Sub Application_OnStart()

        ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)

        ' AssessNET Config Settings
        Application("SoftwareName") = "AssessNET"
        Application("SoftwareOwner") = "PREPRODUCTION DEVELOPMENT VERSION"
        Application("SoftwareOwnerSite") = "www.riskex.co.uk"
        Application("CONFIG_VERSION") = "3.2"
        Application("CONFIG_DBASE") = "MODULE"
        Application("CONFIG_PROVIDER") = "https://local_covid.assessweb.co.uk"
        Application("CONFIG_PROVIDER_SECURE") = "https://local_covid.assessweb.co.uk"
        Application("CONFIG_MAIN") = "https://www.assessweb.co.uk"
        Application("EMAIL_SUPPORT") = "support.desk@assessnet.co.uk"
        Application("EMAIL_TRAINING") = "training@safe-and-sound.com"
        Application("EMAIL_CONSULTANT") = "consultancy@safe-and-sound.com"
        Application("EMAIL_ACCOUNTS") = "accounts@riskex.co.uk"

        Application("CORP_LOGOS_SERVER") = "E:\Builds\pre_production\applications\assessweb.co.uk\htdocs\version3.2\data\documents\pdf_centre\img\corp_logos\"
        Application("CORP_LOGOS_WEB") = "https://www.assessweb.co.uk/version3.2/data/documents/pdf_centre/img/corp_logos/"


        'Email sending settings
        Application("SMTP_RELAY") = "riskex-co-uk.mail.protection.outlook.com"
        Application("SMTP_RELAY_USERNAME") = "notifications@riskexltd.onmicrosoft.com"
        Application("SMTP_RELAY_PASSWORD") = "rX46812@"
        Application("SMTP_FROM_ADDRESS") = "notifications@assessnet.co.uk"
        Application("SMTP_BCC_ADDRESS") = "mailcatcher@assessnet.co.uk"


        Application("EXCEL_LICENCE") = "ZOr66/j66/r56/jl++v4+uX6+eXy8vLy"



        Application("DBTABLE_EMEMO_DATA") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_mailservice"
        Application("DBTABLE_MODULES_DATA") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_licences"
        Application("DBTABLE_HR_HOSTS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_hosts"
        Application("DBTABLE_HR_DATA_STRUCTURE") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_structure"
        Application("DBTABLE_RAMOD_DATA") = Application("CONFIG_DBASE") & "_RA.dbo.RISK_Data_entries"
        Application("DBTABLE_RAMOD_HAZARDS") = Application("CONFIG_DBASE") & "_RA.dbo.RISK_Data_hazards"
        Application("DBTABLE_RAMOD_PERSONS") = Application("CONFIG_DBASE") & "_RA.dbo.RISK_Data_persons"
        Application("DBTABLE_RAMOD_ACTIONS") = Application("CONFIG_DBASE") & "_RA.dbo.RISK_Data_actions"
        Application("DBTABLE_RAMOD_PROCESS") = Application("CONFIG_DBASE") & "_RA.dbo.RISK_Data_process"
        Application("DBTABLE_DSEMOD_DATA") = Application("CONFIG_DBASE") & "_DSE.dbo.DSE_Data_entries"
        Application("DBTABLE_DSEMOD_RESULTS") = Application("CONFIG_DBASE") & "_DSE.dbo.DSE_Data_results"
        Application("DBTABLE_DSEMOD_ACTIONS") = Application("CONFIG_DBASE") & "_DSE.dbo.DSE_Data_actions"
        Application("DBTABLE_DSEMOD_CAMP") = Application("CONFIG_DBASE") & "_DSE.dbo.DSE_Data_campaigns"
        Application("DBTABLE_SHARED_QUESTIONS_DSE") = "SHARED_DATA.dbo.Shared_data_Questions_dse"
        Application("DBTABLE_USER_DATA") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_users"
        Application("DBTABLE_USER_STRUCTURE") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_users_structure"
        Application("DBTABLE_USER_PASSWORD") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_users_password"
        Application("DBTABLE_ACC_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_entries"
        Application("DBTABLE_ACC_ATTACH") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_attach"
        Application("DBTABLE_INJ_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.INJ_Data_entries"
        Application("DBTABLE_INJ_INJURIES") = Application("CONFIG_DBASE") & "_ACC.dbo.INJ_Data_injuries"
        Application("DBTABLE_INJ_CAUSES") = Application("CONFIG_DBASE") & "_ACC.dbo.INJ_Data_causes"
        Application("DBTABLE_ID_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.ID_Data_entries"
        Application("DBTABLE_NM_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.NM_Data_entries"
        Application("DBTABLE_NM_ACTIONS") = Application("CONFIG_DBASE") & "_ACC.dbo.NM_Data_actions"
        Application("DBTABLE_NM_WITNESS") = Application("CONFIG_DBASE") & "_ACC.dbo.NM_Data_witness"
        Application("DBTABLE_DO_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.DO_Data_entries"
        Application("DBTABLE_DO_ACTIONS") = Application("CONFIG_DBASE") & "_ACC.dbo.DO_Data_actions"
        Application("DBTABLE_DO_WITNESS") = Application("CONFIG_DBASE") & "_ACC.dbo.DO_Data_witness"
        Application("DBTABLE_DAM_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.DAM_Data_entries"
        Application("DBTABLE_DAM_WITNESS") = Application("CONFIG_DBASE") & "_ACC.dbo.DAM_Data_witness"
        Application("DBTABLE_DEF_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.DEF_Data_entries"
        Application("DBTABLE_RR_DATA_ENTRIES") = Application("CONFIG_DBASE") & "_ACC.dbo.RR_Data_entries"
        Application("DBTABLE_RR_DATA_INJURIES") = Application("CONFIG_DBASE") & "_ACC.dbo.RR_Data_injuries"
        Application("DBTABLE_RR_ID_DATA_ENTRIES") = Application("CONFIG_DBASE") & "_ACC.dbo.RRID_Data_entries"
        Application("DBTABLE_RR_DATA_ILLNESSES") = Application("CONFIG_DBASE") & "_ACC.dbo.RR_Data_illnesses"
        Application("DBTABLE_INV_DATA_ENTRIES") = Application("CONFIG_DBASE") & "_ACC.dbo.INV_Data_entries"
        Application("DBTABLE_INV_WITNESS") = Application("CONFIG_DBASE") & "_ACC.dbo.INV_Data_witness"
        Application("DBTABLE_INV_ACTIONS") = Application("CONFIG_DBASE") & "_ACC.dbo.INV_Data_actions"
        Application("DBTABLE_INV_MEMBERS") = Application("CONFIG_DBASE") & "_ACC.dbo.INV_Data_members"
        Application("DBTABLE_INV_DIST") = Application("CONFIG_DBASE") & "_ACC.dbo.INV_Data_distro"
        Application("DBTABLE_DO_CATEGORIES") = Application("CONFIG_DBASE") & "_ACC.dbo.DO_Data_categories"
        Application("DBTABLE_HR_DATA_ABOOK") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_abooks"
        Application("DBTABLE_HR_ABOOK_ADMIN") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_abook_admin"
        Application("DBTABLE_MHMOD_DATA") = Application("CONFIG_DBASE") & "_MH.dbo.MH_DATA_ENTRIES"
        Application("DBTABLE_MHMOD_NOTES") = Application("CONFIG_DBASE") & "_MH.dbo.MH_DATA_NOTES"
        Application("DBTABLE_MHMOD_PERSONS") = Application("CONFIG_DBASE") & "_MH.dbo.MH_DATA_PERSONS"
        Application("DBTABLE_FSMOD_DATA") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_ENTRIES"
        Application("DBTABLE_FSMOD_ZONES") = Application("CONFIG_DBASE") & "_FS.dbo.FS_Locations"
        Application("DBTABLE_FSMOD_MATERIAL") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_MATERIAL"
        Application("DBTABLE_FSMOD_IGNITION") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_IGNITION"
        Application("DBTABLE_FSMOD_ALARM") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_ALARM"
        Application("DBTABLE_FSMOD_EQUIP") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_EQUIP"
        Application("DBTABLE_FSMOD_PERSONS") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_PERSONS"
        Application("DBTABLE_FSMOD_MOE") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATE_MOE"
        Application("DBTABLE_FSMOD_OXY") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_OXY"
        Application("DBTABLE_FSMOD_ANSWERS") = Application("CONFIG_DBASE") & "_FS.dbo.FS_Answers"
        Application("DBTABLE_FSMOD_ACTIONS") = Application("CONFIG_DBASE") & "_FS.dbo.FS_DATA_ACTIONS"

        Application("DBTABLEOLD_FSMOD_DATA") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_ENTRIES"
        Application("DBTABLEOLD_FSMOD_MATERIAL") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_MATERIAL"
        Application("DBTABLEOLD_FSMOD_IGNITION") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_IGNITION"
        Application("DBTABLEOLD_FSMOD_OXY") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_OXY"
        Application("DBTABLEOLD_FSMOD_ALARM") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_ALARM"
        Application("DBTABLEOLD_FSMOD_EQUIP") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_EQUIP"
        Application("DBTABLEOLD_FSMOD_PERSONS") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_PERSONS"
        Application("DBTABLEOLD_FSMOD_PERSONNEL") = Application("CONFIG_DBASE") & "_FS.dbo.OLD_FS_DATA_PERSONNEL"

        Application("DBTABLE_AUMOD_DATA") = Application("CONFIG_DBASE") & "_AU.dbo.AU_DATA_ENTRIES"
        Application("DBTABLE_AUMOD_EXTRA") = Application("CONFIG_DBASE") & "_AU.dbo.AU_DATA_EXTRA"
        Application("DBTABLE_LOG_EMAIL") = Application("CONFIG_DBASE") & "_LOG.dbo.LOG_DATA_EMAIL"
        Application("DBTABLE_LOG_ACCESS") = Application("CONFIG_DBASE") & "_LOG.dbo.LOG_Access"
        Application("DBTABLE_LOG_404") = Application("CONFIG_DBASE") & "_LOG.dbo.LOG_404"
        Application("DBTABLE_ACC_RISK") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_DATA_Risks"
        Application("DBTABLE_ACC_MH") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_DATA_Manual"
        Application("DBTABLE_ARCMOD_RISK_DATA") = Application("CONFIG_DBASE") & "_ARC.dbo.RISK_ARC_entries"
        Application("DBTABLE_ARCMOD_RISK_HAZARDS") = Application("CONFIG_DBASE") & "_ARC.dbo.RISK_ARC_hazards"
        Application("DBTABLE_ARCMOD_RISK_PERSONS") = Application("CONFIG_DBASE") & "_ARC.dbo.RISK_ARC_persons"
        Application("DBTABLE_ARCMOD_RISK_ACTIONS") = Application("CONFIG_DBASE") & "_ARC.dbo.RISK_ARC_actions"
        Application("DBTABLE_ARCMOD_GRAPH_SAVED") = Application("CONFIG_DBASE") & "_ARC.dbo.GRAPH_ARC_saved"
        Application("DBTABLE_CMS_COMPANY") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_COMPANY"
        Application("DBTABLE_CMS_CONTACT") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_CONTACT"
        Application("DBTABLE_CMS_INTERESTS") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_INTERESTS"
        Application("DBTABLE_CMS_NOTES") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_NOTES"
        Application("DBTABLE_CMS_RELATIONSHIP") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_RELATIONSHIP"
        Application("DBTABLE_CMS_REG") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_REG"
        Application("DBTABLE_CMS_BK_TRAINING") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_bk_training"
        Application("DBTABLE_CMS_BK_INTEREST") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_bk_INTEREST"
        Application("DBTABLE_CMS_BK_CON_INTEREST") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_bk_CON_INTEREST"
        Application("DBTABLE_SD_EVENT") = Application("CONFIG_DBASE") & "_SD.dbo.SD_EVENT"
        Application("DBTABLE_SD_CATEGORY") = Application("CONFIG_DBASE") & "_SD.dbo.SD_CATEGORY"
        Application("DBTABLE_DEV_DATA") = Application("CONFIG_DBASE") & "_DEV.dbo.DEV_Entries"
        Application("DBTABLE_DEV_NOTES") = Application("CONFIG_DBASE") & "_DEV.dbo.Dev_Notes"
        Application("DBTABLE_OB_DATA") = Application("CONFIG_DBASE") & "_OB.dbo.ORDER_DATA_ENTRIES"
        Application("DBTABLE_OB_JOBS") = Application("CONFIG_DBASE") & "_OB.dbo.ORDER_DATA_JOBS"
        Application("DBTABLE_DSE_DATA") = Application("CONFIG_DBASE") & "_DSE.dbo.NEWDSE_Data_entries"
        Application("DBTABLE_DSE_RESULTS") = Application("CONFIG_DBASE") & "_DSE.dbo.NEWDSE_Data_results"
        Application("DBTABLE_DSE_ACTIONS") = Application("CONFIG_DBASE") & "_DSE.dbo.NEWDSE_Data_actions"
        Application("DBTABLE_ARCDSE_DATA") = Application("CONFIG_DBASE") & "_DSE.dbo.ArchiveDSE_Data_entries"
        Application("DBTABLE_ARCDSE_RESULTS") = Application("CONFIG_DBASE") & "_DSE.dbo.ArchiveDSE_Data_results"
        Application("DBTABLE_ARCDSE_ACTIONS") = Application("CONFIG_DBASE") & "_DSE.dbo.ArchiveDSE_Data_actions"
        Application("DBTABLE_MM_DATA") = Application("CONFIG_DBASE") & "_MM.dbo.QUOTE_Data_entries"
        Application("DBTABLE_MM_DATA_ASN") = Application("CONFIG_DBASE") & "_MM.dbo.ASN_QUOTE_DATA_ENTRIES"
        Application("DBTABLE_MM_INFO") = Application("CONFIG_DBASE") & "_MM.dbo.QUOTE_Data_info"
        Application("DBTABLE_DOMAIN_DATA") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_Domains"
        Application("DBTABLE_GLOBAL_DIR") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_ULOAD_directories"
        Application("DBTABLE_GLOBAL_FILES") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_ULOAD_documents"
        Application("DBTABLE_GLOBAL_ATTACH") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_ULOAD_attachments"
        Application("DBTABLE_GLOBAL_News") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_News"
        Application("DBTABLE_MSDS_Entries") = Application("CONFIG_DBASE") & "_COSHH.dbo.MSDS_Entries"
        Application("DBTABLE_MSDS_Ingredients") = Application("CONFIG_DBASE") & "_COSHH.dbo.MSDS_Ingredients"
        Application("DBTABLE_TASK_NOTE") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Notes"
        Application("DBTABLE_TASK_MANAGEMENT") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Management"
        Application("DBTABLE_TASK_STANDARDS") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Standards_opts"
        Application("DBTABLE_TASK_HISTORY") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_History"
        Application("DBTABLE_TASK_PREF") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Pref"
        Application("DBTABLE_PERMIT_INFO") = Application("CONFIG_DBASE") & "_PTW.dbo.permit_info"
        Application("DBTABLE_PERMIT_GENERAL") = Application("CONFIG_DBASE") & "_PTW.dbo.permit_general"
        Application("DBTABLE_PERMIT_ELEC") = Application("CONFIG_DBASE") & "_PTW.dbo.permit_elec"
        Application("DBTABLE_PERMIT_HOT") = Application("CONFIG_DBASE") & "_PTW.dbo.permit_hot"
        Application("DBTABLE_PERMIT_SPACE") = Application("CONFIG_DBASE") & "_PTW.dbo.permit_space"
        Application("DBTABLE_NEWS") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.assessnet_news"
        Application("DBTABLE_SAS_NEWS") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.sas_news"
        Application("DBTABLE_GLOBAL_FPRINT") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_FINGERPRINT"
        Application("DBTABLE_GLOBAL_FPRINTNOTES") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_FPRINT_NOTES"
        Application("DBTABLE_DSE_REG") = Application("CONFIG_DBASE") & "_DSE.dbo.DSE_data_reg"

        'ptw v2

        Application("DBTABLE_PTW_DATA_ENTRIES") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_entries"
        Application("DBTABLE_PTW_DATA_EXTENSION") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_extension"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_1") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_1"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_2") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_2"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_3") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_3"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_4") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_4"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_5") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_5"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_6") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_6"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_7") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_7"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_8") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_8"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_9") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_9"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_10") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_10"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_11") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_11"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_12") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_12"
        Application("DBTABLE_PTW_DATA_FIELD_TYPE_13") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_field_type_13"
        Application("DBTABLE_PTW_DATA_TEMPLATE_ENTRIES") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_template_entries"
        Application("DBTABLE_PTW_DATA_TEMPLATE_FIELDS") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_template_fields"
        Application("DBTABLE_PTW_DATA_TEMPLATE_OPTIONS") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_template_options"
        Application("DBTABLE_PTW_DATA_TEMPLATE_SECTIONS") = Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_template_sections"

        'SA V2 specific tables
        Application("DBTABLE_SA_CATS") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_categories"
        Application("DBTABLE_SA_QUESTIONS") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_questions"
        Application("DBTABLE_SA_TEMP_QUESTIONS") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_template_questions"
        Application("DBTABLE_SA_TEMP_INFO") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_template_info"
        Application("DBTABLE_SA_AUDIT_ENTRIES") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_audit_entries"
        Application("DBTABLE_SA_AUDIT_OBSERVATIONS") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_audit_observations"
        Application("DBTABLE_SA_AUDIT_DATA") = Application("CONFIG_DBASE") & "_SA.dbo.new_sa_audit_data"
        Application("DBTABLE_SA_V2_TEMPLATE_QUESTIONS") = Application("CONFIG_DBASE") & "_SA.dbo.Sa_v2_template_questions"
        Application("DBTABLE_SA_V2_TEMPLATE_SECTIONS") = Application("CONFIG_DBASE") & "_SA.dbo.Sa_v2_template_sections"
        Application("DBTABLE_SA_V2_TEMPLATE_SCORING") = Application("CONFIG_DBASE") & "_SA.dbo.SA_v2_template_scoring"
        Application("DBTABLE_SA_V2_TEMPLATE_SCORING_NOTIFICATION") = Application("CONFIG_DBASE") & "_SA.dbo.SA_v2_template_scoring_notification"

        Application("DBTABLE_QA_CATS") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_categories"
        Application("DBTABLE_QA_QUESTIONS") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_questions"
        Application("DBTABLE_QA_TEMP_QUESTIONS") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_template_questions"
        Application("DBTABLE_QA_TEMP_INFO") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_template_info"
        Application("DBTABLE_QA_AUDIT_ENTRIES") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_audit_entries"
        Application("DBTABLE_QA_AUDIT_OBSERVATIONS") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_audit_observations"
        Application("DBTABLE_QA_AUDIT_DATA") = Application("CONFIG_DBASE") & "_QA.dbo.new_qa_audit_data"
        Application("DBTABLE_QA_TEMPLATE_LOCS") = Application("CONFIG_DBASE") & "_QA.dbo.template_V2_permissions"
        Application("DBTABLE_QA_V2_TEMPLATE_QUESTIONS") = Application("CONFIG_DBASE") & "_QA.dbo.QA_v2_template_questions"
        Application("DBTABLE_QA_V2_TEMPLATE_SECTIONS") = Application("CONFIG_DBASE") & "_QA.dbo.QA_v2_template_sections"
        Application("DBTABLE_QA_V2_TEMPLATE_SCORING") = Application("CONFIG_DBASE") & "_QA.dbo.QA_v2_template_scoring"
        Application("DBTABLE_QA_V2_TEMPLATE_SCORING_NOTIFICATION") = Application("CONFIG_DBASE") & "_QA.dbo.QA_v2_template_scoring_notification"


        'flowchart
        Application("DBTABLE_FLOWC_TINFO") = Application("CONFIG_DBASE") & "_WFLOW.dbo.temp_info"
        Application("DBTABLE_FLOWC_TCATS") = Application("CONFIG_DBASE") & "_WFLOW.dbo.temp_cats"
        Application("DBTABLE_FLOWC_TQUESTIONS") = Application("CONFIG_DBASE") & "_WFLOW.dbo.temp_questions"
        Application("DBTABLE_FLOWC_AINFO") = Application("CONFIG_DBASE") & "_WFLOW.dbo.audit_info"
        Application("DBTABLE_FLOWC_ADATA") = Application("CONFIG_DBASE") & "_WFLOW.dbo.audit_data"
        Application("DBTABLE_FLOWC_AHISTORY") = Application("CONFIG_DBASE") & "_WFLOW.dbo.audit_history"
        Application("DBTABLE_FCTEMP_DATA") = Application("CONFIG_DBASE") & "_LB.dbo.LBOOK_Template_data"
        Application("DBTABLE_FCTEMP_LIST") = Application("CONFIG_DBASE") & "_LB.dbo.LBOOK_Template_list"
        Application("DBTABLE_FC_DATA") = Application("CONFIG_DBASE") & "_LB.dbo.LBOOK_list_data"

        ' checklist
        Application("DBTABLE_CLTEMP_DATA") = Application("CONFIG_DBASE") & "_CL.dbo.CKLIST_Template_data"
        Application("DBTABLE_CLTEMP_LIST") = Application("CONFIG_DBASE") & "_CL.dbo.CKLIST_Template_list"
        Application("DBTABLE_CL_DATA") = Application("CONFIG_DBASE") & "_CL.dbo.CKLIST_list_data"

        ' method statement
        Application("DBTABLE_MS_DATA") = Application("CONFIG_DBASE") & "_MS.dbo.MSTATEMENT_data_entries"
        Application("DBTABLE_MS_ATTACH") = Application("CONFIG_DBASE") & "_MS.dbo.MSTATEMENT_data_attach"
        Application("DBTABLE_MS_DATA_V2") = Application("CONFIG_DBASE") & "_MS.dbo.MS_data_entries_v2"
        Application("DBTABLE_MS_PA_V2") = Application("CONFIG_DBASE") & "_MS.dbo.MS_data_persons_v2"

        'coshh

        'new tables for coshh 26/10/2005
        Application("DBTABLE_CA_ENTRIES") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_entries"
        Application("DBTABLE_CA_DATA") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_data"
        Application("DBTABLE_COSHH_PERSONS") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_data_persons"
        Application("DBTABLE_COSHH_SUBS") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_substance"
        Application("DBTABLE_COSHH_SUB_DATA") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_substance_data"
        Application("DBTABLE_COSHH_INGS") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_ingredients"
        Application("DBTABLE_COSHH_MON") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_monitoring"
        Application("DBTABLE_COSHH_RPE") = Application("CONFIG_DBASE") & "_COSHH.dbo.new_coshh_rpe"

        Application("DBTABLE_COSHH_ENTRIES") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_entries"
        Application("DBTABLE_COSHH_INGREDIENTS") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_ingredients"
        Application("DBTABLE_COSHH_INGREDIENTS2") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_ingredients2"
        Application("DBTABLE_COSHH_PERSONS") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_persons"
        Application("DBTABLE_COSHH_SUBSTANCES") = Application("CONFIG_DBASE") & "_COSHH.dbo.coshh_substances"



        'new tables for Purchase order book 23/03/2006
        Application("DBTABLE_POB_DATA") = Application("CONFIG_DBASE") & "_POB.dbo.P_ORDER_DATA_ENTRIES"
        Application("DBTABLE_POB_JOBS") = Application("CONFIG_DBASE") & "_POB.dbo.P_ORDER_DATA_JOBS"

        'new tables for Inspection module 06/07/2006
        Application("DBTABLE_INS_DATA") = Application("CONFIG_DBASE") & "_INS.dbo.INSPECTION_data_entries"
        Application("DBTABLE_INS_AREA") = Application("CONFIG_DBASE") & "_INS.dbo.INSPECTION_data_areas"
        Application("DBTABLE_INS_OBSV") = Application("CONFIG_DBASE") & "_INS.dbo.INSPECTION_data_observations"
        Application("DBTABLE_INS_PREF") = Application("CONFIG_DBASE") & "_INS.dbo.INSPECTION_data_pref"

        'new memo tables
        Application("DBTABLE_MEMO_DATA") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.Memo_data_entries"

        ' Relating to partner database
        Application("PDBTABLE_CMS_COMPANY") = "PMODULE_CMS.dbo.CMS_COMPANY"
        Application("PDBTABLE_CMS_CONTACT") = "PMODULE_CMS.dbo.CMS_CONTACT"

        'course booking tables
        Application("DBTABLE_CMS_COURSES") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_COURSES"
        Application("DBTABLE_CMS_COURSEPRICES") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_COURSEPRICES"
        Application("DBTABLE_CMS_COURSEDELS") = Application("CONFIG_DBASE") & "_CMS.dbo.CMS_course_dels"

        'accident groups
        Application("DBTABLE_ACC_GROUP") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_abook_groups"

        'DSE groups and reviewers
        Application("DBTABLE_DSE_REVIEWERS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_dse_reviewers"
        Application("DBTABLE_DSE_GROUPS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_dse_groups"

        'New structure tables
        Application("DBTABLE_STRUCT_TIER1") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier1"
        Application("DBTABLE_STRUCT_TIER2") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier2"
        Application("DBTABLE_STRUCT_TIER3") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier3"
        Application("DBTABLE_STRUCT_TIER4") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier4"
        Application("DBTABLE_STRUCT_TIER5") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier5"
        Application("DBTABLE_STRUCT_TIER6") = Application("CONFIG_DBASE") & "_PEP.dbo.Structure_Data_tier6"
        Application("DBTABLE_STRUCT_DETS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_structure_details"
        Application("DBTABLE_STRUCT_GROUPS") = Application("CONFIG_DBASE") & "_PEP.dbo.structure_groups"
        Application("DBTABLE_STRUCT_GROUPS_DATA") = Application("CONFIG_DBASE") & "_PEP.dbo.structure_groups_bind"


        'SURA tables to be written in here
        Application("DBTABLE_SURA_DATA") = Application("CONFIG_DBASE") & "_SURA.dbo.SURA_Data_entries"
        Application("DBTABLE_SURA_HAZ") = Application("CONFIG_DBASE") & "_SURA.dbo.SURA_Data_hazards"
        Application("DBTABLE_SURA_PERSONS") = Application("CONFIG_DBASE") & "_SURA.dbo.SURA_Data_persons"
        Application("DBTABLE_SURA_INV") = Application("CONFIG_DBASE") & "_SURA.dbo.SURA_Data_involved"


        'SURA archive tables
        Application("DBTABLE_ARC_SURA_DATA") = Application("CONFIG_DBASE") & "_ARC.dbo.SURA_ARC_entries"
        Application("DBTABLE_ARC_SURA_HAZ") = Application("CONFIG_DBASE") & "_ARC.dbo.SURA_ARC_hazards"
        Application("DBTABLE_ARC_SURA_PERSONS") = Application("CONFIG_DBASE") & "_ARC.dbo.SURA_ARC_persons"
        Application("DBTABLE_ARC_SURA_INV") = Application("CONFIG_DBASE") & "_ARC.dbo.SURA_ARC_involved"

        'PDF
        Application("PDF_OBJECT_REF") = "ABCpdf6" '.Doc"

        'Upload (changed drive letter from D to E on development server)
        'Application("DOC_UPLOAD_PATH") = "E:\home\documents\assessweb\"
        'Application("DOC_UPLOAD_PATH") = "\\192.168.0.4\uploads\ASNET-CLIENTS-01\"
        'Application("DOC_UPLOAD_PATH") = "Z:\uploads\ASNET-CLIENTS-01\"
        Application("DOC_UPLOAD_PATH") = "C:\Server Operations\Uploads\ASNET-CLIENTS-PORTAL\"

        Application("DBTABLE_HR_PREFERENCE") = Application("CONFIG_DBASE") & "_HR.dbo.HR_DATA_preferences"

        'Acccident
        Application("DBTABLE_HR_ACC_OPTS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_acc_opts"
        Application("DBTABLE_ACC_WIT_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_witness"
        Application("DBTABLE_ACC_PAGES") = Application("CONFIG_DBASE") & "_ACC.dbo.sp_PagedAccidentRecords"
        Application("DBTABLE_ACC_PCONTACT") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_pcontacted"
        Application("DBTABLE_HR_PCONTACT") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_acc_pcontacted"
        Application("DBTABLE_ACC_ADDQUESTIONS") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_addquestions"
        Application("DBTABLE_ACC_QUESTIONS") = Application("CONFIG_DBASE") & "_ACC.dbo.ACC_Data_questions"
        Application("DBTABLE_HR_QUESTIONS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_acc_questions"
        Application("DBTABLE_HR_ABOOK_SUBGRP") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_abook_sub_groups"
        Application("DBTABLE_HR_ABOOK_LINK") = Application("CONFIG_DBASE") & "_HR.dbo.HR_accb_group_link"
        Application("DBTABLE_HR_ABOOK_USER_LINK") = Application("CONFIG_DBASE") & "_HR.dbo.HR_accb_user_link"

        Application("DBTABLE_ACC_CUSTOM_QUESTIONS") = "Module_HR.dbo.HR_Data_acc_custom_questions"
        Application("DBTABLE_ACC_CUSTOM_LISTS") = "Module_HR.dbo.HR_Data_acc_custom_quest_lists"

        'Compliance
        Application("DBTABLE_CT_DATA") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Data_Entries"
        Application("DBTABLE_CT_SECTIONS") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Data_Sections"
        Application("DBTABLE_CT_QUESTIONS") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Data_Questions"
        Application("DBTABLE_CT_ANSWERS") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Data_Answers"
        Application("DBTABLE_CT_QUESTION_GROUP") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Grouped_Questions"
        Application("DBTABLE_CT_DROPDOWN") = Application("CONFIG_DBASE") & "_CT.dbo.drop_down_values"
        Application("DBTABLE_CT_ANSWERGROUP") = Application("CONFIG_DBASE") & "_CT.dbo.grouped_elements"
        Application("DBTABLE_COMPLIANCE_SECTIONS") = Application("CONFIG_DBASE") & "_CT.dbo.ct_sections"
        Application("DBTABLE_COMPLIANCE_QUESTIONS") = Application("CONFIG_DBASE") & "_CT.dbo.ct_questions"
        Application("DBTABLE_COMPLIANCE_ANSWERS") = Application("CONFIG_DBASE") & "_CT.dbo.ct_answers"
        Application("DBTABLE_CT_STRUCTURE") = Application("CONFIG_DBASE") & "_CT.dbo.Template_Structure"
        Application("DBTABLE_CT_SPLASH") = Application("CONFIG_DBASE") & "_CT.dbo.CT_Cover"
        Application("DBTABLE_CT_SPLASH_USAGE") = Application("CONFIG_DBASE") & "_CT.dbo.CT_Cover_Usage"
        Application("DBTABLE_CT_SPLASH_PARKING") = Application("CONFIG_DBASE") & "_CT.dbo.CT_Cover_Parking"
        Application("DBTABLE_CT_SPLASH_OWNERSHIP") = Application("CONFIG_DBASE") & "_CT.dbo.CT_Cover_Ownership"
        Application("DBTABLE_CT_REPORT") = Application("CONFIG_DBASE") & "_CT.dbo.Compliance_Report"
        Application("DBTABLE_CT_REPORT_FIELDS") = Application("CONFIG_DBASE") & "_CT.dbo.Compliance_Report_Fields"

        'Asset
        Application("DBTABLE_ASSET_ENTRIES") = Application("CONFIG_DBASE") & "_ASS.dbo.ASS_data_entries"
        Application("DBTABLE_ASSET_CATEGORIES") = Application("CONFIG_DBASE") & "_ASS.dbo.ASS_data_categories"
        Application("DBTABLE_ASSET_SUPPLIERS") = Application("CONFIG_DBASE") & "_ASS.dbo.ASS_supplier_data"


        'PUWER
        Application("DBTABLE_PUWER_ENTRIES") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_audit_entries"
        Application("DBTABLE_PUWER_DATA") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_audit_data"
        Application("DBTABLE_PUWER_TEMPLATE") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_template_info"
        Application("DBTABLE_PUWER_TEMPLATE_QUESTIONS") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_template_questions"
        Application("DBTABLE_PUWER_TEMPLATE_SECTIONS") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_template_sections"
        Application("DBTABLE_PUWER_TEMPLATE_SCORING") = Application("CONFIG_DBASE") & "_PUWER.dbo.PUWER_template_scoring"

        'Link
        Application("DBTABLE_GLOBAL_LINK") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Link"

        'DSE Removed
        Application("DBTABLE_DSE_REMOVED") = Application("CONFIG_DBASE") & "_DSE.dbo.NEWDSE_Data_removed"

        'HR Systems
        Application("DBTABLE_HR_SYSTEMS") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Systems"

        'New vehicle tables
        Application("DBTABLE_VEHICLE") = Application("CONFIG_DBASE") & "_PEP.dbo.vehicle"
        Application("DBTABLE_VEHICLE_MOT") = Application("CONFIG_DBASE") & "_PEP.dbo.vehicle_mot"
        Application("DBTABLE_VEHICLE_TAX") = Application("CONFIG_DBASE") & "_PEP.dbo.vehicle_tax"
        Application("DBTABLE_VEHICLE_SERV") = Application("CONFIG_DBASE") & "_PEP.dbo.vehicle_service"
        Application("DBTABLE_VEHICLE_INS") = Application("CONFIG_DBASE") & "_PEP.dbo.vehicle_insurance"

        'reports
        Application("DBTABLE_REPORTS") = Application("CONFIG_DBASE") & "_REPORTS.dbo.REP_Data_entries"
        Application("DBTABLE_REPORTS_RA1_DATA") = Application("CONFIG_DBASE") & "_REPORTS.dbo.REP_Data_RA1"

        'log book V2
        Application("DBTABLE_LB_TEMPLATE_ENTRIES") = Application("CONFIG_DBASE") & "_LB.dbo.template_V2_entries"
        Application("DBTABLE_LB_TEMPLATE_DATA") = Application("CONFIG_DBASE") & "_LB.dbo.template_V2_data"
        Application("DBTABLE_LB_LOGBOOK_ENTRIES") = Application("CONFIG_DBASE") & "_LB.dbo.logbook_V2_entries"
        Application("DBTABLE_LB_LOGBOOK_DATA") = Application("CONFIG_DBASE") & "_LB.dbo.logbook_V2_data"
        Application("DBTABLE_LB_CUSTOM_LISTS") = Application("CONFIG_DBASE") & "_LB.dbo.template_V2_custom_lists"
        Application("DBTABLE_LB_PREFERENCES") = Application("CONFIG_DBASE") & "_LB.dbo.logbook_V2_preferences"

        'recurring tasks
        Application("DBTABLE_TASK_RECURRING") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Recurring"
        Application("DBTABLE_TASK_RECURRING_DIST") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Recurring_Dist_List"

        'Persons Affected
        Application("DBTABLE_PERS_AFFECTED") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_pers_affected_opts"

        'Hazard Register
        Application("DBTABLE_HAZ_ENTRIES") = Application("CONFIG_DBASE") & "_HAZ.dbo.HAZ_Data_entries"
        Application("DBTABLE_HAZ_ENTRIES") = Application("CONFIG_DBASE") & "_HAZ.dbo.HAZ_Data_entries"
        Application("DBTABLE_HAZ_ACTIONS") = Application("CONFIG_DBASE") & "_HAZ.dbo.HAZ_Data_actions"
        Application("DBTABLE_HAZ_MERGE") = Application("CONFIG_DBASE") & "_HAZ.dbo.HAZ_Data_group"

        'Maintenance Register
        Application("DBTABLE_MAINT_ENTRIES") = Application("CONFIG_DBASE") & "_MR.dbo.MAINT_Data_Entries"

        'Task Escalation
        Application("DBTABLE_TASK_ESCALATED") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.TASK_Data_Escalated"
        Application("DBTABLE_HR_ORG_STRUCTURE") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_org_structure"

        'risk matricies
        Application("GLOBAL_RISK_MATRIX_LIKELIHOOD") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.RISK_MATRIX_likelihood"
        Application("GLOBAL_RISK_MATRIX_SEVERITY") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.RISK_MATRIX_severity"
        Application("GLOBAL_RISK_MATRIX_OVERALL") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.RISK_MATRIX_overall"

        'Portal
        Application("DBTABLE_PORTAL_SETTINGS") = Application("CONFIG_DBASE") & "_PTL.dbo.portal_corp_settings"
        Application("DBTABLE_PORTAL_RESTRICTED") = Application("CONFIG_DBASE") & "_PTL.dbo.portal_restricted_users"


        Application("DBTABLE_GLOBAL_RA_OPTS") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts"
        Application("DBTABLE_GLOBAL_DATA_PHRASES") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Data_phrases"

        Application("DBTABLE_TRAINING_PREF") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Training_pref"

        Application("DBTABLE_ENV_DATA") = Application("CONFIG_DBASE") & "_ACC.dbo.ENV_Data_Entries"

        Application("DBTABLE_VI_DATA") = "Module_ACC.dbo.VI_Data_entries"
        Application("DBTABLE_ACC_AUTHORITIES") = "Module_HR.dbo.HR_Data_acc_authorities"
        Application("DBTABLE_ACC_VI_AUTHORITIES") = "Module_ACC.dbo.VI_Data_authorities"
        Application("DBTABLE_ACC_CUSTOM_QUESTIONS") = "Module_HR.dbo.HR_Data_acc_custom_questions"
        Application("DBTABLE_ACC_VI_CUSTOM_ANSWERS") = "Module_ACC.dbo.VI_Data_custom_answers"
        Application("DBTABLE_ACC_ACTIONS") = "Module_ACC.dbo.ACC_Data_actions"

        'pdf settings
        'Application("PDF_LICENCE") = "ZU5URVRFU1xcRVBLVUVWVEtUV0tcXFxc"
        Application("PDF_LICENCE") = "3fbs/ez97Ojs/evz7f3u7PPs7/Pk5OTk"
        Application("PDF_LICENCE_14_4") = "cvzs/ez97O3r/ezp8+397uzz7O/z5OTk5P3t"
        Application("PDF_BASE_URL") = "https://portal.assessweb.co.uk/version3.2/"


        '*******************PRIORITIES*****************************
        Application("DBTABLE_TM_PRIORITY_OPTS") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.task_priority_options"
        '**********************************************************

        '***********************SAVED SEARCHES*******************************************
        Application("DBTABLE_SAVED_SEARCH") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Saved_search"
        Application("DBTABLE_SAVED_SEARCH_DATA") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Saved_search_data"
        Application("DBTABLE_SAVED_SEARCH_USER") = Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Saved_search_users"
        '********************************************************************************


        ' DO NOT CHANGE OR REMOVE BELOW LINE
        'Application("DBCON_MODULE_HR") = "Provider=SQLOLEDB;Data source=192.168.0.10\ASNETDBSTOR01;Database=MODULE_HR;User ID=sqlasnet;Password=SASDB921"

    End Sub

    Sub Session_OnStart()

        ' Set Date/Time/Currency Settings (2057 UK/EN)
        Session.LCID = 2057

        ' Set Default Timeout
        Session.Timeout = 5

        Session("Start") = NOW
        Session("YOUR_IP_ADDRESS") = Request.ServerVariables("REMOTE_ADDR")
        Session("strConnectShared") = "Provider=SQLOLEDB;Data source=ENIGMA;Database=Shared_data;User ID=sqlpublic;Password=SASDB921"
    End Sub

    Sub Session_OnEnd()

    End Sub


    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs

        ' Get the error details
        Dim lastErrorWrapper As HttpException = CType(Server.GetLastError(), HttpException)

        Dim lastError As Exception = lastErrorWrapper
        If lastErrorWrapper.InnerException IsNot Nothing Then
            lastError = lastErrorWrapper.InnerException
        End If

        Dim currentUser As String = CType(HttpContext.Current.Session("YOUR_ACCOUNT_ID"), String)
        Dim currentUserName As String = CType(HttpContext.Current.Session("YOUR_USERNAME"), String)
        Dim corpCode As String = CType(HttpContext.Current.Session("corp_code"), String)

        'If corpCode = "AAAAAA" then
        '		response.redirect("https://portal.assessweb.co.uk/version3.2/security/login/frm_lg_session.asp")
        '	end if

        Dim lastErrorTypeName As String = lastError.GetType().ToString()
        Dim lastErrorMessage As String = lastError.Message
        Dim lastErrorStackTrace As String = lastError.StackTrace

        ' Create the MailMessage object

        Dim mm As New MailMessage()
        mm.From = New MailAddress("error.page@assessnet.co.uk")
        mm.BodyEncoding = Encoding.UTF8
        mm.Subject = "An Error Has Occurred on AssessNET!"
        mm.To.Add(New MailAddress("development@assessnet.co.uk"))
        mm.IsBodyHtml = True
        mm.Priority = MailPriority.High
        mm.Body = "<html>" & vbCrLf &
       "  <body>" & vbCrLf &
       "  <h1>An Error Has Occurred!</h1>" & vbCrLf &
       "  <table cellpadding=""5"" cellspacing=""0"" border=""1"">" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>Server:</td>" & vbCrLf &
       "  <td>Portal</td>" & vbCrLf &
       "  </tr>" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>URL:</td>" & vbCrLf &
       "  <td>" & Request.RawUrl & "</td>" & vbCrLf &
       "  </tr>" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>Info:</td>" & vbCrLf &
       "  <td>" & currentUserName & " - " & currentUser & " - " & corpCode & "</td>" & vbCrLf &
       "  </tr>" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>Exception Type:</td>" & vbCrLf &
       "  <td>" & lastErrorTypeName & "</td>" & vbCrLf &
       "  </tr>" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>Message:</td>" & vbCrLf &
       "  <td>" & lastErrorMessage & "</td>" & vbCrLf &
       "  </tr>" & vbCrLf &
       "  <tr>" & vbCrLf &
       "  <td>Stack Trace:</td>" & vbCrLf &
       "  <td>" & lastErrorStackTrace.Replace(Environment.NewLine, "<br />") & "</td>" & vbCrLf &
       "  </tr> " & vbCrLf &
       "  </table>" & vbCrLf &
       "  </body>" & vbCrLf &
       "</html>"


        'Attach the Yellow Screen of Death for this error
        Dim YSODmarkup As String = lastErrorWrapper.GetHtmlErrorMessage()
        If Not String.IsNullOrEmpty(YSODmarkup) Then
            Dim YSOD As Attachment = Attachment.CreateAttachmentFromString(YSODmarkup, "YSOD.htm")
            mm.Attachments.Add(YSOD)
        End If


        ' Send the email
        Dim smtp As New SmtpClient(Application("SMTP_RELAY"))
        smtp.Credentials = New NetworkCredential(CStr(Application("SMTP_RELAY_USERNAME")), CStr(Application("SMTP_RELAY_PASSWORD")))
        smtp.Send(mm)
    End Sub

    Sub Application_OnEnd()

    End Sub


</SCRIPT> 