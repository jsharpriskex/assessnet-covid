/**
 * Export Web Page As PDF 
 * This module loads a web page and converts the page to PDF.
 * 
 * PhantomJS 1.9  <https://raw.github.com/ariya/phantomjs/master/LICENSE.BSD>
 *
 **/
 
 /*
 
  Command-line Syntax:
  
  phantomjs savePDF.js <url> <output> 
  
  TODO:
  - Add options [--delay <CaptureDelayInMilliseconds>] [--scale <ZoomFactor>] 
    
  */
 
var page = new WebPage(),
	system = require('system'),
	args = system.args,
	parseOptions,
	URL,
	sdate = +new Date();

// Check Application arguments
if (args.length >= 2) {
	
	URL = args[1];
	
	console.log('Wait... loading web page.');
	page.open(URL, function (status) {
	
		// Throw error and exit application web page loading fails
		if (status !== 'success') {
			console.error("Error: Failed to load web page from " + URL  + ".");
			phantom.exit();
		}
		
		// Get commandline options
		var options = parseOptions.apply(this, args);
		
		// Set zoom factor for page 
		page.zoomFactor = options.zoom;
		
		console.log('Successfully loaded web page.');
		console.log('Wait... saving web page as PDF.');
		// Delay capturing
		window.setTimeout(function () {

			// Generate capture filename
			outputFile = options.outputFolder + 
						options.outputFile + "." + 
						options.outputType; 
						
			// Capture chart into image file
			page.render(outputFile);

			console.log("Successfully converted web page to PDF in "+ ((+new Date()) - sdate) +"ms.");
			console.log("The webpage has been saved as " + outputFile + ".")
			
			phantom.exit();
			
		}, options.delay);		
		
		// Show console message from HTML Application
		page.onConsoleMessage = function (msg, lineNum, sourceId) {
			console.log('[#' + lineNum + ':' + sourceId + '] ' + msg);
		};
		
	});

} 
// Throw error as no XML path has been defined in Application argument
else {
	console.log('Error: Missing arguments.');
	console.log('Usage: phantomjs savePDF.js <url> [<output>]');
	//console.log('Usage: phantomjs savePDF.js <url> <output> [--delay <CaptureDelayInMilliseconds>] [--scale <ZoomFactor>] [--pageformat <landscape|portrait>]');
	phantom.exit();
}

parseOptions = function(){
	
	var defaultOptions = {
			delay: 5000,
			outputFolder: '',
			outputType: 'pdf',
			outputFile: "AutoFileName_"+ (+ new Date()),
			zoom: 1
		},
		args = Array.prototype.splice.call(arguments, 0),
		isOutputFileSpecified = args[2] && 
				args[2].indexOf('--') !== 0,
		outputFile,
		optionsArr,
		finalOptions = {
			delay: 5000,
			outputFolder: '',
			outputType: 'pdf',
			zoom: 1		
		};
		
		finalOptions.outputFile = isOutputFileSpecified ? 
				args[2].replace(/\.pdf$/i, '') :
				defaultOptions.outputFile;
				
		// TODO: Parse custom options
		if (isOutputFileSpecified) {
			args.splice(0, isOutputFileSpecified ? 3 : 2);
		}
	
              
    return finalOptions;
}