﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Collections;

/// <summary>
/// Summary description for AuthenticationComponants
/// </summary>
///

// Class of shared componants used accross AssessNET
namespace ASNETNSP
{
    public class Authentication
    {

        public static bool allowBrowser()
        {

            string strContent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToLower();


            if (strContent.IndexOf("opera") >= 0)
            {
                //FIRST: Test for Opera
                //Note: Opera is the ONLY popular browser that refers to "Opera"
                return true;
            }
            else if (strContent.IndexOf("msie") >= 0 || strContent.IndexOf("rv:11.0") >= 0)
            {
                //SECOND: Test for Explorer
                //Note: Opera MAY contain a reference to MSIE but Opera has ALREADY been found

                //if instr(strContent, "msie 9.0") > 0 then
                //    allowBrowser = false
                if (strContent.IndexOf("msie 8.0") >= 0) { return false; }
                else if (strContent.IndexOf("msie 7.0") >= 0) { return false; }
                else if (strContent.IndexOf("msie 6.0") >= 0) { return false; }
                else if (strContent.IndexOf("msie 5.0") >= 0) { return false; }
                else { return true; }
            }
            else if (strContent.IndexOf("firefox") >= 0)
            {
                //THIRD: Test for "Firefox"
                //Note: Opera MAY contain a reference to "Firefox" but Opera has ALREADY been found
                return true;
            }
            else if (strContent.IndexOf("chrome") >= 0)
            {
                //FOURTH: Test for "Chrome"
                //Note: MSIE MAY contain a reference to "Chrome" but MSIE has ALREADY been found
                return true;
            }
            else
            {
                return true;
            }

        }



        public static string returnError(string errorCode, string varUsername, string varScreenRes)
        {
            string var_info_msg = "";

            switch (errorCode)
            {
                case "0001":
                    var_info_msg = "Your login details were incorrect, <a href='https://covid.riskex.co.uk/core/modules/response/admin/reset.aspx'>click here</a> to reset your password. Please ensure your password is case-sensitive.";
                    saveStatus(2, "Username or Password invalid", varUsername, varScreenRes);
                    break;

                case "0002":
                    var_info_msg = "There is a problem with your Account licence please contact on support@assessnet.co.uk.";
                    saveStatus(2, "Username or Password invalid", varUsername, varScreenRes);
                    break;

                case "0003":
                    var_info_msg = "Our security system could not validate your details.";
                    saveStatus(2, "Security failed to validate", varUsername, varScreenRes);
                    break;

                case "0004":
                    var_info_msg = "You account has been disabled, please contact your Health and Safety manager";
                    saveStatus(2, "User Disabled", varUsername, varScreenRes);
                    break;

                case "0005":
                    var_info_msg = "You failed to provide the correct answer to your security question after 3 attempts.";
                    break;

                case "0006":
                    var_info_msg = "Your organisation's AssessNET license has expired.";
                    saveStatus(2, "Account Contract Disabled", varUsername, varScreenRes);
                    break;

                case "0007":
                    var_info_msg = "Your SA account has no valid contracts associated with it, please contact support.";
                    //saveStatus(2, "", varUsername, varScreenRes);
                    break;

                case "0008":
                    var_info_msg = "We were unable to extract the login information for that selected contract.";
                    saveStatus(2, "", varUsername, varScreenRes);
                    break;

                case "0009":
                    //Response.Redirect("asnet_maintenance.asp", true);
                    var_info_msg = "Your account is currently undergoing maintenance. Please try again later.";
                    break;

                case "0010":
                    var_info_msg = "Your memorable information is incorrect, please try again.";
                    saveStatus(2, "Username or Password invalid", varUsername, varScreenRes);
                    break;

                case "0011":
                    var_info_msg = "There is an issue with your account authenticating with our SSO service.";
                    saveStatus(2, "SSO Authenticate issue", varUsername, varScreenRes);
                    break;

                case "0012":
                    var_info_msg = "Please log in to retrieve the record details.";
                    saveStatus(2, "Email Link login", varUsername, varScreenRes);
                    break;
            }

            return var_info_msg;

        }



        public static void saveStatus(int varStatus, string varDetails, string varUsername, string varScreenRes)
        {
            string varHttpLog = HttpContext.Current.Request.ServerVariables["HTTPS"].ToString();
            string varIPLog = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            string varBrowser = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();
            string referrer = ""; // Server.HtmlEncode(Request["referrer"]).ToString();


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_LOG.dbo.logUserEntry", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@varHttpLog", varHttpLog));
                cmd.Parameters.Add(new SqlParameter("@varStatus", varStatus));
                cmd.Parameters.Add(new SqlParameter("@varDetailsLog", varDetails));
                cmd.Parameters.Add(new SqlParameter("@varUsername", varUsername));
                cmd.Parameters.Add(new SqlParameter("@varIPLog", varIPLog));
                cmd.Parameters.Add(new SqlParameter("@varScreenRes", varScreenRes));
                cmd.Parameters.Add(new SqlParameter("@varBrowser", varBrowser));
                cmd.Parameters.Add(new SqlParameter("@varReferrer", referrer));

                cmd.ExecuteNonQuery();
            }
        }





        public static string checkPasswordValidity(string var_corp_code, string var_acc_ref, string txt_new_password, string txt_new_passconf)
        {
            string var_returnValue = "0000";

            ArrayList blockList = new ArrayList();
            blockList.Add("password");
            blockList.Add("p@ssword");
            blockList.Add("passw0rd");
            blockList.Add("p@ssw0rd");
            blockList.Add("pa55word");
            blockList.Add("p@55word");
            blockList.Add("pa55w0rd");
            blockList.Add("p@55w0rd");
            blockList.Add("findiapass");
            blockList.Add("safety");
            blockList.Add("google");
            blockList.Add("online");
            blockList.Add("assessnet");
            blockList.Add("riskex");

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getUserDetails", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", var_acc_ref));

                SqlDataReader objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    blockList.Add(objrs["per_fname"].ToString());
                    blockList.Add(objrs["per_sname"].ToString());
                    blockList.Add(objrs["per_cname"].ToString());
                    blockList.Add(objrs["per_fname"].ToString() + objrs["per_sname"].ToString());
                    blockList.Add(objrs["acc_name"].ToString());
                }
                objrs.Close();
            }

            var numCount = txt_new_password.Count(Char.IsDigit);
            var upperCount = txt_new_password.Count(Char.IsUpper);
            var lowerCount = txt_new_password.Count(Char.IsLower);
            var specialCount = txt_new_password.Count(Char.IsSymbol);
            var punctuationCount = txt_new_password.Count(Char.IsPunctuation);


            if (txt_new_password != txt_new_passconf)
            {
                //entrys don't match so fail
                var_returnValue = "0001";
            }
            else if (txt_new_password.Length == 0 || txt_new_passconf.Length == 0)
            {
                //nothing entered
                var_returnValue = "0002";
            }
            else if (" ".IndexOf(txt_new_password) >= 0)
            {
                //spaces found - not allowed
                var_returnValue = "0003";
            }
            else if (txt_new_password.Length < 7)
            {
                //password to short
                var_returnValue = "0004";
            }
            else if (blockList.Contains(txt_new_password.ToLower()))
            {
                //password on block list
                var_returnValue = "0005";
            }
            else if (numCount == 0 || upperCount == 0 || lowerCount == 0 || (specialCount+punctuationCount == 0))
            {
                //password doesn't contain a mix of upper, lower, number and symbols
                var_returnValue = "0007";
            }
            else
            {
                //get last 3 passwords and salts and compare new password against history. If there's a match then fail
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getPasswordHistory", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", var_acc_ref));

                    SqlDataReader objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        while (objrs.Read())
                        {
                            string hashedPassword = Authentication.setSHA512Hash(txt_new_password, objrs["acc_pass_salt"].ToString());
                            if (hashedPassword == objrs["acc_pass"].ToString())
                            {
                                //password already used
                                var_returnValue = "0006";
                                break;
                            }
                        }
                    }
                    objrs.Close();
                }
            }

            return var_returnValue;
        }


        public static string returnPasswordError(string errorCode)
        {
            string var_returnMessage = "";

            switch (errorCode)
            {
                case "0001":
                    var_returnMessage = "Your entries did not match. Please try again.";
                    break;

                case "0002":
                    var_returnMessage = "Please enter a valid password to update your account.";
                    break;

                case "0003":
                    var_returnMessage = "Your password cannot contain spaces. Please try again.";
                    break;

                case "0004":
                    var_returnMessage = "Your password must be at least 8 characters long. Please try again.";
                    break;

                case "0005":
                    var_returnMessage = "The password entered is on a list of prohibited words. Please try again.";
                    break;

                case "0006":
                    var_returnMessage = "Your password has been used previously. Please try again.";
                    break;

                case "0007":
                    var_returnMessage = "Your password Must contain a mixture of uppercase, lowercase, numeric and symbol characters. Please try again.";
                    break;

            }

            return var_returnMessage;
        }




        public static string isPasswordCorrect(string corpCode, string accRef, string hashedPassword)
        {
            string var_passwordValidity = "0002";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(id) AS accountFound FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" + corpCode + "' AND acc_ref = '" + accRef + "' AND acc_pass = '" + hashedPassword + "'", dbConn);
                SqlDataReader objrs = cmd.ExecuteReader();
                objrs.Read();
                if ((int)objrs["accountFound"] != 0)
                {
                    var_passwordValidity = "0000";
                }
                objrs.Close();
            }

            return var_passwordValidity;
        }

        public static string getUsername(string corpCode, string accRef)
        {
            string var_username = "";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("SELECT acc_name FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" + corpCode + "' AND acc_ref = '" + accRef + "'", dbConn);
                SqlDataReader objrs = cmd.ExecuteReader();
                if (objrs.HasRows)
                {
                    objrs.Read();
                    var_username = objrs["acc_name"].ToString();
                }
                objrs.Close();
            }

            return var_username;
        }




        public static string setSalt(int size)
        {
            var rng = new System.Security.Cryptography.RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        public static string getSalt(string var_saltToFind, string var_username)
        {

            string var_salt = "";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getSalt", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_username", var_username));
                cmd.Parameters.Add(new SqlParameter("@var_saltToFind", var_saltToFind));

                SqlDataReader objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    var_salt = objrs["salt"].ToString();
                }
                else
                {
                    var_salt = setSalt(512);  // username not found, so return a random salt
                }
                objrs.Close();
            }

            return var_salt;
        }

        public static string setSHA512Hash(string input, string salt)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(input + salt);
            System.Security.Cryptography.SHA512Managed sha512hashstring = new System.Security.Cryptography.SHA512Managed();
            byte[] hash = sha512hashstring.ComputeHash(bytes);

            return ByteArrayToString(hash);
        }

        private static string ByteArrayToString(byte[] ba)
        {
            return BitConverter.ToString(ba).Replace("-", "");
        }




    }

}