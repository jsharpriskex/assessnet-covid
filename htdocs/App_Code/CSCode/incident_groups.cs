﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for incident_groups
/// </summary>
/// 
namespace ANET
{
    public class incident_groups
    {

        public static string AddIncidentGroup(string corp_code, string proposed_name)
        {
            try
            {
                using (SqlConnection dbconn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDbConn"].ConnectionString))
                {
                    dbconn.Open();

                    using (SqlCommand accbg = new SqlCommand("", dbconn))
                    {
                        accbg.CommandText = "SELECT id FROM " + HttpContext.Current.Application["DBTABLE_ACC_GROUP"] + " WHERE corp_code = @corp_code and accb_group_name = @proposed_name";
                        accbg.Parameters.Add("@corp_code", SqlDbType.VarChar).Value = corp_code;
                        accbg.Parameters.Add("@proposed_name", SqlDbType.VarChar).Value = proposed_name;


                        using (SqlDataReader objRs = accbg.ExecuteReader())
                        {
                            // check if the accb name exists or not
                            if (!objRs.HasRows)
                            {

                                int new_group_code;

                                //doesn't exist - lets add it
                                using (SqlCommand accbg2 = new SqlCommand("", dbconn))
                                {
                                    try
                                    {

                                        //what's the last accb group code to be used
                                        accbg2.CommandText = "SELECT top 1 coalesce(accb_group_code, '1995') + 5 as new_group_code  from " + HttpContext.Current.Application["DBTABLE_ACC_GROUP"] + " where corp_code = @corp_code or corp_code = @corp_code2  order by id desc ";
                                        accbg2.Parameters.Add("@corp_code", SqlDbType.VarChar).Value = corp_code;
                                        accbg2.Parameters.Add("@corp_code2", SqlDbType.VarChar).Value = corp_code + 'A';
                                        new_group_code = (int)accbg2.ExecuteScalar();

                                    }
                                    catch (Exception ex)
                                    {
                                        return "There was a problem getting the next Incident Group code (" + ex.Message + ")";
                                    }
                                }

                                using (SqlCommand accbg2 = new SqlCommand("", dbconn))
                                {
                                    try
                                    {
                                        //Lets add the group
                                        accbg2.CommandText = "INSERT into " + HttpContext.Current.Application["DBTABLE_ACC_GROUP"] + " (corp_code, accb_group_code, accb_group_name, created_by, created_date) VALUES ( @corp_code, @accb_group_code, @accb_group_name, @created_by, getdate()) ";
                                        accbg2.Parameters.Add("@corp_code", SqlDbType.VarChar).Value = corp_code;
                                        accbg2.Parameters.Add("@accb_group_code", SqlDbType.VarChar).Value = new_group_code;
                                        accbg2.Parameters.Add("@accb_group_name", SqlDbType.VarChar).Value = proposed_name;
                                        accbg2.Parameters.Add("@created_by", SqlDbType.VarChar).Value = "ADMIN";
                                        accbg2.ExecuteNonQuery();

                                        return "Incident group successfully created";
                                    }
                                    catch (Exception ex)
                                    {
                                        return "There was a problem adding the Incident Group (" + ex.Message + ")";
                                    }
                                }


                            }
                            else
                            {
                                // return error if it does
                                return "This Incident group already exists";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public static string LinkIncidentGroup(string corp_code, long group_id, string accb_code)
        {
            using (SqlConnection dbconn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDbConn"].ConnectionString))
            {
                dbconn.Open();


                //check to see if the incident group and centre are already linked
                using (SqlCommand scmd = new SqlCommand("", dbconn))
                {
                    scmd.CommandText = "SELECT id FROM " + HttpContext.Current.Application["DBTABLE_HR_ABOOK_LINK"] + " WHERE corp_code = @corp_code and accb_code = @accb_code and accb_group_code = @accb_group_code ";
                    scmd.Parameters.Add("@corp_code", SqlDbType.VarChar).Value = corp_code;
                    scmd.Parameters.Add("@accb_code", SqlDbType.VarChar).Value = accb_code;
                    scmd.Parameters.Add("@accb_group_code", SqlDbType.VarChar).Value = group_id.ToString();


                    using (SqlDataReader objRs = scmd.ExecuteReader())
                    {
                        // check if the link exists or not
                        if (!objRs.HasRows)
                        {

                            using (SqlCommand scmd2 = new SqlCommand("", dbconn))
                            {
                                try
                                {
                                    scmd2.CommandText = "INSERT into " + HttpContext.Current.Application["DBTABLE_HR_ABOOK_LINK"] + " (corp_code, accb_group_code, accb_code) VALUES (@corp_code, @accb_group_code, @accb_code) ";
                                    scmd2.Parameters.Add("@corp_code", SqlDbType.VarChar).Value = corp_code;
                                    scmd2.Parameters.Add("@accb_group_code", SqlDbType.VarChar).Value = group_id.ToString();
                                    scmd2.Parameters.Add("@accb_code", SqlDbType.VarChar).Value = accb_code;
                                    scmd2.ExecuteNonQuery();

                                    return "Incident group link successfully created";
                                }
                                catch (Exception ex)
                                {
                                    return "There was a problem linking the Incident Group (" + ex.Message + ")";
                                }

                            }
                        }
                        else
                        {
                            // If they are pass back an error
                            return "This Incident group link already exists";
                            // Otherwise add it!
                        }
                    }
                }

            }


        }
    }
}