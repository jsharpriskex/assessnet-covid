﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;



// Class of shared componants used accross AssessNET
namespace ASNETNSP
{


    public class TaskPopUp
    {

        //These controls are referenced when the object is created (see TaskPopUp func).
        private UpdatePanel _taskCreation_Modal;
        private UpdatePanel _taskInformation_Modal;

        private TextBox _taskDetails;
        private RadComboBox _taskForUser;
        private RadDatePicker _createNewTaskDatePicker;

        private Button _createNewAction_Button;
        private Button _saveAction_Button;
        private Button _cancelAction_Button;

        private HtmlGenericControl _actionDetails_Div;
        private HtmlGenericControl _actionRepeater_Div;

        private HiddenField _action_ActionCount_HiddenField;
        private HiddenField _action_CurrentProcess_HiddenField;
        private HiddenField _action_ActionReference_HiddenField;
        private HiddenField _action_HasDataLoaded_HiddenField;

        private ListView _action_ListView;
        

        private string _sectionReference;
        private string _questionReference;
        private string _actionReference;
        private string _recordReference;
        private string _corpCode;
        private string _currentProcess;
        private string _currentActionCount;
        private string _action_HasDataLoaded;

        private bool _createNormalTask;

        public TaskPopUp(UpdatePanel taskCreation_Modal, string corpCode, string recordReference, bool isPageLoad, bool createNormalTask)
        {




            // TEST DATA FOR LISTVIEW
            DataTable testTable = new DataTable();
            testTable.Columns.Add("test", typeof(string));

            _createNormalTask = createNormalTask;

            //Initiate variables through object creation
            _taskCreation_Modal = taskCreation_Modal;
            _taskInformation_Modal = (UpdatePanel)_taskCreation_Modal.FindControl("taskInformation_Modal");


            _taskDetails = (TextBox)_taskCreation_Modal.FindControl("taskDetails");
            _taskForUser = (RadComboBox)_taskCreation_Modal.FindControl("taskForUser");
            _createNewTaskDatePicker = (RadDatePicker)_taskCreation_Modal.FindControl("createNewTaskDatePicker");
            _createNewTaskDatePicker.MinDate = DateTime.Now;

            _createNewAction_Button = (Button)_taskCreation_Modal.FindControl("createNewAction_Button");
            _saveAction_Button = (Button)_taskCreation_Modal.FindControl("saveAction_Button");
            _cancelAction_Button = (Button)_taskCreation_Modal.FindControl("cancelAction_Button");

            _actionDetails_Div = (HtmlGenericControl)_taskCreation_Modal.FindControl("actionDetails_Div");
            _actionRepeater_Div = (HtmlGenericControl)_taskCreation_Modal.FindControl("actionRepeater_Div");

            _action_ListView = (ListView)_taskCreation_Modal.FindControl("action_ListView");

            


            HiddenField action_QuestionReference_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_QuestionReference_HiddenField");
            HiddenField action_SectionReference_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_SectionReference_HiddenField");
            _action_ActionReference_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_ActionReference_HiddenField");
            _action_CurrentProcess_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_CurrentProcess_HiddenField");
            _action_ActionCount_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_ActionCount_HiddenField");
            _action_HasDataLoaded_HiddenField = (HiddenField)_taskCreation_Modal.FindControl("action_HasDataLoaded");



            

            // Set the private reference variables based on the found hiddenfields in the modal.
            _questionReference = action_QuestionReference_HiddenField.Value;
            _sectionReference = action_SectionReference_HiddenField.Value;
            _actionReference = _action_ActionReference_HiddenField.Value;
            _currentProcess = _action_CurrentProcess_HiddenField.Value;
            _currentActionCount = _action_ActionCount_HiddenField.Value;
            _action_HasDataLoaded = _action_HasDataLoaded_HiddenField.Value;

            //ScriptManager.RegisterStartupScript(_taskCreation_Modal.Page, this.GetType(), "scrasdsipDt", "console.log('" + _sectionReference + "   asdasd')", true);





            _corpCode = corpCode;
            _recordReference = recordReference;



            // If there is no question or section reference, then we know not to load anything.
            // If it finds a reference, then we need to work out wether to show add action, or if actions are present show the edit stage
            if (_questionReference != "")
            {
                ReturnCurrentSection();
            }


            //Bind current button do objects new event
            _createNewAction_Button.Click += new EventHandler(_createNewAction_Button_Click);
            _saveAction_Button.Click += new EventHandler(_saveAction_Button_Click);
            _cancelAction_Button.Click += new EventHandler(_cancelAction_Button_Click);

            UpdateAll();

        }



        public void ReturnCurrentSection()
        {

            // _currentProcess will drive which section is open, so that any updates to the panel do not refresh the content entirely.
            // _currentProcess values:
            //    0 = Come in from the button, so it needs to show an action if none have been added or show repeater.
            //    1 = edit phase
            //    2 = repeater phase
            //    3 = REMOVE ACTION

            int actionCount = ReturnActionCount(_corpCode, _recordReference, _sectionReference, _questionReference);

            if (_currentProcess == "0" || _currentProcess == "3")
            {

                ClearInputs();


                // Remove action first
                if (_currentProcess == "3")
                {

                    //Remove Action


                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_ActionCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@sectionreference", _sectionReference));
                            cmd.Parameters.Add(new SqlParameter("@questionreference", _questionReference));
                            cmd.Parameters.Add(new SqlParameter("@actionreference", _actionReference));
                            cmd.Parameters.Add(new SqlParameter("@type", "DELETEACTION"));

                            objrs = cmd.ExecuteReader();



                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                    TriggerButtonChange();

                    //ScriptManager.RegisterStartupScript(_taskCreation_Modal.Page, this.GetType(), "scripDt", "console.log('corpcode: " + _corpCode +
                    //    "- recordreference:" + _recordReference +
                    //    "- sectionreference:" + _sectionReference +
                    //    "- questionreference:" + _questionReference +
                    //    "- actionreference:" + _actionReference +
                    //    "')", true);

                    _action_ActionReference_HiddenField.Value = "";
                    _action_CurrentProcess_HiddenField.Value = "0";
                    _currentProcess = "0";


                    UpdateAll();
                }


                if (actionCount == 0)
                {
                    _actionDetails_Div.Visible = true;
                    _actionRepeater_Div.Visible = false;

                    _action_CurrentProcess_HiddenField.Value = "1";
                    _currentProcess = "1";


                }
                else
                {
                    _actionDetails_Div.Visible = false;
                    _actionRepeater_Div.Visible = true;

                    // We need to initilise the listview.
                    _action_CurrentProcess_HiddenField.Value = "2";
                    _currentProcess = "2";


                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            //_action_ListView.ItemDataBound += _action_ListView_ItemDataBound;
                            //_action_ListView.ItemCreated += _action_ListView_ItemCreated;

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_ActionCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@sectionreference", _sectionReference));
                            cmd.Parameters.Add(new SqlParameter("@questionreference", _questionReference));
                            cmd.Parameters.Add(new SqlParameter("@actionreference", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTION"));

                            objrs = cmd.ExecuteReader();

                            _action_ListView.DataSource = objrs;
                            _action_ListView.DataBind();

                            // We use the ItemDataBound to bind 
                            //_action_ListView.ItemCommand += _action_ListView_OnItemCommand;


                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                }



            }
            else if (_currentProcess == "1")
            {

                //If we have an action reference, we need to pull it through!
                if (_actionReference != "" && _action_HasDataLoaded == "0")
                {
                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            //_action_ListView.ItemDataBound += _action_ListView_ItemDataBound;
                            //_action_ListView.ItemCreated += _action_ListView_ItemCreated;

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_ActionCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@sectionreference", _sectionReference));
                            cmd.Parameters.Add(new SqlParameter("@questionreference", _questionReference));
                            cmd.Parameters.Add(new SqlParameter("@actionreference", _actionReference));
                            cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTION"));

                            objrs = cmd.ExecuteReader();

                            if (objrs.HasRows)
                            {
                                objrs.Read();

                                _taskDetails.Text = objrs["action_text"].ToString();
                                _taskForUser.SelectedValue = objrs["action_to"].ToString();

                                _createNewTaskDatePicker.MinDate = DateTime.MinValue;
                                _createNewTaskDatePicker.SelectedDate = Convert.ToDateTime(objrs["action_due_date"].ToString());

                                if (_createNewTaskDatePicker.SelectedDate < DateTime.Now)
                                {
                                    _createNewTaskDatePicker.MinDate = Convert.ToDateTime(objrs["action_due_date"].ToString());
                                }
                                else
                                {
                                    _createNewTaskDatePicker.MinDate = DateTime.Now;
                                }

                                

                                //initiate userdropdown
                                UserControls.returnUserDropDown_Initilise(_taskForUser, objrs["action_to"].ToString(), "none", _corpCode);
                                _action_HasDataLoaded_HiddenField.Value = "1";
                            }


                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                else
                {

                }



                _actionDetails_Div.Visible = true;
                _actionRepeater_Div.Visible = false;


            }
            else if (_currentProcess == "2")
            {
                _actionDetails_Div.Visible = false;
                _actionRepeater_Div.Visible = true;

            }



        }


        public static int ReturnActionCount(string corpCode, string recordReference, string sectionReference, string questionReference)
        {

            int actionCount = 0;

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_ActionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                    cmd.Parameters.Add(new SqlParameter("@sectionreference", sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@questionreference", questionReference));
                    cmd.Parameters.Add(new SqlParameter("@actionreference", ""));
                    cmd.Parameters.Add(new SqlParameter("@type", "ACTIONCOUNT"));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        actionCount = Convert.ToInt32(objrs["actionCount"]);
                    }

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return actionCount;
        }


        public void SaveActionDetails()
        {

            // ADD CURRENT USER IN


            // Add or edit an action
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    string actionText = Formatting.FormatTextInputField(_taskDetails.Text);
                    string actionAssignedTo = _taskForUser.SelectedValue;
                    DateTime actionDueDate = (DateTime)_createNewTaskDatePicker.SelectedDate;


                     //ScriptManager.RegisterStartupScript(_taskCreation_Modal.Page, this.GetType(), "scripDt", "console.log('corpcode: " + _corpCode +
                     //   "- recordreference:" + _recordReference +
                     //   "- sectionreference:" + _sectionReference +
                     //   "- questionreference:" + _questionReference +
                     //   "- actionreference:" + _actionReference +
                     //   "- actiontext:" + actionText +
                     //   "- actionassignedto:" + actionAssignedTo +
                     //   "- actionduedate:" + actionDueDate +
                     //   "- currentuser:" + actionAssignedTo + "  ')", true);


                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_AddEditAction", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@sectionreference", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@questionreference", _questionReference));
                    cmd.Parameters.Add(new SqlParameter("@actionreference", _actionReference));
                    cmd.Parameters.Add(new SqlParameter("@actiontext", actionText));
                    cmd.Parameters.Add(new SqlParameter("@actionassignedto", actionAssignedTo));
                    cmd.Parameters.Add(new SqlParameter("@actionduedate", actionDueDate));
                    cmd.Parameters.Add(new SqlParameter("@currentuser", HttpContext.Current.Session["YOUR_ACCOUNT_ID"]));

                    if (_createNormalTask == true)
                    {
                        cmd.Parameters.Add(new SqlParameter("@AddTaskNotProcessing", 1));
                    }


                    objrs = cmd.ExecuteReader();





                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }



        }

        private void ClearInputs()
        {
            _taskDetails.Text = "";
            _taskForUser.SelectedValue = null;
            _taskForUser.Text = "";
            _createNewTaskDatePicker.SelectedDate = null;

            _action_ActionReference_HiddenField.Value = "";
            _action_HasDataLoaded_HiddenField.Value = "0";
        }



        // Button Handlers

        private void _createNewAction_Button_Click(object sender, EventArgs e)
        {

            _action_CurrentProcess_HiddenField.Value = "1";
            _currentProcess = "1";

            Update();
        }

        private void _saveAction_Button_Click(object sender, EventArgs e)
        {
            SaveActionDetails();

            //Force it to show adding an action in        
            _action_CurrentProcess_HiddenField.Value = "0";
            _currentProcess = "0";

            ClearInputs();

            TriggerButtonChange();

            Update();
        }

        private void _cancelAction_Button_Click(object sender, EventArgs e)
        {
            //Force it to show adding an action in        
            _action_CurrentProcess_HiddenField.Value = "0";
            _currentProcess = "0";

            ClearInputs();

            Update();
        }

        private void _editAction_Button_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(_taskCreation_Modal.Page, this.GetType(), "scrisdfsdfpDt", "console.log('" + "clicked" + "')", true);
        }


        //ListView events
        protected void _action_ListView_OnItemCommand(object sender, ListViewCommandEventArgs e)
        {
            throw new Exception("SOMETHING WENT WRONG");

            ScriptManager.RegisterStartupScript(_taskCreation_Modal.Page, this.GetType(), "scrisdfsdfpDt", "console.log('" + e.CommandName + "')", true);

            if (String.Equals(e.CommandName, "AddToList"))
            {
                // Verify that the employee ID is not already in the list. If not, add the
                // employee to the list.
                ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            }
        }


        protected void _action_ListView_ItemDataBound(object sender, ListViewItemEventArgs e)
        {

            Button editButton;
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                // Display the e-mail address in italics.
                editButton = (Button)e.Item.FindControl("edit_Action");
                editButton.Click += new EventHandler(_editAction_Button_Click);


            }

            UpdateAll();
        }

        protected void _action_ListView_ItemCreated(object sender, ListViewItemEventArgs e)
        {
            Button editButton;
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                // Display the e-mail address in italics.
                editButton = (Button)e.Item.FindControl("edit_Action");
                editButton.Click += new EventHandler(_editAction_Button_Click);


            }
            UpdateAll();
        }


        //Misc

        public void Update()
        {
            _taskInformation_Modal.Update();

    

        }

        public void UpdateAll()
        {
            _taskCreation_Modal.Update();

           

        }

        public static void Update(UpdatePanel currentUpdatePanel)
        {
            currentUpdatePanel.Update();
        }

        private void TriggerButtonChange()
        {
            ControlsCreate.RecalculateActionCount(_corpCode, _recordReference, _sectionReference, _questionReference, _saveAction_Button);
        }

    }

    public class AttachmentPopUp
    {

        //These controls are referenced when the object is created (see TaskPopUp func).
        private UpdatePanel _attachmentsModal_UpdatePanel;
        private UpdatePanel _attachmentsModal_Content_UpdatePanel;

        private HiddenField _attachment_SectionReference;
        private HiddenField _attachment_QuestionReference;
        private HiddenField _attachment_AttachCount;
        private HiddenField _attachment_Type;


        private Literal _fileList_Literal;
        private Literal _fileCount_Literal;

        private Page _currentPage;


        private string _sectionReference;
        private string _questionReference;
        private string _recordReference;
        private string _corpCode;
        private string _currentAttachCount;

        private string _attachmentReference;

        public const string relatedFilesRoot = "../../../uploads";


        // Load Function
        public AttachmentPopUp(UpdatePanel attachmentsModal_UpdatePanel, string corpCode, string recordReference)
        {
            // Locate the page object
            _currentPage = attachmentsModal_UpdatePanel.Page;


            // Find UpdatePanels and lock them into place
            _attachmentsModal_UpdatePanel = attachmentsModal_UpdatePanel;
            _attachmentsModal_Content_UpdatePanel = (UpdatePanel)_currentPage.FindControl("attachmentsModal_Content_UpdatePanel");


            // Find any HiddenFields which power the whole object
            _attachment_SectionReference = (HiddenField)_currentPage.FindControl("attachment_SectionReference");
            _attachment_QuestionReference = (HiddenField)_currentPage.FindControl("attachment_QuestionReference");
            _attachment_AttachCount = (HiddenField)_currentPage.FindControl("attachment_AttachCount");
            _attachment_Type = (HiddenField)_currentPage.FindControl("attachment_Type");

            _fileList_Literal = (Literal)_currentPage.FindControl("fileList_Literal");
            _fileCount_Literal = (Literal)_currentPage.FindControl("fileCount_Literal");


            // Set all variables
            _sectionReference = _attachment_SectionReference.Value;
            _questionReference = _attachment_QuestionReference.Value;
            _recordReference = recordReference;
            _corpCode = corpCode;

            _attachmentReference = _sectionReference + '_' + _questionReference;

            if (_attachmentReference.Length > 1)
            {
                _currentAttachCount = ReturnAttachmentCount().ToString();

                _fileCount_Literal.Text = _currentAttachCount;
                _fileList_Literal.Text = ReturnFileList();

                InitiateDropZone();


                ScriptManager.RegisterStartupScript(_currentPage, this.GetType(), "scripDfssdgffsdddst", "$('#attach_" + _questionReference + "').find('btn btn-default').addClass('btn btn-success').remove('btn btn-default');", true);

            }

        }

        // Count Function

        private int ReturnAttachmentCount()
        {
            return SharedComponants.returnAttachmentCount(_corpCode, _recordReference, _attachmentReference, "");
        }

        // Return Files List function
        private string ReturnFileList()
        {
            return SharedComponants.getFilesForDropzone(_corpCode, _recordReference, _attachmentReference, "all");
        }


        // Initilise JS function
        private void InitiateDropZone()
        {

            StringBuilder script = new StringBuilder();

            script.AppendLine(String.Format("createDropzone('', 'SR', '{0}', '{1}', '{2}', '');", _recordReference, _attachmentReference.Trim(), relatedFilesRoot));

            ScriptManager.RegisterStartupScript(_currentPage, this.GetType(), "script_DropZone", script.ToString(), true);

        }



    }

    public class CommentPopUp
    {
        private UpdatePanel _commentsModal_UpdatePanel;
        private ListView _comments_ListView;
        private Button _saveComment_Button;

        private UpdatePanel _dseMain_UpdatePanel;

        private TextBox _assessmentNoteText;
        private HiddenField _comment_QuestionReference;
        private HiddenField _comment_SectionReference;
        private HiddenField _comment_CurrentComment;
        private HiddenField _comment_CurrentStatus;
        private Page _currentPage;

        private string _sectionReference;
        private string _questionReference;
        private string _currentComment;
        private string _recordReference;
        private string _corpCode;


        public CommentPopUp(UpdatePanel commentsModal_UpdatePanel, string corpCode, string recordReference)
        {
            // Locate the page object
            _currentPage = commentsModal_UpdatePanel.Page;

            _comments_ListView = (ListView)commentsModal_UpdatePanel.FindControl("comments_ListView");

            //Initiate variables through object creation
            _commentsModal_UpdatePanel = commentsModal_UpdatePanel;

            _assessmentNoteText = (TextBox)_commentsModal_UpdatePanel.FindControl("assessmentNoteText");
            HiddenField comment_QuestionReference = (HiddenField)_commentsModal_UpdatePanel.FindControl("comment_QuestionReference");
            HiddenField comment_SectionReference = (HiddenField)_commentsModal_UpdatePanel.FindControl("comment_SectionReference");
            HiddenField comment_CurrentComment = (HiddenField)_commentsModal_UpdatePanel.FindControl("comment_CurrentComment");
            HiddenField comment_CurrentStatus = (HiddenField)_commentsModal_UpdatePanel.FindControl("comment_CurrentStatus");
            _dseMain_UpdatePanel = (UpdatePanel)_commentsModal_UpdatePanel.Page.FindControl("assessment_UpdatePanel");

            _saveComment_Button = (Button)_commentsModal_UpdatePanel.FindControl("saveComment_Button");

            _comment_QuestionReference = comment_QuestionReference;
            _comment_SectionReference = comment_SectionReference;
            _comment_CurrentComment = comment_CurrentComment;
            _comment_CurrentStatus = comment_CurrentStatus;

            _questionReference = comment_QuestionReference.Value;
            _sectionReference = comment_SectionReference.Value;
            _currentComment = comment_CurrentComment.Value;

            _corpCode = corpCode;
            _recordReference = recordReference;

            // If there is no question or section reference, then we know not to load anything.
            // If it finds a reference, then we need to work out wether to show add action, or if actions are present show the edit stage
            if (_comment_CurrentStatus.Value == "1")
            {
                if (_questionReference != "")
                {
                    ReturnNotes();
                }
            }

            _saveComment_Button.Click += new EventHandler(_saveComment_Button_Click);

           
        }

        public void ReturnNotes()
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    //_action_ListView.ItemDataBound += _action_ListView_ItemDataBound;
                    //_action_ListView.ItemCreated += _action_ListView_ItemCreated;

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_ReturnComments", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@section_reference", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@question_reference", _questionReference));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _comment_CurrentComment.Value = objrs["comment"].ToString();
                    }
                    else
                    {
                        _comment_CurrentComment.Value = "";
                    }


                    _assessmentNoteText.Text = _comment_CurrentComment.Value;
                    _comment_CurrentStatus.Value = "0";
                    _commentsModal_UpdatePanel.Update();

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void _saveComment_Button_Click(object sender, EventArgs e)
        {
            SaveComment();

            ScriptManager.RegisterStartupScript(_currentPage, this.GetType(), "scripDfssdfsdddst", "$('#comment_" + _questionReference + "').find('btn btn-default').addClass('btn btn-success').remove('btn btn-default');", true);
            //_assessmentNoteText.Text = "";
            //_comment_SectionReference.Value = "";
            //_comment_QuestionReference.Value = "";
            //_comment_CurrentComment.Value = "";
            //_comment_CurrentStatus.Value = "0";
            //_commentsModal_UpdatePanel.Update();
        }
        

        public void SaveComment()
        {

            // ADD CURRENT USER IN


            // Add or edit an action
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    string noteText = Formatting.FormatTextInputField(_assessmentNoteText.Text);
                    string currentNoteText = Formatting.FormatTextInputField(_comment_CurrentComment.Value);



                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_AddComment", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@section_reference", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@question_reference", _questionReference));
                    cmd.Parameters.Add(new SqlParameter("@comment", noteText));
                    cmd.Parameters.Add(new SqlParameter("@currentcomment", currentNoteText));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", HttpContext.Current.Session["YOUR_ACCOUNT_ID"]));

                    
                    objrs = cmd.ExecuteReader();

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }


            ScriptManager.RegisterStartupScript(_currentPage, this.GetType(), "scripDfsddst", "$('#commentsModal').modal('hide'); ColourComments('" + _questionReference + "');", true);
            _assessmentNoteText.Text = "";
            _comment_SectionReference.Value = "";
            _comment_QuestionReference.Value = "";
            _comment_CurrentComment.Value = "";
            _commentsModal_UpdatePanel.Update();


            

        }


        //private void ClearInputs()
        //{
        //    _assessmentNoteText.Text = "";
        //}

    }

} // End of Namespace