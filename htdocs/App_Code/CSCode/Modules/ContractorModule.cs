﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;


namespace ASNETNSP
{
    public class ContractorStat
    {

        private string _corpCode;

        public ContractorStat(string corpCode)
        {

            _corpCode = corpCode;

        }

        public string OrgCount()
        {

            string _OrgTotal = "0";

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("Module_CM.dbo.getOrgCount", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));

                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            _OrgTotal = objrs["countvalue"].ToString();

                        }


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _OrgTotal;

        }


        public string ConCount()
        {

            string _ConCount = "0";

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("Module_CM.dbo.getConCount", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));

                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            _ConCount = objrs["countvalue"].ToString();

                        }


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _ConCount;

        }


        public string OrgStatusCount(String StatusType)
        {

            string _OrgStatusCount = "0";
            string _statusType;

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        switch (StatusType.ToLower())
                        {
                            case "pending":
                                _statusType = "Pending";
                                break;
                            case "due soon":
                                _statusType = "Due Soon";
                                break;
                            case "approved":
                                _statusType = "Approved";
                                break;
                            default:
                                _statusType = "Pending";
                                break;

                        }

                        SqlCommand cmd = new SqlCommand("Module_CM.dbo.getOrgStatus", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@status", _statusType));

                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            _OrgStatusCount = objrs["countvalue"].ToString();

                        }


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _OrgStatusCount;

        }


        public string ConStatusCount(String statusType)
        {

            string _ConStatusCount = "0";
            string _statusType;

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        switch (statusType.ToLower())
                        {
                            case "pending":
                                _statusType = "Pending";
                                break;
                            case "due soon":
                                _statusType = "Due Soon";
                                break;
                            case "approved":
                                _statusType = "Approved";
                                break;
                            default:
                                _statusType = "Pending";
                                break;

                        }

                        SqlCommand cmd = new SqlCommand("Module_CM.dbo.getConStatus", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@status", _statusType));

                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            _ConStatusCount = objrs["countvalue"].ToString();

                        }


                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _ConStatusCount;

        }


    }

}