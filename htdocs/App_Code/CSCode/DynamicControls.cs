﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;



// Class of shared componants used accross AssessNET
namespace ASNETNSP
{


    public class ControlsCreate
    {

        //Please note: This is a proof of concept. The alternative is binding to a repeater/listview and controlling the added controls then.
        // The downside of another control is extra work when incorporating this logic elsewhere.

        private DataTable _controlData = new DataTable();
        private DataTable _controlSections = new DataTable();


        private PlaceHolder _controlPlaceHolder;
        private UpdatePanel _controlUpdatePanel;
        private Label _debugText;
        private Literal _pageTitle;
        private Literal _pageTitleDescription;
        private HiddenField _assessmentStatus;

        public bool DebugMode = false;

        private string _corpCode;
        private string _sectionReference;
        private string _recordReference;
        private int _templateID;
        private string _currentUser;

        private string _templateTitle;
        private string _templateDescription;

        private string _status;



        //Initiate the Class
        public ControlsCreate()
        {

            // Do not initiate this way
            throw new NotSupportedException("You have initiated this class using the wrong method. Please ensure links to the placeHolder, updatepanel, corpcode, currentsection, template and recordrefernce. Probably not the best way of doing it, but it works.");

        }

        public ControlsCreate(PlaceHolder currentPlaceHolder, UpdatePanel currentUpdatePanel, string corpCode, string currentSectionReference, int templateID, string recordReference, string currentUser)
        {

            _pageTitle = (Literal)currentPlaceHolder.Page.FindControl("pageTitle");
            _pageTitleDescription = (Literal)currentPlaceHolder.Page.FindControl("pageTitleDescription");

            // Create the object and set the private variables up. They are private so cannot be accessed outside of the interal object.

            _sectionReference = "";
            _controlPlaceHolder = currentPlaceHolder;
            _controlUpdatePanel = currentUpdatePanel;

            _assessmentStatus = (HiddenField)currentPlaceHolder.Page.FindControl("assessmentStatus");


            _corpCode = corpCode;
            _sectionReference = currentSectionReference;
            _templateID = templateID;
            _recordReference = recordReference;

            _debugText = (Label)_controlPlaceHolder.FindControl("textText");

            _currentUser = currentUser;

            if (_sectionReference == "")
            {
                _sectionReference = ReturnFirstSectionReference();
            }

            if(_pageTitleDescription.Text == "")
            {
                //Return title and description
                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_CustomCommands", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@sectionReference", ""));
                        cmd.Parameters.Add(new SqlParameter("@customReference", ""));
                        cmd.Parameters.Add(new SqlParameter("@questionReference", ""));
                        cmd.Parameters.Add(new SqlParameter("@type", "GENERALINFO"));


                        objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            _pageTitleDescription.Text = ASNETNSP.Formatting.FormatTextReplaceReturn(objrs["template_description"].ToString());
                            _pageTitle.Text = objrs["template_title"].ToString();
                            _assessmentStatus.Value = objrs["status"].ToString();
                        }
                        else
                        {
                            //Error - TODO
                        }


                        //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                        cmd.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            _status = _assessmentStatus.Value;



        }



        //Initiate various things on screen
        private void InitiateDataTable()
        {

            _controlData.Clear();
            if (_controlData.Columns.Count == 0)
            {
                _controlData.Columns.Add("questionReference", typeof(string));
                _controlData.Columns.Add("sectionReference", typeof(string));
                _controlData.Columns.Add("type", typeof(string));
                _controlData.Columns.Add("title", typeof(string));
                _controlData.Columns.Add("placeholder", typeof(string));
                _controlData.Columns.Add("currentValue", typeof(string));
                _controlData.Columns.Add("description", typeof(string));
                _controlData.Columns.Add("custom_type", typeof(string));
                _controlData.Columns.Add("custom_refrence", typeof(string));
                _controlData.Columns.Add("commentRequired", typeof(string));
                _controlData.Columns.Add("comment", typeof(string));
                _controlData.Columns.Add("negativeAnswer", typeof(string));
                _controlData.Columns.Add("attach_required", typeof(string));
                _controlData.Columns.Add("action_required", typeof(string));
                //_controlSections.Columns.Add("sectionReference", typeof(string));
                //_controlSections.Columns.Add("sectionTitle", typeof(string));

                //_controlSections.Rows.Add("1_abc", "Section 1 - Concept");
                //_controlSections.Rows.Add("2_xyz", "Section 2 - Concept");
                //_controlSections.Rows.Add("3_xyz", "Section 3 - Concept");


            }



            // Load datatable with data
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_ReturnData", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@sectionReference", _sectionReference));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {

                        while (objrs.Read())
                        {

                            string questionReference = objrs["question_reference"].ToString();
                            string sectionReferencee = objrs["section_reference"].ToString();
                            string questionType = objrs["question_type"].ToString();
                            string questionTitle = objrs["question_title"].ToString();
                            string placeholderText = objrs["question_placeholder"].ToString();
                            string currentValue = Formatting.FormatTextInputField(objrs["answer"].ToString());
                            string description = ASNETNSP.Formatting.FormatTextReplaceReturn(objrs["question_description"].ToString());
                            string customReference = objrs["custom_reference"].ToString();
                            string customType = objrs["customType"].ToString();
                            string commentRequired = objrs["force_comment"].ToString();
                            string comment = objrs["comment"].ToString();
                            string negativeAnswer = objrs["negative_answer"].ToString();
                            string attachRequired = objrs["attach_required"].ToString();
                            string action_required = objrs["action_required"].ToString();

                            _controlData.Rows.Add(questionReference, sectionReferencee, questionType, questionTitle
                                , placeholderText, currentValue, description, customType, customReference, commentRequired, comment
                                , negativeAnswer, attachRequired, action_required);

                        }

                    }
                    else
                    {
                        //Error - TODO
                    }


                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }






        }

        public void InitiateControls()
        {
            InitiateDataTable();
            InitiateControls(_sectionReference);
        }

        private void InitiateControls(string sectionReference)
        {

            // Create the controls on the page.
            foreach (DataRow row in _controlData.Select("sectionReference='" + sectionReference + "'"))
            {


                // Assign variables based on the DataTable column position. This may change.
                string currentControlID = row.Field<string>(0);
                string currentSectionReference = row.Field<string>(1);
                string currentControlType = row.Field<string>(2);
                string currentControlTitle = row.Field<string>(3);
                string currentControlPlaceholder = row.Field<string>(4);
                string currentValue = row.Field<string>(5);
                string description = row.Field<string>(6);
                string customType = row.Field<string>(7);
                string customReference = row.Field<string>(8);
                string commentRequired = row.Field<string>(9);
                string currentComment = row.Field<string>(10);
                string negativeAnswer = row.Field<string>(11);
                string attachRequired = row.Field<string>(12);
                string actionRequired = row.Field<string>(13);

                string[] negativeAnswer_Array = StringToArray(negativeAnswer, ',');
                string[] currentValue_Array = StringToArray(currentValue, ',');

                // If the answer is the templates negative one, then we need to declare it.
                bool currentAnswerIsNegative = false;

                // If its checkboxes multiple can be selected. We check to see if the two arrays share a value.
                bool hasCommonElements = currentValue_Array.Intersect(negativeAnswer_Array).Count() > 0;

                if ((negativeAnswer_Array.Length > 0) && (currentValue != "") && (negativeAnswer_Array.Contains(currentValue) || hasCommonElements))
                {
                    currentAnswerIsNegative = true;
                }

                if (currentComment != null)
                {
                    currentComment = Formatting.FormatTextInputField(currentComment);
                }

                // Extra logic for options with a description
                if (description.Length > 0)
                {
                    description = " <br /> <span class='small'>" + description + "</span>";
                }

                // Create a HTML element for the top of the control.
                LiteralControl startRow = new LiteralControl();
                //startRow.Text = "<div class='row'><div class='col-xs-3'>" + currentControlTitle + "</div><div class='col-xs-9'><div class='form-group'>";
                startRow.Text = "<div class='col-xs-12' style='margin-bottom:5px;'> " +
                                    "<div class='row controlRow'>" +
                                        "  <div class='col-xs-12'>" +
                                        "    <div class='row colBorder'>" +
                                            "      <div class='col-xs-12'>" +
                                                    "<div style='float: left;'>" +
                                                    "  <h4>" + currentControlTitle + description + "</h4>" +
                                                    "</div>" +
                                                     "<div class='buttonGroup'>";

                LiteralControl afterButtonRow = new LiteralControl();
                afterButtonRow.Text = "</div>" +
                                                    "</div>" +
                                            " </div>" +
                                            "  <div class='row'>" +
                                            "      <div class='col-xs-12'>";



                // Create a HTML element for the bottom of the control.
                LiteralControl endRow = new LiteralControl();
                //endRow.Text = "</div></div></div>";
                endRow.Text = " </div>" +
                                    "    </div>" +
                                    " </div>" +
                                "  </div>" +
                            "  </div> ";


                _controlPlaceHolder.Controls.Add(startRow);


                UpdatePanel new_UpdatePanel = new UpdatePanel();
                new_UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
                new_UpdatePanel.ID = "button_UpdatePanel_" + currentControlID;

                if (attachRequired == "1" || attachRequired == "2")
                {
                    LinkButton attachmentButton = new LinkButton();
                    attachmentButton.ID = "attach_" + currentControlID;
                    attachmentButton.Text = "<i class='fa fa-files-o' style='text-align:left;'></i> Attachments";

                    attachmentButton.Click += new EventHandler(attachmentButton_Click);


                    attachmentButton.CssClass = ReturnCSSClassForAttachment(_corpCode, _recordReference, _sectionReference, currentControlID);



                    new_UpdatePanel.ContentTemplateContainer.Controls.Add(attachmentButton);
                }

                if (actionRequired == "1" || actionRequired == "2")
                {
                    LinkButton actionsButton = new LinkButton();
                    actionsButton.ID = "actions_" + currentControlID;
                    actionsButton.Text = "<i class='fa fa-calendar-check-o' style='text-align:left;'></i> Remedial Actions";

                    actionsButton.Click += new EventHandler(actionsButton_Click);


                    actionsButton.CssClass = ReturnCSSClassForActions(_corpCode, _recordReference, _sectionReference, currentControlID);


                    new_UpdatePanel.ContentTemplateContainer.Controls.Add(actionsButton);


                }

                if (commentRequired == "3")
                {
                    Literal commentsButton = new Literal();

                    commentsButton.Text = "<button type='button' class='btn btn-primary pullright' id='actions_" + currentControlID + "' onclick='ShowComments(this);'><i class='fa fa-calendar-check-o' style='text-align:left;'></i> Comments</button>";


                    new_UpdatePanel.ContentTemplateContainer.Controls.Add(commentsButton);


                }

                _controlPlaceHolder.Controls.Add(new_UpdatePanel);
                _controlPlaceHolder.Controls.Add(afterButtonRow);

                //This converts the string of controlType to an Enum.
                QuestionType currentControl = (QuestionType)Enum.Parse(typeof(QuestionType), currentControlType, true);

                switch (currentControl)
                {

                    // TextBox - Single
                    case QuestionType.TextBox_Single:
                        TextBox control_1 = new TextBox();
                        control_1.ID = "control_" + currentControlID;
                        control_1.Attributes.Add("placeholder", currentControlPlaceholder);
                        control_1.CssClass = "form-control";
                        control_1.Text = currentValue;
                        _controlPlaceHolder.Controls.Add(control_1);

                        _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                        break;

                    // Textbox - Multi
                    case QuestionType.TextBox_Multi:
                        TextBox control_2 = new TextBox();
                        control_2.ID = "control_" + currentControlID;
                        control_2.Attributes.Add("placeholder", currentControlPlaceholder);
                        control_2.CssClass = "form-control";
                        control_2.TextMode = TextBoxMode.MultiLine;
                        control_2.Rows = 5;
                        control_2.Text = currentValue;
                        _controlPlaceHolder.Controls.Add(control_2);

                        _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                        break;

                    // Checkbox
                    case QuestionType.CheckBox:
                        CheckBox control_3 = new CheckBox();
                        control_3.ID = "control_" + currentControlID;
                        control_3.CssClass = "";
                        _controlPlaceHolder.Controls.Add(control_3);

                        break;

                    // Radio Buttons Y/N/NA
                    case QuestionType.Radio_YNNA:
                    case QuestionType.Radio_YN:
                        //Radio buttons are styled differently to be buttons.
                        LiteralControl startButtonY = new LiteralControl();
                        LiteralControl endButtonY = new LiteralControl();

                        LiteralControl startButtonN = new LiteralControl();
                        LiteralControl endButtonN = new LiteralControl();

                        LiteralControl startButtonNA = new LiteralControl();
                        LiteralControl endButtonNA = new LiteralControl();

                        LiteralControl startHTML = new LiteralControl();
                        LiteralControl endHTML = new LiteralControl();

                        RadioButton RadioButtonY = new RadioButton();
                        RadioButtonY.ID = "control_" + currentControlID + "_Y";
                        RadioButtonY.GroupName = currentControlID.ToString();
                        RadioButtonY.Checked = (currentValue == "Y") ? true : false;

                        RadioButton RadioButtonN = new RadioButton();
                        RadioButtonN.ID = "control_" + currentControlID + "_N";
                        RadioButtonN.GroupName = currentControlID.ToString();
                        RadioButtonN.Checked = (currentValue == "N") ? true : false;

                        RadioButton RadioButtonNA = new RadioButton();
                        RadioButtonNA.ID = "control_" + currentControlID + "_NA";
                        RadioButtonNA.GroupName = currentControlID.ToString();
                        RadioButtonNA.Checked = (currentValue == "NA") ? true : false;


                        startHTML.Text = "<div class='parentRadioGroup text-center'><div class='radioGroup'>";
                        endHTML.Text = "</div></div>";

                        startButtonY.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"Y\", \"" + commentRequired + "\")' for='control_" + currentControlID + "_Y'>";
                        endButtonY.Text = "<span>Yes</span></label>";

                        startButtonNA.Text = "<label class='blue'  onclick='triggerComments(this, \"" + negativeAnswer + "\", \"NA\", \"" + commentRequired + "\")' for='control_" + currentControlID + "_NA'>";
                        endButtonNA.Text = "<span>N/A</span></label>";

                        startButtonN.Text = "<label class='blue'  onclick='triggerComments(this, \"" + negativeAnswer + "\", \"N\", \"" + commentRequired + "\")' for='control_" + currentControlID + "_N'>";
                        endButtonN.Text = "<span>No</span></label>";

                        _controlPlaceHolder.Controls.Add(startHTML);

                        _controlPlaceHolder.Controls.Add(startButtonY);
                        _controlPlaceHolder.Controls.Add(RadioButtonY);
                        _controlPlaceHolder.Controls.Add(endButtonY);

                        _controlPlaceHolder.Controls.Add(startButtonN);
                        _controlPlaceHolder.Controls.Add(RadioButtonN);
                        _controlPlaceHolder.Controls.Add(endButtonN);

                        if (currentControl == QuestionType.Radio_YNNA)
                        {
                            _controlPlaceHolder.Controls.Add(startButtonNA);
                            _controlPlaceHolder.Controls.Add(RadioButtonNA);
                            _controlPlaceHolder.Controls.Add(endButtonNA);
                        }



                        _controlPlaceHolder.Controls.Add(endHTML);


                        //ToDo
                        _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                        break;

                    case QuestionType.Custom:
                        // This is for customised dropdowns / radiobuttons
                        // The custom_reference is used to populate control.
                        // customType, customReference have been populated.
                        DataTable temp_QuestionHolder = new DataTable();

                        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {

                                SqlDataReader objrs = null;
                                SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_CustomCommands", dbConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                                cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                                cmd.Parameters.Add(new SqlParameter("@type", "RETURNOPTIONS"));
                                cmd.Parameters.Add(new SqlParameter("@sectionReference", DBNull.Value));
                                cmd.Parameters.Add(new SqlParameter("@questionReference", DBNull.Value));

                                objrs = cmd.ExecuteReader();

                                temp_QuestionHolder.Load(objrs);

                                cmd.Dispose();
                                objrs.Dispose();

                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }


                        //Now that we have the data in a temp datatable, we need to assign it to either a dropdownlist or radio selection.
                        CustomQuestionType currentCustomType = (CustomQuestionType)Enum.Parse(typeof(CustomQuestionType), customType, true);
                        switch (currentCustomType)
                        {
                            //Dropdown
                            case CustomQuestionType.DropDown:
                                DropDownList control_DropDownList = new DropDownList();
                                control_DropDownList.ID = "control_" + currentControlID;
                                control_DropDownList.CssClass = "form-control";
                                control_DropDownList.DataSource = temp_QuestionHolder;
                                control_DropDownList.DataValueField = "opt_id";
                                control_DropDownList.DataTextField = "title";

                                control_DropDownList.DataBind();

                                control_DropDownList.Items.Insert(0, new ListItem("Please Select...", ""));

                                if (control_DropDownList.Items.FindByValue(currentValue) != null)
                                {
                                    control_DropDownList.SelectedValue = currentValue;
                                }

                                control_DropDownList.EnableViewState = false;

                                control_DropDownList.Attributes["onChange"] = "triggerComments(this, \"" + negativeAnswer + "\", this.options[this.selectedIndex].value, \"" + commentRequired + "\");";

                                _controlPlaceHolder.Controls.Add(control_DropDownList);

                                _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));


                                break;

                            //Radio Buttons
                            case CustomQuestionType.Radio:


                                LiteralControl cus_startHTML = new LiteralControl();
                                LiteralControl cus_endHTML = new LiteralControl();

                                cus_startHTML.Text = "<div class='parentRadioGroup text-center'><div class='radioGroup'>";
                                cus_endHTML.Text = "</div></div>";

                                _controlPlaceHolder.Controls.Add(cus_startHTML);

                                foreach (DataRow customRow in temp_QuestionHolder.Select())
                                {

                                    RadioButton custom_RadioButton = new RadioButton();
                                    LiteralControl custom_Start = new LiteralControl();
                                    LiteralControl custom_End = new LiteralControl();
                                    decimal current_OptId = customRow.Field<decimal>(3);
                                    string current_OptName = customRow.Field<string>(4);

                                    custom_Start.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"" + current_OptId + "\", \"" + commentRequired + "\")'>";
                                    custom_End.Text = "<span>" + current_OptName + "</span></label>";

                                    custom_RadioButton.ID = "control_" + currentControlID + "_" + current_OptId.ToString();
                                    custom_RadioButton.GroupName = currentControlID.ToString();
                                    custom_RadioButton.Checked = (currentValue == current_OptId.ToString()) ? true : false;

                                    _controlPlaceHolder.Controls.Add(custom_Start);
                                    _controlPlaceHolder.Controls.Add(custom_RadioButton);
                                    _controlPlaceHolder.Controls.Add(custom_End);


                                }

                                _controlPlaceHolder.Controls.Add(cus_endHTML);

                                _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));



                                break;

                            // Checkboxes
                            case CustomQuestionType.Checkbox:
                                LiteralControl cuscheck_startHTML = new LiteralControl();
                                LiteralControl cuscheck_endHTML = new LiteralControl();

                                cuscheck_startHTML.Text = "<div class='parentRadioGroup text-center'><div class='radioGroup'>";
                                cuscheck_endHTML.Text = "</div></div>";

                                _controlPlaceHolder.Controls.Add(cuscheck_startHTML);

                                foreach (DataRow customRow in temp_QuestionHolder.Select())
                                {

                                    CheckBox custom_CheckBox = new CheckBox();
                                    LiteralControl custom_Start = new LiteralControl();
                                    LiteralControl custom_End = new LiteralControl();
                                    decimal current_OptId = customRow.Field<decimal>(3);
                                    string current_OptName = customRow.Field<string>(4);
                                    string[] valueArray = currentValue.Split(',');

                                    custom_Start.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"" + current_OptId + "\", \"" + commentRequired + "\")'>";
                                    custom_End.Text = "<span>" + current_OptName + "</span></label>";

                                    custom_CheckBox.ID = "control_" + currentControlID + "_" + current_OptId.ToString();
                                    custom_CheckBox.Checked = (valueArray.Contains(current_OptId.ToString())) ? true : false;

                                    _controlPlaceHolder.Controls.Add(custom_Start);
                                    _controlPlaceHolder.Controls.Add(custom_CheckBox);
                                    _controlPlaceHolder.Controls.Add(custom_End);

                                }

                                _controlPlaceHolder.Controls.Add(cuscheck_endHTML);


                                // needs further jquery
                                _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));


                                break;

                            default:
                                break;
                        }








                        temp_QuestionHolder.Dispose();

                        break;

                    default:
                        break;
                }

                _controlPlaceHolder.Controls.Add(endRow);

            }


            InitiateBottomButtons();
            InitiateFinalButton();


            if (DebugMode)
            {
                _debugText.Text = this.ToString();
            }

        }



        private void InitiateBottomButtons()
        {

            PlaceHolder button_PlaceHolder = (PlaceHolder)_controlUpdatePanel.FindControl("button_PlaceHolder");
            button_PlaceHolder.Controls.Clear();

            // We create an updatepanel that encapsulates the buttons as a safeguard for postbacks (which was an issue - might not be anymore!)
            UpdatePanel new_UpdatePanel = new UpdatePanel();
            new_UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            new_UpdatePanel.ID = "new_UpdatePanel";

            LiteralControl startRow = new LiteralControl();
            startRow.Text = "<div class='col-xs-12'> " +
                                "<div class='row text-center'>" + "<div class='col-xs-12' >";


            // Page Validation handler
            CustomValidator new_CustomValidator = new CustomValidator();
            new_CustomValidator.ID = "pageValidator";
            new_CustomValidator.ValidationGroup = "page";
            new_CustomValidator.Display = ValidatorDisplay.Dynamic;
            new_CustomValidator.ClientValidationFunction = "ValidatePage";

            new_UpdatePanel.ContentTemplateContainer.Controls.Add(startRow);
            new_UpdatePanel.ContentTemplateContainer.Controls.Add(new_CustomValidator);


            // Create a HTML element for the bottom of the control.
            LiteralControl endRow = new LiteralControl();
            endRow.Text = " </div></div>" +
                        "  </div> ";




            // Run sproc that returns which buttons should be shown.
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_SectionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                    cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@type", "BUTTONS"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        string showNextButton = "0";
                        string showPreviousButton = "0";
                        string showFinalButton = "0";

                        showNextButton = objrs["showNext"].ToString();
                        showPreviousButton = objrs["showPrevious"].ToString();
                        showFinalButton = objrs["showFinal"].ToString();


                        if (showPreviousButton == "1")
                        {
                            Button previousButton = new Button();
                            previousButton.ID = "previousButton";
                            previousButton.CssClass = "btn btn-primary btn-extramargin";
                            previousButton.Text = "Previous Section";
                            previousButton.Click += new EventHandler(previous_Click);
                            previousButton.UseSubmitBehavior = false;
                            //previousButton.ValidationGroup = "page";

                            new_UpdatePanel.ContentTemplateContainer.Controls.Add(previousButton);
                        }

                        if (showNextButton == "1")
                        {
                            Button nextButton = new Button();
                            nextButton.ID = "nextButton";
                            nextButton.CssClass = "btn btn-primary btn-extramargin";
                            nextButton.Text = "Next Section";
                            nextButton.Click += new EventHandler(next_Click);
                            nextButton.UseSubmitBehavior = false;
                            nextButton.ValidationGroup = "page";

                            new_UpdatePanel.ContentTemplateContainer.Controls.Add(nextButton);
                        }

                        if (showFinalButton == "1")
                        {
                            Button finishButton = new Button();
                            finishButton.ID = "finishButton";
                            finishButton.CssClass = "btn btn-primary btn-extramargin";
                            finishButton.Text = "Finish Assessment";
                            finishButton.Click += new EventHandler(finish_Click);
                            finishButton.ValidationGroup = "page";

                            new_UpdatePanel.ContentTemplateContainer.Controls.Add(finishButton);
                        }


                    }
                    else
                    {
                        //Error - TODO
                    }


                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }



            new_UpdatePanel.ContentTemplateContainer.Controls.Add(endRow);

            button_PlaceHolder.Controls.Add(new_UpdatePanel);

        }

        private void InitiateFinalButton()
        {
            Button completeAssessment_Button = (Button)_controlPlaceHolder.Page.FindControl("completeAssessment_Button");
            completeAssessment_Button.Click += new EventHandler(completeAssessment_Click);
        }



        //Process the data (Internal)
        private void NextSection()
        {
            HiddenField sectionReference_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("sectionReference_HiddenField");

            Label sectionReference_Label_v2 = (Label)_controlPlaceHolder.Page.FindControl("sectionReference_Label_v2");
            HiddenField sectionReference_HiddenField_v2 = (HiddenField)_controlPlaceHolder.Page.FindControl("sectionReference_HiddenField_v2");


            //Save the Data!
            SaveData();

            //Clear the current items.
            ClearControls();

            //Grab the next reference and initiate
            string sectionReference = "";


            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_SectionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                    cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@type", "NEXT"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();
                        sectionReference = objrs["section_reference"].ToString();
                        _sectionReference = sectionReference;
                    }
                    else
                    {
                        //Error - TODO
                    }

                    sectionReference_Label_v2.Text = sectionReference;
                    sectionReference_HiddenField_v2.Value = sectionReference;

                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }


            InitiateControls();

            //Update
            UpdateView();

            StringBuilder script = new StringBuilder();
            script.AppendLine("$('html, body').animate({ scrollTop: 0 }, 100);");

            ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", script.ToString(), true);

        }

        private void PreviousSection()
        {
            Label sectionReference_Label_v2 = (Label)_controlPlaceHolder.Page.FindControl("sectionReference_Label_v2");
            HiddenField sectionReference_HiddenField_v2 = (HiddenField)_controlPlaceHolder.Page.FindControl("sectionReference_HiddenField_v2");

            string sectionReference = "";

            //Save the Data!
            SaveData();

            //Clear the current items.
            ClearControls();

            //Grab the previous reference and initiate
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_SectionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                    cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@type", "PREVIOUS"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();
                        sectionReference = objrs["section_reference"].ToString();
                        _sectionReference = sectionReference;
                    }
                    else
                    {
                        //Error - TODO
                    }

                    sectionReference_Label_v2.Text = sectionReference;
                    sectionReference_HiddenField_v2.Value = sectionReference;

                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }




            InitiateControls();

            //Update
            UpdateView();
        }

        private void FinishAssessment()
        {
            //Save the Data!
            SaveData();

            if(_status != "2")
            {
                //Initilise the finilise assessment modal
                ReturnFinalAssessment();
            }
            else
            {
                //if its signoff, then we take them back to the report (they would have edited)
                HttpContext.Current.Response.Redirect("assessment_report.aspx?ref=" + _recordReference + "&acc=" + _currentUser);
            }

            

        }

        private void ClearControls()
        {
            _controlPlaceHolder.Controls.Clear();
        }


        private void ReturnFinalAssessment()
        {
            UpdatePanel finalModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("finalModal_UpdatePanel");
            HiddenField superviseremails = (HiddenField)_controlPlaceHolder.Page.FindControl("superviseremails");
            HtmlGenericControl showEmailSection = (HtmlGenericControl)_controlPlaceHolder.Page.FindControl("showEmailSection");

            // We will also need to work out if we are sending their supervisers an email,
            //    or if we let the user select their own email address for a review.

            // work out if the user has any supervisers with an email.


            string ListOfSupervisers = UserControls.ReturnUserSuperviserEmails(_currentUser, _corpCode);
            bool requiredSignOff = ReturnIfSignOffRequired("RequiresSignOff");

            if (ListOfSupervisers == "" && requiredSignOff)
            {
                // No Supervisers
            }
            else
            {
                //Has supervisers
                superviseremails.Value = ListOfSupervisers;
                showEmailSection.Visible = false;

            }


            finalModal_UpdatePanel.Update();

            ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script", "$('#finalModal').modal('show');", true);
        }

        private void FinishAndRedirect()
        {

            TextBox peopleEmail = (TextBox)_controlPlaceHolder.Page.FindControl("peopleEmail");
            DropDownList domainList = (DropDownList)_controlPlaceHolder.Page.FindControl("domainList");
            HtmlGenericControl alertArea = (HtmlGenericControl)_controlPlaceHolder.Page.FindControl("alertArea");
            Literal alertAreaMsg = (Literal)_controlPlaceHolder.Page.FindControl("alertAreaMsg");
            HiddenField superviseremails = (HiddenField)_controlPlaceHolder.Page.FindControl("superviseremails");

            bool requiresSignOff = ReturnIfSignOffRequired("RequiresSignOff");

            string emailprefix = "";
            string emailsuffix = "";
            string emailaddress = "";


            string supervisorList = superviseremails.Value;

            if(supervisorList != "")
            {

            }
            else
            {
                emailprefix = peopleEmail.Text;
                emailsuffix = domainList.SelectedValue;
                emailaddress = emailprefix + "@" + domainList.SelectedValue;
                alertArea.Visible = false;
                alertArea.Attributes.Add("class", "alert alert-danger");
            }

           


            if (Messages.IsValidEmail(emailaddress) || supervisorList != "" || !requiresSignOff)
            {
            
                //Save comments
                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        TextBox finalComments_TextBox = (TextBox)_controlPlaceHolder.Page.FindControl("finalComments");
                        string finalComments = Formatting.FormatTextInput(finalComments_TextBox.Text);

                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_Finish", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@comment", finalComments));
                        cmd.Parameters.Add(new SqlParameter("@acc_ref", _currentUser));

                        if(supervisorList != "")
                        {
                            cmd.Parameters.Add(new SqlParameter("@email", supervisorList));
                        }
                        else
                        {
                            cmd.Parameters.Add(new SqlParameter("@email", emailaddress));
                        }
                   

                        objrs = cmd.ExecuteReader();

                        cmd.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            // Redirect

            HttpContext.Current.Response.Redirect("assessment_report.aspx?ref=" + _recordReference + "&acc=" + _currentUser);
            }
            else
            {
                alertArea.Visible = true;
                alertAreaMsg.Text = "The email address you provided is not valid, please check and try again.";
            }
        }





        //Public Functionality

        public void SaveData()
        {


            try
            {
                //TextBoxes
                var textBox_List = GetAllControls(_controlPlaceHolder, typeof(TextBox));

                //Get information of the text box and add it to the datatable.
                foreach (var control in textBox_List)
                {
                    TextBox currentControl = (TextBox)control;

                    string currentID = currentControl.ID.Replace("control_", "");
                    string currentValue = currentControl.Text;

                    if (currentID.Contains("comment"))
                    {

                        currentID = currentID.Replace("_comment", "");
                        DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                        row["comment"] = currentValue;

                    }
                    else
                    {
                        // update datatable
                        DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                        row["currentValue"] = currentValue;
                    }



                }


                //Checkboxes
                try
                {
                    var CheckBox_List = GetAllControls(_controlPlaceHolder, typeof(CheckBox));
                    foreach (var control in CheckBox_List)
                    {

                        CheckBox currentControl = (CheckBox)control;

                        string currentID = currentControl.ID.Replace("control_", "");
                        currentID = currentID.Substring(0, currentID.LastIndexOf("_") + 0);

                        string currentValue = "";

                        if (currentControl.Checked)
                        {
                            currentValue = currentControl.ID.Replace("control_" + currentID + "_", "") + ",";
                        }

                        // update datatable
                        DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();

                        //Logic to ensure delimation doesn't duplicate.
                        if (row["currentValue"].ToString().Contains("C"))
                        {

                        }
                        else
                        {
                            row["currentValue"] = "C";
                        }

                        row["currentValue"] = row["currentValue"].ToString() + currentValue;

                    }
                }
                catch
                {
                    throw;
                }

                // Ok so we have done the checkboxes, lets remove any instace of C from them.
                // This selects custom type that are 3 (checkbox) or those that are just checkbox.
                var checkbox_Rows = _controlData.Select("(type=5 and custom_type=3) or (type=3)");
                foreach (var row in checkbox_Rows)
                {
                    row["currentValue"] = row["currentValue"].ToString().Remove(0, 1);
                }



                //Get information of the text box and add it to the datatable.



                //Radio Boxes
                var Radio_List = GetAllControls(_controlPlaceHolder, typeof(RadioButton));

                foreach (var control in Radio_List)
                {
                    RadioButton currentControl = (RadioButton)control;

                    if (currentControl.Checked)
                    {
                        string currentID = currentControl.ID.Replace("control_", "");
                        currentID = currentID.Substring(0, currentID.LastIndexOf("_") + 0);

                        string currentValue = currentControl.ID.Replace("control_" + currentID + "_", "");



                        // update datatable
                        DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                        row["currentValue"] = currentValue;


                    }

                }

                //Dropdown Lists
                var DropDown_List = GetAllControls(_controlPlaceHolder, typeof(DropDownList));
                foreach (var control in DropDown_List)
                {
                    DropDownList currentControl = (DropDownList)control;

                    string currentID = currentControl.ID.Replace("control_", "");
                    string currentValue = currentControl.SelectedValue;

                    // update datatable
                    DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                    row["currentValue"] = currentValue;

                }




                // Now that the datatable has been saved, we need to iterate through
                // the rows and save them. The results will be flushed at a later stage
                // so it doesn't need to do it here.


                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        foreach (DataRow row in _controlData.Select("sectionReference='" + _sectionReference + "'"))
                        {


                            // Assign variables based on the DataTable column position. This may change.
                            string currentQuestionReference = row.Field<string>(0);
                            string currentSectionReference = row.Field<string>(1);
                            string currentValue = row.Field<string>(5);
                            string currentComment = row.Field<string>(10);



                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_UpdateQuestion", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@currentSection", currentSectionReference));
                            cmd.Parameters.Add(new SqlParameter("@currentQuestion", currentQuestionReference));
                            cmd.Parameters.Add(new SqlParameter("@answer", Formatting.FormatTextInput(currentValue)));
                            cmd.Parameters.Add(new SqlParameter("@comment", Formatting.FormatTextInput(currentComment)));

                            objrs = cmd.ExecuteReader();

                            cmd.Dispose();
                            objrs.Dispose();

                        }
                    }
                    catch
                    {
                        throw;
                    }


                }
            }
            catch
            {
                throw;
            }




        }

        override public string ToString()
        {
            StringBuilder returnValue = new StringBuilder();

            foreach (DataRow row in _controlData.Rows)
            {
                returnValue.AppendLine(String.Format("<br />ID: {0}, Current Value: {1} <br />", row.Field<string>(0).ToString(), row.Field<string>(5).ToString()));
            }

            return returnValue.ToString();
        }

        public void UpdateView()
        {
            _controlUpdatePanel.Update();
        }

        public string ReturnFirstSectionReference()
        {

            string sectionReference = "";

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_SectionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                    cmd.Parameters.Add(new SqlParameter("@currentSection", ""));
                    cmd.Parameters.Add(new SqlParameter("@type", "FIRST"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();
                        sectionReference = objrs["section_reference"].ToString();
                    }
                    else
                    {
                        //Error - TODO
                    }

                    Label sectionReference_Label_v2 = (Label)_controlPlaceHolder.Page.FindControl("sectionReference_Label_v2");
                    HiddenField sectionReference_HiddenField_v2 = (HiddenField)_controlPlaceHolder.Page.FindControl("sectionReference_HiddenField_v2");

                    sectionReference_Label_v2.Text = sectionReference;
                    sectionReference_HiddenField_v2.Value = sectionReference;

                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return sectionReference;

        }

        public string ReturnCurrentSectionTitle()
        {

            string sectionTitle = "";

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_SectionCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                    cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@type", "TITLE"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();
                        sectionTitle = objrs["section_title"].ToString();
                    }
                    else
                    {
                        //Error - TODO
                    }


                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return sectionTitle;
        }



        // Return string for CSS for actions and Attachments
        public static string ReturnCSSClassForActions(string corpCode, string recordReference, string sectionReference, string questionReference)
        {

            int[] actionCountArray = ReturnActionCount(corpCode, recordReference, sectionReference, questionReference);

            int actionCount = actionCountArray[0];
            int actionRequired = actionCountArray[1];

            string cssClass;

            //Action Required, 0 means do not show, 1 is forced, 2 is optional

            switch (actionRequired)
            {
                case 0:
                    cssClass = "btn btn-primary pull-right";

                    break;

                case 1:

                    if (actionCount == 0)
                    {
                        cssClass = "btn btn-danger pull-right";
                    }
                    else
                    {
                        cssClass = "btn btn-success pull-right";
                    }

                    break;

                case 2:
                    cssClass = "btn btn-primary pull-right";
                    break;


                default:
                    cssClass = "btn btn-primary pull-right";
                    break;
            }



            return cssClass;
        }

        public static string ReturnCSSClassForAttachment(string corpCode, string recordReference, string sectionReference, string questionReference)
        {
            int[] attachCountArray = ReturnAttachmentCount(corpCode, recordReference, sectionReference, questionReference);

            int attachCount = attachCountArray[0];
            int attachRequired = attachCountArray[1];

            string cssClass;

            //Action Required, 0 means do not show, 1 is forced, 2 is optional

            switch (attachRequired)
            {
                case 0:
                    cssClass = "btn btn-primary pull-right";

                    break;

                case 2:

                    if (attachCount == 0)
                    {
                        cssClass = "btn btn-danger pull-right";
                    }
                    else
                    {
                        cssClass = "btn btn-success pull-right";
                    }

                    break;

                case 1:
                    cssClass = "btn btn-primary pull-right";
                    break;


                default:
                    cssClass = "btn btn-primary pull-right";
                    break;
            }



            return cssClass;
        }

        public static int[] ReturnActionCount(string corpCode, string recordReference, string sectionReference, string questionReference)
        {

            int[] returnValues = new int[2];

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_CustomCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                    cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTIONCOUNT"));
                    cmd.Parameters.Add(new SqlParameter("@sectionReference", sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@questionReference", questionReference));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {

                        objrs.Read();

                        returnValues[0] = Convert.ToInt32(objrs["actionCount"]);
                        returnValues[1] = Convert.ToInt32(objrs["action_required"]);
                    }

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return returnValues;

        }
        public static int[] ReturnAttachmentCount(string corpCode, string recordReference, string sectionReference, string questionReference)
        {

            int[] returnValues = new int[2];

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_CustomCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                    cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@type", "RETURNFILECOUNT"));
                    cmd.Parameters.Add(new SqlParameter("@sectionReference", sectionReference));
                    cmd.Parameters.Add(new SqlParameter("@questionReference", questionReference));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {

                        objrs.Read();

                        returnValues[0] = Convert.ToInt32(objrs["fileCount"]);
                        returnValues[1] = Convert.ToInt32(objrs["attachRequired"]);
                    }

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return returnValues;

        }

        public static void RecalculateActionCount(string corpCode, string recordReference, string sectionReference, string questionReference, Control onPage)
        {
            string cssClass = ReturnCSSClassForActions(corpCode, recordReference, sectionReference, questionReference);
            ScriptManager.RegisterStartupScript(onPage.Page, onPage.GetType(), "recalucateActionCount", "RecalculateAction('" + questionReference + "', '" + cssClass + "');", true);
        }


        //Button Handlers
        private void next_Click(object sender, EventArgs e)
        {
            NextSection();
        }

        private void previous_Click(object sender, EventArgs e)
        {
            PreviousSection();
        }

        private void finish_Click(object sender, EventArgs e)
        {

            FinishAssessment();
        }

        private void actionsButton_Click(object sender, EventArgs e)
        {

            // Workout the question reference based on our selection
            LinkButton currentButton = (LinkButton)sender;
            string actionQuestionReference = currentButton.ID.Replace("actions_", "");

            //SaveData();

            // Generate current actions
            ReturnAssociatedActions(actionQuestionReference);
        }
        private void attachmentButton_Click(object sender, EventArgs e)
        {

            //SaveData();

            // Workout the question reference based on our selection
            LinkButton currentButton = (LinkButton)sender;
            string attachQuestionReference = currentButton.ID.Replace("attach_", "");

            // Generate current actions
            ReturnAssociatedAttachments(attachQuestionReference);
        }

        private void completeAssessment_Click(object sender, EventArgs e)
        {

            FinishAndRedirect();
            
        }



        //Methods relating to actions
        private void ReturnAssociatedActions(string questionReference)
        {

            // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
            HiddenField action_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("action_QuestionReference_HiddenField");
            HiddenField action_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("action_SectionReference_HiddenField");
            HiddenField action_CurrentProcess_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("action_CurrentProcess_HiddenField");
            HiddenField action_ActionCount_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("action_ActionCount_HiddenField");

            Label action_QuestionReference_Label = (Label)_controlPlaceHolder.Page.FindControl("action_QuestionReference_Label");
            Label action_SectionReference_Label = (Label)_controlPlaceHolder.Page.FindControl("action_SectionReference_Label");
            Label action_ActionCount_Label = (Label)_controlPlaceHolder.Page.FindControl("action_ActionCount_Label");

            UpdatePanel taskInformation_Modal = (UpdatePanel)_controlPlaceHolder.Page.FindControl("taskInformation_Modal");


            action_QuestionReference_HiddenField.Value = questionReference;
            action_SectionReference_HiddenField.Value = _sectionReference;

            action_CurrentProcess_HiddenField.Value = "0";

            action_ActionCount_HiddenField.Value = TaskPopUp.ReturnActionCount(_corpCode, _recordReference, _sectionReference, questionReference).ToString();
            //action_ActionCount_Label.Text = action_ActionCount_HiddenField.Value;


            //action_QuestionReference_Label.Text = questionReference;
            //action_SectionReference_Label.Text = _sectionReference;

            taskInformation_Modal.Update();


            ScriptManager.RegisterStartupScript(action_CurrentProcess_HiddenField.Page, this.GetType(), "script", "$('#newTaskModal').modal('show');", true);

        }

        private void ReturnAssociatedAttachments(string questionReference)
        {

            // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
            HiddenField attachment_SectionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_SectionReference");
            HiddenField attachment_QuestionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_QuestionReference");
            HiddenField attachment_Type = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_Type");

            UpdatePanel attachmentsModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("attachmentsModal_UpdatePanel");


            var filteredQuestion = _controlData.Select(@"questionReference = '" + questionReference + "'").FirstOrDefault();
            attachment_Type.Value = filteredQuestion.Field<string>(12);


            attachment_SectionReference.Value = _sectionReference;
            attachment_QuestionReference.Value = questionReference;

            attachmentsModal_UpdatePanel.Update();


            ScriptManager.RegisterStartupScript(attachment_SectionReference.Page, this.GetType(), "script", "$('#attachmentsModal').modal('show');", true);

        }



        private bool ReturnIfSignOffRequired(string type)
        {

            bool returnValue = false;

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SELF.dbo.Assessment_CustomCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@type", "ISSIGNOFFREQUIRED"));
                    cmd.Parameters.Add(new SqlParameter("@sectionReference", DBNull.Value));
                    cmd.Parameters.Add(new SqlParameter("@questionReference", DBNull.Value));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        if(type == "RequiresSignOff")
                        {
                            returnValue = Convert.ToBoolean(objrs["requiresSignOff"]);
                        }
                        else if (type == "SendToSupervisor")
                        {
                            returnValue = Convert.ToBoolean(objrs["send_to_superviser"]);
                        }

                    }


                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return returnValue;
        }



        //Other

        //StackOverflow reference https://stackoverflow.com/questions/3419159/how-to-get-all-child-controls-of-a-windows-forms-form-of-a-specific-type-button
        //This returns all controls that are a child of the parent control of a specific type. Does this recursively too!!
        public IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        private Control ReturnRequiredComment(Type type, string currentControlID, string commentRequired, string currentComment, bool currentNegativeAnswer)
        {
            StringBuilder script = new StringBuilder();
            LiteralControl startHtml = new LiteralControl();
            LiteralControl endHtml = new LiteralControl();
            PlaceHolder comment_PlaceHolder = new PlaceHolder();

            //comment_PlaceHolder.Controls.Add(new LiteralControl(commentRequired));

            // We know that a comment is required, and if they have a current comment.
            // If there is a current comment, we show the comment regardless.
            try
            {
                if (commentRequired == "1" || commentRequired == "2" || currentComment.Length > 0 || commentRequired == "3")
                {

                    // 1 : Force Comment - always visible
                    // 2 : Comment only shows on negative answer - only visible when answered negatively


                    TextBox comment_TextBox = new TextBox();
                    comment_TextBox.ID = "control_" + currentControlID + "_comment";
                    comment_TextBox.Attributes.Add("placeholder", "Please enter a comment...");
                    comment_TextBox.CssClass = "form-control";
                    comment_TextBox.TextMode = TextBoxMode.MultiLine;
                    comment_TextBox.Rows = 5;
                    comment_TextBox.Text = currentComment;

                    if (commentRequired == "2" || commentRequired == "3")
                    {
                        //Hide and show.
                        if (currentNegativeAnswer)
                        {
                            //Show!
                            startHtml.Text = "<div class='comment'><h5>Required Comments:</h5>";
                        }
                        else if (commentRequired == "2")
                        {
                            //Do not show. It will only show when a person explicitally selects a negative answer.
                            startHtml.Text = "<div class='comment' style='display:none'><h5>Required Comments:</h5>";
                        }
                        else if (commentRequired == "3")
                        {
                            //Do not show. It will only show when a person explicitally selects a negative answer.
                            if (currentComment.Length > 0)
                            {
                                startHtml.Text = "<div class='comment'><h5>Comments:</h5>";
                            }
                            else
                            {
                                startHtml.Text = "<div class='comment' style='display:none'><h5>Comments:</h5>";
                            }


                        }

                    }
                    else
                    {
                        startHtml.Text = "<div class='comment'><h5>Required Comments:</h5>";
                    }

                   
                    endHtml.Text = "</div>";

                    comment_PlaceHolder.Controls.Add(startHtml);
                    comment_PlaceHolder.Controls.Add(comment_TextBox);
                    comment_PlaceHolder.Controls.Add(endHtml);

                }
            }
            catch
            {
                throw;
            }



            return comment_PlaceHolder;
        }

        private string[] StringToArray(string input, char deliminator)
        {
            string[] returnArray = input.Split(deliminator);

            return returnArray;

        }


        //Enums
        enum QuestionType
        {
            TextBox_Single = 1,
            TextBox_Multi = 2,
            CheckBox = 3,
            Radio_YNNA = 4,
            Custom = 5,
            Radio_YN = 6


        };

        enum CustomQuestionType
        {
            Radio = 2,
            DropDown = 1,
            Checkbox = 3

        };

    }



} // End of Namespace