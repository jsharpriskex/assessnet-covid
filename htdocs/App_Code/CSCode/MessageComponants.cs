﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.UI;



// Class of shared componants used accross AssessNET
namespace ASNETNSP
{

    public class Messages
    {

        // Simple componant to check if an email is formatted correctly
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
        {

            // This will send back true or false based on success of email.
            // It must be run in such a way its waiting for a return value, not as a void.
            // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

            try
            {

                // Server config
                string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
                int portNumber = 587;

                // Credentials
                string from = "notifications@riskexltd.onmicrosoft.com";
                string password = "rX46812@";

                string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                    "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                    "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                    "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                    "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                    "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                    "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                    "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                    "    <tbody>" +
                                    "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:right; height:80px' valign='top' align='right'> " +
                                    "        <P style='margin-top:17px; margin-bottom:0px; font-size:40px; font-weight:bold; color:#ffffff;'>AssessNET</P>" +
                                    "        <P style='margin-top:0px; font-size:12px; color:#ffffff;'>ONLINE HEALTH AND SAFETY MANAGEMENT</P>" +
                                    "        </td></tr>" +
                                    "        <tr>" +
                                    "           <td style='padding-bottom:25px;' valign='top'>" +
                                    "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                    "               <p>" + emailBody + "</p>" +
                                    "           </td>" +
                                    "        </tr>" +
                                    "        <tr>" +
                                    "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                    "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                    "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                    "</td></tr></tbody></table></body></html>";


                // Email data
                string to = emailAddress;
                string subject = emailSubject;
                string body = template01;


                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("notifications@assessnet.co.uk");
                    mail.To.Add(to);
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = html;

                    using (SmtpClient smtp = new SmtpClient(smtpAddress))
                    {
                        smtp.EnableSsl = true;
                        smtp.Timeout = 5000;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential(from, password);
                        smtp.Send(mail);
                    }

                }

                // Emailed sent
                return true;

            }
            catch
            {

                // Email failed
                return false;

            }


        }


    } // End of Class


    // V2 of class, pulls in text from library.
    public class ASNETEmail
    {

        private string _corpCode;
        private string _typeOfEmail;
        public MailMessage Mail = new MailMessage();
        public string Body = "";
        public string Subject = "";
        public string Legal = "";
        public string EmailTitle = "";
        public string Footer = "";
        public string Signature = "";
        public string EmailTo;
        public string EmailFrom;
        public List<Attachment> Attachments = new List<Attachment>();

        public string PureHtmlBody =
            "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
            "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
            "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
            "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
            "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
            "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
            "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
            "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
            "    <tbody>" +
            "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:right; height:80px' valign='top' align='right'> " +
            "        <P style='margin-top:17px; margin-bottom:0px; font-size:40px; font-weight:bold; color:#ffffff;'>AssessNET</P>" +
            "        <P style='margin-top:0px; font-size:12px; color:#ffffff;'>ONLINE HEALTH AND SAFETY MANAGEMENT</P>" +
            "        </td></tr>" +
            "        <tr>" +
            "           <td style='padding-bottom:25px;' valign='top'>" +
            "               <h1 style='text-align:center'>{{EmailTitle}}</h1>" +
            "            {{Body}}" +
            "            {{Footer}}   " +
            "           </td>" +
            "        </tr>" +
            "        <tr>" +
            "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
            "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'>{{Legal}}</p>" +
            "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Now.Year + "</span>" +
            "</td></tr></tbody></table></body></html>";

        public ASNETEmail(string corpCode, string typeOfEmail)
        {

            _corpCode = corpCode;
            _typeOfEmail = typeOfEmail;

            InitiateMailMessage();

        }

        private void InitiateMailMessage()
        {
            

            Mail.To.Clear();
            Mail.IsBodyHtml = true;

            BuildEmailValues();

            //Grab dudes name


        }

        private void BuildEmailValues()
        {


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand(@"Module_SD.dbo.[RetrieveEmailText]", dbConn);
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@emailType", _typeOfEmail));
                cmd.Parameters.Add(new SqlParameter("@accLang", "en-gb"));

                cmd.CommandType = CommandType.StoredProcedure;

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    EmailFrom = objrs["fromText"].ToString();
                    Subject = objrs["subject"].ToString();
                    EmailTitle = objrs["email_title"].ToString();
                    Body = objrs["body"].ToString();
                    Legal = objrs["Legal"].ToString();
                    Footer = objrs["bodyfooter"].ToString();
                    Signature = objrs["signature"].ToString();


                }



                objrs.Close();
            }


            

        }




        public void SendMail()
        {

            PureHtmlBody = PureHtmlBody.Replace("{{EmailTitle}}", EmailTitle);
            PureHtmlBody = PureHtmlBody.Replace("{{Body}}", Body);
            PureHtmlBody = PureHtmlBody.Replace("{{Legal}}", Legal);
            PureHtmlBody = PureHtmlBody.Replace("{{Footer}}", Footer);



            using (SmtpClient smtp = new SmtpClient(HttpContext.Current.Application["SMTP_RELAY"].ToString()))
            {
                smtp.Credentials = new NetworkCredential(HttpContext.Current.Application["SMTP_RELAY_USERNAME"].ToString(), HttpContext.Current.Application["SMTP_RELAY_PASSWORD"].ToString());

                Mail.Body = PureHtmlBody;
                Mail.Subject = Subject;
                Mail.From = new MailAddress(EmailFrom);
                Mail.To.Add(EmailTo);


                foreach (var attachment in Attachments)
                {

                    System.Net.Mail.Attachment attch = new System.Net.Mail.Attachment(attachment.FileLocation);
                    attch.Name = "Merged RA Export " + DateTime.Now.ToShortDateString() + " " +
                                 DateTime.Now.ToShortTimeString() + ".pdf";

                    Mail.Attachments.Add(attch);

                }

                smtp.Send(Mail);
                Mail.Dispose();
            }
        }

        public void AddAttachment(string title, string fileLocation)
        {
            Attachments.Add(new Attachment(fileLocation,title));
        }



        public class Attachment
        {
            public string Title;
            public string FileLocation;

            public Attachment(string fileLocation, string title)
            {
                Title = title;
                FileLocation = fileLocation;
            }
        }



    }

} // End of Namespace