﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;


/// <summary>
/// This class takes the user's name and email and converts it into a url which will allow them to login
/// </summary>
/// 

namespace ANET
{ 
    public class FreshdeskSSO
    {  

        public static string HelpLogin(string name, string email)
        {
            const string key = "e4c2f6af24d46ebedf267d98876ffa58";
            const string pathTemplate = "http://assessnet.freshdesk.com/login/sso?name={0}&email={1}&timestamp={2}&hash={3}";
       

            string timems = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString();
            var hash = GetHash(key, name, email, timems);
            var path = String.Format(pathTemplate, name, email, timems, hash);
       
            return path;
        }

        private static string GetHash(string secret, string name, string email, string timems)
        {
     
            string input = name + secret + email + timems;
            var keybytes = Encoding.UTF8.GetBytes(secret);
            var inputBytes = Encoding.UTF8.GetBytes(input);

            var crypto = new HMACMD5(keybytes);
            byte[] hash = crypto.ComputeHash(inputBytes);

            return hash.Select(b => b.ToString("x2"))
                       .Aggregate(new StringBuilder(),
                                  (current, next) => current.Append(next),
                                  current => current.ToString());


        }

    }
}