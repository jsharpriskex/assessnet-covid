﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;



// Class of shared componants used accross AssessNET
namespace ASNETNSP
{

    public class Incident
    {

        public class IncidentGeneric
        {

            public static DataTable incidentCentres_DataTable(string defaultText, string defaultValue, string searchedFor, string corpCode, string accRef, string group, bool isGraph = false)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.ReturnIncidentCentres", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@var_YOUR_ACCOUNT_ID", accRef));
                        sqlComm.Parameters.Add(new SqlParameter("@my_rec_bg", group));
                        sqlComm.Parameters.Add(new SqlParameter("@TEXT", searchedFor));
                        sqlComm.Parameters.Add(new SqlParameter("@isGraph", isGraph));

                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(objrs);

                        objrs.Dispose();
                        sqlComm.Dispose();

                        return dt;


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }


            }

            public static DataTable IncidentGroups_DataTable(string corpCode)
            {



                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    try
                    {
                        SqlCommand cmd = new SqlCommand("Module_ACC.dbo.IncidentGroups_Return", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));



                        SqlDataReader objrs = cmd.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(objrs);

                        objrs = null;
                        cmd.Dispose();

                        
                        return dt;

                
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }

        
            }



            //This drop down is bound to the event when you type an incident centre.
            public static void returnIncidentCentresDropDown(RadComboBox userDropDown, RadComboBoxItemsRequestedEventArgs e, string dropdownMode, string corpCode, string accRef, string group, bool isCreate = false)
            {

                userDropDown.Items.Clear();

                bool showDisabled = false;

                switch (dropdownMode)
                {
                    case "any":
                        showDisabled = true;
                        break;

                    case "none":
                        showDisabled = false;
                        break;
                }

                using (DataTable data = incidentCentres_DataTable(TranslationPlugin.SharedComponantTranslate("Type to select an Incident Centre"), "0", e.Text, corpCode, accRef, group))
                {
                    try
                    {



                        // If nothing has been searched for, allow them to select Any User
                        if (e.Text == "" && e.NumberOfItems == 0 && userDropDown.Items.Count == 0)
                        {

                            userDropDown.Items.Clear();
                            for (int i = 0; i < userDropDown.Items.Count; i++)
                            {
                                userDropDown.Items[i].DataBind();
                            }

                            switch (dropdownMode)
                            {
                                default:
                                    
                                    break;

                                case "none":

                                    break;
                            }





                        }

                        if (!isCreate)
                        {
                            userDropDown.Items.Add(new RadComboBoxItem(TranslationPlugin.SharedComponantTranslate("Any Incident Centre"), "0"));
                        }

                        //Maths which works out the position
                        const int ItemsPerRequest = 20;
                        int itemOffset = e.NumberOfItems;
                        int endOffset = itemOffset + ItemsPerRequest;

                        if (endOffset > data.Rows.Count)
                        {
                            endOffset = data.Rows.Count;
                        }

                        e.EndOfItems = endOffset == data.Rows.Count;


                        //userDropDown.DataValueField = "recordreference";
                        //userDropDown.DataTextField = "recordequipment";


                        // Add the data to the list and bind it (if you do not bind it goes invisible)
                        for (int i = itemOffset; i < endOffset; i++)
                        {
                            string accbName;

                            if (data.Rows[i]["tier2_ref"].ToString() == "-1")
                            {
                                accbName = "All " + " Levels";
                            }
                            else
                            {
                                accbName = data.Rows[i]["REC_BU"].ToString();
                                if (string.IsNullOrEmpty(data.Rows[i]["REC_BL"].ToString()))
                                {
                                    //do nothing
                                }
                                else
                                {
                                    accbName = accbName + " > " + data.Rows[i]["REC_BL"].ToString();
                                    if (string.IsNullOrEmpty(data.Rows[i]["tier4_name"].ToString()))
                                    {
                                        //do nothing
                                    }
                                    else
                                    {
                                        accbName = accbName + " > " + data.Rows[i]["tier4_name"].ToString();
                                        if (string.IsNullOrEmpty(data.Rows[i]["tier5_name"].ToString()))
                                        {
                                            //do nothing
                                        }
                                        else
                                        {
                                            accbName = accbName + " > " + data.Rows[i]["tier5_name"].ToString();
                                            if (string.IsNullOrEmpty(data.Rows[i]["tier6_name"].ToString()))
                                            {
                                                //do nothing
                                            }
                                            else
                                            {
                                                accbName = accbName + " > " + data.Rows[i]["tier6_name"].ToString();
                                            }
                                        }
                                    }
                                }
                            }


                            userDropDown.Items.Add(new RadComboBoxItem(
                            accbName + " : (" + data.Rows[i]["ACCB_CODE"].ToString() + ") ", data.Rows[i]["ACCB_CODE"].ToString()));

                        }
                        for (int i = 0; i < userDropDown.Items.Count; i++)
                        {
                            userDropDown.Items[i].DataBind();
                        }


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }


            }

        public static void returnIncidentCentresDropDown_Initilise(RadComboBox currentBox, string userRef, string dropdownMode, string corpCode, string group, string accb, bool isCreate)
        {

            using (DataTable dt = incidentCentres_DataTable(TranslationPlugin.SharedComponantTranslate("Type to select an Incident Centre"), "0", accb, corpCode, userRef, group))
            {

                currentBox.Items.Clear();

                //currentBox.SelectedIndex = -1;
                currentBox.Text = "";



                if (dt.Rows.Count > 0)
                {
                    DataRow rowThatContainsACCB = dt.Select("ACCB_CODE = '" + accb + "'").FirstOrDefault();
                    currentBox.Items.Add(new RadComboBoxItem(TranslationPlugin.SharedComponantTranslate("Type to select an Incident Centre"), "0"));

                    if (!isCreate)
                    {
                        currentBox.Items.Add(new RadComboBoxItem(TranslationPlugin.SharedComponantTranslate("Any Incident Centre"), "0"));
                    }


                    string accbName;

                    if (rowThatContainsACCB != null)
                    {
                            if (rowThatContainsACCB["tier2_ref"].ToString() == "-1")
                            {
                                accbName = TranslationPlugin.SharedComponantTranslate("All Levels");
                            }
                            else
                            {
                                accbName = rowThatContainsACCB["REC_BU"].ToString();
                                if (string.IsNullOrEmpty(rowThatContainsACCB["REC_BL"].ToString()))
                                {
                                    //do nothing
                                }
                                else
                                {
                                    accbName = accbName + " > " + rowThatContainsACCB["REC_BL"].ToString();
                                    if (string.IsNullOrEmpty(rowThatContainsACCB["tier4_name"].ToString()))
                                    {
                                        //do nothing
                                    }
                                    else
                                    {
                                        accbName = accbName + " > " + rowThatContainsACCB["tier4_name"].ToString();
                                        if (string.IsNullOrEmpty(rowThatContainsACCB["tier5_name"].ToString()))
                                        {
                                            //do nothing
                                        }
                                        else
                                        {
                                            accbName = accbName + " > " + rowThatContainsACCB["tier5_name"].ToString();
                                            if (string.IsNullOrEmpty(rowThatContainsACCB["tier6_name"].ToString()))
                                            {
                                                //do nothing
                                            }
                                            else
                                            {
                                                accbName = accbName + " > " + rowThatContainsACCB["tier6_name"].ToString();
                                            }
                                        }
                                    }
                                }
                            }

                            accbName += " : (" + rowThatContainsACCB["ACCB_CODE"].ToString() + ") ";

                            currentBox.Items.Add(new RadComboBoxItem(accbName, accb));

                            currentBox.SelectedIndex = 2;
                    }

                    for (int i = 0; i < currentBox.Items.Count; i++)
                    {
                        currentBox.Items[i].DataBind();
                    }
                }

                

            }

            
        }

            public static string returnAccidentBookStructure(string tier2Name, string tier3Name, string tier4Name, string tier5Name, string tier6Name)
            {
                string AccStructure = "";

                if (tier2Name != "")
                {
                    AccStructure = AccStructure + tier2Name.ToString();
                }
                else
                {
                    return AccStructure = TranslationPlugin.SharedComponantTranslate("ERROR");
                }
                if (tier3Name != "")
                {
                    AccStructure = AccStructure + ">" + tier3Name.ToString();
                }
                if (tier4Name != "")
                {
                    AccStructure = AccStructure + ">" + tier4Name.ToString();
                }
                if (tier5Name != "")
                {
                    AccStructure = AccStructure + ">" + tier5Name.ToString();
                }
                if (tier6Name != "")
                {
                    AccStructure = AccStructure + ">" + tier6Name.ToString();
                }

                return AccStructure;

            }

            // DataTable that returns all accopts
            public static DataTable ReturnAccOptsByType_DataTable(string corpCode, string accType, bool showDisabled, string language, string selectedItem)
            {

                if (selectedItem == "")
                {
                    selectedItem = null;
                }

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.AccOpts_Return", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@type", accType));
                        sqlComm.Parameters.Add(new SqlParameter("@showDisabled", showDisabled));
                        sqlComm.Parameters.Add(new SqlParameter("@language", language));
                        sqlComm.Parameters.Add(new SqlParameter("@selectedItem", selectedItem));
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(objrs);

                        objrs.Dispose();
                        sqlComm.Dispose();

                        // Loop over each item and convert from html character
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr[1] = Formatting.FormatTextInputField(dr[1].ToString());
                        }


                        return dt;

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }

            public static string ReturnAccOptByValue(string corpCode, string accType, string language, string selectedItem)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.AccOpts_Return_Single", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@type", accType));
                        sqlComm.Parameters.Add(new SqlParameter("@language", language));
                        sqlComm.Parameters.Add(new SqlParameter("@selectedItem", selectedItem));
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            return Convert.ToString(objrs["opt_value"]);
                        }
                        else
                        {
                            return TranslationPlugin.SharedComponantTranslate("Not Specified");
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }


            public static DateTime ReturnIncidentDateTime(string corpCode, string incidentCentre, string incidentReference)
            {

                DateTime returnDate = new DateTime();

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.ReturnAccDateTime", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@accb_code", incidentCentre));
                        sqlComm.Parameters.Add(new SqlParameter("@recordreference", incidentReference));
                        sqlComm.Parameters.Add(new SqlParameter("@sub_recordreference", DBNull.Value));
                        SqlDataReader objrs = sqlComm.ExecuteReader();


                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            returnDate = Convert.ToDateTime(objrs["recorddate"]);

                        }
                        else
                        {
                            throw new Exception("Error within ReturnAccDateTime sproc");
                        }
                       
                        objrs.Dispose();
                        sqlComm.Dispose();


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

                return returnDate;
            }

            public static string RecordStatusIcons(string RecStatus, string RecInc, string RecType)
            {
                try
                {
                    if(RecInc == "INJ")
                    {
                        if(RecType == "RowClass")
                        {
                            int RecStatusValue = Convert.ToInt32(RecStatus);
                            switch (RecStatusValue)
                            {
                                case 0:
                                    return "danger";
                                case 1:
                                    return "success";
                                default:
                                    return "danger";
                            }
                        }
                        else if (RecType == "StatusIcon")
                        {
                            int RecStatusValue = Convert.ToInt32(RecStatus);
                            switch (RecStatusValue)
                            {
                                case 0:
                                    return "<i class='fa fa-times fa-2x'></i>";
                                case 1:
                                    return "<i class='fa fa-check fa-2x'></i>";
                                default:
                                    return "<i class='fa fa-times fa-2x'></i>";
                            }
                        }
                        else if (RecType == "Text")
                        {
                            int RecStatusValue = Convert.ToInt32(RecStatus);
                            switch (RecStatusValue)
                            {
                                case 0:
                                    return "Requires Completion";
                                case 1:
                                    return "Complete";
                                default:
                                    return "Requires Completion";
                            }
                        }
                        else
                        {
                            return "Error";
                        }
                    }
                    else if (RecInc == "NM")
                    {
                        if (RecType == "RowClass")
                        {
                            int RecStatusValue = Convert.ToInt32(RecStatus);
                            switch (RecStatusValue)
                            {
                                case 0:
                                    return "danger";
                                case 1:
                                    return "success";
                                case 2:
                                    return "success";
                                default:
                                    return "danger";
                            }
                        }
                        else
                        {
                            return "Error";
                        }
                    }
                    else
                    {
                        return "Error 2";
                    }
                }
                catch
                {
                    return "Error 1";
                }
            }

          
            public static string ReturnIncidentTier2(string corpCode, string incidentCentre, string type)
            {

                string returnString = "";

                try
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {

           
                        SqlCommand cmd = new SqlCommand("Module_ACC.dbo.ReturnIncidentTier2", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@accb_code", incidentCentre));


                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            switch (type)
                            {
                                case "REF":
                                    returnString = objrs["tierReference"] == DBNull.Value ? "-1" : Convert.ToString(objrs["tierReference"]);
                                    break;

                                case "NAME":
                                    returnString = objrs["tierName"] == DBNull.Value ? "All" : Convert.ToString(objrs["tierName"]);
                                    break;

                                default:
                                    break;
                            }

                        }
                

                        objrs = null;
                        cmd.Dispose();

                    }

                }
                catch (Exception ex)
                {
                    throw;
                }


                return returnString;

            }



            public static void InsertIncidentHistory(string corpCode, string accbCode, string recordRef, string subRecordRef, string accRef, 
                string incidentType, string historyText)
            {


                if (string.IsNullOrEmpty(subRecordRef))
                {
                    subRecordRef = recordRef;
                }
                
                try
                {
                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            string sql_textHistory = "INSERT INTO " + Convert.ToString(HttpContext.Current.Application["DBTABLE_GLOBAL_FPRINT"]) + " (HisRelationref,corp_code,HisBy,HisType,HisDateTime,HisMod,Tier1Ref,Tier2Ref) " +
                                                     "values('" + subRecordRef + "','" + corpCode + "','" + accRef + "','" + historyText + "',GETDATE(), '" + incidentType + "', '" + accbCode + "', '" + recordRef + "')";
                            SqlCommand sqlComm = new SqlCommand(sql_textHistory, dbConn);
                            sqlComm.ExecuteReader();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }

            }




        } // End of Class


        public class IncidentPermissions
        {
            public bool HasAccess;
            public bool HasSignOff;
            public bool HasRiddor;
            public bool HasInvestigation;
            public bool HasCharts;
            public bool HasAdmin;

            private string _corpCode;
            private string _incidentCentre;
            private string _currentUser;

            public IncidentPermissions(string corpCode, string incidentCentre, string currentUser)
            {
                _corpCode = corpCode;
                _incidentCentre = incidentCentre;
                _currentUser = currentUser;

                ReturnIncidentPermission();

                if (!HasAccess)
                {
                    HttpContext.Current.Response.Redirect("~/core/system/accessDenied.aspx", true);
                }

            }

            private void ReturnIncidentPermission()
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_HR.dbo.GetACCBAccess", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@acc_ref", _currentUser));
                        sqlComm.Parameters.Add(new SqlParameter("@accbCode", _incidentCentre));
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            HasAccess = Convert.ToBoolean(objrs["hasAccess"]);
                            HasSignOff = Convert.ToBoolean(objrs["sign_off"]);
                            HasRiddor = Convert.ToBoolean(objrs["riddor"]);
                            HasInvestigation = Convert.ToBoolean(objrs["investigation"]);
                            HasCharts = Convert.ToBoolean(objrs["charts"]);
                            HasAdmin = Convert.ToBoolean(objrs["admin"]);

                        }
                        else
                        {
                            // These already are false, or should be, so lets be sure.
                            HasAccess = false;
                            HasSignOff = false;
                            HasRiddor = false;
                            HasInvestigation = false;
                            HasCharts = false;
                            HasAdmin = false;
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }



        }


        public class RiddorFunctions
        {


            public static string returnHseRiddorStatus(string hseStatus)
            {

                int hseStatusValue;

                bool hseValue = Int32.TryParse(hseStatus, out hseStatusValue);

                if (!hseValue)
                {
                    //8 will be non injury RIDDORs
                    hseStatusValue = 8;
                }

                switch (hseStatusValue)
                {
                    case 0:
                        return "Returned Successfully";
                    case 1:
                        return "ERROR - Unreadable";
                    case 2:
                        return "ERROR - XML Validation Error";
                    case 3:
                        return "ERROR - Duplicated Reference";
                    case 4:
                        return "ERROR - Cross Validation Error";
                    case 5:
                        return "Reported Electronically - Awaiting Response";
                    case 6:
                        return "Reported Manually - Awaiting Response";
                    case 7:
                        return "Not Reported";
                    case 8:
                        return "-";
                    default:
                        return "Not Reported";
                }


            }

            public static string returnHseRiddorReportStatus(string hseStatus, string injVersion, string hseRefDoId)
            {

                if(injVersion == "3" || injVersion == "4")
                {
                    switch (hseStatus)
                    {
                        case "0":
                            return "Successfully Reported";
                        case "1":
                        case "2":
                        case "3":
                        case "4":
                            return "Returned with Errors";
                        case "5":
                        case "6":
                            return "Reported - Pending Response";
                        case "7":
                            return "Not Reported";
                            default:
                        return "Not Reported";
                    }
                }
                else
                {
                    if(hseRefDoId.Length > 0)
                    {
                        return "Successfully Reported";
                    }
                    else
                    {
                        return "Incomplete";
                    }
                }

                
            }

            public static bool ReturnLostTimeForTncidentCentre(string corpCode, string incidentCentre)
            {


                bool returnBool = false;

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.ReturnIncidentCentre_Educational", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@accb_code", incidentCentre));


                        SqlDataReader objrs = sqlComm.ExecuteReader();
                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            returnBool = Convert.ToBoolean(objrs["accb_education"]);
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();



                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }



                return returnBool;
            }

            public static string ReturnSpecifiedInjuries(string corpCode, string incidentCentre, string recordReference)
            {


                string returnString = "";

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.[ReturnRiddorStandards]", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@accb_code", incidentCentre));
                        sqlComm.Parameters.Add(new SqlParameter("@recordref", recordReference));

                        SqlDataReader objrs = sqlComm.ExecuteReader();
                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            returnString = Convert.ToString(objrs["specified_injuries"]);
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();



                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }



                return returnString;
            }


            public static bool ReturnEducationalIncidentCentre(string corpCode, string incidentCentre)
            {


                bool returnBool = false;

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.ReturnIncidentCentre_Educational", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                        sqlComm.Parameters.Add(new SqlParameter("@accb_code", incidentCentre));


                        SqlDataReader objrs = sqlComm.ExecuteReader();
                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            returnBool = Convert.ToBoolean(objrs["accb_education"]);
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();



                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }



                return returnBool;
            }

            public static DataTable hseAccOPts_DataTable(string category, string selection, string type)
            {
                if (selection == "")
                {
                    selection = "0";
                }

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.returnHSEAccOpts", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@category", category));
                        sqlComm.Parameters.Add(new SqlParameter("@selection", selection));
                        sqlComm.Parameters.Add(new SqlParameter("@type", type));
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        DataTable dt = new DataTable();
                        dt.Load(objrs);

                        objrs.Dispose();
                        sqlComm.Dispose();

                        return dt;


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }


            }

           
            public static string hseAccOPtsValue(string category, string selection)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {


                        SqlCommand sqlComm = new SqlCommand("Module_ACC.dbo.returnHSEAccOpts_Single", dbConn);
                        sqlComm.CommandType = CommandType.StoredProcedure;
                        sqlComm.Parameters.Add(new SqlParameter("@type", category));
                        sqlComm.Parameters.Add(new SqlParameter("@selectedItem", selection));
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            return Convert.ToString(objrs["opt_title"]);
                        }
                        else
                        {
                            return "Not Specified";
                        }

                        objrs.Dispose();
                        sqlComm.Dispose();


                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

        }

        public class InvestigationFunctions
        {


            public static string returnInvestigationType(string type, string initialOnly)
            {
                
                switch (type)
                {
                    case "I":
                        if(initialOnly == "Y")
                        {
                            return "Investigation";
                        }
                        else
                        {
                            return "Initial Investigation";
                        }
                    case "F":
                        return "Full Investigation";
                    default:
                        return "ERR";
                }


            }



        }

    }

   

} // End of Namespace