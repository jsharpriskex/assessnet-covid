﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.IO;


// Class of shared componants used accross AssessNET
namespace ASNETNSP
{
    public class Calendar
    {

        public static string getDays(string day)
        {
            switch (day)
            {
                case "1":
                    return "Monday";
                case "2":
                    return "Tuesday";
                case "3":
                    return "Wednesday";
                case "4":
                    return "Thursday";
                case "5":
                    return "Friday";
                case "6":
                    return "Saturday";
                case "7":
                    return "Sunday";
                default:
                    return "";
            }
        }

        public static string getWeek(string week)
        {
            switch (week)
            {
                case "1":
                    return "First";
                case "2":
                    return "Second";
                case "3":
                    return "Third";
                case "4":
                    return "Fourth";
                case "5":
                    return "Last";
                default:
                    return "";
            }
        }

        public static string getMonths(string month)
        {
            switch (month)
            {
                case "1":
                    return "January";
                case "2":
                    return "February";
                case "3":
                    return "March";
                case "4":
                    return "April";
                case "5":
                    return "May";
                case "6":
                    return "June";
                case "7":
                    return "July";
                case "8":
                    return "August";
                case "9":
                    return "September";
                case "10":
                    return "October";
                case "11":
                    return "November";
                case "12":
                    return "December";
                default:
                    return "";
            }
        }

        public static string getDayDenote(string dayNumber)
        {
            switch (dayNumber)
            {
                case "1":
                case "21":
                case "31":
                    return "st";
                case "2":
                case "22":
                    return "nd";
                case "3":
                    return "rd";
                default:
                    return "th";
            }
        }

    }


    public class Formatting
    {

        public static string FormatDateTime(Object DateInput, int OutputType, bool tracker = false)
        {

            string dateFormat = TranslationPlugin.SharedComponantTranslate("dd/MM/yyyy");
            string timeFormat = TranslationPlugin.SharedComponantTranslate("dd/MM/yyyy HH:mm");
            string invalid = "";

            if (tracker == true)
            {
                invalid = TranslationPlugin.SharedComponantTranslate("Unknown");
            }
            else
            {
                invalid = TranslationPlugin.SharedComponantTranslate("Invalid Date");
            }
            

            try
            {
                // Globalisation to still go in.
                DateTime DateTimeValue = Convert.ToDateTime(DateInput.ToString());

                switch (OutputType) {
                    case 1:
                        return DateTimeValue.ToString(dateFormat);
                    case 2:
                        return DateTimeValue.ToString(timeFormat);
                    default:
                        return DateInput.ToString();
                }


            } catch (System.Exception e)
            {
                return invalid;
            }

        }


//        public static string FormatDateCSS(Object DateInput)
//        {
//            string css_string = "";
//            DateTime dDate;
//
//            if (DateTime.TryParse(DateInput.ToString(), out dDate))
//            {
//                DateTime todaysDate = DateTime.Today;
//                DateTime DateTimeValue = Convert.ToDateTime(DateInput.ToString());
//
//                if (DateTimeValue < todaysDate)
//                {
//                    css_string = "red-colour'";
//                }
//            }
//
//            return css_string;
//        }

        public static string FormatTextbyLength(object TextInput, int MaxLength)
        {

            string TextValue = TextInput.ToString();

            if (TextValue != null && TextValue.Length > MaxLength)
            {
                // Ok its bigger than 20 and not a NULL so lets strip it down
                TextValue = TextValue.Substring(0, MaxLength) + "...";
                return TextValue;
            }

            return TextValue;

        }

        public static string FormatFullNameDotSurname(object Fnameinput, object Snameinput)
        {
            string invalid = TranslationPlugin.SharedComponantTranslate("Name Invalid");

            string Firstname = Fnameinput.ToString();
            string Surname = Snameinput.ToString();

            if (Firstname.Length > 0 && Surname.Length > 0)
            {
                return Firstname[0].ToString().ToUpper() + "." + Surname;
            }

            return invalid;

        }


        public static string FormatTextReplaceReturn(string myString)
        {

            if (String.IsNullOrEmpty(myString))
            {
                return myString;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            myString = myString.Replace(System.Environment.NewLine, "<br />").Replace("\r\n", "<br />").Replace("\n", "<br />").Replace("\r", "<br />").Replace(lineSeparator, "<br />").Replace(paragraphSeparator, "<br />");

            return myString;

        }

        // It loops through the linebreaks and replaces with a list.
        public static string returnTextReplaceReturnList(string inputString)
        {


            if (inputString != "")
            {
                string replaceWith = "~#~";
                string removedLineBreaks = inputString.Replace("\r\n", replaceWith).Replace("\n", replaceWith)
                    .Replace("\r", replaceWith);
                string[] stringArray = removedLineBreaks.Split(new[] {replaceWith}, StringSplitOptions.None);

                StringBuilder stringStringBuilder = new StringBuilder();

                stringStringBuilder.Append(@"<ul>");

                foreach (string stringSingle in stringArray)
                {
                    if (stringSingle != "")
                    {
                        stringStringBuilder.Append(@"<li>" + stringSingle + @"</li>");
                    }

                }

                stringStringBuilder.Append(@"<ul>");

                return stringStringBuilder.ToString();
            }
            else
            {
                return "";
            }


        }


        /// <summary>
        /// This Encodes the users data. Use this for SAVING or CAPTURING the data.
        /// </summary>
        /// <param name="myString"></param>
        /// <returns></returns>
        public static string FormatTextInput(string myString)
        {

            myString = HttpContext.Current.Server.HtmlEncode(myString);

            return myString;

        }


        /// <summary>
        /// This Decodes the users data. Use this for RETURNING data.
        /// </summary>
        /// <param name="myString"></param>
        /// <returns></returns>
        public static string FormatTextInputField(string myString)
        {

            myString = HttpContext.Current.Server.HtmlDecode(myString);

            return myString;

        }




        public static string FormatReviewDateSpan(string taskDueDate)
        {
            DateTime dt1 = DateTime.Parse(taskDueDate);
            DateTime dt2 = DateTime.Now;
            DateTime dt3 = DateTime.Now.AddDays(30);

            if (dt1.Date < dt2.Date)
            {
                string reviewdatestring = "red-colour";
                return reviewdatestring;
            }
            if (dt1 > dt2 && dt1 < dt3 || dt1.Date == dt2.Date)
            {
                string reviewdatestring = "urgent";
                return reviewdatestring;
            }
            else
            {
                string reviewdatestring = "";
                return reviewdatestring;
            }
        }


    }


    public class UserControls
    {

        public static string NewAccountRef(string corpCode)
        {
            string newRef = "";
            bool valid = false;

            while (valid != true)
            {
                newRef = Guid.NewGuid().ToString("N").Substring(0, 15);

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_HR.dbo.ExistingUserCheck", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", newRef));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        valid = false;
                    }
                    else
                    {
                        valid = true;
                    }
                }

            }

            return newRef;

        }

        public static string RandomPassword()
        {
            return Guid.NewGuid().ToString("N").Substring(0, 10);
        }

        public static string GetEmail(string AccountReference, string corpCode)
        {
            string invalid = TranslationPlugin.SharedComponantTranslate("No Account Ref Given");

            string returnEmail = "";

            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {
                    if (AccountReference != null)
                    {
                        SqlDataReader rdr = null;
                        SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.GetUsersEmailAddress", dbconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@AccountRef", AccountReference));
                        rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            returnEmail =  rdr["EmailAddress"].ToString();
                        }

                    }
                    else
                    {
                        returnEmail = invalid;
                    }
                }

            }
            catch (System.Exception e)
            {
                returnEmail = e.ToString();
            }

            return returnEmail;

        }

        public static string ReturnUserFromEmail(string email, string corpCode, int type)
        {
            string error = TranslationPlugin.SharedComponantTranslate("ERROR");

            string accRef = "";
            int countOfAccounds = 0;

            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {
                    if (email != null)
                    {
                        SqlDataReader rdr = null;
                        SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.GetUsersFromEmail", dbconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@email", email));
                        rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            accRef = rdr["acc_ref"].ToString();
                            countOfAccounds = Convert.ToInt32(rdr["myCount"]);
                        }

                    }
                    else
                    {
                    }
                }

            }
            catch
            {
                throw;
            }


            // Type 1 = Return Email as Normal
            // Type 2 = Return error if there are more than 1 account associated to the email.

            if(type == 1)
            {

            }
            else if(type == 2)
            {
                if(countOfAccounds > 1)
                {
                    accRef = error;
                }
            }



            return accRef;
        }

        public static string ReturnUserSuperviserEmails(string AccountReference, string corpCode)
        {
            string returnValue = "";

            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {
                    if (AccountReference != null)
                    {
                        SqlDataReader rdr = null;
                        SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.getUsersSupervisersEmail", dbconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@accountref", AccountReference));
                        rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            returnValue = rdr["ListOfEmails"].ToString();
                        }

                    }
                    else
                    {
                    }
                }

            }
            catch
            {
                throw;
            }


            return returnValue;

        }

        // Will get users full name based on account reference
        public static string GetUser(string AccountReference, string corpCode)
        {

            string noUser = TranslationPlugin.SharedComponantTranslate("No User Found");
            string noAccRef = TranslationPlugin.SharedComponantTranslate("No Account Ref Given");

            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {
                    if (AccountReference != null)
                    {
                        SqlDataReader rdr = null;
                        SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.GetUsersFullName", dbconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@AccountRef", AccountReference));
                        rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {
                            return rdr["FullName"].ToString();
                        }

                        return noUser;
                    }
                    else
                    {
                        return noAccRef;
                    }
                }

            }
            catch (System.Exception e)
            {
                return e.ToString();
            }
        }

        public static DataTable returnUserList(string corpCode)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    string sql_text = "SELECT distinct data.Acc_Ref, display_name  as user_details, acc_status " +
                                      " FROM MODULE_HR.dbo.HR_Data_users as data " +
                                      " LEFT OUTER JOIN  MODULE_PEP.dbo.Structure_Data_tier4  AS tier4 " +
                                      " on data.corp_code = tier4.corp_code " +
                                      " and data.corp_bd = cast(tier4.tier4_ref as varchar(30)) " +
                                      " WHERE data.corp_code='" + corpCode + "' " +
                                      " AND data.Acc_level NOT LIKE '5'   " +
                                      " AND (data.acc_name NOT LIKE 'sysadmin.%')  " +
                                      " AND data.Acc_level NOT LIKE '3'  " +
                                      " AND data.Acc_status NOT LIKE 'D' " +
                                      " AND data.Acc_status NOT LIKE 'V' " +
                                      " AND data.display_name IS NOT NULL " +
                                      " ORDER BY user_details ASC";

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable returnDataTable = new DataTable();
                    returnDataTable.Load(objrs);

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        //Brings back a list of all the permissions a user has.
        public static List<UserPermissions> returnUserPermission(string accountReference, string corpCode)
        {

            List<UserPermissions> userPermissionList = new List<UserPermissions>();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_HR.dbo.GetUserPermissions_AllModules", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", accountReference));
                    objrs = cmd.ExecuteReader();


                    if (objrs.HasRows)
                    {
                        //                        objrs.Read();
                        //
                        //                        for (int i = 0; i < objrs.FieldCount; i++)
                        //                        {
                        //
                        //                            UserPermissions newPermission = new UserPermissions
                        //                            {
                        //                                Module = objrs.GetName(i),
                        //                                Access = Convert.ToString(objrs[i])
                        //                            };
                        //
                        //                            userPermissionList.Add(newPermission);
                        //                        }

                        while (objrs.Read())
                        {
                            UserPermissions newPermission = new UserPermissions
                            {
                                Module = objrs["moduleName"].ToString(),
                                Access = Convert.ToString(objrs["permission"])
                            };

                            userPermissionList.Add(newPermission);
                        }

                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }

            return userPermissionList;
        }



        // ----------------------------------------------------
        //                  New User DropDowns
        // ----------------------------------------------------

        public static DataTable users_DataTable(string defaultText, string defaultValue, string searchedFor, string corpCode, bool showDisabled, string userRef = "", string setModuleFilter = "")
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    string showDisabled_String = "";

                    if (showDisabled == true)
                    {
                        //showDisabled_String = " AND data.acc_status <> 'D' ";
                        showDisabled_String = "D";
                    }

                    //string sql_text =
                    //    "SELECT distinct data.Acc_Ref, CASE WHEN tier4.struct_name IS NULL THEN per_cname ELSE per_cname + ' (' + tier4.struct_name + ')' END  as user_details, acc_status FROM MODULE_HR.dbo.HR_Data_users as data " +
                    //    " LEFT OUTER JOIN  MODULE_PEP.dbo.Structure_Data_tier4  AS tier4  " +
                    //    "on data.corp_code = tier4.corp_code " +
                    //    "and data.corp_bd = cast(tier4.tier4_ref as varchar(30)) " +
                    //    "WHERE data.corp_code='" + corpCode +
                    //    "' AND data.Acc_level NOT LIKE '5'   AND (data.acc_name NOT LIKE 'sysadmin.%')  AND data.Acc_level NOT LIKE '3' and per_cname <> '' and per_cname like '%" +
                    //    searchedFor + "%'  " + showDisabled_String + " ORDER BY user_details ASC";
                    //
                    //SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    //SqlDataReader objrs = sqlComm.ExecuteReader();

                    SqlCommand sqlComm = new SqlCommand("Module_HR.dbo.GetUserList", dbConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.Add(new SqlParameter("@CORP_CODE", corpCode));
                    sqlComm.Parameters.Add(new SqlParameter("@ACC_REF", userRef));
                    sqlComm.Parameters.Add(new SqlParameter("@LOOKUP_VALUE", searchedFor));
                    sqlComm.Parameters.Add(new SqlParameter("@SHOW_DISABLED", showDisabled_String));
                    sqlComm.Parameters.Add(new SqlParameter("@STATE", "get"));
                    sqlComm.Parameters.Add(new SqlParameter("@MODULE", setModuleFilter));
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(objrs);

                    objrs.Dispose();
                    sqlComm.Dispose();

                    return dt;


                }
                catch (Exception)
                {
                    throw;
                }
            }


        }


        //This drop down is bound to the event when you type a name.
        public static void returnUserDropDown(RadComboBox userDropDown, RadComboBoxItemsRequestedEventArgs e, string dropdownMode, string corpCode, string userRef = "", string setModuleFilter = "")
        {
            string anyUser = TranslationPlugin.SharedComponantTranslate("Any User");
            string typeName = TranslationPlugin.SharedComponantTranslate("Type a name to find a user") + "...";

            userDropDown.Items.Clear();

            bool showDisabled = false;

            switch (dropdownMode)
            {
                case "any":
                    showDisabled = true;
                    break;

                case "none":
                    showDisabled = false;
                    break;
            }

            using (DataTable data = users_DataTable(anyUser, "0", e.Text, corpCode, showDisabled, userRef, setModuleFilter))
            {
                try
                {

                    // If nothing has been searched for, allow them to select Any User
                    if (e.Text == "" && e.NumberOfItems == 0 && userDropDown.Items.Count == 0)
                    {
                        userDropDown.Items.Clear();
                        for (int i = 0; i < userDropDown.Items.Count; i++)
                        {
                            userDropDown.Items[i].DataBind();
                        }


                        switch (dropdownMode)
                        {
                            case "any":
                                //userDropDown.Items.Add(new RadComboBoxItem("Type a name to find a user....", "0"));
                                userDropDown.Items.Add(new RadComboBoxItem(anyUser, "0"));
                                break;

                            case "none":
                                userDropDown.Items.Add(new RadComboBoxItem(typeName, "0"));
                                break;
                        }

                    }


                    //Maths which works out the position
                    const int ItemsPerRequest = 20;
                    int itemOffset = e.NumberOfItems;
                    int endOffset = itemOffset + ItemsPerRequest;

                    if (endOffset > data.Rows.Count)
                    {
                        endOffset = data.Rows.Count;
                    }

                    e.EndOfItems = endOffset == data.Rows.Count;


                    // Add the data to the list and bind it (if you do not bind it goes invisible)
                    for (int i = itemOffset; i < endOffset; i++)
                    {
                        userDropDown.Items.Add(new RadComboBoxItem(
                            data.Rows[i]["user_details"].ToString(), data.Rows[i]["acc_ref"].ToString()));
                    }
                    for (int i = 0; i < userDropDown.Items.Count; i++)
                    {
                        userDropDown.Items[i].DataBind();
                    }


                }
                catch (Exception)
                {
                    throw;
                }
            }


        }


        // Initilise User Drop Down.
        // This dropdown essentially "kick starts" the drop down on page load, or subsequent postbacks NOT relating to the text being typed into it.
        // It accepts the RadComboBox in question, a selected user ref, a dropdownmode and a corpcode.
        // Each parameter does the following:
        //
        // - currentBox: Important. This is the combobox you will be changing.
        // - userRef: You parse the selected userRef for whatever you need into it. If no user is needed, pass "" into it.
        // - dropdownMode: "any" adds "Any User" to the dropdown. "none" just adds "Type a name to find a user..."
        // - setModuleFilter: OPTIONAL: pass the module code if a specific query is needed. Passes through to the SPROC  -- MG 15/02/2018

        //CG Created 16/01/2018

        public static void returnUserDropDown_Initilise(RadComboBox currentBox, string userRef, string dropdownMode, string corpCode, string setModuleFilter = "")
        {
            string anyUser = TranslationPlugin.SharedComponantTranslate("Any User");
            string typeName = TranslationPlugin.SharedComponantTranslate("Type a name to find a user") + "...";

            using (DataTable dt = users_DataTable(anyUser, "0", "", corpCode, false))
            {

                currentBox.Items.Clear();

                //currentBox.SelectedIndex = -1;
                currentBox.Text = "";



                currentBox.DataSource = dt;
                currentBox.DataValueField = "Acc_Ref";
                currentBox.DataTextField = "user_details";

                switch (dropdownMode)
                {
                    case "any":
                        //currentBox.Items.Add(new RadComboBoxItem("Type a name to find a user....", "0"));
                        currentBox.Items.Add(new RadComboBoxItem(anyUser, "0"));
                        break;

                    case "none":
                        currentBox.Items.Add(new RadComboBoxItem(typeName, "0"));
                        break;

                    default:
                        currentBox.Items.Add(new RadComboBoxItem(typeName, "0"));
                        break;
                }

                if (userRef != "")
                {
                    try
                    {
                        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {

                                //    string sql_text =
                                //        "SELECT distinct data.Acc_Ref, CASE WHEN tier4.struct_name IS NULL THEN per_cname ELSE per_cname + ' (' + tier4.struct_name + ')' END  as user_details, acc_status FROM MODULE_HR.dbo.HR_Data_users as data " +
                                //        " LEFT OUTER JOIN  MODULE_PEP.dbo.Structure_Data_tier4  AS tier4  " +
                                //        "on data.corp_code = tier4.corp_code " +
                                //        "and data.corp_bd = cast(tier4.tier4_ref as varchar(30)) " +
                                //        "WHERE data.corp_code='" + corpCode +
                                //        "' AND data.Acc_level NOT LIKE '5'   AND (data.acc_name NOT LIKE 'sysadmin.%')  AND data.Acc_level NOT LIKE '3' and data.acc_ref = '" +
                                //        userRef.Replace("'","") + "'   ORDER BY user_details ASC";
                                //
                                //    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                                //    SqlDataReader objrs = sqlComm.ExecuteReader();

                                SqlCommand sqlComm = new SqlCommand("Module_HR.dbo.GetUserList", dbConn);
                                sqlComm.CommandType = CommandType.StoredProcedure;
                                sqlComm.Parameters.Add(new SqlParameter("@CORP_CODE", corpCode));
                                sqlComm.Parameters.Add(new SqlParameter("@ACC_REF", userRef.Replace("'", "")));
                                sqlComm.Parameters.Add(new SqlParameter("@LOOKUP_VALUE", ""));
                                sqlComm.Parameters.Add(new SqlParameter("@SHOW_DISABLED", ""));
                                sqlComm.Parameters.Add(new SqlParameter("@STATE", "initiate"));
                                sqlComm.Parameters.Add(new SqlParameter("@MODULE", setModuleFilter));
                                SqlDataReader objrs = sqlComm.ExecuteReader();

                                if (objrs.HasRows)
                                {
                                    objrs.Read();

                                   currentBox.Items.Add(new RadComboBoxItem(Formatting.FormatTextInputField(objrs["user_details"].ToString()),
                                        objrs["Acc_Ref"].ToString()));

                                    currentBox.SelectedValue = userRef;
                                }

                                for (int i = 0; i < currentBox.Items.Count; i++)
                                {
                                    currentBox.Items[i].DataBind();
                                }



                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }


                    }

                    catch (Exception e)
                    {
                        throw;
                    }
                }
                else
                {

                    for (int i = 0; i < currentBox.Items.Count; i++)
                    {
                        currentBox.Items[i].DataBind();
                    }

                    if (dropdownMode == "any")
                    {
                        currentBox.SelectedIndex = 0;
                        //currentBox.SelectedValue = "Any User";
                    }
                    else
                    {
                        currentBox.SelectedIndex = 0;
                    }



                }





            }


        }

        public static void validate_UserDropDown(CustomValidator validator, RadComboBox radComboBox, ServerValidateEventArgs args, bool isBlank)
        {
            string selectedValue = radComboBox.SelectedValue;


            if (!isBlank)
            {
                if (selectedValue != null && selectedValue.Length > 0)
                {
                    try
                    {

                        args.IsValid = true;

                    }
                    catch (Exception ex)
                    {
                        args.IsValid = false;
                    }
                }
                else
                    args.IsValid = false;
            }
            else
                args.IsValid = true;

        }



    }



    public class ConnectionManager
    {
        // Connection class will create the new connection and open it for you, prevents injection attacks
        public static SqlConnection GetDatabaseConnection()
        {
            SqlConnection connection = new SqlConnection
            (Convert.ToString(ConfigurationManager.ConnectionStrings["MyDbConn"]));
            connection.Open();


            return connection;
        }

        public static SqlConnection GetLoginConnection()
        {
            SqlConnection connection = new SqlConnection
            (Convert.ToString(ConfigurationManager.ConnectionStrings["Login"]));
            connection.Open();


            return connection;
        }

    }

    public class Refererences
    {

        public static string GenerateRecordReference(string ModuleType, string corpCode)
        {
            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {



                    if (ModuleType != null)
                    {
                        switch (ModuleType)
                        {
                            case "RA":
                                SqlDataReader rdr = null;
                                SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr = cmd.ExecuteReader();

                                while (rdr.Read())
                                {

                                    return rdr["Reference"].ToString() + "RA";
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            case "HAZR":
                                SqlDataReader rdr_HAZ = null;
                                SqlCommand cmd_HAZ = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd_HAZ.CommandType = CommandType.StoredProcedure;
                                cmd_HAZ.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd_HAZ.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr_HAZ = cmd_HAZ.ExecuteReader();

                                while (rdr_HAZ.Read())
                                {

                                    return rdr_HAZ["Reference"].ToString() + "HAZ";
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            case "MH":
                                SqlDataReader rdr_MH = null;
                                SqlCommand cmd_MH = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd_MH.CommandType = CommandType.StoredProcedure;
                                cmd_MH.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd_MH.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr_MH = cmd_MH.ExecuteReader();

                                while (rdr_MH.Read())
                                {

                                    return rdr_MH["Reference"].ToString() + "MH";
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            case "DSE":
                                SqlDataReader rdr_DSE = null;
                                SqlCommand cmd_DSE = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd_DSE.CommandType = CommandType.StoredProcedure;
                                cmd_DSE.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd_DSE.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr_DSE = cmd_DSE.ExecuteReader();

                                while (rdr_DSE.Read())
                                {

                                    return rdr_DSE["Reference"].ToString() + "DSE";
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            case "INS":
                                SqlDataReader rdr_INS = null;
                                SqlCommand cmd_INS = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd_INS.CommandType = CommandType.StoredProcedure;
                                cmd_INS.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd_INS.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr_INS = cmd_INS.ExecuteReader();

                                while (rdr_INS.Read())
                                {

                                    return rdr_INS["Reference"].ToString() + "INS";
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            case "SA":
                                SqlDataReader rdr_SA = null;
                                SqlCommand cmd_SA = new SqlCommand("Module_GLOBAL.dbo.CreateRecordReference", dbconn);
                                cmd_SA.CommandType = CommandType.StoredProcedure;
                                cmd_SA.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd_SA.Parameters.Add(new SqlParameter("@ModuleType", ModuleType));
                                rdr_SA = cmd_SA.ExecuteReader();

                                while (rdr_SA.Read())
                                {

                                    return rdr_SA["Reference"].ToString();
                                }

                                // makes it this far then no record is coming back, error.
                                return "404";

                            default:
                                return "404";

                        }
                    }
                    else
                    {
                        return "404";
                    }
                }


            } catch (System.Exception e) {

                return e.ToString();

            }

        }
    } // End of References Class


    public class SharedComponants
    {

        public static string TaskTypeFullName(string TaskType)
        {

            if (TaskType == null)
            {
                return "err";
            }

            using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.usp_getModuleAltName", dbconn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@moduleCode", TaskType));
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        rdr.Read();

                        return rdr["moduleAltName"].ToString();
                    }
                    else
                    {
                        return TaskType.ToString();
                    }
                }
                catch
                {
                    return TaskType.ToString();
                }
            }


            //                try
            //            {
            //                switch (TaskType)
            //                {
            //                    case "ACC":
            //                        return "Accident / Incident Reporting";
            //                    case "ACC_INC":
            //                        return "Accident / Incident Reporting";
            //                    case "ACC_FI":
            //                        return "Fire Incident Reporting";
            //                    case "ASB":
            //                        return "Asbestos";
            //                    case "ASBA":
            //                        return "Asbestos";
            //                    case "RA":
            //                        return "Risk Assessment";
            //                    case "RA_V":
            //                        return "Risk Assessment";
            //                    case "RAR":
            //                        return "Risk Assessment Review";
            //                    case "CA":
            //                        return "COSHH Assessment";
            //                    case "CA2":
            //                        return "COSHH Assessment";
            //                    case "DSE":
            //                        return "DSE Assessment";
            //                    case "CA2R":
            //                        return "COSHH Assessment Review";
            //                    case "INS":
            //                        return "Safety Inspection Actions";
            //                    case "LB":
            //                        return "Log Book";
            //                    case "LB_A":
            //                        return "Log Book";
            //                    case "SU":
            //                        return "Supported Individual Risk Assessment";
            //                    case "SIRAR":
            //                        return "Supported Individual Risk Assessment Review";
            //                    case "RR":
            //                        return "Risk Register";
            //                    case "RRR":
            //                        return "Risk Register Review";
            //                    case "FS":
            //                        return "Fire Assessment";
            //                    case "FSO":
            //                        return "Fire Assessment Observation";
            //                    case "FSR":
            //                        return "Fire Assessment Review";
            //                    case "PTW":
            //                        return "Permit To Work";
            //                    case "PTWR":
            //                        return "Permit To Work Authorisation Request";
            //                    case "PR":
            //                        return "PUWER Assessment";
            //                    case "PRR":
            //                        return "PUWER Assessment Review";
            //                    case "SA":
            //                        return "Safety Audit";
            //                    case "SAO":
            //                        return "Safety Audit Observation";
            //                    case "APS":
            //                        return "Planned Safety Audits";
            //                    case "QA":
            //                        return "Quality Audit";
            //                    case "QAO":
            //                        return "Quality Audit Observation";
            //                    case "APQ":
            //                        return "Planned Quality Audits";
            //                    case "MH":
            //                        return "Manual Handling";
            //                    case "MHR":
            //                        return "Manual Handling Review";
            //                    case "MSR":
            //                        return "Method Statement Review";
            //                    case "HD":
            //                        return "Historic Document";
            //                    case "PER":
            //                        return "Private";
            //                    case "CL_A":
            //                        return "Checklist";
            //                    case "CL":
            //                        return "Checklist Completion";
            //                    case "DSEA":
            //                        return "DSE Action";
            //                    case "ASST":
            //                        return "Asset Management";
            //                    case "HAZR":
            //                        return "Hazard Reporting";
            //                    case "DSER":
            //                        return "DSE Review";
            //                    case "HPR":
            //                        return "Hazard Profiling Review";
            //                    default:
            //                        return TaskType.ToString() + "...";
            //                }
            //
            //            }
            //            catch
            //            {
            //                return "Error returning details";
            //            }


        }

        public static string RecordTypeIcon(string FileType, string ArcStatus, string PrivateAssessment = "False", string PortalRec = "N", string Module = "", string FileType2 = "")
        {
            try
            {

                if (PrivateAssessment == "True")
                {
                    return "<i class='fa fa-lock fa-2x' aria-hidden='true'></i>";
                }
                if (PortalRec == "Y")
                {
                    int FileTypeValue = Convert.ToInt32(FileType);
                    switch (FileTypeValue)
                    {
                        case 4:
                            return "<i class='fa fa-file-text fa-2x orange-colour' aria-hidden='true'></i>";
                        default:
                            return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                    }
                }
                else
                {
                    switch (FileType)
                    {
                        case "True":
                            FileType = "1";
                            break;
                        case "False":
                            FileType = "0";
                            break;
                    }

                    if (Module == "HAZR")
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-file-text fa-2x grey-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 2:
                                return "<i class='fa fa-file-text fa-2x orange-colour' aria-hidden='true'></i>";
                            case 3:
                                return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if (Module == "INS")
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 1:
                                if(ArcStatus == "1")
                                {
                                    return "<i class='fa fa-lock fa-2x' aria-hidden='true'></i>";
                                }
                                else
                                {
                                    return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                                }
                            default:
                                return "<i class='fa fa-file-text fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if (Module == "MS")
                    {
                        string FileTypeValue = FileType;
                        switch (FileTypeValue)
                        {
                            case "C":
                                return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                            case "I":
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                        }
                    }
                    else if (Module == "DSE")
                    {
                        if(FileType2 == "1")
                        {
                            int FileTypeValue = Convert.ToInt32(FileType);
                            switch (FileTypeValue)
                            {
                                case 0:
                                    return "<i class='fa fa-laptop fa-2x grey-colour' aria-hidden='true'></i>";
                                case 1:
                                    return "<i class='fa fa-laptop fa-2x green-colour' aria-hidden='true'></i>";
                                case 2:
                                    return "<i class='fa fa-laptop fa-2x red-colour' aria-hidden='true'></i>";
                                default:
                                    return "<i class='fa fa-laptop fa-2x' aria-hidden='true'></i>";
                            }
                        }
                        else
                        {
                            int FileTypeValue = Convert.ToInt32(FileType);
                            switch (FileTypeValue)
                            {
                                case 0:
                                    return "<i class='fa fa-desktop fa-2x grey-colour' aria-hidden='true'></i>";
                                case 1:
                                    return "<i class='fa fa-desktop fa-2x green-colour' aria-hidden='true'></i>";
                                case 2:
                                    return "<i class='fa fa-desktop fa-2x red-colour' aria-hidden='true'></i>";
                                default:
                                    return "<i class='fa fa-desktop fa-2x' aria-hidden='true'></i>";
                            }
                        }

                    }
                    else if (Module == "SELF")
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-file-text fa-2x grey-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                            case 2:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if (Module == "SELFTEMP")
                    {
                        string FileTypeValue = FileType;
                        switch (FileTypeValue)
                        {
                            case "D":
                                return "<i class='fa fa-file-text-o fa-2x grey-colour' aria-hidden='true'></i>";
                            case "A":
                                return "<i class='fa fa-file-text-o fa-2x green-colour' aria-hidden='true'></i>";
                            case "X":
                                return "<i class='fa fa-file-text-o fa-2x red-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text-o fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if (Module == "TEMP")
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-file-text-o fa-2x red-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-file-text-o fa-2x green-colour' aria-hidden='true'></i>";
                            case 3:
                                return "<i class='fa fa-file-text-o fa-2x red-colour' aria-hidden='true'></i>";
                            case 4:
                                return "<i class='fa fa-file-text-o fa-2x orange-colour' aria-hidden='true'></i>"; // temp removal of sign off required. replaced orange-color with green-color
                            case 5: //Ikea master template status PENDING APPROVAL
                                return "<i class='fa fa-file-text-o fa-2x orange-colour' aria-hidden='true'></i>";
                            case 6: //Ikea master template status PENDING AMENDMENTS
                                return "<i class='fa fa-file-text-o fa-2x red-colour' aria-hidden='true'></i>";
                            case 7: //Ikea master template status PENDING AMENDMENTS
                                return "<i class='fa fa-file-text-o fa-2x purple-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text-o fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if(Module == "ACC")
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-pencil-square-o fa-2x red-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                            //case 3:
                            //    return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 4:
                                return "<i class='fa fa-check fa-2x green-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else if(Module == "UserManager"){
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-user fa-2x gold-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-user-plus fa-2x silver-colour' aria-hidden='true'></i>";
                            case 2:
                                return "<i class='fa fa-user fa-2x blue-colour' aria-hidden='true'></i>";
                            case 3:
                                return "<i class='fa fa-user fa-2x blue-colour' aria-hidden='true'></i>";
                            case 4:
                                return "<i class='fa fa-user fa-2x blue-colour' aria-hidden='true'></i>";
                            case 6:
                                return "<i class='fa fa-users fa-2x blue-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-user fa-2x' aria-hidden='true'></i>";
                        }
                    }
                    else
                    {
                        int FileTypeValue = Convert.ToInt32(FileType);
                        switch (FileTypeValue)
                        {
                            case 0:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 1:
                                return "<i class='fa fa-file-text fa-2x green-colour' aria-hidden='true'></i>";
                            case 3:
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 4:
                                return "<i class='fa fa-file-text fa-2x orange-colour' aria-hidden='true'></i>"; // temp removal of sign off required. replaced orange-color with green-color
                            case 5: //Ikea master template status PENDING APPROVAL
                                return "<i class='fa fa-file-text fa-2x orange-colour' aria-hidden='true'></i>";
                            case 6: //Ikea master template status PENDING AMENDMENTS
                                return "<i class='fa fa-file-text fa-2x red-colour' aria-hidden='true'></i>";
                            case 7: //Ikea master template status PENDING AMENDMENTS
                                return "<i class='fa fa-file-text fa-2x purple-colour' aria-hidden='true'></i>";
                            default:
                                return "<i class='fa fa-file-text fa-2x' aria-hidden='true'></i>";
                        }
                    }




                }

            } catch
            {
                string error = TranslationPlugin.SharedComponantTranslate("ERROR");
                return error;
            }


        }



        public static string RiskLevelVisual(string Value, string corpCode)
        {

            string noScore = TranslationPlugin.SharedComponantTranslate("No Score");
            string _accLang = HttpContext.Current.Session["YOUR_LANGUAGE"].ToString();

            try
            {


                // No config data, lets grab it but just the once
                if (HttpContext.Current.Session["Matrix"] == null)
                {
                    // Bring in the config from the DB and set in memory until its cleared

                    using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                    {

                        string tmpMatrixText = "";
                        string tmpMatrixScores = "";

                            SqlDataReader rdr = null;
                            SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.GetScoreMatrixData", dbconn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                            cmd.Parameters.Add(new SqlParameter("@language", _accLang));
                            rdr = cmd.ExecuteReader();

                            while (rdr.Read())
                            {

                            tmpMatrixText = tmpMatrixText + rdr["value"] + ",";
                            tmpMatrixScores = tmpMatrixScores + rdr["icon_rating"] + ",";

                            }

                        //Code that is on live to assign a rating is there is no custom matrix being used

                        //if (var_db_col_risk_score = " ") or(var_db_col_risk_score = "") or(isnull(var_db_col_risk_score)) then
                        //  var_db_col_risk = "No Risk"
                        //    var_db_col_risk_visual = "<img src='img/rsklevel_v2_N.png' />"
                        //else
                        //    If var_db_col_risk_score > 17 Then
                        //        var_db_col_risk = "High"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_H.png' />"
                        //    Elseif var_db_col_risk_score > 14 Then
                        //        var_db_col_risk = "Med/High"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_MH.png' />"
                        //    Elseif var_db_col_risk_score > 5 Then
                        //        var_db_col_risk = "Medium"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_M.png' />"
                        //    Elseif var_db_col_risk_score > 4 Then
                        //        var_db_col_risk = "Low/Med"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_ML.png' />"
                        //    Elseif var_db_col_risk_score > 0 then
                        //        var_db_col_risk = "Low"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_L.png' />"
                        //    Else
                        //        var_db_col_risk = "No Risk"
                        //        var_db_col_risk_visual = "<img src='img/rsklevel_v2_N.png' />"
                        //    End if
                        //end if

                        // Remove training comma
                        tmpMatrixText = tmpMatrixText.Substring(0, tmpMatrixText.Length - 1);
                        tmpMatrixScores = tmpMatrixScores.Substring(0, tmpMatrixScores.Length - 1);

                        HttpContext.Current.Session["Matrix"] = tmpMatrixText;
                        HttpContext.Current.Session["MatrixPips"] = tmpMatrixScores;
                        HttpContext.Current.Session["MatrixClass"] = "MatrixDefault";
                    }
                }




                // Is there data about the risk level
                string RiskValue = Value.Trim();
                string RiskScoreLevels = HttpContext.Current.Session["Matrix"].ToString();
                string RiskScorePips = HttpContext.Current.Session["MatrixPips"].ToString();
                string RiskScoreStyle = HttpContext.Current.Session["MatrixClass"].ToString();
                string RiskValueVisual = "<div class='" + RiskScoreStyle + "'>" + RiskValue + "</br>";
                string RiskValueStyleData = "";



                if (RiskScoreLevels.Length > 0 && RiskScorePips.Length > 0 && RiskValue.Length > 0)
                {
                    // Spit into array, we have values
                    string[] RiskScoreLevelsArray = RiskScoreLevels.Split(',');
                    string[] RiskScorePipsArray = RiskScorePips.Split(',');


                    // Grab the values we need to do the logic
                    int RiskScorePipsArrayUBoundValue = Convert.ToInt32(RiskScorePipsArray.GetValue(RiskScoreLevelsArray.GetUpperBound(0)).ToString());
                    int RiskValuePip = Convert.ToInt32(RiskScorePipsArray.GetValue(Array.IndexOf(RiskScoreLevelsArray, RiskValue)));


                    // Grab position in array of the risk value sent so we can compare
                    int pos = Array.IndexOf(RiskScoreLevelsArray, RiskValue);

                    for (int i = 0; i < RiskScorePipsArrayUBoundValue; i++)
                    {

                        RiskValueStyleData = "";

                        if (RiskValuePip - 1 < i)
                        {
                            RiskValueVisual = RiskValueVisual + " " + "<i class='fa fa-square' aria-hidden='true'></i>";
                        } else {
                            RiskValueVisual = RiskValueVisual + " " + "<i class='fa fa-square ScorePip_" + (i + 1) + "' aria-hidden='true'></i>";
                        }

                    }

                    return RiskValueVisual;

                }
                else
                {
                    return noScore;
                }

            }  catch (System.Exception e)
            {
                throw;
                return e.Message.ToString(); //noScore;
            }
        }


        public static string returnTaskStatus(string taskStatus, string taskSignOff)
        {

            //translate in page as the return value sets the CSS as well

            if (taskStatus == "2")
            {
                return "Pending";
            }
            if (taskStatus == "1")
            {
                return "Active";
            }
            if (taskStatus == "0")
            {
                if (taskSignOff == "0")
                {
                    return "Requires Sign-Off";
                }
                else
                {
                    return "Complete";
                }

            }
            else
            {
                return "Processing";
            }
        }


        public static string getStructureElementName(string tierRef, int tierLevel, string corpCode)
        {

            string returnValue = TranslationPlugin.SharedComponantTranslate("No Specific Area Set");

            //if (ts != null)
            //{
            //    returnValue = ts.Translate(returnValue);
            //}

            try
            {
                using (SqlConnection dbconn = ConnectionManager.GetDatabaseConnection())
                {
                    if (tierRef != null)
                    {
                        SqlDataReader rdr = null;
                        SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.GetStructureName", dbconn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@tierLevel", tierLevel));
                        cmd.Parameters.Add(new SqlParameter("@tierRef", tierRef));
                        rdr = cmd.ExecuteReader();

                        while (rdr.Read())
                        {

                            if (tierLevel > 1)
                            {
                                return "<a href = '#' Class='list-group-item list-group-item-warning'><i class='fa fa-building-o' aria-hidden='true'></i> " + rdr["StructureName"].ToString() + "</a>";
                            } else
                            {
                                return "<a href = '#' Class='list-group-item list-group-item-warning'><i class='fa fa-building' aria-hidden='true'></i> " + rdr["StructureName"].ToString() + "</a>";
                            }



                        }

                        if (tierLevel > 1)
                        {
                            return "";
                        }
                        else
                        {
                            return returnValue;
                        }
                    }
                    else
                    {
                        return returnValue;
                    }
                }


            }
            catch
            {
                return "";
            }


        }

        public static string getStructureElementFromName(string tierName, int tierLevel)
        {

            string returnValue = TranslationPlugin.SharedComponantTranslate("No Specific Area Set");


            if (!string.IsNullOrEmpty(tierName))
            {
                return "<a href = '#' Class='list-group-item list-group-item-warning'><i class='fa fa-building-o' aria-hidden='true'></i> " + tierName + "</a>";

            }
            else
            {
                if (tierLevel == 2)
                {
                    return returnValue;
                }
                else
                {
                    return "";
                }
            }

           

        }


        //Notes. Will work for all modules.
        public static DataTable returnNoteList(string recordReference, string corpCode)
        {

            string recordReferenceExpanded = recordReference.Replace("/", " / ");

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    string sql_text = "SELECT Notes.notetext as col_note, Notes.notedatetime as col_notedate, Notes.notetype as col_notetype, CASE WHEN notes.NoteBy = 'SYSTEM' THEN 'SYSTEM' ELSE hrusers.per_fname + ' ' + hrusers.per_sname END as col_notename, notes.display_language, notes.id AS noteId " +
                                       " FROM  " + Convert.ToString(HttpContext.Current.Application["DBTABLE_GLOBAL_FPRINTNOTES"]) + " as Notes " +
                                        "LEFT JOIN " + Convert.ToString(HttpContext.Current.Application["DBTABLE_USER_DATA"]) + " as hrusers " +
                                        "ON notes.corp_code = hrusers.corp_code " +
                                        "AND notes.NoteBy = hrusers.acc_ref " +
                                        "WHERE (Notes.NoteRelationref = '" + recordReference + "' OR Notes.NoteRelationref = '" + recordReferenceExpanded + "') " +
                                        " AND Notes.corp_code='" + corpCode + "' " +
                                        "ORDER BY Notes.ID DESC";

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable returnDataTable = new DataTable();
                    returnDataTable.Load(objrs);

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static DataTable returnAttachedAssessments(string module, string recordReference, string corpCode)
        {

            DataTable returnDataTable = new DataTable();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand sqlComm = null;


                    switch (module)
                    {
                        case "RA":

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.sp_ra_attached_assessments", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_ra_ref", recordReference));

                            break;

                        case "MH":

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.sp_ra_attached_assessments", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_ra_ref", recordReference));

                            break;


                            //default:

                            //    sqlComm = new SqlCommand("Module_GLOBAL.dbo.usp_return_history_generic", dbConn);
                            //    sqlComm.CommandType = CommandType.StoredProcedure;
                            //    sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            //    sqlComm.Parameters.Add(new SqlParameter("@var_record_reference", recordReference));
                            //    sqlComm.Parameters.Add(new SqlParameter("@var_module", module));

                            //    break;
                    }

                    SqlDataReader objrs = sqlComm.ExecuteReader();
                    returnDataTable.Load(objrs);

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        //Assessment history (RA - SA - Generic). Will work for all modules.
        public static DataTable returnAssessmentHistory(string module, string recordReference, string corpCode, string recordReference2 = "")
        {

            DataTable returnDataTable = new DataTable();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand sqlComm = null;


                    switch (module)
                    {
                        case "RA":

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.sp_ra_history", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_ra_ref", recordReference));

                            break;

                        case "SA":

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.sp_sa_history", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_audit_ref", recordReference));

                            break;

                        case "ACC":

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.returnAccidentHistory", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_record_reference", recordReference));
                            sqlComm.Parameters.Add(new SqlParameter("@var_accb_ref", recordReference2));

                            break;


                        default:

                            sqlComm = new SqlCommand("Module_GLOBAL.dbo.usp_return_history_generic", dbConn);
                            sqlComm.CommandType = CommandType.StoredProcedure;
                            sqlComm.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                            sqlComm.Parameters.Add(new SqlParameter("@var_record_reference", recordReference));
                            sqlComm.Parameters.Add(new SqlParameter("@var_module", module));

                            break;
                    }

                    SqlDataReader objrs = sqlComm.ExecuteReader();
                    returnDataTable.Load(objrs);

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        //Save notes. WIll work for all modules.
        public static void saveNote(string noteText, string module, string recordReference, string corpCode, string accRef, string accLang = "en-gb")
        {

            string noteType = "0";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    string sql_text = "INSERT INTO " + Convert.ToString(HttpContext.Current.Application["DBTABLE_GLOBAL_FPRINTNOTES"]) + " (" +
                                     "corp_code," +
                                     "noterelationref," +
                                     "notemod," +
                                     "notedatetime," +
                                     "noteby," +
                                     "notetext," +
                                     "NoteType," +
                                     "display_language) " +
                                     "VALUES(" +
                                     "'" + Formatting.FormatTextInput(corpCode) + "'," +
                                     "'" + Formatting.FormatTextInput(recordReference) + "'," +
                                     "'" + Formatting.FormatTextInput(module) + "'," +
                                     "'" + DateTime.Now + "'," +
                                     "'" + accRef + "'," +
                                     "'" + Formatting.FormatTextInput(noteText) + "'," +
                                     "'" + noteType + "'," +
                                     "'" + accLang + "')";

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    sqlComm.ExecuteReader();

                    if (module == "SA")
                    {
                        recordReference += "SA";
                    }

                    insertHistory(corpCode, recordReference, accRef, "ADDED NOTE", module);

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }



        public static string getPreferenceValue(string corpCode, string accountReference, string preferenceName, string source)
        {

            string sql_string = "";
            string preferenceValue = "";
            string language = "";

            try
            {
                language = HttpContext.Current.Session["YOUR_LANGUAGE"].ToString();
            }
            catch
            {
                language = "en-gb";
            }


            //for permissions
            if (preferenceName == "USER_PERMS_ACTIVE")
            {
                try
                {
                    preferenceValue = HttpContext.Current.Session["USER_PERMS_ACTIVE"].ToString();
                }
                catch
                {
                    if (getPreferenceValue(corpCode, accountReference, "GLOBAL_USR_PERM", "pref") == "Y" && getPreferenceValue(corpCode, accountReference, "YOUR_ACCESS", "user") != "0")
                    {
                        HttpContext.Current.Session["USER_PERMS_ACTIVE"] = "1";
                        preferenceValue = HttpContext.Current.Session["USER_PERMS_ACTIVE"].ToString();

                        HttpContext.Current.Session["ALLOW_CREATE"] = true;
                        HttpContext.Current.Session["ALLOW_EDIT"] = true;

                        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {
                                SqlCommand cmd = null;

                                sql_string = "SELECT TOP 1 temp_id FROM TempPermDB.dbo.TEMP_" + corpCode + "_" + accountReference + " WHERE perm_create = 1";
                                cmd = new SqlCommand(sql_string, dbConn);
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader returnedCreateValue = cmd.ExecuteReader();
                                if (!returnedCreateValue.HasRows) { HttpContext.Current.Session["ALLOW_CREATE"] = false; }

                                sql_string = "SELECT TOP 1 temp_id FROM TempPermDB.dbo.TEMP_" + corpCode + "_" + accountReference + " WHERE perm_edit = 1";
                                cmd = new SqlCommand(sql_string, dbConn);
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader returnedEditValue = cmd.ExecuteReader();
                                if (!returnedEditValue.HasRows) { HttpContext.Current.Session["ALLOW_EDIT"] = false; }

                                cmd.Dispose();
                                returnedCreateValue.Close();
                                returnedEditValue.Close();
                            }
                            catch
                            {
                                throw;
                            }
                        }

                        HttpContext.Current.Session["ALLOW_DSE_VIEW"] = true;
                        HttpContext.Current.Session["ALLOW_DSE_REVIEW"] = true;
                        HttpContext.Current.Session["ALLOW_DSE_ADMIN"] = true;

                        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {
                                SqlCommand cmd = null;

                                sql_string = "SELECT TOP 1 temp_id FROM TempPermDB.dbo.TEMP_" + corpCode + "_" + accountReference + " WHERE dse_read = 1";
                                cmd = new SqlCommand(sql_string, dbConn);
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader returnedDSEReadValue = cmd.ExecuteReader();
                                if (!returnedDSEReadValue.HasRows) { HttpContext.Current.Session["ALLOW_DSE_VIEW"] = false; }

                                sql_string = "SELECT TOP 1 temp_id FROM TempPermDB.dbo.TEMP_" + corpCode + "_" + accountReference + " WHERE dse_review = 1";
                                cmd = new SqlCommand(sql_string, dbConn);
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader returnedDSEReviewValue = cmd.ExecuteReader();
                                if (!returnedDSEReviewValue.HasRows) { HttpContext.Current.Session["ALLOW_DSE_REVIEW"] = false; }

                                sql_string = "SELECT TOP 1 temp_id FROM TempPermDB.dbo.TEMP_" + corpCode + "_" + accountReference + " WHERE dse_admin = 1";
                                cmd = new SqlCommand(sql_string, dbConn);
                                cmd.CommandType = CommandType.Text;
                                SqlDataReader returnedDSEAdminValue = cmd.ExecuteReader();
                                if (!returnedDSEAdminValue.HasRows) { HttpContext.Current.Session["ALLOW_DSE_ADMIN"] = false; }

                                cmd.Dispose();
                                returnedDSEReadValue.Close();
                                returnedDSEReviewValue.Close();
                                returnedDSEAdminValue.Close();
                            }
                            catch
                            {
                                throw;
                            }
                        }
                    }
                    else
                    {
                        HttpContext.Current.Session["USER_PERMS_ACTIVE"] = "0";
                        HttpContext.Current.Session["ALLOW_CREATE"] = true;
                        HttpContext.Current.Session["ALLOW_EDIT"] = true;
                        HttpContext.Current.Session["ALLOW_DSE_VIEW"] = true;
                        HttpContext.Current.Session["ALLOW_DSE_REVIEW"] = true;
                        HttpContext.Current.Session["ALLOW_DSE_ADMIN"] = true;
                    }
                }
            }
            else
            {
                try
                {
                    preferenceValue = HttpContext.Current.Session[preferenceName].ToString();
                    if (preferenceValue == "pref_error")
                    {
                        throw new Exception();
                    }
                }
                catch
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            //string sql_string = "EXEC Module_HR.dbo.usp_getUserPreference '" + corpCode + "', '" + accountReference + "', '" + language + "', '" + preferenceName + "', '" + source + "'";

                            SqlCommand cmd = new SqlCommand("Module_HR.dbo.usp_getUserPreference", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode.Trim()));
                            cmd.Parameters.Add(new SqlParameter("@AccountRef", accountReference.Trim()));
                            cmd.Parameters.Add(new SqlParameter("@Language", language.Trim()));
                            cmd.Parameters.Add(new SqlParameter("@Preference", preferenceName.Trim()));
                            cmd.Parameters.Add(new SqlParameter("@Source", source.Trim()));

                            SqlDataReader returnedPreference = cmd.ExecuteReader();

                            if (returnedPreference.HasRows)
                            {
                                returnedPreference.Read();

                                //return the value
                                preferenceValue = returnedPreference["outputValue"].ToString();

                                //set the session value so we don't have to call the DB next time
                                HttpContext.Current.Session[preferenceName] = preferenceValue;
                            }
                            else
                            {
                                preferenceValue = ""; //sql_string;  // corpCode + "/" + accountReference + "/" + language + "/" + preferenceName + "/" + source + "--";
                            }

                            cmd.Dispose();
                            returnedPreference.Close();
                        }
                        catch (Exception ex)
                        {
                        //    throw;
                            preferenceValue = ex.ToString();
                        }
                    }
                }
            }

            return preferenceValue;

        }


        public static string GetPortalSetting(string corpCode, string sessionGUID, string portalSetting)
        {
            string returnValue = "";

            try
            {
                returnValue = HttpContext.Current.Session[portalSetting].ToString();
                if (returnValue == "pref_error")
                {
                    throw new Exception();
                }
            }
            catch
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT " + portalSetting + " FROM Module_PTL.dbo.portal_corp_settings AS settings " +
                                          "INNER JOIN Module_LOG.dbo.LOG_Session_Status AS sessions " +
                                          "  ON settings.corp_code = sessions.corp_code " +
                                          "  AND settings.id = sessions.portalReadOnly " +
                                          "WHERE settings.corp_code = '" + corpCode + "' AND sessions.sessionGUID = '" + sessionGUID + "'";
                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();
                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            returnValue = objrs[portalSetting].ToString();
                            HttpContext.Current.Session[returnValue] = returnValue;
                        }
                        sqlComm.Dispose();
                        dbConn.Close();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return returnValue;
        }


        public static DataTable returnRootCauseOptions(string corpCode)
        {
            DataTable returnDataTable = new DataTable();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand sqlComm = null;


                    sqlComm = new SqlCommand("Module_GLOBAL.dbo.usp_GenerateRootCauseItems", dbConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.Add(new SqlParameter("@corp_code", corpCode));

                    SqlDataReader objrs = sqlComm.ExecuteReader();
                    returnDataTable.Load(objrs);

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static string returnStructureSingleLine(string structName2, string structName3, string structName4,
            string structName5, string structName6, string character)
        {

            string combinedStructure = "";
            string returnValue = TranslationPlugin.SharedComponantTranslate("No Location Set");

            if (structName2 != "")
            {
                combinedStructure += structName2;

                if (structName3 != "")
                {
                    combinedStructure += " " + character + " " + structName3;

                    if (structName4 != "")
                    {
                        combinedStructure += " " + character + " " + structName4;

                        if (structName5 != "")
                        {
                            combinedStructure += " " + character + " " + structName5;

                            if (structName6 != "")
                            {
                                combinedStructure += " " + character + " " + structName6;
                            }

                        }

                    }
                }
            }
            else
            {
                combinedStructure = returnValue; // ts.Translate(returnValue);
            }

            return combinedStructure;

        }

        public static string createReviewInfo(string taskDueDate, string perFname, string perSname)
        {
            if (string.IsNullOrEmpty(taskDueDate))
            {
                string reviewstring = TranslationPlugin.SharedComponantTranslate("One off Assessment");
                return reviewstring;
            }
            else
            {
                string reviewstring = "<span class='" + ASNETNSP.Formatting.FormatReviewDateSpan(taskDueDate) + "'>" + ASNETNSP.Formatting.FormatDateTime(taskDueDate, 1) + "</span><br />" + ASNETNSP.Formatting.FormatFullNameDotSurname(perFname, perSname);
                return reviewstring;
            }
        }



        public static void insertHistory(string corpCode, string moduleRef, string accRef, string historyText, string module, string moduleRef2 = "")
        {
            if (module == "ACC")
            {

                string accb = "";
                string reference = "";

                if (moduleRef.Contains("/"))
                {
                    string[] accAry = moduleRef.Split('/');
                    accb = accAry[0];
                    reference = accAry[1];
                }
                else
                {
                    accb = moduleRef;
                    reference = moduleRef2;
                }




                try
                {
                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            string sql_textHistory = "INSERT INTO " + Convert.ToString(HttpContext.Current.Application["DBTABLE_GLOBAL_FPRINT"]) + " (HisRelationref,corp_code,HisBy,HisType,HisDateTime,HisMod,Tier1Ref,Tier2Ref) values('" + reference + "','" + corpCode + "','" + accRef + "','" + historyText + "',GETDATE(), '" + module + "', '" + accb + "', '" + reference + "')";
                            SqlCommand sqlComm = new SqlCommand(sql_textHistory, dbConn);
                            sqlComm.ExecuteReader();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            string sql_textHistory = "INSERT INTO " + Convert.ToString(HttpContext.Current.Application["DBTABLE_GLOBAL_FPRINT"]) + " (HisRelationref,corp_code,HisBy,HisType,HisDateTime,HisMod) values('" + moduleRef + "','" + corpCode + "','" + accRef + "','" + historyText + "',GETDATE(), '" + module + "')";
                            SqlCommand sqlComm = new SqlCommand(sql_textHistory, dbConn);
                            sqlComm.ExecuteReader();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        // DROP ZONE

        public static string getFilesForDropzone(string var_corp_code, string recordRef, string recordRef2, string returnType)
        {
            //returns a json string which is passed to jquery for de-serializing. This enables the onclick modal and download of displayed files.
            string jsArrayJSON;

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    string sql_text;

                    if (returnType != "PROC")
                    {
                        sql_text = "EXEC Module_GLOBAL.dbo.usp_file_get '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "', null";
                    }
                    else
                    {
                        sql_text = "EXEC Module_DOC.dbo.getFilesJSON '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "'";
                    }



                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(objrs);

                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
                    jsArrayJSON = json.ToString();

                    objrs.Close();
                    sqlComm.Dispose();

                }
            }
            catch
            {
                throw;
            }

            return jsArrayJSON;
        }

        public static string getFilesForDropzone(string var_corp_code, string recordRef, string recordRef2, string recordRef3, string returnType)
        {
            //returns a json string which is passed to jquery for de-serializing. This enables the onclick modal and download of displayed files.
            string jsArrayJSON;

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    string sql_text;

                    if (returnType != "PROC")
                    {
                        sql_text = "EXEC Module_GLOBAL.dbo.usp_file_get '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "', '" + recordRef3 + "'";
                    }
                    else
                    {
                        sql_text = "EXEC Module_DOC.dbo.getFilesJSON '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "'";
                    }



                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(objrs);

                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
                    jsArrayJSON = json.ToString();

                    objrs.Close();
                    sqlComm.Dispose();

                }
            }
            catch
            {
                throw;
            }

            return jsArrayJSON;
        }

        public static void getFilesForOutput(Repeater fileRepeater, string var_corp_code, string recordRef, string recordRef2, string recordRef3, string returnType)
        {


            //CG Edit from data source to use datatable, this way we can make any control work.
            try
            {
                using (DataTable filesTable = returnFileData(var_corp_code, recordRef, recordRef2, recordRef3, returnType))
                {
                    fileRepeater.DataSource = filesTable;
                    fileRepeater.DataBind();
                }
            }
            catch
            {
                throw;
            }

        }

        public static void getFilesForOutput(Repeater fileRepeater, string var_corp_code, string recordRef, string recordRef2, string returnType)
        {


            //CG Edit from data source to use datatable, this way we can make any control work.
            try
            {
                using (DataTable filesTable = returnFileData(var_corp_code, recordRef, recordRef2, returnType))
                {
                    fileRepeater.DataSource = filesTable;
                    fileRepeater.DataBind();
                }
            }
            catch
            {

            }

        }

        public static void getFilesForOutput(ListView fileRepeater, string var_corp_code, string recordRef, string recordRef2, string returnType)
        {


            try
            {
                using(DataTable filesTable = returnFileData(var_corp_code, recordRef, recordRef2, returnType))
                {
                    fileRepeater.DataSource = filesTable;
                    fileRepeater.DataBind();
                }
            }
            catch
            {

            }

        }

        public static DataTable returnFileData(string var_corp_code, string recordRef, string recordRef2, string returnType)
        {

            DataTable returnTable = new DataTable();

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    string sql_text = "EXEC Module_GLOBAL.dbo.usp_file_get '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "'";
                    SqlCommand cmd = new SqlCommand(sql_text, dbConn);
                    cmd.CommandType = CommandType.Text;

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    returnTable.Load(objrs);

                    cmd.Dispose();
                    sqlComm.Dispose();

                }
            }
            catch
            {
                throw;
            }

            return returnTable;

        }

        public static DataTable returnFileData(string var_corp_code, string recordRef, string recordRef2, string recordRef3, string returnType)
        {

            DataTable returnTable = new DataTable();

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    string sql_text = "EXEC Module_GLOBAL.dbo.usp_file_get '" + var_corp_code + "', '" + recordRef + "', '" + recordRef2 + "', '" + returnType + "', '" + recordRef3 + "'";
                    SqlCommand cmd = new SqlCommand(sql_text, dbConn);
                    cmd.CommandType = CommandType.Text;

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    returnTable.Load(objrs);

                    cmd.Dispose();
                    sqlComm.Dispose();

                }
            }
            catch
            {
                throw;
            }

            return returnTable;

        }

        public static int returnAttachmentCount(string corpCode, string recordRef, string recordRef2, string returnType)
        {

            int count;

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader reader = null;
                    SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.usp_file_count", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corpCode", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@reference", recordRef));
                    cmd.Parameters.Add(new SqlParameter("@reference2", recordRef2));
                    cmd.Parameters.Add(new SqlParameter("@returnType", returnType));
                    reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        reader.Read();

                        count = Convert.ToInt32(reader["attachCount"]);
                    }
                    else
                    {
                        count = 0;
                    }

                    reader.Close();
                    cmd.Dispose();

                }
                catch
                {
                    throw;
                }
            }

            return count;
        }

        public static string returnAttachmentItem(string filePath, string fileName, string friendlyName, string fileType, string size, string relatedFilesRoot)
        {

            //Work out the size of the Attachment.

            string itemClass = "";
            string iconType = "";
            string sizeFA = "";
            string condensedFriendlyName = "";



            StringBuilder currentItem = new StringBuilder();

            switch (size)
            {
                case "small":
                    itemClass = "imagePrint-small";
                    sizeFA = " fa-3x ";
                    condensedFriendlyName = Formatting.FormatTextbyLength(friendlyName, 10);
                    break;

                case "medium":
                    itemClass = "imagePrint";
                    sizeFA = " fa-4x ";
                    condensedFriendlyName = Formatting.FormatTextbyLength(friendlyName, 35);
                    break;

                default:
                    itemClass = "imagePrint";
                    sizeFA = " fa-4x ";
                    condensedFriendlyName = Formatting.FormatTextbyLength(friendlyName, 35);
                    break;
            }

            if(fileType == ".jpg" || fileType == ".gif" || fileType == ".jpeg" || fileType ==".png" || fileType == ".bmp" || fileType == ".ico")
            {
                // If its an image, show a gallery.
                currentItem.AppendLine("<img src='" + relatedFilesRoot + filePath + fileName + "' class='" + itemClass + " img-lightbox' style='cursor: pointer !important;' lightbox-name='" + friendlyName + "' /> ");
            }
            else
            {



                // If its not an image, just show something to be clicked to download.
                currentItem.AppendLine("<a href='" + relatedFilesRoot + filePath + fileName + "' target='_blank' style='color:black;' >");
                currentItem.AppendLine("<div class='" + itemClass + "' style='background-color: #fdfdfd; padding-top: 15%; text-decoration: none;'>");
                currentItem.AppendLine("<div class='row text-center'><i class='fa " + returnFontAwesomeIcon(fileType) + sizeFA + " green-colour'></i></div>");
                currentItem.AppendLine("<div class='row text-center' style='padding-top: 20%; font-size: 12px;'><div style='width:80%; margin: 0 auto;'>" + condensedFriendlyName + "</div></div>");
                currentItem.AppendLine("</div>");
                currentItem.AppendLine("</a>");

            }




            return currentItem.ToString();
        }

        public static string returnFontAwesomeIcon(string fileExtension)
        {
            string iconType = "";

            switch (fileExtension)
            {
                case ".xls": case ".xlsb": case ".xlsm":
                case "xlsx":
                    iconType = "  fa-file-excel-o ";
                    break;

                case ".pdf":
                    iconType = "  fa-file-pdf-o ";
                    break;

                case ".ppt": case ".pptm":
                    iconType = " fa-file-powerpoint-o ";
                    break;

                case ".doc": case ".docm": case ".docx": case ".dot": case ".dotm": case ".dotx":
                    iconType = " fa-file-word-o ";
                    break;

                case ".avi": case ".flv": case ".wmv": case ".mov": case ".mp4": case ".ogg":
                    iconType = " fa-file-movie-o ";
                    break;

                default:
                    iconType = " fa-file ";
                    break;
            }


            return iconType;
        }

        //Drop zone end

        // Methods to return the a light colour if a dark one is put in.
        public static string ToHexColor(Color color, bool alphaChannel)
        {
            return String.Format("#{0}{1}{2}{3}",
                alphaChannel ? color.A.ToString("X2") : String.Empty,
                color.R.ToString("X2"),
                color.G.ToString("X2"),
                color.B.ToString("X2"));
        }

        public static Color ContrastColor(Color color)
        {
            int d = 0;

            // Counting the perceptive luminance - human eye favors green color...
            double a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

            if (a < 0.5)
                d = 0; // bright colors - black font
            else
                d = 255; // dark colors - white font

            return Color.FromArgb(d, d, d);
        }


        public static string ReturnProgressDiv()
        {
            StringBuilder progressDiv = new StringBuilder();

            string title = TranslationPlugin.SharedComponantTranslate("Processing");
            string content = TranslationPlugin.SharedComponantTranslate("Please wait whilst your request is being handled") + "...";

            //if (ts != null)
            //{
            //    title = ts.Translate(title);
            //    content = ts.Translate(content) + "...";
            //}

            progressDiv.AppendLine("<div id='loading' class='updateMain' style='display: block !important;'>" +
                                        "<div class='updateContent'>" +
                                            "<div class='row'>" +
                                               " <div class='col-xs-12'>" +
                                                   " <div class='updateContentBox'>" +
                                                       " <div class='row'>" +
                                                           " <div class='updateSwirlyContainer' style='width: 90%;'>" +
                                                              "  <div style = 'width: 100%; text-align: left !important;'>" +
                                                                 "   <div style='display: block; float: left;'> " +
                                                                  "      <img src = '/core/media/images/core/loading.gif' width='40' />" +
                                                                   " </div>" +
                                                                   " <div class='h3-Processing-Block'>" +
                                                                   "     <h3 class='h3-Processing'>" + title + "</h3>" +
                                                                   " </div>" +
                                                                   " <div style = 'clear: both;' ></div>" +
                                                                "</ div >" +
                                                            "</div>" +
                                                           " <div class='text-center' style=''>" + content + "</div>" +
                                                       " </div>" +
                                                   " </div>" +
                                               " </div>" +
                                          "  </div>" +
                                      "  </div>" +
                                  " </div></div>");


            return progressDiv.ToString();
        }



        // ----------------------------------------------------
        //                  Assessment linkage
        // ----------------------------------------------------

        public static DataTable records_DataTable(string defaultText, string defaultValue, string searchedFor, string corpCode, string recRef, string module)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    SqlCommand sqlComm = new SqlCommand("Module_GLOBAL.dbo.GetRecordsList", dbConn);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.Add(new SqlParameter("@CORP_CODE", corpCode));
                    sqlComm.Parameters.Add(new SqlParameter("@REC_REF", recRef));
                    sqlComm.Parameters.Add(new SqlParameter("@MODULE", module));
                    sqlComm.Parameters.Add(new SqlParameter("@TEXT", searchedFor));
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    DataTable dt = new DataTable();
                    dt.Load(objrs);

                    objrs.Dispose();
                    sqlComm.Dispose();

                    return dt;


                }
                catch (Exception)
                {
                    throw;
                }
            }


        }


        //This drop down is bound to the event when you type a name.
        public static void returnRecordsDropDown(RadComboBox userDropDown, RadComboBoxItemsRequestedEventArgs e, string dropdownMode, string corpCode, string recRef, string module)
        {
            string doNotLink = TranslationPlugin.SharedComponantTranslate("No not link");
            string doNotLinkAny = TranslationPlugin.SharedComponantTranslate("Do not link to any record");

            userDropDown.Items.Clear();

            bool showDisabled = false;

            switch (dropdownMode)
            {
                case "any":
                    showDisabled = true;
                    break;

                case "none":
                    showDisabled = false;
                    break;
            }

            using (DataTable data = records_DataTable(doNotLink, "0", e.Text, corpCode, recRef, module))
            {
                try
                {



                    // If nothing has been searched for, allow them to select Any User
                    if (e.Text == "" && e.NumberOfItems == 0 && userDropDown.Items.Count == 0)
                    {

                        userDropDown.Items.Clear();
                        for (int i = 0; i < userDropDown.Items.Count; i++)
                        {
                            userDropDown.Items[i].DataBind();
                        }

                        switch (dropdownMode)
                        {
                            case "any":
                                userDropDown.Items.Add(new RadComboBoxItem(doNotLinkAny, "0"));
                                break;

                            case "none":

                                break;
                        }





                    }

                    //Maths which works out the position
                    const int ItemsPerRequest = 20;
                    int itemOffset = e.NumberOfItems;
                    int endOffset = itemOffset + ItemsPerRequest;

                    if (endOffset > data.Rows.Count)
                    {
                        endOffset = data.Rows.Count;
                    }

                    e.EndOfItems = endOffset == data.Rows.Count;


                    //userDropDown.DataValueField = "recordreference";
                    //userDropDown.DataTextField = "recordequipment";


                    // Add the data to the list and bind it (if you do not bind it goes invisible)
                    for (int i = itemOffset; i < endOffset; i++)
                    {
                       userDropDown.Items.Add(new RadComboBoxItem(
                       "(" + data.Rows[i]["recordreference"].ToString()+ ") " + data.Rows[i]["recordequipment"].ToString(), data.Rows[i]["recordreference"].ToString()));

                    }
                    for (int i = 0; i < userDropDown.Items.Count; i++)
                    {
                        userDropDown.Items[i].DataBind();
                    }


                }
                catch (Exception)
                {
                    throw;
                }
            }


        }


        public static void validate_RecordsDropDown(CustomValidator validator, RadComboBox radComboBox, ServerValidateEventArgs args, bool isBlank)
        {
            string selectedValue = radComboBox.SelectedValue;


            if (!isBlank)
            {
                if (selectedValue != null && selectedValue.Length > 0)
                {
                    try
                    {

                        args.IsValid = true;

                    }
                    catch (Exception ex)
                    {
                        args.IsValid = false;
                    }
                }
                else
                    args.IsValid = false;
            }
            else
                args.IsValid = true;

        }



        public static void lockAccOpts(string corpCode, string relatedTo, string optId)
        {
            try
            {
                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("Module_HR.dbo.lockAccOpts", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@related_to", relatedTo));
                        cmd.Parameters.Add(new SqlParameter("@id", optId));

                        cmd.ExecuteReader();
                        cmd.Dispose();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// return a string containing the path to the logo file
        /// </summary>
        /// <param name="corpCode">Clients corp code</param>
        /// <param name="accRef">Users acc ref - used if no assessment location is specified or available</param>
        /// <param name="numberOfBacksteps">How many ../ to place before version3.2/... </param>
        /// <param name="tier2Ref">Tier2 Ref if known</param>
        /// <param name="tier3Ref">Tier3 Ref if known</param>
        /// <param name="tier4Ref">Tier4 Ref if known</param>
        /// <param name="tier5Ref">Tier5 Ref if known</param>
        /// <param name="tier6Ref">Tier6 Ref if known</param>
        /// <returns></returns>
        public static string getLogoPath(string corpCode, string accRef, int numberOfBacksteps = 0, string tier2Ref = "", string tier3Ref = "", string tier4Ref = "", string tier5Ref = "", string tier6Ref = "")
        {
            string logoFile = "";
            string backsteps = "";

            string yourCompany = "";
            string yourLocation = "";
            string yourDepartment = "";

            string tierLogos = SharedComponants.getPreferenceValue(corpCode, accRef, "custom_corp_logo", "pref");

            if (tier2Ref == "")
            {
                yourCompany = SharedComponants.getPreferenceValue(corpCode, accRef, "YOUR_COMPANY", "user");
                yourLocation = SharedComponants.getPreferenceValue(corpCode, accRef, "YOUR_LOCATION", "user");
                yourDepartment = SharedComponants.getPreferenceValue(corpCode, accRef, "YOUR_DEPARTMENT", "user");
            }
            else
            {
                yourCompany = tier2Ref;
                yourLocation = tier3Ref != "" ? tier3Ref : "";
                yourDepartment = tier4Ref != "" ? tier4Ref : "";
            }

            if (numberOfBacksteps > 0)
            {
                for (int i = 1; i <= numberOfBacksteps; i++)
                {
                    backsteps += "../";
                }
            }

            if (tierLogos == "4" || tierLogos == "Y")
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_4_" + yourDepartment + ".jpg")))
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_4_" + yourDepartment + ".jpg";
                }
            }

            if (tierLogos == "3" || tierLogos == "Y")
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_3_" + yourLocation + ".jpg")))
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_3_" + yourLocation + ".jpg";
                }
            }

            if (tierLogos == "2" || tierLogos == "Y")
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_2_" + yourCompany + ".jpg")))
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + "_2_" + yourCompany + ".jpg";
                }
            }

            if (logoFile == "")
            {
                if (File.Exists(HttpContext.Current.Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg")))
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg";
                }
                else if (File.Exists(HttpContext.Current.Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".png")))
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".png";
                }
                else
                {
                    logoFile = backsteps + "version3.2/data/documents/pdf_centre/img/corp_logos/assessnet-logo.png";
                }
            }

            return logoFile;
        }

    } // End of SharedComponant Class

    public class UserPermissions
    {
        public string Module { get; set; }
        public string Access { get; set; }
        public string DisplayText { get; set; }
        public string DisplayValue { get; set; }

    }



    public class Permissions : System.Web.UI.Page
    {

        public static string checkPermission(string corp_code, string accRef, string module, string recordRef)
        {

            string returnValue = "none";

            if (module == "RA")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_RA.dbo.SP_RA_Report_permission", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@reference", recordRef));
                        cmd.Parameters.Add(new SqlParameter("@var_your_access", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_ACCESS", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_your_account_id", accRef));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bu", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_COMPANY", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bl", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_LOCATION", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bd", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_DEPARTMENT", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_use_perms", SharedComponants.getPreferenceValue(corp_code, accRef, "USER_PERMS_ACTIVE", "pref")));
                        cmd.Parameters.Add(new SqlParameter("@var_private_globaloveride", SharedComponants.getPreferenceValue(corp_code, accRef, "private_global_ovride", "pref")));
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            if (reader["remove"].ToString() == "True") { returnValue = "remove"; }
                            else if (reader["edit"].ToString() == "True") { returnValue = "edit"; }
                            else if (reader["view_assessment"].ToString() == "True") { returnValue = "view"; }
                            else { returnValue = "none"; }

                            //foreach (SqlParameter p in cmd.Parameters)
                            //{
                            //    returnValue += p.ParameterName.ToString() + "..." + p.Value.ToString() + "<br />";
                            //}


                            reader.Close();
                            cmd.Dispose();
                        }
                        else
                        {
                            returnValue = "none";
                        }

                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (module == "HAZR")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_HAZ.dbo.SP_HAZ_Report_permission", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@reference", recordRef));
                        cmd.Parameters.Add(new SqlParameter("@var_your_access", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_ACCESS", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_your_account_id", accRef));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bu", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_COMPANY", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bl", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_LOCATION", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bd", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_DEPARTMENT", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_use_perms", SharedComponants.getPreferenceValue(corp_code, accRef, "USER_PERMS_ACTIVE", "pref")));
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            if (reader["remove"].ToString() == "True") { returnValue = "remove"; }
                            else if (reader["edit"].ToString() == "True") { returnValue = "edit"; }
                            else if (reader["view_assessment"].ToString() == "True") { returnValue = "view"; }
                            else { returnValue = "none"; }

                            reader.Close();
                            cmd.Dispose();
                        }
                        else
                        {
                            returnValue = "none";
                        }

                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (module == "MH")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_MH.dbo.SP_MH_Report_permission", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@reference", recordRef));
                        cmd.Parameters.Add(new SqlParameter("@var_your_access", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_ACCESS", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_your_account_id", accRef));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bu", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_COMPANY", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bl", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_LOCATION", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bd", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_DEPARTMENT", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_use_perms", SharedComponants.getPreferenceValue(corp_code, accRef, "USER_PERMS_ACTIVE", "pref")));
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            if (Convert.ToBoolean(reader["remove"]) == true) { returnValue = "remove"; }
                            else if (Convert.ToBoolean(reader["edit"]) == true) { returnValue = "edit"; }
                            else if (Convert.ToBoolean(reader["view_assessment"]) == true) { returnValue = "view"; }
                            else { returnValue = "none"; }

                            reader.Close();
                            cmd.Dispose();
                        }
                        else
                        {
                            returnValue = "none";
                        }

                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (module == "INS")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_INS.dbo.SP_INS_Report_permission", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@reference", recordRef));
                        cmd.Parameters.Add(new SqlParameter("@var_your_access", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_ACCESS", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_your_account_id", accRef));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bu", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_COMPANY", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bl", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_LOCATION", "user")));
                        cmd.Parameters.Add(new SqlParameter("@my_rec_bd", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_DEPARTMENT", "user")));
                        cmd.Parameters.Add(new SqlParameter("@var_use_perms", SharedComponants.getPreferenceValue(corp_code, accRef, "USER_PERMS_ACTIVE", "pref")));
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            if (Convert.ToBoolean(reader["remove"]) == true) { returnValue = "remove"; }
                            else if (Convert.ToBoolean(reader["edit"]) == true) { returnValue = "edit"; }
                            else if (Convert.ToBoolean(reader["view_assessment"]) == true) { returnValue = "view"; }
                            else { returnValue = "none"; }

                            reader.Close();
                            cmd.Dispose();
                        }
                        else
                        {
                            returnValue = "none";
                        }

                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (module == "SELF")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_SELF.dbo.ReturnPermission", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@currentUser", accRef));
                        cmd.Parameters.Add(new SqlParameter("@currentAccessLevel", SharedComponants.getPreferenceValue(corp_code, accRef, "YOUR_ACCESS", "user")));
                        cmd.Parameters.Add(new SqlParameter("@requiredUser", recordRef));

                        //

                        reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            reader.Read();

                            if (reader["isSupervisor"].ToString() == "True") { returnValue = "supervisor"; }
                            else if (reader["isAuthorisedUser"].ToString() == "True") { returnValue = "authoriseduser"; }
                            else if (reader["editAccess"].ToString() == "True") { returnValue = "edit"; }
                            else if (reader["createAccess"].ToString() == "True") { returnValue = "create"; }
                            else if (reader["readAccess"].ToString() == "True") { returnValue = "read"; }
                            else { returnValue = "none"; }

                            reader.Close();
                            cmd.Dispose();
                        }
                        else
                        {
                            returnValue = "none";
                        }

                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else if (module == "SA")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        int sprocReturnValue = 0;

                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_SA.dbo.sp_return_user_permission_per_audit", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
                        cmd.Parameters.Add(new SqlParameter("@template_ref", ""));
                        cmd.Parameters.Add(new SqlParameter("@acc_ref", accRef));
                        cmd.Parameters.Add(new SqlParameter("@perm_active", SharedComponants.getPreferenceValue(corp_code, accRef, "USER_PERMS_ACTIVE", "pref")));
                        cmd.Parameters.Add(new SqlParameter("@audit_ref", recordRef));

                        SqlParameter outparam = cmd.Parameters.Add("@myResult", SqlDbType.Int);
                        outparam.Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        sprocReturnValue = Convert.ToInt32(cmd.Parameters["@myResult"].Value);

                        switch (sprocReturnValue)
                        {
                            case 0:
                                returnValue = "none";
                                break;

                            case 1:
                                returnValue = "view";
                                break;

                            case 2:
                                returnValue = "edit";
                                break;

                            default:
                                returnValue = "none";
                                break;
                        }


                    }
                    catch
                    {
                        throw;
                    }
                }
            }
            else
            {
                returnValue = "none";
            }

            return returnValue;

        }




        public static string generatePrivateRAPlaceholder(string var_corp_code, string recordRef)
        {
            bool privateFlag = false;
            string htmlElement = "";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    string sql_text = "SELECT private_flag FROM Module_RA.dbo.RISK_Data_entries " +
                                      "WHERE CORP_CODE = '" + var_corp_code + "' and recordreference = '" + recordRef + "'";
                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        if (objrs["private_flag"].ToString() == "True")
                        {
                            privateFlag = true;
                        }
                    }

                    objrs.Close();
                    sqlComm.Dispose();
                }
                catch
                {
                    throw;
                }
            }


            if (privateFlag == true)
            {
                string privateAssm = TranslationPlugin.SharedComponantTranslate("PRIVATE ASSESSMENT");
                string privateAssmContent = TranslationPlugin.SharedComponantTranslate("This assessment has been marked as private and is only accessible by the following users");

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("Module_RA.dbo.usp_ra_getPrivateAssessmentUsersFullNames", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                        cmd.Parameters.Add(new SqlParameter("@recordRef", recordRef));
                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            while (objrs.Read())
                            {
                                htmlElement += "<li>" + objrs["User"] + " (" + objrs["Permission_Granted"] + " Access)</li>";
                            }
                        }

                        objrs.Close();
                        cmd.Dispose();
                    }
                    catch
                    {
                        throw;
                    }
                }

                htmlElement = "<div class='row' id='oneOffAssessmentNotice' runat='server'>" +
                                     "<div class='col-xs-12'>" +
                                     "<div class='alert alert-success'>" +
                                     "<div class='row'>" +
                                     "<div class='col-xs-1'>" +
                                     "<i class='fa fa-lock' style='padding-left: 20px; font-size: 100px;'></i>" +
                                     "</div>" +
                                     "<div class='col-xs-11'>" +
                                     "<h3><strong>" + privateAssm + "</strong></h3>" + privateAssmContent + ":<ul>" + htmlElement + "</ul>" +
                                     "</div>" +
                                     "</div>" +
                                     "</div>" +
                                     "</div>" +
                                     "</div>";
            }

            return htmlElement;
        }

        public static string generatePrivatePlaceholder(string var_corp_code, string recordRef, string module)
        //General version to be used with other modules. Convert RA to this at later date
        {
            bool privateFlag = false;
            string htmlElement = "";

            string privateAssm = TranslationPlugin.SharedComponantTranslate("PRIVATE ASSESSMENT");
            string privateAssmContent = TranslationPlugin.SharedComponantTranslate("This assessment has been marked as private and is only accessible by the following users");

            if (module == "RA")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT private_flag FROM Module_RA.dbo.RISK_Data_entries " +
                                          "WHERE CORP_CODE = '" + var_corp_code + "' and recordreference = '" + recordRef + "'";
                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            if (objrs["private_flag"].ToString() == "True")
                            {
                                privateFlag = true;
                            }
                        }

                        objrs.Close();
                        sqlComm.Dispose();
                    }
                    catch
                    {
                        throw;
                    }
                }

                if (privateFlag == true)
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("Module_RA.dbo.usp_ra_getPrivateAssessmentUsersFullNames", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                            cmd.Parameters.Add(new SqlParameter("@recordRef", recordRef));
                            SqlDataReader objrs = cmd.ExecuteReader();

                            if (objrs.HasRows)
                            {
                                while (objrs.Read())
                                {
                                    htmlElement += "<li>" + objrs["User"] + " (" + objrs["Permission_Granted"] + " Access)</li>";
                                }
                            }

                            objrs.Close();
                            cmd.Dispose();
                        }
                        catch
                        {
                            throw;
                        }
                    }

                    htmlElement = "<div class='row' id='oneOffAssessmentNotice' runat='server'>" +
                                         "<div class='col-xs-12'>" +
                                         "<div class='alert alert-success'>" +
                                         "<div class='row'>" +
                                         "<div class='col-xs-1'>" +
                                         "<i class='fa fa-lock' style='padding-left: 20px; font-size: 100px;'></i>" +
                                         "</div>" +
                                         "<div class='col-xs-11'>" +
                                         "<h3><strong>" + privateAssm + "</strong></h3>" + privateAssmContent + ":<ul>" + htmlElement + "</ul>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>";
                }
            }

            if (module == "MH")
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT private_flag FROM Module_MH.dbo.MH_Data_entries " +
                                          "WHERE CORP_CODE = '" + var_corp_code + "' and recordreference = '" + recordRef + "'";
                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();

                            if (objrs["private_flag"].ToString() == "True")
                            {
                                privateFlag = true;
                            }
                        }

                        objrs.Close();
                        sqlComm.Dispose();
                    }
                    catch
                    {
                        throw;
                    }
                }

                if (privateFlag == true)
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            SqlCommand cmd = new SqlCommand("Module_MH.dbo.getPrivateAssessmentUsersFullNames", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                            cmd.Parameters.Add(new SqlParameter("@recordRef", recordRef));
                            SqlDataReader objrs = cmd.ExecuteReader();

                            if (objrs.HasRows)
                            {
                                while (objrs.Read())
                                {
                                    htmlElement += "<li>" + objrs["User"] + " (" + objrs["Permission_Granted"] + " Access)</li>";
                                }
                            }

                            objrs.Close();
                            cmd.Dispose();
                        }
                        catch
                        {
                            throw;
                        }
                    }

                    htmlElement = "<div class='row' id='oneOffAssessmentNotice' runat='server'>" +
                                         "<div class='col-xs-12'>" +
                                         "<div class='alert alert-success'>" +
                                         "<div class='row'>" +
                                         "<div class='col-xs-1'>" +
                                         "<i class='fa fa-lock' style='padding-left: 20px; font-size: 100px;'></i>" +
                                         "</div>" +
                                         "<div class='col-xs-11'>" +
                                         "<h3><strong>" + privateAssm + "</strong></h3>" + privateAssmContent + ":<ul>" + htmlElement + "</ul>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>" +
                                         "</div>";
                }
            }


            return htmlElement;
        }


        public static DataTable GetPrivateUsersForEntry(string var_corp_code, string module, string recordRef)
        {
            DataTable assessmentPermissions = new DataTable();

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    string sql_text = "SELECT users.acc_ref AS UserID, UPPER(users.per_sname) + ', ' + users.per_fname AS [User], CASE WHEN private.remove = 1 THEN 'Remove' WHEN private.edit = 1 THEN 'Edit' WHEN private.view_assessment = 1 THEN 'View' END AS [Permission_Granted] " +
                                      "FROM Module_GLOBAL.dbo.GLOBAL_Private AS private " +
                                      "INNER JOIN Module_HR.dbo.HR_Data_users AS users " +
                                      "  ON private.corp_code = users.corp_code " +
                                      "  AND private.restriction = users.acc_ref " +
                                      "WHERE private.corp_code = '" + var_corp_code + "' " +
                                      "  AND private.module = '" + module + "' " +
                                      "  AND private.reference = '" + recordRef + "' " +
                                      "  AND private.permission_type = 2 " +
                                      "  AND private.active = 1 ";
                    SqlDataAdapter adapter = new SqlDataAdapter("Module_GLOBAL.dbo.usp_getPrivateAssessmentUsersForEntry '" + var_corp_code + "', '" + module + "', '" + recordRef + "'", dbConn);

                    adapter.Fill(assessmentPermissions);

                    adapter.Dispose();

                    // Add some default rows to the datatable
                    DataRow dr = assessmentPermissions.NewRow();
                    dr["UserID"] = "0";
                    dr["User"] = "Assessor";
                    dr["Permission_Granted"] = "Remove";
                    assessmentPermissions.Rows.Add(dr);

                    DataRow dr1 = assessmentPermissions.NewRow();
                    dr1["UserID"] = "0";
                    dr1["User"] = "Reviewer";
                    dr1["Permission_Granted"] = "Remove";
                    assessmentPermissions.Rows.Add(dr1);

                }
            }
            catch (Exception)
            {
                throw;
            }

            return assessmentPermissions;
        }



        public static bool moduleAccessAllowed(string corp_code, string accRef, string module, string checkType)
        {
            //checktype - (licence: check acc_licence and licence table)  (permission: check acc_licence only)

            bool permission_granted = false;

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_HR.dbo.GetUserPermissions_SpecificModule", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corp_code));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", accRef));
                    cmd.Parameters.Add(new SqlParameter("@module", module));
                    cmd.Parameters.Add(new SqlParameter("@check_type", checkType));
                    objrs = cmd.ExecuteReader();


                    if (objrs.HasRows)
                    {
                        objrs.Read();
                        permission_granted = (bool)objrs["permission_granted"];
                    }

                    objrs.Close();
                    cmd.Dispose();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return permission_granted;
        }


        public static string returnModuleVersion(string moduleCode, string corpCode)
        {

            string returnValue = "";

            try
            {
                returnValue = HttpContext.Current.Session["MODULE_" + moduleCode.ToUpper()].ToString();
            }
            catch
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlCommand cmd = new SqlCommand("module_hr.dbo.GetLicenceInformation", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@CORP_CODE", corpCode));
                        cmd.Parameters.Add(new SqlParameter("@module_code", moduleCode));

                        SqlDataReader objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            returnValue = objrs["corp_licence_version"].ToString();
                            HttpContext.Current.Session["MODULE_" + moduleCode.ToUpper()] = returnValue;
                        }
                        else
                        {
                            returnValue = "ERR";
                        }

                        objrs.Dispose();
                        cmd.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return returnValue;
        }
    }


    public class SessionState : System.Web.UI.Page
    {

        public static void checkSession(ClientScriptManager cs)
        {
            if (isSessionAlive() == false)
            {
                HttpContext.Current.Session["CORP_CODE"] = "AAAAAA";
                HttpContext.Current.Session["YOUR_ACCOUNT_ID"] = "AAA";
                HttpContext.Current.Session["sessionGUID"] = "AAA";
                HttpContext.Current.Session["PTL_READ_ONLY"] = 9999;
                HttpContext.Current.Session["YOUR_LANGUAGE"] = "en";

                //HttpContext.Current.Response.Redirect("/core/system/sessions/ajaxSessions.ashx", true);

                cs.RegisterClientScriptBlock(HttpContext.Current.GetType(), "RedirectScript", "window.parent.location = '" + getRedirectPage() + "'", true);
            }

        }

        public static string getRedirectPage()
        {
            //return HttpContext.Current.Application["CONFIG_PROVIDER"].ToString() + "/version3.2/security/login/frm_lg_session.asp";
            return HttpContext.Current.Application["CONFIG_PROVIDER"].ToString() + "/core/system/general/session.aspx";
        }

        public static bool isSessionAlive()
        {

            string userSessionGUID = null;
            bool sessionState = false;

            //check for the sync cookie value
            if (HttpContext.Current.Request.Cookies.Get("sync") != null)
            {
                userSessionGUID = HttpContext.Current.Request.Cookies.Get("sync").Value.ToString();
            }


            //if value found, check it against the DB, or force a time out
            if (userSessionGUID != null)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        SqlDataReader reader = null;
                        SqlCommand cmd = new SqlCommand("Module_LOG.dbo.checkSessionStatus", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@sessionGUID", userSessionGUID));
                        reader = cmd.ExecuteReader();

                        if (reader.HasRows == true)
                        {
                            reader.Read();

                            if (reader["status"].ToString() == "True")
                            {
                                //rebuild the basic session elements - session didn't exist
                                HttpContext.Current.Session["sessionGUID"] = userSessionGUID;
                                HttpContext.Current.Session["CORP_CODE"] = reader["corp_code"].ToString();
                                HttpContext.Current.Session["YOUR_ACCOUNT_ID"] = reader["acc_ref"].ToString();
                                HttpContext.Current.Session["PTL_READ_ONLY"] = (int)reader["ptl_read_only"];
                                HttpContext.Current.Session.Timeout = (int)reader["sessionTimeout"];
                                HttpContext.Current.Session["YOUR_LANGUAGE"] = reader["display_language"].ToString();
                                sessionState = true;
                            }
                            else
                            {
                                sessionState = false;
                            }
                        }
                        else
                        {
                            sessionState = false;
                        }
                        cmd.Dispose();
                        reader.Close();

                    }
                    catch
                    {
                        sessionState = false;
                    }
                }
            }
            else
            {
                sessionState = false;
            }

            return sessionState;
        }

        public static void resetSessions(string contains)
        {
            if (HttpContext.Current.Session != null)
            {

                for (int i = 0; i < HttpContext.Current.Session.Count; i++)
                {
                    string sessionName = "";

                    sessionName = Convert.ToString(HttpContext.Current.Session.Keys[i]);

                    if (sessionName.Contains(contains))
                    {
                        HttpContext.Current.Session.Remove(sessionName);
                    }
                }
            }
        }

        public static void clearAllSessions()
        {
            if (HttpContext.Current.Session != null)
            {

                for (int i = 0; i < HttpContext.Current.Session.Count; i++)
                {
                    string sessionName = "";
                    sessionName = Convert.ToString(HttpContext.Current.Session.Keys[i]);
                    HttpContext.Current.Session.Remove(sessionName);
                }

                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
            }
        }

    }


    public class Portal : System.Web.UI.Page
    {

        public static void getPortalSetting(string corpCode, int rowID, string module)
        {
            if(module == "RA")
            {
                try
                {

                    if (HttpContext.Current.Session["RA_ALLOW_FULL"] == null ||
                        HttpContext.Current.Session["RA_PRINT_SEARCH"] == null ||
                        HttpContext.Current.Session["RA_SEARCH_REPORT"] == null)
                    {

                        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                        {

                            try
                            {
                                string sql_text = "SELECT * FROM Module_PTL.dbo.portal_corp_settings WHERE corp_code = '" + corpCode + "' and id = '" + rowID + "' ";

                                SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                                SqlDataReader objrs = sqlComm.ExecuteReader();

                                if (objrs.HasRows)
                                {
                                    objrs.Read();

                                    HttpContext.Current.Session["RA_ALLOW_FULL"] = objrs["ra_allow_full"].ToString();
                                    HttpContext.Current.Session["RA_PRINT_SEARCH"] = objrs["ra_print_search"].ToString();
                                    HttpContext.Current.Session["RA_SEARCH_REPORT"] = objrs["ra_print_report"].ToString();

                                }
                                else
                                {
                                    HttpContext.Current.Session["RA_ALLOW_FULL"] = false;
                                    HttpContext.Current.Session["RA_PRINT_SEARCH"] = false;
                                    HttpContext.Current.Session["RA_SEARCH_REPORT"] = false;
                                }

                                sqlComm.Dispose();
                                objrs.Close();

                            }
                            catch (Exception)
                            {
                                throw;
                            }

                        }
                    }
                    else
                    {
                        //do nothing
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (module == "MH")
            {
                try
                {

                    if (HttpContext.Current.Session["MH_PRINT_SEARCH"] == null ||
                        HttpContext.Current.Session["MH_SEARCH_REPORT"] == null)
                    {

                        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                        {

                            try
                            {
                                string sql_text = "SELECT * FROM Module_PTL.dbo.portal_corp_settings WHERE corp_code = '" + corpCode + "' and id = '" + rowID + "' ";

                                SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                                SqlDataReader objrs = sqlComm.ExecuteReader();

                                if (objrs.HasRows)
                                {
                                    objrs.Read();

                                    HttpContext.Current.Session["MH_PRINT_SEARCH"] = objrs["mh_print_search"].ToString();
                                    HttpContext.Current.Session["MH_SEARCH_REPORT"] = objrs["mh_print_report"].ToString();

                                }
                                else
                                {
                                    HttpContext.Current.Session["MH_PRINT_SEARCH"] = false;
                                    HttpContext.Current.Session["MH_SEARCH_REPORT"] = false;
                                }

                                sqlComm.Dispose();
                                objrs.Close();

                            }
                            catch (Exception)
                            {
                                throw;
                            }

                        }
                    }
                    else
                    {
                        //do nothing
                    }
                }
                catch (Exception ex)
                {

                }
            }
            if (module == "INC")
            {
                if (HttpContext.Current.Session["ACC_PORTAL_SETTINGS_GRABBED"] == null)
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {

                        try
                        {
                            string sql_text = "SELECT * FROM Module_PTL.dbo.portal_corp_settings WHERE corp_code = '" + corpCode + "' and id = '" + rowID + "' ";

                            SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                            SqlDataReader objrs = sqlComm.ExecuteReader();

                            if (objrs.HasRows)
                            {
                                objrs.Read();

                                HttpContext.Current.Session["CORP_STRING"] = objrs["access_string"].ToString();
                                HttpContext.Current.Session["CORP_RANDOM_STRING"] = objrs["random_string"].ToString();
                                HttpContext.Current.Session["CORP_ROW_ID"] = objrs["id"].ToString();

                                HttpContext.Current.Session["ACC_ALLOW_INJ"] = objrs["acc_allow_inj"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_ID"] = objrs["acc_allow_id"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_DO"] = objrs["acc_allow_do"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_PD"] = objrs["acc_allow_pd"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_NM"] = objrs["acc_allow_nm"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_WIT"] = objrs["acc_allow_wit"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_DOC"] = objrs["acc_allow_doc"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_INV"] = objrs["acc_allow_inv"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_DI"] = objrs["acc_allow_di"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_VI"] = objrs["acc_allow_vi"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_ENV"] = objrs["acc_allow_env"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_MULTIPLES"] = objrs["acc_allow_multiples"].ToString();
                                HttpContext.Current.Session["ACC_ALLOW_TEMPLATES"] = objrs["acc_allow_templates"].ToString();
                                HttpContext.Current.Session["ACC_DISPLAY_OVERVIEW"] = objrs["acc_display_overview"].ToString();
                                HttpContext.Current.Session["ACC_ASK_ADDITIONAL"] = objrs["acc_ask_additional"].ToString();
                                HttpContext.Current.Session["ACC_NOTIFICATION_TYPE"] = objrs["acc_notification_type"].ToString();
                                HttpContext.Current.Session["ACC_NOTIFICATION_ATTACHMENT"] = objrs["acc_notification_attachment"].ToString();
                                HttpContext.Current.Session["ACC_RESTRICT_WIT_DOC"] = objrs["acc_restrict_wit_doc"].ToString();
                                HttpContext.Current.Session["ACC_FINSIH_BUTTONS"] = objrs["acc_finish_buttons"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL"] = objrs["acc_send_email"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_ATTACHMENT"] = objrs["acc_send_email_attachment"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_REPORTER"] = objrs["acc_send_email_reporter"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_REPORTER_ATTACHMENT"] = objrs["acc_send_email_reporter_attachment"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_INVOLVED"] = objrs["acc_send_email_involved"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_INVOLVED_ATTACHMENT"] = objrs["acc_send_email_involved_attachment"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC"] = objrs["acc_send_email_generic"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC_ATTACHMENT"] = objrs["acc_send_email_generic_attachment"].ToString();
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC_ADDRESS"] = objrs["acc_send_email_generic_address"].ToString();
                                HttpContext.Current.Session["ACC_SHOW_LTA"] = objrs["acc_SHOW_LTA"].ToString();
                                HttpContext.Current.Session["ACC_SHOW_RIDDOR"] = objrs["acc_show_riddor"].ToString();
                                HttpContext.Current.Session["ACC_SHOW_DPA_STATEMENT"] = objrs["acc_show_dpa_statement"].ToString();
                                HttpContext.Current.Session["ACC_CONSENT_STATEMENT"] = objrs["acc_consent_statement"].ToString();
                                HttpContext.Current.Session["ACC_ASK_INC_CLASS_QUESTION"] = objrs["acc_ask_inc_class_question"].ToString();
                                HttpContext.Current.Session["ACC_INV_NOTIFICATION"] = objrs["acc_inv_notification"].ToString();
                                HttpContext.Current.Session["ACC_INV_FULL"] = objrs["acc_inv_full"].ToString();

                                HttpContext.Current.Session["ACC_PORTAL_SETTINGS_GRABBED"] = true;
                            }
                            else
                            {
                                HttpContext.Current.Session["ACC_ALLOW_INJ"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_ID"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_DO"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_PD"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_NM"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_WIT"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_DOC"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_INV"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_DI"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_VI"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_ENV"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_MULTIPLES"] = "N";
                                HttpContext.Current.Session["ACC_ALLOW_TEMPLATES"] = "N";
                                HttpContext.Current.Session["ACC_DISPLAY_OVERVIEW"] = "N";
                                HttpContext.Current.Session["ACC_ASK_ADDITIONAL"] = "N";
                                HttpContext.Current.Session["ACC_NOTIFICATION_TYPE"] = "N";
                                HttpContext.Current.Session["ACC_NOTIFICATION_ATTACHMENT"] = "N";
                                HttpContext.Current.Session["ACC_RESTRICT_WIT_DOC"] = "N";
                                HttpContext.Current.Session["ACC_FINSIH_BUTTONS"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_ATTACHMENT"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_REPORTER"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_REPORTER_ATTACHMENT"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_INVOLVED"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_INVOLVED_ATTACHMENT"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC_ATTACHMENT"] = "N";
                                HttpContext.Current.Session["ACC_SEND_EMAIL_GENERIC_ADDRESS"] = "N";
                                HttpContext.Current.Session["ACC_SHOW_LTA"] = "N";
                                HttpContext.Current.Session["ACC_SHOW_RIDDOR"] = "N";
                                HttpContext.Current.Session["ACC_SHOW_DPA_STATEMENT"] = "N";
                                HttpContext.Current.Session["ACC_CONSENT_STATEMENT"] = "N";
                                HttpContext.Current.Session["ACC_ASK_INC_CLASS_QUESTION"] = "N";
                                HttpContext.Current.Session["ACC_INV_NOTIFICATION"] = "N";
                                HttpContext.Current.Session["ACC_INV_FULL"] = "N";

                                HttpContext.Current.Session["ACC_PORTAL_SETTINGS_GRABBED"] = false;
                            }

                            sqlComm.Dispose();
                            objrs.Close();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
            }

        }

    }


    public class RiskMatrix
    {

        private string _corpCode { get; set; }

        private string _likelihood_title;
        private string _severity_title;

        private string _accLang;

        private List<RiskMatrix_Item> RiskMatrixOverall_List = new List<RiskMatrix_Item> { };
        private List<RiskMatrix_Severity_Item> Severity_List = new List<RiskMatrix_Severity_Item> { };
        private List<RiskMatrix_Likelihood_Item> Likelihood_List = new List<RiskMatrix_Likelihood_Item> { };





        public RiskMatrix(string ccode, string language = "en-gb")
        {
            _corpCode = ccode;
            _likelihood_title = SharedComponants.getPreferenceValue(_corpCode, "", "RISK_MATRIX_LIKE", "pref"); ;
            _severity_title = SharedComponants.getPreferenceValue(_corpCode, "", "RISK_MATRIX_SEV", "pref"); ;

            _accLang = language;  // HttpContext.Current.Session["YOUR_LANGUAGE"].ToString();
            if (_accLang == null)
            {
                _accLang = "en-gb";
            }

            if (_likelihood_title == "")
            {
                _likelihood_title = "Likelihood";
            }

            if (_severity_title == "")
            {
                _severity_title = "Severity";
            }

            GenerateRiskMatrix();
        }

        public RiskMatrix()
        {
            throw new NotSupportedException("Unsupported intilisation of class. Please initilise with new RiskMatrix(corpcode).");
        }


        private void GenerateRiskMatrix()
        {
            // Populate a list of all the current risk matrix options.
            initiateOverall();
            initiateSeverity();
            initiateLikelihood();
        }

        private void initiateOverall()
        {
            try
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        string sql_text = "SELECT overall.severity_value, overall.likelihood_value, overall.[description], overall.graph_colour, overall.font_colour, overall.likelihood_value*overall.severity_value AS grid_score " +
                                     "FROM " + HttpContext.Current.Application["GLOBAL_RISK_MATRIX_overall"] + " AS overall " +
                                     "WHERE overall.corp_code = '" + _corpCode + "' " +
                                     "  AND overall.display_language = '" + _accLang + "' " +
                                     "ORDER BY overall.likelihood_value, overall.severity_value";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();


                        if (objrs.HasRows)
                        {

                            while (objrs.Read())
                            {

                                string severityValue;
                                string likelihoodValue;
                                string gridScore;
                                string key;

                                string desription;
                                string graphColour;
                                string fontColour;

                                severityValue = objrs["severity_value"] == null ? "err" : objrs["severity_value"].ToString();
                                likelihoodValue = objrs["likelihood_value"] == null ? "err" : objrs["likelihood_value"].ToString();
                                gridScore = objrs["grid_score"] == null ? "err" : objrs["grid_score"].ToString();

                                key = severityValue + "_" + likelihoodValue + "_" + gridScore;

                                desription = objrs["description"] == null ? "" : objrs["description"].ToString();
                                graphColour = objrs["graph_colour"] == null ? "" : objrs["graph_colour"].ToString();
                                fontColour = objrs["font_colour"] == null ? "" : objrs["font_colour"].ToString();


                                RiskMatrix_Item currentItem = new RiskMatrix_Item(key, desription, graphColour, fontColour);
                                RiskMatrixOverall_List.Add(currentItem);

                            }

                            sqlComm.Dispose();
                            objrs.Dispose();

                        }
                        else
                        {

                        }

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }
            catch
            {
                throw;
            }
        }

        private void initiateSeverity()
        {
            try
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT title, score, [description] " +
                                         "FROM " + HttpContext.Current.Application["GLOBAL_RISK_MATRIX_severity"] + " " +
                                         "WHERE corp_code = '" + _corpCode + "' AND score != 0 AND display_language = '" + _accLang + "' " +
                                         "ORDER BY score";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();


                        if (objrs.HasRows)
                        {

                            while (objrs.Read())
                            {

                                string title;
                                string score;
                                string description;

                                title = objrs["title"] == null ? "err" : objrs["title"].ToString();
                                score = objrs["score"] == null ? "err" : objrs["score"].ToString();
                                description = objrs["description"] == null ? "err" : objrs["description"].ToString();



                                RiskMatrix_Severity_Item currentItem = new RiskMatrix_Severity_Item(title, score, description);
                                Severity_List.Add(currentItem);

                            }

                        }
                        else
                        {

                        }

                        sqlComm.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }
            catch
            {
                throw;
            }
        }

        private void initiateLikelihood()
        {
            try
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT title, score, [description] " +
                                         "FROM " + HttpContext.Current.Application["GLOBAL_RISK_MATRIX_likelihood"] + " " +
                                         "WHERE corp_code = '" + _corpCode + "' AND score != 0 AND display_language = '" + _accLang + "' " +
                                         "ORDER BY score";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();


                        if (objrs.HasRows)
                        {

                            while (objrs.Read())
                            {

                                string title;
                                string score;
                                string description;

                                title = objrs["title"] == null ? "err" : objrs["title"].ToString();
                                score = objrs["score"] == null ? "err" : objrs["score"].ToString();
                                description = objrs["description"] == null ? "err" : objrs["description"].ToString();



                                RiskMatrix_Likelihood_Item currentItem = new RiskMatrix_Likelihood_Item(title, score, description);
                                Likelihood_List.Add(currentItem);

                            }

                        }
                        else
                        {

                        }

                        sqlComm.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }

            }
            catch
            {
                throw;
            }
        }

        public string ReturnRiskMatrix(string additionalStyling)
        {

            // This function returns the risk matrix for the client in a visual format.
            // CG - 28/03/2018.
            // MG created original Classic ASP on scripts/inc/autogenmenus.

            StringBuilder fullMatrix = new StringBuilder();
            StringBuilder currentRow1 = new StringBuilder();
            StringBuilder currentRow2 = new StringBuilder();
            StringBuilder currentRow3 = new StringBuilder();
            StringBuilder currentTableRow = new StringBuilder();
            int counter = 0;
            int cellScore = 0;



            if (Severity_List.Count > 0)
            {

                if (additionalStyling == "MAX")
                {
                    fullMatrix.AppendLine("<div style='display: inline-block;'><div style='margin-left:-50px;'>");
                }
                else
                {
                    fullMatrix.AppendLine("<div><div style=' " + additionalStyling + " '>");
                }


                fullMatrix.AppendLine("<table class='matrix_table'>");
                fullMatrix.AppendLine("<tr><td colspan='3'>&nbsp;</td><td colspan='" + Severity_List.Count + "' class='matrix_header'><strong>" + _severity_title + "</strong></td></tr>");

                foreach (RiskMatrix_Severity_Item item in Severity_List)
                {
                    currentRow1.Append("<td class='c' title='" + item.description + "'><!--" + item.score + "--></td>");
                    currentRow2.Append("<td style='width:70px;' class='c' title='" + item.description + "'>" + item.title + "</td>");
                }

                fullMatrix.AppendLine("<tr><td colspan='3'></td>" + currentRow1.ToString() + "</tr>");
                fullMatrix.AppendLine("<tr><td colspan='3'></td>" + currentRow2.ToString() + "</tr>");

                currentRow1.Clear();
                currentRow2.Clear();



                bool isFirstSixth = false;

                foreach (RiskMatrix_Likelihood_Item likelihood in Likelihood_List)
                {



                    if (counter == 0)
                    {
                        currentRow3.Append("<td rowspan='" + Likelihood_List.Count + "' class='rotate matrix_header'><strong>" + _likelihood_title + "</strong></td>");
                    }
                    else
                    {
                        currentRow3.Clear();
                    }
                    counter += 1;

                    foreach (RiskMatrix_Severity_Item severity in Severity_List)
                    {

                        // If the counter is 5 and the score is 6, then we assume that its most probably the old assessnet skipping matrix issue.
                        if ((counter.ToString() == severity.score) || (counter == 5 && severity.score == "6" && isFirstSixth == false))
                        {
                            //Error Fix
                          if(counter == 5 && severity.score == "6")
                            {
                                isFirstSixth = true;
                            }

                          currentRow2.Append("<td class='c' title='" + likelihood.title + "'><!--" + likelihood.score + "--></td><td class='r' title='" + likelihood.description + "'>" + likelihood.title + "</td>");
                        }


                        cellScore = Convert.ToInt32(severity.score) * Convert.ToInt32(likelihood.score);

                        // Now lets find the relevant information on the matrix, given we know the scores.
                        // (I love lambda expressions)
                        RiskMatrix_Item currentItem = RiskMatrixOverall_List.Find(e => e.cellKey == severity.score + "_" + likelihood.score + "_" + cellScore);

                        currentRow1.Append("<td class='c' style='background-color:#" + currentItem.cellItemColour + "; color:#" + currentItem.cellFontColour + "; border-radius: 5px;' border: 2px solid #" + currentItem.cellItemColour + "; title='" + currentItem.cellItemDesc + "'>" + cellScore + "</td>");



                    }

                    currentTableRow.Append("<tr style='height:30px;'>" + currentRow3.ToString() + currentRow2.ToString() + currentRow1.ToString() + "</tr>");
                    fullMatrix.Append(currentTableRow);
                    currentRow1.Clear();
                    currentRow2.Clear();
                    currentRow3.Clear();
                    currentTableRow.Clear();


                }



                fullMatrix.AppendLine("</table></div></div>");


            }


            return fullMatrix.ToString();
        }


        private class RiskMatrix_Item
        {

            public RiskMatrix_Item(string key, string desc, string itemColour, string fontColour)
            {
                cellKey = key;
                cellItemDesc = desc;
                cellItemColour = itemColour;
                cellFontColour = fontColour;

            }

            public string cellKey { get; set; }
            public string cellItemDesc { get; set; }
            public string cellItemColour { get; set; }
            public string cellFontColour { get; set; }
        }

        private class RiskMatrix_Severity_Item
        {
            public RiskMatrix_Severity_Item(string title_In, string score_In, string description_In)
            {
                title = title_In;
                score = score_In;
                description = description_In;
            }

            public string title { get; set; }
            public string score { get; set; }
            public string description { get; set; }
        }

        private class RiskMatrix_Likelihood_Item
        {
            public RiskMatrix_Likelihood_Item(string title_In, string score_In, string description_In)
            {
                title = title_In;
                score = score_In;
                description = description_In;
            }

            public string title { get; set; }
            public string score { get; set; }
            public string description { get; set; }
        }
    }


    public class Hazards
    {

        public static string getSelectedHazardName(string hazType, string corpCode, string language = "en-gb")
        {
            string _accLang = language;

            string notSpecified = TranslationPlugin.SharedComponantTranslate("Not Specified");

            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT opts.opt_name AS hazName FROM " + "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" + " AS opts" +
                                          " WHERE opts.corp_code='" + corpCode + "' and opt_language='" + _accLang + "' and opts.opt_ref='" + hazType + "' ";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();


                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            return objrs["hazName"].ToString();
                        }
                        else
                        {
                            if (hazType != null)
                            {
                                return hazType.ToString();
                            }
                            else
                            {
                                return notSpecified;
                            }
                        }

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static void lockSelectableOpts(string corpCode, string optRef, string module, string relatedTo)
        {


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    if(module == "RA")
                    {
                        string sql_text = "UPDATE " + HttpContext.Current.Application["DBTABLE_GLOBAL_RA_OPTS"] +
                                      " SET locked = 1 " +
                                      " WHERE corp_code = '" + corpCode + "' and opt_name = '" + optRef + "' and related_to = '" + relatedTo + "' ";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        sqlComm.ExecuteReader();
                    }
                    else
                    {
                        string sql_text = "UPDATE " + HttpContext.Current.Application["DBTABLE_GLOBAL_RA_OPTS"] +
                                      " SET locked = 1 " +
                                      " WHERE corp_code = '" + corpCode + "' and opt_ref = '" + optRef + "' and related_to = '" + relatedTo + "' ";

                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        sqlComm.ExecuteReader();
                    }




                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

    }


} // End of Namespace


public class SqlCommandDumper
{
    public static string GetCommandText(SqlCommand sqc)
    {
        StringBuilder sbCommandText = new StringBuilder();

        sbCommandText.AppendLine("");

        // params
        for (int i = 0; i < sqc.Parameters.Count; i++)
            logParameterToSqlBatch(sqc.Parameters[i], sbCommandText);
        sbCommandText.AppendLine("");

        // command
        if (sqc.CommandType == CommandType.StoredProcedure)
        {
            sbCommandText.Append("EXEC ");

            bool hasReturnValue = false;
            for (int i = 0; i < sqc.Parameters.Count; i++)
            {
                if (sqc.Parameters[i].Direction == ParameterDirection.ReturnValue)
                    hasReturnValue = true;
            }
            if (hasReturnValue)
            {
                sbCommandText.Append("@returnValue = ");
            }

            sbCommandText.Append(sqc.CommandText + " ");

            bool hasPrev = false;
            for (int i = 0; i < sqc.Parameters.Count; i++)
            {
                var cParam = sqc.Parameters[i];
                if (cParam.Direction != ParameterDirection.ReturnValue)
                {
                    if (hasPrev)
                        sbCommandText.Append(", ");

                    sbCommandText.Append(cParam.ParameterName);
                    sbCommandText.Append(" = ");
                    sbCommandText.Append(cParam.ParameterName);

                    if (cParam.Direction.HasFlag(ParameterDirection.Output))
                        sbCommandText.Append(" OUTPUT");

                    hasPrev = true;
                }
            }
        }
        else
        {
            sbCommandText.AppendLine(sqc.CommandText);
        }

        sbCommandText.AppendLine("-- RESULTS");
        sbCommandText.Append("SELECT 1 as Executed");
        for (int i = 0; i < sqc.Parameters.Count; i++)
        {
            var cParam = sqc.Parameters[i];

            if (cParam.Direction == ParameterDirection.ReturnValue)
            {
                sbCommandText.Append(", @returnValue as ReturnValue");
            }
            else if (cParam.Direction.HasFlag(ParameterDirection.Output))
            {
                sbCommandText.Append(", ");
                sbCommandText.Append(cParam.ParameterName);
                sbCommandText.Append(" as [");
                sbCommandText.Append(cParam.ParameterName);
                sbCommandText.Append(']');
            }
        }
        sbCommandText.AppendLine(";");

        sbCommandText.AppendLine("");
        return sbCommandText.ToString();
    }

    private static void logParameterToSqlBatch(SqlParameter param, StringBuilder sbCommandText)
    {
        sbCommandText.Append("DECLARE ");
        if (param.Direction == ParameterDirection.ReturnValue)
        {
            sbCommandText.AppendLine("@returnValue INT;");
        }
        else
        {
            sbCommandText.Append(param.ParameterName);

            sbCommandText.Append(' ');
            if (param.SqlDbType != SqlDbType.Structured)
            {
                logParameterType(param, sbCommandText);
                sbCommandText.Append(" = ");
                logQuotedParameterValue(param.Value, sbCommandText);

                sbCommandText.AppendLine(";");
            }
            else
            {
                logStructuredParameter(param, sbCommandText);
            }
        }
    }

    private static void logStructuredParameter(SqlParameter param, StringBuilder sbCommandText)
    {
        sbCommandText.AppendLine(" {List Type};");
        var dataTable = (DataTable)param.Value;

        for (int rowNo = 0; rowNo < dataTable.Rows.Count; rowNo++)
        {
            sbCommandText.Append("INSERT INTO ");
            sbCommandText.Append(param.ParameterName);
            sbCommandText.Append(" VALUES (");

            bool hasPrev = true;
            for (int colNo = 0; colNo < dataTable.Columns.Count; colNo++)
            {
                if (hasPrev)
                {
                    sbCommandText.Append(", ");
                }
                logQuotedParameterValue(dataTable.Rows[rowNo].ItemArray[colNo], sbCommandText);
                hasPrev = true;
            }
            sbCommandText.AppendLine(");");
        }
    }

    const string DATETIME_FORMAT_ROUNDTRIP = "o";
    private static void logQuotedParameterValue(object value, StringBuilder sbCommandText)
    {
        try
        {
            if (value == null)
            {
                sbCommandText.Append("NULL");
            }
            else
            {
                value = unboxNullable(value);

                if (value is string
                    || value is char
                    || value is char[]
                    || value is System.Xml.Linq.XElement
                    || value is System.Xml.Linq.XDocument)
                {
                    sbCommandText.Append("N'");
                    sbCommandText.Append(value.ToString().Replace("'", "''"));
                    sbCommandText.Append('\'');
                }
                else if (value is bool)
                {
                    // True -> 1, False -> 0
                    sbCommandText.Append(Convert.ToInt32(value));
                }
                else if (value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal)
                {
                    sbCommandText.Append(value.ToString());
                }
                else if (value is DateTime)
                {
                    // SQL Server only supports ISO8601 with 3 digit precision on datetime,
                    // datetime2 (>= SQL Server 2008) parses the .net format, and will
                    // implicitly cast down to datetime.
                    // Alternatively, use the format string "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fffK"
                    // to match SQL server parsing
                    sbCommandText.Append("CAST('");
                    sbCommandText.Append(((DateTime)value).ToString(DATETIME_FORMAT_ROUNDTRIP));
                    sbCommandText.Append("' as datetime2)");
                }
                else if (value is DateTimeOffset)
                {
                    sbCommandText.Append('\'');
                    sbCommandText.Append(((DateTimeOffset)value).ToString(DATETIME_FORMAT_ROUNDTRIP));
                    sbCommandText.Append('\'');
                }
                else if (value is Guid)
                {
                    sbCommandText.Append('\'');
                    sbCommandText.Append(((Guid)value).ToString());
                    sbCommandText.Append('\'');
                }
                else if (value is byte[])
                {
                    var data = (byte[])value;
                    if (data.Length == 0)
                    {
                        sbCommandText.Append("NULL");
                    }
                    else
                    {
                        sbCommandText.Append("0x");
                        for (int i = 0; i < data.Length; i++)
                        {
                            sbCommandText.Append(data[i].ToString("h2"));
                        }
                    }
                }
                else
                {
                    sbCommandText.Append("/* UNKNOWN DATATYPE: ");
                    sbCommandText.Append(value.GetType().ToString());
                    sbCommandText.Append(" *" + "/ N'");
                    sbCommandText.Append(value.ToString());
                    sbCommandText.Append('\'');
                }
            }
        }

        catch (Exception ex)
        {
            sbCommandText.AppendLine("/* Exception occurred while converting parameter: ");
            sbCommandText.AppendLine(ex.ToString());
            sbCommandText.AppendLine("*/");
        }
    }

    private static object unboxNullable(object value)
    {
        var typeOriginal = value.GetType();
        if (typeOriginal.IsGenericType
            && typeOriginal.GetGenericTypeDefinition() == typeof(Nullable<>))
        {
            // generic value, unboxing needed
            return typeOriginal.InvokeMember("GetValueOrDefault",
                System.Reflection.BindingFlags.Public |
                System.Reflection.BindingFlags.Instance |
                System.Reflection.BindingFlags.InvokeMethod,
                null, value, null);
        }
        else
        {
            return value;
        }
    }

    private static void logParameterType(SqlParameter param, StringBuilder sbCommandText)
    {
        switch (param.SqlDbType)
        {
            // variable length
            case SqlDbType.Char:
            case SqlDbType.NChar:
            case SqlDbType.Binary:
                {
                    sbCommandText.Append(param.SqlDbType.ToString().ToUpper());
                    sbCommandText.Append('(');
                    sbCommandText.Append(param.Size);
                    sbCommandText.Append(')');
                }
                break;
            case SqlDbType.VarChar:
            case SqlDbType.NVarChar:
            case SqlDbType.VarBinary:
                {
                    sbCommandText.Append(param.SqlDbType.ToString().ToUpper());
                    sbCommandText.Append("(MAX /* Specified as ");
                    sbCommandText.Append(param.Size);
                    sbCommandText.Append(" */)");
                }
                break;
            // fixed length
            case SqlDbType.Text:
            case SqlDbType.NText:
            case SqlDbType.Bit:
            case SqlDbType.TinyInt:
            case SqlDbType.SmallInt:
            case SqlDbType.Int:
            case SqlDbType.BigInt:
            case SqlDbType.SmallMoney:
            case SqlDbType.Money:
            case SqlDbType.Decimal:
            case SqlDbType.Real:
            case SqlDbType.Float:
            case SqlDbType.Date:
            case SqlDbType.DateTime:
            case SqlDbType.DateTime2:
            case SqlDbType.DateTimeOffset:
            case SqlDbType.UniqueIdentifier:
            case SqlDbType.Image:
                {
                    sbCommandText.Append(param.SqlDbType.ToString().ToUpper());
                }
                break;
            // Unknown
            case SqlDbType.Timestamp:
            default:
                {
                    sbCommandText.Append("/* UNKNOWN DATATYPE: ");
                    sbCommandText.Append(param.SqlDbType.ToString().ToUpper());
                    sbCommandText.Append(" *" + "/ ");
                    sbCommandText.Append(param.SqlDbType.ToString().ToUpper());
                }
                break;
        }
    }
}