﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Class to be used for use as a replacement for the Page object
/// </summary>
///

namespace ASNETNSP
{


    public class AssessNETPage : System.Web.UI.Page, IRequiresSessionState
    {
        
        public string CorpCode;
        public string CurrentUser;
        public string CurrentUserAccess;


        public TranslationServices ts;
        public string AccLang;

        public string DefaultLocationT2;
        public string DefaultLocationT3;
        public string DefaultLocationT4;
        public string DefaultLocationT5;
        public string DefaultLocationT6;

        public PageType TypeOfPage; 


        // PreInit - REFERENCE: This is done BEFORE the controls are made!
        protected override void OnPreInit(EventArgs e)
        {
            
            base.OnPreInit(e);


            InitiateBasicSessions();

        }


        protected override void OnLoad(EventArgs e)
        {
            //https://stackoverflow.com/questions/5364375/page-load-vs-onload


            base.OnLoad(e);


            if (!IsPostBack)
            {
                SetCustomStyling();
            }

        }




        // Dispose of anything outstanding - In this case just the translation object.
        public override void Dispose()
        {

            base.Dispose();

            if (ts != null)
            {
                ts.Dispose();
            }
            
        }


        private void InitiateBasicSessions()
        {
            
            //***************************
            // CHECK THE SESSION STATUS
            SessionState.checkSession(Page.ClientScript);
            CorpCode = Context.Session["CORP_CODE"].ToString();
            CurrentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
            //END OF SESSION STATUS CHECK
            //***************************


            //***************************
            // INITIATE AND SET TRANSLATIONS
            AccLang = Context.Session["YOUR_LANGUAGE"].ToString();
            AccLang = "fr-fr";

            ts = new TranslationServices(CorpCode, AccLang, TypeOfPage.ToString());
            // END TRANSLATIONS
            //**********


            CurrentUserAccess = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_ACCESS", "user");   //0;

            DefaultLocationT2 = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_COMPANY", "user");
            DefaultLocationT3 = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_LOCATION", "user");
            DefaultLocationT4 = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_DEPARTMENT", "user");
            DefaultLocationT5 = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_TIER5", "user");
            DefaultLocationT6 = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_TIER6", "user");

        }


        // Function that sets the clients custom styling if it exists.
        private void SetCustomStyling()
        {
            Literal cssLinkClientStyleSheet = (Literal) Page.FindControl("cssLinkClientStylesheet");

            if (cssLinkClientStyleSheet != null)
            {
                cssLinkClientStyleSheet.Text = "<link href='../../framework/css/customs/" + CorpCode + ".css' rel='stylesheet' />";
            }
        }

        public enum PageType
        {
            Incident,
            RiskAssessment,
            Audit,
            UserControls
        }


    }

}