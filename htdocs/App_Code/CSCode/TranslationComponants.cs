﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;

using System.Net;
using System.Web.Script.Serialization;
using System.Resources;
using System.IO;
using System.Text;


// Class of shared componants used accross AssessNET
namespace ASNETNSP
{

    public class JsonData
    {
        public Data Data { get; set; }
    }
    public class Data
    {
        public List<Translation> Translations { get; set; }
    }
    public class Translation
    {
        public string TranslatedText { get; set; }
    }


    public class TranslationServices
    {

        private ResXResourceSet _resxSetMaster;
        private ResXResourceSet _resxSetCustom;
        private ResXResourceSet _resxSetStandard;
        private bool _resxSetCustomFlag = false;
        private string _corpCode;
        private string _language;
        private string _langCode;


        

        public TranslationServices(string corpCode, string langCode, string module)
        {

            if (corpCode == "AAAAAA")
            {
                HttpContext.Current.Response.Redirect(SessionState.getRedirectPage(), true);
            }


            _corpCode = corpCode;
            _langCode = langCode;

            if (langCode.Length > 2)
            {
                _language = langCode.Substring(0, 2);
            }
            else
            {
                _language = langCode;
            }

            SetResource(module);

            HttpContext.Current.Session["ts"] = this;
        }


        // This is used for PDF's, or things external to our usual webform system.
        public TranslationServices(string corpCode, string accRef, string module, string filename)
        {
            _corpCode = corpCode;
            
            // Grab LanguageCode from User.
            _langCode =  ReturnLanguageFromUser(accRef);

            if (_langCode.Length > 2)
            {
                _language = _langCode.Substring(0, 2);
            }
            else
            {
                _language = _langCode;
            }


            SetResource(module);

            HttpContext.Current.Session["ts"] = this;

        }

        private void SetResource(string module)
        {
            string resxFileBase =  System.AppDomain.CurrentDomain.BaseDirectory + @"/App_GlobalResources/";

            if (File.Exists(resxFileBase + _corpCode + "-" + _language + ".resx"))
            {
                _resxSetCustom = new ResXResourceSet(resxFileBase + _corpCode + "-" + _language + ".resx");
                _resxSetCustomFlag = true;
            }

            _resxSetMaster = new ResXResourceSet(resxFileBase + module + "-" + _language + ".resx");

            if (_language != "en")
            {
                _resxSetStandard = new ResXResourceSet(resxFileBase + "standardDefinitions-" + _language + ".resx");
            }

        }

        public void Dispose()
        {
            if (_resxSetCustomFlag == true)
            {
                _resxSetCustom.Close();
                _resxSetCustom.Dispose();
            }

            if (_language != "en")
            {
                _resxSetStandard.Close();
                _resxSetStandard.Dispose();
            }

            _resxSetMaster.Close();
            _resxSetMaster.Dispose();

            HttpContext.Current.Session["ts"] = null;

        }

        public string LanguageCode()
        {
            return _langCode;
        }




        public string GetPageMarker(string SourceLanguage, string customMessage = null)
        {

            string long_acc_language = "";
            string long_src_language = "";

            if (customMessage == null)
            {
                customMessage = "Electronically translated from";
            }

            string returnValue = "";

            if (_langCode != SourceLanguage)
            {
                long_acc_language = GetLanguageName(_langCode);
                long_src_language = GetLanguageName(SourceLanguage);

                returnValue = "<div class='bs-callout bs-callout-info bs-callout-xl'>" + Translate(customMessage + " " + long_src_language + " to " + long_acc_language + " using Google Translate.", SourceLanguage, "", "", "", "translation_warning") + "</div>";
            }

            return returnValue;
        }

        public string GetRecordMarker(string SourceLanguage)
        {
            string returnValue = "";

            returnValue = "<span class='badge pull-right' style='margin-left:3px;' visible='true'>" + GetLanguageName(SourceLanguage) + "</span>";

            return returnValue;
        }

        public string GetRecordMarker(string SourceLanguage, string UserLanguage)
        {
            string returnValue = "";

            if (SourceLanguage != UserLanguage)
            {
                returnValue = "<span class='badge pull-right' style='margin-left:3px;' visible='true'>" + GetLanguageName(SourceLanguage) + "</span>";
            }

            

            return returnValue;
        }


        public string GetLanguageName(string languageCode)
        {
            string returnValue = "";

            if (HttpContext.Current.Session[languageCode] != null)
            {
                returnValue = HttpContext.Current.Session[languageCode].ToString();
            }
            else
            {

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        string sql_text = "SELECT language_title FROM Module_GLOBAL.dbo.GLOBAL_Languages WHERE language_code = '" + languageCode + "'";
                        SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                        SqlDataReader objrs = sqlComm.ExecuteReader();
                        if (objrs.HasRows)
                        {
                            objrs.Read();
                            returnValue = Translate(objrs["language_title"].ToString());

                            HttpContext.Current.Session[languageCode] = returnValue;
                        }
                        sqlComm.Dispose();
                        dbConn.Close();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }

            return returnValue;
        }

        public string GetLanguage()
        {
            return _langCode;
        }


        /// <summary>
        /// Get the text to be displayed on screen from the resource file
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetResource(string key)
        {
            string returnValue = null;

            if (key != null)
            {
                if (_resxSetCustomFlag == true)
                {
                    returnValue = _resxSetCustom.GetString(key);
                }

                if (returnValue == null)
                {
                    returnValue = _resxSetMaster.GetString(key) ?? "";
                }
            }

            return returnValue;
        }


        /// <summary>
        /// Returns a translated value directly from the Google API, without storing in the database first.
        /// </summary>
        /// <param name="TextToTranslate"></param>
        /// <param name="SourceLanguage"></param>
        /// <returns></returns>
        public string Translate(string TextToTranslate, string SourceLanguage)
        {

            string TargetLanguage = _langCode;

            string translatedText = "";
            int charCount = 0;

            TextToTranslate = TextToTranslate.Trim();

            if (TextToTranslate.Length > 0)
            {

                try
                {
                    if (SourceLanguage.Length > 2 && SourceLanguage.Contains("-") == true)
                    {
                        SourceLanguage = SourceLanguage.Substring(0, 2);
                    }
                    if (TargetLanguage.Length > 2 && TargetLanguage.Contains("-") == true)
                    {
                        TargetLanguage = TargetLanguage.Substring(0, 2);
                    }


                    if (SourceLanguage != TargetLanguage)
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                        try
                        {
                            charCount = TextToTranslate.Length;

                            string url = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyDHPjD62pF36YePEGNU1CmFir8sTom3PwI";
                            url += "&source=" + SourceLanguage;
                            url += "&target=" + TargetLanguage;
                            url += "&q=" + TextToTranslate.Trim();
                            WebClient client = new WebClient();
                            string json = client.DownloadString(url);

                            byte[] bytes = Encoding.Default.GetBytes(json);
                            string encodedJson = Encoding.UTF8.GetString(bytes);

                            JsonData jsonData = (new JavaScriptSerializer()).Deserialize<JsonData>(encodedJson);
                            string output = jsonData.Data.Translations[0].TranslatedText;

                            translatedText = HttpContext.Current.Server.HtmlDecode(output);

                            //increment the character count of how many characters translated for the requesting contract (for costing purposes)
                            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                            {
                                SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.TranslationUpdateCharCount", dbConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@Corp_code", _corpCode));
                                cmd.Parameters.Add(new SqlParameter("@Char_Count", charCount));
                    
                                cmd.ExecuteNonQuery();
                                cmd.Dispose();

                                dbConn.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("[400]"))
                            {
                                translatedText = "An error occured translating into the requested language. Please contact support.";
                            }
                            else
                            {
                                translatedText = ex.Message;
                            }
                        }
                    }
                    else
                    {
                        translatedText = TextToTranslate;
                    }
                }
                catch
                {
                    translatedText = "An error has occured in the translation process. Please refresh and try again";
                }
            }
            else
            {
                translatedText = TextToTranslate;
            }

            return translatedText;
        }


        /// <summary>
        /// Return a translated version of the text passed in. The function will first search a local database based on the references and fieldname passed, before using the google API. The value returned by google will be inserted into the database with the associated references and fieldnames for future lookup
        /// </summary>
        /// <param name="TextToTranslate"></param>
        /// <param name="SourceLanguage"></param>
        /// <param name="recordRef1"></param>
        /// <param name="recordRef2"></param>
        /// <param name="recordRef3"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public string Translate(string TextToTranslate, string SourceLanguage, string recordRef1, string recordRef2, string recordRef3, string fieldName)
        {
            string TargetLanguage = _langCode;

            string translatedText = "";
            long cacheRowId = 0;

            if (recordRef1 == null) { recordRef1 = ""; }
            if (recordRef2 == null) { recordRef2 = ""; }
            if (recordRef3 == null) { recordRef3 = ""; }

            if (SourceLanguage != TargetLanguage)
            {

		TextToTranslate = HttpContext.Current.Server.HtmlDecode(TextToTranslate);

                //increment the character count of how many characters translated for the requesting contract (for costing purposes)
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.TranslationGetCache", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@RecordRef1", recordRef1));
                    cmd.Parameters.Add(new SqlParameter("@RecordRef2", recordRef2));
                    cmd.Parameters.Add(new SqlParameter("@RecordRef3", recordRef3));
                    cmd.Parameters.Add(new SqlParameter("@FieldName", fieldName));
                    cmd.Parameters.Add(new SqlParameter("@SourceText", HttpContext.Current.Server.HtmlEncode(TextToTranslate)));
                    cmd.Parameters.Add(new SqlParameter("@SourceLanguage", SourceLanguage));
                    cmd.Parameters.Add(new SqlParameter("@TargetLanguage", TargetLanguage));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        cacheRowId = Convert.ToInt32(objrs["rowId"]);
                        translatedText = HttpContext.Current.Server.HtmlDecode(objrs["translatedText"].ToString());
                    }

                    if ((translatedText == "Invalid" && cacheRowId > 0) || translatedText == "Not Found")
                    {
                        //get a new translation and update the cache
                        translatedText = Translate(TextToTranslate, SourceLanguage);

                        //store the new translation in the DB
                        SqlCommand cmd1 = new SqlCommand("Module_GLOBAL.dbo.TranslationSetCache", dbConn);
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Add(new SqlParameter("@Corp_code", _corpCode));
                        cmd1.Parameters.Add(new SqlParameter("@CachedRowId", cacheRowId));
                        cmd1.Parameters.Add(new SqlParameter("@RecordRef1", recordRef1));
                        cmd1.Parameters.Add(new SqlParameter("@RecordRef2", recordRef2));
                        cmd1.Parameters.Add(new SqlParameter("@RecordRef3", recordRef3));
                        cmd1.Parameters.Add(new SqlParameter("@FieldName", fieldName));
                        cmd1.Parameters.Add(new SqlParameter("@SourceText", HttpContext.Current.Server.HtmlEncode(TextToTranslate)));
                        cmd1.Parameters.Add(new SqlParameter("@TargetText", HttpContext.Current.Server.HtmlEncode(translatedText)));
                        cmd1.Parameters.Add(new SqlParameter("@SourceLanguage", SourceLanguage));
                        cmd1.Parameters.Add(new SqlParameter("@TargetLanguage", TargetLanguage));

                        cmd1.ExecuteNonQuery();
                        cmd1.Dispose();
                    }

                    objrs.Close();
                    cmd.Dispose();

                    dbConn.Close();
                }
                
            }
            else
            {
                translatedText = TextToTranslate;
            }

            return translatedText;
        }

        /// <summary>
        /// Use to convert standard AssessNET Terms such as Pending, Active, etc into the users language
        /// </summary>
        /// <param name="StandardTerm"></param>
        /// <returns></returns>
        public string Translate(string StandardTerm)
        {
            string returnValue = null;
            string store = StandardTerm;

            if (_langCode == "en-gb")
            {
                returnValue = StandardTerm;
            }
            else
            {

                StandardTerm = StandardTerm.Replace(" ", "_");
                StandardTerm = StandardTerm.Replace("(", "");
                StandardTerm = StandardTerm.Replace(")", "");
                StandardTerm = StandardTerm.Replace("-", "");
                StandardTerm = StandardTerm.Replace("/", "");
                StandardTerm = StandardTerm.Replace(":", "");

                if (StandardTerm != null)
                {
                    if (_resxSetCustomFlag == true)
                    {
                        returnValue = _resxSetCustom.GetString(StandardTerm);
                    }
            
                    if (returnValue == null && _resxSetStandard != null)
                    {
                        returnValue = _resxSetStandard.GetString(StandardTerm);
                    }
                }

                //return the original string if nothing found in the resource files.
                if (returnValue == null)
                {
                    returnValue = store;
                }

            }

            return returnValue;
        }



        public string TranslateName(string name)
        {
            return name.Contains("{{U}}") ? this.Translate("Details Unavailable") : name;
        }


        private string ReturnLanguageFromUser(string accRef)
        {

            string language = "";

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_HR.dbo.GetUserLanguage", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", accRef));
               
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    language = Convert.ToString(objrs["display_language"]);
                }

               
                objrs.Close();
                cmd.Dispose();

                dbConn.Close();
            }

            return language;

        }




    }

    public class TranslationPlugin
    {
        public static string SharedComponantTranslate(string StandardTerm)
        {
            TranslationServices _ts = (TranslationServices)HttpContext.Current.Session["ts"];

            if (_ts != null)
            {
                return _ts.Translate(StandardTerm);
            }
            else
            {
                return StandardTerm;
            }

        }

        public static string SharedComponantTranslate(string TextToTranslate, string FieldName)
        {
            TranslationServices _ts = (TranslationServices)HttpContext.Current.Session["ts"];

            if (_ts != null)
            {
                return _ts.Translate(TextToTranslate, "en-gb", "", "", "", FieldName);
            }
            else
            {
                return TextToTranslate;
            }

        }

        public static string SharedComponantLanguage()
        {
            TranslationServices _ts = (TranslationServices)HttpContext.Current.Session["ts"];

            if (_ts != null)
            {
                return _ts.LanguageCode();
            }
            else
            {
                return "en-gb";
            }
        }

    }

} // End of Namespace