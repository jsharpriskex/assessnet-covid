﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;



// Class of shared componants used accross AssessNET
namespace ASNETNSP
{

    public class Structure
    {


        //return structure elements as a DataTable for use within the modules.
        public static DataTable ReturnStructure (string corpCode, string accRef, string tierRef, string structureType, string accessLevel, string moduleType, string tier, string switch2)
        {
            bool hasMoreThanAllOrAny = false;

            string appendDisabled = TranslationPlugin.SharedComponantTranslate("DISABLED");
            string language = TranslationPlugin.SharedComponantLanguage();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_PEP.dbo.GetStructureTier"+ tier, dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corpCode", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@tierRef", tierRef));
                    cmd.Parameters.Add(new SqlParameter("@accRef", accRef));
                    cmd.Parameters.Add(new SqlParameter("@structureType", structureType));
                    cmd.Parameters.Add(new SqlParameter("@accessLevel", accessLevel));
                    cmd.Parameters.Add(new SqlParameter("@moduleType", moduleType));
                    cmd.Parameters.Add(new SqlParameter("@switch2", switch2));
                    cmd.Parameters.Add(new SqlParameter("@language", language));
                    objrs = cmd.ExecuteReader();



                    DataTable returnDataTable = new DataTable();
                    returnDataTable.Load(objrs);
                    
                    //This allows the columns to have data appended.
                    foreach (DataColumn col in returnDataTable.Columns)
                        col.ReadOnly = false;

                    //If there are only Any or All, we need to wipe them.
                    foreach (DataRow dr in returnDataTable.Rows)
                    {

                        if (dr[0].ToString() == "-1" || dr[0].ToString() == "0")
                        {
                            dr[1] = TranslationPlugin.SharedComponantTranslate(dr[1].ToString(), "Tier" + tier + "_" + dr[1].ToString().Replace(" ", "_"));
                        }
                        else
                        {
                            hasMoreThanAllOrAny = true;
                        }

                        //Disabled Check
                        //Check the current structure to ensure that it hasn't since been disabled.
                        //If it has but the assessment is still linked to it, do not remove it from the search. Should be fairly easy to do from here.
                        if (dr[2].ToString() == "1")
                        {
                            if (structureType == "CREATE")
                            {
                                //If the current row's tier reference is equal to the reference being passed into it
                                // AND its disabled, then do not remove it. Otherwise remove it. This will stop
                                // users from selecting structure that is disabled and also ensuring assessments already
                                // assigned to a disabled structure element will work as expected.

                                //OKAY this doesn't work. Because only the initial tier is the only one disabled, the ones below dont get. Needs more
                                // Logic within the SPROC.
                                if (dr[0].ToString() == tierRef)
                                {
                                    
                                    dr[1] = dr[1].ToString() + " (" + appendDisabled + ") ";
                                }
                                else
                                {
                                    //dr.Delete();
                                    //dr[1] = dr[1].ToString() + " (DISABLED) ";
                                }

                            }
                            else
                            {
                                dr[1] = dr[1].ToString() + " (" + appendDisabled + ") "; //Issue here. Data table row cant be added to as read only.
                            }
                        }
                    }

                    if (!hasMoreThanAllOrAny)
                    {
                        returnDataTable.Clear();
                    }

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }

        }


        public static DataTable ReturnStructure(string corpCode, string accRef, string tierRef, string nextTierRef, string structureType, string accessLevel, string moduleType, string tier, string switch2)
        {
            bool hasMoreThanAllOrAny = false;

            string appendDisabled = TranslationPlugin.SharedComponantTranslate("DISABLED");
            string language = TranslationPlugin.SharedComponantLanguage();

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_PEP.dbo.GetStructureTier" + tier, dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corpCode", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@tierRef", tierRef));
                    cmd.Parameters.Add(new SqlParameter("@accRef", accRef));
                    cmd.Parameters.Add(new SqlParameter("@structureType", structureType));
                    cmd.Parameters.Add(new SqlParameter("@accessLevel", accessLevel));
                    cmd.Parameters.Add(new SqlParameter("@moduleType", moduleType));
                    cmd.Parameters.Add(new SqlParameter("@switch2", switch2));
                    cmd.Parameters.Add(new SqlParameter("@language", language));
                    objrs = cmd.ExecuteReader();



                    DataTable returnDataTable = new DataTable();
                    returnDataTable.Load(objrs);

                    //This allows the columns to have data appended.
                    foreach (DataColumn col in returnDataTable.Columns)
                        col.ReadOnly = false;

                    //If there are only Any or All, we need to wipe them.
                    foreach (DataRow dr in returnDataTable.Rows)
                    {

                        dr[1] = Formatting.FormatTextInputField(dr[1].ToString());

                        dr[1] = (dr[1].ToString()).Replace("andamp;", "&");
                        dr[1] = (dr[1].ToString()).Replace("andquot;", "\"");
                        dr[1] = (dr[1].ToString()).Replace("andgt;", ">");
                        dr[1] = (dr[1].ToString()).Replace("andlt;", "<");


                        if (dr[0].ToString() == "-1" || dr[0].ToString() == "0")
                        {
                            dr[1] = TranslationPlugin.SharedComponantTranslate(dr[1].ToString(), "Tier" + tier + "_" + dr[1].ToString().Replace(" ", "_"));
                        }
                        else
                        {
                            hasMoreThanAllOrAny = true;
                        }

                        //Disabled Check
                        //Check the current structure to ensure that it hasn't since been disabled.
                        //If it has but the assessment is still linked to it, do not remove it from the search. Should be fairly easy to do from here.
                        if (dr[2].ToString() == "1")
                        {
                            if (structureType == "CREATE")
                            {
                                //If the current row's tier reference is equal to the reference being passed into it
                                // AND its disabled, then do not remove it. Otherwise remove it. This will stop
                                // users from selecting structure that is disabled and also ensuring assessments already
                                // assigned to a disabled structure element will work as expected.

                                //This just doesn't work down to tier2.
                                if (dr[0].ToString() == nextTierRef)
                                {

                                    dr[1] = dr[1].ToString() + " (" + appendDisabled + ") ";
                                }
                                else
                                {
                                    dr.Delete();
                                    //dr[1] = dr[1].ToString() + " (DISABLED) ";
                                }

                            }
                            else
                            {
                                dr[1] = dr[1].ToString() + " (" + appendDisabled + ") "; //Issue here. Data table row cant be added to as read only.
                            }
                        }



                    }

                    if (!hasMoreThanAllOrAny)
                    {
                        returnDataTable.Clear();
                    }

                    return returnDataTable;

                }
                catch (Exception)
                {
                    throw;
                }
            }

        }



    }

} // End of Namespace