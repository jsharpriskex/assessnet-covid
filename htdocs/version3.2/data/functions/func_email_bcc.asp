<%
' -------------------------------------------
'  Sub to send email
'
'  Using this will allow us to switch to CDO
'  instead of CDONTS when the time comes
' -------------------------------------------

Sub sendEmail(emailTo,emailFrom,emailSubject,emailBody,emailBcc)
    Dim objSend
    Set objSend = Server.CreateObject("CDONTS.NewMail")
    
    objSend.BodyFormat = 0 ' HTML
    objSend.MailFormat = 0 '
    objSend.To = emailTo
    objSend.Bcc = emailBcc
    objSend.From = emailFrom
    objSend.Subject = emailSubject
    objSend.Body = emailBody
    objSend.Send
    
    SET objSend = Nothing
End sub
%>