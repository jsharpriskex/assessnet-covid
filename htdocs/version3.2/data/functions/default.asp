<!-- #INCLUDE FILE="../../data/scripts/inc/auto_logout.inc" -->
<!-- #INCLUDE FILE="../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../data/functions/func_tasksview.asp" -->
<%


dim objrs
dim objconn
dim objcommand

call startconnections()

SUB showModules()

Response.Write  "<SELECT name='frm_modules' onChange='submit();'>" & _
                "<option value=''>- Select Module -</option>"


if mid(session("YOUR_LICENCE"),1,1) = "1" then
    Response.Write "<option value='1'>Risk Assessment</option>"
end if
if mid(session("YOUR_LICENCE"),5,1) = "1" Then
    Response.Write "<option value='5'>Method Statement</option>"
end if
if mid(session("YOUR_LICENCE"),2,1) = "1" Then
    Response.Write "<option value='2'>Manual Handling</option>"
end if
if mid(session("YOUR_LICENCE"),3,1) = "1" Then
    Response.Write "<option value='3'>DSE Assessment</option>"
end if
if mid(session("YOUR_LICENCE"),4,1) = "1" Then
    Response.Write "<option value='4'>Fire Safety</option>"
end if
if mid(session("YOUR_LICENCE"),11,1) = "1" Then
    Response.Write "<option value='11'>COSHH Assessment</option>"
end if
if mid(session("YOUR_LICENCE"),6,1) = "1" Then
    Response.Write "<option value='6'>Safety Audit</option>"
end if
if mid(session("YOUR_LICENCE"),14,1) = "1" Then
    Response.Write "<option value='14'>Permit to Work</option>"
end if
if mid(session("YOUR_LICENCE"),15,1) = "1" Then
    Response.Write "<option value='15'>Inspection</option>"
end if
Response.Write "</select>"

END SUB



%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="../../data/scripts/css/global.css" media="all" />
    <link rel='stylesheet' href="../hsafety/risk_assessment/app/default.css" media="all" />
</head>

<body>
<br />

<div id='section_0' class='optionsection' align='right'>
    <table width='100%'>
    <tr>
        <td class='l title'>Your Home Page<span>...</span><br /><span class='info'>Access useful areas of AssessNET, all from the same page.</span></td>
    </tr>
    </table>
</div>

<br />

<table border="0" cellpadding="5" cellspacing="0" width="100%">
<tr>
    <td width='50%' class='menu_col'>

    <table border="0" cellspacing="0" width="100%" class='action' onclick='goTo("menu1")'>
      <tr>
        <td width="20" class='action_icon'><img src='../../images/icons/action_create.png' /></td>
        <td class='action_text' height='50' valign='top'>
        <a href="" class='menu' id='menu1'>Create a New Assessment</a><br />
        Click here or use the menu list provided to be directed to the module(s) avalaible to you.       
        </td>
        <td width='100' class='action_text'>
        <%
            call showModules()
        %>        
        </td>
      </tr>
    </table>
    
    &nbsp;
    
    <table border="0" cellspacing="0" width="100%" class='action' onclick='goTo("menu1")'>
      <tr>
        <td width="20" class='action_icon'><img src='../../images/icons/action_search.png' /></td>
        <td class='action_text' height='50' valign='top'>
        <a href="" class='menu' id='A1'>Search for an Assessment</a><br />
        Use the menu list provided to be directed to the selected module search engine.      
        </td>
        <td width='100' class='action_text'>
        <%
            call showModules()
        %>        
        </td>
      </tr>
    </table>
    
    &nbsp;

    <table border="0" cellspacing="0" width="100%" class='action' onclick='goTo("menu1")'>
      <tr>
        <td width="20" class='action_icon'><img src='../../images/icons/action_manage.png' /></td>
        <td class='action_text' height='50' valign='top'>
        <a href="" class='menu' id='A2'>Manage and view assessments you created</a><br />
        view completed, incomplete, copied and transferred records that you own and/or created on the system       
        </td>
      </tr>
    </table>
    
    &nbsp;

    <table border="0" cellspacing="0" width="100%" class='action' onclick='goTo("menu1")'>
      <tr>
        <td width="20" class='action_icon'><img src='../../images/icons/action_kb.png' /></td>
        <td class='action_text' height='50' valign='top'>
        <a href="" class='menu' id='A3'>Online help / User guide</a><br />
        Knowledge Base is a comprehensive guide to using AssessNET. You will find information on general usage, as well as individual modules.    
        </td>
      </tr>
    </table>

    &nbsp;

    <table border="0" cellspacing="0" width="100%" class='action' onclick='goTo("menu1")'>
      <tr>
        <td width="20" class='action_icon'><img src='../../images/icons/action_kb.png' /></td>
        <td class='action_text' height='50' valign='top'>
        <a href="" class='menu' id='A4'>Contact Technical Support</a><br />
        Our Technical Support department is ready to assist in any way it can. Contact us if you have any problems, queries or suggestions.   
        </td>
      </tr>
    </table>

  </td>
  <td width="50%" class='menu_col'>

  <!-- Window style starts -->
  <div id='windowtitle' class='notopmargin'>
    <h5>Your top 5 tasks</h5>
    <!-- hidden drop -->
    <div id='windowdrop'>
    <table width='100%'>
    <tr>
        <th>Hidden filter</th><td></td>
    </tr>   
    </table>
    </div>
  <!-- hidden drop end -->
  <div id='windowmenu'>
   <span><a href='../management/taskmanager_v2/' target='main' >View all your tasks</a></span>
  </div>
  <!-- body of window -->
  <div id='windowbody' class='pad'>
  
  <% CALL Taskview(Session("YOUR_ACCOUNT_ID")) %>

    </div>
  </div>

  <!-- Window style starts -->
  <div id='windowtitle' class='notopmargin'>
    <h5>Latest news from the H.S.E.</h5>
    <!-- hidden drop -->
    <div id='windowdrop'>
    <table width='100%'>
    <tr>
        <th>Hidden filter</th><td></td>
    </tr>   
    </table>
    </div>
  <!-- hidden drop end -->
  <div id='windowmenu'>
   <span><a href='http://www.hse.gov.uk/press/press.htm' target='_blank'>View all HSE news</a></span>
  </div>
  <!-- body of window -->
  <div id='windowbody' class='pad'>

    <div class='news'>
        <table width='100%' cellpadding='2px' cellspacing='2px'>
        <tr><td width='3' valign='middle'><img src='../../images/icons/feed.gif' /></td><td valign='middle'>HSE launches REACH helpdesk</td><td width='5'><a href='http://www.hse.gov.uk/press/2006/e06101.htm' target="_blank"><img src='../../images/icons/sml_viewclr.gif' title='View this news in more detail' /></a></td></tr>
        </table>
        <table width='100%' cellpadding='2px' cellspacing='2px'>
        <tr><td width='3' valign='middle'><img src='../../images/icons/feed.gif' /></td><td valign='middle'>HSE publishes report following investigation of construction worker death</td><td width='5'><a href='http://www.hse.gov.uk/press/2006/e06100.htm' target="_blank"><img src='../../images/icons/sml_viewclr.gif' title='View this news in more detail' /></a></td></tr>
        </table>
        <table width='100%' cellpadding='2px' cellspacing='2px'>
        <tr><td width='3' valign='middle'><img src='../../images/icons/feed.gif' /></td><td valign='middle'>Migrant worker research published</td><td width='5'><a href='http://www.hse.gov.uk/press/2006/e06099.htm' target="_blank"><img src='../../images/icons/sml_viewclr.gif' title='View this news in more detail' /></a></td></tr>
        </table>
        <table width='100%' cellpadding='2px' cellspacing='2px'>
        <tr><td width='3' valign='middle'><img src='../../images/icons/feed.gif' /></td><td valign='middle'>Company fined �100,000, employees suffered from painful allergic dermatitis over four year period</td><td width='5'><a href='http://www.hse.gov.uk/press/2006/e06098.htm' target="_blank"><img src='../../images/icons/sml_viewclr.gif' title='View this news in more detail' /></a></td></tr>
        </table>
    </div>

  </div>
  </div>

  </td>
</tr>
</table>
<% CALL endconnections() %>
</body>
</html>