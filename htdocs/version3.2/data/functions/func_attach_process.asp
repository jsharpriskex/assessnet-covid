<!-- #INCLUDE FILE="../scripts/inc/auto_logout.inc" -->
<!-- #INCLUDE FILE="../scripts/inc/dbconnections.inc" -->

<%

dim objrs
dim objconn
dim objcommand

call startconnections()


var_att_command = request.queryString("att_cmd")
var_att_mod_reference = request.queryString("att_mod_ref")
var_att_reference = request.queryString("att_ref")
var_att_mod = request.querystring("att_mod")
var_att_mod_sub = request.querystring("att_mod_sub")

if var_att_command = "rem_att" then

    if instr(1, var_att_mod_reference, "-") > 0 then
        var_att_mod_anchor = mid(var_att_mod_reference, instr(1, var_att_mod_reference, "-")+1, len(var_att_mod_reference))
        var_att_mod_reference = left(var_att_mod_reference, instr(1, var_att_mod_reference, "-")-1)
    else
        var_att_mod_anchor = "section5a"
    end if
        

    'remove attachment
    objCommand.commandText =  "UPDATE MODULE_GLOBAL.dbo.GLOBAL_Attach_Assessments " & _
                              "SET corp_code = '" & session("CORP_CODE") & "A' " & _
                              "WHERE corp_code = '" & session("CORP_CODE") & "' AND recordreference = '" & var_att_mod_reference & "' AND attach_ref = '" & var_att_reference & "' "
    objCommand.execute                              
    
    select case var_att_mod
        case "MS"
            objCommand.CommandText =  "UPDATE " & Application("DBTABLE_MS_ATTACH") & " " & _
                                      "SET corp_code = '" & session("CORP_CODE") & "A' " & _
                                      "WHERE corp_code = '" & session("CORP_CODE") & "' AND recordreference = '" & var_att_mod_reference & "' AND attached_ref = '" & var_att_reference & "' "
            objCommand.Execute

            response.Redirect "../../modules/hsafety/method_file.asp?frm_stage=10&cmd=EDIT&frm_reference=" & var_att_mod_reference & "&stage10"  
            response.End 

        case "PTW"
            if var_att_mod_sub = "file" then

                if instr(1, var_att_mod_reference, "_") > 0 then
                    var_att_mod_reference_link = left(var_att_mod_reference, instr(1, var_att_mod_reference, "_")-1)
                else
                    var_att_mod_reference_link = var_att_mod_reference
                end if

                'need to find the template this permit uses to pass back
                objCommand.commandText = "SELECT template_reference FROM " & Application("DBTABLE_PTW_DATA_ENTRIES") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND ptw_reference = '" & var_att_mod_reference_link & "'"
                set objRs_ptw = objCommand.execute
                if not objRs_ptw.EOF then
                    objRs_ptw.movefirst
                    var_ptw_template_ref = objRs_ptw("template_reference")
                end if
                set objRs_ptw = nothing

                response.Redirect "../../modules/hsafety/permit_to_work_v2/app/ptw_report.asp?link=srch&ptw_ref=" & var_att_mod_reference_link & "&temp_ref=" & var_ptw_template_ref & "#" & var_att_mod_anchor
            else
                response.Redirect "../../modules/hsafety/permit_to_work_v2/app/template/template_preview.asp?link=srch&cmd=vw&ref=" & var_att_mod_reference & "#section5a"
            end if
            response.end 

        case "ACC"
            response.Redirect "../../modules/hsafety/accident/accident_details.asp?IC=" & left(var_att_mod_reference, instr(1,var_att_mod_reference, "/")-1) & "&INFOID=" & mid(var_att_mod_reference, instr(1,var_att_mod_reference,"/")+1, len(var_att_mod_reference)) & "&view=AT&anchor=tabs"
            response.end 

    end select

End IF

call endconnections()

%>