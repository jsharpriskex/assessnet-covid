<!-- #include file="../scripts/inc/auto_logout.inc" -->
<!-- #include file="../scripts/inc/dbconnections.inc" -->
<%

'Global Strings
DIM Objrs
DIM Objconn
DIM Objcommand

'' Start Database Connection
CALL startCONNECTIONS()

Sub uploadfile()

		Set Upload = Server.CreateObject("Persits.Upload.1")
		Set File = Upload.Files("up_frm_file")
		
		Upload.OverwriteFiles = False
		'Upload.LogonUser "SASBUK02", "Administrator", "Admin8461" 		
		Upload.SetMaxSize 2000000, True
		Upload.Save
		
		Path = "c:\upload\" & Session("CORP_CODE") & "\" & Session("YOUR_ACCOUNT_ID") & "\"
		Upload.CreateDirectory Path, True
		
			For Each File in Upload.Files
			
				   If Upload.FileExists(Path & file.Filename ) Then
     				response.write "Filename already exsists"
   				   		Else
   				   			If (File.Ext = ".jpg") OR (File.Ext = ".jpeg") OR (File.Ext = ".jpe") OR (File.Ext = ".gif") OR (File.Ext = ".png") OR (File.Ext = ".doc") OR (File.Ext = ".xls") OR (File.Ext = ".pdf") OR (File.Ext = ".rtf") OR (File.Ext = ".txt") OR (File.Ext = ".wps") OR (File.Ext = ".mcw") Then
								
								IF (left(File.ContentType,5) = "Audio") OR (left(File.ContentType,5) = "Video") Then
								response.write "Detected Audio or Video file"
									ELSE				   		  	
   				   		  			File.SaveAs Path & File.Filename
   				   		  			
   				   		  			' SQL db
   				   		  			UP_STOR_Size = File.OriginalSize
   				   		  			UP_STOR_Ext = File.Ext
   				   		  			UP_STOR_name = File.Filename
   				   		  			UP_STOR_path = Path & File.Filename
   				   		  			UP_STOR_vpath = Session("CORP_CODE") & "/" & Session("YOUR_ACCOUNT_ID") & "/"
   				   		  			UP_STOR_desc = Upload.Form("up_frm_desc")
   				   		  			ACCOUNTID = Session("YOUR_ACCOUNT_ID")
   				   		  			CORP_CODE = Session("CORP_CODE")
   				   		  			HOSTNAME = Session("YOUR_IP_ADDRESS")
   				   		  			UP_STOR_datetime = Date() & " " & Time()
   				   		  			 
										Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_DOCS") & " " & _
							 									 "(UP_STOR_SIZE,UP_STOR_EXT,UP_STOR_NAME,UP_STOR_PATH,UP_STOR_VPATH,UP_STOR_DESC,ACCOUNTID,CORP_CODE,HOSTNAME,UP_STOR_DATETIME) " & _
							 									 "VALUES('" & UP_STOR_SIZE & "','" & UP_STOR_EXT & "','" & UP_STOR_NAME & "','" & UP_STOR_PATH & "','" & UP_STOR_VPATH & "','" & UP_STOR_DESC & "','" & ACCOUNTID & "','" & CORP_CODE & "','" & HOSTNAME & "','" & UP_STOR_DATETIME & "')"
										Objcommand.execute
   				   		  			   				   		  			
   				   		  		End IF
   				   		  		
								ELSE
   				   		  		response.write "invalid file extention " & File.Ext
   				   		  	END IF
   				   End IF
			
			Next			

		SET Upload = NOTHING
		SET File = NOTHING
		
End Sub

IF Request("status") = "go" Then

	CALL uploadfile()

End IF

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<!--

**********************************************************
* Upload tools                                           *
*                                                        *
**********************************************************

gif, jpg, jpeg, jpe, png

xls, doc, pdf, rtf, txt, mcw, wps

test for Audio/ and Video/ 

-->

<head>
    <title>My Documents - AssessNET</title>
    <meta name="author" content="Safe and Sound Management Consultants Ltd" />
    <meta name="description" content="AssessNET Health and Safety System Online Assessments and Management" />

    <link rel='stylesheet' href='../scripts/css/global.css' type='text/css' media='screen' />
    <style type='text/css'>
  

.inputtable
{
border-top: solid 1px #0066C0;
}  

.uploadcell
{
border-bottom: solid 1px #3399FF;
border-left: solid 1px #3399FF;
border-right: solid 1px #3399FF;
background: #CDD7FE;
}

TD.inputs
{
 background: #f5f5f5;
 border-left: 1px solid #E8E8E8;
 border-right: 1px solid #E8E8E8;
 border-bottom: 1px solid #E8E8E8;
}

TD.files
{
 border-bottom: 1px solid #E8E8E8;
}

.dynamicarea
{
 width: 99%;	
}

 
    </style>
</head>

<body topmargin="0">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td><div id='lost'>
      <strong>Lost?</strong> You are in ::
        Account Centre
      </div>
    </td>
    <td width='180' align='right'>
      <div id='corplogo'></div>
    </td>
  </tr>
</table>

<h1>Upload a document / Image <span>...</span></h1> 

<FORM METHOD="POST" ENCTYPE="multipart/form-data" ACTION="func_documents.asp?status=go">	
<hr color='#E8E8E8' size='1' />

<div>
	<table width='100%' cellspacing='0' cellpadding='3' class='inputtable'>
		<tr>
			<td valign='top'>
			<strong>Select a file to upload</strong><br />
			Click "Browse" to open the file manager and search through your computer or local network and select the file you wish to upload
			<br /><br />
			<strong>File types accepted are :</strong> jpg, jpeg, jpe, gif, png, doc, xls, pdf, rtf, txt, wps, mcw.
			<br />
			<strong>Maximum File size accepted :</strong> 2mb (2000k)
			</td>
			<td width='400' class='uploadcell' align='center'>
			
				<table width='370' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td align='left' colspan='2'><img border='0' src='../../images/icons/source_grn.gif' align='absmiddle'> <strong>File to upload</strong></td>
					</tr>
					<tr>
						<td align='left' colspan='2'><INPUT TYPE=FILE NAME="up_frm_file" size='30'></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td align='left' colspan='2'><img border='0' src='../../images/icons/source_grn.gif' align='absmiddle'> <strong>File Description</strong></td>
					</tr>
					<tr>
						<td align='left' colspan='2'><TEXTAREA name='up_frm_desc' class='dynamicarea'></TEXTAREA></td>
					</tr>
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>
					<tr>
						<td align='left'><INPUT TYPE=IMAGE  src='../../images/icons/lrg_upload.gif' VALUE="Upload!"></td>
						<td align='right'><input type=checkbox /> Agree to Terms and Conditions</td>
					</tr>
				</table>
			
			</td>
		</tr>
	</table>

</div>

<hr color='#E8E8E8' size='1' />

</FORM>

<br />

<div>
	
	<table width='100%' cellspacing='0' cellpadding='3' border='0'>
		<tr>
			<td width='130'><img border="0" src="../../images/icons/source.gif" align='absmiddle'> <a href='#'><strong>Your Uploads</strong></a></td>
			<td width='130'><img border="0" src="../../images/icons/source.gif" align='absmiddle'> <a href='#'><strong>Shared Uploads</strong></a></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	
	<table width='100%' cellspacing='0' cellpadding='3' border='1' class='filetable'>
		<tr>
			<td width='18' class='inputs'>&nbsp;</td>
			<td width='100' class='inputs'>Date Uploaded</td>
			<td width='200' class='inputs'>File Name</td>
			<td class='inputs'>File Description</td>
			<td width='70' class='inputs'>File Size</td>
			<td width='150' class='inputs'>Options</td>
		</tr>
	</table>
	
	<table width='100%' cellspacing='1' cellpadding='3' border='0' class='filetable'>
		<tr>
		
<%

	Objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_GLOBAL_DOCS") & " " & _
							 "WHERE CORP_CODE='" & Session("CORP_CODE") & "' "
	SET Objrs = Objcommand.execute
		
		If Objrs.EOF Then
		
			Response.write "<td colspan='6' align='center'><br />You have currently uploaded no files to AssessNET</td>"
		
		ELSE
		
			While NOT Objrs.EOF
			
			SELECT CASE Objrs("UP_STOR_EXT")
				CASE ".gif"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/gif.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".jpe"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/jpe.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".jpeg"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/jpeg.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".jpg"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/jpg.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".pdf"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/pdf.gif' align='absmiddle'"
				UP_STOR_CMD = "<a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'><img src='../../images/icons/sml_download.gif'></a>"
				CASE ".png"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/png.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".txt"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/txt.gif' align='absmiddle'"
				UP_STOR_CMD = "<img src='../../images/icons/sml_viewclr.gif'>"
				CASE ".xls"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/xls.gif' align='absmiddle'"
				UP_STOR_CMD = "<a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'><img src='../../images/icons/sml_download.gif'></a>"
				CASE ".doc"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/doc.gif' align='absmiddle'"
				UP_STOR_CMD = "<a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'><img src='../../images/icons/sml_download.gif'></a>"
				CASE ".wps"
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/wps.gif' align='absmiddle'"
				UP_STOR_CMD = "<a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'><img src='../../images/icons/sml_download.gif'></a>"
				CASE ESE
				UP_STOR_FILETYPE = "<img border='0' src='../../images/icons/filetypes/txt.gif' align='absmiddle'"
				UP_STOR_CMD = "<a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'><img src='../../images/icons/sml_download.gif'></a>"
			END SELECT
			
		
			response.write "<tr>"
			response.write "<td width='18' class='files'>" & UP_STOR_FILETYPE & "</td>"
			response.write "<td width='100' class='files'>" & left(Objrs("UP_STOR_datetime"),10) & "</td>"
			response.write "<td width='200' class='files'><a href='../../../uploads/" & Objrs("UP_STOR_vpath") & Objrs("UP_STOR_name") & "'>" & Objrs("UP_STOR_name") & "</a></td>"
			response.write "<td class='files'>" & Objrs("UP_STOR_desc") & "</td>"
			response.write "<td width='70' class='files'>" & Round((Objrs("UP_STOR_size") / 1024)) & "K</td>"
			response.write "<td width='150' class='files'>" & UP_STOR_CMD & " <img src='../../images/icons/sml_disable_dis.gif'></td>"
			response.write "</tr>"			
			
			Objrs.Movenext
			Wend

		End IF

	CALL EndCONNECTIONS()
		
%>
	
		</tr>
	</table>
	
</div>

&nbsp;

</body>
</html>