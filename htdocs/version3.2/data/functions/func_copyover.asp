<!-- #INCLUDE FILE="../scripts/inc/auto_logout.inc" -->
<% 

varCorpCode = "199002"

If request.querystring("CMD") = "COPY" then

	' Make Objects needed for this script
	
	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH


	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn
	
	SET objCommand2 = Server.CreateObject("ADODB.Command")
	SET objCommand2.ActiveConnection = objconn
   	  	
   	' Get current Record Header information
   	
   		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_DATA") & _
   								 " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   								 
   		SET objrs = objcommand.execute
   	
   		hostname = Session("YOUR_IP_ADDRESS")
   		status = "3"
   		recorddate = date()
   		recordtime = time()
   		recordtype = objrs("recordtype")
   		accountid = "DEMEX214431191"
   		recordrevision = "0"
   		recordcreator = Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME")
   		recordequipment = Replace("" & objrs("recordequipment"),"'","''")
   		recorddescription = Replace("" & objrs("recorddescription"),"'","''")
   		recordrisk = Replace("" & objrs("recordrisk"),"'","''")
   		recordgenerated = date()
		recordriskscore = objrs("recordriskscore")
		recordtotalaffected = objrs("recordtotalaffected")
		recordlastmodified = Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME")
		rec_BG = Replace("" & objrs("rec_BG"),"'","''")
		rec_BU = Replace("" & objrs("rec_BU"),"'","''")
		rec_BL = Replace("" & objrs("rec_BL"),"'","''")
		rec_BD = Replace("" & objrs("rec_BD"),"'","''")
   	
       ' Find a new Record Number
		objcommand.commandtext = "SELECT corp_licence_value FROM " & Application("DBTABLE_MODULES_DATA") & _
								 " WHERE corp_licence_type='RA' AND CORP_CODE='" & varCorpCode & "'" 
								 
		Set objRS = objCommand.Execute	
		recordreference = objrs("corp_licence_value") + 1
		
        ' Add the new Number
		objcommand.commandtext = "UPDATE " & Application("DBTABLE_MODULES_DATA") & " SET corp_licence_value=" & recordreference & _ 
		                         " WHERE corp_licence_type='RA' AND CORP_CODE='" & varCorpCode & "'"
		                          
		objcommand.execute
		recordreference = recordreference & "RA"
					
        ' Insert Information into new Row with new Record Number
		objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_DATA") & _
		                         " (corp_code,accountid,hostname,status,recorddate,recordtime,recordtype,recordrevision," & _
		                         "recordcreator,recordequipment,recorddescription,recordrisk,recordreference," & _
		                         "recordriskscore,recordgenerated,recordtotalaffected,recordlastmodified,rec_BG" & _
		                         ",rec_BU,rec_BL,rec_BD) VALUES ('" & varCorpCode & "','" & accountid & "','" & hostname & "','" & status & _
		                         "','" & recorddate & "','" & recordtime & "','" & recordtype & "','0','" & recordcreator & _
		                         "','" & recordequipment & "','" & recorddescription & "','" & recordrisk & _
		                         "','" & recordreference & "','" & recordriskscore & "','" & recordgenerated & _
		                         "','" & recordtotalaffected & "','" & recordlastmodified & "','" & rec_BG & "','" & rec_BU & _
		                         "','" & rec_BL & "','" & rec_BD & "') "
		objcommand.execute
		
        ' Select all person affected for that Record	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_PERSONS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
        ' Add each one to a new row as they are found
		If NOT objRs.eof Then
			While NOT objRs.EOF
   				objCommand2.CommandText = "INSERT INTO " & Application("DBTABLE_RAMOD_PERSONS") & _
   				                          " (corp_code,recordpersons,recordpersonsamount,recordreference,rec_BG,rec_BU,rec_BL,rec_BD)" & _
   				                          " VALUES ('" & varCorpCode & "','" & objrs("recordpersons") & "','" & objrs("recordpersonsamount") & _
   				                          "','" & recordreference & "','" & rec_BG & "','" & rec_BU & "','" & rec_BL & _
   				                          "','" & rec_BD & "') "
   				                          
				objCommand2.Execute
				objRs.MoveNext
			Wend
		End if
	
	' Select all hazards for that Record
	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_HAZARDS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
	' Add each one to a new row as they are found
		
		If objrs.eof then
			else
			While NOT objrs.EOF
   				objcommand2.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_HAZARDS") & _
   				                          " (corp_code,recordreference,recordhaztype,recordhazdescription,recordhazcontrols," & _
   				                          "recordhazseverity,recordhazlikelihood,recordhazscore,recordhazcontrolsreq," & _
   				                          "recordhazseverityreq,recordhazlikelihoodreq,recordhazscorereq,recordhazowner," & _
   				                          "recordhazrisk,recordhazriskreq,rec_BG,rec_BU,rec_BL,rec_BD) " & _
   				                          "VALUES ('" & varCorpCode & "','" & recordreference & "','" & objrs("recordhaztype") & _
   				                          "','" & objrs("recordhazdescription") & "','" & objrs("recordhazcontrols") & _
   				                          "','" & objrs("recordhazseverity") & "','" & objrs("recordhazlikelihood") & _
   				                          "','" & objrs("recordhazscore") & "','" & objrs("recordhazcontrolsreq") & _
   				                          "','" & objrs("recordhazseverityreq") & "','" & objrs("recordhazlikelihoodreq") & _
   				                          "','" & objrs("recordhazscorereq") & "','Not Actioned','" & objrs("recordhazrisk") & "','" & objrs("recordhazriskreq") & "','" & rec_BG & _
   				                          "','" & rec_BU & "','" & rec_BL & "','" & rec_BD & "') "
   				                          
				objcommand2.execute
				objrs.movenext
			wend
		end if

	IF Session("MODULE_RA") = "PRO" then
	' Select all hazards for that Record
	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_ACTIONS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
	' Add each one to a new row as they are found
		
		If objrs.eof then
			else
			While NOT objrs.EOF
   				objcommand2.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_ACTIONS") & _
   				                          " (corp_code,recordreference,actioned,actiondate,hazid,task,likelihood,severity,risk,accountid,rec_BG,rec_BU,rec_BL,rec_BD) " & _
   				                          "VALUES ('" & varCorpCode & "','" & objrs("recordreference") & "','" & objrs("actioned") & "','" & objrs("actiondate") & "','" & objrs("hazid") & "','" & objrs("task") & "','" & objrs("likelihood") & "','" & objrs("severity") & "','" & objrs("risk") & "','" & objrs("accountid") & "','" & rec_BG & "','" & rec_BU & "','" & rec_BL & "','" & rec_BD & "') "
   				                          
				objcommand2.execute
				objrs.movenext
			wend
		end if
	END IF
	
	' Kill objects
	
	objrs.close
	objCommand.ActiveConnection.close

	SET objCommand2.ActiveConnection = NOTHING
	SET objcommand2 = NOTHING
	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING
	
	redirectURL = "../../modules/acentre/acentre_briefcase.asp?cmd=SORTreference&show=" & request.querystring("show")
	response.redirect(redirectURL)

ELSE

%>
<HTML>
<BODY>

<script type="text/javascript">
var name = confirm("Are you sure you want to Copy Record (<% =request.querystring("code") %>)")
if (name == true)
{
parent.main.location.href=("func_copyover.asp?CMD=COPY&code=<% =request.querystring("code") %>&show=<% =request.querystring("show") %>")
}
else
{
parent.main.location.href=("../../modules/acentre/acentre_briefcase.asp?show=<% =request.querystring("show") %>&page=<% =Request.querystring("page") %>&bmk=<% =Request.querystring("bmk") %>")
}
</script>

</BODY>
</HTML>
<%

END IF
   	  	
%>