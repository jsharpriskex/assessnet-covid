<%
response.buffer = true
Set objRS = Server.CreateObject("ADODB.Recordset")
Set objcommand = Server.CreateObject("ADODB.Command")
objcommand.activeconnection = Session("strConnect")
objcommand.commandtype = adCmdText
objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_DATA") & " WHERE recordreference = '" & request.querystring("code") & "'"
SET objrs = objcommand.execute
recordcompany = objrs("recordcompany")
recorddate = objrs("recorddate")
recordrevision = objrs("recordrevision")
recordcreator = objrs("recordcreator")
recordgenerated = objrs("recordgenerated")
recorddepartment = objrs("recorddepartment")
recordlocation = objrs("recordlocation")
recordequipment = objrs("recordequipment")
recordreassessment = "20/20/2001"
recorddescription = objrs("recorddescription")
recordtotalaffected = objrs("recordtotalaffected")
recordreference = request.querystring("code")

SET objrs = Nothing
objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_PERSONS") & " WHERE recordreference = '" & request.querystring("code") & "'"
SET objrs = objcommand.execute
recordpersons = ""
while NOT objrs.EOF
recordpersons = recordpersons & objrs("recordpersons") & ", "
objrs.movenext
wend
SET objrs = Nothing

objcommand.commandtext = "SELECT recordhazrisk FROM " & Application("DBTABLE_RAMOD_HAZARDS") & " WHERE recordreference = '" & request.querystring("code") & "'"
SET objrs = objcommand.execute
while not objrs.eof
		If objrs("recordhazrisk") = "Low Risk" then
		low = "true"
		elseif objrs("recordhazrisk") = "Medium Risk" then
		medium = "true"
		elseif objrs("recordhazrisk") = "High Risk" then
		high = "true"
		end if
objrs.movenext
wend
		If low = "true" then
		recordreassessment = DateAdd("yyyy",1,recorddate)
		end if
		If medium = "true" then
		recordreassessment = DateAdd("m",1,recorddate)
		end if
		If high = "true" then
		recordreassessment = recorddate
		end if
		
		low = ""
		medium = ""
		high = ""
		
SET objrs = Nothing

Dim file_being_created
dim fso
dim act

file_being_created="../my_documents/" & recordreference & ".doc"
set fso = createobject("scripting.filesystemobject")
Set act = fso.CreateTextFile(server.mappath(file_being_created), true)

act.writeLine("<html>")
act.writeLine("<title>Weclome to Your AssessWEB</title>")
act.writeLine("</head>")
act.writeLine("<body topmargin='0' leftmargin='0' link='#E89C00' vlink='#E89C00' alink='#E89C00'>")
act.writeLine("<table border='0' width='590' cellspacing='0' cellpadding='0'>")
  act.writeLine("<tr>")
    act.writeLine("<td width='616'>")
      act.writeLine("<font face='Verdana' size='1'>")
      act.writeLine("<br>")
      act.writeLine("<b>&nbsp;Risk assessment report details for reference number&nbsp; :")
      act.writeLine("( <font color=red>" & request.querystring("code") & "</font> )<br>")
      act.writeLine("<br>")
      act.writeLine("</b>")
      act.writeLine("</font>")

      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='71%' style='border-left: 1 solid #000000; border-top: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Company</font></b></td>")
          act.writeLine("<td width='15%' style='border: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Date</font></b></td>")
          act.writeLine("<td width='14%' style='border-right: 1 solid #000000; border-top: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Revision</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='71%' style='border-left: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordcompany & "</td>")
          act.writeLine("<td width='15%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recorddate & "</td>")
          act.writeLine("<td width='14%' style='border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordrevision & "</td>")
        act.writeLine("</tr>")
      act.writeLine("</table>")
      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='34%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Assessed</font></b></td>")
          act.writeLine("<td width='47%' style='border-right: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Last")
            act.writeLine("modified by</font></b></td>")
          act.writeLine("<td width='19%' style='border-right: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Generated</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='34%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordcreator & "</td>")
          act.writeLine("<td width='47%' style='border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordcreator & "</td>")
          act.writeLine("<td width='19%' style='border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordgenerated & "</td>")
        act.writeLine("</tr>")
      act.writeLine("</table>")
      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='33%' style='border-left: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Department")
            act.writeLine("Assessed</font></b></td>")
          act.writeLine("<td width='33%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000' bgcolor='#FFEEDD'><b><font face='Verdana' size='1'>Location")
            act.writeLine("Assessed</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='33%' style='border-left: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recorddepartment & "</td>")
          act.writeLine("<td width='33%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordlocation & "</td>")
        act.writeLine("</tr>")
      act.writeLine("</table>")
      act.writeLine("<p><font face='Verdana' size='1'>&nbsp;<b>Risk assessment details ( </b>To")
      act.writeLine("be re-assessed on " & recordreassessment & " <b>)</b></font>")
      act.writeLine("</p>")
      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='73%' style='background-color: #FFEEDD; border-left: 1 solid #000000; border-top: 1 solid #000000; border-bottom: 1 solid #000000'><b><font face='Verdana' size='1'>What")
            act.writeLine("was Assessed</font></b></td>")
          act.writeLine("<td width='27%' style='background-color: #FFEEDD; border: 1 solid #000000'><b><font face='Verdana' size='1'>Re-assessment")
            act.writeLine("date</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='73%' style='border-left: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordequipment & "</font></td>")
          act.writeLine("<td width='27%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordreassessment & "</font></td>")
        act.writeLine("</tr>")
      act.writeLine("</table>")
      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='100%' style='background-color: #FFEEDD; border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><b><font face='Verdana' size='1'>Description")
            act.writeLine("of work area and/or activity assessed</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='100%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & replace(recorddescription,vbCrLf,"<BR>") & "</font></td>")
        act.writeLine("</tr>")
      act.writeLine("</table>")
      act.writeLine("<table border='0' width='100%' cellpadding='2' cellspacing='0'>")
        act.writeLine("<tr>")
          act.writeLine("<td width='91%' style='background-color: #FFEEDD; border-left: 1 solid #000000; border-bottom: 1 solid #000000'><b><font face='Verdana' size='1'>Persons")
            act.writeLine("affected</font></b></td>")
          act.writeLine("<td width='9%' style='background-color: #FFEEDD; border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><b><font face='Verdana' size='1'>Total</font></b></td>")
        act.writeLine("</tr>")
        act.writeLine("<tr>")
          act.writeLine("<td width='91%' style='border-left: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordpersons & "</font></td>")
          act.writeLine("<td width='9%' style='border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000'><font face='Verdana' size='1'>" & recordtotalaffected & "</font></td>")
        act.writeLine("</tr>")
      act.writeLine("</table><br>")
    
      objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_HAZARDS") & " WHERE recordreference = '" & request.querystring("code") & "'"
      SET objrs = objcommand.execute
      while not objrs.EOF
      
      If objrs("recordhazseverity") = "1" then
      recordhazseverity = "Minor"
      elseif objrs("recordhazseverity") = "2" then
      recordhazseverity = "Moderated"
      elseif objrs("recordhazseverity") = "3" then
      recordhazseverity = "Serious"
      elseif objrs("recordhazseverity") = "4" then
      recordhazseverity = "Major"
      elseif objrs("recordhazseverity") = "5" then
      recordhazseverity = "Catastrophic"
      end if
      
      If objrs("recordhazlikelihood") = "1" then
      recordhazlikelihood = "Unlikely"
      elseif objrs("recordhazlikelihood") = "2" then
      recordhazlikelihood = "Possible"
      elseif objrs("recordhazlikelihood") = "3" then
      recordhazlikelihood = "Likely"
      elseif objrs("recordhazlikelihood") = "4" then
      recordhazlikelihood = "Probable"
      elseif objrs("recordhazlikelihood") = "5" then
      recordhazlikelihood = "Imminent"
      end if 
      						act.writeLine(" <table border='0' width='590'>")
							act.writeLine("   <tr>")
							act.writeLine("     <td width='50%'><font face='verdana' size='1'><b>Potential")
							act.writeLine("       hazard: </font><font face='Arial, Helvetica,Geneva,Swiss' size='2' color='red'>&nbsp;" & objrs("recordhaztype") & "</b></font></td>")
							act.writeLine("     <td width='50%'><font face='verdana' size='1'><b>Existing</b></font><font face='verdana' size='1'><b>")
							act.writeLine("       control measures in place:</b></font></td>")
							act.writeLine("   </tr>")
							act.writeLine(" </table>")
							act.writeLine(" <table border='0' width='590'>")
							act.writeLine("   <tr>")
							act.writeLine("     <td bgcolor='#FFEEDD' valign=top width='50%' style='border: 1 solid #000000'><font face='verdana' size='1'>" & replace(objrs("recordhazdescription"),vbCrLf,"<BR>") & "</font></td>")
							act.writeLine("     <td width='50%' valign=top style='border: 1 solid #000000'><font face='verdana' size='1'>" & replace(objrs("recordhazcontrols"),vbCrLf,"<BR>") & "</font></td>")
							act.writeLine("   </tr>")
							act.writeLine(" </table>")
							act.writeLine(" <table border='0' width='590' height='63'>")
							act.writeLine("   <tr>")
							act.writeLine("     <td width='100%' height='15'>")
							act.writeLine("       <p align='center'><font face='verdana' size='1'><br><b>Current")
							act.writeLine("       risk rating = </font><font face='Arial, Helvetica,Geneva,Swiss' size='2' color='red'>" & objrs("recordhazrisk") & "</font><font face='verdana' size='1'>&nbsp; ( Likelihood = </b><i>" & recordhazlikelihood & "</i><b>&nbsp; v Severity = </b><i>" & recordhazseverity & "</i><b> )<br><br></font></td>")
							act.writeLine("   </tr>")
							act.writeLine("   <tr>")
							act.writeLine("     <td width='100%' height='14'><font face='verdana' size='1'><b>Required")
							act.writeLine("       action(s) to reduce risk:</b></font></td>")
							act.writeLine("   </tr>")
							act.writeLine("   <tr>")
							act.writeLine("     <td width='100%' style='border: 1 solid #000000' height='16'><font face='verdana' size='1'>" & replace(objrs("recordhazcontrolsreq"),vbCrLf,"<BR>") & "</font></td>")
							act.writeLine("   </tr>")
							act.writeLine("   <tr>")
							act.writeLine("     <td width='100%' height='16'>")
							act.writeLine("       <center><br><font face='verdana' size='1'><b>Potential")
							act.writeLine("       risk with required action(s) in place</b> = <b> " & objrs("recordhazriskreq") & " </b></font></td>")
							act.writeLine("   </tr></center>")
							
		If objrs("recordhazrisk") = "Low Risk" then
		low = "true"
		reviewdate = DateAdd("yyyy",1,recorddate)
		elseif objrs("recordhazrisk") = "Medium Risk" then
		medium = "true"
		reviewdate = DateAdd("m",1,recorddate)
		elseif objrs("recordhazrisk") = "High Risk" then
		high = "true"
		reviewdate = recorddate
		end if
							
		act.writeLine("<TR><td><br><font face='verdana' size='1'><center> Hazard review date = <b>" & reviewdate & "</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To be actioned by : <b>" & objrs("recordhazowner") & "</b></td></tr>")
		act.writeLine("</table><hr>")
							
		objrs.movenext
		wend
		
		If low = "true" then
		totalrisk = "Low Risk"
		totaladvice = "Monitor this Hazard for any changes in the future, no immediate action required."
		end if

		If medium = "true" then
		totalrisk = "Medium Risk"
		totaladvice = "Introduce Control Measures to lower hazard and re-assess process."
		end if

		If high = "true" then
		totalrisk = "High Risk"
		totaladvice = "STOP WORK!!, Review method of work and make changes to reduce risk or seek further advice from Safe and Sound on 01525 290922."
		end if
		
		act.writeLine("<br><center><font face='verdana' size='2'><b>Highest Residual Risk : </b>" & totalrisk & "<br><b>Advice:</b> " & totaladvice & "</center><br>")
		act.writeLine("<br><br></td></tr></table></form></body></html>")

act.close
%>
<html>

<!--
Start of Page Heading
-->
<head>
<!--
==========================================================
 �2001-2002 SAFE AND SOUND MANAGEMENT CONSULTANTS LIMITED 
 All rights reserved.
==========================================================
-->

<!--
Start of Styles
-->
<style>
a:link { TEXT-DECORATION: underline; COLOR: #EFA303; }
a:visited { TEXT-DECORATION: underline; COLOR: #EFA303; }
a:hover { TEXT-DECORATION: underline; COLOR: #5A8CCE; }
</style>
<!--
End of Styles
-->

<meta name="GENERATOR" content="Microsoft FrontPage 5.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

</head>
<body link="#EFA303" vlink="#EFA303" alink="#EFA303" bgcolor="#FFFFFF" topmargin="0">
<!--
End of Page Heading
-->

<!-- #INCLUDE FILE="../../SSI_scripts/main_header.asp" -->

<!--
Start of I Want to...
-->
<font face="Arial Black" size="5" color="#5A8CCE">Creating Word Report.</font><font face="Arial Black" size="5" color="#EFA303">.</font><font face="Arial Black" size="5" color="#5A8CCE">.</font><font face="Arial Black" size="5" color="#EFA303">.</font>


      <p align="center">
      <font face="Verdana" size="1">
      <br>
      <br>
      You document has been created for reference number - <b><% =recordreference %><br>
      <br>
      </b></font><b><font face="Verdana" size="4"><a href="../my_documents><% =recordreference %>.doc">Download</a></font></b>
      <br>
      <br>
&nbsp;<hr color="#C0C0C0" size="1">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
  <tr>
    <td width="100%"><font face="Arial" size="2">You are here - </font>
    <font face="Arial" size="2">You are here -
    <font color="#5A8CCE"><a href="../../my_account/my_departments/health_safety">
    <font color="#5A8CCE">Health and Safety</font></a></font> / <u> 
    <font color="#5A8CCE"><a href="../../modules/hsafety/risk_assessment/default.asp"><font color="#5A8CCE">
    <a href="../../modules/hsafety/risk_assessment"><font color="#5A8CCE">Risk 
    Assessment</font></a></font></a></font></u><font color="#EFA303"> </font>/<font color="#EFA303"> </font>
    <font color="#5A8CCE">Create Risk Assessment </font>
    / <font color="#EFA303">Word Report</font></font></td>
  </tr>
</table>
<!--
End of Navigation
-->

<!--
Start of About
-->
<hr color="#C0C0C0" size="1">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
  <tr>
    <td width="100%">
    <p align="right"><font face="Verdana" size="1"><b>AssessNET</b> is Provided 
    and Maintained by <b>Safe and Sound</b> Management Consultants LTD.</font></p>
    </td>
  </tr>
</table>
</div>
<!--
End of About
-->

</body>

</html>

