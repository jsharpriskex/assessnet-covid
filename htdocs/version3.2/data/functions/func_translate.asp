﻿<%

 '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
 '
 ' Last Update of this function was 15/04/2015
 '
 ' Translate function to work as close to .NET method as possible, using .NET resource files.
 ' -------------------------------------------------------------------------------------------
 ' HOW TO USE
 '  
 ' Include this function script in with your ASP page.
 ' If required, create a Resource file in App_GlobalResources or modify with the required labels (will need this done in all language packs)
 ' First you need to load up the resource file else nothing will come back. You do this by calling ResourcesFile("NAME OF RESOURCE FILE") do not worry about file extention. 
 ' To get the required language, you use the TRANS("PHRASE/WORD LABEL IN RESOURCE FILE").  The Labels are always in english.
 ' If nothing is coming back, then check you have the name of the resource file correct, the resource file is in App_GlobalResources (root) and it contains that label
 ' You can edit the resource files using the built in visual studio editor, this will not cause an issue.  The Resource files should then be 100% compatiable with .NET
 ' To swap languages at any time, you just need to re-call the ResourcesFile() with the desired language pack when you re-load the page.
 '
 ' Example Code
 ' ------------
 '
 ' ResourcesFile("en-german")
 ' Response.write "<p>" &  Trans("Hello World") & "</p>"
 ' * This will write out "Hallo Welt" on the page
 '
 '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

 Dim xmllangpack
 Function ResourcesFile(ResourceFile)
  Set xmllangpack = Server.CreateObject("Microsoft.XMLDOM") 
  xmllangpack.async=false 
  xmllangpack.load(Server.MapPath("App_GlobalResources\" & ResourceFile & ".resx"))
 End Function
 
 Function Trans(english)
  Set objLst = xmllangpack.getElementsByTagName("data")
  For i = 0 to (objLst.length -1)
        EnglishText = objLst.item(i).attributes.getNamedItem("name").text
        If english=EnglishText Then
        Trans= objLst.item(i).text
        Exit For
        End If
  Next
 End Function

%>
