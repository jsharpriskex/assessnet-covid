<!-- #INCLUDE FILE="../scripts/inc/dbconnections.inc" -->
<%
 
DIM Objrs
DIM Objconn
DIM Objcommand

var_reference = Request("Ref")
var_acc = Request("Acc")


IF Request("Cmd") = "ArcOK" Then

CALL startConnections()

SUB RemoveTASK(var_reference)
        ' Task Manager
        Objcommand.commandtext = "SELECT ID FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND Task_mod_ref = '" & var_reference & "'"
        SET Objrs = Objcommand.execute
        
        IF NOT Objrs.eof Then
        
        ' Grab ID so it can remove notes and history
        var_taskid = Objrs("ID")
        
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND Task_mod_ref = '" & var_reference & "'"
        Objcommand.execute
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_TASK_HISTORY") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND task_id = '" & var_taskid & "'"
        Objcommand.execute
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_TASK_NOTE") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND task_id = '" & var_taskid & "'"
        Objcommand.execute
        
        END IF
END SUB

SUB RemoveRA(var_reference)
        ' Risk Assessment Module
        
        ' Entries
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_RAMOD_DATA") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' Persons affected
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_RAMOD_PERSONS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Hazs
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_RAMOD_HAZARDS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        
END SUB

SUB RemoveMH(var_reference)
        ' Manual Handling Module
        
        ' Entries
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_MHMOD_DATA") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' Persons affected
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_MHMOD_PERSONS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Notes
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_MHMOD_NOTES") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        
END SUB

SUB RemoveFS(var_reference)
        ' Fire Safety Module
        
        ' Entries
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_DATA") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' Zones
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_ZONES") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Materials
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_MATERIAL") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Ignition
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_IGNITION") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Oxygen
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_OXY") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Alarm
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_ALARM") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Equipment
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_EQUIP") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Persons
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_PERSONS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
         ' Means of escape
        Objcommand.commandtext = "UPDATE " &  Application("DBTABLE_FSMOD_MOE") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
         ' Answers
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_ANSWERS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
         ' Actions
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_FSMOD_ACTIONS") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & var_reference & "'"
        Objcommand.execute
        
END SUB

SUB RemoveSA(var_reference)
        ' Safety Audit Module
        
        ' Entries
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_SA_AUDIT_ENTRIES") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND AUDIT_REF = '" & var_reference & "'"
        Objcommand.execute 
        ' Data
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_SA_AUDIT_DATA") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND AUDIT_REF = '" & var_reference & "'"
        Objcommand.execute
        
END SUB

SUB ArchiveCHECK(var_DSEUSERACC_ID)

	Objcommand.commandtext = "SELECT ID, RECORDREFERENCE FROM " & Application("DBTABLE_DSE_DATA") & " WHERE STATUS > 4 AND Corp_Code = '" & Session("CORP_CODE") & "' AND DSEUSERACC_ID = '" & var_DSEUSERACC_ID & "' ORDER BY ID DESC"
	SET Objrs = Objcommand.execute
	
	IF NOT Objrs.EOF Then
	
		var_arc_ID = Objrs("ID")
		var_arc_recordreference = Objrs("recordreference")
		
	' Date file
	Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_ARCDSE_DATA") & " (CORP_CODE,RECORDREFERENCE,DSEUSERNAME,DSEMAKEANDMODEL,DSEUSERJOBTITLE,DSEEMPLOYEEID,DSEPOSITION,DSEMOUNTMAX,DSEILLHEALTHOTHER,STATUS,HOSTNAME,RECORDDATE,RECORDCREATOR,RECORDSCORE,ACCOUNTID,DSECAMP_ID,DSEUSERACC_ID,DSEREVIEWEDDATE,DSEREVIEWEDBY,DSESUMMARY,REC_BG,REC_BU,REC_BL,REC_BD,ARC_DATETIME,ARC_BY) " & _
								"SELECT CORP_CODE,RECORDREFERENCE,DSEUSERNAME,DSEMAKEANDMODEL,DSEUSERJOBTITLE,DSEEMPLOYEEID,DSEPOSITION,DSEMOUNTMAX,DSEILLHEALTHOTHER,STATUS,HOSTNAME,RECORDDATE,RECORDCREATOR,RECORDSCORE,ACCOUNTID,DSECAMP_ID,DSEUSERACC_ID,DSEREVIEWEDDATE,DSEREVIEWEDBY,DSESUMMARY,REC_BG,REC_BU,REC_BL,REC_BD,'" & Date() & " " & Time() & "','" & Session("YOUR_ACCOUNT_ID") & "' FROM " & Application("DBTABLE_DSE_DATA") & " " & _
								"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND DSEUSERACC_ID = '" & var_DSEUSERACC_ID & "' AND ID='" & var_arc_id & "' AND recordreference = '" & var_arc_recordreference & "' "
	objcommand.execute
	
	' Result file
	Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_ARCDSE_RESULTS") & " (CORP_CODE,RECORDREFERENCE,DSERESULT,DSEREMOVES,DSECOMMENTS,DSECOMMENTS_DATE) " & _
								"SELECT CORP_CODE,RECORDREFERENCE,DSERESULT,DSEREMOVES,DSECOMMENTS,DSECOMMENTS_DATE FROM " & Application("DBTABLE_DSE_RESULTS") & " " & _
								"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND recordreference = '" & var_arc_recordreference & "' "
	Objcommand.execute
	
	' Action file
	Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_ARCDSE_ACTIONS") & " (CORP_CODE,RECORDREFERENCE,TASKID,HOSTNAME,COMPANYNAME,DSEACTIONMANAGERDESCRIPTION,DSEACTIONSTATUS,DSEEMEMO,DSEEMAIL,LASTMODIFIED,LASTMODIFIEDBY,ACTIONEDUSER) " & _
								"SELECT CORP_CODE,RECORDREFERENCE,TASKID,HOSTNAME,COMPANYNAME,DSEACTIONMANAGERDESCRIPTION,DSEACTIONSTATUS,DSEEMEMO,DSEEMAIL,LASTMODIFIED,LASTMODIFIEDBY,ACTIONEDUSER FROM " & Application("DBTABLE_DSE_ACTIONS") & " " & _
								"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND recordreference = '" & var_arc_recordreference & "' "
	Objcommand.execute
	
	
	' abandon old records from database
	Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_DSE_DATA") & " SET CORP_CODE = Corp_code + 'A' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND DSEUSERACC_ID = '" & var_DSEUSERACC_ID & "' AND recordreference = '" & var_arc_recordreference & "' "
	Objcommand.execute
	
	Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_DSE_RESULTS") & " SET CORP_CODE = Corp_code + 'A' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND recordreference = '" & var_arc_recordreference & "' "
	Objcommand.execute
	
	Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_DSE_ACTIONS") & " SET CORP_CODE = Corp_code + 'A' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND recordreference = '" & var_arc_recordreference & "' "
	Objcommand.execute
	
	
	END IF

END SUB

SUB RemoveUSER(var_acc)

    Objcommand.commandtext =    "UPDATE " & Application("DBTABLE_USER_DATA") & " SET CORP_CODE = corp_code + 'A' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Acc_Ref = '" & var_acc & "' "
    Objcommand.execute

END SUB

SUB RemovePTW(var_reference)
        ' Permit to Work Module
        
        ' Info
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_PERMIT_INFO") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORD_REFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' General
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_PERMIT_GENERAL") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORD_REFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' Elec
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_PERMIT_ELEC") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORD_REFERENCE = '" & var_reference & "'"
        Objcommand.execute
        ' Hot
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_PERMIT_HOT") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORD_REFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        ' Space
        Objcommand.commandtext = "UPDATE " & Application("DBTABLE_PERMIT_SPACE") & " SET CORP_CODE = '" & Session("CORP_CODE") & "A' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND RECORD_REFERENCE = '" & var_reference & "'"
        Objcommand.execute 
        
END SUB


    IF (Len(var_reference) <> 0) OR (Len(var_acc) <> 0) Then
    
    SELECT Case Request("Mod")
        CASE "Ra"
        RemoveRA(var_reference)
        RemoveTASK(var_reference)
        
            IF Request("Page") = "Srch" Then
                var_redirect = "../../modules/hsafety/risk_assessment/search/"
            ELSEIF Request("Page") = "View" Then
                var_redirect = "../../modules/acentre/acentre_briefcase.asp?show=Risk"                
            END IF
            
        CASE "Mh"
        RemoveMH(var_reference)
        RemoveTASK(var_reference)
        
            IF Request("Page") = "Srch" Then
                var_redirect = "../../modules/hsafety/manual_handling/search/"
            ELSEIF Request("Page") = "View" Then
                var_redirect = "../../modules/acentre/acentre_briefcase.asp?show=Mhandling"                
            END IF
        
        CASE "Fs"
        RemoveFS(var_reference)
        RemoveTASK(var_reference)
        
            IF Request("Page") = "Srch" Then
                var_redirect = "../../modules/hsafety/fire_safety/search/"
            ELSEIF Request("Page") = "View" Then
                var_redirect = "../../modules/acentre/acentre_briefcase.asp?show=FireSafety"                
            END IF
        
        CASE "Sa"
        RemoveSA(var_reference)
        RemoveTASK(var_reference)
        
            IF Request("Page") = "Srch" Then
                var_redirect = "../../modules/hsafety/safety_audit/search/search_audit.asp"
            ELSEIF Request("Page") = "View" Then
                var_redirect = "../../modules/acentre/acentre_briefcase.asp?show=Audit"                
            END IF
        
        CASE "Dse"
        
        ArchiveCHECK(var_acc)
        RemoveUSER(var_acc)
        
            IF Request("Page") = "Passport" Then
                var_redirect = "../../modules/hsafety/dse_assessment/manage/dse_manage.asp"
            END IF
        
        CASE "Ptw"
        RemovePTW(var_reference)
        
            IF Request("Page") = "Srch" Then
                var_redirect = "../../modules/hsafety/permit_to_work/search/"
            END IF
  
    END SELECT
    
    END IF

' Back to page
Response.Redirect(var_redirect)

CALL endConnections()

END IF

IF Request("Mod") = "Dse" Then
Alert_msg = "Are you sure you want to remove and archive " & request("name") & "'s DSE Account and records ?"
    Else
    Alert_msg = "Are you sure you want to remove record and place it in the archive (" & request("ref") & ") ?"
End IF


%>

<HTML>
<BODY>

<script type="text/javascript">
var name = confirm("<% =Alert_msg %>")
if (name == true)
{
parent.main.location.href=("func_commands.asp?Page=<% =Request("Page") %>&Cmd=ArcOK&Mod=<% =Request("Mod") %>&Ref=<% =Request("Ref") %>&Acc=<% =Request("Acc") %>")
}
else
{
history.back()
}
</script>

</BODY>
</HTML>