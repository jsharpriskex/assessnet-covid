<!-- #INCLUDE FILE="../scripts/inc/auto_logout.inc" -->
<% 

	' Make Objects needed for this script
	
	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH
	
	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn

If request.querystring("CMD") = "TRANSFER" then

	SET objCommand2 = Server.CreateObject("ADODB.Command")
	SET objCommand2.ActiveConnection = objconn

   	' Get current Record Header information
   	
   		objcommand.commandtext = "SELECT Per_cname FROM " & Application("DBTABLE_USER_DATA") & _
   								 " WHERE Acc_Ref='" & request.form("Acc_Ref") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   		SET objrs = objcommand.execute
   		
   		If objrs.eof then
   		'error
   		else
   		Per_cname = objrs("Per_cname")
   		Acc_Ref = request.form("Acc_Ref")
   		End IF
   	
	If request.form("transferSTATUS") = "ON" then
	
		' Find a new Record Number
   	
		objcommand.commandtext = "SELECT corp_licence_value FROM " & Application("DBTABLE_MODULES_DATA") & _
								 " WHERE corp_licence_type='RA' AND CORP_CODE='" & Session("CORP_CODE") & "'" 
								 
		Set objRS = objCommand.Execute	
		recordreference = objrs("corp_licence_value") + 1
		
		' Add the new Number

		objcommand.commandtext = "UPDATE " & Application("DBTABLE_MODULES_DATA") & " SET corp_licence_value=" & recordreference & _ 
		                         " WHERE corp_licence_type='RA' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                          
		objcommand.execute
		recordreference = recordreference & "RA"
	
  		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_DATA") & _
   								 " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   								 
   		SET objrs = objcommand.execute
   	
   		hostname = Session("YOUR_IP_ADDRESS")
   		status = "2"
   		recorddate = date
   		recordtime = time
   		recordtype = objrs("recordtype")
   		accountid = Acc_Ref
   		recordrevision = "0"
   		recordcreator = Per_Cname
   		recordequipment = replace(objrs("recordequipment"),"'","''")
   		recorddescription = replace(objrs("recorddescription"),"'","''")
   		recordrisk = replace(objrs("recordrisk"),"'","''")
   		recordgenerated = date
		recordriskscore = objrs("recordriskscore")
		recordgenerated = date
		recordtotalaffected = objrs("recordtotalaffected")
		recordlastmodified = Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME")
		rec_BG = objrs("rec_BG")
		rec_BU = objrs("rec_BU")
		rec_BL = objrs("rec_BL")
		rec_BD = objrs("rec_BD")
   				
	' Insert Information into new Row with new Record Number
	
		objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_DATA") & _
		                         " (corp_code,accountid,hostname,status,recorddate,recordtime,recordtype,recordrevision," & _
		                         "recordcreator,recordequipment,recorddescription,recordrisk,recordreference," & _
		                         "recordriskscore,recordgenerated,recordtotalaffected,recordlastmodified,rec_BG" & _
		                         ",rec_BU,rec_BL,rec_BD) VALUES ('" & Session("CORP_CODE") & "','" & accountid & "','" & hostname & "','" & status & _
		                         "','" & recorddate & "','" & recordtime & "','" & recordtype & "','0','" & recordcreator & _
		                         "','" & recordequipment & "','" & recorddescription & "','" & recordrisk & _
		                         "','" & recordreference & "','" & recordriskscore & "','" & recordgenerated & _
		                         "','" & recordtotalaffected & "','" & recordlastmodified & "','" & rec_BG & "','" & rec_BU & _
		                         "','" & rec_BL & "','" & rec_BD & "') "
		                         
		objcommand.execute
		
	' Select all person affected for that Record
	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_PERSONS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
	' Add each one to a new row as they are found
	
		If objrs.eof then
			else
			While NOT objrs.EOF
   				objcommand2.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_PERSONS") & _
   				                          " (corp_code,recordpersons,recordpersonsamount,recordreference,rec_BG,rec_BU,rec_BL,rec_BD)" & _
   				                          " VALUES ('" & Session("CORP_CODE") & "','" & objrs("recordpersons") & "','" & objrs("recordpersonsamount") & _
   				                          "','" & recordreference & "','" & rec_BG & "','" & rec_BU & "','" & rec_BL & _
   				                          "','" & rec_BD & "') "
   				                          
				objcommand2.execute
				objrs.movenext
			wend
		end if
	
	' Select all hazards for that Record
	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_HAZARDS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
	' Add each one to a new row as they are found
		
		If objrs.eof then
			else
			While NOT objrs.EOF
   				objcommand2.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_HAZARDS") & _
   				                          " (corp_code,recordreference,recordhaztype,recordhazdescription,recordhazcontrols," & _
   				                          "recordhazseverity,recordhazlikelihood,recordhazscore,recordhazcontrolsreq," & _
   				                          "recordhazseverityreq,recordhazlikelihoodreq,recordhazscorereq,recordhazowner," & _
   				                          "recordhazrisk,recordhazriskreq,rec_BG,rec_BU,rec_BL,rec_BD) " & _
   				                          "VALUES ('" & Session("CORP_CODE") & "','" & recordreference & "','" & objrs("recordhaztype") & _
   				                          "','" & objrs("recordhazdescription") & "','" & objrs("recordhazcontrols") & _
   				                          "','" & objrs("recordhazseverity") & "','" & objrs("recordhazlikelihood") & _
   				                          "','" & objrs("recordhazscore") & "','" & objrs("recordhazcontrolsreq") & _
   				                          "','" & objrs("recordhazseverityreq") & "','" & objrs("recordhazlikelihoodreq") & _
   				                          "','" & objrs("recordhazscorereq") & "','Not Actioned','" & objrs("recordhazrisk") & "','" & objrs("recordhazriskreq") & "','" & rec_BG & _
   				                          "','" & rec_BU & "','" & rec_BL & "','" & rec_BD & "') "
   				                          
				objcommand2.execute
				objrs.movenext
			wend
		end if
	
	IF Session("MODULE_RA") = "PRO" Then
	
	' Select all actions for that Record
	
		objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_ACTIONS") & _
		                         " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
		                         
		SET objrs = objcommand.execute
		
	' Add each one to a new row as they are found
		
		If objrs.eof then
			else
			While NOT objrs.EOF
   				objcommand2.commandtext = "INSERT INTO " & Application("DBTABLE_RAMOD_ACTIONS") & _
   				                          " (corp_code,recordreference,actioned,actiondate,hazid,task,likelihood,severity,risk,accountid,rec_BG,rec_BU,rec_BL,rec_BD) " & _
   				                          "VALUES ('" & Session("CORP_CODE") & "','" & objrs("recordreference") & "','" & objrs("actioned") & "','" & objrs("actiondate") & "','" & objrs("hazid") & "','" & objrs("task") & "','" & objrs("likelihood") & "','" & objrs("severity") & "','" & objrs("risk") & "','" & objrs("accountid") & "','" & rec_BG & "','" & rec_BU & "','" & rec_BL & "','" & rec_BD & "') "
   				                          
				objcommand2.execute
				objrs.movenext
			wend
		end if
		
	END IF
	
	objrs.close
	
	ELSE
   	
      	recordlastmodified = Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME")
      	accountid = Acc_Ref
	
	' Insert Information into new Row with new Record Number
	
		objcommand.commandtext = "UPDATE " & Application("DBTABLE_RAMOD_DATA") & " SET status='4', accountid='" & accountid & "', recordlastmodified='" & recordlastmodified & "' WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"                    
		objcommand.execute
		
		objcommand.commandtext = "UPDATE " & Application("DBTABLE_RAMOD_DATA") & " SET accountid='" & accountid & "' WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"                    
		objcommand.execute

	End IF
	
	' Kill objects
	
	
	objCommand.ActiveConnection.close
	
	SET objCommand.ActiveConnection = NOTHING
	SET objcommand2 = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING

	redirectURL = "../../modules/acentre/acentre_briefcase.asp?show=" & request.querystring("show")
	response.redirect(redirectURL)

END IF

	
objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_RAMOD_DATA") & " WHERE recordreference = '" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
SET objrs = objcommand.execute
recordgroup = objrs("Rec_BG")
recordcompany = objrs("Rec_BU")
recorddate = objrs("recorddate")
recordrevision = objrs("recordrevision")
recordcreator = objrs("recordcreator")
recordgenerated = objrs("recordgenerated")
recorddepartment = objrs("Rec_BD")
recordlocation = objrs("Rec_BL")
recordequipment = objrs("recordequipment")
recordlastmodified = objrs("recordlastmodified")


%>
<html>

<!--
Start of Page Heading
-->
<head>
<!--
==========================================================
 �2001-2002 SAFE AND SOUND MANAGEMENT CONSULTANTS LIMITED 
 All rights reserved.
==========================================================
-->

<!--
Start of Styles
-->
<style>
a:link { TEXT-DECORATION: underline; COLOR: #000000; }
a:visited { TEXT-DECORATION: underline; COLOR: #000000; }
a:hover { TEXT-DECORATION: underline; COLOR: #CC0000; }
</style>
<!--
End of Styles
-->


<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

</head>
<body link="#EFA303" vlink="#EFA303" alink="#EFA303" bgcolor="#FFFFFF" topmargin="0">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td>
<hr color="#808080" size="1">
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%">
  <tr>
    <td align="right">
      <p align="left"><font face="Arial" size="2"><b>Lost?... </b>you are in <b>::</b> 
      <a href="../../modules/acentre/">
    <span style="text-decoration: none"><font color="#CC0000">Your Account
    Centre</font></span></a></font></p>
    </td>
  </tr>
</table>
<!--
End of Navigation
-->

<!--
Start of About
-->
<hr color="#808080" size="1">
    </td>
    <td width="220"><img border="0" src="../../images/icons/ass_title.gif" align="right"></td>
  </tr>
</table>
<!--
End of Page Heading
-->


<!--
Start of I Want to...
--><font face="Arial" size="5"><font color="#CC0000"><br>
Transfer Record to another user</font>....</font><font color="#CC0000"><font face="Arial" size="5"><br>
</font><font face="Verdana" size="1" color="#EFA303">&nbsp;</font><br>

</font>

      <font face="Arial" size="2">
      Risk assessment report details for <b>reference</b> number&nbsp; :
      (</font><font color="#CC0000" face="Arial"> <b> <% =request.querystring("code") %>
 </b>
</font>

      <font face="Arial" size="2">
)</font><font color="#CC0000"><b><font face="Verdana" size="1"><br>
      &nbsp; </font></b><table border="0" width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td width="73%" style="border-left: 1 solid #000000; border-top: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Company Information</font></b></td>
          <td width="13%" style="border: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Date</font></b></td>
          <td width="14%" style="border-right: 1 solid #000000; border-top: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Revision</font></b></td>
        </tr>
        <tr>
          <td width="73%" style="border-left: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordgroup %> :: <% =recordcompany %></td>
          <td width="13%" style="border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recorddate %></td>
          <td width="14%" style="border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordrevision %></font></td>
        </tr>
      </table>
      <table border="0" width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td width="34%" style="border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Assessed 
          by</font></b></td>
          <td width="47%" style="border-right: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Last
            modified by</font></b></td>
          <td width="19%" style="border-right: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Date Revised</font></b></td>
        </tr>
        <tr>
          <td width="34%" style="border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordcreator %></td>
          <td width="47%" style="border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordlastmodified %></td>
          <td width="19%" style="border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordgenerated %></td>
        </tr>
      </table>
      <table border="0" width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td width="50%" style="border-left: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Department
            Assessed</font></b></td>
          <td width="50%" style="border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000" bgcolor="#FFEEDD"><b><font face="Verdana" size="1">Location
            Assessed</font></b></td>
        </tr>
        <tr>
          <td width="50%" height="17" style="border-left: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recorddepartment %></td>
          <td width="50%" height="17" style="border-left: 1 solid #000000; border-right: 1 solid #000000; border-bottom: 1 solid #000000"><font face="Verdana" size="1"><% =recordlocation %></td>
        </tr>
      </table>
</font>
<!--webbot BOT="GeneratedScript" PREVIEW=" " startspan --><script Language="JavaScript"><!--
function FrontPage_Form1_Validator(theForm)
{

  if (theForm.Acc_Ref.selectedIndex == 0)
  {
    alert("The first \"Select User to Transfer to\" option is not a valid selection.  Please choose one of the other options.");
    theForm.Acc_Ref.focus();
    return (false);
  }
  return (true);
}
//--></script><!--webbot BOT="GeneratedScript" endspan --><form action="func_transfer.asp?CMD=TRANSFER&code=<% =request.querystring("code") %>&show=Risk" Method=post onsubmit="return FrontPage_Form1_Validator(this)" name="FrontPage_Form1" language="JavaScript">      
<b><font face="Arial" size="2">Transfer </font>
</b> 
<font face="Arial" size="2"> 
this record to : </font>
<font color="#CC0000"><font face="Arial" size="2"> </font>
    <font face="Verdana" size="1">
    
  <!--webbot bot="Validation" s-display-name="Select User to Transfer to"
  b-disallow-first-item="TRUE" --><SELECT name='Acc_Ref' style="border: 1px solid #5A8CCE">
  <%
	
	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_BU,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE Corp_BG='" & Session("YOUR_GROUP") & "' AND CORP_CODE='" & Session("CORP_CODE") & "' AND Acc_level NOT LIKE '5' ORDER BY Per_sname ASC"
	SET objrs = objcommand.Execute
	
	response.write "<OPTION value='Select User'>Select User</OPTION>"
	
   	While NOT objrs.EOF
   	
   	response.write "<OPTION value='" & objrs("Acc_Ref") & "'>" & objrs("Per_sname") & ", " & objrs("Per_fname") & " :: (<i>" & objrs("Corp_BU") & "</i>) - " & objrs("Corp_BD") & "</option>" 
   	
   	objrs.movenext
   	
	wend 
	
	objrs.close
	objCommand.ActiveConnection.close

	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING  
  
  %>
  </SELECT></font><br>
<font face="Verdana" size="2">&nbsp;</font><table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
  <tr>
    <td width="82%">
<font face="Arial"><font size="2">
<img border="0" src="../../images/icons/icon4.gif"> Transfer only a <b>copy</b> to the user : </font>
<input type="checkbox" name="transferSTATUS" value="ON" checked><font size="2"><br>
(<b>Note :</b>
</font></font>
    </font>
<font face="Arial" size="2" color="#CC0000">De-selecting this option will transfer full ownership and 
reference number to the above user</font><font face="Arial" size="2">)</font></td>
  <font color="#CC0000">
    <td width="18%" align="center">
    <input type="submit" value="Transfer" name="submit" style="border: 1px solid #000000; padding-left: 4; padding-right: 4; padding-top: 1; padding-bottom: 1; background-color: #FFEECC"></td>
  </tr>
</table>
</form>
<br>
</body>
</html>