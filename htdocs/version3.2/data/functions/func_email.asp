<%
' -------------------------------------------
'  Sub to send email
'
'  Rewritten to use CDO instead of CDONTS
' -------------------------------------------

Sub sendEmail(emailTo,emailFrom,emailSubject,emailBody)

    Dim objSend
    Set objSend = Server.CreateObject("CDO.Message")

    if len(emailFrom) = 0 then
        emailFrom = "notifications@assessnet.co.uk"
    end if

    ' PREFERENCE: Default from e-mail address, set in preferences (if any)
    if len(session("pref_email_defaultfrom")) > 5 then
        emailFrom = session("pref_email_defaultfrom")
    end if

    emailTo = replace(emailTo, chr(39), "'")
    emailTo = replace(emailTo, chr(96), "'")
    emailTo = replace(emailTo, "''", "'")

    objSend.MimeFormatted = True
    objSend.BodyPart.Charset = "utf-8"
    objSend.Fields("urn:schemas:mailheader:priority").Value = 2

    objSend.To = emailTo
    objSend.BCC = "mailcatcher@assessnet.co.uk"
    objSend.From = emailFrom
    
    objSend.Subject = emailSubject
    objSend.HtmlBody = emailBody

    'Name or IP of remote SMTP server
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "localhost"
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
    'Server port
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 
    objSend.Configuration.Fields.Update

    'response.write emailTo & " " & emailFrom
    'response.end
    objSend.Send

    set objSend = nothing  



end sub

' -------------------------------------------
'  Sub to send email
'
'  Using this will allow us to switch to CDO
'  instead of CDONTS when the time comes
' -------------------------------------------

'Sub sendEmail(emailTo,emailFrom,emailSubject,emailBody)
'  
'    Dim objSend
'    Set objSend = Server.CreateObject("CDONTS.NewMail")
'    if len(emailFrom) = 0 then
'        emailFrom = "notifications@assessnet.co.uk"
'    end if
'    
'
'        ' PREFERENCE: Default from e-mail address, set in preferences (if any)
'        if len(session("pref_email_defaultfrom")) > 5 then
'            emailFrom = session("pref_email_defaultfrom")
'        end if
'    
'        ' PREFERENCE: Should the email be branded?
'     '   if len(session("pref_email_branded") = "Y") then 
'        
'     '       var_logo_found = false
'     '       set fs = CreateObject("Scripting.FileSystemObject")
'        
'     '       if fs.FileExists(server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & ".jpg")) then
'     '           var_logo_str = "http://www.assessweb.co.uk:8080/version3.2/data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & ".jpg"
'     '           var_logo_found = true
'     '       end if
'
'
'     '       if var_logo_found then
'     '           'emailBody = "<html><body><div style='float:right;'><img src='" & var_logo_str & "'></div></body></html>" & emailBody
'     '       end if
'
'
'     '   end if
'
'    emailTo = replace(emailTo, chr(39), "'")
'    emailTo = replace(emailTo, chr(96), "'")
'
'    emailTo = replace(emailTo, "''", "'")
'
'    objSend.BodyFormat = 0 ' HTML
'    objSend.MailFormat = 0 '
'    objSend.Importance = 2
'    objSend.To = emailTo
'    objSend.BCC = "mailcatcher@assessnet.co.uk"
'    objSend.From = emailFrom
'    objSend.Subject = emailSubject
'    objSend.Body = emailBody
'
'    'response.write emailTo & " " & emailFrom
'    'response.end
'    objSend.Send
'    
'    SET objSend = Nothing
'End sub

Sub addEmailHistory(eventStatus, AccountID, Sender)
    If Len(eventStatus) > 0 AND Len(AccountID) > 0 AND Len(Sender) > 0 Then
        objCommand.commandText = "insert into " & Application("DBTABLE_LOG_EMAIL") & " (eventdatetime, eventstatus, accountid, sender, corp_code, eventprogress) " & _
                                  " values (getdate(), '" & eventstatus & "', '" & AccountID & "', '" & Sender & "', '" & Session("corp_code") & "' , 'SUCCESSFUL') "
        objcommand.execute
    End If
End Sub
%>