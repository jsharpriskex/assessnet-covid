<!-- #INCLUDE FILE="../scripts/inc/auto_logout.inc" -->
<!-- #INCLUDE FILE="../scripts/inc/dbconnections.inc" -->

<%
dim objrs
dim objconn
dim objcommand

CALL startconnections()

var_frm_mod = validateInput(request.QueryString("mod"))
var_frm_ref = validateInput(request.QueryString("ref"))
var_frm_sref = validateInput(request.QueryString("subref"))

var_frm_cmd = validateInput(request.QueryString("cmd"))
var_frm_cmdref = validateInput(request.QueryString("cmdref"))

var_frm_id = request.QueryString("currid")

if var_frm_cmd = "remove" then
    if len(var_frm_cmdref) > 0 then
        objCommand.commandText = "UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                 "SET corp_code = corp_code + 'A' " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND id = " & var_frm_cmdref
        objCommand.execute
    end if
end if

%>

<html>
<head>
	<title>Assessment Viewers - AssessNET.co.uk (Riskex Ltd) Copyright(c) <% =Year(date) %></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel='stylesheet' href="../../modules/hsafety/risk_assessment/app/default.css" media="all" />
    <script type='text/javascript' src='../data/scripts/js/global.js'></script>
</head>

<body>
<div id='Div1' class='optionsection' align='right'>
    <table width='100%'>
        <tr>
            <td class='l title'>Assessment <strong>Viewers</strong><br /><span class='info'>Check who has been notified to view the selected assessment</span></td>
         </tr>
    </table>
</div> 

<!-- Window style starts -->
<div id='windowtitle'>
<h5>Assessment Viewers</h5>
<div id='windowmenu'>
    <span><a href='javascript:window.close();'>Close this window</a></span>
</div>
    <!-- body of window -->
<div id='windowbody' class='pad'>
   <br />
    <div id='section_1' class='sectionarea'>
        
        <%


          objCommand.commandText = "SELECT tasks.id, users.per_fname, users.per_sname, tasks.task_status, tasks.task_due_date, tasks.viewed_ass_id, tasks.task_description, tasks.auto_complete, history.gendatetime as task_lastmodified " & _
                                     "FROM " & Application("DBTABLE_USER_DATA") & " AS users " & _
                                     "LEFT JOIN " & Application("DBTABLE_TASK_MANAGEMENT") & " AS tasks " & _
                                     "  ON users.corp_code = tasks.corp_code " & _
                                     "  AND users.acc_ref = tasks.task_account " & _
                                     "left join " & Application("DBTABLE_TASK_HISTORY") & " as history " & _
                                     "on tasks.corp_code = history.corp_code " & _
                                     " and tasks.id = history.task_id " & _
                                     "and (history.task_history like 'Task changed from % to Complete' OR history.task_history like 'Task automatically changed to complete %') " & _
                                     "WHERE users.corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND tasks.task_module = '" & var_frm_mod & "_V' " & _
                                     "  AND tasks.task_mod_ref = '" & var_frm_ref & "' " & _
                                     "ORDER BY tasks.task_due_date DESC"
' if lcase(session("your_username")) = "sysadmin.ikea" then response.write objcommand.commandtext
'
'            objCommand.commandText = "SELECT tasks.id, users.per_fname, users.per_sname, tasks.task_status, tasks.task_due_date, tasks.task_lastmodified, tasks.viewed_ass_id, tasks.task_description, tasks.auto_complete " & _
'                                     "FROM " & Application("DBTABLE_USER_DATA") & " AS users " & _
'                                     "LEFT JOIN " & Application("DBTABLE_TASK_MANAGEMENT") & " AS tasks " & _
'                                     "  ON users.corp_code = tasks.corp_code " & _
'                                     "  AND users.acc_ref = tasks.task_account " & _
'                                     "WHERE users.corp_code = '" & session("CORP_CODE") & "' " & _
'                                     "  AND tasks.task_module = '" & var_frm_mod & "_V' " & _
'                                     "  AND tasks.task_mod_ref = '" & var_frm_ref & "' " & _
'                                     "ORDER BY tasks.task_due_date DESC"
            set objRs = objCommand.execute
           
            if not objRs.EOF then
                response.Write "<table width='98%' cellpadding='2px' cellspacing='2px' class='showtd'>"
                
                response.Write "<tr><th>User</th><th width='75px'>To View By</th><th width='75px'>Viewed On</th><th width='50px'>Viewed</th></tr>"
                
                while not objRs.EOF 
                    var_remove = ""
                    var_row_colour = "#FFFFFF"
                    
                    if instr(1, objRs("task_description"), "has been requested to view") = 0 then
                        var_task_desc = "<br /><span class='info'>" & replace(objRs("task_description"),vbcrlf,"<br />") & "</span>"
                    else
                        var_task_desc = ""
                    end if
                    
                    if clng(objRs("viewed_ass_id")) <> clng(var_frm_id) then
                        var_row_colour = "#D8D8D8"
                        var_row_text = "This request to view was issued for a previous version of this risk assessment"
                    else
                        var_row_text = ""
                    end if

                    select case objRs("task_status")
                        case "0"
                            var_viewed_date = left(objRs("task_lastmodified"),10)
                            var_viewed = "<img src='../../modules/hsafety/risk_assessment/app/img/grn_tick.png' title='viewed on " & left(objRs("task_lastmodified"),10) & "' />"
                        
                        case "2"
                            if objRs("task_due_date") < now() then
                                var_row_colour = "#FFB9B9"
                                var_alttext = "viewing overdue"
                            else
                                var_alttext = "not viewed"
                            end if

                            var_viewed_date = "-"
                            var_viewed = "<img src='../../modules/hsafety/risk_assessment/app/img/red_cross.png' title='" & var_alttext & "' />"
                            var_remove = " [ <a href='func_assessment_viewers_popup.asp?mod=" & var_frm_mod & "&ref=" & var_frm_ref & "&subref=" & var_frm_sref & "&cmd=remove&cmdref=" & objRs("id") & "&currid=" & var_frm_id & "' title='remove this notification for this user'>remove</a> ]"
                        case else
                            var_viewed_date = "-"
                            var_viewed = "task<br />active"
                    end select
                    
                    response.Write "<tr title='" & var_row_text & "' style='background-color:" & var_row_colour & ";' onmouseover='this.style.backgroundColor = ""#FFF6D1""' onmouseout='this.style.backgroundColor = """ & var_row_colour & """'><td>" & objRs("per_fname") & " " & objRs("per_sname") & var_remove & var_task_desc & "</td><td class='c'>" & left(objRs("task_due_date"),10) & "</td><td class='c'>" & var_viewed_date & "</td><td class='c'>" & var_viewed & "</td></tr>"
                    objRs.movenext
                wend
            else
                response.Write "<div class='warning'><table width='100%'><tr><th width='50px'>&nbsp;</th><td><strong>Note:</strong><br />No users have been notified to view this assessment</td></tr></table></div>"
            end if
        
        %>
        
    </div>
</div>
</div>
</body>

</html>


<%
CALL endConnections()
%>