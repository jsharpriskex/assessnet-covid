<!-- #include file="../scripts/inc/dbconnections.inc" -->
<!-- #include file="../scripts/inc/sys_checkuser.asp" -->
<%

Dim objConn
Dim objCommand
Dim objRs

CALL startConnections()


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                             ''
'' SUB (errCode) instructions of use                           ''
'' This sub requires,                                          ''
''                    0001 - Invalid username or password      ''
''                    0002 - No modules for licence            ''
''                    0003 - Invalid IP for read-only          ''
''                    0004 - Account disabled                  ''
''                    0005 - Mem. info Failed after 3 attempts ''
''                    0006 - License expired                   ''
''                                                             ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub errCode(varERRnumber)
   LoginURL = "../Login/frm_lg_entry.asp?ERR=" & varERRnumber
   
   Session.Abandon
   Call EndConnections()
   
   'Error redirect
   Response.Redirect(LoginURL)
End sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''                                                        ''
	'' SUB (userLOG) instructions of use                      ''
	'' This sub requires,                                     ''
	''                    status (1 for success, 2 for fail)  ''
	''                    details (Reason for log entry)      ''
	''					  username (Username entered at login ''
	''                                                        ''
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	SUB useLOG(varSTATUSlog,varDETAILSlog,varUNAMElog)

	varDATElog = Date() & " " & Time()
	varHTTPlog = Request.ServerVariables("HTTPS")
	varIPlog = Request.ServerVariables("REMOTE_ADDR")

		Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_LOG_ACCESS") & " " & _
								  "(datetime,https,success,reason,username,ip_address) " & _
								  "VALUES('" & varDATElog & "','" & varHTTPlog & "','" & varSTATUSlog & "','" & varDETAILSlog & "','" & varUNAMElog & "','" & varIPlog & "') "
		Objcommand.execute

	END SUB


IF Request.QueryString("CMD") = "abandon" Then

	Session.Contents.Remove("SA_ADMIN")
	Session.Contents.Remove("SA_LOGIN")

End IF



IF (len(Session("SA_ADMIN")) > 0) and (Session("SA_LOGIN") = "Y") Then

	Objcommand.commandtext = "SELECT acc_name, acc_pass FROM " & Application("DBTABLE_USER_DATA") & " WHERE acc_ref = '" & Session("SA_ADMIN") & "' and sa_contracts IS NOT NULL"
	SET Objrs = Objcommand.execute

	IF Objrs.eof Then
	Session.Contents.Remove("SA_ADMIN")
	Session.Contents.Remove("SA_LOGIN")
	Response.Redirect("func_exit.asp")
		ELSE
		
		varConUser = Objrs("acc_name")
		varConPass = Objrs("acc_pass")
		
			Call checkUSER(varConUser,varConPass)
		
	END IF


%>
<html>
<script language="JavaScript">
<!--
function logout(){

var name = confirm("Would you like to login to another contract? If so click OK to continue, else CANCEL to exit.")
if (name == true)
{         
    parent.location.href="<% =Application("CONFIG_PROVIDER_SECURE") %>/version<% =Application("CONFIG_VERSION") %>/security/login/frm_lg_meminfo.asp"
} else {
	parent.location.href="func_exit.asp?CMD=abandon"
}

}
// -->
</script>
<%

ELSE

' Close Session object for this user, specified three times to ensure closure 
Session.Abandon
Session.Abandon
Session.Abandon

if request("cmd") = "dse_demo" then
    var_redirect = Application("CONFIG_PROVIDER") & "/dse_demo_link.html" 
elseif request("cmd") = "dse_link" then
    var_redirect = Application("CONFIG_PROVIDER") & "/dse.asp?t=" & validateinput(request("t")) & "&i=" &   validateinput(request("i")) 
else 
    var_redirect = Application("CONFIG_PROVIDER")
end if
%>
<html>
<script language="JavaScript">
<!--

function logout(){
          
    parent.location.href="<%=var_redirect %>"
}
// -->
</script>
<% 

END IF

CALL Endconnections()

%>

</head>
<body onLoad="javascript:logout()">
</body>
</html>