<!-- #INCLUDE FILE="../scripts/inc/dbconnections.inc" -->
<%

DIM str_RECORDREFERENCE
DIM str_URL
DIM str_ALERT

str_RECORDREFERENCE = Request.Querystring("REF")
str_URL = Request.Querystring("URL")

IF Len(str_RECORDREFERENCE) > 0 Then

DIM Objconn
DIM Objrs
DIM Objcommand

CALL StartCONNECTIONS()

	Objcommand.commandtext = 	"SELECT Count(ID) as ARC_COUNT FROM " & Application("DBTABLE_ARCMOD_RISK_DATA") & " WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "'"
	SET Objrs = Objcommand.execute		

	str_ARC_REFERENCE = Objrs("ARC_COUNT") + 1

		Objcommand.commandtext = 	"INSERT INTO " & Application("DBTABLE_ARCMOD_RISK_DATA") & " (Corp_Code, hostname, status, recorddate, recordtime, recordtype, accountid, recordrevision, recordcreator, Rec_intREF, recordreference, recordequipment, recorddescription, recordrisk, recordriskscore, recordgenerated, recordtotalaffected, recordlastmodified, Rec_BG, Rec_BU, Rec_BL, Rec_BD) " & _
									"SELECT Corp_Code, hostname, status, recorddate, recordtime, recordtype, accountid, recordrevision, recordcreator, Rec_intREF, recordreference, recordequipment, recorddescription, recordrisk, recordriskscore, recordgenerated, recordtotalaffected, recordlastmodified, Rec_BG, Rec_BU, Rec_BL, Rec_BD " & _
									"FROM " & Application("DBTABLE_RAMOD_DATA") & " " & _
									"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "'"
		Objcommand.execute
							
			Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_ARCMOD_RISK_DATA") & " SET ARC_DATETIME='" & Date() & "',ARC_REFERENCE='" & str_ARC_REFERENCE & "' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "' AND ARC_REFERENCE IS NULL"
			Objcommand.execute

		Objcommand.commandtext = 	"INSERT INTO " & Application("DBTABLE_ARCMOD_RISK_HAZARDS") & " (Corp_code, recordreference, recordhaztype, recordhazdescription, recordhazcontrols, recordhazseverity, recordhazlikelihood, recordhazscore, recordhazcontrolsreq, recordhazseverityreq, recordhazlikelihoodreq, recordhazscorereq, recordhazrevdate, recordhazowner, recordhazrisk, recordhazriskreq, Rec_BG, Rec_BU, Rec_BL, Rec_BD) " & _ 
									"SELECT Corp_code, recordreference, recordhaztype, recordhazdescription, recordhazcontrols, recordhazseverity, recordhazlikelihood, recordhazscore, recordhazcontrolsreq, recordhazseverityreq, recordhazlikelihoodreq, recordhazscorereq, recordhazrevdate, recordhazowner, recordhazrisk, recordhazriskreq, Rec_BG, Rec_BU, Rec_BL, Rec_BD " & _
									"FROM " & Application("DBTABLE_RAMOD_HAZARDS") & " " & _
									"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "'"
		Objcommand.execute
							
			Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_ARCMOD_RISK_HAZARDS") & " SET ARC_REFERENCE='" & str_ARC_REFERENCE & "' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "' AND ARC_REFERENCE IS NULL"
			Objcommand.execute

		Objcommand.commandtext = 	"INSERT INTO " & Application("DBTABLE_ARCMOD_RISK_PERSONS") & " (Corp_code, recordreference, recordpersons, recordpersonsamount, Rec_BG, Rec_BU, Rec_BL, Rec_BD) " & _
									"SELECT Corp_code, recordreference, recordpersons, recordpersonsamount, Rec_BG, Rec_BU, Rec_BL, Rec_BD " & _
									"FROM " & Application("DBTABLE_RAMOD_PERSONS") & " " & _
									"WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "'"							
		Objcommand.execute

			Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_ARCMOD_RISK_PERSONS") & " SET ARC_REFERENCE='" & str_ARC_REFERENCE & "' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "' AND ARC_REFERENCE IS NULL"
			Objcommand.execute

		Objcommand.commandtext =	"UPDATE " & Application("DBTABLE_RAMOD_DATA") & " SET ARC_STATUS='1' WHERE Corp_Code = '" & Session("CORP_CODE") & "' AND Recordreference = '" & str_RECORDREFERENCE & "'"
		Objcommand.execute

CALL endCONNECTIONS()

str_ALERT = "Record (" & str_RECORDREFERENCE & ") has been archived successfully for future reference"
	ELSE
	str_ALERT = "WARNING : No record was archived, error : unknown record"
END IF

%>
<HTML>
<HEAD>

<% IF Request.querystring("refer") = "RAstep4" Then %>

<SCRIPT LANGUAGE="javascript">
alert("<% =str_ALERT %>")
parent.main.location.href=("../../modules/hsafety/risk_assessment/standard/create_step_four.asp?ref=<% =str_RECORDREFERENCE %>&cmd=mkrec")
</SCRIPT>


<% elseIF Request.querystring("refer") = "RAstep1" Then %>

<SCRIPT LANGUAGE="javascript">
alert("<% =str_ALERT %>")
parent.main.location.href=("../../modules/hsafety/risk_assessment/standard/create_step_one.asp?cmd=chrec&ref=<% =str_RECORDREFERENCE %>")
</SCRIPT>

<% ELSE %>

<SCRIPT LANGUAGE="javascript">
alert("<% =str_ALERT %>")
parent.main.location.href=("../../modules/acentre/acentre_briefcase.asp?show=<% =request.querystring("show") %>&page=<% =Request.querystring("page") %>&bmk=<% =Request.querystring("bmk") %>&info=<% =Request.querystring("info") %>")
</SCRIPT>

<% END IF %>

</HEAD>
<BODY>

</BODY>
</HTML>