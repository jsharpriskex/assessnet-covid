<%

'*************************************************************************
'*  Function to show and process the users choice of who to send a task  *
'*  too, who should be notified to view the task                         *
'*************************************************************************



' display the form for selecting the users to send notification too
sub displayAssessmentViewersForm(var_ass_module, var_ass_reference, var_ass_subreference, current_page_name, querystring_text, var_current_record_id, var_ass_company, var_ass_location)
    ' var_ass_module: a reference to the module currently being used
    ' var_ass_reference: the reference number of the report being viewed
    ' var_ass_subreference: an optional value for any sub references for the current report
    ' current_page_name: the name of the current page being viewed. used when the form is submitted.
    ' querystring_text: the querystring to be passed to the form in the 'process assessment' sub routine.
    
    if session("DIST_LIST") = "Y" then

        ' process any view notifications that have been requested
        ' this allows only one call to be placed in the parent page making the integration quicker
        call processAssessmentViewersForm(var_ass_module, var_ass_reference, "", current_page_name, querystring_text, var_current_record_id)
        
        response.Write "<br />"
        response.Write "<div id='windowtitle'><h5>Distribution List</h5><div id='windowmenu'>&nbsp;<a name='sectionviewer' id='sectionviewer'>&nbsp;</a></div><div id='windowbody' class='pad'>"
        
        response.Write "<div id='section_viewer' class='sectionarea'>"

        response.Write "    <div class='warning'><table width='100%'><tr><td>Please select the users who should be notified to view this Assessment</td></tr></table></div>"
        
        response.Write "    <form name='frm_ass_viewer' action='" & current_page_name & "?" & querystring_text & "' method='post' onsubmit='return valViewerSelection(this)'>"
        

            response.Write "<table width='100%' cellpadding='2px' cellspacing='2px' class='showtd'>"
            
            if session("dist_list_custom") = "Y" then
                var_r1_checked = ""
                var_r2_checked = " checked "
                var_r2_disabled = ""
            else
                var_r1_checked = " checked "
                var_r2_checked = ""
                var_r2_disabled = " disabled "
            end if
            
            
            response.Write "<tr><th colspan='3'>Task Setup</th></tr>" & _
                           "<tr><td colspan='3'><input type='radio' class='radio' name='frm_viewer_taskoption' id='frm_viewer_taskoption_1' value='1' onclick='enableDisableTaskViewerOptions(this);' " & var_r1_checked & " /> <label for='frm_viewer_taskoption_1'>Standard AssessNET view Task</label> <span class='info'>(user will be requested to view this assessment and the task will complete automatically when viewed)</span><br />" & _
                           "                    <input type='radio' class='radio' name='frm_viewer_taskoption' id='frm_viewer_taskoption_2' value='2' onclick='enableDisableTaskViewerOptions(this);' " & var_r2_checked & " /> <label for='frm_viewer_taskoption_2'>Your own message</label> <span class='info'>(specify your own requirements for users viewing this assessment)</span><br />" & _
                           "                    <textarea class='textarea' name='frm_viewer_taskrequirement' id='frm_viewer_taskrequirement' rows='3' " & var_r2_disabled & "></textarea><br />" & _
                           "                    <input type='checkbox' class='checkbox' name='frm_viewer_autocomplete' id='frm_viewer_autocomplete' " & var_r2_disabled & "/> <label for='frm_viewer_autocomplete'>Automatically complete task when this assessment is viewed</label><br /><br /></td></tr>"
            
            response.Write "<tr><th>Available Users</th><th colspan='2'>Options</th></tr>"
            
            ' list of users
            response.Write "<tr><td width='70%' rowspan='2'>"
                if session("LOCALISED_USERS") = "Y" then
                    call DisplayUSERInfo_location("", "frm_viewer_users", "", "N", "", "N", "", "8", var_ass_company, var_ass_location, "", "", "", "")
                else
                    call DisplayUSERInfo("", "frm_viewer_users", "", "N", "", "N", "", "8")
                end if
            response.Write "<br /><span class='info'>Hold Ctrl key to select multiple users</span></td>"

            objCommand.commandText = "SELECT COUNT(id) AS num_selected FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND task_module = '" & var_ass_module & "_V' " & _
                                     "  AND task_mod_ref = '" & var_ass_reference & "'"
            set objRsV = objCommand.execute
            if objRsV("num_selected") = 0 then
                var_disabled = "disabled"
                var_tooltip = "There has been nobody assigned to view this assessment in the past"
                var_num_users = ""
            else
                var_disabled = ""
                var_tooltip = "Several users have previously been notified to view this assessment in the past"
                var_num_users = " (" & objRsV("num_selected") & ")"
            end if
            
            response.Write "<td colspan='2' class='c'><input type='button' class='submit' name='frm_viewer_selected' value='View Previously Selected Users" & var_num_users & "' title='" & var_tooltip & "' onclick='javascript:viewSELECTEDVIEWERS(""" & var_ass_module & """, """ & var_ass_reference & """, """ & var_ass_subreference & """, """ & var_current_record_id & """)' " & var_disabled & " /></td></tr>"
            
            response.Write "<tr><td class='c' nowrap>To be viewed by<br />"
                call autoDate("NONE", "frm_viewer_day", "frm_viewer_month", "frm_viewer_year","F")
            response.Write "</td><td class='c'><input type='submit' class='submit' name='frm_viewer_submit' value='Send Notification' /><br /><span class='info'>Add a task to the selected users</span></td></tr></table>"

        response.Write "    </form>"
    
    'Response.Write "<br />"
        
        response.Write "</div></div></div>"
    
    
    
    end if

end sub


' process the form submitted by the user
sub processAssessmentViewersForm(var_ass_module, var_ass_reference, var_ass_subreference, current_page_name, reference_var_name, var_current_record_id)
    
    if validateInput(request.Form("frm_viewer_submit")) = "Send Notification" then
        ' add tasks for the selected users to the notified to view the assessment
        
        var_viewer_viewby_date = validateInput(request.Form("frm_viewer_day")) & "/" & validateInput(request.Form("frm_viewer_month")) & "/" & validateInput(request.Form("frm_viewer_year"))
        if var_viewer_viewby_date = "DD/MM/YYYY" or var_viewer_viewby_date = "//" then
            var_viewer_viewby_date = now()
        end if
        
        var_viewer_taskoption = validateInput(request.Form("frm_viewer_taskoption"))
        var_viewer_taskrequirement = validateInput(request.Form("frm_viewer_taskrequirement"))
        var_viewer_autocomplete = validateInput(request.Form("frm_viewer_autocomplete"))
        if var_viewer_autocomplete = "on" then
            var_viewer_autocomplete = 1
        else
            var_viewer_autocomplete = 0 
        end if
        
        var_viewer_users = validateInput(request.Form("frm_viewer_users"))
        varUsers = split(var_viewer_users, ", ")


        objCommand.commandText = "SELECT rec_bu, rec_bl, rec_bd FROM " & Application("DBTABLE_RAMOD_DATA") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND recordreference = '" & var_ass_reference & "'"
        set objRsLoc = objCommand.execute
        if not objRsLoc.EOF then
            var_ass_tier2 = objRsLoc("rec_bu")
            var_ass_tier3 = objRsLoc("rec_bl")
            var_ass_tier4 = objRsLoc("rec_bd")
        end if
        set objRsLoc = nothing


        for each User in varUsers 
            objCommand.commandText = "SELECT per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & User & "' "
            set objRs = objCommand.execute
            
            
            if var_viewer_taskoption = 1 then
                var_task_requirement = objRs("per_fname") & " " & objRs("per_sname") & " has been requested to view assessment " & var_ass_reference
                var_task_autocomplete = 1
            else
                var_task_requirement = var_viewer_taskrequirement
                var_task_autocomplete = var_viewer_autocomplete
            end if
        
            objCommand.commandText = "INSERT INTO " & Application("DBTABLE_TASK_MANAGEMENT") & " (corp_code, gendatetime, task_due_date, task_account, task_module, task_mod_ref, task_alert_date, task_by, task_status, task_lastmodified, task_lastmodified_by, task_description, task_accepted, viewed_ass_id, auto_complete, task_company, task_location, task_department) " & _
                                     " VALUES ('" & session("CORP_CODE") & "', '" & now() & "', '" & var_viewer_viewby_date & "', '" & User & "', '" & var_ass_module & "_V', '" & var_ass_reference & "', '" & var_viewer_viewby_date & "', '" & session("YOUR_ACCOUNT_ID") & "', '2', '" & now() & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & var_task_requirement & "', 1, " & var_current_record_id & ", " & var_task_autocomplete & ", '" & var_ass_tier2 & "', '" & var_ass_tier3 & "', '" & var_ass_tier4 & "')"
            objcommand.execute


            Objcommand.CommandText = "SELECT top 1 * FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & var_ass_module & "_V' AND TASK_MOD_REF = '" & var_ass_reference & "' order by id desc"    
    SET Objrs = Objcommand.Execute
        
        dbTaskid = Objrs("id")
        varTempTaskModActionUser = Objrs("Task_account")
        var_frm_task_description = Objrs("Task_description")
        var_eventdate = Objrs("Task_due_date")
    
    '************************************************************************
    '     Send the task to the recipient via email, in addition to adding 
    '     to task manager. Added for Ocado to notify users when tasks added
    '     Don't send the email if the recipient is the person issuing
    '************************************************************************


    if session("task_email_instanotification") = "Y"  and Session("corp_code") = "336872" then
        
        objCommand.commandText = "SELECT corp_email FROM " & Application("DBTABLE_USER_DATA") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND acc_ref = '" & varTempTaskModActionUser & "' " & _
                                 " AND acc_ref <> '" & Session("YOUR_ACCOUNT_ID") & "' " & _ 
                                 "  AND (corp_email IS NOT NULL AND corp_email <> '')"
        set objRs_email = objCommand.execute
        if not objRs_email.EOF then

            emailTo = objRs_email("corp_email")

            emailSubject = "AssessNET: A new action has been assigned to you for completion"

            emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
                             "<br />" & _
                             "<p>Dear Sir / Madam,</p>" & _
                             "<p>The following task has been assigned to you by " & session("YOUR_FULLNAME")  & ".</p>" & _
                             "<p>Task reference: " & dbTaskid & "<br />" & var_frm_task_description & "</p>" & _
                             "<p>The due date of the task is: <strong>" & var_eventdate & "</strong></p>" & _
                             "<p>To view more information regarding this task, please log into AssessNET and view your task list.</p>" & _
                             "<p>Kind regards,</p>" & _
                             "<p>The AssessNET Team</p>" & _
                         "</BODY></HTML>"

            if session("corp_code") = "000010" then
                call sendEmail("aiptemp@parliament.uk", "task-service@assessnet.co.uk", emailSubject, emailBody)
            else
                emailTo = emailTo
                call sendEmail(emailTo, "task-service@assessnet.co.uk", emailSubject, emailBody)
            end if
            task_history_text = "Task emailed to: " & emailTo
        
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)

        end if
        objRs_email.close()
    end if

        next
        
        var_anchor = "sectionviewer"
        
        response.write "<SCRIPT LANGUAGE=""JavaScript""> alert(""The selected users have been notified of your request"");</script>"
        
        
    else
        ' check the person currently viewing the assessment. if they have 'view' tasks for this assessment, mark them as complete
        
        objCommand.commandText = "SELECT id, auto_complete FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND task_account = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                 "  AND task_module = '" & var_ass_module & "_V' " & _
                                 "  AND task_mod_ref = '" & var_ass_reference & "' " & _
                                 "  AND (task_status = '2' OR task_status = '1')"
        set objRsV = objCommand.execute
        if not objRsV.EOF then
            while not objRsV.EOF
                
                if objRsV("auto_complete") = True then
                    ' create the tasks fingerprint to say its completed
	                Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " " & _
								                "(corp_code,task_id,task_history_by,task_history,gendatetime) " & _
								                "VALUES('" & Session("CORP_CODE") & "','" & objRsV("id") & "','" & session("YOUR_ACCOUNT_ID") & "','Task automatically changed to complete when assessment viewed','" & date() & " " & Time() & "')"
                    objcommand.execute
                    
                    ' update the task to completed
                    objCommand.commandText = "UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                             "SET task_status = '0', task_lastmodified = '" & now() & "', task_lastmodified_by = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                             "  AND id = " & objRsV("id") & " " & _
                                             "  AND task_mod_ref = '" & var_ass_reference & "' " & _
                                             "  AND task_account = '" & session("YOUR_ACCOUNT_ID") & "'"
                    objCommand.execute
                end if
                
                objRsV.movenext
            wend
        end if
    end if
    
end sub


%>