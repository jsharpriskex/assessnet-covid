<%

sub attachedrecords(modRef, modRef_sub, var_reference, var_alternate_reference, var_view, var_stepsToRoot, divID, sectionID, sectionTitle, user_access_level, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'modRef: PTW, RA, etc...
    'modRef_sub: for more grnaular control (allows PTW to attach assessments to a template, but also specific assessments to a permit)
    'var_reference: reference number of the incident/assessment/permit assessments are attached too
    'var_alternate_reference: used in conjection with modRef_sub to allow other attachments based on a different reference code
    'var_view: vw: view option only; ed: view and remove buttons
    'var_stepsToRoot: how many steps backwards before the directory structure will make sense
    'divID: what to call the div on the form
    'sectionID: what to call the section on the form
    'sectionTitle: give the section a title in keeping with the current form
    'user_access_level: allow the calling form to specify the access level of the current user, overriding their own permission level. 
    'sub_sw1: define the attachment link to present to the user
    'sub_sw2: define the section to return to after processing (default is hard coded into respective modules)



    
        select case modRef
            case "MS"
                modTitle = "Method Statement"
                modReferer = "ms_attach"
                heading_style = "3"
                tableTitle = "Records attached to this " & modTitle

            case "PTW"
                modTitle = "Permit To Work"
                if modRef_sub = "file" then
                    modReferer = "ptw_file"
                    heading_style = "3"
                    tableTitle = "Records attached to this " & modTitle
                elseif modRef_sub = "question" then
                    modReferer = "ptw_file"
                    heading_style = "0"
                    tableTitle = "Records attached to this question"
                else
                    modReferer = "ptw_template"
                    heading_style = "3"
                    tableTitle = "Records attached to this template"
                end if

            case "ACC"
                modTitle = "Incident"
                modReferer = "acc_attach"
                heading_style = "4"
                tableTitle = "Records attached to this " & modTitle

            case "MH"
                modTitle = "Manual Handling Assessment"
                modReferer = "mh_attach"
                heading_style = "3"
                tableTitle = "Records attached to this " & modTitle

            case "RA"
                modTitle = "Risk Assessment"
                modReferer = "ra_attach"
                heading_style = "3"
                tableTitle = "Records attached to this " & modTitle
    
        end select

        if user_access_level = "" then  
            user_access_level = session("YOUR_ACCESS")
        end if

        var_nav = ""
        for j = 1 to var_stepsToRoot
            var_nav = var_nav & "../"
        next


        if modReferer = "ptw_file" then
            objCommand.commandText = "SELECT att.attach_ref AS attached_ref, ra.recordequipment as output1_ra, ra.recorddescription as output2_ra, sp.sp_location as output1_sp, sp.sp_proposedworks as output2_sp, attach_datetime as output3, ra.id as output4, att.attach_arc_reference as output5 " & _
                                     "FROM MODULE_GLOBAL.dbo.GLOBAL_Attach_Assessments AS att " & _
                                     "LEFT JOIN " & Application("DBTABLE_RAMOD_DATA") & " AS ra " & _
                                     "  ON att.corp_code = ra.corp_code " & _
                                     "  AND att.attach_ref = ra.recordreference " & _
                                     "LEFT JOIN module_ptw.dbo.sprog_entries as SP " & _
                                     " ON att.corp_code = sp.corp_code " & _
                                     " AND att.attach_ref = sp.reference " & _
                                     "WHERE att.CORP_CODE = '" & Session("CORP_CODE") & "' " & _
                                     "  AND att.RECORDREFERENCE = '" & var_alternate_reference & "' " & _
                                     "ORDER BY attach_datetime DESC"
        else
            objCommand.commandText = "SELECT att.attach_ref AS attached_ref, ra.recordequipment as output1_ra, ra.recorddescription as output2_ra, attach_datetime as output3, ra.id as output4, att.attach_arc_reference as output5 " & _
                                     "FROM MODULE_GLOBAL.dbo.GLOBAL_Attach_Assessments AS att " & _
                                     "LEFT JOIN " & Application("DBTABLE_RAMOD_DATA") & " AS ra " & _
                                     "  ON att.corp_code = ra.corp_code " & _
                                     "  AND att.attach_ref = ra.recordreference " & _
                                     "WHERE att.CORP_CODE = '" & Session("CORP_CODE") & "' " & _
                                     "  AND att.RECORDREFERENCE = '" & var_reference & "' " & _
                                     "ORDER BY attach_datetime DESC"
        end if

        if modRef = "MS" then
            objCommand.commandText = "SELECT att.attached_ref, ra.recordequipment as output1_ra, ra.recorddescription as output2_ra, att.gendatetime AS attach_datetime as output3, ra.id, '' as output5 " & _
                                     "FROM " & Application("DBTABLE_MS_ATTACH") & " as att " & _
                                     "LEFT JOIN " & Application("DBTABLE_RAMOD_DATA") & " as ra " & _
                                     "  ON att.corp_code = ra.corp_code " & _
                                     "  AND att.attached_ref = ra.recordreference " & _
                                     "WHERE att.CORP_CODE = '" & session("CORP_CODE") & "' " & _
                                     "  AND att.RECORDREFERENCE = '" & var_reference & "' " & _
                                     "UNION " & objCommand.commandText

        end if

        set objRs = Objcommand.execute
    
        response.write "<div id='" & divID & "' class='sectionarea'>" & _
                        "<a href='#' name='" & sectionID & "' id='" & sectionID & "'>&nbsp;</a>"

        if len(sectionTitle) > 0 then
            response.write "<h" & heading_style & ">" & sectionTitle & "</h" & heading_style & ">"
        end if
                            

        if objRs.EOF and len(sectionTitle) > 0 then

            Response.write  "<div class='warning'>" & _
                                "<table width='100%' cellpadding='2px' cellspacing='2px'>" & _
                                "<tr><th width='50'>&nbsp;</th><td class='l noborder'><strong>NOTICE</strong><br />There are currently no assessments attached to this " & modTitle & "</td></tr>" & _
                                "</table>" & _
                            "</div>"
        elseif objRs.EOF and len(sectionTitle) = 0 then
            'do nothing
        else

            response.write "<table width='100%' cellpadding='1px' cellspacing='2px' class='showtd'>" & _
                           "<tr><th width='100px'>Type</th><th>" & tableTitle & "</th><th width='70'>Reference</th></tr>"

            while not objRs.eof


                ' output details
                ' output1 = title/description/info of record
                ' output2 = title/description/info of record
                ' output3 = date it was attached
                ' output4 = id of original record its linked to
                ' output5 = attachment arc reference

                ' default outputs, do not change based on record type
                var_output3 = objrs("output3")
                var_output4 = objrs("output4")
                var_output5 = objrs("output5")

                ' find out which module it is, then set the outputs
                if instr(ucase(objrs("attached_ref")),"RA") > 0 then
                    var_file_type = "Risk Assessment"
                    var_output1 = objrs("output1_ra")
                    var_output2 = objrs("output2_ra")

                    if isnull(var_output5) = false and var_output5 <> "" then
                        var_view_option = "<a href='" & var_nav & "data/documents/pdf_centre/ra_archive.aspx?ccode=" & session("CORP_CODE") & "&dcode=" & session("DEMO_COMPANY_ID") & "&ref=" & objRs("attached_ref") & "&multi=" & session("RA_MULTI") & "&arc_ref=" & var_output5 & "' target='_blank'>view</a>"
                    else 
                        var_view_option = "<a href='" & var_nav & "data/documents/pdf_centre/ra.aspx?r=" & var_output4 & "&ccode=" & session("CORP_CODE") & "&dcode=" & session("DEMO_COMPANY_ID") & "&ref=" & objRs("attached_ref") & "&multi=" & session("RA_MULTI") & "' target='_blank'>view</a>"
                    end if

                elseif instr(ucase(objrs("attached_ref")),"SP") > 0 then
                    var_file_type = "Safety Programme"
                    var_output1 = objrs("output1_sp")
                    var_output2 = objrs("output2_sp")

                    ' NOTE the arc reference is redundant in reference to SP's do its used to carry the switch that identifies its an electrical or mechanical programme PDF
                    var_view_option = "<a href='" & var_nav & "data/documents/pdf_centre/sp_report_pdf_" & var_output5 & ".aspx?r=" & var_output4 & "&ccode=" & session("CORP_CODE") & "&dcode=" & session("DEMO_COMPANY_ID") & "&ref=" & objrs("attached_ref") & "&userref=" & session("YOUR_ACCOUNT_ID") & "' target='_blank'>view</a>"
            
                end if


        



                if len(sub_sw2) > 0 then
                    var_alternate_reference = var_alternate_reference & "-" & sub_sw2
                end if

                response.Write "<tr>" & _
                               "<td>" & var_file_type & "</td>" & _
                               "<td><Strong>" & var_output1 & "<br /></strong>" & var_output2 & "</td>" & _
                               "<td width='100px' align='center'>" & objRs("attached_ref") & "<br />"
                               
                                

                                if var_view = "ed" and user_access_level < 3 then
                                    if modRef = "PTW" and (modRef_sub = "file" or modRef_sub = "question") then
                                        response.write var_view_option & " &nbsp;|&nbsp; <a href='" & var_nav & "data/functions/func_attach_process.asp?att_cmd=rem_att&att_mod_ref=" & var_alternate_reference & "&att_ref=" & objrs("attached_ref") & "&att_mod=" & modRef & "&att_mod_sub=file' onclick='return confirm(""Are you sure you want to detach this record?"")'>Remove</a>"
                                    else
                                        response.write var_view_option & " &nbsp;|&nbsp; <a href='" & var_nav & "data/functions/func_attach_process.asp?att_cmd=rem_att&att_mod_ref=" & var_reference & "&att_ref=" & objrs("attached_ref") & "&att_mod=" & modRef & "' onclick='return confirm(""Are you sure you want to detach this record?"")'>Remove</a>"
                                    end if
                                end if

                response.write "</td></tr>"
                objRs.movenext
            wend

            response.write "</table>"
        end if
        set objRs = nothing
    
        if var_view = "ed" then
            if modRef = "PTW" and modRef_sub = "file" then
                response.write "<p align='right'>[<a href='" & var_nav & "modules/hsafety/risk_assessment/app/rassessment_search.asp?frm_reference=" & var_alternate_reference & "&referer=" & modReferer & "'>Attach Risk Assessment</a>]&nbsp;[<a href='" & var_nav & "modules/hsafety/safety_programme/app/sp_srch.asp?frm_reference=" & var_alternate_reference & "&referer=" & modReferer & "'>Attach Safety Programme</a>]</p>"
            elseif modRef = "PTW" and modRef_sub = "question" then
                if len(sub_sw2) > 0 then
                    var_alternate_reference = var_alternate_reference & "-" & sub_sw2
                end if

                if sub_sw1 = "RA" then
                    response.write "<p align='right'><a href='" & var_nav & "modules/hsafety/risk_assessment/app/rassessment_search.asp?frm_reference=" & var_alternate_reference & "&referer=" & modReferer & "'>Attach Risk Assessment</a></p>"
                elseif sub_sw1 = "SP" then
                    response.write "<p align='right'><a href='" & var_nav & "modules/hsafety/safety_programme/app/sp_srch.asp?frm_reference=" & var_alternate_reference & "&referer=" & modReferer & "'>Attach Safety Programme</a></p>"
                end if
            else
                'response.write "test: " & var_acc_inc_classification_perms_edit
                if (session("acc_incident_classification_permissions") = "Y" and var_acc_inc_classification_perms_edit = 1) or (session("acc_incident_classification_permissions") <> "Y") then
                    response.write "<p align='right'><a href='" & var_nav & "modules/hsafety/risk_assessment/app/rassessment_search.asp?frm_reference=" & var_reference & "&referer=" & modReferer & "'>click here to attach assessment(s)</a></p>"
                end if
            end if
        end if

        response.write "</div>"

end sub


sub attachedrecords_reverse(modRef, modRef_sub, var_reference, var_alternate_reference, var_view, var_stepsToRoot, divID, sectionID, sectionTitle, user_access_level, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'modRef: PTW, RA, etc...
    'modRef_sub: for more grnaular control (allows PTW to attach assessments to a template, but also specific assessments to a permit)
    'var_reference: reference number of the incident/assessment/permit assessments are attached too
    'var_alternate_reference: used in conjection with modRef_sub to allow other attachments based on a different reference code
    'var_view: vw: view option only; ed: view and remove buttons
    'var_stepsToRoot: how many steps backwards before the directory structure will make sense
    'divID: what to call the div on the form
    'sectionID: what to call the section on the form
    'sectionTitle: give the section a title in keeping with the current form
    'user_access_level: allow the calling form to specify the access level of the current user, overriding their own permission level. 
    'sub_sw1: define the attachment link to present to the user
    'sub_sw2: define the section to return to after processing (default is hard coded into respective modules)



    
        select case modRef
            case "MS"
                modTitle = "Method Statement"
                modReferer = "ms_attach"
                heading_style = "3"
                tableTitle = "Records this " & modTitle & " is attached to"

            case "PTW"
                modTitle = "Permit To Work"
                if modRef_sub = "file" then
                    modReferer = "ptw_file"
                    heading_style = "3"
                    tableTitle = "Records this " & modTitle & " is attached to"
                end if

            case "ACC"
                modTitle = "Incident"
                modReferer = "acc_attach"
                heading_style = "4"
                tableTitle = "Records this " & modTitle & " is attached to"

            case "MH"
                modTitle = "Manual Handling Assessment"
                modReferer = "mh_attach"
                heading_style = "3"
                tableTitle = "Records this " & modTitle & " is attached to"

            case "RA"
                modTitle = "Risk Assessment"
                modReferer = "ra_attach"
                heading_style = "3"
                tableTitle = "Records this " & modTitle & " is attached to"

            case "SP"
                modTitle = "Safety Programme"
                modReferer = "SP"
                heading_style = "3"
                tableTitle = "Records this " & modTitle & " is attached to"
    
        end select

        if user_access_level = "" then  
            user_access_level = session("YOUR_ACCESS")
        end if



        var_nav = ""
        for j = 1 to var_stepsToRoot
            var_nav = var_nav & "../"
        next





        if modRef = "PTW" and modRef_sub = "file" or modRef_sub = "question" then
            var_reference = var_alternate_reference
        end if


        objCommand.commandText = "SELECT DISTINCT attach_source_module FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND attach_ref = '" & var_reference & "' "
        set objRs = objCommand.execute
        if not objRs.EOF then

            response.write "<div id='" & divID & "' class='sectionarea'>" & _
                           "<a href='#' name='" & sectionID & "' id='" & sectionID & "'>&nbsp;</a>"

            if len(sectionTitle) > 0 then
                response.write "<h" & heading_style & ">" & sectionTitle & "</h" & heading_style & ">"
            end if

            response.write "<table width='100%' cellpadding='2px' cellspacing='1px' class='showtd'>" & _
                           "<tr><th width='100px'>Type</th><th>" & tableTitle & "</th><th width='70'>Reference</th></tr>"

            while not objRs.EOF

                select case objRs("attach_source_module")
                    case "PROJ"

                        attached_module_title = "Project File"

                        ObjCommand.commandText = "SELECT att.recordreference, proj.project_title, proj.project_description, proj.project_active " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "INNER JOIN " & "Module_GLOBAL.dbo.PROJECT_Entries" & " AS proj " & _
                                                 "  ON att.corp_code = proj.corp_code " & _
                                                 "  AND att.recordreference = proj.projectreference " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "'"
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then
                            while not objRs1.EOF
                                var_view_option = "<br /><a href='../" & var_nav & "core/modules/project/project.asp?ref=" & objRs1("recordreference") & "'>view</a>"

                                response.write "<tr><td>" & attached_module_title & "</td><td>" & objRs1("project_title") & "<br /><span class='info'>" & objRs1("project_description") & "</span></td><td class='c'>" & objRs1("recordreference") & var_view_option & "</td></tr>"
        
                                objRs1.movenext
                            wend
                        end if
                        set objRs1 = nothing

                    case "ptw_file"

                        attached_module_title = "Permit to Work"

                        ObjCommand.commandText = "SELECT att.recordreference, temp.template_title, temp.template_description, temp.template_reference " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "INNER JOIN " & "Module_PTW.dbo.ptw_v2_entries" & " AS ptw " & _
                                                 "  ON att.corp_code = ptw.corp_code " & _
                                                 "  AND att.recordreference = ptw.ptw_reference " & _
                                                 "INNER JOIN " & "Module_PTW.dbo.ptw_v2_template_entries" & " AS temp " & _
                                                 "  ON ptw.corp_code = temp.corp_code " & _
                                                 "  AND ptw.template_reference = temp.template_reference " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "'"
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then
                            while not objRs1.EOF
                                var_view_option = "<br /><a href='" & var_nav & "modules/hsafety/permit_to_work_v2/app/ptw_report.asp?ptw_ref=" & objRs1("recordreference") & "&temp_ref=" & objRs1("template_reference") & "'>view</a>"

                                response.write "<tr><td>" & attached_module_title & "</td><td>" & objRs1("template_title") & "<br /><span class='info'>" & objRs1("template_description") & "</span></td><td class='c'>" & objRs1("recordreference") & "PTW" & var_view_option & "</td></tr>"

                                objRs1.movenext
                            wend
                        end if
                        set objRs1 = nothing                                                 

                    case "ptw_question"

                        attached_module_title = "Permit to Work"

                        objCommand.commandText = "SELECT att.recordreference " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "' " & _
                                                 "  AND att.attach_source_module = 'ptw_question' "
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then

                            while not objRs1.EOF                            
                                arry_reference = split(objRs1("recordreference"), "_")

                                ObjCommand.commandText = "SELECT att.recordreference, temp.template_title, temp.template_description, template.template_reference " & _
                                                         "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                         "INNER JOIN " & "Module_PTW.dbo.ptw_v2_entries" & " AS ptw " & _
                                                         "  ON att.corp_code = ptw.corp_code " & _
                                                         "  AND ptw.ptw_reference = '" & arry_reference(0) & "' " & _
                                                         "INNER JOIN " & "Module_PTW.dbo.ptw_v2_template_entries" & " AS temp " & _
                                                         "  ON ptw.corp_code = temp.corp_code " & _
                                                         "  AND ptw.template_reference = temp.template_reference " & _
                                                         "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                         "  AND att.attach_ref = '" & var_reference & "' " & _
                                                         "  AND att.attach_source_module = 'ptw_question' "
                                set objRs2 = objCommand.execute
                                if not objRs2.EOF then
                                    while not objRs2.EOF
                                        var_view_option = "<br /><a href='" & var_nav & "modules/hsafety/permit_to_work_v2/app/ptw_report.asp?ptw_ref=" & objRs1("recordreference") & "&temp_ref=" & objRs1("template_reference") & "'>view</a>"

                                        response.write "<tr><td>" & attached_module_title & "</td><td>" & objRs2("template_title") & "<br /><span class='info'>" & objRs2("template_description") & "</span></td><td class='c'>" & arry_reference(0) & "PTW" & var_view_option & "</td></tr>"

                                        objRs2.movenext
                                    wend
                                end if
                                set objRs2 = nothing                                                 
                           
                                objRs1.movenext
                            wend

                        end if
                        set objRs1 = nothing




                    case "ptw_template"

                        attached_module_title = "Permit to Work Template"

                        ObjCommand.commandText = "SELECT att.recordreference, temp.template_title, temp.template_description " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "INNER JOIN " & "Module_PTW.dbo.ptw_v2_template_entries" & " AS temp " & _
                                                 "  ON att.corp_code = temp.corp_code " & _
                                                 "  AND att.recordreference = temp.template_reference " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "'"
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then
                            while not objRs1.EOF
                                response.write "<tr><td>" & attached_module_title & "</td><td>" & objRs1("template_title") & "<br /><span class='info'>" & objRs1("template_description") & "</span></td><td class='c'>" & objRs1("recordreference") & "</td></tr>"

                                objRs1.movenext
                            wend
                        end if
                        set objRs1 = nothing  

                    case "ms_attach"

                        attached_module_title = "Method Statement"

                        ObjCommand.commandText = "SELECT att.recordreference, ms.project_title, ms.project_description, ms.id " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "INNER JOIN " & "Module_MS.dbo.MS_Data_entries_v2" & " AS ms " & _
                                                 "  ON att.corp_code = ms.corp_code " & _
                                                 "  AND att.recordreference = ms.recordreference " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "'"
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then
                            while not objRs1.EOF
                                var_view_option = "<br /><a href='" & var_nav & "modules/hsafety/method_statement_v2/app/method_report.asp?r=" & objRs1("id") & "&frm_reference=" & objRs1("recordreference") & "'>view</a>"

                                response.write "<tr><td>" & attached_module_title & "</td><td>" & objRs1("project_title") & "<br /><span class='info'>" & objRs1("project_description") & "</span></td><td class='c'>" & objRs1("recordreference") & var_view_option & "</td></tr>"

                                objRs1.movenext
                            wend
                        end if
                        set objRs1 = nothing                          

                    case "acc_attach"

                        attached_module_title = "Incident Report"

                        objCommand.commandText = "SELECT att.recordreference " & _
                                                 "FROM " & "Module_GLOBAL.dbo.GLOBAL_Attach_Assessments" & " AS att " & _
                                                 "WHERE att.corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND att.attach_ref = '" & var_reference & "' " & _
                                                 "  AND att.attach_source_module = 'acc_attach' "
                        set objRs1 = objCommand.execute
                        if not objRs1.EOF then
                            while not objRs1.EOF
                                response.write "<tr><td>" & attached_module_title & "</td><td>Incident Report<br /><span class='info'>For data protection no information is provided in this preview</span></td><td class='c'>" & objRs1("recordreference") & "</td></tr>"

                                objRs1.movenext
                            wend
                        end if
                        set objRs1 = nothing  

                    case "RA"

                end select

                objRs.movenext
            wend

            response.write "</table>"

            response.write "</div>"

        end if
        set objRs = nothing

end sub


%>