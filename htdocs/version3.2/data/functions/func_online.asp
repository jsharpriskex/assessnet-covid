<!-- #include file="../scripts/inc/dbconnections.inc" -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>Who's Online?</title>
    <meta name="author" content="Safe and Sound Management Consultants Ltd" />
    <meta name="description" content="AssessNET Health and Safety System Online Assessments and Management" />
    <script language="JavaScript">
    </script>
    <link rel='stylesheet' href='../scripts/css/global.css' type='text/css' media='screen' />
    <style type='text/css'>
    BODY { background: #e2e2e2; }
    DIV.sect_head
    {
     background: #f1f1f1;
     border: 1px solid #999;
     padding: 4px;
    }
    DIV.sect_body
    {
     background: #fff;
     border-left: 1px solid #999;
     border-right: 1px solid #999;
     padding: 0px;
    }
    DIV.sect_end { border-bottom: 1px solid #999; }
    TR.hl TD { background: #edf3fe; }
    </style>
</head>

<body>
<%

DIM objConn,objRs,objCommand
startConnections()

objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_USER_DATA") & _
    " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ACC_LEVEL <> '5' AND ACC_REF <> '" & Session("YOUR_ACCOUNT_ID") & "'"
Set objRs = objCommand.Execute

If objRs.EOF Then
    Response.Write "<div class='sect_head'><strong>Online<strong></div>"
    Response.Write "<div class='sect_body'>No-one is online"
Else
    Response.Write "<div class='sect_head'><strong>Online</strong><br />Click a user to send an eMemo</div>"
    Response.Write "<div class='sect_body'>"
    Response.write "<table cellpadding='4' cellspacing='0' width='100%'>"
    numOnline = 0
    While NOT objRs.EOF
        If numOnline MOD 2 = 0 Then
            Response.write "<tr class='hl'>"
        Else
            Response.write "<tr>"
        End if
        Response.Write "<td width='30' class='r'><img src='../../images/icons/action_user_online.png' align='absmiddle' /></td>" & _
        			   "<td>" & objRs("Per_sname") & ", " & objRs("Per_fname") & "</td>"
        Response.write "</tr>"
        objRs.MoveNext
        numOnline = numOnline + 1
    Wend
    Response.write "</table>"
End if

%>
</div>
<div class='sect_head'><strong>Not Online</strong></div>
<div class='sect_body sect_end'>
<%
objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_USER_DATA") & _
    " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ACC_LEVEL = '5' AND ACC_REF <> '" & Session("YOUR_ACCOUNT_ID") & "'"
Set objRs = objCommand.Execute

If objRs.EOF Then
    Response.Write "No-one is online"
Else
    Response.write "<table cellpadding='4' cellspacing='0' width='100%'>"
    numOnline = 0
    While NOT objRs.EOF
        If numOnline MOD 2 = 0 Then
            Response.write "<tr class='hl'>"
        Else
            Response.write "<tr>"
        End if
        Response.Write "<td width='30' class='r'><img src='../../images/icons/action_user_offline.png' align='absmiddle' /></td>" & _
        			   "<td class='disabled'>" & objRs("Per_sname") & ", " & objRs("Per_fname") & "</td>"
        Response.write "</tr>"
        objRs.MoveNext
        numOnline = numOnline + 1
    Wend
    Response.write "</table>"
End if


endConnections()
%>
</div>

</body>
</html>