<!-- #include file="../scripts/inc/dbconnections.inc" -->
<%

Dim objConn
Dim objCommand
Dim objRs

CALL startConnections()

If session("corp_code") = "800044" then
	var_redirect = Application("CONFIG_PROVIDER") & "/core/login/"
else

if request.querystring("redirect") <> "false" and request("cmd") <> "dse_demo" and request("cmd") <> "dse_link" then
    Response.redirect("../../../../core/system/sessions/sessionReset.aspx?redirectTo=logout")
end if

End If

    userSessionGUID = Request.Cookies("sync")

    'close the users session in the DB
    objCommand.commandText = "UPDATE Module_LOG.dbo.LOG_Session_Status SET sessionEnd = GETDATE(), sessionValid = 0, sessionEndedBy = 'Logout' " & _
                             "WHERE sessionGUID = '" & userSessionGUID & "'"
    objCommand.execute

    Response.Cookies("sync").Expires = dateadd("d", -1, now())


' Log the session closure
' this is to track early session closures - not required for session sharing with .NET (Thats above ^^^)

        SET objCommand = Server.CreateObject("ADODB.Command")
	    SET objCommand.ActiveConnection = objconn
    
        objcommand.commandtext = "INSERT INTO MODULE_LOG.DBO.LOG_SESSIONS (CORP_CODE,ACCOUNT_ID,SESSION_ID,TYPE) VALUES('" & Session("corp_code") & "','" & Session("YOUR_ACCOUNT_ID") & "', '" & Session.Sessionid & "','Logout')"
        objcommand.execute



' Close Session object for this user, specified three times to ensure closure 
Session.Contents.RemoveAll
Session.Abandon


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                             ''
'' SUB (errCode) instructions of use                           ''
'' This sub requires,                                          ''
''                    0001 - Invalid username or password      ''
''                    0002 - No modules for licence            ''
''                    0003 - Invalid IP for read-only          ''
''                    0004 - Account disabled                  ''
''                    0005 - Mem. info Failed after 3 attempts ''
''                    0006 - License expired                   ''
''                                                             ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub errCode(varERRnumber)
   LoginURL = "../Login/frm_lg_entry.aspx?ERR=" & varERRnumber
   
   Call EndConnections()
   
   'Error redirect
   Response.Redirect(LoginURL)
   Response.end
End sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''                                                        ''
	'' SUB (userLOG) instructions of use                      ''
	'' This sub requires,                                     ''
	''                    status (1 for success, 2 for fail)  ''
	''                    details (Reason for log entry)      ''
	''					  username (Username entered at login ''
	''                                                        ''
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	SUB useLOG(varSTATUSlog,varDETAILSlog,varUNAMElog)

	varDATElog = Date() & " " & Time()
	varHTTPlog = Request.ServerVariables("HTTPS")
	varIPlog = Request.ServerVariables("REMOTE_ADDR")

		Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_LOG_ACCESS") & " " & _
								  "(datetime,https,success,reason,username,ip_address) " & _
								  "VALUES('" & varDATElog & "','" & varHTTPlog & "','" & varSTATUSlog & "','" & varDETAILSlog & "','" & varUNAMElog & "','" & varIPlog & "') "
		Objcommand.execute

	END SUB



' temp code for login demo on IOSH licence

if len(var_redirect) > 10 then
	
else

if request("cmd") = "dse_demo" then
    var_redirect = Application("CONFIG_PROVIDER") & "/dse_demo_link.html" 
elseif request("cmd") = "dse_link" then
    var_redirect = Application("CONFIG_PROVIDER") & "/dse.asp?t=" & validateinput(request("t")) & "&i=" &   validateinput(request("i")) 
else
    var_redirect = Application("CONFIG_PROVIDER")
end if

End if 


    
CALL Endconnections()

    %>
<html>
<script type="text/javascript">
<!--

function logout(){
          
    parent.location.href="<%=var_redirect %>"
}
// -->
</script>
<% 




%>
    </head>
<body onLoad="javascript:logout()">
</body>
</html>