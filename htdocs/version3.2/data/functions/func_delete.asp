<!-- #INCLUDE FILE="../scripts/inc/auto_logout.inc" -->
<%   

If request.querystring("CMD") = "DEL" then

	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH


	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn

	IF request.querystring("show") = "Risk" then
	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_RAMOD_DATA") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   	objcommand.execute
   	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_RAMOD_HAZARDS") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
    objcommand.execute
   	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_RAMOD_PERSONS") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   	objcommand.execute
   	
   	ELSEIF request.querystring("show") = "DSE" then
   	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_DSEMOD_DATA") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   	objcommand.execute
   	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_DSEMOD_RESULTS") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
    	objcommand.execute
   	
   	objcommand.commandtext = "DELETE FROM " & Application("DBTABLE_DSEMOD_ACTIONS") & " WHERE recordreference='" & request.querystring("code") & "' AND CORP_CODE='" & Session("CORP_CODE") & "'"
   	objcommand.execute
   	
   	END IF
   	
	objCommand.ActiveConnection.close

	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING

   	redirectURL = "../../modules/acentre/acentre_briefcase.asp?show=" & request.querystring("show") & "&page=" & Request.querystring("page") & "&bmk=" & Request.querystring("bmk")

	response.redirect(redirectURL)
	
ELSE

%>
<HTML>
<BODY>

<script type="text/javascript">
var name = confirm("Are you sure you want to delete Record (<% =request.querystring("code") %>)")
if (name == true)
{
parent.main.location.href=("func_delete.asp?CMD=DEL&code=<% =request.querystring("code") %>&show=<% =request.querystring("show") %>&page=<% =Request.querystring("page") %>&bmk=<% =Request.querystring("bmk") %>")
}
else
{
parent.main.location.href=("../../modules/acentre/acentre_briefcase.asp?show=<% =request.querystring("show") %>&page=<% =Request.querystring("page") %>&bmk=<% =Request.querystring("bmk") %>")
}
</script>

</BODY>
</HTML>
<%

END IF
   	  	
%>