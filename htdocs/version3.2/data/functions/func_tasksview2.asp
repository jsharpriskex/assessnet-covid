<%

SUB Taskview(var_taskuser_val)
        
        var_HIDEcompletes = "NOT (tasks.task_status < '1' AND tasks.task_due_date < getdate()) AND"
        
        ' Start of window menu bar                                     
                        
        Response.Write  "<table width='100%' cellpadding='3' cellspacing='0' class='taskwin'>" 
                                  
        ' End of window menu bar
        
        
        ' Ok get data from task manager
        
        Objcommand.commandtext = "SELECT TOP 3 tasks.*, RA.recordequipment, RA.recordreference, SA.count_action_req, SA.count_meet_req, SA.count_doesnt_meet, SA.rec_bd, SA.audit_ref, INS.pname, INS.pdesc, INS.record_reference, CA.title, CA.description, CA.ca_ref, MH.mh_step2_op, MH.mh_step2_opdes, MH.recordreference as MHref FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " as tasks " & _
                                 "LEFT JOIN " & Application("DBTABLE_RAMOD_DATA") & " as RA " & _
                                 "ON RA.corp_code = Tasks.corp_code " & _
                                 "AND RA.recordreference = Tasks.task_mod_ref " & _
                                 "LEFT JOIN " & Application("DBTABLE_INS_DATA") & " as INS " & _
                                 "ON INS.corp_code = Tasks.corp_code " & _
                                 "AND INS.record_reference = Tasks.task_mod_ref " & _
                                 "LEFT JOIN " & Application("DBTABLE_CA_DATA") & " as CA " & _
                                 "ON CA.corp_code = Tasks.corp_code " & _
                                 "AND CAST(CA.ca_ref as varchar(50)) = Tasks.task_mod_ref " & _
                                 "LEFT JOIN " & Application("DBTABLE_MHMOD_DATA") & " as MH " & _
                                 "ON MH.corp_code = Tasks.corp_code " & _
                                 "AND MH.recordreference = Tasks.task_mod_ref " & _
                                 "LEFT JOIN " & Application("DBTABLE_SA_AUDIT_ENTRIES") & " as SA " & _
                                 "ON SA.corp_code = Tasks.corp_code " & _
                                 "AND CAST(SA.audit_ref as varchar(50)) = Tasks.task_mod_ref " & _                                 
                                 "WHERE " & var_HIDEcompletes & " tasks.CORP_CODE = '" & SESSION("CORP_CODE") & "' AND tasks.TASK_ACCOUNT LIKE '" & var_taskuser_val & "' " & SQL_filterquery & " ORDER BY tasks.TASK_DUE_DATE ASC"
        SET Objrs = Objcommand.execute
        
        
        IF Objrs.EOF Then
        Response.write  "<tr>"
        
            
            Response.write  "<td height='100' colspan='6' align='center'>" & _
                            "<strong>Note :</strong> You currently have no tasks that require completion, please continue to check your tasks daily</td>"
        
        Response.Write  "</td>" & _
                        "</tr>"
        
        ELSE
        ' Ok now loop through the tasks
        while not objrs.eof
        
        'Get DB data
        row_overdue = ""
        var_taskid = Objrs("id")
        var_duedate = Cdate(left(objrs("task_due_Date"),10))
        
        ' Work out the differences with current date        
        varDueDate_countdown_d = DateDiff("d",date(),var_duedate)
        
        
        ' Find the status of the task
        SELECT case objrs("task_status")
            case "0"
            var_icon = "<img src='../management/taskmanager_v2/img/comp_task_file.png' align='absmiddle'>"
            var_status_class = "txt_shade_grn"
            var_status = "Complete"
            case "1"
            var_icon = "<img src='../management/taskmanager_v2/img/active_task_file.png' align='absmiddle'>"
            var_status_class = "txt_shade_org"
            var_status = "Active"
            case "2"
            var_icon = "<img src='../management/taskmanager_v2/img/new_task_file.png' align='absmiddle'>"
            var_status_class = "txt_shade_red"
            var_status = "Pending"
        END SELECT
                      
        SELECT CASE Objrs("task_module")
            case "DSE"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>DSE Assessment - </span>"
            var_file_desc = "Concerns require attention"
            var_task_desc = left(objrs("task_description"),200)
            var_dse_link = "_dse"
            case "PER"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Private - </span>"
            var_file_desc = left(objrs("task_description"),200)
            var_task_desc = objrs("task_furtherinfo")
            var_dse_link = ""
            case "RA"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Risk Assessment</span> - (" & objrs("recordreference") & ")"
            var_file_desc = left(objrs("recordequipment"),150)
            var_task_desc = objrs("task_description")
            var_dse_link = ""
            case "MH"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Manual Handling</span> - (" & objrs("MHref") & ")"
            var_file_desc = left(objrs("mh_step2_op"),150)
            var_task_desc = objrs("task_description")
            var_dse_link = ""
            case "CA"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>COSHH Assessment</span> - (" & objrs("ca_ref") & "CA)"
            var_file_desc = left(objrs("title"),150)
            var_task_desc = objrs("description")
            var_dse_link = ""
            case "INS"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Inspection</span> - (" & objrs("record_reference") & ")"
            var_file_desc = left(objrs("pname"),150)
            var_task_desc = objrs("task_description")
            var_dse_link = ""
            case "SA"
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Safety Audit</span> - (" & objrs("audit_ref") & "SA)"
            var_file_desc = left(objrs("rec_bd"),150)
            var_task_desc = "Total Actions Required: <strong>" & objrs("count_action_req") & "</strong> &nbsp;&nbsp;&nbsp;Non-Compliant: <strong>" & objrs("count_doesnt_meet") & "</strong> &nbsp;&nbsp;&nbsp;Compliant: <strong>" & objrs("count_meet_req") & "</strong>"
            var_dse_link = ""
            case else
            var_class_countdown = "shade"
            var_file_ref = "<span class='txt_shade_red'>Unknown</span> - (" & objrs("recordreference") & ")"
            var_file_desc = left(objrs("recordequipment"),150)
            var_task_desc = objrs("task_description")
            var_dse_link = ""
        END SELECT
        
        ' Some quick error correction
        IF var_file_ref = "()" Then
        var_file_ref = "(----)"
        end if
        
        IF var_file_desc = "" OR isNull(var_file_desc) Then
        var_file_desc = "File not found please contact support"
        end if
        
        IF var_task_desc = "" OR isNull(var_task_desc) Then
        var_task_desc = "Unable to gather information please contact support"
        end if

                 
        ' day or days
        If varDueDate_countdown_d = 1 OR varDueDate_countdown_d = 0 Then
        prefix = "day"
        else
            prefix = "days"
        End If
        
       
        ' Overdue?
        if varDueDate_countdown_d < -1 AND Year(var_duedate) =< Year(date) AND var_taskstatus <> "0" Then
            var_section_title = "Top 3 tasks"
            var_class_countdown = "shade_overdue"
            row_overdue = "_overdue"
            varDueDate_countdown = "Overdue"
        elseif varDueDate_countdown_d = 0 AND Year(var_duedate) = Year(date) Then
            var_section_title = "Top 3 tasks"
            varDueDate_countdown = "Today"
        else
            var_section_title = "Top 3 tasks"
            varDueDate_countdown = varDueDate_countdown_d & " " & prefix
        end if
        

        Response.write  "<tr class='taskrow" & row_overdue & "'>" & _
                        "<td width='5' class='" & var_class_countdown & "'>" & var_icon & "</td>" & _
                        "<td class='l " & var_class_countdown & "' valign='top'>" & var_file_ref & " " & left(var_file_desc,30) & "...<br/><span class='info'>" & left(var_task_desc,35) & "...</span></td>" & _
                        "<td width='50' class='" & var_class_countdown & "' nowrap><span class='" & var_status_class & "'>" & var_status & "</span></td>" & _
                        "<td width='55' nowrap class='c " & var_class_countdown & "'>" & varDueDate_countdown & "</td>" & _
                        "<td width='5' class='" & var_class_countdown & "'><a id='task" & var_taskid & "' href='../management/taskmanager_v2/task_info.asp?cmd=view&frm_taskid=" & var_taskid & var_url & "'><img src='../../images/icons/sml_viewclr.gif' title='View this task in more detail' /></a></td>" & _
                        "</tr>"
        
        objrs.movenext
        
        Wend
        END IF
        
        Response.write "</table>" 
                        


End SUB
%>