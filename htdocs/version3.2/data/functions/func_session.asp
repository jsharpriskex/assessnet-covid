
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>AssessNET</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel='stylesheet' href="../scripts/css/global.css" media="all" />
</head>

<body topmargin="0">
<table border="0" cellpadding="0" cellspacing="0" width='100%'>
  <tr>
    <td><div id='lost'>
      <strong>Lost?</strong> You are in ::
      <span class='alt'>AssessNET Login</span>
      </div>
    </td>
    <td width='180' align='right'><div id='corplogo'></div></td>
  </tr>
</table>

<h1>Sorry!</h1>
<p>Your AssessNET session has ended for one of the following reasons:</p>
  
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td width="125" valign="top"><img border="0" src="../../images/icons/form_error.gif" width="125" height="110"></td>
      <td valign="top">
        <ul>
          <li><strong>Your Account has Timed Out</strong><br />
            AssessNET sessions automatically timeout after <strong class='alt'>20 Minutes</strong> of inactivity
            as a security precaution.<br />&nbsp;</li>
          <li><strong>You tried to enter AssessNET via a Bookmark, Shortcut or Favorite</strong><br />
            You cannot return directly to a bookmarked page within AssessNET. You can only gain access to AssessNET 
            through the login screen.</li>
        </ul>
      </td>
    </tr>
  </table>


<p class='c'><a href="<% =application("CONFIG_PROVIDER") %>" target="_parent"><img border="0" src="../../images/icons/lrg-login.gif" width="184" height="24"></a></p>

</body>
</html>
<% 

Session.Abandon
Session.Abandon
Session.Abandon
%>