<%
' --------------------------------------------------------------
'  Sub to send an eMemo
' --------------------------------------------------------------
'   ememoTo         - Recipient of ememo (Account ID)
'   ememoFrom       - Sender of ememo (Account ID or "system")
'   ememoFromName   - Use "Session("FULL_NAME")" or "System"
'   ememoSubject    - Subject of eMemo
'   ememoBody       - Body of message
'   ememoPriority   - 1 = High, 0 = Low

' All escaping performed by sub - just send unescaped data

Sub SendEmemo(ememoTo,ememoFrom,ememoFromName,ememoSubject,ememoBody,ememoPriority,ememoSenderId)
    If Len(ememoFrom) = 0 Then
        ememoFrom = "system"
    End if
    If Len(ememoFromName) = 0 Then
        ememoFromName = "System"
    End if
    ememoType = 1 ' Inbox
    ememoStatus = 1 ' Unread
    ememoSubject = Replace(ememoSubject,"'","''")
    ememoBody = Replace(ememoBody,"'","''")
    
    ' Add to eMemo database
    objCommand.CommandText = "INSERT INTO " & Application("DBTABLE_EMEMO_DATA") & " " & _
                             "(CORP_CODE,accountid,ememotype,ememopriority,ememostatus,ememobody,ememosender,ememodate," & _
                             "  ememosubject,ememosenderid) " & _
                             "VALUES ('" & Session("CORP_CODE") & "','" & ememoTo & "','" & ememoType & "','" & ememoPriority & "'," & _
                             "  '" & ememoStatus & "','" & ememoBody & "','" & ememoFromName & "',GETDATE(),'" & ememoSubject & "'," & _
                             "  '" & ememoFrom & "')"
    objCommand.Execute
End sub
%>