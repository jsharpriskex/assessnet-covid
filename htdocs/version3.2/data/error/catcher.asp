<%
Option Explicit

If Len(Session("AUTH")) < 1 Then
    Response.Redirect(Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/data/functions/func_session.asp")
End if

Const lngMaxFormBytes = 200

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL

If Response.Buffer Then
    Response.Clear
    Response.Status = "500 Internal Server Error"
    Response.ContentType = "text/html"
    Response.Expires = 0
End If


' ------------------------------------------------------
' --------------- COMPILE ERROR DETAILS ----------------

Set objASPError = Server.GetLastError

Dim bakCodepage, errorBody, errorTitle, shortError, errorBrowser, errorPage, errorPost
On Error resume Next
    bakCodepage = Session.Codepage
    Session.Codepage = 1252
On Error goto 0


If objASPError.ASPCode > "" Then 
    errorBody = errorBody & Server.HTMLEncode(", " & objASPError.ASPCode)
End if

errorBody = errorBody & Server.HTMLEncode(" (0x" & Hex(objASPError.Number) & ")" ) & "<br />"

If objASPError.ASPDescription > "" Then 
    errorBody = errorBody & Server.HTMLEncode(objASPError.ASPDescription) & "<br />"
ElseIf (objASPError.Description > "") Then 
    errorBody = errorBody & Server.HTMLEncode(objASPError.Description) & "<br />"
End if

shortError = errorBody

blnErrorWritten = False

' Only show the Source if it is available and the request is from the same machine as IIS
If objASPError.Source > "" Then
    strServername = LCase(Request.ServerVariables("SERVER_NAME"))
    strServerIP = Request.ServerVariables("LOCAL_ADDR")
    strRemoteIP =  Request.ServerVariables("REMOTE_ADDR")
    If (strServername = "localhost" Or strServerIP = strRemoteIP) And objASPError.File <> "?" Then
        errorBody = errorBody & Server.HTMLEncode(objASPError.File)
        If objASPError.Line > 0 Then errorBody = errorBody & ", line " & objASPError.Line
        If objASPError.Column > 0 Then
            errorBody = errorBody & ", column " & objASPError.Column
        End if
        errorBody = errorBody & "<br>"
        errorBody = errorBody & "<font style=""COLOR:000000; FONT: 8pt/11pt courier new""><b>"
        errorBody = errorBody & Server.HTMLEncode(objASPError.Source) & "<br />"

        If objASPError.Column > 0 Then
            errorBody = errorBody & String((objASPError.Column - 1), "-") & "<br />"
        End if
        errorBody = errorBody & "</b></font>"
        blnErrorWritten = True
    End If
End If

If Not blnErrorWritten And objASPError.File <> "?" Then
    errorTitle = Server.HTMLEncode(objASPError.File)
    If objASPError.Line > 0 Then
        errorTitle = errorTitle & Server.HTMLEncode(", line " & objASPError.Line)
    End if
    If objASPError.Column > 0 Then
        errorTitle = errorTitle & ", column " & objASPError.Column
    End if
    errorTitle = errorTitle
End If
thisFingerPrint = errorTitle

errorBrowser = "<span title='" & Server.HTMLEncode(Request.ServerVariables("HTTP_USER_AGENT")) & "'>[ UA ]</span>" 

errorPage = "<span title='"

strMethod = Request.ServerVariables("REQUEST_METHOD")
errorPage = errorPage & strMethod & " "

If strMethod = "POST" Then
    errorPage = errorPage & Request.TotalBytes & " bytes to "
End If
errorPage = errorPage & Request.ServerVariables("SCRIPT_NAME")

lngPos = InStr(Request.QueryString, "|")

If lngPos > 1 Then
    errorPage = errorPage & "?" & Server.HTMLEncode(Left(Request.QueryString, (lngPos - 1)))
End If

errorPage = errorPage & "'>[ Pg ]</span>"


'If strMethod = "POST" Then
'    errorPost = "<span title='"
'    If Request.TotalBytes > lngMaxFormBytes Then
'       errorPost = errorPost & Server.HTMLEncode(Left(Request.Form, lngMaxFormBytes)) & " ..."
'    Else
'       errorPost = errorPost & Server.HTMLEncode(Request.Form)
'    End If
'    errorPost = errorPost & "'>[ POST ]</span>"
'End If

' ----------------- END ERROR DETAILS ------------------
' ------------------------------------------------------

Dim ObjSend, mailFrom, mailTo, mailSubject, mailBody

thisCreator = Session("YOUR_ACCOUNT_ID")
thisDescription = thisFingerPrint & _
                  "<p>" & errorPage & " " & errorBrowser & " <span title='" & Request.ServerVariables("HTTP_REFERER") & "'>[ Rf ]</span><br />" & _
                  "<p>" & errorPost & " " & _
                  "<p>" & errorBody & "</p>" & _
                  "<p><b>Requested URL:</b> " & request.QueryString & _
                   "<br /><b>HTTP Header:</b> " & Request.ServerVariables("ALL_HTTP")  & _
                  "</p><h3>Session details</h3>" & _
                  "<b>Corp. code:</b> " & Session("CORP_CODE") & _
                  "<br /><b>Group:</b> " & Session("YOUR_GROUP") & _
                  "<br /><b>Company:</b> " & Session("YOUR_COMPANY_NAME") & _
                  "<br /><b>Location:</b> " & Session("YOUR_LOCATION_NAME") & _
                  "<br /><b>Department:</b> " & Session("YOUR_DEPARTMENT_NAME") & _
                  "<br /><b>Person:</b> " & Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME") & _
                  "<br /><b>Username:</b> " & Session("YOUR_USERNAME") & _
                  "<br /><b>E-mail:</b> " & Session("YOUR_EMAIL")
thisDescription = Replace(thisDescription,"'","''")
thisStatus = 0

%>
<!-- #INCLUDE FILE="../scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../functions/func_random.asp" -->
<%
Dim objRs, objConn, objCommand
Dim thisRef, thisCreator, thisDescription, thisStatus, thisFingerPrint, sqlErrorNotify, allowed_ref,checkRS


Call startConnections()

' -------------------  Add to DB  ------------------------

objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_DEV_DATA") & " " & _
                         "WHERE error_fingerprint='" & Replace(thisFingerPrint,"'","''") & "'" & _
                         "  AND status < 6"

Set objRs = objCommand.Execute

If objRs.EOF Then

    'generate unique ref
    
    allowed_ref = 0
    while allowed_ref = 0 

        thisRef = generateRandom(12)
        objCommand.CommandText = "SELECT id FROM " & Application("DBTABLE_DEV_DATA") & " where ref= '" & thisRef & "' "
        set checkRS = objcommand.execute
        
        if checkRS.eof then
           allowed_ref = 1  
        end if

    wend 




    objCommand.CommandText = "INSERT INTO " & Application("DBTABLE_DEV_DATA") & _
                             "  (REF,CREATOR,DATE_CREATED,CORP_CODE,category,description,status,priority,error_fingerprint,error_notify,original_date_created) " & _
                             "VALUES" &  _
                             "  ('" & thisRef & "','" & thisCreator & "',GETDATE(),'" & Session("CORP_CODE") & "'," & _
                             "   'error','" & thisDescription & "','" & thisStatus & "'," & _
                             "   '5','" & thisFingerPrint & "','" & Session("YOUR_EMAIL") & "',GETDATE())"
    objCommand.Execute                    
    
Else
    If InStr("" & objRs("error_notify"), Session("YOUR_EMAIL")) Then
        sqlErrorNotify = ""
    Else
        sqlErrorNotify = ", error_notify = error_notify + ', " & Session("YOUR_EMAIL") & "'"
    End if

    objCommand.CommandText = "UPDATE " & Application("DBTABLE_DEV_DATA") & _
                             "  SET DATE_CREATED=GETDATE(), error_count = error_count + 1" & sqlErrorNotify & " " & _
                             "WHERE error_fingerprint = '" & thisFingerPrint & "'"
    
    objCommand.Execute
  
End if

Call endConnections()

' -------------------- SEND EMAIL -------------------------

MailFrom = "Error Page <error.page@assessnet.co.uk>"
MailTo = "Development <development@assessnet.co.uk>"
MailSubject = "Error Page - " & Date()
MailBody = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN""><html><head></head><body>" & _
           thisDescription & _
           "</body></html>"

'Set objSend = Server.CreateObject("CDONTS.Newmail")
'
'objSend.Importance = 1  '0 - Low, 1 - Norm, 2 - High
'objSend.BodyFormat = 0  '0 for HTML , 1 for Plain text
'objSend.MailFormat = 0
'objSend.From = mailFrom
'objSend.To = mailTo
'objSend.Subject = mailSubject
'objSend.body = mailBody
'objSend.send
'
'Set objSend = Nothing

    Set objSend = Server.CreateObject("CDO.Message")

    objSend.MimeFormatted = True
    objSend.BodyPart.Charset = "utf-8"
    objSend.Fields("urn:schemas:mailheader:priority").Value = 2

    objSend.To = MailTo
    objSend.BCC = "mailcatcher@assessnet.co.uk"
    objSend.From = MailFrom
    
    objSend.Subject = MailSubject
    objSend.HtmlBody = MailBody

    'Name or IP of remote SMTP server
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "localhost"
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
    'Server port
    objSend.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 
    objSend.Configuration.Fields.Update

    objSend.Send

    set objSend = nothing  

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>AssessNET</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/version3.2/security/login/css/login.css" media="all" />
</head>

<body class = 'nobanner'>


<br />
<br />
 
 <!-- Box -->
    <div id='box_top_brn'>
    <div id='box_bottom_brn'>
    <div id='box_main'>

    <form action='frm_lg_session.asp' method='get' id='reg-done' >
    
    <div id='reg'>

<h1>AssessNET cannot complete your request<span>...</span></h1>

<p>The most likely reasons are:</p>

<ul class="error">
<li>You omitted some data on the previous page.</li>
<li>You entered some invalid data / dates on the previous page.</li>
<li>You exceeded the maximum character limit of a text area.</li>
</ul>


<p><img align="middle" src="/version3.2/images/icons/source.gif" /> &nbsp; 
   <a href='javascript:history.back()'>Go back to previous page and check my data...</a></p>

<h3>If you see this page again...</h3>
<p>Whenever AssessNET cannot show the required page, an e-mail is sent to the development team
   informing them where you are experiencing difficulties. Any issues are then resolved as 
   quickly as possible.</p>
<p>We suggest you try the link again in a few hours, by which time our development team may have been able to resolve the problem.</p>

</div>
    
    </form>

    
</div>
</div>
</div>
<!-- End Box -->

<div id='statement'>
    <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all login attempts are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a></p> 



</div>

<%
'If Application("CONFIG_PROVIDER") = "http://dev.assessweb.co.uk:8080" OR Application("CONFIG_PROVIDER") = "http://test.assessweb.co.uk:8080" Then
    Response.Write "<!-- <hr />" & errorTitle & "<p>" & errorBody & "</p> -->"
'End if
%>

</body>
</html>
<!--
<%= shortError %>
-->