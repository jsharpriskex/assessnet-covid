<%

ON ERROR RESUME NEXT

' ------------------------------------------------------
' --------------- LOG ERROR TO DATABASE ----------------

Dim objConn,objCommand

SET objconn = Server.CreateObject("ADODB.Connection")
WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
END WITH

SET objCommand = Server.CreateObject("ADODB.Command")
SET objCommand.ActiveConnection = objconn


objCommand.CommandText = "INSERT INTO " & APPLICATION("DBTABLE_LOG_404") & " (DATETIME,PAGE,REFERER,CORP_CODE,ACCOUNT_ID,USER_AGENT) " & _
                         " VALUES (GETDATE(),'" & Right(Request.ServerVariables("QUERY_STRING"),Len(Request.ServerVariables("QUERY_STRING")) - 4) & "','" & Request.ServerVariables("HTTP_REFERER") & "','" & SESSION("CORP_CODE") & "','" & SESSION("YOUR_ACCOUNT_ID") & "','" & Request.ServerVariables("HTTP_USER_AGENT") & "')"
objCommand.Execute

objCommand.ActiveConnection.close

SET objCommand.ActiveConnection = NOTHING
SET objCommand = NOTHING
SET objConn = NOTHING

' ----------------------- END --------------------------
' ------------------------------------------------------
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>AssessNET 404 Error</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel='stylesheet' href="/version3.2/data/scripts/css/global.css" media="all" />
    <style type='text/css'>
        .full { width: 95%; }
    </style>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width='100%'>
  <tr>
    <td><div id='lost'>
      <strong>Lost?</strong> You are in ::
      <a href="#">AssessNET</a> / 
      404 - Page Not Found
      </div>
    </td>
    <td width='180' align='right'><div id='corplogo'></div></td>
  </tr>
</table>
 
<h1>AssessNET cannot find the page you requested<span>...</span></h1>

<p>This could be due to one the following reasons:</p>
<ul>
    <li>The link you clicked is currently not working</li>
    <li>You clicked on a bookmark or shortcut to a page that has been moved</li>
</ul>

<p>We suggest you go <a href='javascript:history.back()'>back to the previous page</a> and try clicking the link
again. If you still see this message, we suggest you try the link again in a few hours, by which time the 
AssessNET development team would have had a chance to resolve the problem.</p>

<p>The AssessNET development team has been alerted of the missing page.</p>

<p class='disabled'>404 Page Not Found</p>
</body>
</html>
