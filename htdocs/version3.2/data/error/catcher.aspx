<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>AssessNET</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<link rel="stylesheet" href="/version<%= Application("CONFIG_VERSION") %>/data/scripts/css/global.css" media="all" />
    <style type='text/css'>
        .full { width: 95%; }
    </style>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td><div id="lost">Follow the instructions below to return to previous page</div>
    </td>
    <td width='180' align='right'><div id='corplogo'></div></td>
  </tr>
</table>
 
<h1>ASPX - AssessNET cannot complete your request<span>...</span></h1>

<p>The most likely reason is that you omitted some data on the previous page.</p>

<p><img align="middle" src="/version3.2/images/icons/source.gif" /> &nbsp; 
   <a href='javascript:history.back()'>Go back to previous page and check my data...</a></p>

<h3>If you see this page again...</h3>
<p>Whenever AssessNET cannot show the required page, an e-mail is sent to the development team
   informing them where you are experiencing difficulties. Any issues are then resolved as 
   quickly as possible.</p>
<p>You may therefore wish to return to this page in the near future.</p>

</body>
</html>