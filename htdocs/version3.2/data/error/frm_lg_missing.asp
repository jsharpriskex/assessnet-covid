<%

ON ERROR RESUME NEXT

' ------------------------------------------------------
' --------------- LOG ERROR TO DATABASE ----------------

Dim objConn,objCommand

SET objconn = Server.CreateObject("ADODB.Connection")
WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
END WITH

SET objCommand = Server.CreateObject("ADODB.Command")
SET objCommand.ActiveConnection = objconn


objCommand.CommandText =  "INSERT INTO " & APPLICATION("DBTABLE_LOG_404") & " (DATETIME,PAGE,REFERER,CORP_CODE,ACCOUNT_ID,USER_AGENT) " & _
                                            "VALUES (GETDATE(),'" & Right(Request.ServerVariables("QUERY_STRING"),Len(Request.ServerVariables("QUERY_STRING")) - 4) & "','" & Request.ServerVariables("HTTP_REFERER") & "','" & SESSION("CORP_CODE") & "','" & SESSION("YOUR_ACCOUNT_ID") & "','" & Request.ServerVariables("HTTP_USER_AGENT") & "')"
objCommand.Execute

objCommand.ActiveConnection.close

SET objCommand.ActiveConnection = NOTHING
SET objCommand = NOTHING
SET objConn = NOTHING

' ----------------------- END --------------------------
' ------------------------------------------------------
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %> <% =Application("SoftwareOwner") %> <% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="css/login.css" media="all" />
    

</head>
<body class='nobanner'>

<br />
<br />

<!-- Box -->
<div id='box_top_brn'>
<div id='box_bottom_brn'>
<div id='box_main'>

    <form action='frm_lg_session.asp' method='get' id='reg-done' >
    
    <div id='reg'>
    <h1>Page cannot be found</h1>
    <h6>404 Error has occured</h6>
    
    <input type="hidden" name="destination" value="/frm_lg_session.asp" />
    
    <p>The reasons for this may include:</p>
    
    <br />
    
    <p><strong>Reason one;</strong><br />
    The link / form you have used is not currently working.</p>
    
    <p><strong>Reason two;</strong><br />
    You tried to enter AssessNET via a Bookmark, Shortcut or Favourite</p>

    <p>We suggest you go <a href='history.back()'>back to the previous page</a> and try clicking the link again. If you still see this message, we suggest you try the link again in a few hours, by which time our development team would have had a chance to resolve the problem.</p>
    
    <br />
    
    </div>
    
    </form>

    
</div>
</div>
</div>
<!-- end box -->

<div id='statement'>
    <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all login attempts are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a></p> 



</div>

</body>
</html>