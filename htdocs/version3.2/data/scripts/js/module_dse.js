


// Stop people looking at source

function right(e) 
	{
	if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2))
	return false;
		else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2 || event.button == 3)) {
		alert("(c) Copyright 2003 Safe and Sound Management Consultants Ltd");
		return false;
		}
	return true;
	}

//document.onmousedown=right;
//document.onmouseup=right;
//if (document.layers) window.captureEvents(Event.MOUSEDOWN);
//if (document.layers) window.captureEvents(Event.MOUSEUP);
//window.onmousedown=right;
//window.onmouseup=right;

function isEmail(string) {
    return  (string.search(/^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/) != -1)
}

function isUserEmail(string) {
    return  (string.search(/^[A-Za-z0-9]+[-_.A-Za-z0-9]*$/) != -1)
}

function valPASS(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following field:\n"
  if ("" == myForm.FRM_PASS.value) {
    valid = 0
    myForm.FRM_PASS.style.background = col0
    errMsg += "\n  - No Password"
  } else {
    myForm.FRM_PASS.style.background = col1
  }
   
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valPASSWORD(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  if ("" == myForm.FRM_REGPASS.value) {
    valid = 0
    myForm.FRM_REGPASS.style.background = col0
    errMsg += "\n  - Password"
  } else {
    myForm.FRM_REGPASS.style.background = col1
  }
  if ("" == myForm.FRM_REGPASS_CONF.value) {
    valid = 0
    myForm.FRM_REGPASS_CONF.style.background = col0
    errMsg += "\n  - Confirm Password"
  } else {
    myForm.FRM_REGPASS_CONF.style.background = col1
  }
  if (myForm.FRM_REGPASS.value == myForm.FRM_REGPASS_CONF.value) {
  	myForm.FRM_REGPASS.style.background = col1
  	myForm.FRM_REGPASS_CONF.style.background = col1
  } else {
  	valid = 0
    myForm.FRM_REGPASS.style.background = col0
  	myForm.FRM_REGPASS_CONF.style.background = col0
  	errMsg += "\n - Passwords do not match"
  }
   
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valVALIDATION(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  if ("" == myForm.FRM_VAL_CODE.value) {
    valid = 0
    myForm.FRM_VAL_CODE.style.background = col0
    errMsg += "\n  - Validation Code"
  } else {
    myForm.FRM_VAL_CODE.style.background = col1
  }
 
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valLOGIN(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  if ("" == myForm.CAMPAIGN_ID.value) {
    valid = 0
    myForm.CAMPAIGN_ID.style.background = col0
    errMsg += "\n  - DSE Campaign ID"
  } else {
    myForm.CAMPAIGN_ID.style.background = col1
  }
//  if ("" == myForm.FRM_EMAILADDRESS.value || !isEmail(myForm.FRM_EMAILADDRESS.value)) {
//  	valid = 0
//  	myForm.FRM_EMAILADDRESS.style.background = col0
//  	errMsg += "\n  - A valid company E-mail address"
//  } else {
//  	myForm.FRM_EMAILADDRESS.style.background = col1
// }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valREGISTRATION(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  if ("" == myForm.FRM_TITLE.value) {
    valid = 0
    myForm.FRM_TITLE.style.background = col0
    errMsg += "\n  - Your Title"
  } else {
    myForm.FRM_TITLE.style.background = col1
  }
  if ("" == myForm.FRM_FORENAME.value) {
  	valid = 0
  	myForm.FRM_FORENAME.style.background = col0
  	errMsg += "\n  - Your Forename"
  } else {
  	myForm.FRM_FORENAME.style.background = col1
  }
  if ("" == myForm.FRM_SURNAME.value) {
  	valid = 0
  	myForm.FRM_SURNAME.style.background = col0
  	errMsg += "\n  - Your Surname"
  } else {
  	myForm.FRM_SURNAME.style.background = col1
  }
  if ("" == myForm.FRM_DEPARTMENT.value) {
  	valid = 0
  	myForm.FRM_DEPARTMENT.style.background = col0
  	errMsg += "\n  - Your Department"
  } else {
  	myForm.FRM_DEPARTMENT.style.background = col1
  }
  if ("" == myForm.FRM_EMAILUSERNAME.value || !isUserEmail(myForm.FRM_EMAILUSERNAME.value)) {
	valid = 0
	myForm.FRM_EMAILUSERNAME.style.background = col0
	errMsg += "\n  - A valid company E-mail Address"
  } else {
	myForm.FRM_EMAILUSERNAME.style.background = col1
  }
  if ("ERR" == myForm.FRM_EMAILDOMAIN.value) {
  	valid = 0
  	myForm.FRM_EMAILDOMAIN.style.background = col0
  	errMsg += "\n  - Select your E-mail Domain"
  } else {
  	myForm.FRM_EMAILDOMAIN.style.background = col1
  }
  if ("" == myForm.FRM_PHONENUMBER.value) {
  	valid = 0
  	myForm.FRM_PHONENUMBER.style.background = col0
  	errMsg += "\n  - A valid company contact number"
  } else {
  	myForm.FRM_PHONENUMBER.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

// Comment Window
 
function openComment(ref,question)
{
var comment = question
var reference = ref
window.open('/version3.2/modules/hsafety/dse_assessment/external/dsecomment.asp?ref=' + reference + '&id=' + comment + '', 'window', 'width=413,height=210,screenX=300,screenY=300,left=300,top=300,scrollbars=no,resizable=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

// Print Report Window
   
function show_PRINT(ref)
{
var reference = ref
window.open('/version3.2/modules/hsafety/dse_assessment/standard/create_print.asp?ref=' + reference + '', 'window', 'width=650,height=500,screenX=70,screenY=20,left=70,top=20,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 

// View and Edit the Summary

function openSummary(ref)
{
var reference = ref
window.open('/version3.2/modules/hsafety/dse_assessment/external/dsesummary.asp?code=' + reference + '', 'window', 'width=566,height=220,screenX=200,screenY=200,left=200,top=200,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

// Edit - Create Action

function openAction(question,reference)
{
var comment = question
var ref = reference
window.open('/version3.2/modules/hsafety/dse_assessment/external/dseaction.asp?code=' + ref + '&id=' + comment + '', 'window', 'width=567,height=232,screenX=200,screenY=200,left=200,top=200,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 


function fixQuestion(question,reference)
{
var comment = question
var ref = reference
    if (confirm('Are you sure this issue has been resolved' + '\n' + 'and can be removed from the assessment?')) {
        parent.main.location.href=('/version3.2/modules/hsafety/dse_assessment/standard/create_report.asp?REF=' + ref + '&ID=' + comment + '&cmd=F')
    } else {
    
    }
}

// Help Window

function open_help(type)
{
var type
window.open('/version3.2/data/helps/system/account_help.asp?type=' + type + '', 'window', 'width=408,height=158,screenX=340,screenY=300,left=340,top=300,scrollbars=auto,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 

// Training Windows

function Open_Training ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-health-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training1 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-monitor-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 
         
function Open_Training2 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-keyboard-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training3 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-chair-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 
         
function Open_Training4 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-footrest-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training5 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-worksurface-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}
         
function Open_Training6 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-software-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training7 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-workposture-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 
         
function Open_Training8 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-workroutine-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 
         
function Open_Training9 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-noise-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training10 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-lighting-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 

function Open_Training11 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-uconsiderations-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 

function Open_Training12 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-heatvent-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function Open_Training13 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-electricity-st1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
} 


// Medical Info windows
    
function Open_Info1 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-info1.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}
         
function Open_Info2 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-info2.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}
         
function Open_Info3 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-info3.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}
         
function Open_Info4 ()
{
window.open('/version3.2/modules/hsafety/dse_assessment/training/tr_dse-info4.asp', 'window', 'width=750,height=435,screenX=20,screenY=50,left=20,top=50,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}