function valINJURY(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INJFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Forename"
  	myForm.INJFNAME.style.background = col0
  } else {
	myForm.INJFNAME.style.background = col1
  }
   
  if ("" == myForm.INJSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Surname"
  	myForm.INJSNAME.style.background = col0
  } else {
	myForm.INJSNAME.style.background = col1
  }
 
  if ("" == myForm.INJADDRESS1.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Street"
  	myForm.INJADDRESS1.style.background = col0
  } else {
	myForm.INJADDRESS1.style.background = col1
  }
  
  if ("" == myForm.INJADDRESS2.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Town / City"
  	myForm.INJADDRESS2.style.background = col0
  } else {
	myForm.INJADDRESS2.style.background = col1
  }
  
  if ("" == myForm.INJADDRESS3.value) {
  	valid = 0 
  	errMsg += "\n - Section 1 : County"
  	myForm.INJADDRESS3.style.background = col0
  } else {
	myForm.INJADDRESS3.style.background = col1
  }
  
  if ("" == myForm.INJPOSTCODE.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Postcode"
  	myForm.INJPOSTCODE.style.background = col0
  } else {
	myForm.INJPOSTCODE.style.background = col1
  }
  
  if ("" == myForm.INJOCCUPATION.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Occupation"
  	myForm.INJOCCUPATION.style.background = col0
  } else {
	myForm.INJOCCUPATION.style.background = col1
  }

  if ("" == myForm.INJSTATUS.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Status"
  	myForm.INJSTATUS.style.background = col0
  } else {
	myForm.INJSTATUS.style.background = col1
  }

  if ("" == myForm.INJGENDER.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Gender"
  	myForm.INJGENDER.style.background = col0
  } else {
	myForm.INJGENDER.style.background = col1
  }
  
  if ("" == myForm.REPFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Forename"
  	myForm.REPFNAME.style.background = col0
  } else {
	myForm.REPFNAME.style.background = col1
  }
  
  if ("" == myForm.REPSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Surname"
  	myForm.REPSNAME.style.background = col0
  } else {
	myForm.REPSNAME.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS1.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Street"
  	myForm.REPADDRESS1.style.background = col0
  } else {
	myForm.REPADDRESS1.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS2.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Town / City"
  	myForm.REPADDRESS2.style.background = col0
  } else {
	myForm.REPADDRESS2.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS3.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : County"
  	myForm.REPADDRESS3.style.background = col0
  } else {
	myForm.REPADDRESS3.style.background = col1
  }
  
  if ("" == myForm.REPPOSTCODE.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Postcode"
  	myForm.REPPOSTCODE.style.background = col0
  } else {
	myForm.REPPOSTCODE.style.background = col1
  }
  
  if ("" == myForm.REPOCCUPATION.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Occupation"
  	myForm.REPOCCUPATION.style.background = col0
  } else {
	myForm.REPOCCUPATION.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 4 : inValid Incident Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if ("" == myForm.INJSTAGE4.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : About the accident"
  	myForm.INJSTAGE4.style.background = col0
  } else {
	myForm.INJSTAGE4.style.background = col1
  }
  
  if ("" == myForm.INJTYPE.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : Type of Injury"
  	myForm.INJTYPE.style.background = col0
  } else {
	myForm.INJTYPE.style.background = col1
  }
  
  if ("" == myForm.INJCAUSE.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : Apparent Cause of Injury"
  	myForm.INJCAUSE.style.background = col0
  } else {
	myForm.INJCAUSE.style.background = col1
  }

  if ("" == myForm.INJBODY.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : Body Part"
  	myForm.INJBODY.style.background = col0
  } else {
	myForm.INJBODY.style.background = col1
  }
   
  if ("" == myForm.INJSTAGE4B.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : How the accident happened"
  	myForm.INJSTAGE4B.style.background = col0
  } else {
	myForm.INJSTAGE4B.style.background = col1
  }
  
  if ("" == myForm.INJSTAGE5.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Materials used in treatment"
  	myForm.INJSTAGE5.style.background = col0
  } else {
	myForm.INJSTAGE5.style.background = col1
  }
  
  if ("NA" == myForm.INJRIDDORID.value) {
  	valid = 0
  	errMsg += "\n - Section 6 : Is this RIDDOR Reportable?"
  	myForm.INJRIDDORID.style.background = col0
  } else {
	myForm.INJRIDDORID.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valILLNESS(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.IDFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Forename"
  	myForm.IDFNAME.style.background = col0
  } else {
	myForm.IDFNAME.style.background = col1
  }
   
  if ("" == myForm.IDSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Surname"
  	myForm.IDSNAME.style.background = col0
  } else {
	myForm.IDSNAME.style.background = col1
  }
 
  if ("" == myForm.IDADDRESS1.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Street"
  	myForm.IDADDRESS1.style.background = col0
  } else {
	myForm.IDADDRESS1.style.background = col1
  }
  
  if ("" == myForm.IDADDRESS2.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Town / City"
  	myForm.IDADDRESS2.style.background = col0
  } else {
	myForm.IDADDRESS2.style.background = col1
  }
  
  if ("" == myForm.IDADDRESS3.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : County"
  	myForm.IDADDRESS3.style.background = col0
  } else {
	myForm.IDADDRESS3.style.background = col1
  }
  
  if ("" == myForm.IDPOSTCODE.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Postcode"
  	myForm.IDPOSTCODE.style.background = col0
  } else {
	myForm.IDPOSTCODE.style.background = col1
  }
  
  if ("" == myForm.IDOCCUPATION.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Occupation"
  	myForm.IDOCCUPATION.style.background = col0
  } else {
	myForm.IDOCCUPATION.style.background = col1
  }

  if ("" == myForm.IDSTATUS.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Status"
  	myForm.IDSTATUS.style.background = col0
  } else {
	myForm.IDSTATUS.style.background = col1
  }

  if ("" == myForm.IDGENDER.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Gender"
  	myForm.IDGENDER.style.background = col0
  } else {
	myForm.IDGENDER.style.background = col1
  }
  
  if ("" == myForm.REPFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Forename"
  	myForm.REPFNAME.style.background = col0
  } else {
	myForm.REPFNAME.style.background = col1
  }
  
  if ("" == myForm.REPSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Surname"
  	myForm.REPSNAME.style.background = col0
  } else {
	myForm.REPSNAME.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS1.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Street"
  	myForm.REPADDRESS1.style.background = col0
  } else {
	myForm.REPADDRESS1.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS2.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Town / City"
  	myForm.REPADDRESS2.style.background = col0
  } else {
	myForm.REPADDRESS2.style.background = col1
  }
  
  if ("" == myForm.REPADDRESS3.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : County"
  	myForm.REPADDRESS3.style.background = col0
  } else {
	myForm.REPADDRESS3.style.background = col1
  }
  
  if ("" == myForm.REPPOSTCODE.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Postcode"
  	myForm.REPPOSTCODE.style.background = col0
  } else {
	myForm.REPPOSTCODE.style.background = col1
  }
  
  if ("" == myForm.REPOCCUPATION.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Occupation"
  	myForm.REPOCCUPATION.style.background = col0
  } else {
	myForm.REPOCCUPATION.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 4 : inValid Incident Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if ("" == myForm.IDSTAGE4.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : About the illness"
  	myForm.IDSTAGE4.style.background = col0
  } else {
	myForm.IDSTAGE4.style.background = col1
  }
  
  if ("" == myForm.IDSTAGE4B.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : How the illness occured"
  	myForm.IDSTAGE4B.style.background = col0
  } else {
	myForm.IDSTAGE4B.style.background = col1
  }
  
  if ("" == myForm.IDSTAGE5.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Materials used in treatment"
  	myForm.IDSTAGE5.style.background = col0
  } else {
	myForm.IDSTAGE5.style.background = col1
  }
  
  if ("NA" == myForm.IDRIDDORID.value) {
  	valid = 0
  	errMsg += "\n - Section 6 : Is this RIDDOR Reportable?"
  	myForm.IDRIDDORID.style.background = col0
  } else {
	myForm.IDRIDDORID.style.background = col1
  }
     
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function WARNsigned() 
{
    alert("Please take note:" + '\n' + "This injury entry will be maked as UNSIGNED until the user has signed the hard copy," + '\n' + "at which time you should edit this entry for your records.")
}
   
function OpenCAT()
{
   	window.open('doccurrence/external/picklist.asp', 'window', 'width=700,height=550,screenX=200,screenY=300,left=200,top=30,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}
         
function showDEPTnotice()
{
   	window.open('picklist.asp', 'window', 'width=700,height=550,screenX=200,screenY=300,left=200,top=30,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}

function delRECORD(REC_ID,REC_REF)
{
	var name = confirm("Delete this record ? (" + REC_REF + ")")
	if (name == true)
	{
		parent.main.location.href=("accident_manager.asp?CMD=DEL&REC_ID=" + REC_ID + "&REC_REF=" + REC_REF)
	} else {
		
	}
}


function valNEARMISSs1(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.NMDESCRIPTION.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : describe what happened"
  	myForm.NMDESCRIPTION.style.background = col0
  } else {
	myForm.NMDESCRIPTION.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 1 : inValid Incident Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if ("NA" == myForm.NMTYPE.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : how serious was the Near Miss"
  	myForm.NMTYPE.style.background = col0
  } else {
	myForm.NMTYPE.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valNEARMISSs2(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.NMREPFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Forename"
  	myForm.NMREPFNAME.style.background = col0
  } else {
	myForm.NMREPFNAME.style.background = col1
  }
   
  if ("" == myForm.NMREPSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Surname"
  	myForm.NMREPSNAME.style.background = col0
  } else {
	myForm.NMREPSNAME.style.background = col1
  }
  
  if ("" == myForm.NMREPPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Contact Number"
  	myForm.NMREPPHONE.style.background = col0
  } else {
	myForm.NMREPPHONE.style.background = col1
  }
  
  if ("" == myForm.NMACTIONS.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : What action has been taken"
  	myForm.NMACTIONS.style.background = col0
  } else {
	myForm.NMACTIONS.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 4 : inValid Undertaken Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valNEARMISSs3(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.NMWITFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Forename"
  	myForm.NMWITFNAME.style.background = col0
  } else {
	myForm.NMWITFNAME.style.background = col1
  }
   
  if ("" == myForm.NMWITSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Surname"
  	myForm.NMWITSNAME.style.background = col0
  } else {
	myForm.NMWITSNAME.style.background = col1
  }
  
  if ("" == myForm.NMWITPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Contact Number"
  	myForm.NMWITPHONE.style.background = col0
  } else {
	myForm.NMWITPHONE.style.background = col1
  }
   
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valDAMAGEs1(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DAMDESCRIPTION.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : describe what happened"
  	myForm.DAMDESCRIPTION.style.background = col0
  } else {
	myForm.DAMDESCRIPTION.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 1 : inValid Incident Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if ("NA" == myForm.DAMTYPE.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : how serious was the damage"
  	myForm.DAMTYPE.style.background = col0
  } else {
	myForm.DAMTYPE.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valDAMAGEs2(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DAMREPFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Forename"
  	myForm.DAMREPFNAME.style.background = col0
  } else {
	myForm.DAMREPFNAME.style.background = col1
  }
   
  if ("" == myForm.DAMREPSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Surname"
  	myForm.DAMREPSNAME.style.background = col0
  } else {
	myForm.DAMREPSNAME.style.background = col1
  }
  
  if ("" == myForm.DAMREPPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Contact Number"
  	myForm.DAMREPPHONE.style.background = col0
  } else {
	myForm.DAMREPPHONE.style.background = col1
  }
  
  if ("" == myForm.DAMACTIONS.value) {
  	valid = 0
  	errMsg += "\n - Section 4 : What action has been taken"
  	myForm.DAMACTIONS.style.background = col0
  } else {
	myForm.DAMACTIONS.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 4 : inValid Undertaken Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valDAMAGEs3(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DAMWITFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Forename"
  	myForm.DAMWITFNAME.style.background = col0
  } else {
	myForm.DAMWITFNAME.style.background = col1
  }
   
  if ("" == myForm.DAMWITSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Surname"
  	myForm.DAMWITSNAME.style.background = col0
  } else {
	myForm.DAMWITSNAME.style.background = col1
  }
  
  if ("" == myForm.DAMWITPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Contact Number"
  	myForm.DAMWITPHONE.style.background = col0
  } else {
	myForm.DAMWITPHONE.style.background = col1
  }
   
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valDOCCURRENCEs1(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DODESCRIPTION.value) {
  	valid = 0
  	errMsg += "\n - Section 2 : Describe what happened"
  	myForm.DODESCRIPTION.style.background = col0
  } else {
	myForm.DODESCRIPTION.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valDOCCURRENCEs2(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DOREPFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Forename"
  	myForm.DOREPFNAME.style.background = col0
  } else {
	myForm.DOREPFNAME.style.background = col1
  }
  
  if ("" == myForm.DOREPSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Surname"
  	myForm.DOREPSNAME.style.background = col0
  } else {
	myForm.DOREPSNAME.style.background = col1
  }

  if ("" == myForm.DOREPADDRESS1.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Street"
  	myForm.DOREPADDRESS1.style.background = col0
  } else {
	myForm.DOREPADDRESS1.style.background = col1
  }

  if ("" == myForm.DOREPADDRESS2.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Town / City"
  	myForm.DOREPADDRESS2.style.background = col0
  } else {
	myForm.DOREPADDRESS2.style.background = col1
  }
  
  if ("" == myForm.DOREPADDRESS3.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : County"
  	myForm.DOREPADDRESS3.style.background = col0
  } else {
	myForm.DOREPADDRESS3.style.background = col1
  }
  
  if ("" == myForm.DOREPPOSTCODE.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Postcode"
  	myForm.DOREPPOSTCODE.style.background = col0
  } else {
	myForm.DOREPPOSTCODE.style.background = col1
  }

  if ("" == myForm.DOREPOCCUPATION.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Occupation"
  	myForm.DOREPOCCUPATION.style.background = col0
  } else {
	myForm.DOREPOCCUPATION.style.background = col1
  }

  if ("" == myForm.DOREPWPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Work Contact Number"
  	myForm.DOREPWPHONE.style.background = col0
  } else {
	myForm.DOREPWPHONE.style.background = col1
  }
  
  if ("" == myForm.DOREPHPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 3 : Home Contact Number"
  	myForm.DOREPHPHONE.style.background = col0
  } else {
	myForm.DOREPHPHONE.style.background = col1
  }
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Section 4 : inValid Date Reported"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valDOCCURRENCEs3(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.DOWITFNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Forename"
  	myForm.DOWITFNAME.style.background = col0
  } else {
	myForm.DOWITFNAME.style.background = col1
  }
 
  if ("" == myForm.DOWITSNAME.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Surname"
  	myForm.DOWITSNAME.style.background = col0
  } else {
	myForm.DOWITSNAME.style.background = col1
  }
  
  if ("" == myForm.DOWITWPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Work Contact Number"
  	myForm.DOWITWPHONE.style.background = col0
  } else {
	myForm.DOWITWPHONE.style.background = col1
  }

  if ("" == myForm.DOWITHPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 5 : Home Contact Number"
  	myForm.DOWITHPHONE.style.background = col0
  } else {
	myForm.DOWITHPHONE.style.background = col1
  }
 
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valRIDDORs1(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.frm_fname.value) {
  	valid = 0
  	errMsg += "\n - Part A : Full Name"
  	myForm.frm_fname.style.background = col0
  } else {
	myForm.frm_fname.style.background = col1
  }
  
  if ("" == myForm.frm_jtitle.value) {
  	valid = 0
  	errMsg += "\n - Part A : Job Title"
  	myForm.frm_jtitle.style.background = col0
  } else {
	myForm.frm_jtitle.style.background = col1
  }
  
  if ("" == myForm.frm_pnumber.value) {
  	valid = 0
  	errMsg += "\n - Part A : Contact number"
  	myForm.frm_pnumber.style.background = col0
  } else {
	myForm.frm_pnumber.style.background = col1
  }
   
  if ("" == myForm.frm_orgaddpost.value) {
  	valid = 0
  	errMsg += "\n - Part A : Address and Postcode"
  	myForm.frm_orgaddpost.style.background = col0
  } else {
	myForm.frm_orgaddpost.style.background = col1
  }
  
  if ("" == myForm.frm_orgworktype.value) {
  	valid = 0
  	errMsg += "\n - Part A : Organisation Work Type"
  	myForm.frm_orgworktype.style.background = col0
  } else {
	myForm.frm_orgworktype.style.background = col1
  }
  
 if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
  	valid = 0
  	errMsg += "\n - Part B : inValid Date"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY.style.background = col1
  	myForm.RECORDDATE_MONTH.style.background = col1
  	myForm.RECORDDATE_YEAR.style.background = col1
  }
  
  if ("" == myForm.frm_happenlocal.value) {
  	valid = 0
  	errMsg += "\n - Part B : Where did the accident happen"
  	myForm.frm_happenlocal.style.background = col0
  } else {
	myForm.frm_happenlocal.style.background = col1
  }
  
  if ("" == myForm.frm_happenaddr.value) {
  	valid = 0
  	errMsg += "\n - Part B : What address did the accident happen at"
  	myForm.frm_happenaddr.style.background = col0
  } else {
	myForm.frm_happenaddr.style.background = col1
  }
  
  if ("" == myForm.frm_lauthority.value) {
  	valid = 0
  	errMsg += "\n - Part B : Local authority name or postcode"
  	myForm.frm_lauthority.style.background = col0
  } else {
	myForm.frm_lauthority.style.background = col1
  }
  
  if ("" == myForm.frm_acclocation.value) {
  	valid = 0
  	errMsg += "\n - Part B : Where on the premises did it happen"
  	myForm.frm_acclocation.style.background = col0
  } else {
	myForm.frm_acclocation.style.background = col1
  }
    
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function getADDRRIDDOR()
{
  if (riddorform.frm_happenlocal.value == "1") {
  riddorform.frm_happenaddr.value = riddorform.frm_orgaddpost.value
  } else {
  
  }
}

function valACCIDENT(myForm)
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
 	valid = 0
  	errMsg += "\n - No Date Selected"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
 
	var get_DATE = new Date();
  	var frm_DATE = new Date();
  	var frm_MONTH = myForm.RECORDDATE_MONTH.value - 1

  	frm_DATE.setDate(myForm.RECORDDATE_DAY.value);
  	frm_DATE.setMonth(frm_MONTH);
  	frm_DATE.setFullYear(myForm.RECORDDATE_YEAR.value);
  	
  	if (frm_DATE > get_DATE) {
  		valid = 0
  		errMsg += "\n - Date selected is in the future"
  		myForm.RECORDDATE_DAY.style.background = col0
  		myForm.RECORDDATE_MONTH.style.background = col0
  		myForm.RECORDDATE_YEAR.style.background = col0
  	} else {
  		myForm.RECORDDATE_DAY.style.background = col1
  		myForm.RECORDDATE_MONTH.style.background = col1
  		myForm.RECORDDATE_YEAR.style.background = col1
  	}
  
  }
  
  if ("" == myForm.REC_BD.value) {
  	valid = 0
  	errMsg += "\n - No Department Selected"
  	myForm.REC_BD.style.background = col0
  } else {
	myForm.REC_BD.style.background = col1
  }
  
  if ("" == myForm.REC_INCLOC.value) {
  	valid = 0
  	errMsg += "\n - No location specified (e.g. Reception)"
  	myForm.REC_INCLOC.style.background = col0
  } else {
	myForm.REC_INCLOC.style.background = col1
  }
  
  if (myForm.INJ_VALUE[0].checked == false && myForm.ID_VALUE[0].checked == false && 
      myForm.DO_VALUE[0].checked == false && myForm.DAM_VALUE[0].checked == false && 
      myForm.NM_VALUE[0].checked == false) {
  	valid = 0
  	errMsg += "\n - At least one of questions 4-8 must be 'Yes'"
  } else {

  }
  
  if (myForm.INJ_VALUE[0].checked == true && myForm.NM_VALUE[0].checked == true || myForm.ID_VALUE[0].checked == true && myForm.NM_VALUE[0].checked == true || myForm.DO_VALUE[0].checked == true && myForm.NM_VALUE[0].checked == true || myForm.DAM_VALUE[0].checked == true && myForm.NM_VALUE[0].checked == true) {
  	valid = 0
  	errMsg += "\n - Invalid choice: a Near Miss cannot occur with another Incident"
  	myForm.NM_VALUE[0].style.background = col0
  } else {
  	myForm.NM_VALUE[0].style.background = col1
  }
 
  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valriddor1(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = ''
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.RRINJPHONE.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Home Phone"
  	myForm.RRINJPHONE.style.background = col0
  } else {
	myForm.RRINJPHONE.style.background = col1
  }

  if ("" == myForm.RRINJAGE.value) {
  	valid = 0
  	errMsg += "\n - Section 1 : Age"
  	myForm.RRINJAGE.style.background = col0
  } else {
	myForm.RRINJAGE.style.background = col1
  }

  if (myForm.RRINJOCCSTATUS[0].checked == false && myForm.RRINJOCCSTATUS[1].checked == false && myForm.RRINJOCCSTATUS[2].checked == false && myForm.RRINJOCCSTATUS[3].checked == false && myForm.RRINJOCCSTATUS[4].checked == false && myForm.RRINJOCCSTATUS[5].checked == false) {
  	valid = 0
  	errMsg += "\n - Section 2 : Was the injured person? - select option"
  	myForm.RRINJOCCSTATUS[0].style.background = col0
	myForm.RRINJOCCSTATUS[1].style.background = col0
  	myForm.RRINJOCCSTATUS[2].style.background = col0
	myForm.RRINJOCCSTATUS[3].style.background = col0
	myForm.RRINJOCCSTATUS[4].style.background = col0
  	myForm.RRINJOCCSTATUS[5].style.background = col0
  } else {
  	myForm.RRINJOCCSTATUS[0].style.background = col1
	myForm.RRINJOCCSTATUS[1].style.background = col1
  	myForm.RRINJOCCSTATUS[2].style.background = col1
	myForm.RRINJOCCSTATUS[3].style.background = col1
	myForm.RRINJOCCSTATUS[4].style.background = col1
  	myForm.RRINJOCCSTATUS[5].style.background = col1
  }

  if (myForm.RRINCTYPE[0].checked == false && myForm.RRINCTYPE[1].checked == false && myForm.RRINCTYPE[2].checked == false && myForm.RRINCTYPE[3].checked == false) {
  	valid = 0
  	errMsg += "\n - Section 3 : Was the injury? - select option"
  	myForm.RRINCTYPE[0].style.background = col0
	myForm.RRINCTYPE[1].style.background = col0
  	myForm.RRINCTYPE[2].style.background = col0
	myForm.RRINCTYPE[3].style.background = col0
  } else {
  	myForm.RRINCTYPE[0].style.background = col1
	myForm.RRINCTYPE[1].style.background = col1
  	myForm.RRINCTYPE[2].style.background = col1
	myForm.RRINCTYPE[3].style.background = col1
  }

  if (myForm.RRINJSTATUS[0].checked == false && myForm.RRINJSTATUS[1].checked == false && myForm.RRINJSTATUS[2].checked == false && myForm.RRINJSTATUS[3].checked == false) {
  	valid = 0
  	errMsg += "\n - Section 2 : Did the injured person? - select option"
  	myForm.RRINJSTATUS[0].style.background = col0
	myForm.RRINJSTATUS[1].style.background = col0
  	myForm.RRINJSTATUS[2].style.background = col0
	myForm.RRINJSTATUS[3].style.background = col0
  } else {
  	myForm.RRINJSTATUS[0].style.background = col1
	myForm.RRINJSTATUS[1].style.background = col1
  	myForm.RRINJSTATUS[2].style.background = col1
	myForm.RRINJSTATUS[3].style.background = col1
  }

  if (myForm.RRINJOCCSTATUS[1].checked == true && "" == myForm.RRINJDETAILS1.value) {
     	valid = 0
	errMsg += "\n - Section 2 : Training scheme details"
	myForm.RRINJDETAILS1.style.background = col0
  } else {
	myForm.RRINJDETAILS1.style.background = '#ffffff'
  }


  if (myForm.RRINJOCCSTATUS[3].checked == true && "" == myForm.RRINJDETAILS2.value) {
     	valid = 0
	errMsg += "\n - Section 2 : employed by someone else? give details"
	myForm.RRINJDETAILS2.style.background = col0
  } else {
	myForm.RRINJDETAILS2.style.background = '#ffffff'
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valriddor2(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.RRFNAME.value) {
  	valid = 0
  	errMsg += "\n - Part A : Your Full Name"
  	myForm.RRFNAME.style.background = col0
  } else {
	myForm.RRFNAME.style.background = col1
  }

  if ("" == myForm.RRJTITLE.value) {
  	valid = 0
  	errMsg += "\n - Part A : Your Job Title"
  	myForm.RRJTITLE.style.background = col0
  } else {
	myForm.RRJTITLE.style.background = col1
  }
  
  if ("" == myForm.RRPNUMBER.value) {
  	valid = 0
  	errMsg += "\n - Part A : Your Full Contact Number"
  	myForm.RRPNUMBER.style.background = col0
  } else {
	myForm.RRPNUMBER.style.background = col1
  }

  if ("" == myForm.RRORGNAME.value) {
  	valid = 0
  	errMsg += "\n - Part A : Name of your organisation"
  	myForm.RRORGNAME.style.background = col0
  } else {
	myForm.RRORGNAME.style.background = col1
  }

  if ("" == myForm.RRORGADDPOST.value) {
  	valid = 0
  	errMsg += "\n - Part A : Address of your organisation"
  	myForm.RRORGADDPOST.style.background = col0
  } else {
	myForm.RRORGADDPOST.style.background = col1
  }

  if ("" == myForm.RRORGWORKTYPE.value) {
  	valid = 0
  	errMsg += "\n - Part A : Type of work your organisation does"
  	myForm.RRORGWORKTYPE.style.background = col0
  } else {
	myForm.RRORGWORKTYPE.style.background = col1
  }


  if (myForm.RECORDDATE_DAY.value == "DD" || myForm.RECORDDATE_MONTH.value == "MM" || myForm.RECORDDATE_YEAR.value == "YYYY") {
 	valid = 0
  	errMsg += "\n - Part B : No Date Selected"
  	myForm.RECORDDATE_DAY.style.background = col0
  	myForm.RECORDDATE_MONTH.style.background = col0
  	myForm.RECORDDATE_YEAR.style.background = col0
  } else {
 
	var get_DATE = new Date();
  	var frm_DATE = new Date();
  	var frm_MONTH = myForm.RECORDDATE_MONTH.value - 1

  	frm_DATE.setDate(myForm.RECORDDATE_DAY.value);
  	frm_DATE.setMonth(frm_MONTH);
  	frm_DATE.setFullYear(myForm.RECORDDATE_YEAR.value);
  	
  	if (frm_DATE > get_DATE) {
  		valid = 0
  		errMsg += "\n - Part B : Date selected is in the future"
  		myForm.RECORDDATE_DAY.style.background = col0
  		myForm.RECORDDATE_MONTH.style.background = col0
  		myForm.RECORDDATE_YEAR.style.background = col0
  	} else {
  		myForm.RECORDDATE_DAY.style.background = col1
  		myForm.RECORDDATE_MONTH.style.background = col1
  		myForm.RECORDDATE_YEAR.style.background = col1
  	}
  
  }


  if (myForm.RRHAPPENLOCAL[0].checked == false && myForm.RRHAPPENLOCAL[1].checked == false) {
  	valid = 0
  	errMsg += "\n - Part B : Did the incident happen at address in Part A? - select option"
  	myForm.RRHAPPENLOCAL[0].style.background = col0
	myForm.RRHAPPENLOCAL[1].style.background = col0
  } else {
  	myForm.RRHAPPENLOCAL[0].style.background = ''
	myForm.RRHAPPENLOCAL[1].style.background = ''
  }

  if ("" == myForm.RRACCLOCATION.value) {
  	valid = 0
  	errMsg += "\n - Part B : Department or Location of incident"
  	myForm.RRACCLOCATION.style.background = col0
  } else {
	myForm.RRACCLOCATION.style.background = col1
  }

  if ("SEL" == myForm.RRACCKIND.value) {
  	valid = 0
  	errMsg += "\n - Part E / G : Select accident type"
  	myForm.RRACCKIND.style.background = col0
  } else {
	myForm.RRACCKIND.style.background = col1
  }

  if ("" == myForm.RRACCDESC.value) {
  	valid = 0
  	errMsg += "\n - Part E / G : Describe what happened"
  	myForm.RRACCDESC.style.background = col0
  } else {
	myForm.RRACCDESC.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}



function valinvestigation1(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INVACCLOCATION.value) {
  	valid = 0
  	errMsg += "\n - Physical Location of Accident / Incident"
  	myForm.INVACCLOCATION.style.background = col0
  } else {
	myForm.INVACCLOCATION.style.background = col1
  }

  if (myForm.RECORDDATE_DAY_1.value == "DD" || myForm.RECORDDATE_MONTH_1.value == "MM" || myForm.RECORDDATE_YEAR_1.value == "YYYY") {
 	valid = 0
  	errMsg += "\n - Date and Time of Accident"
  	myForm.RECORDDATE_DAY_1.style.background = col0
  	myForm.RECORDDATE_MONTH_1.style.background = col0
  	myForm.RECORDDATE_YEAR_1.style.background = col0
  } else {
 
	var get_DATE = new Date();
  	var frm_DATE = new Date();
  	var frm_MONTH = myForm.RECORDDATE_MONTH_1.value - 1

  	frm_DATE.setDate(myForm.RECORDDATE_DAY_1.value);
  	frm_DATE.setMonth(frm_MONTH);
  	frm_DATE.setFullYear(myForm.RECORDDATE_YEAR_1.value);
  	
  	if (frm_DATE > get_DATE) {
  		valid = 0
  		errMsg += "\n - Date selected is in the future"
  		myForm.RECORDDATE_DAY_1.style.background = col0
  		myForm.RECORDDATE_MONTH_1.style.background = col0
  		myForm.RECORDDATE_YEAR_1.style.background = col0
  	} else {
  		myForm.RECORDDATE_DAY_1.style.background = col1
  		myForm.RECORDDATE_MONTH_1.style.background = col1
  		myForm.RECORDDATE_YEAR_1.style.background = col1
  	}
  
  }


  if (myForm.RECORDDATE_DAY_2.value == "DD" || myForm.RECORDDATE_MONTH_2.value == "MM" || myForm.RECORDDATE_YEAR_2.value == "YYYY") {
	valid = 0
  	errMsg += "\n - Date and Time investigation was started"
  	myForm.RECORDDATE_DAY_2.style.background = col0
  	myForm.RECORDDATE_MONTH_2.style.background = col0
  	myForm.RECORDDATE_YEAR_2.style.background = col0

  } else {
 
	var get_DATE = new Date();
  	var frm_DATE = new Date();
  	var frm_MONTH = myForm.RECORDDATE_MONTH_2.value - 1

  	frm_DATE.setDate(myForm.RECORDDATE_DAY_2.value);
  	frm_DATE.setMonth(frm_MONTH);
  	frm_DATE.setFullYear(myForm.RECORDDATE_YEAR_2.value);
  	
  	if (frm_DATE > get_DATE) {
  		valid = 0
  		errMsg += "\n - Date selected is in the future"
  		myForm.RECORDDATE_DAY_2.style.background = col0
  		myForm.RECORDDATE_MONTH_2.style.background = col0
  		myForm.RECORDDATE_YEAR_2.style.background = col0
  	} else {
  		myForm.RECORDDATE_DAY_2.style.background = col1
  		myForm.RECORDDATE_MONTH_2.style.background = col1
  		myForm.RECORDDATE_YEAR_2.style.background = col1
  	}

  }


  if (myForm.RECORDDATE_DAY_3.value == "DD" || myForm.RECORDDATE_MONTH_3.value == "MM" || myForm.RECORDDATE_YEAR_3.value == "YYYY") {

  } else {
 
	var get_DATE = new Date();
  	var frm_DATE = new Date();
  	var frm_MONTH = myForm.RECORDDATE_MONTH_3.value - 1

  	frm_DATE.setDate(myForm.RECORDDATE_DAY_3.value);
  	frm_DATE.setMonth(frm_MONTH);
  	frm_DATE.setFullYear(myForm.RECORDDATE_YEAR_3.value);
  	
  	if (frm_DATE > get_DATE) {
  		valid = 0
  		errMsg += "\n - Date and Time Investigation was completed"
  		myForm.RECORDDATE_DAY_3.style.background = col0
  		myForm.RECORDDATE_MONTH_3.style.background = col0
  		myForm.RECORDDATE_YEAR_3.style.background = col0
  	} else {
  		myForm.RECORDDATE_DAY_3.style.background = col1
  		myForm.RECORDDATE_MONTH_3.style.background = col1
  		myForm.RECORDDATE_YEAR_3.style.background = col1
  	}

	if ("" == myForm.INVINCOMPLETENOTE.value) {
	valid = 0
  	errMsg += "\n - Give reason why its not completed"
  	myForm.INVINCOMPLETENOTE.style.background = col0
	} else {
	myForm.INVINCOMPLETENOTE.style.background = col1
	}	
  
  }


  if ("" == myForm.INVACCDETAILS.value) {
  	valid = 0
  	errMsg += "\n - Details of the Accident / Incident"
  	myForm.INVACCDETAILS.style.background = col0
  } else {
	myForm.INVACCDETAILS.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function varinvestigation2(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INVWITFNAME.value) {
  	valid = 0
  	errMsg += "\n - Forename of Witness"
  	myForm.INVWITFNAME.style.background = col0
  } else {
	myForm.INVWITFNAME.style.background = col1
  }

  if ("" == myForm.INVWITSNAME.value) {
  	valid = 0
  	errMsg += "\n - Surname of witness"
  	myForm.INVWITSNAME.style.background = col0
  } else {
	myForm.INVWITSNAMES.style.background = col1
  }

  if ("" == myForm.INVWITWPHONE.value) {
  	valid = 0
  	errMsg += "\n - Work Tel No. of witness"
  	myForm.INVWITWPHONE.style.background = col0
  } else {
	myForm.INVWITWPHONE.style.background = col1
  }

  if ("" == myForm.INVWITHPHONE.value) {
  	valid = 0
  	errMsg += "\n - Home Tel No. of witness"
  	myForm.INVWITHPHONE.style.background = col0
  } else {
	myForm.INVWITHPHONE.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valinvestigation3(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INVACTDETAILS.value) {
  	valid = 0
  	errMsg += "\n - Action Details"
  	myForm.INVACTDETAILS.style.background = col0
  } else {
	myForm.INVACTDETAILS.style.background = col1
  }

  if (myForm.RECORDDATE_DAY_1.value == "DD" || myForm.RECORDDATE_MONTH_1.value == "MM" || myForm.RECORDDATE_YEAR_1.value == "YYYY") {
 	valid = 0
  	errMsg += "\n - Date and Time of completion"
  	myForm.RECORDDATE_DAY_1.style.background = col0
  	myForm.RECORDDATE_MONTH_1.style.background = col0
  	myForm.RECORDDATE_YEAR_1.style.background = col0
  } else {
  	myForm.RECORDDATE_DAY_1.style.background = col1
  	myForm.RECORDDATE_MONTH_1.style.background = col1
  	myForm.RECORDDATE_YEAR_1.style.background = col1
  }

  if ("NA" == myForm.INVACTPERSON.value) {
  	valid = 0
  	errMsg += "\n - Select Person Responsible"
  	myForm.INVACTPERSON.style.background = col0
  } else {
	myForm.INVACTPERSON.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}

function valinvestigation4(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INVMEMNAME.value) {
  	valid = 0
  	errMsg += "\n - Full Name"
  	myForm.INVMEMNAME.style.background = col0
  } else {
	myForm.INVMEMNAME.style.background = col1
  }

  if ("" == myForm.INVMEMCOMPANY.value) {
  	valid = 0
  	errMsg += "\n - Company"
  	myForm.INVMEMCOMPANY.style.background = col0
  } else {
	myForm.INVMEMCOMPANY.style.background = col1
  }

  if ("" == myForm.INVMEMPNUMBER.value) {
  	valid = 0
  	errMsg += "\n - Tel No."
  	myForm.INVMEMPNUMBER.style.background = col0
  } else {
	myForm.INVMEMPNUMBER.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function valinvestigation5(myForm) 
{
  valid = 1
  col0 = '#99ccff'
  col1 = '#ffffff'
  errMsg = "There is an error in your form. Please correct the following fields:\n"
  
  if ("" == myForm.INVDISNAME.value) {
  	valid = 0
  	errMsg += "\n - Full Name"
  	myForm.INVDISNAME.style.background = col0
  } else {
	myForm.INVDISNAME.style.background = col1
  }

  if ("" == myForm.INVDISCOMPANY.value) {
  	valid = 0
  	errMsg += "\n - Company"
  	myForm.INVDISCOMPANY.style.background = col0
  } else {
	myForm.INVDISCOMPANY.style.background = col1
  }

  if ("" == myForm.INVDISPNUMBER.value) {
  	valid = 0
  	errMsg += "\n - Tel No."
  	myForm.INVDISPNUMBER.style.background = col0
  } else {
	myForm.INVDISPNUMBER.style.background = col1
  }


  if (!valid) {
    window.alert(errMsg)
    return false
  } else {
    return true
  }
}


function OpenGraph()
{
         	window.open('search/graph/default.asp', 'window', 'width=550,height=400,screenX=200,screenY=100,left=200,top=100,scrollbars=no,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=yes,copyhistory=no');
}

function OPENreportRA(reference)
{
             var code = reference
             window.open('../risk_assessment/standard/create_print.asp?P=N&code=' + code + '', 'window', 'width=650,height=500,screenX=70,screenY=20,left=70,top=20,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=yes,copyhistory=no');
}

function OPENreportMH(reference)
{
             var code = reference
             window.open('../Manual_Handling/standard/create_print.asp?P=N&code=' + code + '', 'window', 'width=650,height=500,screenX=70,screenY=20,left=70,top=20,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=yes,copyhistory=no');
}