function toggleHistory(butt,ele)
{
  if ("Show notes �" == butt.value)
  {
  	myEle = document.getElementById(ele)
    myEle.style.display = "block"
	butt.value = "� Hide notes"
  }
  else
  {
    myEle = document.getElementById(ele)
    myEle.style.display = "none"
	butt.value = "Show notes �"
  }
}

function helpWin(topic)
{
  window.open('./help/account_help.asp?type=' + topic, 'helpWin', 'width=308,height=158,screenX=340,screenY=300,left=340,top=300,scrollbars=no,resizable=no,toolbar=no,location=no,status=no,menubar=no');
}

function valMh1(myForm)
{
	valid = 1
	col0 = '#99ccff'
	col1 = '#ffffff'

	errMsg = "Please make a valid selection for each of the following:\n"
	if (0 == myForm.BU.selectedIndex) {
		errMsg += "\n - 'Select the Company' options.";
		myForm.BU.style.background = col0
		valid = 0
	} else {
		myForm.BU.style.background = col1
	}
	if (0 == myForm.BL.selectedIndex)
	{
		errMsg += "\n - 'Select a Location' options.";
		myForm.BL.style.background = col0
		valid = 0
  	} else {
		myForm.BL.style.background = col1
	}
	if (0 == myForm.BD.selectedIndex)
	{
		errMsg += "\n - 'Select a Department' options.";
		myForm.BD.style.background = col0
		valid = 0
  	} else {
		myForm.BD.style.background = col1
	}
	if (!valid) {
		window.alert(errMsg)
		return false
	} else {
		return true
	}
}

function valMh2(myForm)
{
	valid = 1
  	col0 = '#99ccff'
	col1 = '#ffffff'
	
	errMsg = "Please provide a valid answer for the following fields:\n"
	if (0 == myForm.mh_assess_d.selectedIndex) {
		valid = 0
		myForm.mh_assess_d.style.background = col0
		errMsg += "\n - Day of assessment"
	} else {
		myForm.mh_assess_d.style.background = col1
	}
	if (0 == myForm.mh_assess_m.selectedIndex) {
		valid = 0
		myForm.mh_assess_m.style.background = col0
		errMsg += "\n - Month of assessment"
	} else {
		myForm.mh_assess_m.style.background = col1
	}
	if (0 == myForm.mh_assess_y.selectedIndex) {
		valid = 0
		myForm.mh_assess_y.style.background = col0
		errMsg += "\n - Year of assessment"
	} else {
		myForm.mh_assess_y.style.background = col1
	}
	if (!myForm.mh_step2_op.value) {
		valid = 0
		myForm.mh_step2_op.style.background = col0
		errMsg += "\n - Operation being assessed"
	} else {
		myForm.mh_step2_op.style.background = col1
	}
	if (!myForm.mh_step2_opdes.value) {
		valid = 0
		myForm.mh_step2_opdes.style.background = col0
		errMsg += "\n - Description of manual handling operation"
	} else {
		myForm.mh_step2_opdes.style.background = col1
	}
	
	if (!(myForm.c2_q1[0].checked || myForm.c2_q1[1].checked)) {
		valid = 0
		errMsg += "\n - Question 1 (Yes or No)"
	}
  	if (!(myForm.c2_q2[0].checked || myForm.c2_q2[1].checked)) {
		valid = 0
		errMsg += "\n - Question 2 (Yes or No)"
	}
	if (valid) {
		return true 
	} else {
		window.alert(errMsg)
		return false
	}
}

function valPersAff(myForm)
{
	valid = 1
	col0 = '#99ccff'
	col1 = '#ffffff'
	errMsg = "Please provide a valid answer for the following fields:\n"
	if (0 == myForm.pers_type.selectedIndex) {
		valid = 0
		errMsg += "\n - Person affected menu"
		myForm.pers_type.style.background = col0
	} else {
		myForm.pers_type.style.background = col1
	}
	if (-1 == myForm.pers_qty.value.search(/^[0-9]+$/)) {
		valid = 0
		errMsg += "\n - Quantity"
		myForm.pers_qty.style.background = col0
	} else {
		myForm.pers_qty.style.background = col1
	}
	if (valid) {
		return true 
	} else {
		window.alert(errMsg)
		return false
	}
}

function valMh3(myForm,persAffDone)
{
	valid = 1
	errMsg = ""
	col0 = '#99ccff'
	col1 = '#ffffff'
	if (0 == persAffDone) {
		valid = 0
		errMsg += "You must select at least one person who is affected by this operation.\n\n"
  	}
	errMsg += "Please provide answers for the following fields:\n"
	if ('' == myForm.c3_q1.value) {
    	errMsg += "\n - Description of load"
		myForm.c3_q1.style.background = col0
		valid = 0
	} else {
		myForm.c3_q1.style.background = col1
	}
	if ('nil' == myForm.c3_q2.value)
	{
		errMsg += "\n - Weight of load"
		myForm.c3_q2.style.background = col0
		valid = 0
	} else {
		myForm.c3_q2.style.background = col1
	}
	if ('nil' == myForm.c3_q3.value)
	{
		errMsg += "\n - Frequency of operation"
		myForm.c3_q3.style.background = col0
		valid = 0
	} else {
		myForm.c3_q3.style.background = col1
	}
	if (0 == valid) {
		window.alert(errMsg)
		return false
	} else {
		return true
	}
}

function val4Radios(myForm,step,numQs) {
	var ele,eleName,oneSelected,valid,errMsg,i,j,myForm,step,numQs
	valid = 1
	errMsg = "Please select a risk level for every hazard."
	for (i = 0; i < numQs; i++) {
		eleName = "myForm." + "c4" + step + "_q" + (i + 1)
		ele = eval(eleName)
		oneSelected = 0
		for (j = 0; j < 4; j++) {
			if (ele[j].checked) {
				oneSelected = 1
			}
		}
		if (!oneSelected) {
			valid = 0
		}
	}
	if (!valid) {
		window.alert(errMsg)
		return false
	} else {
		return true
	}
}

function valMh5(myForm)
{
	valid = 1
  	col0 = '#99ccff'
	col1 = '#ffffff'
	
	errMsg = "Please provide a valid answer for the following fields:\n"
	if ("" == myForm.mh_remact.value) {
		valid = 0
		myForm.mh_remact.style.background = col0
		errMsg += "\n - Remedial action"
	} else {
		myForm.mh_remact.style.background = col1
	}
	if (0 == myForm.mh_target_d.selectedIndex) {
		valid = 0
		myForm.mh_target_d.style.background = col0
		errMsg += "\n - Target completion day"
	} else {
		myForm.mh_target_d.style.background = col1
	}
	if (0 == myForm.mh_target_m.selectedIndex) {
		valid = 0
		myForm.mh_target_m.style.background = col0
		errMsg += "\n - Target completion month"
	} else {
		myForm.mh_target_m.style.background = col1
	}
	if (0 == myForm.mh_target_y.selectedIndex) {
		valid = 0
		myForm.mh_target_y.style.background = col0
		errMsg += "\n - Target completion year"
	} else {
		myForm.mh_target_y.style.background = col1
	}

	if (0 == myForm.mh_step5_act_pers.selectedIndex) {
		valid = 0
		myForm.mh_step5_act_pers.style.background = col0
		errMsg += "\n - Person to carry out action(s)"
	} else {
		myForm.mh_step5_act_pers.style.background = col1
	}
	
	if (0 == myForm.mh_rev_d.selectedIndex) {
		valid = 0
		myForm.mh_rev_d.style.background = col0
		errMsg += "\n - Day of review"
	} else {
		myForm.mh_rev_d.style.background = col1
	}
	if (0 == myForm.mh_rev_m.selectedIndex) {
		valid = 0
		myForm.mh_rev_m.style.background = col0
		errMsg += "\n - Month of review"
	} else {
		myForm.mh_rev_m.style.background = col1
	}
	if (0 == myForm.mh_rev_y.selectedIndex) {
		valid = 0
		myForm.mh_rev_y.style.background = col0
		errMsg += "\n - Year of review"
	} else {
		myForm.mh_rev_y.style.background = col1
	}
	
	if (valid) {
		return true 
	} else {
		window.alert(errMsg)
		return false
	}
}
