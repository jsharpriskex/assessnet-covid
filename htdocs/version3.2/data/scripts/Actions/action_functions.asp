<%
    
CALL startCONNECTIONS()



sub add_action_v1(module, module_ref, module_sub_ref, actions, action_date, action_to, action_to_email, action_priority, action_priority_qty, action_priority_type, action_standard, action_sign_off_required, action_type, original_action_id)
   
    if ucase(module) = "SA" then
   



        if session("pref_task_signoff") = "Y" and  session("pref_task_signoff_sa") = "T" then 'preferences are active but its per task so check the check box
           ' if len(action_sign_off_required) = 0 or isnull(action_sign_off_required) = true then
           '     action_sign_off_required = 0
           ' else
           '     action_sign_off_required = 1
           ' end if  
    
            if InStr(action_sign_off_required, "true") > 0 then
                action_sign_off_required = 1
            elseif InStr(action_sign_off_required, "false") > 0 then
                action_sign_off_required = 0
            else
                action_sign_off_required = 0
            end if
            
                                                       
        elseif session("pref_task_signoff") = "Y" and  session("pref_task_signoff_sa") = "A" then 'preferences are active but its for all tasks so force it
            action_sign_off_required = 1 
        else 'preferences are not active - clear setting
            action_sign_off_required = 0
        end if

        'TODO populate section ref
           module_additional_ref = ""

       

        'grab the count + 1 of the action_id for that audit
        new_action_id =1
        objcommand.commandtext = "select count(id) +1 as new_action_id from " & Application("DBTABLE_SA_ACTIONS") & "  where corp_code ='" & session("corp_code") & "' and audit_ref = '" & module_ref & "' and question_id='" & module_sub_ref & "'"
      
     'response.end
        set objrs =  objcommand.execute

        if not objrs.eof then
             new_action_id = objrs("new_action_id")
        end if

        if len(action_standard) > 0 then
            var_act_std_head = ", action_standard"
            var_act_std_val = ", '" & action_standard & "' "

        end if

        if original_action_id = "" then
            original_action_id = new_action_id
        end if


        objCommand.commandtext = "insert into " & Application("DBTABLE_SA_ACTIONS") & " (corp_code, created_by, audit_ref, question_id, action_id, action_text, action_to, action_due_date, action_to_email, action_priority_name, action_priority_qty, action_priority_type" & var_act_std_head & ", action_sign_off_required, original_action_id) " & _
                                            " values ('" & session("corp_code") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & module_ref & "' , '" & module_sub_ref & "', '" & new_action_id & "', '" & actions & "', '" & action_to & "', '" & action_date & "', '" & action_to_email & "', '" & action_priority & "', '" & action_priority_qty & "', '" & action_priority_type & "' " & var_act_std_val & ", '" & action_sign_off_required & "', '" & original_action_id & "' )" 
        'response.write objcommand.commandtext
        'response.end
        objCommand.execute

        if action_type = "report" then
            'if it comes from the safety audit report, then we need to process the action now rather than at the end of the audit process.        

            objcommand.commandtext =  "select a.isActive, a.question_id + '_' + cast(a.action_id as varchar(20)) as question_id, a.audit_ref, a.action_text, a.action_due_date, a.action_to, a.action_to_email, a.action_priority_name, a.action_priority_qty, a.action_priority_type, a.action_standard, a.action_sign_off_required from " & APPLICATION("DBTABLE_SA_ACTIONS") & " as a " & _
	   		        			      " LEFT join  " & Application("DBTABLE_SA_V2_TEMPLATE_QUESTIONS") & " as c" &_ 
		                              " on  a.question_id = c.question_reference " &_ 
							          "where a.audit_ref='" & module_ref & "' and a.corp_code='" & session("CORP_CODE") &  "' and a.action_id = '" & new_action_id & "' and a.question_id = '" & module_sub_ref & "'"
            response.write objcommand.commandtext
    set objrs = objcommand.execute
            IF not objrs.eof Then
	            while not objrs.eof   'records exist  
                    

                        objCommand.commandText = "SELECT id FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
		                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
		                                 "  AND task_module = 'SA' " & _
		                                 "  AND task_mod_ref = '" & module_ref & "' " & _
		                                 "  AND task_mod_action_ref = '" & objrs("question_id") & "'"
   ' response.write objcommand.commandtext 
		                set objRs_tasks = objCommand.execute
		                if not objRs_tasks.EOF then
		                    task_id = objRs_tasks("id")
        		           
		                    objCommand.commandText = "INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " (corp_code, task_id, task_history, task_history_by, gendatetime) " & _
		                                                "VALUES ('" & session("CORP_CODE") & "', " & task_id & ", 'AUTOMATED UPDATE: task removed when question answer changed to `meets requirements`', '" & session("YOUR_ACCOUNT_ID") & "', '" & now() & "')"
		                    objCommand.execute
        		           
		                    objCommand.commandText = "UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
		                                                "SET corp_code = corp_code + 'A' " & _
		                                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
		                                                "  AND id = " & task_id & " " & _
		                                                "  AND task_module = 'SA' " & _ 
		                                                "  AND task_mod_ref = '" & module_ref & "' " & _
		                                                "  AND task_mod_action_ref = '" & objrs("question_id") & "'"
                            response.write objcommand.commandtext
		                    objCommand.execute
		                end if

                        '***********Task Manager tie in**************
                                    
                        TaskPri = "Not Stated" 'priority(Objrs("priority"))
                        TaskModRef = module_ref ' SA Reference
                        TaskModActionRef =  objrs("question_id") 
                        TaskModActionUser = objrs("action_to") ' Actioned user account ID
                        TaskDueDate = objrs("action_due_date") ' Duh!
                        TaskDescription =  objrs("action_text") ' Action
                        Task_action_email_to = objrs("action_to_email")
                        if TaskModActionUser <> "Email" then
                            Task_action_email_to = ""      
                        end if      
                        var_date_text = objrs("action_priority_name")
                        var_date_qty = objrs("action_priority_qty")
                        var_date_type  = objrs("action_priority_type")

                        Task_standard = objrs("action_standard")
                
                        if Task_standard = "" Then
                            Task_standard = "0"
                        end if

                        varTaskSignoff = objrs("action_sign_off_required")

                                
                        'CALL ADD_ACTION("SA",TaskModRef,TaskModActionRef,TaskModActionUser,TaskDueDate,TaskPri,TaskDescription)
	                    'response.write validateinput(TaskDescription)

                        CALL ADD_ACTION_V2("SA", TaskModRef, TaskModActionRef, TaskModActionUser, TaskDueDate, TaskPri, TaskDescription, Task_action_email_to, session("YOUR_ACCOUNT_ID"), var_date_text, var_date_qty, var_date_type, Task_standard, varTaskSignoff, sw7, sw8, sw9, sw10, sw11, sw12, sw13, sw14, sw15, sw16, sw17, sw18, sw19, sw20)  
                        
    

                        '********************************************
						
			            objrs.movenext
		        wend			
	        End if
	        set objRs = nothing


        end if


    end if

    'add to TM or is this at the end?

      if session("PREF_TASK_STANDARDS") = "Y" then
        '*************************************************
        'lock associated records
        objCommand.commandText = "UPDATE " & Application("DBTABLE_TASK_STANDARDS") & " SET locked = 1 " & _
                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                "  AND opt_id = '" & action_standard & "' "
        objCommand.execute
        '*************************************************
      end if
 
         objCommand.Commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " (HisRelationref,corp_code,HisBy,HisType,HisDateTime,HisMod) values('" & module_ref & "SA','" & Session("corp_code") & "','" & Session("YOUR_ACCOUNT_ID") & "','EDITED ACTION ON REPORT','" & date() & " " & time() & "', 'SA')"
         objcommand.execute


end sub


sub remove_action(action_id, module, module_ref, module_sub_ref, action_type)
   
    objCommand.commandtext = "update " & Application("DBTABLE_SA_ACTIONS") & " set  isActive = 0, Removed_date = getdate(), removed_by = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                       " where corp_code = '" & session("corp_code") & "' and isActive = 1 and action_id = '" & action_id & "' and audit_ref='" & module_ref & "' and question_id='" & module_sub_ref & "' "
 

    objCommand.execute

    if action_type = "report" then

        objCommand.commandText = "UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
		                            "SET corp_code = corp_code + 'A' " & _
		                            "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
		                            "  AND task_module = 'SA' " & _ 
		                            "  AND task_mod_ref = '" & module_ref & "' " & _
		                            "  AND task_mod_action_ref = '" & module_sub_ref & "_" & action_id & "'"
    response.write objcommand.commandtext
		objCommand.execute

    end if

    objCommand.Commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " (HisRelationref,corp_code,HisBy,HisType,HisDateTime,HisMod) values('" & module_ref & "SA','" & Session("corp_code") & "','" & Session("YOUR_ACCOUNT_ID") & "','REMOVED ACTION FROM REPORT','" & date() & " " & time() & "', 'SA')"
    objcommand.execute
    
end sub

sub mark_appropriate_actions(action_id, module, module_ref, module_sub_ref)
   
    objCommand.commandtext = "update " & Application("DBTABLE_SA_AUDIT_DATA") & " set  is_action_assigned_elsewhere = 1, action_assigned_elsewhere_date = getdate(), action_assigned_elsewhere_by = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                       " where corp_code = '" & session("corp_code") & "'  and audit_ref='" & module_ref & "' and question_id='" & module_sub_ref & "' "
    
    objCommand.execute
    
end sub

sub remove_mark_appropriate_actions(action_id, module, module_ref, module_sub_ref)
   
    objCommand.commandtext = "update " & Application("DBTABLE_SA_AUDIT_DATA") & " set  is_action_assigned_elsewhere = 0, action_assigned_elsewhere_date = NULL, action_assigned_elsewhere_by = '' " & _
                                       " where corp_code = '" & session("corp_code") & "'  and audit_ref='" & module_ref & "' and question_id='" & module_sub_ref & "' "
  
    objCommand.execute
    
end sub
    

sub edit_action(action_id, module, module_ref, module_sub_ref, actions, action_date, action_to, action_to_email, action_priority, action_priority_qty, action_priority_type, action_standard, action_sign_off_required, action_type)
   

    'response.write action_id & " - " & module & " - " & module_ref & " - " & module_sub_ref & " - " & action_type
    'If action has already got an original id, then do not assign another one.
    objcommand.commandtext = "select original_action_id from " & Application("DBTABLE_SA_ACTIONS") & "  where corp_code ='" & session("corp_code") & "' and audit_ref = '" & module_ref & "' and question_id='" & module_sub_ref & "' and action_id = '" & action_id & "' "
    set objrs =  objcommand.execute
    
    if not objrs.eof then
            original_action_id = objrs("original_action_id")
    else
            original_action_id = action_id
    end if



    call remove_action(action_id, module, module_ref, module_sub_ref, action_type)
      
    
    


    call add_action_v1(module, module_ref, module_sub_ref, actions, action_date, action_to, action_to_email, action_priority, action_priority_qty, action_priority_type, action_standard, action_sign_off_required, action_type, original_action_id)
    
    '**************************************************************************************************************************
    '********************begin the ultra inneficient fix for the task history that got Tim'd***********************************
    '**************************************************************************************************************************
    'first we need to retain the history of the old task. Nothing suggests that this has been removed so i just need to update the task id in the history with the new task. As the task history table takes a thousand years to load
    ' i can only guess, so this may work. it may not.
    ' First grab the id for the now removed task
    objcommand.commandtext = "SELECT TOP 1 task.* FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " AS task " & _
                            " WHERE task.CORP_CODE = '" & session("CORP_CODE") & "A' AND task.Task_mod_ref = '" & module_ref & "' ORDER BY task.ID desc" 
    set objrsOLDTASK = objCommand.execute
    if objrsOLDTASK.eof then
        ' something went really wrong 
        var_no_task_found = true
    else
        var_oldtask_id = objrsOLDTASK("ID")
        var_oldduedate = objrsOLDTASK("Task_due_date")
        var_oldtaskaccount = objrsOLDTASK("Task_account")
        var_oldtaskstatus = objrsOLDTASK("Task_Status")
        var_no_task_found = false
    End if

    if var_no_task_found = false then
        'now do the same but for the new task
        objcommand.commandtext = "SELECT TOP 1 task.* FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " AS task " & _
                                " WHERE task.CORP_CODE = '" & session("CORP_CODE") & "' AND task.Task_mod_ref = '" & module_ref & "' ORDER BY task.ID desc" 
        set objrsNEWTASK = objCommand.execute
        if objrsNEWTASK.eof then
           ' oops
        else
            var_newtask_id = objrsNEWTASK("ID") 
            var_newduedate = objrsNEWTASK("Task_due_date")
            var_newtaskaccount = objrsNEWTASK("Task_account")
            var_newtaskstatus = objrsNEWTASK("Task_Status")
        End if


        'Sweet, now we have the task id's. What will we do with them? find out....right now! maybe (we want to update the history with the new task id)
        objcommand.commandtext = "UPDATE " & Application("DBTABLE_TASK_HISTORY") & " " & _
                                "SET task_id = " & var_newtask_id & " " & _
                               "WHERE corp_code = '" & session("CORP_CODE") & "' AND task_id = '" & var_oldtask_id & "' "
        objCommand.execute 


        'now lets work out what we have changed and add it to the history of our sparkly new created task
        if var_newtaskaccount <> var_oldtaskaccount then
	            objCommand.commandText = "SELECT per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " " & _
	                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
	                                     "  AND acc_ref = '" & var_newtaskaccount & "'"
	            set actObjRsUser = objCommand.execute
	            if not actObjRsUser.EOF then
	                task_history_text = actObjRsUser("per_fname") & " " & actObjRsUser("per_sname")
	            else
	                task_history_text = "Unknown"
	            end if
	            set actObjRsUser = nothing
	        
	            task_history_text = "Task delegated to " & task_history_text
	            call ADD_TASK_ACTION(var_newtask_id, task_history_text)
	        end if
	    
	        if var_newduedate <> var_oldduedate then
	            task_history_text = "Task due date changed from " & var_oldduedate & " to " & var_newduedate
	            call ADD_TASK_ACTION(var_newtask_id, task_history_text)
	        end if
	    
	        if var_newtaskstatus <> var_oldtaskstatus then
	    
	            select case var_newtaskstatus
	                case "0"
	                    newTaskStatusText = "Complete"
	                case "1"
	                    newTaskStatusText = "Active"
	                case "2"
	                    newTaskStatusText = "Pending"
	            end select
	            select case var_oldtaskstatus
	                case "0"
	                    oldTaskStatusText = "Complete"
	                case "1"
	                    oldTaskStatusText = "Active"
	                case "2"
	                    oldTaskStatusText = "Pending"
	            end select
	    
	            task_history_text = "Task changed from " & oldTaskStatusText & " to " & newTaskStatusText
	            call ADD_TASK_ACTION(var_newtask_id, task_history_text)
	        end if
	    
	end if


    '*************************************************************************************************************************
    '******************************************End of ultra inefficent fix****************************************************
    '*************************************************************************************************************************

        
end sub

sub display_actions(module, audit_ref, question)
    mycount = 0
   uid = audit_ref & "_" & question
   objCommand.commandtext = "select a.* , u.per_fname + ' ' +  u.per_sname as name, u2.per_fname + ' ' +  u2.per_sname as name2, tm.*, standards.opt_value AS task_standards_text, pri.id, pri.ref, pri.name AS pri_name, pri.val_qty, pri.val_datepart, pri.val_colour, a.action_sign_off_required, entries.assessor as auditor  from  " & Application("DBTABLE_SA_ACTIONS") & " a " & _
                            " left join  " & Application("DBTABLE_TASK_MANAGEMENT") & " TM "   & _
                            "  on a.corp_code  = tm.corp_code " & _
                            "  and a.question_id + '_' + cast( a.action_id as varchar(20))  = tm.task_mod_action_ref " & _
                            "  and a.audit_ref = tm.task_mod_ref " & _
                            " left join  " & Application("DBTABLE_USER_DATA") & " U "   & _
                            "  on tm.corp_code  = u.corp_code " & _
                            "  and tm.task_account = u.acc_ref " & _
                            " left join  " & Application("DBTABLE_USER_DATA") & " U2 "   & _
                            "  on a.corp_code  = u2.corp_code " & _
                            "  and a.action_to = u2.acc_ref " & _    
                            " LEFT JOIN " & Application("DBTABLE_TASK_STANDARDS") & " AS standards " & _
                            "  ON a.corp_code = standards.corp_code " & _
                            "  AND a.action_standard = cast(standards.opt_id as varchar(20)) " & _
                            " LEFT JOIN " & Application("DBTABLE_TM_PRIORITY_OPTS") & " AS pri " & _
                            "  ON a.corp_code = pri.corp_code " & _
                            "  AND a.action_priority_name = pri.name " & _
                            " LEFT JOIN " & APPLICATION("DBTABLE_SA_AUDIT_ENTRIES") & " as entries " & _ 
                            "  ON a.corp_code = entries.corp_code " & _
                            "  AND a.audit_ref = entries.audit_ref " & _
                            " where a.corp_code = '" & session("corp_code") & "' and a.isActive = 1 and a.audit_ref='" & audit_ref & "' and a.question_id='" & question & "' " & _
                            " order by a.original_action_id asc "
    ' response.write objcommand.commandtext
    'response.end
     set objrsACTS = objCommand.execute

    if objrsACTS.eof then
        'display error
        if displayCommands then
            response.write "<div class='alert alert-warning' style='margin-right: 10px'><strong>No Remedial Actions have been added</strong><br />Use the Add Action button to add the root cause for this issue</div>"
        end if
    else
        response.write "<table class='table table-striped table-bordered'><thead><tr><th class='actionBG'>Action</th><th width='250px' class='actionBG'>Actioned To</th>"
    
        if session("pref_task_standards") = "Y" then response.write "<th width='150px'>Associated Standard</th>"
    
      response.Write "<th width='150px' class='actionBG'>Current Status</th><th width='150px' class='actionBG'>Due Date</th><th width='120px' class='actionBG'>Options</th></tr></thead><tbody>"
    
        while not objrsACTS.eof            
                     
        actions =  objrsACTS("action_text")       
        action_to_email =  objrsACTS("action_to_email")
      
         if isnull(objrsACTS("name"))  then 'no task generated yet   
                action_to =  objrsACTS("action_to")       
                action_to_name =  objrsACTS("name2") 
                standard = objrsACTS("action_standard") 
                standard_text =  objrsACTS("task_standards_text") 
                action_date = objrsACTS("action_due_date") 
                var_status_class = "txt_shade_red"
                var_sa_curr_status = "Processing"
                var_pri_name = objrsACTS("action_priority_name")
                var_pri_col = objrsACTS("val_colour")
                var_pri_ref = objrsACTS("ref")
                var_sign_off = objrsACTS("action_sign_off_required")

                var_auditor = objrsACTS("auditor")
                var_created_by = objrsACTS("created_by")

        else 'use TM for as many vals as possible

            action_to =  objrsACTS("task_account") 
            action_to_name =  objrsACTS("name") 
            standard = objrsACTS("action_standard") 
            standard_text =  objrsACTS("task_standards_text") 
            action_date = objrsACTS("action_due_date") 
    
            var_auditor = objrsACTS("auditor")
            var_created_by = objrsACTS("created_by")

            var_pri_name = objrsACTS("action_priority_name")
            var_pri_col = objrsACTS("val_colour")
            var_pri_ref = objrsACTS("ref")

            var_sa_curr_status   = objrsACTS("task_status")     
            var_sign_off = objrsACTS("action_sign_off_required")       
    
           if session("PREF_TASK_SIGNOFF") = "Y" and var_sa_curr_status = "0" then
                if objrsACTS("task_signoff") = "0" then
                    var_so_status = "N"                 
                    var_so_status_text = var_so_status_text & "</span>"
                else
                    var_so_status = "Y"
                    if not isnull(var_task_signoff_date) then 
                        var_so_status_text  = "<br /><span class='info' title='Signed off by " & var_task_signoff_by_text  & " on " & var_task_signoff_date & "'>Signed Off</span>"
                    end if
                end if
            else
                var_so_status = ""
                var_so_status_text = ""
            end if
                                                    
            select case var_sa_curr_status

            case 0 
                if var_so_status = "N" then 
                    var_status_class = "txt_shade_blue"
                    var_sa_curr_status = "Awaiting Sign Off"
                else
                    var_status_class = "txt_shade_grn"
                    var_sa_curr_status = "Complete"
                end if 
            case 1
                var_status_class = "txt_shade_org"
                var_sa_curr_status = "Active"                                                  
            case 2
                var_status_class = "txt_shade_red"
                var_sa_curr_status = "Pending"                                                                                
            case else
                var_status_class = "txt_shade_red"
                    var_sa_curr_status = "Processing"
            end select
        end if

        var_sa_curr_status = var_sa_curr_status & var_so_status_text



        'validation
        if standard = "0" then standard_text= "Not Applicable"
        if action_to = "Email" then action_to_name = "Email - " & action_to_email


        'Output
            response.Write "<tr><td  title='" & actions & "'  data-toggle='tooltip'  id='actions_" & audit_ref & "_" & question & "_" & objrsACTS("action_id")  & "'>  " & Replace(left(actions, 100), chr(10), "<br>") & " </td><td  class='vert-align'>" & action_to_name & "</td>"
    
            if session("pref_task_standards") = "Y" then response.write "<td  class='vert-align'>" & standard_text & "</td>"
    
            response.Write  "<td class='" & var_status_class & " c vert-align'  >" & var_sa_curr_status & "</td><td class='c vert-align' >" & action_date 
            If len(var_pri_name) > 0 then response.write "<br /><span style='color: " & var_pri_col & "'>(" & var_pri_name & ")</span>"
            response.write  "</td>"

                                                                                                                        
                    response.write "<td class='vert-align'><a class='btn btn-xs btn-primary' onclick='return edit_action_" & uid & "(""" & objrsACTS("action_id") & """, """ &  Server.HTMLEncode(Replace(actions, chr(10), "<br>")) & """, """ & action_to & """, """ & action_to_email & """, """ & action_date & """, """ & standard & """, """ & var_pri_ref & """, """ & var_sign_off & """, """ & session("signoff_date_block") & """, """ & session("YOUR_ACCESS") & """, """ & Session("YOUR_ACCOUNT_ID") & """, """ & var_auditor & """, """ & var_created_by & """ )'>Edit</a> "
                    'response.write "edit_action_" & uid & "(""" & objrsACTS("action_id") & """, """ &  Server.HTMLEncode(Replace(actions, chr(10), "<br>")) & """, """ & action_to & """, """ & action_to_email & """, """ & action_date & """, """ & standard & """, """ & var_pri_ref & """, """ & var_sign_off & """, """ & session("signoff_date_block") & """, """ & session("YOUR_ACCESS") & """, """ & Session("YOUR_ACCOUNT_ID") & """, """ & var_auditor & """, """ & var_created_by & """ )"
                    response.write  "<a class='btn btn-xs btn-danger' onclick='return remove_action_" & uid & "(""" & objrsACTS("action_id") & """)'>Remove</a></td>"
            
            response.write "</tr>"

            objrsACTS.movenext
        wend
        
    
        response.Write "</tbody></table>"
    end if

       
end sub

    
function count_actions(module, module_ref, module_sub_ref)
   
    if ucase(module) = "SA" then 
   
       objCommand.commandtext = "select count(id) as act_count  from  " & Application("DBTABLE_SA_Actions") & " act  " & _
                                " where act.corp_code = '" & session("corp_code") & "' and act.isActive = 1 and  act.audit_ref='" & module_ref  & "' and act.question_id='" & module_sub_ref & "' "
  
         set objrsact = objCommand.execute

        if objrsact.eof then
            count_actions =0
        else
           count_actions = objrsact("act_count")
        end if
    else
         count_actions =0
    end if
    
   
    
    
end function


sub display_actions_report(module, audit_ref, question)
    mycount = 0
   uid = audit_ref & "_" & question
   objCommand.commandtext = "select a.* , u.per_fname + ' ' +  u.per_sname as name, u2.per_fname + ' ' +  u2.per_sname as name2, tm.id as task_id, tm.*, standards.opt_value AS task_standards_text,  tasksSO.per_fname + ' ' + tasksSO.per_sname as task_signoff_by_text, pri.id, pri.ref, pri.name AS pri_name, pri.val_qty, pri.val_datepart, pri.val_colour  from  " & Application("DBTABLE_SA_ACTIONS") & " a " & _
                            " left join  " & Application("DBTABLE_TASK_MANAGEMENT") & " TM "   & _
                            "  on a.corp_code  = tm.corp_code " & _
                            "  and a.question_id + '_' + cast( a.action_id as varchar(20))  = tm.task_mod_action_ref " & _
                            "  and a.audit_ref = tm.task_mod_ref " & _
                            " left join  " & Application("DBTABLE_USER_DATA") & " U "   & _
                            "  on tm.corp_code  = u.corp_code " & _
                            "  and tm.task_account = u.acc_ref " & _
                            " left join  " & Application("DBTABLE_USER_DATA") & " U2 "   & _
                            "  on a.corp_code  = u2.corp_code " & _
                            "  and a.action_to = u2.acc_ref " & _    
                            " LEFT JOIN " & Application("DBTABLE_TASK_STANDARDS") & " AS standards " & _
                            "  ON a.corp_code = standards.corp_code " & _
                            "  AND a.action_standard = cast(standards.opt_id as varchar(20)) " & _
                            " LEFT JOIN " & Application("DBTABLE_USER_DATA") & " AS tasksSO " & _
                            "  ON tm.task_signoff_by = tasksSO.Acc_Ref " &_
		                    "  AND tm.corp_code = tasksSO.corp_code " & _	
                            " LEFT JOIN " & Application("DBTABLE_TM_PRIORITY_OPTS") & " AS pri " & _
                            "  ON a.corp_code = pri.corp_code " & _
                            "  AND a.action_priority_name = pri.name " & _
                            " where a.corp_code = '" & session("corp_code") & "' and a.isActive = 1 and a.audit_ref='" & audit_ref & "' and a.question_id='" & question & "' " & _
                            " order by a.original_action_id asc "
    if Request.ServerVariables("REMOTE_ADDR") = "195.60.7.88" then
        'response.write objcommand.commandtext
    end if

    set objrsACTS = objCommand.execute

    if objrsACTS.eof then
        'display error
        if displayCommands then
            response.write "<div class='alert alert-warning' style='margin-right: 10px'><strong>No Remedial Actions have been added</strong><br />Use the Add Action button to add the root cause for this issue</div>"
        end if
    else
    
            while not objrsACTS.eof        
    
    
        actions =  objrsACTS("action_text")          
    
        if session("pref_task_standards") = "Y"  then colspan =  "4" else colspan = "3"

        response.write "<table class='table table-bordered highlight"
    
        'if session("corp_code") <> "222999" then response.write  "highlight"        
        response.write "'><tr><th colspan='" & colspan & "'>Action</th></tr>" & _
                         "<tr><td  colspan='" & colspan & "'>"  & replace(actions, vbCrLf, "<br />")   & "</td></tr>" & _
                        "<tr><th>Actioned To</th>"
    
        if session("pref_task_standards") = "Y" then response.write "<th width='150px'>Associated Standard</th>"
    
        response.Write "<th width='150px'>Current Status</th><th width='150px'>Due Date</th></tr>"
    
        
                      
        action_to_email =  objrsACTS("action_to_email")
      
         if isnull(objrsACTS("name"))  then 'no task generated yet   
                action_to =  objrsACTS("action_to")       
                action_to_name =  objrsACTS("name2") 
                standard = objrsACTS("action_standard") 
                standard_text =  objrsACTS("task_standards_text") 
                action_date = objrsACTS("action_due_date") 
                var_status_class = "txt_shade_red"
                var_sa_curr_status = "Processing"
    
                var_pri_name = objrsACTS("action_priority_name")
                var_pri_col = objrsACTS("val_colour")
        else 'use TM for as many vals as possible

            action_to =  objrsACTS("task_account") 
            action_to_name =  objrsACTS("name") 
            standard = objrsACTS("action_standard") 
            standard_text =  objrsACTS("task_standards_text") 
            action_date = objrsACTS("task_due_date") 
            var_task_signoff_date  = objrsACTS("task_signoff_date") 
    
                var_pri_name = objrsACTS("action_priority_name")
                var_pri_col = objrsACTS("val_colour")
             var_sa_curr_status   = objrsACTS("task_status")            
    
           if session("PREF_TASK_SIGNOFF") = "Y" and var_sa_curr_status = "0" then
    
                if objrsACTS("task_signoff") = "0" then
                    var_so_status = "N"                 
                    var_so_status_text = var_so_status_text & "</span>"
                else
                    var_so_status = "Y"
                    if not isnull(var_task_signoff_date) then 
                        var_so_status_text  = "<br /><span class='info' title='Signed off by " & objrsacts("task_signoff_by_text")  & " on " & var_task_signoff_date & "'>Signed Off</span>"
                    end if
                end if
            else
                var_so_status = ""
                var_so_status_text = ""
            end if
                                                    
            select case var_sa_curr_status

            case 0 
                if var_so_status = "N" then 
                    var_status_class = "txt_shade_blue"
                    var_sa_curr_status = "Awaiting Sign Off"
                else
                    var_status_class = "txt_shade_grn"
                    var_sa_curr_status = "Complete"
                end if 
            case 1
                var_status_class = "txt_shade_org"
                var_sa_curr_status = "Active"                                                  
            case 2
                var_status_class = "txt_shade_red"
                var_sa_curr_status = "Pending"                                                                                
            case else
                var_status_class = "txt_shade_red"
                    var_sa_curr_status = "Processing"
            end select
        end if

        var_sa_curr_status = var_sa_curr_status & var_so_status_text



        'validation
        if standard = "0" then standard_text= "Not Applicable"
        if action_to = "Email" then action_to_name = "Email - " & action_to_email


        'Output
	     if isnull(objrsACTS("task_id")) = true then
                response.write "<tr><td>" & action_to_name & "</td>"
            else
            	  response.write "<tr><td ><div class='btn-group' role='group'><a class='btn btn-default btn-xs' href='../../../../management/taskmanager_v2/task_layout.asp?cmd=view_task&amp;frm_taskid=" & objrsACTS("task_id")  & "'>Open Task</a> <a class='btn btn-default btn-xs' onclick='javascript:OpenHistory(""" & objrsACTS("task_id")  & """,""" &  action_to_name & """,""" & audit_ref  & """)' >View History</a></div> " & action_to_name & "</td>"
            end if

            if session("pref_task_standards") = "Y" then response.write "<td >" & standard_text & "</td>"
    
            response.Write  "<td class='" & var_status_class & " c'  >" & var_sa_curr_status & "</td><td class='c' >" & action_date
            If len(var_pri_name) > 0 then response.write "<br /><span style='color: " & var_pri_col & "'>(" & var_pri_name & ")</span>"
            response.Write "</td></tr>"

            objrsACTS.movenext
        wend
        
    
        response.Write "</tbody></table>"
    end if

       
end sub

     
sub reload_specific_question_sa(questionID,  auditRef, templateID, access, weighting)
    'response.write access
    call generateQuestion(switch, auditRef, templateID, sectionID, questionID, access, weighting)
    
end sub

%>