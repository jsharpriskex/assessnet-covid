﻿<!-- #include file="../inc/auto_logout.inc" -->
<!-- #include file="../inc/dbconnections.inc" -->
<!-- #include file="action_functions.asp" -->
<!-- #INCLUDE FILE="../inc/action_control.asp" -->
<!-- #INCLUDE file="../inc/autogen_datetime_v2.inc" -->
<%CALL startCONNECTIONS()
DIM objRs,objConn,objCommand, uid
    
uid = request("ref") & "_" &  request("sub_ref")
var_tier2_id = request("t2")
var_tier3_id = request("t3")
var_tier4_id = request("t4")
var_tier5_id = request("t5")
var_tier6_id = request("t6")
var_option = request("option")
var_access = request("access")

'Options
'SAR = Safety Audit Report

objcommand.commandtext = "SELECT DISTINCT  d.template_id from " & APPLICATION("DBTABLE_SA_AUDIT_ENTRIES") & " AS d " & _
					     "WHERE (d.audit_ref = '" & request("ref") & "') AND (d.corp_code = '" & session("corp_code") & "')"
set objrs = objcommand.execute

if not objrs.eof then
    var_template_id = objrs("template_id")
end if


var_assigned_elsewhere = false

objcommand.commandtext = "SELECT DISTINCT  d.is_action_assigned_elsewhere from " & APPLICATION("DBTABLE_SA_AUDIT_DATA") & " AS d " & _
					     "WHERE (d.audit_ref = '" & request("ref") & "') AND (d.corp_code = '" & session("corp_code") & "') and question_id = '" &  request("sub_ref") &  "'"
set objrs = objcommand.execute

if not objrs.eof then
    var_assigned_elsewhere = objrs("is_action_assigned_elsewhere")
end if

objcommand.commandtext = "SELECT act.action_sign_off_required from " & Application("DBTABLE_SA_ACTIONS") & " as act " & _
                        "WHERE (act.audit_ref = '" & request("ref") & "') AND (act.corp_code = '" & session("corp_code") & "') and question_id = '" &  request("sub_ref") &  "'"
'response.write objcommand.commandtext
'set objrs = objcommand.execute

'if not objrs.eof then
'    so_required = objrs("sa_sign_off_required")
'end if



actions_count = count_actions(request("module"), request("ref"), request("sub_ref"))

%>
<!DOCTYPE html>
<html>
<head>
    <title></title>
	<meta charset="utf-8" />
    <script type="text/javascript">


        priority_freedate= '<%=session("pref_task_priority_freedate")%>'
        task_standards = '<%=session("pref_task_standards")%>'
       
        function open_action_form<%=uid%>()
        { 
            $("#div_action_btns_<%=uid%>").hide()
            var rowCount = $('#count_actions_<%=request("sub_ref")%>').text();
            
            if (rowCount == '0') // Only use default actions if there are no actions currently assigned          
            {
                    //Are there any default actions to use?
                    var default_acts = $("#td_actions_<%=request("sub_ref")%>").attr('default_act');
                    $("#tb_actions_<%=uid%>").val(default_acts);
            
                    var default_user = $("#td_actions_<%=request("sub_ref")%>").attr('default_user');
                    if (default_user != 'user') {
                        $("#ddl_action_to_<%=uid%>").val(default_user);
                    };

                    var default_date = $("#td_actions_<%=request("sub_ref")%>").attr('default_date');
                    if (default_date != 'user') {
                        var datesplit = default_date.split("/"); 

                        $("#ddl_date_day_<%=uid%>").val(datesplit[0]);
                        $("#ddl_date_month_<%=uid%>").val(datesplit[1]);
                        $("#ddl_date_year_<%=uid%>").val(datesplit[2]);
                    };
            }

            $("#div_action_<%=uid%>").show()


        }

             

        function saveAction<%=uid %>() 
        {

            valid = 1
            errorColour = '#99ccff'
            okColour = '#ffffff'
            errMsg = "There is an error in your form. Please correct the following fields:\n"

            var sign_off = $("input[name=cb_require_Sign_Off_<%=uid%>]").prop('checked')
           
            
            var actions = $("#tb_actions_<%=uid%>");

            if (actions.val().length == 0) {
                valid = 0;
                errMsg += "\n - Please provide a remedial action for this task";
                actions.css("background-color", errorColour);
            } else {
                actions.css("background-color", okColour);
            }

            var actioned_to_email = $("#tb_action_to_email_<%=uid%>");
            var actioned_to = $("#ddl_action_to_<%=uid%>");
           
            if (actioned_to.val() == "0") {
                valid = 0;
                errMsg += "\n - Please select a user to issue this task to";
                actioned_to.css("background-color", errorColour);
            } else if (actioned_to.val() == "Email") {
               
                if (!isEmail(actioned_to_email.val())) {
                    valid = 0;
                    errMsg += "\n - Please enter an email address";
                    actioned_to_email.css("background-color", errorColour);
                } else {
                    actioned_to_email.css("background-color", okColour);                                    
                }
            } else {
                actioned_to.css("background-color", okColour);

            }
        
                                          
                            

            var date_choice = "date";
            var date_type = $("#ddl_priority_<%=uid%>_used");
            var priority_value = $("#ddl_priority_<%=uid%>").length;
            
            if (priority_freedate != "Y" && $("#ddl_priority_<%=uid%>").length) 
            {
                date_choice = "pri"
            } 
            else 
            {
                if (date_type.val()) 
                {
                     date_choice =   $('input[name=ddl_priority_<%=uid%>_rad]:checked').val();
                }
                    
            }

            
       

            if (date_choice == "pri") 
            {
               
                if ($("#ddl_priority_<%=uid%>").val()=="na") {
                    valid = 0;
                    errMsg += "\n - Please select a priority option from the list provided";
                    $("#ddl_priority_<%=uid%>").css("background-color", errorColour);
                } else {
                    $("#ddl_priority_<%=uid%>").css("background-color", okColour);

                   if (!isvalidDate($("#ddl_date_day_<%=uid%>").val(),  $("#ddl_date_month_<%=uid%>").val(),  $("#ddl_date_year_<%=uid%>").val())) {
                    
                        valid = 0;
                        errMsg += "\n - Due date for completion / review must be a date";
                        $("#ddl_date_day_<%=uid%>").css("background-color", errorColour);
                        $("#ddl_date_month_<%=uid%>").css("background-color", errorColour);
                        $("#ddl_date_year_<%=uid%>").css("background-color", errorColour);
                    } else {
                        
                        // prioritiesArry must be made available in header section of calling page (See FRA/RA modules for reference)

                        // is date within priority date range (from array)
                        var maxD = prioritiesArry[$("#ddl_priority_<%=uid%>").prop("selectedIndex") - 1].substring(0, 2);
                        var maxM = prioritiesArry[$("#ddl_priority_<%=uid%>").prop("selectedIndex") - 1].substring(3, 5);
                        var maxY = prioritiesArry[$("#ddl_priority_<%=uid%>").prop("selectedIndex") - 1].substring(6, 10);

                        var maxDate = new Date(maxY, maxM - 1, maxD, "23", "59", "59", "000");
                        var selDate = new Date($("#ddl_date_year_<%=uid%>").val(), ($("#ddl_date_month_<%=uid%>").val() - 1), $("#ddl_date_day_<%=uid%>").val(), "23", "59", "59", "000");
                        var curDate = new Date();
                      
                        if (selDate > maxDate || curDate > selDate) {
                            valid = 0;
                            errMsg += "\n - Due date must be between today and " + prioritiesArry[$("#ddl_priority_<%=uid%>").prop("selectedIndex") - 1].substring(0, 10);
                            $("#ddl_date_day_<%=uid%>").css("background-color", errorColour);  
                            $("#ddl_date_month_<%=uid%>").css("background-color", errorColour);  
                            $("#ddl_date_year_<%=uid%>").css("background-color", errorColour); 
                            
                            pri= '';
                            pri_qty='';
                            pri_type='';
                        } else {
                            $("#ddl_date_day_<%=uid%>").css("background-color", okColour);  
                            $("#ddl_date_month_<%=uid%>").css("background-color", okColour);  
                            $("#ddl_date_year_<%=uid%>").css("background-color", okColour);  
                           
                            pri=$("#ddl_priority_<%=uid%> option:selected").val();
                            pri_qty='';
                            pri_type='';
                            //alert(pri);
                        }
                    }
                }
	   
            }
            else // no priority. just check date
            {
                pri='';
                pri_qty='';
                pri_type='';

                if (!isvalidDate($("#ddl_date_day_<%=uid%>").val(), $("#ddl_date_month_<%=uid%>").val(), $("#ddl_date_year_<%=uid%>").val())) 
                { 
                    valid = 0
                    errMsg += "\n - Due date for completion / review must be a date"
                    $("#ddl_date_day_<%=uid%>").css("background-color", errorColour);  
                    $("#ddl_date_month_<%=uid%>").css("background-color", errorColour);  
                    $("#ddl_date_year_<%=uid%>").css("background-color", errorColour);  
                }
                else 
                {
                    var date1 = new Date()
                    date1.setFullYear($("#ddl_date_year_<%=uid%>").val(), $("#ddl_date_month_<%=uid%>").val()-1, $("#ddl_date_day_<%=uid%>").val());  
  		           		           
                    today_date = new Date()
  		            		           
                    if (date1 < today_date)
                    {
                        valid = 0
                        errMsg += "\n - Due date for completion / review must be in the future"
                        $("#ddl_date_day_<%=uid%>").css("background-color", errorColour);  
                        $("#ddl_date_month_<%=uid%>").css("background-color", errorColour);  
                        $("#ddl_date_year_<%=uid%>").css("background-color", errorColour);  
                    }
                    else
                    {
                        $("ddl_date_day_<%=uid%>").css("background-color", okColour);  
                        $("ddl_date_month_<%=uid%>").css("background-color", okColour);  
                        $("ddl_date_year_<%=uid%>").css("background-color", okColour);  
                    }
                }
            }

            if (task_standards == "Y") {
                if ($("#ddl_standard_<%=uid%>")) {
                    if ($("#ddl_standard_<%=uid%>").prop("selectedIndex") == 0) {
                        valid = 0
                        errMsg += "\n - Please select the associated standard for this task"
                        $("#ddl_standard_<%=uid%>").css("background-color", errorColour);    
                        std = '';

                    } else {
                        $("#ddl_standard_<%=uid%>").css("background-color", okColour);   
                        std = $("#ddl_standard_<%=uid%>").val();
                    }
                }
            }
            else {
                std = '';
            }


 	       
            if (!valid)
            {
                alert(errMsg)
                return false
            }
            else 
            {
                var cmd = "add";
                var action_id = "";

                //commit save
                if ($("#edit_action_id<%=uid%>").val() !="")
                {
                    cmd = "edit";
                    action_id = "&act_id=" + $("#edit_action_id<%=uid%>").val(); 
                }  
                
                //alert($("#ddl_date_day_<%=uid%>").val() + '/' + $("#ddl_date_month_<%=uid%>").val() + '/' +$("#ddl_date_year_<%=uid%>").val())

                var postData = 'actions=' + encodeURIComponent(actions.val()) + '&act_date_day=' + $("#ddl_date_day_<%=uid%>").val() + '&act_date_month=' + $("#ddl_date_month_<%=uid%>").val() + '&act_date_year=' + $("#ddl_date_year_<%=uid%>").val() + '&act_to=' + actioned_to.val() + '&act_to_email=' + encodeURIComponent(actioned_to_email.val()) + '&pri=' + pri +  '&std=' + std + '&so_req=' + $('#cb_require_Sign_Off_<%=uid%>').is(":checked") + '&type=<%=request("type")%>' // '&pri_qty=' + pri_qty + '&pri_type=' + pri_type +
                 $.ajax({                     
                     url: '../../../../../data/scripts/Actions/action_commands.asp?cmd=' + cmd + '&module=<%=request("module")%>&module_ref=<%=request("ref")%>&so_req=' + sign_off + '&module_sub_ref=<%=request("sub_ref")%>' + action_id + '&' + postData,
                     cache: false,
                     type: "POST",   
                     data: postData,
                    success: function(data) {
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Saved Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                
                        //refresh whole Actions div
                        populateActions('<%=request("sub_ref")%>')
                        reload_specific_question_sa()

                        
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                       // alert(xhr.status);
                        alert(thrownError);
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was a problem saving the action. Please double check your internet connection.</div>"); 
                    }
                 });

               
                //Disable button

                 $("#action_save_button_<%=uid %>").prop('disabled', true).val('Updating..')


                return true
            }	

           
        }


        function remove_action_<%=uid%>(actionID)
        {
            
            var r =  confirm("Are you sure you want to remove this Action?");
            if (r==true)
            {

                //TODO
                //commit save
                $.ajax({
                    url: '../../../../../data/scripts/Actions/action_commands.asp?cmd=remove&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>&act_id=' + actionID,
                    cache: false,
                    success: function(data) {
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Removed Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                
                        //refresh whole Actions div
                        populateActions('<%=request("sub_ref")%>')
                        reload_specific_question_sa()
                        refresh_Actions_Status('<%=request("sub_ref")%>', '')
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                    //    alert(xhr.status);
                     //   alert(thrownError);
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem removing the Action. Please double check your internet connection.</div>"); 
                    }
                });

                return true;            
            }
            else
            {
                return false;
            }        
        }

        function reload_specific_question_sa()
        {
            
            //TODO
            //commit save
            $.ajax({
                url: '../../../../../data/scripts/Actions/action_commands.asp?cmd=reload_specific_question_sa&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>&questionID=<%=request("sub_ref")%>&template_id=<%=var_template_id%>&access=<%=var_access%>',
                cache: false,
                success: function(data) {
                    $('#<%=request("sub_ref")%>')
                        .html(data); 
                
                   
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    //    alert(xhr.status);
                    //   alert(thrownError);
                    $("#div_action_message_<%=uid%>")
                        .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem removing the Action. Please double check your internet connection.</div>"); 
                }
            });

            return true;            
                    
        }

        function mark_action_elsewhere<%=uid%>()
        {
            
            
                //commit save
                $.ajax({
                    url: '../../../../../data/scripts/Actions/action_commands.asp?cmd=mark_appropriate_actions&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>',
                    cache: false,
                    success: function(data) {
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Updated Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                
                        //refresh whole Actions div
                        populateActions('<%=request("sub_ref")%>')

                        reload_specific_question_sa();

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                       // alert(xhr.status);
                      //  alert(thrownError);
                        $("#div_action_message_<%=uid%>")
                            .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem updating these actions. Please double check your internet connection.</div>"); 
                    }
                });

                return true;            
           
        }


        function unmark_action_elsewhere<%=uid%>()
        {
            
            
            //commit save
            $.ajax({
                url: '../../../../../data/scripts/Actions/action_commands.asp?cmd=remove_mark_appropriate_actions&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>',
                cache: false,
                success: function(data) {
                    $("#div_action_message_<%=uid%>")
                        .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Updated Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                    reload_specific_question_sa()
                    //refresh whole Actions div
                    populateActions('<%=request("sub_ref")%>')
                },
                error: function(xhr, ajaxOptions, thrownError) {
                   // alert(xhr.status);
                   // alert(thrownError);
                    $("#div_action_message_<%=uid%>")
                        .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem updating these actions. Please double check your internet connection.</div>"); 
                }
            });

            return true;            
           
        }

        
        function edit_action_<%=uid%>(action_id, actions, action_to, action_to_email, action_date, standard, pri , sign_off, signoff_block, your_access, acc_ref, auditor, action_creator) //TODO
        {
         
            //update the save button and a holding id
            $("#action_save_button_<%=uid%>").val("Update Action");

           //populate data
            $("#edit_action_id<%=uid%>").val(action_id)   
            
            
            $("#tb_actions_<%=uid%>").val($("#actions_<%=uid%>_" + action_id).attr("title"));
            $("#ddl_action_to_<%=uid%>").val(action_to);

            if (action_to == 'Email')
            {
                $("#ddl_action_to_<%=uid%>_maindiv").hide()
                $("#ddl_action_to_<%=uid%>_emaildiv").show()               
            }
            else
            {
                $("#ddl_action_to_<%=uid%>_maindiv").show()
                $("#ddl_action_to_<%=uid%>_emaildiv").hide()
            }

            //sign-off
            //alert (sign_off)
            //alert (action_id)

            if(sign_off == "True" || sign_off == 1){
                $("input[name=cb_require_Sign_Off_<%=uid%>]").prop('checked', true);
            } else {
                $("input[name=cb_require_Sign_Off_<%=uid%>]").prop('checked', false);
            }

            $("tb_action_to_email_<%=uid%>").val(action_to_email)


            $("#ddl_date_day_<%=uid%>").val(action_date.substring(0,2))
            $("#ddl_date_month_<%=uid%>").val(action_date.substring(3,5))
            $("#ddl_date_year_<%=uid%>").val(action_date.substring(6,10))

            if(signoff_block == "Y"){
                if (your_access == "0" || auditor == acc_ref || created_by == acc_ref){
                    $("input[name=cb_require_Sign_Off_<%=uid%>]").attr("disabled", false);
                } else {
                    $("input[name=cb_require_Sign_Off_<%=uid%>]").attr("disabled", true);
                }
            }
            
            if(signoff_block == "Y" && sign_off == "True" || sign_off == 1){
                $("#ddl_date_day_<%=uid%>").attr("disabled", true);
                $("#ddl_date_month_<%=uid%>").attr("disabled", true);
                $("#ddl_date_year_<%=uid%>").attr("disabled", true);
                $("#ddl_priority_<%=uid%>_rad_pri").attr("disabled", true);
                $("#ddl_priority_<%=uid%>").attr("disabled", true);
            }
            else{
                $("#ddl_date_day_<%=uid%>").attr("disabled", false);
                $("#ddl_date_month_<%=uid%>").attr("disabled", false);
                $("#ddl_date_year_<%=uid%>").attr("disabled", false);
                $("#ddl_priority_<%=uid%>_rad_pri").attr("disabled", false);
                $("#ddl_priority_<%=uid%>").attr("disabled", false);
            }


            $("input[name=cb_require_Sign_Off_<%=uid%>]").click(function () {
                if ($("input[name=cb_require_Sign_Off_<%=uid%>]").not(":checked")) {
                    $("#ddl_date_day_<%=uid%>").attr("disabled", false);
                    $("#ddl_date_month_<%=uid%>").attr("disabled", false);
                    $("#ddl_date_year_<%=uid%>").attr("disabled", false);
                    $("#ddl_priority_<%=uid%>_rad_pri").attr("disabled", false);
                    $("#ddl_priority_<%=uid%>").attr("disabled", false);
                }
            });

            if ($("#ddl_standard_<%=uid%>"))
            { 
                if (standard == "0")
                {
                    $("#ddl_standard_<%=uid%>").prop('selectedIndex', 1);
                }
                else
                {
                    $("#ddl_standard_<%=uid%>").val(standard); 
                }
            }

            if (pri != "")
            {
                $("#ddl_priority_<%=uid%>_rad_pri").prop("checked", true);
                $("#ddl_priority_<%=uid%>_priority_div").show()
                $("#ddl_priority_<%=uid%>").val(pri);
            }
            else
            {
                $("#ddl_priority_<%=uid%>_rad_date").prop("checked", true);      
                $("#ddl_priority_<%=uid%>_priority_div").hide()
                $("#ddl_priority_<%=uid%>").prop('selectedIndex', 1);
                
            }
           
            
            // open up the div
            open_action_form<%=uid%>();
                
        }

        function clear_action_data_<%=uid%>() //TODO
        {
           
            //refresh whole Actions div
            populateActions('<%=request("sub_ref")%>')
        }


    </script>

</head>
<body>
    <div id="div_action_message_<%=uid %>"></div>
   
    
    <%if not var_assigned_elsewhere then %>


        <%if var_option <> "SAR" then %>
        <div id="div_action_btns_<%=uid %>" style="padding: 5px">
        <a id="btn_add_action_<%=uid %>" onclick="open_action_form<%=uid%>()" class="btn btn-xs btn-success">Add Action</a>    

            <%if actions_count = 0 then %>
            or
                <a id="btn_elsewhere_action_<%=uid %>" onclick="mark_action_elsewhere<%=uid%>()" class="btn btn-xs btn-warning">Mark as appropriate actions have been assigned elsewhere</a>
            <%end if %>
        </div>
        <%elseif var_option = "SAR" then %>

        <% call display_actions(request("module"), request("ref"), request("sub_ref")) %>

        <div id="div_action_btns_<%=uid %>" style="padding: 5px; height: 25px;">
            <div class="col-md-12 text-center">
                <a id="btn_add_action_<%=uid %>" onclick="open_action_form<%=uid%>()" class="btn btn-primary">Add Action</a>
            </div>
        </div>
        <br />

        <%end if %>
     <form>

         <%'response.write "test2 " & so_required & "<br />" %>
    
    <div id="div_action_<%=uid %>" style="display: none;">
        <input type="hidden" id="edit_action_id<%=uid%>" />
        <%
            call Generate_Action_HTML("Remedial Action", "Due Date", "Actioned To", "", "tb_actions_" & uid , "", "ddl_date_day_" & uid, "ddl_date_month_" & uid, "ddl_date_year_" & uid, "NONE", "ddl_action_to_" & uid, "", "tb_action_to_email_" & uid , "", false, "", "", "", "", "", "", "", "", "", "highlight showtd", false, "", "", "", "N", var_tier2_id, var_tier3_id, sw1, sw2, sw3, "", var_tier4_id, "ddl_priority_" & uid, sw7, sw8, var_tier5_id, var_tier6_id, "SA", sw12, "", "ddl_standard_" & uid, "", default_actions, sw17, "cb_require_Sign_Off_" & uid, so_required, sw20)
        %>                  
        <br />
        <div class="form-group col-sm-12">
            <input type="button"  class="btn btn-sm btn-warning col-sm-2 col-sm-offset-3"  value="Cancel" onclick="return clear_action_data_<%=uid %>()" /> 
            &nbsp;<input type="button" id="action_save_button_<%=uid %>" class="btn btn-sm btn-success col-sm-2 col-sm-offset-1  "  value="Save Action" onclick="return saveAction<%=uid %>()" />
        </div>
        <div class="clearfix"></div>
        <!--<hr />-->
    </div>

        <!-- Field below is used to populate the values of the counts on each of the questions in the main audit -->
         <input type="hidden" id="hidden_actions_count_<%=uid %>" value='<%=actions_count %>' />
    </form>

  
    <%if var_option <> "SAR" Then 
        call display_actions(request("module"), request("ref"), request("sub_ref"))
    end if %>

    <script type='text/javascript'>
          
            $('.textarea').keyup(function () {
                autoresizeTextArea(this);
            });
        </script>

    <%
    else
         response.write "<div class='alert alert-success clearfix' style='margin-right: 10px'><span style = 'float: left'><strong>Actions Assigned Elsewhere</strong><br />The auditor has indicated that suitable remedial actions to this issue have been entered elsewhere on this audit.</span><a id='btn_elsewhere_action_" & uid & "' onclick='unmark_action_elsewhere" & uid & "()' class='btn btn-xs btn-primary' style='float: right;'>Reset Actions Assigned Elsewhere</a></div>"
       
        'set this field so the status in the table header can be set 
          response.write "<input type='hidden' id='hidden_actions_count_" & uid & "' value='assigned_elsewhere' />"
    end if %>
</body>
</html>
<%Call   endConnections() %>