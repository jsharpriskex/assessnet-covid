﻿<!-- #include file="../inc/auto_logout.inc" -->
<!-- #include file="../inc/dbconnections.inc" -->
<!-- #include file="action_functions.asp" -->
<%   
CALL startCONNECTIONS()
DIM objRs,objConn,objCommand, uid
Dim cmd

cmd = request("cmd")
    
        module = validateinput(request("module"))
        module_ref = validateinput(request("module_ref"))  'Audit = audit ref
        module_sub_ref = validateinput(request("module_sub_ref")) 'Audit = question ref
       ' module_additional_ref = validateinput(request("module_additional_ref"))   'Audit = section ref
        
        action_id = validateinput(request("act_id"))
    
        uid = module_ref & "_" & module_sub_ref
        
        actions = validateinput(request("actions"))
        action_date = validateinput(request("act_date_day")) & "/" & validateinput(request("act_date_month")) & "/" & validateinput(request("act_date_year"))
        action_to = validateinput(request("act_to"))
        action_to_email = validateinput(request("act_to_email"))
        action_priority = validateinput(request("pri"))
        action_priority_qty = validateinput(request("pri_qty"))
        action_priority_type = validateinput(request("pri_type"))
        action_standard = validateinput(request("std"))
        action_sign_off_required = validateinput(request("so_req"))


select case cmd
        
    case "add"        

        call add_action(module, module_ref, module_sub_ref,  actions, action_date, action_to, action_to_email, action_priority, action_priority_qty, action_priority_type, action_standard, action_sign_off_required)

    case "remove"
       call remove_action(action_id, module, module_ref, module_sub_ref)

    case "edit"
        call edit_action(action_id, module, module_ref, module_sub_ref,  actions, action_date, action_to, action_to_email, action_priority, action_priority_qty, action_priority_type, action_standard, action_sign_off_required)

    case "mark_appropriate_actions"
        call mark_appropriate_actions(action_id, module, module_ref, module_sub_ref)
    

    case "remove_mark_appropriate_actions"
        call remove_mark_appropriate_actions(action_id,  module, module_ref, module_sub_ref)
    
    
     case else
        response.write "Unknown command"   
   
end select

Call   endConnections() 
    
%>