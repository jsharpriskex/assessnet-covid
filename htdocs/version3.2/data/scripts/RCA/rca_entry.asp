﻿<!-- #include file="../inc/auto_logout.inc" -->
<!-- #include file="../inc/dbconnections.inc" -->
<!-- #include file="rca_functions.asp" -->
<%CALL startCONNECTIONS()
DIM objRs,objConn,objCommand, uid
    
uid = request("ref") & "_" &  request("sub_ref")
%>
<!DOCTYPE html>
<html>
<head>
    <title></title>
	<meta charset="utf-8" />
    <script type="text/javascript">
       
        function open_rca_form<%=uid%>()
        { 
            $("#btn_add_rca_<%=uid%>").hide()
            $("#div_rca_<%=uid%>").show()
        }

        function update_sub_cause<%=uid%>(selected_option)
        {
            var parent_value = $("#ddl_cause_<%=uid%>").val()
            
            var str_array = parent_value.split(":")
            var item_value = str_array[0]
            var child_present = str_array[1]


            if (child_present == 1)
            {
                //child present

                //populate the child select element
                $("#ddl_sub_cause_<%=uid%>").empty()

                $.ajax({
                    url: '../../../../../data/scripts/RCA/rca_commands.asp?cmd=refresh_sub_cause&parent=' + item_value + '&selected_option=' + selected_option,
                    cache: false,
                    success: function(data) {
                        $("#ddl_sub_cause_<%=uid%>").append(data);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                   //     alert(xhr.status);
                     //   alert(thrownError);
                        $("#ddl_sub_cause_<%=uid%>")
                            .append($("<option></option>")
                            .attr("value","x")
                            .text("Sub causes could not be returned at this time")); 
                        //select this placeholder so its value is set
                        $("#ddl_sub_cause_<%=uid%>").val("x");
                    }
                });

                // for ui purposes
                $("#ddl_sub_cause_<%=uid%>")
                     .append($("<option></option>")
                     .attr("value","x")
                     .text("Choose a sub root cause")); 



                //show the child div

                $("#sub_cause_<%=uid%>").show()

            }
            else 
            {   //no child.

                //hide the sub cause div
                $("#sub_cause_<%=uid%>").hide()
                //empty any options in there
                $("#ddl_sub_cause_<%=uid%>").empty()
                //throw in a place holder
                $("#ddl_sub_cause_<%=uid%>")
                    .append($("<option></option>")
                    .attr("value","x")
                    .text("placeholder")); 
                //select this placeholder so its value is set
                $("#ddl_sub_cause_<%=uid%>").val("x");
            }
        }

        function saveRootCause<%=uid %>()
        {
            comments = ""
            prc = "0"
            src = "0"

            valid = 1
            col0 = '#99ccff'
            col1 = '#ffffff'
            errMsg = "There is an error in your form. Please correct the following fields:\n"

            if ("x" ==  $("#ddl_cause_<%=uid%>").val())
            {
                valid = 0
                errMsg += "\n - Please choose a root cause"
                $("#ddl_cause_<%=uid%>").css("background-color", col0);
            }           
            else
            {
                $("#ddl_cause_<%=uid%>").css("background-color", col1);
                 

                //check sub root cause
                var selected_value = $("#ddl_cause_<%=uid%>").val()
                var str_array = selected_value.split(":")
                var prc = str_array[0]
                var child_present = str_array[1]

                if (child_present == 1)
                {
                    if ("x" ==  $("#ddl_sub_cause_<%=uid%>").val())
                    {
                        valid = 0
                        errMsg += "\n - Please choose a root cause"                        
                        $("#ddl_sub_cause_<%=uid%>").css("background-color", col0);

                    }           
                    else
                    {
                        $("#ddl_sub_cause_<%=uid%>").css("background-color", col1);
                        src =  $("#ddl_sub_cause_<%=uid%>").val()
                    }
                
                }
                
            }

            //comments are optional
            comments = $("#tb_comments_<%=uid%>").val()
 	       
            if (!valid)
            {
                alert(errMsg)
                return false
            }
            else 
            {
                var cmd = "add";
                var rca_id = "";

                //commit save
                if ($("#edit_rca_id<%=uid%>").val() !="")
                {
                    cmd = "edit";
                    rca_id = "&rca=" + $("#edit_rca_id<%=uid%>").val(); 
                }    

                var postData = "c=" + encodeURIComponent(comments) + "&prc=" + prc + "&src=" + src;


                 $.ajax({
                     url: '../../../../../data/scripts/RCA/rca_commands.asp?cmd=' + cmd + '&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>' + rca_id ,
                     cache: false,
                     type: "POST",   
                     data: postData,
                    success: function(data) {
                        $("#div_rca_message_<%=uid%>")
                            .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Saved Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                
                        //refresh whole RCA div
                        populateRCA('<%=request("sub_ref")%>')
                        refresh_RCA_Status('<%=request("sub_ref")%>', '')

                        
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                       // alert(xhr.status);
                       // alert(thrownError);
                        $("#div_rca_message_<%=uid%>")
                            .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem saving the Root Cause Analysis. Please double check your internet connection.</div>"); 
                    }
                 });

               

                return true
            }	

           
        }


        function remove_rca_<%=uid%>(rca)
        {
            
            var r =  confirm("Are you sure you want to remove this Root Cause?");
            if (r==true)
            {
                //commit save
                $.ajax({
                    url: '../../../../../data/scripts/RCA/rca_commands.asp?cmd=remove&module=<%=request("module")%>&module_ref=<%=request("ref")%>&module_sub_ref=<%=request("sub_ref")%>&rca=' + rca,
                    cache: false,
                    success: function(data) {
                        $("#div_rca_message_<%=uid%>")
                            .html("<div class='alert alert-success' style='margin-right: 10px'><strong>Removed Successfully!</strong><br />All data saved successfully. Please wait whilst the table refreshes.</div>"); 
                
                        //refresh whole RCA div
                        populateRCA('<%=request("sub_ref")%>')
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                      //  alert(xhr.status);
                        //alert(thrownError);
                        $("#div_rca_message_<%=uid%>")
                            .html("<div class='alert alert-danger' style='margin-right: 10px'><strong>Warning</strong><br />There was an problem removing the Root Cause Analysis. Please double check your internet connection.</div>"); 
                    }
                });

                return true;            
            }
            else
            {
                return false;
            }        
        }


        function edit_rca_<%=uid%>(rca, prc, src)
        {
           
            //update the save button and a holding id
            $("#rca_save_button_<%=uid%>").val("Update Root Cause");

           //populate data
            $("#edit_rca_id<%=uid%>").val(rca)            
            $("#tb_comments_<%=uid%>").val($("#comments_<%=uid%>_" + rca).attr("title")) ;

            var exists = false;
            $('#ddl_cause_<%=uid%> option').each(function(){
                if (this.value == prc + ':1') {
                    exists = true;
                    return false;
                }
            });

            if (exists)
            {
                $("#ddl_cause_<%=uid%>").val(prc + ":1");
                update_sub_cause<%=uid%>(src)
            }
            else
            {
                $("#ddl_cause_<%=uid%>").val(prc + ":0");            
            }

            
          //  $("#ddl_cause_<%=uid%>").selectmenu('refresh');

          //  alert( $("#ddl_cause_<%=uid%>").val());
            
            // open up the div
            open_rca_form<%=uid%>();
                 
        }

        function clear_rca_data_<%=uid%>()
        {
            $("#rca_save_button_<%=uid%>").val("Save Root Cause");
            $("#div_rca_<%=uid%>").hide()
            $("#edit_rca_id<%=uid%>").val("")
            $("#tb_comments_<%=uid%>").val("")
            $("#ddl_cause_<%=uid%>").val("");
            $("#sub_cause_<%=uid%>").hide()
            $("#btn_add_rca_<%=uid%>").show()

        }

    </script>

</head>
<body>
    <div id="div_rca_message_<%=uid %>"></div>
    <form>
    
    <%'only show button if active RCA's = 0 or the preference for single RCA's is turned off
    

     if  session("RCA_Limit_To_One") <>  "Y" or (session("RCA_Limit_To_One") =  "Y" and count_rcas(request("module"), request("ref"), request("sub_ref")) < 1) then
    %>
    <a id="btn_add_rca_<%=uid %>" onclick="open_rca_form<%=uid%>()" class="btn btn-xs btn-success">Add RCA</a>
    <%end if %>
    <div id="div_rca_<%=uid %>" style="display: none;">
        <input type="hidden" id="edit_rca_id<%=uid%>" />
       <div class="form-group col-sm-8">
            <label class="col-sm-3" for="ddl_cause_<%=uid %>">Primary Cause</label>
                     
                <select class="form-control"  name="ddl_cause_<%=uid %>" id="ddl_cause_<%=uid %>" class="col-sm-8"  onchange="update_sub_cause<%=uid%>(0)" >
                    <option value="x">Choose a primary root cause</option>

                    <%call fill_rca_options(0) %>
                </select>            
            </div>
        

        <div class="form-group  col-sm-8"  id="sub_cause_<%=uid %>" style="display: none;">
            <label class="col-sm-3" for="ddl_sub_cause_<%=uid %>">Secondary Cause</label>
            <select class="form-control"  name="ddl_sub_cause_<%=uid %>" id="ddl_sub_cause_<%=uid %>"  >
               <!--filled from ajax-->
            </select>
        </div>

        <div class="form-group col-sm-8">
            <label class="col-sm-3 " for="tb_comments_<%=uid %>">Comments</label>
            <textarea class="form-control" rows="3" name="tb_comments_<%=uid %>" id="tb_comments_<%=uid %>"></textarea>
        </div>

         <div class="form-group col-sm-8">
        <input type="button"  class="btn btn-sm btn-warning col-sm-2 col-md-offset-3"  value="Cancel" onclick="return clear_rca_data_<%=uid %>()" /> 
        &nbsp;<input type="button" id="rca_save_button_<%=uid %>" class="btn btn-sm btn-success col-sm-4   "  value="Save Root Cause" onclick="return saveRootCause<%=uid %>()" />
        </div>
        <div class="clearfix"></div>

        <hr />
    </div>

        <!-- Field below is used to populate the values of the counts on each of the questions in the main audit -->
        <input type="hidden" id="hidden_rca_count_<%=uid %>" value='<%= count_rcas(request("module"), request("ref"), request("sub_ref"))%>' />
    </form>

  

    <%call display_rcas(request("module"), request("ref"), request("sub_ref"), true)%>

</body>
</html>
<%Call   endConnections() %>