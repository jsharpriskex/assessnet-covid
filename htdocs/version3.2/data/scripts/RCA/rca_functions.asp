<%
    
sub add_rca(rc, src, comments, module, module_ref, module_sub_ref)
   
    objCommand.commandtext = "insert into " & Application("DBTABLE_RCA_Entries") & " (corp_code, created_by, root_cause, secondary_cause, comments, module, module_ref, module_sub_ref) values " & _
                                            " ('" & session("corp_code") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & rc & "', '" & src & "', '" & comments & "', '" & module & "', '" & module_ref & "', '" & module_sub_ref & "') "
    response.write objCommand.commandtext 
    objCommand.execute

    'Lock RCA option
     objCommand.commandtext = "update " & Application("DBTABLE_RCA_OPTS") & " set  locked = 1 " & _
                                            " where corp_code = '" & session("corp_code") & "' and opt_id ='" & rc & "'"
    
    objCommand.execute

    'Lock RCA  sub option

         objCommand.commandtext = "update " & Application("DBTABLE_RCA_OPTS") & " set  locked = 1 " & _
                                            " where corp_code = '" & session("corp_code") & "' and opt_id ='" & src & "'"
    
    objCommand.execute
    
end sub

sub fill_rca_options(selected_option)


      objCommand.commandtext = "select * from " & Application("DBTABLE_RCA_OPTS") & " " & _
                                       " where corp_code = '" & session("corp_code") & "' and parent_id = 0 "
     set objrs = objCommand.execute

    if not  objrs.eof then
   
         while not objrs.eof
             response.Write "<option value='" & objrs("opt_id") & ":" & objrs("child_present")  & "' "
             if cint(selected_option) =  cint(objrs("opt_id")) then response.write " selected "
             response.write " >" & objrs("opt_value") & "</option>"

            objrs.movenext
        wend
        
    end if
    
end sub

    
sub fill_rca_sub_options(selected_option, parent)


      objCommand.commandtext = "select * from " & Application("DBTABLE_RCA_OPTS") & " " & _
                                       " where corp_code = '" & session("corp_code") & "' and parent_id = '" & parent & "' "
     set objrs = objCommand.execute

    if not  objrs.eof then
   
         while not objrs.eof
             response.Write "<option value='" & objrs("opt_id") & "' " '& ":" & objrs("child_present") 
             if cint(selected_option) =  cint(objrs("opt_id")) then response.write " selected "
             response.write " >" & objrs("opt_value") & "</option>"

            objrs.movenext
        wend
        
    end if
    
end sub

sub remove_rca(rc_id, module, module_ref, module_sub_ref)
   
    objCommand.commandtext = "update " & Application("DBTABLE_RCA_Entries") & " set  isActive = 0, removedDateTime = getdate(), removed_by = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                       " where corp_code = '" & session("corp_code") & "' and isActive = 1 and id = '" & rc_id & "' and  module = '" & module & "' and module_ref='" & module_ref & "' and module_sub_ref='" & module_sub_ref & "' "
    'response.write  objCommand.commandtext
   ' response.end
    objCommand.execute
    
end sub

sub edit_rca(rc_id, rc, src, comments, module, module_ref, module_sub_ref)
   
    call remove_rca(rc_id, module, module_ref, module_sub_ref)
    call add_rca(rc, src, comments, module, module_ref, module_sub_ref)
        
end sub

sub display_rcas(module, module_ref, module_sub_ref, displayCommands)
    mycount = 0
   uid = module_ref & "_" & module_sub_ref
   objCommand.commandtext = "select rca.*, prc.opt_value as root_cause_text, src.opt_value as secondary_cause_text  from  " & Application("DBTABLE_RCA_Entries") & " rca  " & _
                            " left join  " & Application("DBTABLE_RCA_OPTS") & " prc "   & _
                            " on rca.corp_code  = prc.corp_code " & _
                            " and rca.root_cause = prc.opt_id " & _
                            " left join  " & Application("DBTABLE_RCA_OPTS") & " src "   & _
                            " on rca.corp_code  = src.corp_code " & _
                            " and rca.secondary_cause = src.opt_id " & _
                            " where rca.corp_code = '" & session("corp_code") & "' and rca.isActive = 1 and  rca.module = '" & module & "' and rca.module_ref='" & module_ref & "' and rca.module_sub_ref='" & module_sub_ref & "' "
  'response.write objCommand.commandtext
     set objrsRCAS = objCommand.execute

    if objrsRCAS.eof then
        'display error
        if displayCommands then
            response.write "<div class='alert alert-warning' style='margin-right: 10px'><strong>No Root Causes have been added</strong><br />Use the add RCA button to add the root cause for this issue</div>"
        end if
    else
        response.write "<table class='table table-striped  table-bordered'><thead><tr><th>Root Cause</th>"
        
        if displayCommands then
            response.write "<th width='120px'>Options</th>"
        end if    
        
        response.write"</tr></thead><tbody>"
    
        while not objrsRCAS.eof
            mycount = mycount + 1
            comments =  objrsRCAS("comments") 
            prc =  objrsRCAS("root_cause_text") 
            if objrsRCAS("secondary_cause") <> "0" then
                src =  " > " & objrsRCAS("secondary_cause_text") 
            else
                src =  ""        
            end if
            cause = prc & src
            
            comments_id = uid & "_" & objrsRCAS("id") 

            response.Write "<tr><td "

            if  displayCommands then  response.write  "data-toggle='tooltip' id='comments_" & comments_id & "' title='" & objrsRCAS("comments") & "'"
    
             response.write ">" & cause 

            if not displayCommands and len(objrsRCAS("comments")) > 0 then
                response.write "<br /><span class='info'>Comments: " & objrsRCAS("comments")  & "</span>"
            end if

            response.write "</td>"

               if displayCommands then
                    response.write "<td ><a class='btn btn-xs btn-primary' onclick='return edit_rca_" & uid & "(""" & objrsRCAS("id") & """, """ & objrsRCAS("root_cause") & """, """ & objrsRCAS("secondary_cause") & """)'>Edit</a> <a class='btn btn-xs btn-danger' onclick='return remove_rca_" & uid & "(""" & objrsRCAS("id") & """)'>Remove</a></td>"
                end if   
            response.write "</tr>"

            objrsRCAS.movenext
        wend
        
    
        response.Write "</tbody></table>"
    end if

       
end sub

    
function count_rcas(module, module_ref, module_sub_ref)
   
   objCommand.commandtext = "select count(id) as rca_count  from  " & Application("DBTABLE_RCA_Entries") & " rca  " & _
                            " where rca.corp_code = '" & session("corp_code") & "' and rca.isActive = 1 and  rca.module = '" & module & "' and rca.module_ref='" & module_ref & "' and rca.module_sub_ref='" & module_sub_ref & "' "
  'response.write objCommand.commandtext
     set objrsrca = objCommand.execute

    if objrsrca.eof then
        count_rcas = 0
    else
       count_rcas = objrsrca("rca_count")
    end if

    
    
end function


    
sub fill_rca_search_options(selected_option)

	if isnull(selected_option) = true or seclect_option = "" then
        	selected_option = "0"
       end if

      objCommand.commandtext = "select * from " & Application("DBTABLE_RCA_OPTS") & " " & _
                                       " where corp_code = '" & session("corp_code") & "' and parent_id = 0 "
     set objrs = objCommand.execute

    if not  objrs.eof then
   
         while not objrs.eof
             response.Write "<option style='font-weight: bold' value='" & objrs("opt_id") & "' "

             if len(selected_option) > 0 then 
                if cint(selected_option) =  cint(objrs("opt_id")) then response.write " selected "
             end if

             response.write " >" & objrs("opt_value") & "</option>"

            if objrs("child_present") = "1"  then ' add all the child options
                  objCommand.commandtext = "select * from " & Application("DBTABLE_RCA_OPTS") & " " & _
                                                   " where corp_code = '" & session("corp_code") & "' and parent_id = '" & objrs("opt_id") & "' "
                 set objrs_child = objCommand.execute

                if not  objrs_child.eof then
   
                     while not objrs_child.eof
                         response.Write "<option value='" & objrs_child("opt_id") & "' " '& ":" & objrs("child_present") 
                        if len(selected_option) > 0 then 
                            if cint(selected_option) =  cint(objrs_child("opt_id")) then response.write " selected "
                        end if 
                        response.write " >-- " & objrs_child("opt_value") & "</option>"

                        objrs_child.movenext
                    wend
        
                end if

            end if

            objrs.movenext
        wend
        
    end if
    
end sub
     
     
%>