<%
Dim dictContext
Set dictContext = CreateObject("Scripting.Dictionary")

dictContext.Add "AI", "Accident / Incident"
dictContext.Add "ACT", "Account Centre"
dictContext.Add "AB", "Address Book"
dictContext.Add "AS", "AssessNET.co.uk"
dictContext.Add "BACK", "Back End"
dictContext.Add "CL", "Check List"
dictContext.Add "CP", "Control Panel"
dictContext.Add "COSH", "COSHH"
dictContext.Add "DEV", "Dev Manager"
dictContext.Add "DSE", "DSE"
dictContext.Add "DOC", "Document Upload"
dictContext.Add "EMEM", "eMemo Manager"
dictContext.Add "FS", "Fire Safety"
dictContext.Add "GEN", "General"
dictContext.Add "GRAP", "Graphing"
dictContext.Add "LB", "Log Book"
dictContext.Add "MH", "Manual Handling"
dictContext.Add "MM", "Market Manager"
dictContext.Add "MS", "Method Statement"
dictContext.Add "OB", "Order Book"
dictContext.Add "PD", "Partners"
dictContext.Add "PTW", "Permit To Work"
dictContext.Add "POB", "Purchase Order Book"
dictContext.Add "RA", "Risk Assessment"
dictContext.Add "RM", "Record Manager"
dictContext.Add "SAS", "Safe and Sound"
dictContext.Add "SA", "Safety Audit"
dictContext.Add "SI", "Safety Index"
dictContext.Add "SIC", "Safety Index Control"
dictContext.Add "SP", "Safety Policy"
dictContext.Add "SD", "Scheduler"
dictContext.Add "SRCH", "Search"
dictContext.Add "SUP", "Support"
dictContext.Add "TSK", "Task Manager"

Sub autogenContext(myContext)
    For Each key In dictContext
        Response.Write "<option value='" & key & "'"
        If myContext = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictContext.item(key) & "</option>" & vbCrLf
    Next
End sub

Function getContextForCode(myContext)
    getContextForCode = dictContext.Item(Cstr(myContext))
    if len(getContextForCode) = 0 then
        getContextForCode = myContext
    end if
End function
%>