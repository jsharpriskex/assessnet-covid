<%

sub pr_ss_script_del_session_vars(module_code)
    ' This script deletes existing session vars for this module

    'grab len of module_code
    var_len_module_code = len(module_code)

    'loop through session vars where len of module code = module code and remove
    For Each Item In Session.Contents 
        if left(Item,var_len_module_code) = module_code then Session.Contents.Remove(Item)
    Next 


end sub 


sub pr_ss_script_retrieve_session_vars(reference)
    ' This script retieves the saved session vars for this reference

    'grab all vars and populate sessions
    objcommand.commandtext = "select * from " & Application("DBTABLE_SAVED_SEARCH_DATA") & " where corp_code='" & session("corp_code") & "' and search_reference ='" & reference & "'"
    set objrs = objcommand.execute

    while not objrs.eof
        session(objrs("session_name")) = objrs("session_value")

        objrs.movenext
    wend

    set objrs = nothing

end sub 


sub pr_ss_script_get_default_search(module, module_code)
    ' This script scans the saved searches for the current user/current module and returns them if no other search has been submitted

    
         if UCASE(Session("YOUR_USERNAME")) = "HMARSHALL.RB" then
    'response.write session(module_code)
          ' response.end
        end if


    if session(module_code) <> "Y" then

        objCommand.commandText = "SELECT users.search_reference FROM " & Application("DBTABLE_SAVED_SEARCH_USER") & " AS users " & _
                                 "INNER JOIN " & Application("DBTABLE_SAVED_SEARCH") & " AS searches " & _
                                 "  ON users.corp_code = searches.corp_code " & _
                                 "  AND users.search_reference = searches.search_reference " & _
                                 "WHERE users.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND users.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                 "  AND users.default_search = 1 " & _
                                 "  AND searches.mod_ref = '" & module & "' " & _
                                 "  AND searches.status = 1 " 
        set objRs = objCommand.execute
        if not objRs.EOF then

            call pr_ss_script_retrieve_session_vars(objRs("search_reference"))
            session(module_code) = "Y"

	    ' if ACC = "ACC" then
                session("DEFAULT_SEARCH") = "Y"
            'end if

        end if
        set objRs = nothing

        if module = "ACC" then 
            'session("srch_acc_reset") = "N"
        end if

    end if


end sub


sub pr_ss_script_remove_default(module)
    ' This script removes any previous default searches for the selected module

    objCommand.commandText = "UPDATE users SET users.default_search = 0 " & _
                             "FROM " & Application("DBTABLE_SAVED_SEARCH_USER") & " AS users " & _
                             "INNER JOIN " & Application("DBTABLE_SAVED_SEARCH") & " AS searches " & _
                             "  ON users.corp_code = searches.corp_code " & _
                             "  AND users.search_reference = searches.search_reference " & _
                             "WHERE users.corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND users.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                             "  AND searches.mod_ref = '" & module & "' "
    objCommand.execute

end sub


sub pr_ss_script_make_default(reference)
    ' This script assigns the selected saved search as the default for when the search engine loads for the first time

    objCommand.commandText = "UPDATE " & Application("DBTABLE_SAVED_SEARCH_USER") & " SET default_search = 1 " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                             "  AND search_reference = '" & reference & "' " 
    objCommand.execute


end sub


sub pr_ss_script_save(module, title, description, make_default, session_prefix)
    ' This script saves the search

    'grab unique reference
    
    unique_ref = "N"
    var_search_reference  = ""

    if len(make_default) = 0 then
        make_default = 0
    elseif make_default = "on" then
        make_default = 1
        call pr_ss_script_remove_default(module)
    end if
    
    while unique_ref = "N"
        Randomize()
        var_search_reference = generateRandom(20)
        
       objcommand.commandtext = "select id from  " & Application("DBTABLE_SAVED_SEARCH") & _
                                " where search_reference='" & var_search_reference & "'"
       set objrs = objcommand.execute
       
       if objrs.eof then  unique_ref = "Y"       
    wend
    

    'save the search 
    objcommand.commandtext = "insert into " & Application("DBTABLE_SAVED_SEARCH") & " (corp_code, mod_ref, search_reference, search_title, description) " & _
                             " values ('" & session("corp_code") & "', '" & module & "', '" & var_search_reference & "', '" & title & "', '" & description & "') "
    objcommand.execute
    'save all the variables
    var_len_module_code = len(session_prefix)
    
    For Each Item In Session.Contents 
        if left(Item,var_len_module_code) = session_prefix then
           objcommand.commandtext = "insert into " & Application("DBTABLE_SAVED_SEARCH_DATA") & " (corp_code, search_reference, session_name, session_value) " & _
                             " values ('" & session("corp_code") & "', '" & var_search_reference & "', '" & Item &"', '" & session(Item) & "') "
           objcommand.execute
        end if
    Next 

    'save the user's access
    objcommand.commandtext = "insert into " & Application("DBTABLE_SAVED_SEARCH_USER") & " (corp_code, search_reference, acc_ref, default_search) " & _
                             " values ('" & session("corp_code") & "', '" & var_search_reference & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & make_default & "') "
    objcommand.execute
    
end sub



sub pr_ss_display_retrieve_icon()
    ' This script displays the icon allowing the user to retieve saved searches

     response.write "<td width='100' class='dl_icon l link' ><a id='menu_icon' href='#' onclick=""window.scrollTo(0,0); popup('popUpDivSS_Ret')""><span>Load</span><br />saved searches</a></td>"
    
end sub


sub pr_ss_display_save_icon()
    ' This script displays the icon allowing the user save searches

     response.write "<a href='#' onclick=""window.scrollTo(0,0); popup('popUpDivSS_Sav')"" ><div class='warning' style='float: right; ' ><table style='width: 150px'><tr><td><img src='/version3.2/modules/hsafety/risk_assessment/app/img/icon_v2_downl.png' style='float: left; padding: 3px 10px 3px 3px' /><div style='padding: 4px'>Save<br />this search</div></td></tr></table></div></a>"

end sub

sub pr_ss_display_save_icon_v2()
    ' This script displays the icon allowing the user save searches

     response.write "<a href='#' onclick=""window.scrollTo(0,0); popup('popUpDivSS_Sav')"" ><div  style='display: block; float: left; margin: 10px' class='c'><img src='/version3.2/images/icons/save_gs.png' /><br />Save<br />Search</div></a>"

end sub


      


sub pr_ss_display_retrieve_menu(var_module)
    ' This script displays the light box allowing the user to retieve saved searches

 response.write   "<!--********START OF Saved Search opts******-->" & _
                 "<div id='popUpDivSS_Ret' style='display:none;  width: 500px' class='c'>" & _
                   "<div id='windowtitle'>" & _
                        "<h5>Saved Searches</h5>" & _
                        "<div id='windowmenu'>" & _
                            "<span><a href='#' onclick=""popup('popUpDivSS_Ret')"" >Close</a></span>" & _
                        "</div>" & _        
                        "<div id='windowbody' class='pad'>" & _
                            "<div class='sectionarea c' >" & _
                                "<p class='info l' style='font-size: 12px'>Saved searches allow you to save search criteria that you undertake on a regular basis and recall them at any time. Below is a list of all the searches that you currently have for your account. To save a new search, choose your criteria, run the search then click on the icon that appears at the top of the results.  </p>" 
          
             IF var_module <> "SA_REP" then response.write "<p class='info l' style='font-size: 12px'>Setting a saved search as default will automatically load the search criteria when entering the search engine after logging in</p>" 
                                      
                       objcommand.commandtext = "exec module_global.dbo.sp_return_saved_searches '" & session("corp_code") & "', '" & session("YOUR_ACCOUNT_ID") & "', '"  & var_module & "', 1"               
                       set srch_rs = objcommand.execute               
                    
                        if not srch_rs.eof  then
                            response.write "<div style='max-height: 250px; overflow: auto;'><table width='99%' class='showtd' cellpadding='2' cellspacing='1'><tr><th>Title</th><th>Options</th></tr>" 
                            while not srch_rs.eof  
    
                                var_retrieve = "[<a href='/version3.2/data/scripts/inc/saved_searches_scripts.asp?cmd=ss_retrieve&mod=" & var_module & "&ref=" & srch_rs("search_reference") & "'>Retrieve</a>]"
                                var_delete = "[<a href='/version3.2/data/scripts/inc/saved_searches_scripts.asp?cmd=ss_del&mod=" & var_module & "&ref=" & srch_rs("search_reference") & "' onclick='return confirm(""Delete Save Search. Are you sure?"")' >Delete</a>]"

                                IF var_module <> "SA_REP" then
                                    if srch_rs("default_search") = true then
                                        var_set_default = "[<a href='/version3.2/data/scripts/inc/saved_searches_scripts.asp?cmd=ss_remove_default&mod=" & var_module & "&ref=" & srch_rs("search_reference") & "'>Remove Default</a>]"
                                    else            
                                        var_set_default = "[<a href='/version3.2/data/scripts/inc/saved_searches_scripts.asp?cmd=ss_set_default&mod=" & var_module & "&ref=" & srch_rs("search_reference") & "'>Set as Default</a>]"
                                    end if
                                end if

                                response.write "<tr><td class='l' title='" & fn_capitalise(srch_rs("description")) & "'>" & fn_capitalise(srch_rs("search_title")) & "</td><td width='210px'>" & var_retrieve & "&nbsp;&nbsp;" & var_delete & "&nbsp;&nbsp;" & var_set_default & "</td></tr>" 
                       
                                srch_rs.movenext
                            wend
                             response.write            "<tr><th colspan='2'>&nbsp;</th></tr></table></div>" 
                       else
                        response.write "<p>No saved searches found</p>" 
                       
                       end if
                        response.write           "</div>" & _
                        "</div>" & _
                    "</div>" & _    
                "</div>" & _
                "<!--********END OF Saved Search opts******-->"

end sub


sub pr_ss_display_save_menu(var_module)
    ' This script displays the lightbox allowing the user to save the search criteria

 response.write   "<!--********START OF Saved Search opts******-->" & _
                 "<div id='popUpDivSS_Sav' style='display:none;  width: 500px' class='c'>" & _
                   "<div id='windowtitle'>" & _
                        "<h5>Saved Searches</h5>" & _                       
                        "<div id='windowmenu'>" & _
                            "<span><a href='#' onclick=""popup('popUpDivSS_Sav')"" >Close</a></span>" & _
                        "</div>" & _        
                        "<div id='windowbody' class='pad'>" & _
                            "<div class='sectionarea c'>" & _
                                "<p class='info l'  style='font-size: 12px'>To save your search simply give your search a title and description. You can retrieve your saved searches by clicking on the disk icon at the top of each search engine. </p>" & _
                                "<form action='/version3.2/data/scripts/inc/saved_searches_scripts.asp?cmd=ss_sav&mod=" & var_module & "' method='post' onsubmit=""return chkSavedSearch(this)"">" & _                
                                    "<table width='99%' class='showtd' cellpadding='2' cellspacing='1' style='overflow: auto' >" & _
                                        "<tr><th>Title</th><td><input type='text' class='text' style='width: 97%' name='tb_frm_ss_title' /></td></tr> " & _      
                                        "<tr><th colspan='2'>Description</th></tr>" & _
                                        "<tr><td colspan='2'><textarea class='textarea' name='tb_frm_ss_desc' rows='3'></textarea></td></tr><tr>"
                                        
                                        IF var_module <> "SA_REP" then response.write  "<th>Make Default</th><td class='l'><input type='checkbox' class='checkbox' name='cb_frm_ss_default' id='cb_frm_ss_default' /><label for='cb_frm_ss_default'>Set as default for module start up</label></td></tr>"


                                        response.write  "<tr><td colspan='2' class='noborder'>&nbsp;</td></tr>" & _
                                        "<tr><td colspan='2' style='align: center' class='noborder'><input type='submit' class='submit' name='btn_frm_ss_desc' rows='3' value='Save my search' /></td></tr> " & _                        
                                    "</table>" & _
                                "</form>" & _
                            "</div>" & _
                        "</div>" & _
                    "</div>" & _    
                "</div>" & _
                "<!--********END OF Saved Search opts******-->"

end sub

sub pr_ss_script_del_sav_search(reference)

    objcommand.commandtext = "update " & Application("DBTABLE_SAVED_SEARCH") & " set status = 0  where search_reference ='" & reference & "' and corp_code = '" & session("corp_code") & "'"
    objcommand.execute
end sub







%>