<%

'ON ERROR RESUME NEXT

Dim d
Set d=Server.CreateObject("Scripting.Dictionary")

d.Add "01", "Jan"
d.Add "02", "Feb"
d.Add "03", "Mar"
d.Add "04", "Apr"
d.Add "05", "May"
d.Add "06", "Jun"
d.Add "07", "Jul"
d.Add "08", "Aug"
d.Add "09", "Sep"
d.Add "10", "Oct"
d.Add "11", "Nov"
d.Add "12", "Dec"

DIM strM, strD, strY, strHr, strMin, menuBG, menuSTATE, strBlnk, strFsetting

' strDATE 		Today`s date. Format:  
' strDname  	Value of HTML `name` attribute (day)
' strMname  	Value of HTML `name` attribute (month)
' strYname  	Value of HTML `name` attribute (Year)
' strFsetting	`F` for future years

Sub AutoDATE(strDATE,strDname,strMname,strYname,strFsetting)

strD = 1
strM = 1

if strFsetting = "Fdate" then
	strY = Year(date)
elseif strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
elseif strFsetting = "DOB" then
	strY = 1900
else
	strY = 1990
end if

'used for limiting the calendar control below
strYStartValue = strY

strBlnk = "N"

If strDATE = "NONE" then
	'Setup Day Menu
	Response.write "<select id='" & strDname & "' name='" & strDname & "' class='select'>"
	Response.write "<option value='DD' style='background-color: #FFF000' selected>DD</option>" & vbCrLf
	Do until strD > 31
		if strD < 10 then
			strD = "0" & strD
		else
			strD = strD
		End IF	
		Response.write "<option value='" & strD & "'>" & strD & "</option>" & vbCrLf
		strD = strD + 1
	Loop
	Response.Write "</select>&nbsp;"
	
	'Setup Month Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select'>"
	Response.write "<option value='MM' style='background-color: #FFF000' selected>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
		ELSE
			strM = strM
		End IF
		    IF strFsetting  = "F" Then
		    Response.write "<option value='" & strM & "'>" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'>" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
			strM = strM + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Year Menu
	Response.write "<select id='" & strYname & "' name='" & strYname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate"   or  strFsetting = "DOB" Then
			    Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else

	DAYstrDATE = Day(strDATE)
	MONTHstrDATE = Month(strDATE)

	If Len(Day(strDATE)) < 2 then
	DAYstrDATE = "0" & DAYstrDATE
	End IF

	If Len(Month(strDATE)) < 2 then
		MONTHstrDATE = "0" & MONTHstrDATE
	End IF

		'Setup Day Menu
		Response.write "<select id='" & strDname & "' name='" & strDname & "'  class='select'>"
		Response.write "<option value='DD'>DD</option>" & vbCrLf
			Do until strD > 31
		
				If strD < 10 then
				strD = "0" & strD
					ELSE
					strD = strD
				End IF
		
			IF strD = DAYstrDATE then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
		    Else
				menuBG = ""
				menuSTATE = ""
			End IF			
	
			Response.write "<option value='" & strD & "'" & menuBG & menuSTATE & ">" & strD & "</option>" & vbCrLf
			strD = strD + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Month Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select'>"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
			ELSE
			strM = strM
		End IF
	
			IF strM = MONTHstrDATE then
                menuBG = " style='background-color: #fff000;'"
                menuSTATE = " selected"
            Else
				menuSTATE = ""
				menuBG = ""
			End IF
			
			IF strFsetting  = "F" Then
		    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
		
			strM = strM + 1
		LOOP
	Response.write "</SELECT>&nbsp;"

	'Setup Year Menu
	Response.write "<SELECT id='" & strYname & "' name='" & strYname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		   IF strFsetting = "Fdate" or  strFsetting = "DOB" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
		
End IF

	' ****************************************************
	' ****************************************************
	
	'             For the calendar control
	
	' ****************************************************
	' ****************************************************
	
	Randomize
	uniqueNumber = cint(Rnd*1000)
	functionName = "setMultipleValues3_" & uniqueNumber
	calReference = "cal" & uniqueNumber
	
	Response.write  "<div id='calendarDiv" & uniqueNumber & "' style='position:absolute;visibility:hidden;background-color:#FFFFFF !important;'></div>"
	
	
	Response.write 	"<script language='JavaScript'>var " & calReference & " = new CalendarPopup(""calendarDiv" & uniqueNumber & """); " & _ 
					" " & calReference & ".setReturnFunction(""" & functionName & """); " & _
					" " & calReference & ".showYearNavigation(); " & _
					" " & calReference & ".setCssPrefix(""calControl_""); " & _
					" " & calReference & ".addDisabledDates(null,""31/12/" & strYStartValue-1 & """); " & _
					" " & calReference & ".addDisabledDates(""01/01/" & strY & """,null); " & _
					" /*fill in the boxes on the screen*/ " & _
					"function " & functionName & "(y,m,d) { " & _
					"    document.getElementById(""" & strYname & """).value=y; " & _
					"    document.getElementById(""" & strMname & """).selectedIndex=m; " & _
					"	 document.getElementById(""" & strDname & """).selectedIndex=d; } </script>"

	Response.Write "<a href='#' class='calIcon' onclick='" & calReference & ".showCalendar(""anchor" & uniqueNumber & """,getDateString(document.getElementById(""" & strYname & """),document.getElementById(""" & strMname & """),document.getElementById(""" & strDname & """))); return false;' title='Display a calendar' NAME='anchor" & uniqueNumber & "' ID='anchor" & uniqueNumber & "'>&nbsp;</a>"
	
	' ****************************************************
	' ****************************************************
	
	'               end of calendar contol
	
	' ****************************************************
	' ****************************************************

End Sub


Sub AutoTIME(strTIME,strHname,strMname)

    strHr = 0
    strMin = 0

IF UCase(strTIME) = "NONE" Then

	'Setup Hours Menu
	Response.write "<SELECT name='" & strHname & "'  class='select'>"
	Response.write "<option value='HH' SELECTED>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select name='" & strMname & "'  class='select'>"
	Response.write "<option value='MM' SELECTED>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>"

else

    if instr(strTIME, "/") > 0 then
		'time was set to 00:00 on a previous screen
		strTIME = "00:00"
		HOURstrTIME = 00
		MINstrTIME = 00
	else
		HOURstrTIME = Hour(strTIME)
		MINstrTIME = Minute(strTIME)
	end if
    	
    If Len(Hour(strTIME)) < 2 then
        HOURstrTIME = "0" & HOURstrTIME
    End IF
    If Len(Minute(strTIME)) < 2 then
        MINstrTIME = "0" & MINstrTIME
    End if

	'Setup Hours Menu
	Response.write "<SELECT name='" & strHname & "' class='select'>"
	Response.write "<option value='HH'>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select name='" & strMname & "' class='select'>"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>"

END IF
	
End Sub




Sub AutoYEAR(strDATE,strYname,strFsetting)

strD = 1
strM = 1

if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
else
	strY = 1990
end if

strBlnk = "N"

If strDATE = "NONE" then
	
	
	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else



	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

End Sub




Sub AutoYEARExtended(strDATE,strYname,strFsetting, displayMonth)

strD = 1
if isnumeric(month(strDATE)) then
	strM = month(strDATE)
	if len(strM) = 1 then strM = "0" + cstr(strM)
elseif isnumeric(displayMonth) then
	strM = displayMonth
	if len(displayMonth) = 1 then strM = "0" + cstr(strM)
else 
	strM = "01"
end if


if strFsetting = "F" then
	wholeDate = "01/" & strM  & "/" & Year(date)
elseif strFsetting = "M" then
	wholeDate = "01/" & strM  & "/2000"
else
	wholeDate = "01/" & strM  & "/2000"
end if

strBlnk = "N"

If strDATE = "NONE" then
	
	
	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>Choose a year</option>" & vbCrLf

	objcommand.commandtext = "select * from module_global.[dbo].[YearsinRangeExtended] ('" & wholeDate & "', '" & DateAdd("yyyy", 3, date) & "', '" & displayMonth & "') order by startdate"
	set dateRS = objcommand.execute

	if dateRS.eof then 
		Response.write "<option value='XX'>There was an issue with generating the dates</option>" & vbCrLf
	else
		while not dateRS.eof
			Response.write "<option value='" & dateRS("startdate") & "'>" & dateRS("yearname") & "</option>" & vbCrLf
			dateRS.movenext()
		wend
	end if
	Response.write "</select>"
	
else



	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>Choose a year</option>" & vbCrLf
	
	objcommand.commandtext = "select * from module_global.[dbo].[YearsinRangeExtended] ('" & wholeDate & "', '" & DateAdd("yyyy", 3, date) & "', '" & displayMonth & "') order by startdate"
	set dateRS = objcommand.execute

	if dateRS.eof then 
		Response.write "<option value='XX'>There was an issue with generating the dates</option>" & vbCrLf
	else
		while not dateRS.eof

			
			IF left(strDATE, 10) = left(dateRS("startdate"), 10)  then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF

			Response.write "<option value='" & dateRS("startdate") & "' " & menuBG & menuSTATE & ">" & dateRS("yearname") & "</option>" & vbCrLf
			dateRS.movenext()
		wend
	end if
	Response.write "</select>"
End IF

End Sub




Sub AutoQuarterExtended(strDATE,strQname,strFsetting, displayMonth)

strD = 1
if isnumeric(month(strDATE)) then
	strM = month(strDATE)
	if len(strM) = 1 then strM = "0" + cstr(strM)
elseif isnumeric(displayMonth) then
	strM = displayMonth
	if len(displayMonth) = 1 then strM = "0" + cstr(strM)
else 
	strM = "01"
end if


if strFsetting = "F" then
	wholeDate = "01/" & strM  & "/" & Year(date)
elseif strFsetting = "M" then
	wholeDate = "01/" & strM  & "/2000"
else
	wholeDate = "01/" & strM  & "/2000"
end if

strBlnk = "N"

If strDATE = "NONE" then
	
	
	'Setup Year Menu
	Response.write "<select name='" & strQname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>Choose a quarter</option>" & vbCrLf

	objcommand.commandtext = "select * from module_global.[dbo].[QuartersinRangeExtended] ('" & wholeDate & "', '" & DateAdd("yyyy", 3, date) & "', '" & displayMonth & "') order by startdate "
	set dateRS = objcommand.execute

	if dateRS.eof then 
		Response.write "<option value='XX'>There was an issue with generating the dates</option>" & vbCrLf
	else
		while not dateRS.eof
			Response.write "<option value='" & dateRS("startdate") & "'>" & dateRS("quartername") & "</option>" & vbCrLf
			dateRS.movenext()
		wend
	end if
	Response.write "</select>"
	
else



	'Setup Year Menu
	Response.write "<SELECT name='" & strQname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>Choose a quarter</option>" & vbCrLf
	
	objcommand.commandtext = "select * from module_global.[dbo].[QuartersinRangeExtended] ('" & wholeDate & "', '" & DateAdd("yyyy", 3, date) & "', '" & displayMonth & "') order by startdate"
	set dateRS = objcommand.execute

	if dateRS.eof then 
		Response.write "<option value='XX'>There was an issue with generating the dates</option>" & vbCrLf
	else
		while not dateRS.eof

			
			IF left(strDATE, 10) = left(dateRS("startdate"), 10)  then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF

			Response.write "<option value='" & dateRS("startdate") & "' " & menuBG & menuSTATE & ">" & dateRS("quartername") & "</option>" & vbCrLf
			dateRS.movenext()
		wend
	end if
	Response.write "</select>"
End IF

End Sub


Sub AutoMONTHYEAR(strDATE,strMname,strYname,strFsetting)

strD = 1
strM = 1

if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
else
	strY = 1990
end if

strBlnk = "N"

If strDATE = "NONE" then
		
	'Setup Month Menu
	Response.write "<select name='" & strMname & "' class='select'>"
	Response.write "<option value='MM' style='background-color: #FFF000' selected>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
		ELSE
			strM = strM
		End IF
		    IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'>" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'>" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
			strM = strM + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else

	MONTHstrDATE = Month(strDATE)

	If Len(Month(strDATE)) < 2 then
		MONTHstrDATE = "0" & MONTHstrDATE
	End IF

	
	'Setup Month Menu
	Response.write "<select name='" & strMname & "' class='select'>"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
			ELSE
			strM = strM
		End IF
	
			IF strM = MONTHstrDATE then
                menuBG = " style='background-color: #fff000;'"
                menuSTATE = " selected"
            Else
				menuSTATE = ""
				menuBG = ""
			End IF
			
			IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
		
			strM = strM + 1
		LOOP
	Response.write "</SELECT>&nbsp;"

	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

End Sub

Sub AutoQuartYEAR(strQDATE,strYDATE,strQname,strYname,strFsetting, switch1, switch2, switch3)

Dim d
Set d=Server.CreateObject("Scripting.Dictionary")
d.Add "1","Q1 Jan - Mar"
d.Add "2","Q2 Apr - Jun"
d.Add "3","Q3 Jul - Sep"
d.Add "4","Q4 Oct - Dec"

strQ = 1


if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
else
	strY = 1990
end if

'strBlnk = "N"

If strQDATE = "0" or strYDATE = "YYYY" then
		
	'Setup Quarter Menu
	Response.write "<select name='" & strQname & "' class='select' >"
	Response.write "<option value='0' style='background-color: #FFF000' selected>Quarter</option>" & vbCrLf
		Do until strQ > 4		
		    
			myval = d.Item(cstr(strQ))
			Response.write "<option value='" & strQ & "'>" & myval & "</option>" & vbCrLf
		    
			strQ = strQ + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select'>"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else
	
	'Setup Quarter Menu
	Response.write "<select name='" & strQname & "' class='select'>"
	Response.write "<option value='0'>Quarter</option>" & vbCrLf
		Do until strQ > 4
			IF cstr(strQ) = strQDATE then
                menuBG = " style='background-color: #fff000;'"
                menuSTATE = " selected"
            Else
				menuSTATE = ""
				menuBG = ""
			End IF

			myval = d.Item(cstr(strQ))
			
			IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strQ & "'" & menuBG & menuSTATE & ">" & myval & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strQ & "'" & menuBG & menuSTATE & ">" & myval & "</option>" & vbCrLf
		    End if
		
			strQ = strQ + 1
		LOOP
	Response.write "</SELECT>&nbsp;"
	
	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select'>"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
			IF cstr(strY) = cstr(strYDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
				Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

End Sub

%>