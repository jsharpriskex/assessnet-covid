﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports Winnovative.WnvHtmlConvert
Imports System.IO

Partial Class inspection_test
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        '  Session.LCID = 2057

        Dim pdfConverter As PdfConverter = New PdfConverter()
        ' set the license key
        pdfConverter.LicenseKey = Application("PDF_LICENCE")
        Dim selectablePDF As Boolean = True
        Dim baseURL As String = Application("PDF_BASE_URL")

        ' set the converter options
        pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4
        pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.NoCompression
        If Request("print_orient") = "L" Then
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Landscape
        Else
            pdfConverter.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait
        End If
        pdfConverter.PdfDocumentOptions.ShowHeader = False
        pdfConverter.PdfDocumentOptions.ShowFooter = False
        ' set to generate selectable pdf or a pdf with embedded image
        pdfConverter.PdfDocumentOptions.GenerateSelectablePdf = selectablePDF
        ' set the embedded fonts option - optional, by default is false
        pdfConverter.PdfDocumentOptions.EmbedFonts = False
        ' enable the live HTTP links option - optional, by default is true
        pdfConverter.PdfDocumentOptions.LiveUrlsEnabled = True
        ' enable the support for right to left languages , by default false
        pdfConverter.RightToLeftEnabled = False
        ' set PDF security options - optional
        'pdfConverter.PdfSecurityOptions.CanPrint = True
        pdfConverter.PdfSecurityOptions.CanEditContent = False
        'pdfConverter.PdfSecurityOptions.UserPassword = ""
        ' set PDF document description - optional
        pdfConverter.PdfDocumentInfo.AuthorName = "Riskex Ltd"

        pdfConverter.PdfFooterOptions.FooterTextFontName = "Arial"
        pdfConverter.PdfFooterOptions.FooterText = "Copyright Riskex Ltd 2001 - " & DateTime.Now.ToString("yyyy") & " / AssessNET.co.uk Online Health and Safety Management"
        pdfConverter.PdfFooterOptions.FooterHeight = 30

        pdfConverter.PdfFooterOptions.DrawFooterLine = False
        pdfConverter.PdfFooterOptions.PageNumberText = "Page"
        pdfConverter.PdfFooterOptions.PageNumberTextFontType = PdfFontType.HelveticaBold
        pdfConverter.PdfFooterOptions.PageNumberTextFontSize = 8
        pdfConverter.PdfFooterOptions.ShowPageNumber = True
        pdfConverter.PdfDocumentOptions.ShowFooter = True

        pdfConverter.PdfDocumentOptions.LeftMargin = 10
        pdfConverter.PdfDocumentOptions.RightMargin = 10
        pdfConverter.PdfDocumentOptions.TopMargin = 10
        pdfConverter.PdfDocumentOptions.BottomMargin = 10



        Dim dbConn As SqlConnection

        dbConn = New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn.Open()

        Dim dbConn2 As SqlConnection

        dbConn2 = New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn2.Open()

        Dim dbConn3 As SqlConnection

        dbConn3 = New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn3.Open()

        Dim dbConn4 As SqlConnection

        dbConn4 = New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn4.Open()

        Dim var_corp_code = Request.QueryString("ccode")

        Dim var_logo_str
        If Len(Request.QueryString("dcode")) > 0 Then
            Dim f As New IO.FileInfo(Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & Request.QueryString("dcode") & ".jpg"))
            If f.Exists = True Then
                var_logo_str = "<img  src='data/documents/pdf_centre/img/corp_logos/" & Request.QueryString("dcode") & ".jpg'  />"
            Else
                var_logo_str = "<img  src='data/documents/pdf_centre/img/corp_logos/default.jpg'  />"
            End If
        Else
            Dim f As New IO.FileInfo(Server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & var_corp_code & ".jpg"))
            If f.Exists = True Then
                var_logo_str = "<img  src='data/documents/pdf_centre/img/corp_logos/" & var_corp_code & ".jpg'  />"
            Else
                var_logo_str = "<img  src='data/documents/pdf_centre/img/corp_logos/default.jpg'  />"
            End If
        End If


        Dim var_insp_frm_reference = Request.QueryString("ref")
        Dim myPDFSTR As String
        Dim PDF
        Dim sql_text
        Dim var_reference

        Dim var_temp_title
        Dim var_temp_desc

        ' Convert
        var_reference = var_insp_frm_reference


        'check if the inspection's linked to a template
        sql_text = "select a.* from " & Application("DBTABLE_INS_TEMP_INFO") & " as a where a.template_ref = '" & var_reference & "'"

        Dim sqlCommTP As New SqlCommand(sql_text, dbConn)
        Dim objrsTP As SqlDataReader = sqlCommTP.ExecuteReader()


        Dim var_temp_ref

        If objrsTP.HasRows Then
            objrsTP.Read()
            var_temp_ref = objrsTP("template_ref")
            var_temp_title = objrsTP("template_name")
            var_temp_desc = objrsTP("template_desc")
        Else '  no template
            var_temp_ref = 0
        End If


        objrsTP.Close()



        Dim var_default_icon_value As String = ""
        Dim var_default_icon_text As String = ""
        Dim var_default_icon_folder As String = ""

        Dim var_corp_tier2 = "Company"
        Dim var_corp_tier3 = "Location"
        Dim var_corp_tier4 = "Department"
        Dim var_corp_tier5 = ""
        Dim var_corp_tier6 = ""



        sql_text = "SELECT * FROM " & Application("DBTABLE_HR_PREFERENCE") & " " & _
                   "WHERE corp_code='" & var_corp_code & "' "
        Dim sqlComm2 As New SqlCommand(sql_text, dbConn)
        Dim objrs2 As SqlDataReader = sqlComm2.ExecuteReader()
        If objrs2.HasRows Then

            While objrs2.Read()
                Select Case objrs2("pref_id")
                    Case "corp_tier2"
                        var_corp_tier2 = objrs2("pref_switch")
                    Case "corp_tier3"
                        var_corp_tier3 = objrs2("pref_switch")
                    Case "corp_tier4"
                        var_corp_tier4 = objrs2("pref_switch")
                    Case "corp_tier5"
                        var_corp_tier5 = objrs2("pref_switch")
                    Case "corp_tier6"
                        var_corp_tier6 = objrs2("pref_switch")
                End Select
            End While

            If IsDBNull(var_corp_tier2) = True Then
                var_corp_tier2 = "Company"
            End If
            If IsDBNull(var_corp_tier3) = True Then
                var_corp_tier3 = "Location"
            End If
            If IsDBNull(var_corp_tier4) = True Then
                var_corp_tier4 = "Department"
            End If
        Else
            var_corp_tier2 = "Company"
            var_corp_tier3 = "Location"
            var_corp_tier4 = "Department"
        End If
        objrs2.Close()



        Dim dtNow As DateTime = DateTime.Now

        Dim i

        Dim datenow As String = dtNow.ToString



        '*******************************************
        '       General Assessment Details
        '*******************************************

        myPDFSTR = myPDFSTR & "<div id='section_1' class='sectionarea' style='page-break-inside: avoid;'>"
        myPDFSTR = myPDFSTR & "<h3>Inspection Template</h3>"

        myPDFSTR = myPDFSTR & "<table width='45%' cellpadding='1px' cellspacing='2px' class='showtd' style='float: left;'>"
        myPDFSTR = myPDFSTR & "<tr><th nowrap width='220'>Status</th><td nowrap>Draft&nbsp;</td>"
        myPDFSTR = myPDFSTR & "</tr><tr><th nowrap width='220'>Inspected by</th><td >&nbsp;</td></tr>"
        myPDFSTR = myPDFSTR & "<tr><th nowrap width='220'>Inspection date</th><td nowrap >&nbsp;</td></tr>"

        myPDFSTR = myPDFSTR & "</table>"

        myPDFSTR = myPDFSTR & "<table width='45%' cellpadding='1px' cellspacing='2px' class='showtd' style='float: right;'>"

        If Not IsDBNull(var_corp_tier2) And len(var_corp_tier2) > 0 Then
            myPDFSTR = myPDFSTR & "<tr><th width='170'>" & var_corp_tier2 & "</th><td>&nbsp;</td></tr>"
            If Not IsDBNull(var_corp_tier3) And len(var_corp_tier3) > 0 Then
                myPDFSTR = myPDFSTR & "<tr><th width='170'>" & var_corp_tier3 & "</th><td>&nbsp;</td></tr>"
                If Not IsDBNull(var_corp_tier4) And len(var_corp_tier4) > 0 Then
                    myPDFSTR = myPDFSTR & "<tr><th width='170'>" & var_corp_tier4 & "</th><td>&nbsp;</td></tr>"
                    If Not IsDBNull(var_corp_tier5) And len(var_corp_tier5) > 0 Then
                        myPDFSTR = myPDFSTR & "<tr><th width='170'>" & var_corp_tier5 & "</th><td>&nbsp;</td></tr>"
                        If Not IsDBNull(var_corp_tier6) And len(var_corp_tier6) > 0 Then
                            myPDFSTR = myPDFSTR & "<tr><th width='170'>" & var_corp_tier6 & "</th><td>&nbsp;</td></tr>"
                        End If
                    End If
                End If
            End If
        End If

        myPDFSTR = myPDFSTR & "</table>"

        myPDFSTR = myPDFSTR & "<div class='clear'></div>"
        myPDFSTR = myPDFSTR & "<table width = '99%' class='showtd'>"
        myPDFSTR = myPDFSTR & "<tr><td colspan='5' class='noborder'>&nbsp;</td></tr>"
        myPDFSTR = myPDFSTR & "<tr><th nowrap width='220'>Inspection name</th><td nowrap colspan='4'>&nbsp;</td></tr>"
        myPDFSTR = myPDFSTR & "<tr><td colspan='5'  class='noborder'>&nbsp;</td></tr>"
        myPDFSTR = myPDFSTR & "<tr><th colspan='5' >Description</th></tr>"
        myPDFSTR = myPDFSTR & "<tr><td colspan='5' >" & var_temp_desc & "<br /><br />&nbsp;</td></tr>"
        myPDFSTR = myPDFSTR & "</table>"
        myPDFSTR = myPDFSTR & "</div>"



        '*******************************************
        '           Areas
        '*******************************************  

        myPDFSTR = myPDFSTR & "<div style='clear'>&nbsp;</div><div id='section_2' class='sectionarea'>"

        sql_text = "Select count(*) as cntarea From " & Application("DBTABLE_INS_TEMP_SECT") & " Where template_ref='" & var_reference & "'"

        Dim sqlComm5 As New SqlCommand(sql_text, dbConn)
        Dim objrs5 As SqlDataReader = sqlComm5.ExecuteReader()
        Dim var_objARScount As String

        If objrs5.HasRows() Then
            objrs5.Read()
            var_objARScount = objrs5("cntarea")
        Else
            var_objARScount = "0"
        End If

        objrs5.Close()

        sql_text = "Select section_title From " & Application("DBTABLE_INS_TEMP_SECT") & " Where template_ref='" & var_reference & "' order by position"
        Dim sqlComm6 As New SqlCommand(sql_text, dbConn)
        Dim objARS As SqlDataReader = sqlComm6.ExecuteReader()
        Dim var_cnt As Integer

        If objARS.HasRows() Then

            'At least one area 
            var_cnt = 1

            While objARS.read()

                myPDFSTR = myPDFSTR & "<div id='section_3' class='sectionarea' style='page-break-inside: avoid;'>"
                myPDFSTR = myPDFSTR & "<h3>Area " & var_cnt & " of " & var_objARScount & ": " & objARS("section_title") & "</h3> "


                myPDFSTR = myPDFSTR & "<div class='outer_blue_area' style='page-break-inside: avoid;'><div class='inner_blue_area'>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='showtd' >" & _
                                  "<tr>" & _
                                      "<th>Observation</th></tr>" & _
                                  "<tr><td ><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                         "<tr><th>Requirements</th></tr>" & _
                                         "<tr><td ><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                   "<tr><th>Actioned to</th><th width='150px'>Due Date</th></tr>" & _
                                   "<tr><td>&nbsp;</td><td>&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "</div></div><br />"

                myPDFSTR = myPDFSTR & "<div class='outer_blue_area' style='page-break-inside: avoid;'><div class='inner_blue_area'>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='showtd' >" & _
                                  "<tr>" & _
                                      "<th>Observation</th></tr>" & _
                                  "<tr><td ><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                         "<tr><th>Requirements</th></tr>" & _
                                         "<tr><td><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                   "<tr><th>Actioned to</th><th width='150px'>Due Date</th></tr>" & _
                                   "<tr><td>&nbsp;</td><td>&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "</div></div><br />"

                myPDFSTR = myPDFSTR & "<div class='outer_blue_area' style='page-break-inside: avoid;'><div class='inner_blue_area'>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='showtd' >" & _
                                  "<tr>" & _
                                      "<th>Observation</th></tr>" & _
                                  "<tr><td ><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                         "<tr><th>Requirements</th></tr>" & _
                                         "<tr><td ><br /><br />&nbsp;</td></tr></table>"
                myPDFSTR = myPDFSTR & "<table width='100%' cellpadding='1px' cellspacing='2px' class='highlight showtd'>" & _
                                   "<tr><th>Actioned to</th><th width='150px'>Due Date</th></tr>" & _
                                   "<tr><td>&nbsp;</td><td>&nbsp;</td></tr></table>"

                myPDFSTR = myPDFSTR & "</div></div><br /></div>"

                var_cnt = var_cnt + 1
            End While


        End If

        myPDFSTR = myPDFSTR & "</div>"

        objARS.Close()



        '*******************************************
        '           Additional Comments
        '*******************************************

        myPDFSTR = myPDFSTR & "<div id='section_3' class='sectionarea' style='page-break-inside: avoid;'><h3>Additional Comments</h3>"






        '*******************************************
        '           End of the PDF
        '*******************************************


        myPDFSTR = myPDFSTR & "</body></html>"

        myPDFSTR = "<html><head><link rel=""stylesheet"" type=""text/css"" href='data/documents/pdf_centre/default_pdf.css' /><title>AssessNET PDF Report</title></head><body><div id='section_0' class='optionsection' align='right' style='width: 99%;'>" & _
                    "<table width='99%' ><tr><td width='280px' height='60px'>" & var_logo_str & "</td><td class='c title'><strong>Inspection Template<br />" & var_temp_title & "</strong><br /><span class='info'>Information valid as of " & datenow & "</span></td><td></td><td  class='r' width='290px'><table width='1%'><tr><td class='c'>&nbsp;<td></tr></table><td></tr></table></div><div id='windowbody' class='pad'>" & myPDFSTR




        ' Performs the conversion and get the pdf document bytes that you can further 
        ' save to a file or send as a browser response
        '
        ' The baseURL parameter helps the converter to get the CSS files and images
        ' referenced by a relative URL in the HTML string. This option has efect only if the HTML string
        ' contains a valid HEAD tag. The converter will automatically inserts a <BASE HREF="baseURL"> tag. 
        Dim pdfBytes As Byte() = Nothing
        If (baseURL.Length > 0) Then '
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(myPDFSTR, baseURL)
        Else
            pdfBytes = pdfConverter.GetPdfBytesFromHtmlString(myPDFSTR)
        End If

        ' send the PDF document as a response to the browser for download
        Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response


        response.Write(myPDFSTR)

        response.Cache.SetNoStore()

        response.Clear()
        response.ContentType = "application/pdf"
        response.AddHeader("Content-Length", pdfBytes.Length.ToString())
        response.AddHeader("Content-Disposition", "attachment; filename=AssessNET_" & var_reference & ".pdf;")
        response.Flush()
        response.BinaryWrite(pdfBytes)

        response.Write(myPDFSTR)



        dbConn.Close()
        dbConn2.Close()
        dbConn3.Close()
    End Sub
End Class
