<!-- #INCLUDE FILE="dbconnections.inc" -->
<!-- #INCLUDE FILE="autogen_counties.asp" -->
<!-- #INCLUDE FILE="../../functions/func_random.asp" -->

<%

Dim objRs
Dim Objconn
Dim Objcommand

Call StartCONNECTIONS()


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                             ''
'' Function ipCheck                                            ''
'' This function requires,                                     ''
''        varIP = Ip address for R-Only    	                   ''
''        varAccess = Access Level of user                     ''
''                                                             ''
''    Function ammended 23/11/2011 (MGreen)                    ''
''    Function now checks against an ip range rather than      ''
''    a specific ip address.                                   ''
''    In addition, the second query requirement varAccess      ''
''    was added so that the ip restriction can be applied      ''
''    to more than just R-Only users.                          ''
''                                                             ''
''    Moved from LoginCheckv3.asp on 23/11/2011                ''
''                                                             ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function ipCheck(varIP, varAccess)

    dim arry_ip_block

    ' is there an ip address/range in the hosts table? if not, access hasn't been restricted so bypass the checking process
    objCommand.commandText = "SELECT count(id) AS num_hosts FROM " & Application("DBTABLE_HR_HOSTS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND (ACC_LEVEL_AFFECTED LIKE '%" & varAccess & "%' OR ACC_LEVEL_AFFECTED LIKE '%all%')"
    set objRs = objCommand.execute
    if objRs("num_hosts") > 0 then

        arry_ip_blocks = split(varIP,".")

        ' check the users ip against the database as a range has been found
        objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_HOSTS") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND (" & arry_ip_blocks(0) & " BETWEEN IPHOST_RANGE_START_BLOCK1 AND IPHOST_RANGE_END_BLOCK1) " & _
                                 "  AND (" & arry_ip_blocks(1) & " BETWEEN IPHOST_RANGE_START_BLOCK2 AND IPHOST_RANGE_END_BLOCK2) " & _
                                 "  AND (" & arry_ip_blocks(2) & " BETWEEN IPHOST_RANGE_START_BLOCK3 AND IPHOST_RANGE_END_BLOCK3) " & _
                                 "  AND (" & arry_ip_blocks(3) & " BETWEEN IPHOST_RANGE_START_BLOCK4 AND IPHOST_RANGE_END_BLOCK4) " & _
                                 "  AND (ACC_LEVEL_AFFECTED LIKE '%" & varAccess & "%' OR ACC_LEVEL_AFFECTED LIKE '%all%')"
        set objRsCheck = objCommand.execute
        if objRsCheck.EOF then
            ipCheck = "FALSE"
        else
            ipCheck = "TRUE"
        end if
        set objRsCheck = nothing
    else
        ipCheck = "TRUE"
    end if
    set objRs = nothing


    'Objcommand.Commandtext = "SELECT IPHOST From " & Application("DBTABLE_HR_HOSTS") & " WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND IPHOST = '" & varIP & "' "
    'SET objRs = Objcommand.execute
    '
    'If objRs.EOF then
    '    ipCheck = "FALSE"
    'Else
    '    ipCheck = "TRUE"
    'End if

End Function



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' SUB (userLOG) instructions of use                      ''
'' This sub requires,                                     ''
''                    status (1 for success, 2 for fail)  ''
''                    details (Reason for log entry)      ''
''					  username (Username entered at login ''
''                                                        ''
''    Moved from LoginCheckv3.asp on 23/11/2011           ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
SUB useLOG(varSTATUSlog,varDETAILSlog,varUNAMElog,varfrmRESlog)

	varDATElog = Date() & " " & Time()
	varHTTPlog = Request.ServerVariables("HTTPS")
	varIPlog = Request.ServerVariables("REMOTE_ADDR")
    var_screen_res = varfrmRESlog
    var_browser_type = Request.ServerVariables("HTTP_USER_AGENT")
    var_referer = validateInput(request("referer"))

	Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_LOG_ACCESS") & " " & _
								"(datetime,https,success,reason,username,ip_address, screen_res, browser, http_referer) " & _
								"VALUES('" & varDATElog & "','" & varHTTPlog & "','" & varSTATUSlog & "','" & varDETAILSlog & "','" & varUNAMElog & "','" & varIPlog & "', '" & var_screen_res & "', '" & var_browser_type & "', '" & var_referer & "') "
	Objcommand.execute

END SUB



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Sub (checkUSER) instructions of use                    ''
'' This function requires,                                ''
''                        varfrmUSER = Username form input''
''                        varfrmPASS = Password from input''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub checkUSER(varfrmUSER,varfrmPASS,varfrmRES, varLoginHASH)

    ' get the details passed by the user - from the other app server if being passed over
    if len(varLoginHASH) > 0 and isnull(varLoginHASH) = false then
        objCommand.commandText = "SELECT acc_name, acc_pass FROM " & Application("DBTABLE_USER_DATA") & " WHERE temp_guid = '" & varLoginHASH & "' "
        set objRs = objCommand.execute
        if not objRs.EOF then
            varfrmUSER = objRs("acc_name")
            varfrmPASS = objRs("acc_pass")

            objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " WHERE guid='" & varLoginHASH & "'"
            set objRsDirect = objCommand.execute
            if not objRsDirect.EOF then

                while not objRsDirect.EOF

                    session(objRsDirect("session_name")) = replace(objRsDirect("session_value"),"`","'")

                    objRsDirect.movenext
                wend
            end if
            set objRsDirect = nothing
        else
            'fail
            varfrmUSER = ""
            varfrmPASS = ""
        end if
        set objRs = nothing

        objCommand.commandText = "DELETE FROM " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " WHERE guid = '" & varLoginHASH & "' "
        objCommand.execute

        objCommand.commandText = "UPDATE " & Application("DBTABLE_USER_DATA") & " SET temp_guid = NULL WHERE temp_guid = '" & varLoginHASH & "' "
        objCommand.execute
    else
        varfrmUSER = trim(varfrmUSER)
        varfrmPASS = trim(varfrmPASS)
    end if



    'if ucase(right(varfrmUSER,4)) = ".BCC" then
    ' Call errCode("0010")
    'end if
', tier1.struct_name as tier1_name, tier2.struct_name as tier2_name, tier3.struct_name as tier3_name, tier4.struct_name as tier4_name
'  "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER1")& "  AS tier1 " & _
                            ' "on hr.corp_code = tier1.corp_code " & _
                             '"and hr.corp_bg = tier1.tier1_ref " & _
                             '"LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER2")& "  AS tier2 " & _
                             '"on hr.corp_code = tier2.corp_code " & _
                             '"and hr.corp_bu = tier2.tier2_ref " & _
                             '"LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3")& "  AS tier3 " & _
                             '"on hr.corp_code = tier3.corp_code " & _
                             '"and hr.corp_bl = tier3.tier3_ref " & _
                             '"LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4")& "  AS tier4 " & _
                             '"on hr.corp_code = tier4.corp_code " & _
                             '"and hr.corp_bd = tier4.tier4_ref " & _
    Objcommand.commandtext = "SELECT hr.*, tier1.struct_name as tier1_name, tier2.struct_name as tier2_name, tier3.struct_name as tier3_name, tier4.struct_name as tier4_name, platforms.domain, platforms.platform   FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _
                            "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER1")& "  AS tier1 " & _
                             "on hr.corp_code = tier1.corp_code " & _
                             "and hr.corp_bg = tier1.tier1_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER2")& "  AS tier2 " & _
                             "on hr.corp_code = tier2.corp_code " & _
                             "and hr.corp_bu = tier2.tier2_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3")& "  AS tier3 " & _
                             "on hr.corp_code = tier3.corp_code " & _
                             "and hr.corp_bl = tier3.tier3_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4")& "  AS tier4 " & _
                             "on hr.corp_code = tier4.corp_code " & _
                             "and hr.corp_bd = tier4.tier4_ref " & _
                             "LEFT OUTER JOIN " & "Module_HR.dbo.HR_Systems" & " AS systems " & _
                             "on hr.corp_code = systems.corp_code " & _
                             "LEFT OUTER JOIN " & Application("DBTABLE_HR_PLATFORMS") & " AS platforms " & _
                             "on systems.platform = platforms.platform " & _
                             "WHERE (LOWER(hr.acc_name) = LOWER('" & varfrmUSER & "')) AND (hr.acc_pass = '" & varfrmPASS & "') AND (hr.acc_level < '5')"


                            ' "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER5")& "  AS tier5 " & _
                            ' "on hr.corp_code = tier5.corp_code " & _
                            ' "and hr.tier5_ref = tier5.tier5_ref " & _
                            ' "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER6")& "  AS tier6 " & _
                            ' "on hr.corp_code = tier6.corp_code " & _
                            ' "and hr.tier6_ref = tier6.tier6_ref " & _
    Set objRs = Objcommand.execute

    If objRs.EOF Then
        '' User details invalid make log entry
        Call useLOG("2","Username or Password invalid",varfrmUSER,varfrmRES)
        Call errCode("0001")
    Else




        if instr(1,ucase(varfrmUSER),"SYSADMIN") = 0 and instr(1,ucase(varfrmUSER),".SAS") = 0 then
            objCommand.commandText = "SELECT order_sent FROM " & Application("DBTABLE_HR_SYSTEMS") & " " & _
                                     "WHERE corp_code = '" & objRs("corp_code") & "' "
            set objRsSys = objCommand.execute
            if not objRsSys.EOF then
                if ucase(objRsSys("order_sent")) = "C" then
                    Call useLOG("2","Account Contract Disabled",varfrmUSER,varfrmRES)
                    Call errCode("0006")
                end if
            end if
        end if


    if (objRs("acc_ref") = "cm15omxzypq3i70" or objRs("acc_ref") = "OIKLeT50jgzHxW0") and objRs("corp_code") = "200200" then
        call errCode("0004")
    end if


 '  now handled by .NET - no longer required
 '   ' Moving here ri attempt to fix an issue caused by switching to app4/app5
 '       ' Create cookie
 '       IF request.Form("writeCookie") = "Y" Then
 '           Response.Cookies("SavedLogin")("USER") = varfrmUSER
 '           Response.Cookies("SavedLogin").Expires = Date + 30
 '           else
 '           ' kill it if they have took the remember me off
 '           If Request.Cookies("SavedLogin").HasKeys then
 '           Response.Cookies("SavedLogin").Expires = Date - 1
 '           end if
 '       END IF




        ' redirect to the preferred server for the contract
'if objRs("acc_ref") = "ezkqttcQpBfHmhR" or objRs("acc_ref") = "vxogSrrkv0BGExm" then
   'objRs("acc_ref") = "LbdHyMoiNefoEGe" or

        var_domain = objrs("domain")
        var_platform = objrs("platform")
        if cstr(var_domain) <> cstr(Application("CONFIG_PROVIDER")) then
            varLoginHASH = generateRandom(44) & objrs("corp_code")
            HASHunique = false
            do while HASHunique = false
                objCommand.commandText = "SELECT COUNT(id) AS numrecords FROM " & Application("DBTABLE_USER_DATA") & " WHERE temp_guid = '" & varLoginHASH & "'"
                set objRsCheck = objCommand.execute
                if objRsCheck("numrecords") = 0 then
                    objCommand.commandText = "SELECT COUNT(id) AS numrecords FROM " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " WHERE guid = '" & varLoginHASH & "'"
                    set objRsCheck2 = objCommand.execute
                    if objRsCheck2("numrecords") = 0 then
                        HASHunique = true
                    end if
                    set objRsCheck2 = nothing
                end if
                set objRsCheck = nothing

                if HASHunique = false then
                    varLoginHASH = generateRandom(44) & objrs("corp_code")
                end if
            loop

            'save the session values so that they can be called on later (email redirect, mainly)
            For Each Item In Session.Contents
                if isnull(session(Item)) = false then
                    var_item_content = replace(session(Item),"'","`")
                else
                    var_item_content = session(Item)
                end if

                objCommand.commandText = "INSERT INTO " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " (corp_code, guid, session_name, session_value) " & _
                                         "VALUES ('" & objrs("corp_code") & "', '" & varLoginHASH & "', '" & Item & "', '" & var_item_content & "')"
                objCommand.execute
            Next


            objCommand.commandText = "UPDATE " & Application("DBTABLE_USER_DATA") & " SET temp_guid = '" & varLoginHASH & "' WHERE corp_code = '" & objrs("corp_code") & "' AND acc_name = '" & objrs("acc_name") & "' and acc_ref = '" & objrs("acc_ref") & "'"
            objCommand.execute
            'var_domain_redirect = var_domain & "/version3.2/security/handler/LoginCheckv3.asp?hash=" & varLoginHASH

	    if var_platform = "Setup" then
                var_domain_redirect = var_domain & "/auth.aspx?h=" + varLoginHASH
            else
                var_domain_redirect = var_domain & "/version3.2/security/login/frm_lg_fwswitch.asp?h=" & varLoginHASH
            end if

	    call endConnections()
            response.redirect (var_domain_redirect)
            response.end
        else
            session("SESSION_HOME_DOMAIN") = Application("CONFIG_PROVIDER")
            session("SESSION_HOME_PLATFORM") = var_platform
        end if
'else
'    session("SESSION_HOME_DOMAIN") = "https://build.assessweb.co.uk:4430"
'    session("SESSION_HOME_PLATFORM") = "App1"
'end if


        ' record which server we're connected to by reference rather than by name

        objCommand.commandText = "SELECT platform FROM " & Application("DBTABLE_HR_PLATFORMS") & " WHERE domain = '" & Application("CONFIG_PROVIDER") & "'"
        set objRs_platforms = objCommand.execute
        if not objRs_platforms.EOF then
            session("SESSION_CURRENT_PLATFORM") = objRs_platforms("platform")
        end if







        Session("YOUR_FULLNAME") = objRs("Per_fname") & " " & objRs("Per_sname")
        Session("YOUR_EMAIL") = objRs("Corp_email")
        Session("YOUR_ACCESS") = objRs("Acc_level")
        Session("YOUR_GROUP") = objRs("Corp_BG")
        Session("YOUR_COMPANY") = objRs("Corp_BU")
        Session("YOUR_LOCATION") = objRs("Corp_BL")
        Session("YOUR_DEPARTMENT") = objRs("Corp_BD")
        Session("YOUR_TIER5") = objRs("tier5_ref")
        Session("YOUR_TIER6") = objRs("tier6_ref")
        Session("YOUR_GROUP_NAME") = objRs("tier1_name")
        Session("YOUR_COMPANY_NAME") = objRs("tier2_name")
        Session("YOUR_LOCATION_NAME") = objRs("tier3_name")
        Session("YOUR_DEPARTMENT_NAME") = objRs("tier4_name")
        Session("YOUR_PASSWORD") = objRs("Acc_pass")
        Session("YOUR_MQUESTION") = objRs("Acc_mquestion")
        Session("YOUR_MANSWER") = objRs("Acc_manswer")
        Session("YOUR_USERNAME") = objRs("Acc_name")
        Session("YOUR_ACCOUNT_ID") = objRs("Acc_ref")
        Session("YOUR_LICENCE") = objRs("Acc_licence")

        'if Session("YOUR_ACCESS") = "0" or session("YOUR_ACCOUNT_ID") = "DO2tbpqvrRuVShM" then Session("YOUR_LICENCE")  = left(Session("YOUR_LICENCE"),28) & "1" & right(Session("YOUR_LICENCE"),len(Session("YOUR_LICENCE"))-29)
        'response.Write "1234567890123456789012345678<span style='color: Red;'>9</span>012345678901234567890<br />"
       ' response.Write Session("YOUR_LICENCE")
        'response.end

        Session("YOUR_LANGUAGE") = "en-gb"

        Session("YOUR_ACCOUNT_ID_RESELLER") = objRs("RN_Acc_Ref")
        Session("YOUR_FIRSTNAME") = objRs("Per_fname")
        Session("YOUR_MIDDLENAME") = objRs("Per_mname")
        Session("YOUR_SURNAME") = objRs("Per_sname")
        Session("YOUR_TITLE") = objRs("Per_title")
        Session("YOUR_ADDR_STREET") = objRs("Per_street")
        Session("YOUR_ADDR_AREA") = objRs("Per_town")
        Session("YOUR_ADDR_COUNTY") = objRs("Per_county")
        Session("YOUR_ADDR_COUNTY_NAME") = getCountyForCode2(objRs("Per_county"))
        Session("YOUR_ADDR_COUNTRY") = objRs("Per_country")
        Session("YOUR_ADDR_POSTCODE") = objRs("Per_pcode")
        Session("YOUR_UNIQUE_ID") = objRs("Per_idnumber")
        Session("YOUR_PHONE") = objRs("Corp_phone")
        Session("YOUR_FAX") = objRs("Corp_fax")
        Session("CORP_CODE") = objRs("Corp_code")
        Session("CORP_PIN") = objRs("Corp_pin")
        Session("CORP_ACCBOOK") = objRs("Corp_Accbook")

        Session("YOUR_JOB_TITLE") = objRs("per_job_title")
        Session("YOUR_ACCESS_STATUS") = objRs("Acc_status")
        Session("YOUR_IP_ADDRESS") = Request.ServerVariables("REMOTE_ADDR")
        Session("SA_CONTRACTS") = objRs("sa_contracts")
        Session("SA_ADMIN") = objRs("acc_sadmin")
        var_tm_access = objrs("tm_access")

        Session("FONT_PREFERENCE") = objRs("font_preference")

        if session("corp_code") = "005004" or session("corp_code") = "021205" then
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user / pupil"
        else
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user"
        end if


        Session("USER_CHECK_SETTINGS") = objRs("settings_check")
        Session("ALLOW_SUPPORT_REQUEST") = objRs("allow_support_request")
        Session("TM_DATE_DEFAULT") = objRs("tm_date_default")


        ' Create cookie
'        IF request.Form("writeCookie") = "Y" Then
'            Response.Cookies("SavedLogin")("USER") = varfrmUSER
'            Response.Cookies("SavedLogin").Expires = Date + 30
'            else
'            ' kill it if they have took the remember me off
'            If Request.Cookies("SavedLogin").HasKeys then
'            Response.Cookies("SavedLogin").Expires = Date - 1
'            end if
'        END IF






        '' Build SD elements
        If Len(objRs("sd_users_shown")) > 0 Then Session("SD_USERS_SHOWN") = objRs("sd_users_shown") & ""
        If Len(objRs("sd_users_allowed")) > 0 Then Session("SD_USERS_ALLOWED") = objRs("sd_users_allowed") & ""
        If Len(objRs("sd_start_hour")) > 0 Then Session("SD_START_HOUR") = objRs("sd_start_hour")
        If Len(objRs("sd_end_hour")) > 0 Then Session("SD_END_HOUR") = objRs("sd_end_hour")

   '     '' Add additional accident data
   '     If Len(Session("CORP_ACCBOOK")) > 1 THEN
   '         ObjCommand.Commandtext = "SELECT REC_BU,REC_BL FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " WHERE ACCB_CODE='" & Session("CORP_ACCBOOK") & "' AND CORP_CODE='" & Session("CORP_CODE") & "' "
   '         Set objRs = objcommand.execute

   '         If objRs.EOF THEN
   '             ' Remove un-needed session variables
   '             Session.Contents.Remove("CORP_ACCBOOK")
   '             Session.Contents.Remove("YOUR_ADDR_STREET")
   '             Session.Contents.Remove("YOUR_ADDR_AREA")
   '             Session.Contents.Remove("YOUR_ADDR_COUNTY")
   '             Session.Contents.Remove("YOUR_ADDR_POSTCODE")
   '             Session.Contents.Remove("YOUR_JOB_TITLE")
   '         Else
   '             Session("CORP_ACCBOOK_UNIT") = objRs("REC_BU")
   '             Session("CORP_ACCBOOK_LOC") = objRs("REC_BL")
   '         End if
   '     End if



        '********Global preferences************
        session("default_min_date") = ""
        objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_HR_PREFERENCE") & " WHERE corp_code = '" & session("corp_code") & "' and (user_ref = '" &  session("YOUR_ACCOUNT_ID") & "' or  LOWER(user_ref) = 'all') and (language='en-gb' or LOWER(language) = 'all')"
        set objrs2 = objcommand.execute
        while not objrs2.eof
                session(objrs2("pref_id")) = objrs2("pref_switch")
            objrs2.movenext
        wend
        set objRs2 = nothing


        '********Global preference - priority*********
        ' if pref_task_priority is set and user is a local or global admin, set this preference too
        if session("PREF_TASK_PRIORITY") = "Y" and (session("YOUR_ACCESS") = "0" or session("YOUR_ACCESS") = "1") then
            session("PREF_TASK_PRIORITY_FREEDATE") = "Y"
        end if

        if session("default_min_date") <> "i" and session("default_min_date") <> "" then
            session("default_min_date") = "01/01/" & (2012 - session("default_min_date"))
        end if
        '*********************************************

        '******* Custom Label Changes ********
        if len(session("coshh_label_change")) = 0 then
            session("coshh_label_change") = "COSHH"
        end if

        if len(session("non_conformance_label_change")) = 0 then
            session("non_conformance_label_change") = "Non Conformance"
        end if

        if len(session("violence_label_change")) = 0 then
            session("violence_label_change") = "Violence"
        end if

        '*************************************

        session("user_perms_active") = 0
        if session("global_usr_perm") = "Y" then
            session("user_tm_active") =  var_tm_access
        else
            session("global_usr_perm") = "N"
        end if



        '-------------------------
        '------------- TIME OUT --

         ' see if they are using a global preference for timeout.


        if session("global_timeout") = "0" or  len(session("global_timeout")) = 0 then
            if len(objrs("acc_timeout")) > 0 then
                Session.TimeOut = objRs("acc_timeout")
            else
                Session.TimeOut = 20
            end if
        else
            if  IsNumeric(session("global_timeout")) then
                Session.TimeOut = session("global_timeout")
            else
                Session.TimeOut = 20
            end if

        end if


        if Session("defaultlocation_to_any") = "Y" Then
            if Session("YOUR_COMPANY") = "-1" then Session("YOUR_COMPANY") = "0"
            if Session("YOUR_LOCATION") = "-1" then Session("YOUR_LOCATION") = "0"
            if Session("YOUR_DEPARTMENT") = "-1" then Session("YOUR_DEPARTMENT") = "0"
            if Session("YOUR_TIER5") = "-1" then Session("YOUR_TIER5") = "0"
            if Session("YOUR_TIER6") = "-1" then Session("YOUR_TIER6") = "0"
        end if



      '  response.write session.Timeout
       ' response.end


        ' Session sync cookie
        ' **********************************************
        'added for .net
        userSessionGUID_temp = Request.Cookies("sync")
        guidValid = false
        if len(userSessionGUID_temp) > 0 then
            'lookup session with saved GUID - if active, continue login, else create one  ---only needed during development. .net will create the session in future
            objCommand.commandText = "EXEC Module_LOG.dbo.checkSessionStatus '" & userSessionGUID_temp & "'"
            set objRsGUID = objCommand.execute
            if not objRsGUID.EOF then
                guidValid = objRsGUID("status")
                userSessionGUID = userSessionGUID_temp
            end if
        end if
        'end of .net addition
        if guidValid = false then
            objCommand.commandText = "EXEC Module_LOG.dbo.createSession '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_IP_ADDRESS") & "', '" & session.Timeout & "', '0'"
            set objRsGUID = objCommand.execute
            if not objRsGUID.EOF then
                userSessionGUID = objRsGUID("sessionGUID")
            else
                Call errCode("0001")
            end if
            Response.Cookies("sync") = userSessionGUID
            Response.Cookies("sync").Domain = "riskex.co.uk"
        end if
        set objRsGUID = nothing
        ' **********************************************


        set objRs = nothing

        '-----------------------
        '--------E ND TIME OUT--

        if session("global_usr_perm") = "Y"   then



            '*********BUILD THE TEMP TABLE FOR PERMISSIONS*********
            objcommand.commandtext = "EXEC Module_HR.dbo.sp_build_perms '" & session("corp_code") & "', '" &  session("YOUR_ACCOUNT_ID") & "', 'GEN'"
            objcommand.execute

            if session("YOUR_ACCESS") <> "0" then
                session("allow_create") = true
                session("allow_edit") = true
                session("user_perms_active") = 1

                objcommand.commandtext = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where perm_create = 1"
                set objrs = objcommand.execute

                if  objrs.eof then
                    session("allow_create") = False
                end if

                objcommand.commandtext = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where perm_edit = 1"
                set objrs = objcommand.execute

                if  objrs.eof then
                    session("allow_edit") = False
                end if


                session("allow_dse_view") = true
                session("allow_dse_review") = true
                session("allow_dse_admin") = true

                objcommand.commandtext = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_read = 1"
                set objrs = objcommand.execute

                if  objrs.eof then
                    session("allow_dse_view") = False
                end if

                objcommand.commandtext = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_review = 1"
                set objrs = objcommand.execute

                if  objrs.eof then
                    session("allow_dse_review") = False
                end if

                objcommand.commandtext = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_admin = 1"
                set objrs = objcommand.execute

                if  objrs.eof then
                    session("allow_dse_admin") = False
                end if
            end if

        end if





        '********JOB ROLE ASSIGNMENT***********
        objCommand.commandText = "SELECT roles.session_role FROM " & Application("DBTABLE_JOB_ROLES") & " AS roles " & _
                                 "INNER JOIN " & Application("DBTABLE_USER_JOB_ROLES") & " AS userroles " & _
                                 "  ON roles.corp_code = userroles.corp_code " & _
                                 "  AND roles.role_ref = userroles.job_role " & _
                                 "WHERE userroles.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND userroles.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                 "  AND roles.session_role IS NOT NULL"
        set objRs = objCommand.execute
        if not objRs.EOF then
            while not objRs.EOF
                session(objRs("session_role")) = "Y"
                objRs.movenext
            wend
        end if
        set objRs = nothing




        'CORPORATE STRUCTURE NAMING DEFAULTS
        if len(session("CORP_TIER2")) = 0 or isnull(session("CORP_TIER2")) = true then
            session("CORP_TIER2") = "Company"
            session("CORP_TIER2_PLURAL") = "Companies"
        end if
        if len(session("CORP_TIER3")) = 0 or isnull(session("CORP_TIER3")) = true then
            session("CORP_TIER3") = "Location"
            session("CORP_TIER3_PLURAL") = "Locations"
        end if
        if len(session("CORP_TIER4")) = 0 or isnull(session("CORP_TIER4")) = true then
            session("CORP_TIER4") = "Department"
            session("CORP_TIER4_PLURAL") = "Departments"
        end if



		'********Accident Status Title*********
		if len(session("ACC_STATUS_COL_TITLE")) = 0 then
			session("ACC_STATUS_COL_TITLE") = "Status"
		end if

        '********Password Expired?**************
        if len(session("PASSWORD_EXPIRY")) = 0 then
            var_password_expiry = 0 'password never expires (or preference hasn't been set in preferences table)
        else
            var_password_expiry = cint(session("PASSWORD_EXPIRY"))
        end if

        if var_password_expiry > 0 then
            objCommand.commandText = "SELECT TOP 1 id FROM " & Application("DBTABLE_USER_PASSWORD") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                     "  AND acc_pass = '" & session("YOUR_PASSWORD") & "' " & _
                                     "  AND (GETDATE() >= gendatetime+" & var_password_expiry & " OR sysacc_pass = 1)" & _
                                     "ORDER BY gendatetime DESC"
            objCommand.execute
            set objRs = objCommand.execute
            if not objRs.EOF and (instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 or instr(1,ucase(session("YOUR_USERNAME")),"RTECHNICIAN") = 0) then
                Session("PASSWORD_EXPIRED") = True
            end if
            set objRs = nothing
        elseif var_password_expiry = 0 then
            'check for password resets from the 'forgot password' page
            objCommand.commandText = "SELECT TOP 1 id FROM " & Application("DBTABLE_USER_PASSWORD") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                     "  AND acc_pass = '" & session("YOUR_PASSWORD") & "' " & _
                                     "  AND sysacc_pass = 1 " & _
                                     "ORDER BY gendatetime DESC"
            objCommand.execute
            set objRs = objCommand.execute
            if not objRs.EOF and (instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 or instr(1,ucase(session("YOUR_USERNAME")),"RTECHNICIAN") = 0) then
                Session("PASSWORD_EXPIRED") = True
            end if
            set objRs = nothing
        end if

        '*********Never Logged in before*********
        if ucase(session("CORP_PIN")) <> "BETA" then
            objCommand.commandText = "SELECT top 1 id FROM " & Application("DBTABLE_LOG_ACCESS") & " " & _
                                     "WHERE USERNAME = '" & session("YOUR_USERNAME") & "' " & _
                                     "  AND success='1' " & _
                                     "ORDER BY ID DESC"
            set objRs = objCommand.execute
            if objRs.EOF and instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 then
                Session("USER_CHECK_SETTINGS") = True
            end if
            set objRs = nothing
        end if



        '******** logo ,  maintenance , structure locking ************
        objcommand.commandtext = "select logo, maintenance, lock_structure from " & Application("DBTABLE_HR_SYSTEMS") & " where corp_code='" & session("corp_code") & "'"
        set objrs = objcommand.execute

        if not objrs.eof then
            if objrs("logo") = "Y" then
                session("corp_logo") = session("corp_code") & ".png"
            else
                session("corp_logo") = "ASSN_default_logo.png"
            end if

            'maintenance switch
            if objrs("maintenance") and (left(ucase(session("YOUR_USERNAME")),8) <> "SYSADMIN" and left(ucase(session("YOUR_USERNAME")),11) <> "RTECHNICIAN" and left(ucase(session("YOUR_USERNAME")),13) <> "STRUCTUREUSER") then
                Call errCode("0009")
            end if

            'structure locking switch
            if instr(1, session("YOUR_USERNAME"), "rtechnician.") > 0 OR instr(1, session("YOUR_USERNAME"), "radmin.") > 0 OR instr(1, session("YOUR_USERNAME"), "sysadmin.") > 0 then
                session("LOCK_STRUCTURE_CHANGES") = false
            else
                session("LOCK_STRUCTURE_CHANGES") = objRs("lock_structure")
            end if
        end if



        '**********************DSE Preferences********************************
        objcommand.commandtext = "exec MODULE_Global.dbo.sp_DSE_Training_Pref_GET '" & session("corp_code") & "'"
        set objrs = objcommand.execute

        if objrs.eof then
            'enter defaults
            '*******************Please note that these defaults are also set in the dse_settings.asp page in the admin area
            session("dse_training_type") = "2"
            session("dse_training_years") = "1"
            session("dse_exam") = True
            session("dse_exam_set") = True
            session("dse_pass_rate") = "70"
            session("dse_no_questions") = "10"
            session("dse_attempts") = "3"
            session("attempt") = 0
            session("dse_on_fail") = "4"
            session("dse_on_final_fail") = "4"
            session("dse_on_fail_email") = ""
            session("dse_certificate") = True
            session("issues_box") = "Contact the Administration Manager to arrange your eyesight test"
        else
            session("dse_training_type") = objrs("training_type")
            session("dse_training_years") = objrs("training_years")
            session("dse_exam") = objrs("exam_flag")
            session("dse_exam_set") = objrs("exam_flag")
            session("dse_pass_rate") = objrs("pass_rate")
            session("dse_no_questions") = objrs("no_questions")
            session("dse_attempts") = objrs("attempts")
            session("attempt") = 0
            session("dse_on_fail") = objrs("on_fail")
            session("dse_on_final_fail") = objrs("on_final_fail")
            session("dse_on_fail_email") = objrs("on_fail_email")
            session("dse_certificate") = objrs("certificate_flag")
            session("issues_box") = objrs("issues_box")
        end if



        '*************************Task Accept Switch******************************
        objCommand.commandText = "SELECT pref_switch FROM " & Application("DBTABLE_HR_PREFERENCE") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND pref_id = 'task_accept' " & _
                             "  AND (user_ref = 'all' OR user_ref = '" & session("YOUR_ACCOUNT_ID") & "')"
        set objRs = objCommand.execute
        if not objRs.EOF then
            session("TASK_ACCEPT_ENABLED") = objRs("pref_switch")
        else
            session("TASK_ACCEPT_ENABLED") = "N"
        end if
          '*******************End of Task Accept Switch**************************





		                    Session.Contents.Remove("MODULE_RA")
                            Session.Contents.Remove("MODULE_DSE")
                            Session.Contents.Remove("MODULE_ACCB")
                            Session.Contents.Remove("MODULE_TRA")
                            Session.Contents.Remove("MODULE_MH")
                            Session.Contents.Remove("MODULE_SP")
                            Session.Contents.Remove("MODULE_COSHH")
                            Session.Contents.Remove("MODULE_FS")
                            Session.Contents.Remove("MODULE_SA")
                            Session.Contents.Remove("MODULE_QA")
                            Session.Contents.Remove("MODULE_CMS")
                            Session.Contents.Remove("MODULE_SD")
                            Session.Contents.Remove("MODULE_PTW")
                            Session.Contents.Remove("MODULE_FC")
                            Session.Contents.Remove("MODULE_MS")
                            Session.Contents.Remove("MODULE_INS")
				            Session.Contents.Remove("MODULE_HAZR")
                            Session.Contents.Remove("MODULE_HP")
				            Session.Contents.Remove("MODULE_MAINR")
				            Session.Contents.Remove("MODULE_COMPL")
				            Session.Contents.Remove("MODULE_ASSET")
                            Session.Contents.Remove("MODULE_ASB")
                            Session.Contents.Remove("MODULE_RR")
                            Session.Contents.Remove("MODULE_PUWER")
				            Session.Contents.Remove("MODULE_TRAINING")
				            Session.Contents.Remove("MODULE_LOGBOOK")
				            Session.Contents.Remove("MODULE_CHECKLIST")
                            Session.Contents.Remove("MODULE_SURA")
                            Session.Contents.Remove("MODULE_TOOLS")
                            Session.Contents.Remove("MODULE_SELF")

                            ' Edition string for EDITION
                            Session.Contents.Remove("EDITION")
                            Session.Contents.Remove("MODULE_RA_RO")
                            Session.Contents.Remove("MODULE_DSE_RO")
                            Session.Contents.Remove("MODULE_ACCB_RO")
                            Session.Contents.Remove("MODULE_MH_RO")
                            Session.Contents.Remove("MODULE_MS_RO")
                            Session.Contents.Remove("MODULE_INS_RO")
                            Session.Contents.Remove("MODULE_COSHH_RO")
                            Session.Contents.Remove("MODULE_FS_RO")
                            Session.Contents.Remove("MODULE_SA_RO")
                            Session.Contents.Remove("MODULE_QA_RO")
                            Session.Contents.Remove("MODULE_PTW_RO")

                            Session.Contents.Remove("MODULE_TM")

        ' Build licence information for admins
            objCommand.Commandtext = "SELECT corp_licence_type, corp_licence_version, read_only FROM " & Application("DBTABLE_MODULES_DATA") & " WHERE corp_code='" & Session("CORP_CODE") & "' "
            Set objRs = objcommand.execute



            If objRs.EOF Then
                ' No modules - tell user of error
                IF Len(Session("SA_CONTRACTS")) < 1 Then
                Call errCode("0002")
                End IF
            Else
                While NOT objRs.EOF
                    SELECT CASE objRs("corp_licence_type")
                         CASE "RA"
                            Session("MODULE_RA") = objRs("corp_licence_version")
                            session("MODULE_RA_RO") = objRs("read_only")
                         CASE "MS"
                            Session("MODULE_MS") = objRs("corp_licence_version")
                            session("MODULE_MS_RO") = objRs("read_only")

                            session("MS_AUTH") = "Y"
                         CASE "MH"
                            Session("MODULE_MH") = objRs("corp_licence_version")
                            session("MODULE_MH_RO") = objRs("read_only")
                         CASE "DSE"
                            Session("MODULE_DSE") = objRs("corp_licence_version")
                            session("MODULE_DSE_RO") = objRs("read_only")

                            session("DSE_AUTH") = "Y"
                         CASE "FS"
                            Session("MODULE_FS") = objRs("corp_licence_version")
                            session("MODULE_FS_RO") = objRs("read_only")
                         CASE "SP"
                            Session("MODULE_SP") = objRs("corp_licence_version")
                         CASE "COSHH"
                            Session("MODULE_COSHH") = objRs("corp_licence_version")
                            session("MODULE_COSHH_RO") = objRs("read_only")
                         CASE "SA"
                            Session("MODULE_SA") = objRs("corp_licence_version")
                            session("MODULE_SA_RO") = objRs("read_only")
                         CASE "QA"
                            Session("MODULE_QA") = objRs("corp_licence_version")
                            session("MODULE_QA_RO") = objRs("read_only")
                         CASE "PTW"
							Session("MODULE_PTW") = objRs("corp_licence_version")
							session("MODULE_PTW_RO") = objRs("read_only")
                         CASE "ACCB"
                            Session("MODULE_ACCB") = objRs("corp_licence_version")
                            session("MODULE_ACCB_RO") = objRs("read_only")
                         CASE "TRA"
                            Session("MODULE_TRA") = objRs("corp_licence_version")
                         CASE "CMS"
                            Session("MODULE_CMS") = objRs("corp_licence_version")
                         CASE "SD"
                            Session("MODULE_SD") = objRs("corp_licence_version")
                         CASE "FC"
							Session("MODULE_FC") = objRs("corp_licence_version")
					     CASE "INS"
							Session("MODULE_INS") = objRs("corp_licence_version")
							session("MODULE_INS_RO") = objRs("read_only")
						 CASE "SURA"
							Session("MODULE_SURA") = objRs("corp_licence_version")
							session("MODULE_SURA_RO") = objRs("read_only")
					     CASE "TOOLS"
							Session("MODULE_TOOLS") = objRs("corp_licence_version")
					     CASE "REPS"
							Session("MODULE_REPS") = objRs("corp_licence_version")
						 CASE "HAZR"
							Session("MODULE_HAZR") = objRs("corp_licence_version")
    			         CASE "HP"
							Session("MODULE_HP") = objRs("corp_licence_version")
						 CASE "MAINR"
							Session("MODULE_MAINR") = objRs("corp_licence_version")
					     CASE "COMPL"
					        Session("MODULE_COMPL") = objRs("corp_licence_version")
					     CASE "ASSET"
					        Session("MODULE_ASSET") = objRs("corp_licence_version")
                         CASE "ASB"
                            Session("MODULE_ASB") = objRs("corp_licence_version")
                         CASE "RR"
                            Session("MODULE_RR") = objRs("corp_licence_version")
                         CASE "PUWER"
                            Session("MODULE_PUWER") = objRs("corp_licence_version")
					     CASE "LB"
					        Session("MODULE_LOGBOOK") = objRs("corp_licence_version")
					     CASE "CL"
					        Session("MODULE_CHECKLIST") = objRs("corp_licence_version")
					     CASE "PORTAL"
					        Session("MODULE_PORTAL") = objRs("corp_licence_version")
					     CASE "TMX"
					        Session("MODULE_TRAINING") = objRs("corp_licence_version")
					     CASE "SME"
					        Session("EDITION") = "SME"
					     CASE "RN"
					        Session("EDITION") = "RN"
					     CASE "DEMO"
					        Session("EDITION") = "DEMO"
                         CASE "TM"
                            Session("MODULE_TM") = objRs("corp_licence_version")
                        CASE "SELF"
                            Session("MODULE_SELF") =  objRs("corp_licence_version")
                    End Select
                objRs.MoveNext
                Wend
            End IF

          IF UCASE(Session("your_username")) = UCASE("RTECHNICIAN.OCADO") THEN
            Session("MODULE_RA") = "10"
            Session("MODULE_TM") = "10"
            Session("MODULE_SA") = "10"
        END IF


        ' Check for disabled users
        If Session("YOUR_ACCESS_STATUS") = "D" Then
            Call useLOG("2","User Disabled",varfrmUSER,varfrmRES)
            Call errCode("0004")
        ElseiF len(Session("SA_CONTRACTS")) > 0 Then
            Session("PW_OK") = "Y"
            Session("SA_OK") = "Y"
            ' sa logins can bypass the mem info as they are asked it anyways.
            'if Session("SA_LOGIN") = "Y" then
            '    LoginURL = "../Login/frm_lg_accepted.asp"
            'else
            '    LoginURL = "../Login/frm_lg_meminfo.asp"
            'end if

        ' If Access requires more security, set phase 2 flag (mem info) or is R-only
        Elseif (Session("YOUR_ACCESS") <= 3) OR (Session("YOUR_ACCESS") = 4) OR (Len(Session("CORP_ACCBOOK")) > 0) Then
            Session("PW_OK") = "Y"
            ' sa logins can bypass the mem info as they are asked it anyways.
            'if Session("SA_LOGIN") = "Y" then
            '    LoginURL = "../Login/frm_lg_accepted.asp"
            'else
            '    LoginURL = "../Login/frm_lg_meminfo.asp"
            'end if
        end if

        ' check IP address
        if ipCheck(Session("YOUR_IP_ADDRESS"), Session("YOUR_ACCESS")) = "TRUE" Then
            'Session("AUTH") = "Y"

            ' sa logins can bypass the mem info as they are asked it anyways.
            if Session("SA_LOGIN") = "Y" then
                Call useLOG("1","Access OK",varfrmUSER,varfrmRES)
                LoginURL = "../Login/frm_lg_accepted.asp"
            else
                LoginURL = "../Login/frm_lg_meminfo.asp"
            end if
        Else
            ' IP security failed
            Call useLOG("2","Security Failed to Validate",varfrmUSER,varfrmRES)
            Call errCode("0003")
        End if

         'Drop Any currently active temp tables
            var_unique_search_code = session("YOUR_ACCOUNT_ID") & "" & session("corp_code")
          objcommand.commandtext = "exec module_global.dbo.DropTempSearchTables 'RA" & var_unique_search_code & "Search'"
           objcommand.execute

          objcommand.commandtext = "exec module_global.dbo.DropTempSearchTables 'TM" & var_unique_search_code & "Search'"
           objcommand.execute

         objcommand.commandtext = "exec module_global.dbo.DropTempSearchTables 'SA" & var_unique_search_code & "Search'"
           objcommand.execute


    End if


End Sub








Sub checkUSERSUPPORT(varfrmUSER,varfrmRES)

    if Session("YOUR_IP_ADDRESS") <> "195.60.7.88" or len(varfrmUSER) = 0 then
     Call errCode("0011")
    end if

    Objcommand.commandtext = "SELECT hr.*, tier1.struct_name as tier1_name, tier2.struct_name as tier2_name, tier3.struct_name as tier3_name, tier4.struct_name as tier4_name  FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER1")& "  AS tier1 " & _
                             "on hr.corp_code = tier1.corp_code " & _
                             "and hr.corp_bg = tier1.tier1_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER2")& "  AS tier2 " & _
                             "on hr.corp_code = tier2.corp_code " & _
                             "and hr.corp_bu = tier2.tier2_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3")& "  AS tier3 " & _
                             "on hr.corp_code = tier3.corp_code " & _
                             "and hr.corp_bl = tier3.tier3_ref " & _
                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4")& "  AS tier4 " & _
                             "on hr.corp_code = tier4.corp_code " & _
                             "and hr.corp_bd = tier4.tier4_ref " & _
                             "WHERE (LOWER(hr.acc_name) = LOWER('" & varfrmUSER & "')) and (hr.corp_code = '200200')  "
    Set objRs = Objcommand.execute

    If objRs.EOF Then
        '' User details invalid make log entry
        Call useLOG("2","Username or Password invalid",varfrmUSER,varfrmRES)
        Call errCode("0001")
    Else

        ' Make Session elements

        'if len(objrs("acc_timeout")) > 0 then
        '    Session.TimeOut = objRs("acc_timeout")
        'else
        'Session.TimeOut = 20
        'end if

        Session("YOUR_FULLNAME") = objRs("Per_fname") & " " & objRs("Per_sname")
        Session("YOUR_EMAIL") = objRs("Corp_email")
        Session("YOUR_ACCESS") = objRs("Acc_level")
        Session("YOUR_GROUP") = objRs("Corp_BG")
        Session("YOUR_COMPANY") = objRs("Corp_BU")
        Session("YOUR_LOCATION") = objRs("Corp_BL")
        Session("YOUR_DEPARTMENT") = objRs("Corp_BD")
        Session("YOUR_GROUP_NAME") = objRs("tier1_name")
        Session("YOUR_COMPANY_NAME") = objRs("tier2_name")
        Session("YOUR_LOCATION_NAME") = objRs("tier3_name")
        Session("YOUR_DEPARTMENT_NAME") = objRs("tier4_name")
        Session("YOUR_PASSWORD") = objRs("Acc_pass")
        Session("YOUR_MQUESTION") = objRs("Acc_mquestion")
        Session("YOUR_MANSWER") = objRs("Acc_manswer")
        Session("YOUR_USERNAME") = objRs("Acc_name")
        Session("YOUR_LICENCE") = objRs("Acc_licence")
        Session("YOUR_LANGUAGE") = "en-gb"
        Session("YOUR_ACCOUNT_ID") = objRs("Acc_ref")
        Session("YOUR_ACCOUNT_ID_RESELLER") = objRs("RN_Acc_Ref")
        Session("YOUR_FIRSTNAME") = objRs("Per_fname")
        Session("YOUR_MIDDLENAME") = objRs("Per_mname")
        Session("YOUR_SURNAME") = objRs("Per_sname")
        Session("YOUR_TITLE") = objRs("Per_title")
        Session("YOUR_ADDR_STREET") = objRs("Per_street")
        Session("YOUR_ADDR_AREA") = objRs("Per_town")
        Session("YOUR_ADDR_COUNTY") = objRs("Per_county")
        Session("YOUR_ADDR_COUNTY_NAME") = getCountyForCode2(objRs("Per_county"))
        Session("YOUR_ADDR_COUNTRY") = objRs("Per_country")
        Session("YOUR_ADDR_POSTCODE") = objRs("Per_pcode")
        Session("YOUR_UNIQUE_ID") = objRs("Per_idnumber")
        Session("YOUR_PHONE") = objRs("Corp_phone")
        Session("YOUR_FAX") = objRs("Corp_fax")
        Session("CORP_CODE") = objRs("Corp_code")
        Session("CORP_PIN") = objRs("Corp_pin")
        Session("CORP_ACCBOOK") = objRs("Corp_Accbook")

        Session("YOUR_JOB_TITLE") = objRs("per_job_title")
        Session("YOUR_ACCESS_STATUS") = objRs("Acc_status")
        Session("YOUR_IP_ADDRESS") = Request.ServerVariables("REMOTE_ADDR")
        Session("SA_CONTRACTS") = objRs("sa_contracts")
        Session("SA_ADMIN") = objRs("acc_sadmin")
        if session("corp_code") = "005004" or session("corp_code") = "021205" then
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user / pupil"
        else
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user"
        end if

        if Session("defaultlocation_to_any") = "Y" Then
            if Session("YOUR_COMPANY") = "-1" then Session("YOUR_COMPANY") = "0"
            if Session("YOUR_LOCATION") = "-1" then Session("YOUR_LOCATION") = "0"
            if Session("YOUR_DEPARTMENT") = "-1" then Session("YOUR_DEPARTMENT") = "0"
            if Session("YOUR_TIER5") = "-1" then Session("YOUR_TIER5") = "0"
            if Session("YOUR_TIER6") = "-1" then Session("YOUR_TIER6") = "0"
        end if

        ' Create cookie
        IF validateInput(request.Form("writeCookie")) = "Y" Then
            Response.Cookies("SavedLogin")("USER") = varfrmUSER
            Response.Cookies("SavedLogin").Expires = Date + 30
            else
            ' kill it if they have took the remember me off
            If Request.Cookies("SavedLogin").HasKeys then
            Response.Cookies("SavedLogin").Expires = Date - 1
            end if
        END IF

        '' Build SD elements
        If Len(objRs("sd_users_shown")) > 0 Then Session("SD_USERS_SHOWN") = objRs("sd_users_shown") & ""
        If Len(objRs("sd_users_allowed")) > 0 Then Session("SD_USERS_ALLOWED") = objRs("sd_users_allowed") & ""
        If Len(objRs("sd_start_hour")) > 0 Then Session("SD_START_HOUR") = objRs("sd_start_hour")
        If Len(objRs("sd_end_hour")) > 0 Then Session("SD_END_HOUR") = objRs("sd_end_hour")

        '' Add additional accident data
       ' If Len(Session("CORP_ACCBOOK")) > 1 THEN
       '     ObjCommand.Commandtext = "SELECT REC_BU,REC_BL FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " WHERE ACCB_CODE='" & Session("CORP_ACCBOOK") & "' AND CORP_CODE='" & Session("CORP_CODE") & "' "
       '     Set objRs = objcommand.execute''

'            If objRs.EOF THEN
 '               ' Remove un-needed session variables
  '              Session.Contents.Remove("CORP_ACCBOOK")
   '             Session.Contents.Remove("YOUR_ADDR_STREET")
    '            Session.Contents.Remove("YOUR_ADDR_AREA")
     '           Session.Contents.Remove("YOUR_ADDR_COUNTY")
      '          Session.Contents.Remove("YOUR_ADDR_POSTCODE")
       '         Session.Contents.Remove("YOUR_JOB_TITLE")
        '    Else
         '       Session("CORP_ACCBOOK_UNIT") = objRs("REC_BU")
          '      Session("CORP_ACCBOOK_LOC") = objRs("REC_BL")
           ' End if
        'End if


        '********Global preferences************
        objcommand.commandtext = "select * from " & Application("DBTABLE_HR_PREFERENCE") & " where corp_code='" & session("corp_code") & "'"
        set objrs = objcommand.execute

        while not objrs.eof
            if objrs("user_ref") = session("YOUR_ACCOUNT_ID") or lcase(objrs("user_ref")) = "all" then
                session(objrs("pref_id")) = objrs("pref_switch")
            end if
            objrs.movenext
        wend


        '-------------------------
        '------------- TIME OUT --

         ' see if they are using a global preference for timeout.


        if session("global_timeout") = "0" or  len(session("global_timeout")) = 0 then
            if len(objrs("acc_timeout")) > 0 then
                Session.TimeOut = objRs("acc_timeout")
            else
                Session.TimeOut = 20
            end if
        else
            if  IsNumeric(session("global_timeout")) then
                Session.TimeOut = session("global_timeout")
            else
                Session.TimeOut = 20
            end if

        end if


        '********Global preference - priority*********
        ' if pref_task_priority is set and user is a local or global admin, set this preference too
        if session("PREF_TASK_PRIORITY") = "Y" and (session("YOUR_ACCESS") = "0" or session("YOUR_ACCESS") = "1") then
            session("PREF_TASK_PRIORITY_FREEDATE") = "Y"
        end if

		'********Accident Status Title*********
		if len(session("ACC_STATUS_COL_TITLE")) = 0 then
			session("ACC_STATUS_COL_TITLE") = "Status"
		end if


        '********logo************
        objcommand.commandtext = "select logo from " & Application("DBTABLE_HR_SYSTEMS") & " where corp_code='" & session("corp_code") & "'"
        set objrs = objcommand.execute

        if not objrs.eof then
            if objrs("logo") = "Y" then
                session("corp_logo") = session("corp_code") & ".png"
            else
                session("corp_logo") = "ASSN_default_logo.png"
            end if
        end if


       'session("corp_logo") = session("corp_code") & "_3_" & Session("YOUR_LOCATION")

		                    Session.Contents.Remove("MODULE_RA")
                            Session.Contents.Remove("MODULE_DSE")
                            Session.Contents.Remove("MODULE_ACCB")
                            Session.Contents.Remove("MODULE_TRA")
                            Session.Contents.Remove("MODULE_MH")
                            Session.Contents.Remove("MODULE_SP")
                            Session.Contents.Remove("MODULE_COSHH")
                            Session.Contents.Remove("MODULE_FS")
                            Session.Contents.Remove("MODULE_SA")
                            Session.Contents.Remove("MODULE_QA")
                            Session.Contents.Remove("MODULE_CMS")
                            Session.Contents.Remove("MODULE_SD")
                            Session.Contents.Remove("MODULE_PTW")
                            Session.Contents.Remove("MODULE_FC")
                            Session.Contents.Remove("MODULE_MS")
                            Session.Contents.Remove("MODULE_INS")
				            Session.Contents.Remove("MODULE_HAZR")
                            Session.Contents.Remove("MODULE_HP")
				            Session.Contents.Remove("MODULE_MAINR")
				            Session.Contents.Remove("MODULE_COMPL")
				            Session.Contents.Remove("MODULE_ASSET")
                            Session.Contents.Remove("MODULE_ASB")
                            Session.Contents.Remove("MODULE_RR")
                            Session.Contents.Remove("MODULE_PUWER")
				            Session.Contents.Remove("MODULE_TRAINING")
                            Session.Contents.Remove("MODULE_SURA")
                            Session.Contents.Remove("MODULE_TOOLS")
                            ' Edition string for EDITION
                            Session.Contents.Remove("EDITION")

                            Session.Contents.Remove("MODULE_RA_RO")
                            Session.Contents.Remove("MODULE_DSE_RO")
                            Session.Contents.Remove("MODULE_ACCB_RO")
                            Session.Contents.Remove("MODULE_MH_RO")
                            Session.Contents.Remove("MODULE_MS_RO")
                            Session.Contents.Remove("MODULE_INS_RO")
                            Session.Contents.Remove("MODULE_COSHH_RO")
                            Session.Contents.Remove("MODULE_FS_RO")
                            Session.Contents.Remove("MODULE_SA_RO")
                            Session.Contents.Remove("MODULE_QA_RO")
                            Session.Contents.Remove("MODULE_PTW_RO")

                            Session.Contents.Remove("MODULE_TM")

        ' Build licence information for admins
            objCommand.Commandtext = "SELECT corp_licence_type, corp_licence_version, read_only FROM " & Application("DBTABLE_MODULES_DATA") & " WHERE corp_code='" & Session("CORP_CODE") & "' "
            Set objRs = objcommand.execute



            If objRs.EOF Then
                ' No modules - tell user of error
                IF Len(Session("SA_CONTRACTS")) < 1 Then
                Call errCode("0002")
                End IF
            Else
                While NOT objRs.EOF
                    SELECT CASE objRs("corp_licence_type")
                         CASE "RA"
                            Session("MODULE_RA") = objRs("corp_licence_version")
                            session("MODULE_RA_RO") = objRs("read_only")
                         CASE "MS"
                            Session("MODULE_MS") = objRs("corp_licence_version")
                            session("MODULE_MS_RO") = objRs("read_only")

                            session("MS_AUTH") = "Y"
                         CASE "MH"
                            Session("MODULE_MH") = objRs("corp_licence_version")
                            session("MODULE_MH_RO") = objRs("read_only")
                         CASE "DSE"
                            Session("MODULE_DSE") = objRs("corp_licence_version")
                            session("MODULE_DSE_RO") = objRs("read_only")

                            session("DSE_AUTH") = "Y"
                         CASE "FS"
                            Session("MODULE_FS") = objRs("corp_licence_version")
                            session("MODULE_FS_RO") = objRs("read_only")
                         CASE "SP"
                            Session("MODULE_SP") = objRs("corp_licence_version")
                         CASE "COSHH"
                            Session("MODULE_COSHH") = objRs("corp_licence_version")
                            session("MODULE_COSHH_RO") = objRs("read_only")
                         CASE "SA"
                            Session("MODULE_SA") = objRs("corp_licence_version")
                            session("MODULE_SA_RO") = objRs("read_only")
                         CASE "QA"
                            Session("MODULE_QA") = objRs("corp_licence_version")
                            session("MODULE_QA_RO") = objRs("read_only")
                         CASE "PTW"
							Session("MODULE_PTW") = objRs("corp_licence_version")
							session("MODULE_PTW_RO") = objRs("read_only")
                         CASE "ACCB"
                            Session("MODULE_ACCB") = objRs("corp_licence_version")
                            session("MODULE_ACCB_RO") = objRs("read_only")
                         CASE "TRA"
                            Session("MODULE_TRA") = objRs("corp_licence_version")
                         CASE "CMS"
                            Session("MODULE_CMS") = objRs("corp_licence_version")
                         CASE "SD"
                            Session("MODULE_SD") = objRs("corp_licence_version")
                         CASE "FC"
							Session("MODULE_FC") = objRs("corp_licence_version")
					     CASE "INS"
							Session("MODULE_INS") = objRs("corp_licence_version")
							session("MODULE_INS_RO") = objRs("read_only")
						 CASE "SURA"
							Session("MODULE_SURA") = objRs("corp_licence_version")
							session("MODULE_SURA_RO") = objRs("read_only")
					     CASE "TOOLS"
							Session("MODULE_TOOLS") = objRs("corp_licence_version")
					     CASE "REPS"
							Session("MODULE_REPS") = objRs("corp_licence_version")
						 CASE "HAZR"
							Session("MODULE_HAZR") = objRs("corp_licence_version")
						 CASE "HP"
							Session("MODULE_HP") = objRs("corp_licence_version")
						 CASE "MAINR"
						    Session("MODULE_MAINR") = objRs("corp_licence_version")
						 CASE "COMPL"
						    Session("MODULE_COMPL") = objRs("corp_licence_version")
						 CASE "ASSET"
						    Session("MODULE_ASSET") = objRs("corp_licence_version")
                         CASE "ASB"
                            Session("MODULE_ASB") = objRs("corp_licence_version")
                         CASE "RR"
                            Session("MODULE_RR") = objRs("corp_licence_version")
                         CASE "PUWER"
                            Session("MODULE_PUWER") = objRs("corp_licence_version")
					     CASE "LB"
					        Session("MODULE_LOGBOOK") = objRs("corp_licence_version")
					     CASE "CL"
					        Session("MODULE_CHECKLIST") = objRs("corp_licence_version")
					     CASE "PORTAL"
					        Session("MODULE_PORTAL") = objRs("corp_licence_version")
						 CASE "TMX"
						    Session("MODULE_TRAINING") = objRs("corp_licence_version")
					     CASE "SME"
					        Session("EDITION") = "SME"
					     CASE "RN"
					        Session("EDITION") = "RN"
					     CASE "DEMO"
					        Session("EDITION") = "DEMO"
                         CASE "TM"
                            Session("MODULE_TM") = objRs("corp_licence_version")
                    End Select
                objRs.MoveNext
                Wend
            End IF

          IF UCASE(Session("your_username")) = UCASE("RTECHNICIAN.OCADO") THEN
            Session("MODULE_RA") = "10"
            Session("MODULE_TM") = "10"
            Session("MODULE_SA") = "10"
        END IF


        ' Check for disabled users
        If Session("YOUR_ACCESS_STATUS") = "D" Then
            Call useLOG("2","User Disabled",varfrmUSER,varfrmRES)
            Call errCode("0004")
        Elseif ipCheck(Session("YOUR_IP_ADDRESS"), Session("YOUR_ACCESS")) = "TRUE" Then
            Session("AUTH") = "Y"
            Call useLOG("1","Access OK",varfrmUSER,varfrmRES)
            LoginURL = "../../../layout/default.asp?cmd=support"
        Else
            ' IP security failed
            Call useLOG("2","Security Failed to Validate",varfrmUSER,varfrmRES)
            Call errCode("0003")
        End if
    End if
End Sub

%>