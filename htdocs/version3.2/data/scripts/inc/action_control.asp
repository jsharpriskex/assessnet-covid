<%

'declare global variable for array
dim global_user_v3_array
redim global_user_v3_array(3,0)


'Please Note: ADD_ACTION has now been replaced by ADD_ACTION_V2
SUB ADD_ACTION(varMod,varTaskModRef,varTaskModActionRef,varTaskModActionUser,varTaskDueDate,varTaskPri,varTaskDescription)

    SET actobjCommand = Server.CreateObject("ADODB.Command")
    SET actobjCommand.ActiveConnection = objconn

    varTaskDescription = replace(varTaskDescription,"'","''")

    ' Pretend that no root record to the action has been found
    varRecordfound = "NO"
    varTaskStatus = "2"
    

    ' Depending on the module, lets get the location info
    SELECT CASE varMod 

        ' Risk Assessment
        CASE "RA", "RAR"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_RAMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute
	        
	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("rec_tier5_ref")
		        varTaskTier6 = actobjrs("rec_tier6_ref")
	        END IF
	
        ' Manual Handling
        CASE "MH", "MHR"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_MHMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

        ' COSHH
        CASE "CA"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_CA_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ca_ref = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF	
	
        ' COSHH version 2
        CASE "CA2", "CA2R"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_COSHH_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF	

        ' Inspection
        CASE "INS"
            actObjcommand.CommandText = "SELECT pcompany, plocation, tier4_ref, tier5_ref, tier6_ref FROM " & Application("DBTABLE_INS_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORD_REFERENCE='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("pcompany")
		        varTaskLocation = actobjrs("plocation")
		        varTaskDepartment = actobjrs("tier4_ref")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF
	
        CASE "SA"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_SA_AUDIT_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND audit_ref='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

        CASE "QA"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_QA_AUDIT_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND audit_ref='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

	
        CASE "FS", "FSR", "FSO"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_FSMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF
        
        'Permit to work
        CASE "PTW", "PTWR"
            actObjcommand.CommandText =	"select status from " & Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_entries where corp_code = '" & session("corp_code") & "' AND PTW_REFERENCE = '" & varTaskModRef & "' "
            SET actObjrs = actObjcommand.Execute

            IF not actObjrs.EOF Then
                if actObjrs("status") = "3" then
                    varTaskStatus = "0"      
                elseif actObjrs("status") = "2" then
                    varTaskStatus = "1"      
                else
                    varTaskStatus = "2"      
                end if
            end if
      
        Case "MSR"
        actObjcommand.CommandText = "SELECT rec_tier2_ref, rec_tier3_ref, rec_tier4_ref, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_MS_DATA_V2") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
        SET actObjrs = actObjcommand.Execute     
        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	    ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info  
            varTaskCompany = actobjrs("rec_tier2_ref")
		    varTaskLocation = actobjrs("rec_tier3_ref")
		    varTaskDepartment = actobjrs("rec_tier4_ref")
		    varTaskTier5 = actobjrs("rec_tier5_ref")
		    varTaskTier6 = actobjrs("rec_tier6_ref")
        end if

         Case "HAZR"
            actObjcommand.CommandText = "SELECT tier2_ref, tier3_ref, tier4_ref, tier5_ref, tier6_ref FROM " & Application("DBTABLE_HAZ_ENTRIES") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute     
            IF actObjrs.EOF Then
	            ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	            ' Record found, let the system know
	            varRecordfound = "YES"
	            ' Get the location info  
                varTaskCompany = actobjrs("tier2_ref")
		        varTaskLocation = actobjrs("tier3_ref")
		        varTaskDepartment = actobjrs("tier4_ref")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
            end if

            Case "PR","PRR"
                    actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_PUWER_ENTRIES") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
                    SET actObjrs = actObjcommand.Execute     
                    IF actObjrs.EOF Then
	                    ' oops! no record, strange, best tell the app not to change these values
	                ELSE
	                    ' Record found, let the system know
	                    varRecordfound = "YES"
	                    ' Get the location info  
                        varTaskCompany = actobjrs("rec_bu")
		                varTaskLocation = actobjrs("rec_bl")
		                varTaskDepartment = actobjrs("rec_bd")
		                varTaskTier5 = actobjrs("tier5_ref")
		                varTaskTier6 = actobjrs("tier6_ref")
                    end if

            'Permit to work
        CASE "PTW", "PTWR"
            actObjcommand.CommandText =	"select status, rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref from " & Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_entries where corp_code = '" & session("corp_code") & "' AND PTW_REFERENCE = '" & varTaskModRef & "' "
            SET actObjrs = actObjcommand.Execute
            IF actObjrs.EOF Then
	            ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	            ' Record found, let the system know
                varRecordfound = "YES"
                varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
                response.end
            end if

        CASE "DSER", "DSEA"
            actObjcommand.CommandText =	"select rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref from " & Application("DBTABLE_DSE_DATA") & " where corp_code = '" & session("corp_code") & "' AND recordreference = '" & varTaskModRef & "' "
            SET actObjrs = actObjcommand.Execute
            IF actObjrs.EOF Then
	            ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	            ' Record found, let the system know
                varRecordfound = "YES"
                varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
            end if

        CASE "SIRAR", "SU"
            actObjcommand.CommandText =	"select rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref from " & application("DBTABLE_SURA_DATA") & " where corp_code = '" & session("corp_code") & "' AND recordreference = '" & varTaskModRef & "' "
            SET actObjrs = actObjcommand.Execute
            IF actObjrs.EOF Then
	            ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	            ' Record found, let the system know
                varRecordfound = "YES"
                varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
            end if

     Case "HPR"
                    actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_HP_DATA") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
                    SET actObjrs = actObjcommand.Execute     
                    IF actObjrs.EOF Then
	                    ' oops! no record, strange, best tell the app not to change these values
	                ELSE
	                    ' Record found, let the system know
	                    varRecordfound = "YES"
	                    ' Get the location info  
                        varTaskCompany = actobjrs("rec_bu")
		                varTaskLocation = actobjrs("rec_bl")
		                varTaskDepartment = actobjrs("rec_bd")
		                varTaskTier5 = actobjrs("rec_tier5_ref")
		                varTaskTier6 = actobjrs("rec_tier6_ref")
                    End If

    
    

    END SELECT
    ' Check!, does the action already exsist
    if varmod = "PTWR" then
        actObjcommand.CommandText = "SELECT id, task_due_date, task_account, task_status FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "' AND TASK_ACCOUNT = '" & varTaskModActionUser & "'"
    else
    
        actObjcommand.CommandText = "SELECT id, task_due_date, task_account, task_status FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
    end if
    SET actObjrs = actObjcommand.Execute



    IF actObjrs.eof Then
        ' No Action, do an insert
        actObjcommand.CommandText =	"INSERT INTO " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
							        "(corp_code," & _
							        "gendatetime," & _
							        "task_due_date," & _
							        "task_account," & _
							        "task_module," & _
							        "task_description," & _
							        "task_mod_ref," & _
							        "task_mod_action_ref," & _
							        "task_alert_date," & _
							        "task_by," & _
							        "task_status," & _
							        "task_lastmodified," & _
							        "task_lastmodified_by," & _
							        "task_mod_pri," & _
                                    "task_company," & _
							        "task_location," & _
							        "task_department," & _
                                    "task_tier5," & _
							        "task_tier6) " & _
							        "VALUES ('" & Session("CORP_CODE") & "','" & date() & " " & time() & "','" & varTaskDueDate & "','" & varTaskModActionUser & "','" & varMod & "','" & varTaskDescription & "','" & varTaskModRef & "','" & varTaskModActionRef & "','" & varTaskDueDate & "','" & Session("YOUR_ACCOUNT_ID") & "','" & varTaskStatus & "','" & Date() & " " & time() & "','" & Session("YOUR_ACCOUNT_ID") & "','" & varTaskpri & "', '" & varTaskCompany & "', '" & varTaskLocation & "', '" & varTaskDepartment & "', '" & varTaskTier5 & "', '" & varTaskTier6 & "' )"    
    'response.write actObjcommand.CommandText
    'response.end
    actObjcommand.Execute

	Else

	    ' Action is there, ok now lets check if some of the key info has changed
	    dbTaskModActionUser = actObjrs("task_account")
	    dbTaskDueDate = actObjrs("task_due_date")
	    dbTaskStatus = actObjrs("task_status")
	    dbTaskid = actObjrs("id")
	    
	    if varTaskModActionUser <> dbTaskModActionUser then
	        objCommand.commandText = "SELECT per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " " & _
	                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
	                                 "  AND acc_ref = '" & varTaskModActionUser & "'"
	        set actObjRsUser = objCommand.execute
	        if not actObjRsUser.EOF then
	            task_history_text = actObjRsUser("per_fname") & " " & actObjRsUser("per_sname")
	        else
	            task_history_text = "Unknown"
	        end if
	        set actObjRsUser = nothing
	        
	        task_history_text = "Task delegated to " & task_history_text
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if
	    
	    if varTaskDueDate <> dbTaskDueDate then
	        if varMod = "PTW" and varTaskDueDate > dbTaskDueDate then
	            task_history_text = "Permit to Work Extended until " & varTaskDueDate
	        else
	            task_history_text = "Task due date changed from " & dbTaskDueDate & " to " & varTaskDueDate
	        end if
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if
	    
	    if varTaskStatus <> dbTaskStatus then
	    
	        select case varTaskStatus
	            case "0"
	                varTaskStatusText = "Complete"
	            case "1"
	                varTaskStatusText = "Active"
	            case "2"
	                varTaskStatusText = "Pending"
	        end select
	        select case dbTaskStatus
	            case "0"
	                dbTaskStatusText = "Complete"
	            case "1"
	                dbTaskStatusText = "Active"
	            case "2"
	                dbTaskStatusText = "Pending"
	        end select
	    
	        task_history_text = "Task changed from " & dbTaskStatusText & " to " & varTaskStatusText
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if
	    
	    

	    IF varTaskModActionUser <> dbTaskModActionUser Then
	        ' User is not the same so we need to move the action to another user , but mark it as pending.
	        actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
								        "Task_due_date = '" & varTaskDueDate & "', " & _
								        "Task_lastmodified = '" & Date() & "', " & _
								        "Task_lastmodified_by = '" & Session("YOUR_ACCOUNT_ID") & "', " & _
								        "Task_account = '" & varTaskModActionUser & "', " & _
								        "Task_mod_pri = '" & varTaskPri & "', " & _
								        "Task_description = '" & varTaskDescription & "', " & _
								        "Task_status = '" & varTaskStatus & "' " & _ 
								        "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_ACCOUNT= '" & dbTaskModActionUser & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
	        actObjcommand.Execute

	    ELSE
		    ' User is the same, just update the action with raw details
		    actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
									    "Task_due_date = '" & varTaskDueDate & "', " & _
									    "Task_lastmodified = '" & Date() & "', " & _
									    "Task_lastmodified_by = '" & Session("YOUR_ACCOUNT_ID") & "', " & _
									    "Task_mod_pri = '" & varTaskPri & "', " & _
									    "Task_description = '" & varTaskDescription & "', " & _
									    "Task_status = '" & varTaskStatus & "' " & _ 
									    "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_ACCOUNT= '" & dbTaskModActionUser & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
		    actObjcommand.Execute
        End IF
    END IF

    IF varRecordfound = "YES" Then
	    ' ok update the action with the latest location info
	    actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
								    "Task_company = '" & varTaskCompany & "', " & _
								    "Task_location = '" & varTaskLocation & "', " & _
								    "Task_department = '" & varTaskdepartment & "', " & _
								    "Task_tier5 = '" & varTaskTier5 & "', " & _
								    "Task_tier6 = '" & varTaskTier6 & "' " & _
								    "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"

    actObjcommand.Execute
    END IF




'    '************************************************************************
'    '     Send the task to the recipient via email, in addition to adding 
'    '     to task manager if the task is due within the next 7 days
'    '     Don't send the email if the recipient is the person issuing
'    '************************************************************************
'
'    var_current_date_add7 = dateadd("d", 7, date())
'
'    if cdate(var_current_date_add7) >= cdate(varTaskDueDate) then
'        
'        objCommand.commandText = "SELECT corp_email FROM " & Application("DBTABLE_USER_DATA") & " " & _
'                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
'                                 "  AND acc_ref = '" & varTempTaskModActionUser & "' " & _
'                                 "  AND (corp_email IS NOT NULL AND corp_email <> '')"
'        set objRs_email = objCommand.execute
'        if not objRs_email.EOF then
'
'            emailTo = objRs_email("corp_email")
'
'            emailSubject = "AssessNET: A new action has been assigned to you for completion within the next 7 days"
'
'            emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
'                             "<br />" & _
'                             "<p>Dear Sir / Madam,</p>" & _
'                             "<p>The following task has been assigned to you by " & session("YOUR_FULLNAME")  & ".</p>" & _
'                             "<p>" & varTaskDescription & "</p>" & _
'                             "<p>The due date of the task is: <strong>" & varTaskDueDate & "</strong></p>" & _
'                             "<p>To view more information regarding this task, please log into AssessNET and view your task list.</p>" & _
'                             "<p>Kind regards,</p>" & _
'                             "<p>The AssessNET Team</p>" & _
'                         "</BODY></HTML>"
'
'            if session("corp_code") = "000010" then
'                call sendActionEmail("aiptemp@parliament.uk", "task-service@assessnet.co.uk", emailSubject, emailBody)
'            else
'                call sendActionEmail(emailTo, "task-service@assessnet.co.uk", emailSubject, emailBody)
'            end if
'            task_history_text = "Task emailed to: " & varEmailTo
'        
'	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
'
'        end if
'        objRs_email.close()
'    end if

    

    'IF Ucase(varTaskpri) = "HIGH" Then
        'actObjcommand.commandtext = "SELECT per_fname, per_sname, corp_email FROM " & Application("DBTABLE_USER_DATA") & " WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND acc_ref = '" & varTaskModActionUser & "' "
        'SET actObjrs = actobjcommand.execute

        'IF actObjrs.EOF THEN

        'ELSE
            ' This will send a high priority e-mail.
        'END IF
    'END IF

    Objcommand.CommandText = "SELECT top 1 * FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' order by id desc"    
    'response.write objcommand.commandtext
    'response.end    
    SET Objrs = Objcommand.Execute
        
        dbTaskid = Objrs("id")
        varTempTaskModActionUser = Objrs("Task_account")
        var_frm_task_description = Objrs("Task_description")
        var_eventdate = Objrs("Task_due_date")

    if session("task_email_instanotification") = "Y" and Session("corp_code") = "336872"  then
        
        objCommand.commandText = "SELECT corp_email FROM " & Application("DBTABLE_USER_DATA") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND acc_ref = '" & varTempTaskModActionUser & "' " & _
                                 " AND acc_ref <> '" & Session("YOUR_ACCOUNT_ID") & "' " & _ 
                                 "  AND (corp_email IS NOT NULL AND corp_email <> '')"
        set objRs_email = objCommand.execute

        if not objRs_email.EOF then

            emailTo = objRs_email("corp_email")

            emailSubject = "AssessNET: A new action has been assigned to you for completion"

            emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
                             "<br />" & _
                             "<p>Dear Sir / Madam,</p>" & _
                             "<p>The following task has been assigned to you by " & session("YOUR_FULLNAME")  & ".</p>" & _
                             "<p>Task reference: " & dbTaskid & "<br />" & varTaskDescription & "</p>" & _
                             "<p>The due date of the task is: <strong>" & varTaskDueDate & "</strong></p>" & _
                             "<p>To view more information regarding this task, please log into AssessNET and view your task list.</p>" & _
                             "<p>Kind regards,</p>" & _
                             "<p>The AssessNET Team</p>" & _
                         "</BODY></HTML>"

            if session("corp_code") = "000010" then
                call sendEmail("aiptemp@parliament.uk", "task-service@assessnet.co.uk", emailSubject, emailBody)
            else
                emailTo = emailTo

              call sendEmail(emailTo, "task-service@assessnet.co.uk", emailSubject, emailBody)
            end if
            task_history_text = "Task emailed to: " & emailTo
        
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)

        end if
        objRs_email.close()
    end if




END SUB


'Sub sendActionEmail(emailTo,emailFrom,emailSubject,emailBody)
'
'    'this sub has been copied from func_email.asp so that tasks can be sent without
'    'updating the whole system to see func_email. This is an interim update to put
'    'this change out quicker.
'    'MG 11/01/2013
'
'    if session("CORP_CODE") = "000010" then
'        emailTo = "aiptemp@parliament.uk"
'    end if
'  
'    Dim objSend
'    Set objSend = Server.CreateObject("CDONTS.NewMail")
'    if len(emailFrom) = 0 then
'        emailFrom = "notifications@assessnet.co.uk"
'    end if
'    
'    objSend.BodyFormat = 0 ' HTML
'    objSend.MailFormat = 0 '
'    objSend.Importance = 2
'    objSend.To = emailTo
'    objSend.From = emailFrom
'    objSend.Subject = emailSubject
'    objSend.Body = emailBody
'
'    'response.write emailTo & " " & emailFrom
'    'response.end
'    objSend.Send
'    
'    SET objSend = Nothing
'End sub




SUB ADD_TASK_ACTION(varTaskID, varTaskHistory)
    Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " " & _
						     "(corp_code, task_id, task_history_by, task_history, gendatetime) " & _
						     "VALUES('" & Session("CORP_CODE") & "','" & varTaskID & "','" & session("YOUR_ACCOUNT_ID") & "','" & replace(varTaskHistory,"'","`") & "','" & date() & " " & Time() & "')"
    Objcommand.execute
END SUB

      
sub Generate_Action_HTML(var_tm_act_req_heading, var_tm_date_heading, var_tm_act_to_heading, var_tm_act_req_helper, var_tm_act_act_req_id, var_tm_act_act_req_value, var_tm_date_day_name, var_tm_date_month_name, var_tm_date_year_name, var_tm_date_value, var_tm_action_user_id, var_tm_action_user_id_value, var_tm_action_user_id_email, var_tm_action_user_id_email_value, var_risk_rating, var_risk_rating_severity_name, var_risk_rating_severity_text, var_risk_rating_severity_value, var_risk_rating_severity_helper, var_risk_rating_likelihood_name, var_risk_rating_likelihood_text, var_risk_rating_likelihood_value, var_risk_rating_likelihood_helper, var_tm_mode, var_table_css_class, var_tm_cost, var_tm_cost_heading, var_tm_cost_name, var_tm_cost_value, var_tm_localised_users, var_tm_parent_bu, var_tm_parent_bl, var_default_user, var_deadline_date, var_deadline_date_name, var_action_to_email, var_tm_parent_bd, var_frm_task_pri_name, var_tm_list_type, var_tm_recurrence, var_tm_parent_tier5, var_tm_parent_tier6, var_tm_parent_module, var_tm_accb_code, var_frm_task_pri_value, var_frm_task_standard_name, var_frm_task_standard, var_default_action, var_recurrence_task_id, var_frm_task_signoff_name, var_frm_task_signoff_value, sw20)
    '***********Variable summary************
    'var_tm_act_req_heading = Actions required textbox heading
    'var_tm_date_heading =  Due date heading
    'var_tm_act_to_heading = person responsible heading
    'var_tm_act_req_helper = Adds contents to the Actions required textbox heading - in particular its used for the controls assistant
    'var_tm_act_act_req_id = name/id of control for actions required
    'var_tm_act_act_req_value = value of control for actions required
    'var_tm_date_day_name = name of control for due date day
    'var_tm_date_month_name = name of control for due date month
    'var_tm_date_year_name = name of control for due date year
    'var_tm_date_value = value for due date control
    'var_tm_action_user_id = name of control for user to assign task
    'var_tm_action_user_id_value = value of control for user to assign task
    'var_tm_action_user_id_email = value of control for email to assign task to
    'var_tm_action_user_id_email_value = name of control for email to assign task to
    'var_risk_rating =  if true then uses the next 8 variables to add likelihood and severity fields to the remedial action (Code must be provided to process these extra boxes)
    'var_risk_rating_severity_name = name of control for SOH
    'var_risk_rating_severity_text = heading for control for SOH
    'var_risk_rating_severity_value = value of control for SOH
    'var_risk_rating_severity_helper = helper link of control for SOH
    'var_risk_rating_likelihood_name = name of control for LOH
    'var_risk_rating_likelihood_text = heading for control for LOH
    'var_risk_rating_likelihood_value = value of control for LOH
    'var_risk_rating_likelihood_helper = helper link of control for LOH
    'var_tm_mode = if " disabled " disables controls
    'var_table_css_class = css class given to the table that contains the fields e.g. highlight for yellow background to TH cells
    'var_tm_cost = if true then uses the next 3 variables to add a Cost field to the remedial action (Code must be provided to process this extra box)
    'var_tm_cost_heading = heading for cost control
    'var_tm_cost_name = name for cost control
    'var_tm_cost_value = value for cost control
    'var_tm_localised_users = if "Y" then it calls a different drop down list to normal and passes the next two variables 
    'var_tm_parent_bu = assessment company
    'var_tm_parent_bl = assessment location
    'var_default_user =  enforce default user
    'var_deadline_date = if it's length is greater than 0 then the deadline will be included in the code 
    'var_deadline_date_name =   the name of the field which contains the deadline date.
    'var_action_to_email = used to preset who the email will be sent to on those modules which don't distribute tasks until the end    
    'var_tm_parent_bd = assessment department
    'var_frm_task_pri_name = used as a base for all id and form names related to the priority
    'var_tm_list_type = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_E for LogBook Viewers)
    'var_tm_recurrence = recurrence (Y = On / N or "" = Off)
    'var_tm_parent_tier5 =  assessment tier5
    'var_tm_parent_tier6 =   assessment tier6
    'var_tm_parent_module =   assessment module
    'var_tm_accb_code = incident book code
    'var_frm_task_pri_value = value set for priority dropdown
    'var_frm_task_standard_name = name for task standard control
    'var_frm_task_standard = value set for task standard (ISO9001, OHSAS18001, etc... [option values defined by client])
    'var_default_action = enforce the action to be assigned (specifically from audit modules)
    'var_recurrence_task_id = row id of the recurrence record
    'var_frm_task_signoff_name = name for task sign off required control
    'var_frm_task_signoff_value = value for task sign off required control
    'sw20 = Not in use 
    '********End of Variable summary*******
   
    '*****Populate defaults*****
    if len(var_tm_act_req_heading) = 0 then var_tm_act_req_heading = "Actions Required"
    if len(var_tm_date_heading) = 0 then var_tm_date_heading = "Due Date"
    if len(var_tm_act_to_heading) = 0 then var_tm_act_to_heading = "Assign To"

    if len(var_tm_act_req_helper) = 0 then var_tm_act_req_helper = "" else var_tm_act_req_helper = "<span style='padding-left: 10px;'>" & var_tm_act_req_helper & "</span>"
    if len(var_tm_date_value) = 0 then var_tm_date_value = "NONE"
    if not isdate(var_tm_date_value) then var_tm_date_value = "NONE"
    if var_risk_rating = "" then var_risk_rating = false
    if not isnumeric(var_risk_rating_severity_value) then var_risk_rating_severity_value = 0
    if not isnumeric(var_risk_rating_likelihood_value) then var_risk_rating_likelihood_value = 0
    if len(var_table_css_class) > 0 then var_table_css_class = " class='" & var_table_css_class & "' "
   
    if isdate(var_deadline_date) then
        var_tm_date_value = var_deadline_date
        var_deadline_date = true
    else 
        var_deadline_date_name = ""
        var_deadline_date = false
    end if
    

    if len(var_tm_parent_bu) = 0 then var_tm_parent_bu = 0
    if len(var_tm_parent_bl) = 0 then var_tm_parent_bl = 0
    if len(var_tm_parent_bd) = 0 then var_tm_parent_bd = 0
    if len(var_tm_parent_tier5) = 0 then var_tm_parent_tier5 = 0
    if len(var_tm_parent_tier6) = 0 then var_tm_parent_tier6 = 0

    if isnull(var_frm_task_standard) = true then var_frm_task_standard = "x"
    
    response.write "<table width='100%' cellspacing='2px' cellpadding='1px' style='border: none;' " & var_table_css_class & " >" & _
                        "<tr>" & _
                            "<th>" & var_tm_act_req_heading & " " & var_tm_act_req_helper & "</th>"
                            'riskrating code to go here
    if var_risk_rating then 
            response.write "<th width='200px'>" & var_risk_rating_severity_helper & var_risk_rating_severity_text & "</th>"                      
    end if
    response.write      "</tr>" & _
                        "<tr>" & _
                            "<td "
    if var_risk_rating then response.write " rowspan='3' "               
    
     'SA BUG FIX
    if len(var_default_action) = 0 or len(var_tm_act_act_req_value) > 0 then
        response.write          "><textarea class='textarea' rows='6' name='" & var_tm_act_act_req_id & "' id='" & var_tm_act_act_req_id & "' " & var_tm_mode & ">" & var_tm_act_act_req_value & "</textarea></td>"
    else
     
        response.write          "><textarea class='textarea' rows='6' name='" & var_tm_act_act_req_id & "' id='" & var_tm_act_act_req_id & "' " & var_tm_mode & ">" & var_default_action & "</textarea></td>"
   
     '  response.write          "><textarea class='textarea' rows='4' disabled>" & var_default_action & "</textarea><input type='hidden' name='" & var_tm_act_act_req_id & "' id='" & var_tm_act_act_req_id & "' value='" & var_default_action & "' /></td>"
    end if
    
    
    'riskrating code to go here
    if var_risk_rating then                    
     response.write         "<td>" 
                        call showSOH(var_risk_rating_severity_value,var_risk_rating_severity_name, var_tm_mode)
     response.write         "</td>" & _
                        "</tr>" & _
                        "<tr>" & _
                            "<th>" & var_risk_rating_likelihood_helper & var_risk_rating_likelihood_text  & "</th>" & _
                        "</tr>" & _
                        "<tr>" & _
                            "<td>"                            
                        call showLOH(var_risk_rating_likelihood_value,var_risk_rating_likelihood_name, var_tm_mode)
      response.write    "</td>" 

    end if
    response.write      "</tr>"
    response.write "</table>"

    response.write "<table width='100%' cellspacing='2px' cellpadding='1px' border='0' " & var_table_css_class & " >" & _
                        "<tr>" & _
                            "<th width='200px'>" & var_tm_date_heading & "</th><td nowrap>"
                            
                            if session("pref_task_priority") = "Y" and len(var_frm_task_pri_name) > 0 then
                            
                                if var_deadline_date then 'deadline date is in use so kill priority
                                    var_use_tm_pri = false 
                                else
                                    'if var_tm_date_value <> "NONE" then 'date value has already been set
                                    '    var_use_tm_pri = false 
                                    'else
                                        var_use_tm_pri = true 
                                    'end if                                   
                                end if
                            else
                                var_use_tm_pri = false 'priority is not in use so don't use it!                 
                            end if 


                            'fix to get it to redisplay the recurrence details on form load
                            '****************************************************
                            if isnull(var_recurrence_task_id) = true or var_recurrence_task_id = "" or isnumeric(var_recurrence_task_id) = false then
                                var_recurrence_task_id = 0
                            end if

                            date_opt_selected = ""
                            priority_opt_selected = ""
                            recurring_opt_selected = ""
                            objCommand.commandText = "SELECT COUNT(*) AS num_records FROM " & Application("DBTABLE_TASK_RECURRING") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND task_module = '" & var_tm_parent_module & "' AND id = '" & var_recurrence_task_id & "' "
                            set objRsRC = objCommand.execute
                            if var_tm_recurrence = "Y" and objRsRC("num_records") > 0 then
                                recurring_opt_selected = " checked "
                                datediv_status = "none"
                                prioritydiv_status = "none"
                                recurringdiv_status = "block"
                            elseif len(var_frm_task_pri_value) = 0 or isnull(var_frm_task_pri_value) = true and len(recurring_opt_selected) = 0 then
                                date_opt_selected = " checked "
                                datediv_status = "block"
                                prioritydiv_status = "none"
                                recurringdiv_status = "none"                           
                            else
                                priority_opt_selected = " checked "
                                datediv_status = "none"
                                prioritydiv_status = "block"
                                recurringdiv_status = "none"
                            end if
                            '****************************************************
                          
                            if var_use_tm_pri then 
                                objcommand.commandtext = "exec Module_Global.dbo.sp_task_priority_list '" & session("CORP_CODE") & "' "
                                set objrsprigrp = objcommand.execute                           
                                if not objrsprigrp.eof then 

                            if var_frm_task_signoff_value = "1" and session("signoff_date_block") = "Y" then
                                var_disabled = "disabled"
                            else
                                var_disabled = ""
                            end if

                                    if session("pref_task_priority_freedate") = "Y" then

                                        response.write "<div style='display:inline-block;'>"

                                        response.write "<div style='display:inline-block; width:75px;'><input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_pri' value='pri' checked onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)' " & var_tm_mode & " " & var_disabled & "> <label for='" & var_frm_task_pri_name & "_rad_pri'>Priority</label>&nbsp;&nbsp;</div>"
                                        response.write "<div style='display:inline-block; width:75px;'><input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_date' value='date' onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)'  " & var_tm_mode & " " & date_opt_selected & " " & var_disabled & "> <label for='" & var_frm_task_pri_name & "_rad_date'>Date</label>&nbsp;&nbsp;</div>"



                                        if var_tm_recurrence = "Y" then response.write "<div style='display:inline-block; width:75px;'><input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_rec' value='rec' onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)'  " & var_tm_mode & " " & recurring_opt_selected & "> <label for='" & var_frm_task_pri_name & "_rad_rec'>Recurring</label></div>"
                                    
    
                                        response.write "</div>"
    
                                    elseif var_tm_recurrence = "Y" and (var_tm_parent_module = "APS" or var_tm_parent_module = "APQ") then
                                        response.write "<input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_pri' value='pri' checked onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)' " & var_tm_mode & " " & var_disabled & "> <label for='" & var_frm_task_pri_name & "_rad_pri'>Priority</label>&nbsp;&nbsp;"
                                        response.write "<input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_rec' value='rec' onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)'  " & var_tm_mode & " " & recurring_opt_selected & " " & var_disabled & "> <label for='" & var_frm_task_pri_name & "_rad_rec'>Recurring</label>"
                                        datediv_status = "none"
                                        prioritydiv_status = "block"
                                    else
                                        datediv_status = "none"
                                        prioritydiv_status = "block"
                                    end if
                                   
                                    'if var_frm_task_signoff_value = "1" and session("signoff_date_block") = "Y" then
                                        response.write "<div id='" & var_frm_task_pri_name & "_priority_div' style='display: " & prioritydiv_status & "' " & var_disabled & " >"
                                    'else
                                       ' response.write "<div id='" & var_frm_task_pri_name & "_priority_div' style='display: " & prioritydiv_status & "'>"
                                   ' end if

                                    response.write "<select class='select' id='" & var_frm_task_pri_name & "' name='" & var_frm_task_pri_name & "' " & var_tm_mode & " onchange='openTaskPriorityDate(""" & var_frm_task_pri_name & """);'>" & _
                                                   "<option value='na'>Select a priority</option>" & _
                                                   "<optgroup label='Please select a priority'>"  

                                    while not objrsprigrp.eof
                                        var_db_priority_id = objrsprigrp("ref")
                                        var_db_priority_name = objrsprigrp("name")                                    
                                        var_db_priority_colour = objrsprigrp("val_colour")
                                        var_db_priority_maxdate = objrsprigrp("val_datecalc")

                                        if var_frm_task_pri_value = var_db_priority_name then
                                            datediv_status = "block"
                                            response.Write "<option style='color:" & var_db_priority_colour & "' value='" & var_db_priority_id & "' selected>" & var_db_priority_name & "</option>"
                                        else
                                            response.Write "<option style='color:" & var_db_priority_colour & "' value='" & var_db_priority_id & "'>" & var_db_priority_name & "</option>"
                                        end if

                                        objrsprigrp.movenext
                                    wend
                                    response.write "</optgroup></select></div>"
                                    
                                   ' if var_frm_task_signoff_value = "1" and session("signoff_date_block") = "Y" then
                                        response.write "<div id='" & var_frm_task_pri_name & "_date_div' style='display: " & datediv_status & "; padding-top:3px;' " & var_disabled & " >"
                                    'else
                                     '   response.write "<div id='" & var_frm_task_pri_name & "_date_div' style='display: " & datediv_status & "; padding-top:3px;'>"
                                    'end if
                                else ' no priorities available
                                    var_use_tm_pri = false
                                end if
                           
                            end if

                            if var_tm_recurrence = "Y" and not var_use_tm_pri then 'print out the radio buttons
                                response.write "<input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_date' checked value='date' onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)'  " & var_tm_mode & "> <label for='" & var_frm_task_pri_name & "_rad_date'>Date</label>&nbsp;&nbsp;"
                                response.write "<input type='radio' class='radio' name='" & var_frm_task_pri_name & "_rad' id='" & var_frm_task_pri_name & "_rad_rec' value='rec' onChange='swapTaskPriority(""" & var_frm_task_pri_name & """)' " & var_tm_mode & " " & recurring_opt_selected & " > <label for='" & var_frm_task_pri_name & "_rad_rec'>Recurring</label>"
                                'print out the date div
                                response.write "<div id='" & var_frm_task_pri_name & "_date_div' style='display: " & datediv_status & "; padding-top:3px;'>"
                            end if


                                   
                            call autoDate(var_tm_date_value,var_tm_date_day_name,var_tm_date_month_name,var_tm_date_year_name,"F",var_tm_mode,"","")
                            if var_use_tm_pri then    
                                response.write "<br /><span class='info' id='" & var_frm_task_pri_name & "_max_date'></span>"
                            end if
                            if var_deadline_date  then   
                                response.Write "<input type='hidden' name='deadline" & objrs("question_id") & "' value='" & var_tm_date_value & "' /> <span class='info'>Deadline: " & var_tm_date_value & "</span>"
				            end if  
                            
            
                           

                             if var_use_tm_pri and var_tm_recurrence = "Y" then 
                                response.Write "</div>"
                                response.write "<div id='" & var_frm_task_pri_name & "_rec_div' style='display: " & recurringdiv_status & "'>"

                                call displayRecurrence("200px", var_tm_mode, var_tm_parent_module, "", "", "", var_recurrence_task_id)
                                response.write "</div>"
                             elseif var_use_tm_pri  then
                                response.Write "</div>"
                             elseif var_tm_recurrence = "Y" and not var_use_tm_pri then
                                response.Write "</div>"
                               response.write "<div id='" & var_frm_task_pri_name & "_rec_div' style='display: " & recurringdiv_status & "'>"
           
                                    'call displayRecurrence("200px", var_tm_mode, "TM", var_lb_frm_ref, "", "")
                                    call displayRecurrence("200px", var_tm_mode, var_tm_parent_module, "", "", "", var_recurrence_task_id)
                                response.write "</div>"
                             end if

                             if var_tm_recurrence = "Y" then var_use_tm_pri = true
                                'we'll add the date radio button value here in a a hidden field so that we can just check in one place if it was used
                             response.write "<input type='hidden' id='" & var_frm_task_pri_name & "_used' name='" & var_frm_task_pri_name & "_used' value='" & var_use_tm_pri & "' />"
                             
                                                  
    response.write "</td><th  width='200px'>" & var_tm_act_to_heading & "</th><td nowrap>"
     'SA BUG FIX
    if  len(var_tm_action_user_id_value) > 0 then
        var_default_user = 0
    end if

    if var_default_user  then 
        call DisplayUSERInfoV3(var_tm_action_user_id_value, var_tm_action_user_id, "disabled", "", "", "N", "RO", "", var_tm_parent_bu, var_tm_parent_bl, var_tm_parent_bd, var_tm_parent_tier5, var_tm_parent_tier6, var_tm_parent_module, var_tm_accb_code, "", "", "", "")
        response.Write "<input type='hidden' name='" & var_tm_action_user_id & "' value='" & var_tm_action_user_id_value & "' />"        
    else
        if session("pref_task_email") = "Y" then
            response.write          "<div style='display: "
            if var_tm_action_user_id_value = "Email" then response.write "none" else response.write "block"
            response.write ";' id='" & var_tm_action_user_id & "_maindiv'>"

            var_tm_javascript = "onChange=""opentaskEmail('" & var_tm_action_user_id & "', '" & session("YOUR_ACCOUNT_ID") & "')"""
        end if
         
        if var_tm_localised_users = "Y" then
            call DisplayUSERInfo_locationv2(var_tm_action_user_id_value, var_tm_action_user_id, var_tm_mode, var_tm_list_type, var_tm_javascript, "N", "RO", "", var_tm_parent_bu, var_tm_parent_bl, var_tm_parent_bd, "", "", "")
        else 
            call DisplayUSERInfoV3(var_tm_action_user_id_value, var_tm_action_user_id, var_tm_mode, var_tm_list_type, var_tm_javascript, "N", "RO", "", var_tm_parent_bu, var_tm_parent_bl, var_tm_parent_bd, var_tm_parent_tier5, var_tm_parent_tier6, var_tm_parent_module, var_tm_accb_code, "", "", "", "")
        end if

        if session("pref_task_email") = "Y" then
            response.write          "</div>"   
            response.write          "<div style='display: "
            if var_tm_action_user_id_value = "Email" then response.write "block" else response.write "none"
            response.write ";' id='" & var_tm_action_user_id & "_emaildiv'><span style='color: #888'>Please enter an email address for this task to be sent to</span><br /><input type='text' name='" & var_tm_action_user_id_email & "' id='" & var_tm_action_user_id_email & "' class='input' style='width: 250px' value='" & var_action_to_email & "' /> <a onClick=""(canceltaskEmail('" & var_tm_action_user_id & "'))""><img src='" & Application("CONFIG_PROVIDER_SECURE") & "/version" & Application("CONFIG_VERSION") & "/modules/hsafety/risk_assessment/app/img/red_cross.png' alt='Cancel Icon' title='Cancel email user control' style='vertical-align: middle; width: 20px'  /></a></div>"
        end if 
    end if
    response.write          "</td>" 

    if var_tm_cost then 
        response.write "<th>" & var_tm_cost_heading & "</th><td>� <input type='text' class='text' name='" & var_tm_cost_name &"' id='" & var_tm_cost_name & "' value='" & var_tm_cost_value & "' " & var_tm_mode & " /></td>"
    end if

    response.write        "</tr>" 


    if session("PREF_TASK_STANDARDS") = "Y" and (var_tm_parent_module = "SA" or var_tm_parent_module = "QA" or var_tm_parent_module = "ACC" or var_tm_parent_module = "INS") then

        objCommand.commandText = "SELECT opt_id, opt_value FROM " & Application("DBTABLE_TASK_STANDARDS") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "ORDER BY opt_id ASC"
        set objRsOpts = objCommand.execute
        if not objRsOpts.EOF then

            response.write "<tr><th>Associated Standard</th><td colspan='3'>" & _
                           "<select class='select' name='" & var_frm_task_standard_name & "' id='" & var_frm_task_standard_name & "' " & var_tm_mode & ">" & _
                           "<option value='0'>Please Select</option>"
                           
            if cstr(var_frm_task_standard) = "0" then
                response.write "<option value='0' selected>Not Applicable</option>"
            else
                response.write "<option value='0'>Not Applicable</option>"
            end if

            while not objRsOpts.EOF
                if cstr(objRsOpts("opt_id")) = cstr(var_frm_task_standard) then
                    response.write "<option value='" & objRsOpts("opt_id") & "' selected>" & objRsOpts("opt_value") & "</option>"
                else
                    response.write "<option value='" & objRsOpts("opt_id") & "'>" & objRsOpts("opt_value") & "</option>"
                end if

                objRsOpts.movenext
            wend

            response.write "</td></tr>"
        end if
        set objRsOpts = nothing
    end if

   ' response.write "jhfdjsk"

    if session("PREF_TASK_signoff") = "Y" and ((var_tm_parent_module = "SA" and session("PREF_TASK_signoff_sa") = "T") or (var_tm_parent_module = "ACC" and session("PREF_TASK_signoff_acc") = "T") or (var_tm_parent_module = "INS" and session("PREF_TASK_signoff_ins") = "T") ) then
        response.write "<tr><th>Require task to be signed off</th><td colspan='3'><input  class='checkbox' type='checkbox' name='" & var_frm_task_signoff_name & "' id='" & var_frm_task_signoff_name & "' value='1'"
        if session("YOUR_ACCESS") <> "0" and session("signoff_date_block") = "Y" then response.write " disabled"
        if var_frm_task_signoff_value then  response.write " checked "
        response.write   " " & var_tm_mode & " /></td></tr>"
    elseif session("PREF_TASK_signoff") = "Y" and ((var_tm_parent_module = "SA" and session("PREF_TASK_signoff_sa") = "A")  or (var_tm_parent_module = "ACC" and session("PREF_TASK_signoff_acc") = "A") or (var_tm_parent_module = "INS" and session("PREF_TASK_signoff_ins") = "A") ) then
        response.write "<input type='hidden' value='1' name='" & var_frm_task_signoff_name & "' id='" & var_frm_task_signoff_name & "' >"   
    else
        response.write "<input type='hidden' value='0' name='" & var_frm_task_signoff_name & "' id='" & var_frm_task_signoff_name & "' >"
    end if
        
    'if var_tm_recurrence = "Y" then
    '    response.write "<tr><td colspan='4'><div id='" & var_frm_task_pri_name & "_rec_div' style='display: none'>"
    '                            
    '    call displayRecurrence("200px", var_tm_mode, "TM", var_lb_frm_ref, "", "")
    '    response.write "</div></td></tr>"
    'end if
    response.write                  "</table>"

end sub


SUB ADD_ACTION_V2(varMod,varTaskModRef,varTaskModActionRef,varTaskModActionUser,varTaskDueDate,varTaskPri,varTaskDescription,varTaskModActionEmailTo, var_default_user_on_email, var_pri_text, var_pri_qty, var_pri_type, varTaskStandard, varTaskSignOff, sw7, sw8, sw9, sw10, sw11, sw12, sw13, sw14, sw15, sw16, sw17, sw18, sw19, sw20)


    varTaskModActionUser_org = varTaskModActionUser
  
    SET actobjCommand = Server.CreateObject("ADODB.Command")
    SET actobjCommand.ActiveConnection = objconn


    'Bug fix for SA. Stops double validation!
    if varMod = "SA" Then
        varTaskDescription = varTaskDescription
    else
        varTaskDescription = validateinput(varTaskDescription)
    end if

    if varTaskModActionUser = "Email" then
        if len(var_default_user_on_email) > 0 then 
            varTempTaskModActionUser = var_default_user_on_email  ' assigns the task to the specified default user
        else
            varTempTaskModActionUser = session("YOUR_ACCOUNT_ID") ' assigns the task to that person
        end if


        'check whether the email address added was that of an active user on this contract
        objcommand.commandtext = "select acc_ref from " & Application("DBTABLE_USER_DATA") & "  where corp_code = '" & session("corp_code") & "' and corp_email='" & trim(varTaskModActionEmailTo) & "' and acc_level < 5 and acc_status <> 'D' "
        set TM_chckUsr = objcommand.execute
       

        if not TM_chckUsr.eof then ' one exists so assign it to them instead.
            varTaskModActionUser = TM_chckUsr("acc_ref")            
            varTempTaskModActionUser = varTaskModActionUser
            varTaskModActionUser_org = varTaskModActionUser 
        end if
    else
        varTempTaskModActionUser = varTaskModActionUser
    end if

    if len(varTaskStandard) = 0 then
        varTaskStandard = "0"
    end if

    ' Pretend that no root record to the action has been found
    varRecordfound = "NO"
    varTaskStatus = "2"
   
    if varTaskSignOff = "" or varTaskSignOff = "0"  or varTaskSignOff = false or varTaskModActionUser = session("YOUR_ACCOUNT_ID")  then  
        varTaskSignoff = "1"
    elseif  varTaskSignOff = true or varTaskSignOff = "1" then 
        varTaskSignoff = "0"
    end if

  
    ' Depending on the module, lets get the location info
    SELECT CASE varMod 
   

        ' Risk Assessment
        CASE "RA", "RAR"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_RAMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute
	        
	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("rec_tier5_ref")
		        varTaskTier6 = actobjrs("rec_tier6_ref")

	        END IF

        ' Risk Register
        CASE "RR", "RRR"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_RR_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute
	        
	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF
	
        ' Manual Handling
        CASE "MH", "MHR"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_MHMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORDREFERENCE = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

        ' COSHH
        CASE "CA"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_CA_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ca_ref = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF	
	
        ' COSHH version 2
        CASE "CA2", "CA2R"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_COSHH_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference = '" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF	

        ' Inspection
        CASE "INS"
            actObjcommand.CommandText = "SELECT pcompany, plocation, tier4_ref,  tier5_ref, tier6_ref FROM " & Application("DBTABLE_INS_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND RECORD_REFERENCE='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("pcompany")
		        varTaskLocation = actobjrs("plocation")
		        varTaskDepartment = actobjrs("tier4_ref")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

           ' if session("PREF_TASK_SIGNOFF") = "Y" and varTaskModActionUser <> session("YOUR_ACCOUNT_ID") then
           '     varTaskSignoff = "0"
            'end if
	
        CASE "SA", "SAO"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd,  tier5_ref, tier6_ref FROM " & Application("DBTABLE_SA_AUDIT_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND audit_ref='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

           ' if session("PREF_TASK_SIGNOFF") = "Y" and varTaskModActionUser <> session("YOUR_ACCOUNT_ID") then
           '     varTaskSignOff = "0"
            'end if

        CASE "QA", "QAO"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd,  tier5_ref, tier6_ref FROM " & Application("DBTABLE_QA_AUDIT_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND audit_ref='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

            'if session("PREF_TASK_SIGNOFF") = "Y" and varTaskModActionUser <> session("YOUR_ACCOUNT_ID") then
           '     varTaskSignoff = "0"
           ' end if
	
        CASE "FS", "FSR", "FSO"
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd,  tier5_ref, tier6_ref FROM " & Application("DBTABLE_FSMOD_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

        'Permit to work
        CASE "PTW", "PTWR"
            actObjcommand.CommandText =	"select status, rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref from " & Application("CONFIG_DBASE") & "_PTW.dbo.ptw_v2_entries where corp_code = '" & session("corp_code") & "' AND PTW_REFERENCE = '" & varTaskModRef & "' "
            SET actObjrs = actObjcommand.Execute

            IF not actObjrs.EOF Then
                varRecordfound = "YES"
                varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
                if actObjrs("status") = "3" then
                    varTaskStatus = "0"      
                elseif actObjrs("status") = "2" then
                    varTaskStatus = "1"      
                else
                    varTaskStatus = "2"      
                end if
            end if

          

         CASE "ACC"
            var_acc_reference = split(TaskModRef, "/")
            actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd,  rec_tier5, rec_tier6 FROM " & Application("DBTABLE_ACC_DATA") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND accb_code='" & var_acc_reference(0) & "' and  recordreference='" & var_acc_reference(1) & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("rec_bu")
		        varTaskLocation = actobjrs("rec_bl")
		        varTaskDepartment = actobjrs("rec_bd")
		        varTaskTier5 = actobjrs("rec_tier5")
		        varTaskTier6 = actobjrs("rec_tier6")
	        END IF

          '  if session("PREF_TASK_SIGNOFF") = "Y" and varTaskModActionUser <> session("YOUR_ACCOUNT_ID") then
           '     varTaskSignoff = "0"
            'end if
           
        CASE "HD"
      
            actObjcommand.CommandText = "SELECT up_stor_tier2_ref, up_stor_tier3_ref, up_stor_tier4_ref, up_stor_tier5_ref, up_stor_tier6_ref FROM " & Application("DBTABLE_GLOBAL_FILES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND reference='" & varTaskModRef & "'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("up_stor_tier2_ref")
		        varTaskLocation = actobjrs("up_stor_tier3_ref")
		        varTaskDepartment = actobjrs("up_stor_tier4_ref")
		        varTaskTier5 = actobjrs("up_stor_tier5_ref")
		        varTaskTier6 = actobjrs("up_stor_tier6_ref")
	        END IF
         CASE "LB_A"
            actObjcommand.CommandText = "SELECT tier2_ref, tier3_ref, tier4_ref, tier5_ref, tier6_ref FROM " & Application("DBTABLE_LB_LOGBOOK_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND logbook_ref='" & varTaskModRef & "' AND template_type='LB'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("tier2_ref")
		        varTaskLocation = actobjrs("tier3_ref")
		        varTaskDepartment = actobjrs("tier4_ref")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

            CASE "CL_A"
            actObjcommand.CommandText = "SELECT tier2_ref, tier3_ref, tier4_ref, tier5_ref, tier6_ref FROM " & Application("DBTABLE_LB_LOGBOOK_ENTRIES") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND logbook_ref='" & varTaskModRef & "' AND template_type='CL'"
            SET actObjrs = actObjcommand.Execute

	        IF actObjrs.EOF Then
	        ' oops! no record, strange, best tell the app not to change these values
	        ELSE
	        ' Record found, let the system know
	        varRecordfound = "YES"
	        ' Get the location info
		        varTaskCompany = actobjrs("tier2_ref")
		        varTaskLocation = actobjrs("tier3_ref")
		        varTaskDepartment = actobjrs("tier4_ref")
		        varTaskTier5 = actobjrs("tier5_ref")
		        varTaskTier6 = actobjrs("tier6_ref")
	        END IF

            Case "MSR"
                actObjcommand.CommandText = "SELECT rec_tier2_ref, rec_tier3_ref, rec_tier4_ref, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_MS_DATA_V2") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
                SET actObjrs = actObjcommand.Execute     
                IF actObjrs.EOF Then
	                ' oops! no record, strange, best tell the app not to change these values
	            ELSE
	                ' Record found, let the system know
	                varRecordfound = "YES"
	                ' Get the location info  
                    varTaskCompany = actobjrs("rec_tier2_ref")
		            varTaskLocation = actobjrs("rec_tier3_ref")
		            varTaskDepartment = actobjrs("rec_tier4_ref")
		            varTaskTier5 = actobjrs("rec_tier5_ref")
		            varTaskTier6 = actobjrs("rec_tier6_ref")
                end if

            Case "HAZR"
                actObjcommand.CommandText = "SELECT tier2_ref, tier3_ref, tier4_ref, tier5_ref, tier6_ref FROM " & Application("DBTABLE_HAZ_ENTRIES") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND hazard_ref='" & varTaskModRef & "'"
                SET actObjrs = actObjcommand.Execute     
                IF actObjrs.EOF Then
	                ' oops! no record, strange, best tell the app not to change these values
	            ELSE
	                ' Record found, let the system know
	                varRecordfound = "YES"
	                ' Get the location info  
                    varTaskCompany = actobjrs("tier2_ref")
		            varTaskLocation = actobjrs("tier3_ref")
		            varTaskDepartment = actobjrs("tier4_ref")
		            varTaskTier5 = actobjrs("tier5_ref")
		            varTaskTier6 = actobjrs("tier6_ref")
                end if
            Case "PR","PRR"
                actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, tier5_ref, tier6_ref FROM " & Application("DBTABLE_PUWER_ENTRIES") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
                SET actObjrs = actObjcommand.Execute     
                IF actObjrs.EOF Then
	                ' oops! no record, strange, best tell the app not to change these values
	            ELSE
	                ' Record found, let the system know
	                varRecordfound = "YES"
	                ' Get the location info  
                    varTaskCompany = actobjrs("rec_bu")
		            varTaskLocation = actobjrs("rec_bl")
		            varTaskDepartment = actobjrs("rec_bd")
		            varTaskTier5 = actobjrs("tier5_ref")
		            varTaskTier6 = actobjrs("tier6_ref")
                end if

            Case "HPR"
                    actObjcommand.CommandText = "SELECT rec_bu, rec_bl, rec_bd, rec_tier5_ref, rec_tier6_ref FROM " & Application("DBTABLE_HP_DATA") &  " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND recordreference='" & varTaskModRef & "'"
                    SET actObjrs = actObjcommand.Execute     
                    IF actObjrs.EOF Then
	                    ' oops! no record, strange, best tell the app not to change these values
	                ELSE
	                    ' Record found, let the system know
	                    varRecordfound = "YES"
	                    ' Get the location info  
                        varTaskCompany = actobjrs("rec_bu")
		                varTaskLocation = actobjrs("rec_bl")
		                varTaskDepartment = actobjrs("rec_bd")
		                varTaskTier5 = actobjrs("rec_tier5_ref")
		                varTaskTier6 = actobjrs("rec_tier6_ref")
                    End If

    END SELECT




    ' Check!, does the action already exsist
    if varmod = "PTWR" then
        actObjcommand.CommandText = "SELECT id, task_due_date, task_account, task_status FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "' AND TASK_ACCOUNT = '" & varTempTaskModActionUser & "'"
    else
        actObjcommand.CommandText = "SELECT id, task_due_date, task_account, task_status FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
    
    response.write actObjCommand.commandtext
    'asda
    end if
    SET actObjrs = actObjcommand.Execute

    IF actObjrs.eof Then
        ' No Action, do an insert
        actObjcommand.CommandText =	"INSERT INTO " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
							        "(corp_code," & _
							        "gendatetime," & _
							        "task_due_date," & _
							        "task_account," & _
							        "task_module," & _
							        "task_description," & _
							        "task_mod_ref," & _
							        "task_mod_action_ref," & _
							        "task_alert_date," & _
							        "task_by," & _
							        "task_status," & _
							        "task_lastmodified," & _
							        "task_lastmodified_by," & _
							        "task_mod_pri," & _
                                    "email_to," & _
                                    "org_task_pri_text, " & _
                                    "org_task_pri_qty, " & _
                                    "org_task_pri_unit, " & _   
                                    "task_standard, " & _  
                                    "task_signoff " & _                               
                                    ") " & _
							        "VALUES ('" & Session("CORP_CODE") & "','" & date() & " " & time() & "','" & varTaskDueDate & "','" & varTempTaskModActionUser & "','" & varMod & "','" & varTaskDescription & "','" & varTaskModRef & "','" & varTaskModActionRef & "','" & varTaskDueDate & "','" & Session("YOUR_ACCOUNT_ID") & "','" & varTaskStatus & "','" & Date() & " " & time() & "','" & Session("YOUR_ACCOUNT_ID") & "','" & varTaskpri & "', '" & varTaskModActionEmailTo & "', '" & var_pri_text & "', '" & var_pri_qty & "', '" & var_pri_type & "', '" & varTaskStandard & "', '" & varTaskSignoff & "')"

        actObjcommand.Execute


        'Now get the task ID for later processing
       
        actObjcommand.CommandText = "SELECT top 1 id FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MODULE = '" & varMod & "' AND TASK_MOD_REF = '" & varTaskModRef & "' order by id desc"
        SET actObjrs = actObjcommand.Execute
        
        dbTaskid = actObjrs("id")
        
	Else


	    ' Action is there, ok now lets check if some of the key info has changed
	    dbTaskModActionUser = actObjrs("task_account")
	    dbTaskDueDate = actObjrs("task_due_date")
	    dbTaskStatus = actObjrs("task_status")
	    dbTaskid = actObjrs("id")
	    
    'fix for safety audit tasks. It wasn't retaining the original task status CG
        if varMod = "SA" or varMod = "INS" then
         varTaskStatus = dbTaskStatus
        end if

	    if varTempTaskModActionUser <> dbTaskModActionUser then
	        objCommand.commandText = "SELECT per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " " & _
	                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
	                                 "  AND acc_ref = '" & varTempTaskModActionUser & "'"
	        set actObjRsUser = objCommand.execute
	        if not actObjRsUser.EOF then
	            task_history_text = actObjRsUser("per_fname") & " " & actObjRsUser("per_sname")
	        else
	            task_history_text = "Unknown"
	        end if
	        set actObjRsUser = nothing
	        
	        task_history_text = "Task delegated to " & task_history_text
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if
	    
	    if varTaskDueDate <> dbTaskDueDate then
	        if varMod = "PTW" and varTaskDueDate > dbTaskDueDate then
	            task_history_text = "Permit to Work Extended until " & varTaskDueDate
	        else
	            task_history_text = "Task due date changed from " & dbTaskDueDate & " to " & varTaskDueDate
	        end if
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if
	    
	    if varTaskStatus <> dbTaskStatus and varMod <> "SA" then
	    
	        select case varTaskStatus
	            case "0"
	                varTaskStatusText = "Complete"
	            case "1"
	                varTaskStatusText = "Active"
	            case "2"
	                varTaskStatusText = "Pending"
	        end select
	        select case dbTaskStatus
	            case "0"
	                dbTaskStatusText = "Complete"
	            case "1"
	                dbTaskStatusText = "Active"
	            case "2"
	                dbTaskStatusText = "Pending"
	        end select
	    
	        task_history_text = "Task changed from " & dbTaskStatusText & " to " & varTaskStatusText
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
	    end if


	    IF varTempTaskModActionUser <> dbTaskModActionUser Then
	        ' User is not the same so we need to move the action to another user , but mark it as pending.
	        actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
								        "Task_due_date = '" & varTaskDueDate & "', " & _
								        "Task_lastmodified = '" & Date() & "', " & _
								        "Task_lastmodified_by = '" & Session("YOUR_ACCOUNT_ID") & "', " & _
								        "Task_account = '" & varTempTaskModActionUser & "', " & _
								        "Task_mod_pri = '" & varTaskPri & "', " & _
								        "Task_description = '" & varTaskDescription & "', " & _
								        "Task_status = '" & varTaskStatus & "', " & _ 
                                        "email_to = '" & varTaskModActionEmailTo & "', " & _ 
                                        "org_task_pri_text = '" & var_pri_text & "', " & _
                                        "org_task_pri_qty = '" & var_pri_qty & "', " & _
                                        "org_task_pri_unit = '" & var_pri_type & "', " & _
                                        "Task_standard = '" & varTaskStandard & "', " & _
                                        "Task_signoff = '" & varTaskSignoff & "' " & _
								        "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_ACCOUNT= '" & dbTaskModActionUser & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
	        actObjcommand.Execute

	    ELSE
		    ' User is the same, just update the action with raw details
		    actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
									    "Task_due_date = '" & varTaskDueDate & "', " & _
									    "Task_lastmodified = '" & Date() & "', " & _
									    "Task_lastmodified_by = '" & Session("YOUR_ACCOUNT_ID") & "', " & _
									    "Task_mod_pri = '" & varTaskPri & "', " & _
									    "Task_description = '" & varTaskDescription & "', " & _
									    "Task_status = '" & varTaskStatus & "', " & _ 
                                        "email_to = '" & varTaskModActionEmailTo & "', " & _ 
                                        "org_task_pri_text = '" & var_pri_text & "', " & _
                                        "org_task_pri_qty = '" & var_pri_qty & "', " & _
                                        "org_task_pri_unit = '" & var_pri_type & "', " & _
                                        "Task_standard = '" & varTaskStandard & "', " & _
                                        "Task_signoff = '" & varTaskSignoff & "' " & _
									    "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_ACCOUNT= '" & dbTaskModActionUser & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
		   
            actObjcommand.Execute
        End IF
    END IF

    IF varRecordfound = "YES" Then
	    ' ok update the action with the latest location info
	    actObjcommand.Commandtext =	"UPDATE " & Application("DBTABLE_TASK_MANAGEMENT") & " SET " & _
								    "Task_company = '" & varTaskCompany & "', " & _
								    "Task_location = '" & varTaskLocation & "', " & _
								    "Task_department = '" & varTaskdepartment & "', " & _ 
								    "Task_tier5 = '" & varTaskTier5 & "', " & _
								    "Task_tier6 = '" & varTaskTier6 & "' " & _
								    "WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND TASK_MOD_REF = '" & varTaskModRef & "' AND TASK_MOD_ACTION_REF = '" & varTaskModActionRef & "'"
	    actObjcommand.Execute

       
    END IF


    '************************************************************************
    '     Send the task to the recipient via email, in addition to adding 
    '     to task manager. Added for Ocado to notify users when tasks added
    '     Don't send the email if the recipient is the person issuing
    '************************************************************************


    if session("task_email_instanotification") = "Y" and Session("corp_code") = "336872"  then
        
        objCommand.commandText = "SELECT corp_email FROM " & Application("DBTABLE_USER_DATA") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND acc_ref = '" & varTempTaskModActionUser & "' " & _
                                 " AND acc_ref <> '" & Session("YOUR_ACCOUNT_ID") & "' " & _ 
                                 "  AND (corp_email IS NOT NULL AND corp_email <> '')"
    'response.write objcommand.commandtext
    'response.end    
    set objRs_email = objCommand.execute
        if not objRs_email.EOF then

            emailTo = objRs_email("corp_email")

            emailSubject = "AssessNET: A new action has been assigned to you for completion"

            emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
                             "<br />" & _
                             "<p>Dear Sir / Madam,</p>" & _
                             "<p>The following task has been assigned to you by " & session("YOUR_FULLNAME")  & ".</p>" & _
                             "<p>Task reference: " & dbTaskid & "<br />" & varTaskDescription & "</p>" & _
                             "<p>The due date of the task is: <strong>" & varTaskDueDate & "</strong></p>" & _
                             "<p>To view more information regarding this task, please log into AssessNET and view your task list.</p>" & _
                             "<p>Kind regards,</p>" & _
                             "<p>The AssessNET Team</p>" & _
                         "</BODY></HTML>"

            if session("corp_code") = "000010" then
                call sendEmail("aiptemp@parliament.uk", "task-service@assessnet.co.uk", emailSubject, emailBody)
            else
                emailTo = emailTo
                call sendEmail(emailTo, "task-service@assessnet.co.uk", emailSubject, emailBody)
            end if
            task_history_text = "Task emailed to: " & emailTo
        
	        call ADD_TASK_ACTION(dbTaskid, task_history_text)

        end if
        objRs_email.close()
    end if



'    '************************************************************************
'    '     Send the task to the recipient via email, in addition to adding 
'    '     to task manager if the task is due within the next 7 days
'    '     Don't send the email if the recipient is the person issuing
'    '************************************************************************
'
'    var_current_date_add7 = dateadd("d", 7, date())
'
'    if cdate(var_current_date_add7) >= cdate(varTaskDueDate) then
'        
'        objCommand.commandText = "SELECT corp_email FROM " & Application("DBTABLE_USER_DATA") & " " & _
'                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
'                                 "  AND acc_ref = '" & varTempTaskModActionUser & "' " & _
'                                 "  AND (corp_email IS NOT NULL AND corp_email <> '')"
'        set objRs_email = objCommand.execute
'        if not objRs_email.EOF then
'
'            emailTo = objRs_email("corp_email")
'
'            emailSubject = "AssessNET: A new action has been assigned to you for completion within the next 7 days"
'
'            emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
'                             "<br />" & _
'                             "<p>Dear Sir / Madam,</p>" & _
'                             "<p>The following task has been assigned to you by " & session("YOUR_FULLNAME")  & ".</p>" & _
'                             "<p>" & varTaskDescription & "</p>" & _
'                             "<p>The due date of the task is: <strong>" & varTaskDueDate & "</strong></p>" & _
'                             "<p>To view more information regarding this task, please log into AssessNET and view your task list.</p>" & _
'                             "<p>Kind regards,</p>" & _
'                             "<p>The AssessNET Team</p>" & _
'                         "</BODY></HTML>"
'
'            if session("corp_code") = "000010" then
'                call sendEmail("aiptemp@parliament.uk", "task-service@assessnet.co.uk", emailSubject, emailBody)
'            else
'                emailTo = "m.green@assessnet.co.uk"
'                call sendEmail(emailTo, "task-service@assessnet.co.uk", emailSubject, emailBody)
'            end if
'            task_history_text = "Task emailed to: " & varEmailTo
'        
'	        call ADD_TASK_ACTION(dbTaskid, task_history_text)
'
'        end if
'        objRs_email.close()
'    end if




    '************************************************************************
    '     Send the task to an external email address
    '************************************************************************

    if varTaskModActionUser_org = "Email" then
        'send the original user the email and add some history    
        
        objCommand.commandText = "SELECT tier2.struct_name AS tier2_name, tier3.struct_name AS tier3_name, tier4.struct_name AS tier4_name " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER2") & " AS tier2 " & _
                                 "LEFT JOIN " & Application("DBTABLE_STRUCT_TIER3") & " AS tier3 " & _
                                 "  ON tier2.corp_code = tier3.corp_code " & _
                                 "  AND tier2.tier2_ref = tier3.tier2_ref " & _
                                 "LEFT JOIN " & Application("DBTABLE_STRUCT_TIER4") & " AS tier4 " & _
                                 "  ON tier3.corp_code = tier4.corp_code " & _
                                 "  AND tier3.tier3_ref = tier4.tier3_ref " & _
                                 "WHERE tier2.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND tier2.tier2_ref = '" & varTaskCompany & "' " & _
                                 "  AND tier3.tier3_ref = '" & varTaskLocation & "' " & _
                                 "  AND tier4.tier4_ref = '" & varTaskdepartment & "'"
        set objRsTier = objCommand.execute
        if not objRsTier.EOF then
            varTaskCompany_name = objRsTier("tier2_name")
            varTaskLocation_name = objRsTier("tier3_name")
            varTaskDepartment_name = objRsTier("tier4_name")

            varTask_descriptor = "<p>The task relates to the following area: <br />" & varTaskCompany_name & " > " & varTaskLocation_name & " > " & varTaskDepartment_name & "</p>"
        else
            varTask_descriptor = ""
        end if
        set objRsTier = nothing


        emailBody =  "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN""><BODY>" & _
                                "<br />" & _
                                "<p>Dear Sir / Madam,</p>" & _
                                "<p>The following task has been assigned to your email address by " & session("YOUR_FULLNAME")  & ".</p>" & _
                                "<p>" & varTaskDescription & "</p>" & _
                                varTask_descriptor & _
                                "<p>The due date of the task is: <strong>" & varTaskDueDate & "</strong></p>" & _
                                "<p>Please report any progress with this task to: <strong>" & session("YOUR_FULLNAME")  & "</strong>.</p>" & _
                                "<p>Kind regards,</p>" & _
                                "<p>The AssessNET Team</p>" & _
                                "</BODY></HTML>"
                   
        
        
        if session("corp_code") = "000010" then                      
            call sendEmail("aiptemp@parliament.uk","noreply@assessnet.co.uk","New Task Entered",emailBody)
        else
            call sendEmail(varTaskModActionEmailTo,"noreply@assessnet.co.uk","New Task Entered",emailBody)
        end if
        task_history_text = "Task emailed to: " & varTaskModActionEmailTo
        
	    call ADD_TASK_ACTION(dbTaskid, task_history_text)

    else

    
        'TODO - Add code that checks a preference and sees if this falls under what is considered high priority
        'TODO - If it is then email that user!
        'TODO - ADD_TASK_ACTION to say that it's been emailed

    end if
   
END SUB


sub DisplayUSERInfoV2(sub_user_value, sub_user_name, disabled_flag, type_switch, javascript_functions, show_disabled, hide_users, multi_select, var_tier2_id, var_tier3_id, var_tier4_id, sub_sw4)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'disabled_flag = Disabled
    'type_switch = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine)
    'javascript_functions = Javascript
    'show_disabled = Show disabled users
    'hide_users = if instr hide these users: (TM - Task manager, RO - Read Only)
    'multi_select = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox
    'var_tier2_id = used when the structure roles are active
    'var_tier3_id = used when the structure roles are active
    'var_tier4_id = used when the structure roles are active
    'sub_sw4 not used

    if len(multi_select) > 0 then
        var_multiple = " size='" & multi_select & "' multiple" 
    else
        var_multiple = " size='1'"
    end if
   
    Response.Write  "<select name='" & sub_user_name & "' id='" & sub_user_name & "' class='select' " & disabled_flag & " " & javascript_functions & var_multiple & ">"
                    
                    select case type_switch 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Please select a user</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"

                                        if session("pref_task_email") = "Y" then
                                            response.Write  "<option value='Email' "
                                            If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                            response.write ">- Email to a user</option>"
                                        end if 

                        case "N"
                            'do nothing                            
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if
                                                        
                        case else
                               response.Write    "<option value='0'>Please select a user</option>"        
                               if session("pref_task_email") = "Y" then
                                    response.Write  "<option value='Email' "
                                    If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                    response.write ">- Email to a user</option>"
                                end if 
                    end select 


                      if session("pref_task_structure_roles") = "Y" then 
                            'Grab the roles for the group, company, location and department and enter them at the top.
                            'Don't worry about selecting them because it's just going to contain the reference to the user which will get picked up normally anyway

                            'Group Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=1 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='Group Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                            'Company Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                  
                                                  " and b.tier_ref = '" & var_tier2_id & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=2   ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER2") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                             'Location Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                
                                                  " and b.tier_ref = '" & var_tier3_id & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=3 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER3") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                            'Department Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                
                                                  " and b.tier_ref = '" & var_tier4_id & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=4 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER4") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if
                            

                      end if
                   

	    objCommand.Commandtext = "SELECT data.Acc_Ref, display_name  as user_details FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                 "  on data.corp_code = tier4.corp_code " & _
                                 "  and CAST(data.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) "

        if type_switch = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            'do nothing
        elseif type_switch = "ST" and session("YOUR_ACCESS") <> "0" then
            objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                              "  ON data.corp_code = org.corp_code " & _
                                                              "  AND data.acc_ref = org.user_acc_ref "
        end if

        objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


        if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
            objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
        end if
	
	    if type_switch = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
        elseif type_switch = "ST" and session("YOUR_ACCESS") <> "0" then
	        objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	    end if
                    
                    if show_disabled = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if 
                    
                    if instr(hide_users, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(hide_users, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                    
          objCommand.Commandtext = objCommand.Commandtext & "  ORDER BY data.Per_sname ASC"
	
    SET objRsuser = objCommand.Execute

    if not objRsuser.eof then 
      if session("pref_task_structure_roles") = "Y" then response.write "<optgroup label='Users'>"
     
   	    While NOT objRsuser.EOF
            Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
            'If cstr(objRsuser("Acc_ref")) = cstr(sub_user_value) Then
            If instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
                'response.Write "..." & cstr(objRsuser("Acc_ref")) & "..." & cstr(sub_user_value) & "...<br />"
                Response.Write " selected"
            End if
            Response.Write ">" & objRsuser("user_details") & "</option>"
        
            objRsuser.movenext
	    Wend
        if session("pref_task_structure_roles") = "Y" then response.write "</optgroup>"
    end if
    response.Write "</select>"
end sub



sub DisplayUSERInfo_locationV2(sub_user_value, sub_user_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5, sub_sw6, sub_sw7, sub_sw8, sub_sw9, sub_sw10, sub_sw11, sub_sw12)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine)
    'sub_sw3 = Javascript
    'sub_sw4 = Show disabled users
    'sub_sw5 = if instr hide these users: (TM - Task manager, RO - Read Only)
    'sub_sw6 = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox
    'sub_sw7 = company
    'sub_sw8 = location
    'sub_sw9 = department
    'sub_sw10 = UNUSED
    'sub_sw11 = UNUSED
    'sub_sw12 = UNUSED
    
    if len(sub_sw6) > 0 then
        var_multiple = " size='" & sub_sw6 & "' multiple" 
    else
        var_multiple = " size='1'"
    end if
   
    Response.Write  "<select name='" & sub_user_name & "' class='select' " & sub_sw1 & " " & sub_sw3 & var_multiple & ">"
                    
                    select case sub_sw2 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Select person</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"

                                        
                                        if session("pref_task_email") = "Y" then
                                            response.Write  "<option value='Email' "
                                            If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                            response.write ">- Email to a user</option>"
                                        end if 

                        case "N"
                            'do nothing
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if
                                                        
                        case else
                            response.Write    "<option value='0'>Please select a user</option>"  
                              
                            if session("pref_task_email") = "Y" then
                                response.Write  "<option value='Email' "
                                If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                response.write ">- Email to a user</option>"
                            end if 
   
                    end select 



                    if session("pref_task_structure_roles") = "Y" then 
                            'Grab the roles for the group, company, location and department and enter them at the top.
                            'Don't worry about selecting them because it's just going to contain the reference to the user which will get picked up normally anyway

                            'Group Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=1 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='Group Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                            'Company Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                  
                                                  " and b.tier_ref = '" & sub_sw7 & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=2   ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER2") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                             'Location Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                
                                                  " and b.tier_ref = '" & sub_sw8 & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=3 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER3") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if

                            'Department Roles
                            objcommand.commandtext = "SELECT ref, name, acc_ref FROM " & Application("DBTABLE_STRUCTURE_ROLES") & " as a " & _  
                                                     "inner join " & Application("DBTABLE_HR_USER_STRUCTURE_ROLE") & " as b  " & _
                                                  " on a.corp_code = b.corp_code " & _
                                                  " and a.ref = b.role_ref " & _
                                                  " and b.tier_level = a.tier " & _                                                
                                                  " and b.tier_ref = '" & sub_sw9 & "' " & _
                                                  "WHERE a.corp_code='" & session("CORP_CODE") & "' and a.tier=4 ORDER BY position ASC"
                  
                             set mystructrole = objcommand.execute

                             if not mystructrole.eof then 
                           
                                'if some then lets write them out 
                                response.write "<optgroup label='" & session("CORP_TIER4") & " Roles'>"
            
                                while not mystructrole.eof
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 

                                   mystructrole.movenext 
                                wend 
                                response.write "</optgroup>"

                             end if
                            

                      end if

                   

	    objCommand.Commandtext = "SELECT data.Acc_Ref, display_name  as user_details, 0 AS queryOrder FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                 "  on data.corp_code = tier4.corp_code " & _
                                 "  and CAST(data.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) "

        if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            'do nothing
        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
            objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                              "  ON data.corp_code = org.corp_code " & _
                                                              "  AND data.acc_ref = org.user_acc_ref "
        end if

        objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


        if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
            objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
        end if
	
	    if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
	        objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	    end if
                    
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if 
                    
                    if instr(sub_sw5, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(sub_sw5, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                    
        
        if len(sub_sw7) > 0 and sub_sw7 <> "0" then
            objCommand.commandText = objCommand.commandText & " AND data.corp_bu = '" & sub_sw7 & "' " & _
                                                              " AND data.acc_level <> '0' "
        end if
        if len(sub_sw8) > 0 and sub_sw8 <> "0" then
            objCommand.commandText = objCommand.commandText & " AND data.corp_bl = '" & sub_sw8 & "' " & _
                                                              " AND data.acc_level <> '0' "
        end if


        
        
        if len(sub_sw7) > 0 or len(sub_sw8) > 0 then
            objCommand.commandText = objCommand.Commandtext & " UNION SELECT data.Acc_Ref, display_name  as user_details, 1 AS queryOrder FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                                 "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3") & "  AS tier3 " & _
                                     "  on data.corp_code = tier3.corp_code " & _
                                     "  and CAST(data.corp_bl AS nvarchar(50)) = CAST(tier3.tier3_ref AS nvarchar(50)) "
    
            if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                'do nothing
            elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
                objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                                  "  ON data.corp_code = org.corp_code " & _
                                                                  "  AND data.acc_ref = org.user_acc_ref "
            end if

            objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


            if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
            end if
    	
	        if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
            elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
	            objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	        end if
                    
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if  
            
            objCommand.commandText = objCommand.commandText & " AND data.acc_level = '0' "   
            
            objCommand.commandText = objCommand.commandText & " ORDER BY queryOrder ASC, user_details ASC"
            
        else
            objCommand.Commandtext = objCommand.Commandtext & " ORDER BY user_details ASC"
        end if
        
        


'response.Write "<br /><br />" & objcommand.commandtext 
'response.end 
        
	SET objRsuser = objCommand.Execute
	
	var_list_order = ""
	
   	While NOT objRsuser.EOF
   	    
   	    if var_list_order <> objRsuser("queryOrder") then
   	        if var_list_order <> "" or (var_list_order = "" and objRsuser("queryOrder") = "1") then
   	            response.Write "<option value='0'> </option><option value='0'> ---- Corporate Administrators ---- </option>"
   	        end if
   	        var_list_order = objRsuser("queryOrder")
   	    end if
   	    
        Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
        'If cstr(objRsuser("Acc_ref")) = cstr(sub_user_value) Then
        If instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
            'response.Write "..." & cstr(objRsuser("Acc_ref")) & "..." & cstr(sub_user_value) & "...<br />"
            Response.Write " selected"
        End if
        Response.Write ">" & objRsuser("user_details") & "</option>"
        
        objRsuser.movenext
	Wend
    response.Write "</select>"
end sub



sub DisplayUSERInfoV3(sub_user_value, sub_user_name, disabled_flag, type_switch, javascript_functions, show_disabled, hide_users, multi_select, var_tier2_id, var_tier3_id, var_tier4_id, var_tier5_id, var_tier6_id, var_module, var_accb_code, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'disabled_flag = Disabled
    'type_switch = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine) (RV is for Review Sections)
    'javascript_functions = Javascript
    'show_disabled = Show disabled users
    'hide_users = if instr hide these users: (TM - Task manager, RO - Read Only,)
    'multi_select = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox
    'var_tier2_id = used when the structure roles are active
    'var_tier3_id = used when the structure roles are active
    'var_tier4_id = used when the structure roles are active
    'var_tier5_id = used when the structure roles are active
    'var_tier6_id = used when the structure roles are active
    'var_module = If not blank then used to filter the list to users who have access to that module
    'var_accb_code = if var_module is acc then use this to filter access to that accb
    'sub_sw2 - 5 not used

    if len(multi_select) > 0 then
        var_multiple = " size='" & multi_select & "' multiple" 
    else
        var_multiple = " size='1'"
    end if

    Response.Write  "<select name='" & sub_user_name & "' id='" & sub_user_name & "' class='select' " & disabled_flag & " " & javascript_functions & var_multiple & ">"
                    
                    select case type_switch 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Please select a user</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"

                                        if session("pref_task_email") = "Y" and (var_module <> "INS" and var_module <> "SAO" and var_module <> "QAO" and var_module <> "FSO" and instr(1, var_module, "AP") = 0) then
                                            response.Write  "<option value='Email' "
                                            If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                            response.write ">- Email to a user</option>"
                                        end if 

                        case "N"
                            'do nothing                            
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if

                        case "RV"
                            response.Write    "<option value='0'>Please select a reviewer</option>"        
                            if   var_module = "RA" then
                                response.Write    "<option value='nreq'"
                                if cstr(sub_user_value) = "nreq" then response.Write    " selected " 
                                response.Write    ">One off assessment</option>"  
                            end if                             
                        
                        case else
                               response.Write    "<option value='0'>Please select a user</option>"        
                               
                               if session("pref_task_email") = "Y" and (var_module <> "INS" and var_module <> "SAO" and var_module <> "QAO" and var_module <> "FSO" and instr(1, var_module, "AP") = 0) then
                                    response.Write  "<option value='Email' "
                                    If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                    response.write ">- Email to a user</option>"
                                end if 
                    end select 

        'don't need to run any calculations - just pump out the details for whomevers selected
        if disabled_flag = "disabled" then 

            objCommand.Commandtext = "SELECT distinct data.Acc_Ref, display_name  as user_details FROM " & Application("DBTABLE_USER_DATA") & " as data  " & _
                                 " where data.corp_code='" & Session("corp_code") & "' AND data.Acc_level NOT LIKE '5' and acc_ref = '" & sub_user_value & "' " 

            set objUsr = objcommand.execute

            if not objUsr.eof then
                response.write "<option selected>" & objUsr("user_details") & "</option>"
            end if
        else

        if ubound(global_user_v3_array,2) > 0 then
            'grab the first group name
            var_grp_name = global_user_v3_array(0,1)
            if len(var_grp_name) > 0 then  response.write "<optgroup label='" & var_grp_name & "'>"
            'loop through the array and populate the fields
            for i = 1 to ubound(global_user_v3_array,2)
                if var_grp_name <> global_user_v3_array(0,i) then  
                    var_grp_name = global_user_v3_array(0,i) 
                  response.write "</optgroup><optgroup label='" & var_grp_name & "'>"
                end if
                response.write "<option value='" & global_user_v3_array(1,i) & "' "
                if global_user_v3_array(3,i) = "N" and global_user_v3_array(1,i) = sub_user_value then response.write " selected "
                response.write ">" & global_user_v3_array(2,i) & "</option>"
                
            next
            if len(var_grp_name) > 0 then  response.write "</optgroup>"
        
        else 
         
                if session("pref_task_structure_roles") = "Y" then  ' Start of grab roles
                        'Grab the roles and enter them at the top.
                        'Don't worry about selecting them because it's just going to contain the reference to the user which will get picked up normally anyway


                        'error correction
                        if len(var_tier2_id) = 0 or isnull(var_tier2_id) then var_tier2_id = 0
                        if len(var_tier3_id) = 0 or isnull(var_tier3_id) then var_tier3_id = 0
                        if len(var_tier4_id) = 0 or isnull(var_tier4_id) then var_tier4_id = 0
                        if len(var_tier5_id) = 0 or isnull(var_tier5_id) then var_tier5_id = 0
                        if len(var_tier6_id) = 0 or isnull(var_tier6_id) then var_tier6_id = 0

                        objcommand.commandtext = "exec module_hr.dbo.sp_Return_Roles_for_Structure '" & session("corp_code") & "','" & var_tier2_id & "','" & var_tier3_id & "','" & var_tier4_id & "','" & var_tier5_id & "','" & var_tier6_id & "'"
                      '  response.write  objcommand.commandtext
                        set mystructrole = objcommand.execute
                            
                        if not mystructrole.eof then 
                            var_grp_name = returnRoleNameforLevel(mystructrole("level"))      
                                                   
                            if len(var_grp_name) > 0 then  response.write "<optgroup label='" & var_grp_name & "'>"

                            while not mystructrole.eof 
                                'if some then lets write them out 
                                    if var_grp_name <> returnRoleNameforLevel(mystructrole("level")) then      
                                    var_grp_name = returnRoleNameforLevel(mystructrole("level"))                                    
                                    response.write "</optgroup><optgroup label='" & var_grp_name & "'>"
                                end if     
                               
                                    Response.Write "<option value='" & mystructrole("acc_Ref") & "' >" & mystructrole("name") & "</option>" 
                                    var_cur_num =  ubound(global_user_v3_array,2)+1
                                    redim preserve global_user_v3_array(3,var_cur_num)
                                    global_user_v3_array(0,var_cur_num) = "Group Roles"
                                    global_user_v3_array(1,var_cur_num) = mystructrole("acc_Ref")
                                    global_user_v3_array(2,var_cur_num) = mystructrole("name")
                                    global_user_v3_array(3,var_cur_num) = "Y"
                                    mystructrole.movenext 
                                
                                    
                            wend
                            if len(var_grp_name) > 0 then  response.write "</optgroup>"
    
                        end if


                end if ' End of grab roles
                   
        
        
                '*********************************** Build gen SQL *********************************************  
                comText = ""
                if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                    comText = comText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
                end if
	
	            if type_switch = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                   comText = comText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
                elseif type_switch = "ST" and session("YOUR_ACCESS") <> "0" then
	                comText = comText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	            end if
                                    
                if instr(hide_users, "RO") then
                    comText = comText & " AND data.Acc_level NOT LIKE '3' "
                end if      
                    
                if instr(hide_users, "TM") then
                    comText = comText & " AND data.Acc_level NOT LIKE '4' "
                end if


                '*********************************** End of build gen SQL ********************************************* 
            if var_module = "ACC" then 
                specComText = " "
                        
                specComText = specComText & " and data.Acc_status not like 'D' "

               specComText = specComText & " and data.acc_ref  in (select user_ref from " & Application("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code = data.corp_code and accb_code = '" & var_accb_code & "' and user_ref = data.acc_ref ) "
             
                call writeUserGroupForSQL("Users with access to this Incident Centre", comText & specComText, sub_user_value, var_module)
                specComText = " "
                
                specComText = specComText & " and data.Acc_status not like 'D' "

                specComText = specComText & " and data.acc_ref not  in (select user_ref from " & Application("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code = data.corp_code and accb_code = '" & var_accb_code & "' and user_ref = data.acc_ref ) "

                call writeUserGroupForSQL("Users without access to this Incident Centre", comText & specComText, sub_user_value, var_module)
            else 'Start of non accident modules
               if session("global_usr_perm") = "Y" then
                        if var_tier6_id > 0 then
                         '*********************************** Build specific SQL to grab all users with tier 6 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 

                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier6_ref = '" & var_tier6_id & "') "

                            call writeUserGroupForSQL(session("CORP_TIER6") & " Users", comText & specComText, sub_user_value, var_module)
                            
                            
                        '*********************************** End of build specific SQL to grab all users with tier 6 access *********************************************  
                        end if
    
                        if var_tier5_id > 0 then
                        '*********************************** Build specific SQL to grab all users with tier 5 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER5") & " Users", comText & specComText, sub_user_value, var_module)
                            
                          
                        '*********************************** End of build specific SQL to grab all users with tier 5 access *********************************************  
                        end if
    
                        if var_tier4_id > 0 then
                         '*********************************** Build specific SQL to grab all users with tier 4 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier4_ref = '" & var_tier4_id & "' and tier5_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER4") & " Users", comText & specComText, sub_user_value, var_module)
    
                            
                        '*********************************** End of build specific SQL to grab all users with tier 4 access *********************************************  
                        end if
    
                        if var_tier3_id > 0 then
                        '*********************************** Build specific SQL to grab all users with tier 3 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier3_ref = '" & var_tier3_id & "' and tier4_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER3") & " Users", comText & specComText, sub_user_value, var_module)
    
                            
                        '*********************************** End of build specific SQL to grab all users with tier 3 access *********************************************  
                         end if
    
                        if var_tier2_id > 0 then
                          '*********************************** Build specific SQL to grab all users with tier 2 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "' and tier3_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER2") & " Users", comText & specComText, sub_user_value, var_module)
    
                           
                        '*********************************** End of build specific SQL to grab all users with tier 2 access *********************************************    
                        end if

            end if
                         
    
                         '*********************************** Build specific SQL to grab all users with GA access *********************************************  
                         if session("PreventNonGAsFromAssigningTasksToGAs") <> "Y" or session("YOUR_ACCESS") = "0" then 
                            specComText = " "
            
       
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level = 0 "

                            call writeUserGroupForSQL("Global Admins", comText & specComText, sub_user_value, var_module)
    
                           
                        end if
                        '*********************************** End of build specific SQL to grab all users with GA access *********************************************    


                        if (session("TM_Hide_Other_Users") <> "Y" or var_module = "TM")  or (var_module = "PER") then 
                        '*********************************** Build specific SQL to grab all users with module access *********************************************  
       
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level <> 0 "

                            if session("global_usr_perm") = "Y"  and var_tier2_id <> "0"   then
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "' and tier3_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "' and tier4_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'   and tier4_ref = '" & var_tier4_id & "' and tier5_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'  and tier4_ref = '" & var_tier4_id & "'  and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'  and tier4_ref = '" & var_tier4_id & "'  and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '" & var_tier6_id & "') "
                                header_text = "Other users with access to this module"
                            else
                                header_text = "Users with access to this module"
                            end if

                            if len(var_module) > 0  then
                                call writeUserGroupForSQL(header_text, comText & specComText, sub_user_value, var_module)
                            else
                                call writeUserGroupForSQL("Other users without access restrictions", comText & specComText, sub_user_value, var_module)
                            end if 
    
                       
                        '*********************************** End of build specific SQL to grab all users with module access *********************************************    
                        end if 


                        if (session("TM_Hide_Users_WO_Access") <> "Y"  and var_module <> "TM") or (var_module = "PER")   then 
                         '*********************************** Build specific SQL to grab all users without module access *********************************************  
                          if len(var_module) > 0 and type_switch <> "RV" then
                            specComText = " "
                            select case var_module
                                case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '0' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '0' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '0' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '0' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '0' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '0' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '0' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '0' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '0' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '0' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '0' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '0' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '0' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '0' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level <> 0 "

                            call writeUserGroupForSQL("Users without access to this module", comText & specComText, sub_user_value, var_module)
    
                            
                        end if
                     '*********************************** End of build specific SQL to grab all users with module access *********************************************  
                 end if  
                
          end if 'end of non accident modules  
          
         
            if show_disabled <> "N" then
          
                '*********************************** Build specific SQL to grab all disabled users *********************************************  
                specComText = " "
      
                specComText = specComText &  " and data.Acc_status  like 'D' "

                call writeUserGroupForSQL("Disabled Users", comText & specComText, sub_user_value, var_module)

                '*********************************** End of build specific SQL to grab all disabled users *********************************************   
            end if 


      end if 'end of don't need to run any calculations - just pump out the details for whomevers selected
      
    end if
    response.Write "</select>"
end sub





sub DisplayUSERInfoV4(sub_user_value, sub_user_name, disabled_flag, type_switch, javascript_functions, show_disabled, hide_users, multi_select, var_module, sub_sw2, sub_sw3, sub_sw4, sub_sw5)
'written specifically for selecting distribution lists

    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'disabled_flag = Disabled
    'type_switch = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine) (RV is for Review Sections)
    'javascript_functions = Javascript
    'show_disabled = Show disabled users
    'hide_users = if instr hide these users: (TM - Task manager, RO - Read Only,)
    'multi_select = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox
    'var_module = If not blank then used to filter the list to users who have access to that module
    'var_accb_code = if var_module is acc then use this to filter access to that accb
    'sub_sw2 - 5 not used

    if len(multi_select) > 0 then
        var_multiple = " size='" & multi_select & "' multiple" 
    else
        var_multiple = " size='1'"
    end if
   
    Response.Write  "<select name='" & sub_user_name & "' id='" & sub_user_name & "' class='select' " & disabled_flag & " " & javascript_functions & var_multiple & ">"
                    
                    select case type_switch 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Please select a user</option>" 
                                  '    response.Write  "<option value='nreq' "
                                  '           If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                  '    response.Write   ">Nobody required at this time</option>" &_                            
                                  '      "<option value='0'>----------------------------</option>"

                                  '      if session("pref_task_email") = "Y" and (var_module <> "INS" and var_module <> "SAO" and var_module <> "QAO" and var_module <> "FSO") then
                                  '          response.Write  "<option value='Email' "
                                  '          If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                                  '          response.write ">- Email to a user</option>"
                                  '      end if 

                        case "N"
                            'do nothing                            
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if

                        case "RV"
                            response.Write    "<option value='0'>Please select a reviewer</option>"        
                            if   var_module = "RA" then
                                response.Write    "<option value='nreq'"
                                if cstr(sub_user_value) = "nreq" then response.Write    " selected " 
                                response.Write    ">One off assessment</option>"  
                            end if                             
                        
                        case else
                               response.Write    "<option value='0'>Please select a user</option>"        
                             '  if session("pref_task_email") = "Y" then
                             '       response.Write  "<option value='Email' "
                             '       If cstr("Email") = cstr(sub_user_value) Then response.Write " selected "
                             '       response.write ">- Email to a user</option>"
                             '   end if 
                    end select 

        if ubound(global_user_v3_array,2) > 0 then
            'grab the first group name
            var_grp_name = global_user_v3_array(0,1)
            if len(var_grp_name) > 0 then  response.write "<optgroup label='" & var_grp_name & "'>"
            'loop through the array and populate the fields
            for i = 1 to ubound(global_user_v3_array,2)
                if var_grp_name <> global_user_v3_array(0,i) then  
                    var_grp_name = global_user_v3_array(0,i) 
                  response.write "</optgroup><optgroup label='" & var_grp_name & "'>"
                end if
                response.write "<option value='" & global_user_v3_array(1,i) & "' "
                if global_user_v3_array(3,i) = "N" and global_user_v3_array(1,i) = sub_user_value then response.write " selected "
                response.write ">" & global_user_v3_array(2,i) & "</option>"
                
            next
            if len(var_grp_name) > 0 then  response.write "</optgroup>"
        
        else 

         
                if session("pref_task_structure_roles") = "Y" then  ' Start of grab roles
                        'Grab the roles and enter them at the top.
                        'Don't worry about selecting them because it's just going to contain the reference to the user which will get picked up normally anyway


                        'error correction
                        if len(var_tier2_id) = 0 or isnull(var_tier2_id) then var_tier2_id = 1
                        if len(var_tier3_id) = 0 or isnull(var_tier3_id) then var_tier3_id = 1
                        if len(var_tier4_id) = 0 or isnull(var_tier4_id) then var_tier4_id = 1
                        if len(var_tier5_id) = 0 or isnull(var_tier5_id) then var_tier5_id = 1
                        if len(var_tier6_id) = 0 or isnull(var_tier6_id) then var_tier6_id = 1

                        objcommand.commandtext = "exec module_hr.dbo.sp_Return_Roles '" & session("corp_code") & "'"
                      '  response.write  objcommand.commandtext
                        set mystructrole = objcommand.execute
                            
                        if not mystructrole.eof then 
                            var_grp_name = returnRoleNameforLevel(mystructrole("level"))                               
                            if len(var_grp_name) > 0 then  response.write "<optgroup label='" & var_grp_name & "'>"

                            while not mystructrole.eof 
                                'if some then lets write them out 
                                    if var_grp_name <> returnRoleNameforLevel(mystructrole("level")) then      
                                    var_grp_name = returnRoleNameforLevel(mystructrole("level"))                                    
                                    response.write "</optgroup><optgroup label='" & var_grp_name & "'>"
                                end if     
                               
                                    Response.Write "<option value='" & mystructrole("level") & "_" & mystructrole("name") & "' >" & mystructrole("name") & "</option>" 
                                    var_cur_num =  ubound(global_user_v3_array,2)+1
                                    redim preserve global_user_v3_array(3,var_cur_num)
                                    global_user_v3_array(0,var_cur_num) = "Group Roles"
                                    global_user_v3_array(1,var_cur_num) = mystructrole("level") & "_" & mystructrole("name")
                                    global_user_v3_array(2,var_cur_num) = mystructrole("name")
                                    global_user_v3_array(3,var_cur_num) = "Y"
                                    mystructrole.movenext 
                                
                                    
                            wend
                            if len(var_grp_name) > 0 then  response.write "</optgroup>"
                        end if


                end if ' End of grab roles
                   
        
        
                '*********************************** Build gen SQL *********************************************  
                comText = ""
                if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                    comText = comText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
                end if
	
	            if type_switch = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                   comText = comText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
                elseif type_switch = "ST" and session("YOUR_ACCESS") <> "0" then
	                comText = comText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	            end if
                                    
                if instr(hide_users, "RO") then
                    comText = comText & " AND data.Acc_level NOT LIKE '3' "
                end if      
                    
                if instr(hide_users, "TM") then
                    comText = comText & " AND data.Acc_level NOT LIKE '4' "
                end if
            '*********************************** End of build gen SQL ********************************************* 
            if var_module = "ACC" then 
                specComText = " "
                
        
                specComText = specComText & " and data.Acc_status not like 'D' "

                specComText = specComText & " and data.acc_ref  in (select user_ref from " & Application("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code = data.corp_code and accb_code = '" & var_accb_code & "' and user_ref = data.acc_ref ) "

                call writeUserGroupForSQL("Users with access to this Incident Centre", comText & specComText, sub_user_value, var_module)


                specComText = " "
                
                specComText = specComText & " and data.Acc_status not like 'D' "

                specComText = specComText & " and data.acc_ref not  in (select user_ref from " & Application("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code = data.corp_code and accb_code = '" & var_accb_code & "' and user_ref = data.acc_ref ) "

                call writeUserGroupForSQL("Users without access to this Incident Centre", comText & specComText, sub_user_value, var_module)
            else 'Start of non accident modules
               if session("global_usr_perm") = "Y" then
                        if var_tier6_id > 0 then
                         '*********************************** Build specific SQL to grab all users with tier 6 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 

                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier6_ref = '" & var_tier6_id & "') "

                            call writeUserGroupForSQL(session("CORP_TIER6") & " Users", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with tier 6 access *********************************************  
                        end if
    
                        if var_tier5_id > 0 then
                        '*********************************** Build specific SQL to grab all users with tier 5 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER5") & " Users", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with tier 5 access *********************************************  
                        end if
    
                        if var_tier4_id > 0 then
                         '*********************************** Build specific SQL to grab all users with tier 4 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier4_ref = '" & var_tier4_id & "' and tier5_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER4") & " Users", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with tier 4 access *********************************************  
                        end if
    
                        if var_tier3_id > 0 then
                        '*********************************** Build specific SQL to grab all users with tier 3 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier3_ref = '" & var_tier3_id & "' and tier4_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER3") & " Users", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with tier 3 access *********************************************  
                         end if
    
                        if var_tier2_id > 0 then
                          '*********************************** Build specific SQL to grab all users with tier 2 access *********************************************  
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_ref  in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "' and tier3_ref = '0') "

                            call writeUserGroupForSQL(session("CORP_TIER2") & " Users", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with tier 2 access *********************************************    
                        end if

            end if
                         '*********************************** Build specific SQL to grab all users with GA access *********************************************  
                            specComText = " "
            
       
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level = 0 "

                            call writeUserGroupForSQL("Global Admins", comText & specComText, sub_user_value, var_module)

                        '*********************************** End of build specific SQL to grab all users with GA access *********************************************    


                 if session("TM_Hide_Other_Users") <> "Y"  or var_module = "TM" then 
                        '*********************************** Build specific SQL to grab all users with module access *********************************************  
       
     
                            specComText = " "
                            select case var_module
                               case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '1' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '1' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '1' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '1' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '1' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '1' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '1' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '1' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '1' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '1' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '1' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '1' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '1' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '1' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '1' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level <> 0 "

                            if session("global_usr_perm") = "Y" then
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "' and tier3_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "' and tier4_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'   and tier4_ref = '" & var_tier4_id & "' and tier5_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'  and tier4_ref = '" & var_tier4_id & "'  and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '0') "
                                specComText = specComText & " and data.acc_ref not in (select acc_ref from " & Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = data.corp_code and arc_flag = 0 and tier2_ref = '" & var_tier2_id & "'  and tier3_ref = '" & var_tier3_id & "'  and tier4_ref = '" & var_tier4_id & "'  and tier5_ref = '" & var_tier5_id & "' and tier6_ref = '" & var_tier6_id & "') "
                                header_text = "Other users with access to this module"
                            else
                                header_text = "Users with access to this module"
                            end if

                            if len(var_module) > 0  then
                                call writeUserGroupForSQL(header_text, comText & specComText, sub_user_value, var_module)
                            else
                                call writeUserGroupForSQL("Other users without access restrictions", comText & specComText, sub_user_value, var_module)
                            end if 
                        '*********************************** End of build specific SQL to grab all users with module access *********************************************    
            end if

            if session("TM_Hide_Users_WO_Access") <> "Y"  and var_module <> "TM"  then 
                         '*********************************** Build specific SQL to grab all users without module access *********************************************  
                          if len(var_module) > 0 and type_switch <> "RV" then
                            specComText = " "
                            select case var_module
                                case "RA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence, 1, 1 ) = '0' "
                                case "MH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 2 , 1 ) = '0' "
                                case "DSE"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 3 , 1 ) = '0' "
                                case "FS", "FSO"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 4 , 1 ) = '0' "
                                case "SA", "SAO", "APS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 6 , 1 ) = '0' "
                                case "QA", "QAO", "APQ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 42 , 1 ) = '0' "
                                case "COSHH"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 11 , 1 ) = '0' "
                                case "PTW"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 14 , 1 ) = '0' "
                                case "INS"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 15, 1 ) = '0' "  
                                case "SURA"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 21, 1 ) = '0' "  
                                case "LB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 13 , 1 ) = '0' "
                                case "CL"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 12 , 1 ) = '0' "
                                case "ASB"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 31 , 1 ) = '0' "
                                case "PUWER"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 34 , 1 ) = '0' "
                                case "HAZ"
                                    specComText = specComText & " AND SUBSTRING ( data.Acc_licence , 25 , 1 ) = '0' "
                            end select 
        
                            specComText = specComText & " and data.Acc_status not like 'D' "

                            specComText = specComText & " and data.acc_level <> 0 "

                            call writeUserGroupForSQL("Users without access to this module", comText & specComText, sub_user_value, var_module)
                        end if
          
           '*********************************** End of build specific SQL to grab all users with module access *********************************************    
          end if

     end if 'end of non accident modules 
    
              if show_disabled <> "N" then
          
                '*********************************** Build specific SQL to grab all disabled users *********************************************  
                specComText = " "
      
                specComText = specComText &  " and data.Acc_status  like 'D' "

                call writeUserGroupForSQL("Disabled Users", comText & specComText, sub_user_value, var_module)

                '*********************************** End of build specific SQL to grab all disabled users *********************************************   
            end if 

      
    end if
    response.Write "</select>"
end sub




sub writeUserGroupForSQL(grpName, mycomText, selVal, var_module)
   
   objcommand.commandtext = "SELECT DISTINCT data.Acc_Ref, ISNULL(display_name, UPPER(data.per_sname) + ', ' + data.per_fname)  as user_details, data.per_sname FROM " & Application("DBTABLE_USER_DATA") & " as data " 
	                             

    if type_switch = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
        'do nothing
    elseif type_switch = "ST" and session("YOUR_ACCESS") <> "0" then
        objcommand.commandtext = objcommand.commandtext & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                          "  ON data.corp_code = org.corp_code " & _
                                                          "  AND data.acc_ref = org.user_acc_ref "
    end if

    if (var_module = "APS" or var_module = "APQ") and session("YOUR_ACCESS") <> "0" then
		objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" &  session("CORP_CODE") & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
											              "  ON (data.corp_bu = perms.tier2_ref OR data.corp_bu = '-1') " & _
												          "  AND (data.corp_bl = perms.tier3_ref OR data.corp_bl = '-1') " & _
												          "  AND (data.corp_bd = perms.tier4_ref OR data.corp_bd = '-1') " & _
												          "  AND (data.tier5_ref = perms.tier5_ref OR data.tier5_ref = '-1') " & _
												          "  AND (data.tier6_ref = perms.tier6_ref OR data.tier6_ref = '-1') " & _
                                                          "  AND perms.perm_read = 1 "
    end if

    objcommand.commandtext = objcommand.commandtext & " WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' " & mycomText
    if session("global_usr_perm") = "Y" then objcommand.commandtext = objcommand.commandtext & " and data.tm_access = 1 "                                                      
    objcommand.commandtext = objcommand.commandtext & "  ORDER BY data.Per_sname ASC"

    if ucase(session("your_username")) = "SYSADMIN.PAK" then
        'response.write vbcrlf &  vbcrlf & objcommand.commandtext & vbcrlf & vbcrlf
    end if

'response.write vbcrlf &  vbcrlf & objcommand.commandtext & vbcrlf & vbcrlf
    SET objRsuser = objCommand.Execute

    if not objRsuser.eof then 
        'if session("pref_task_structure_roles") = "Y" then 
        response.write "<optgroup label='" & grpName & "'>"
     
   	    While NOT objRsuser.EOF
            Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
            If instr(1,selVal,objRsuser("Acc_ref")) > 0 and cstr(selVal) <> "0" and len(selVal) > 0 then
                Response.Write " selected"
            End if
            Response.Write ">" & objRsuser("user_details") & "</option>"
            
            'populate the global_user_v3_array
            var_cur_num =  ubound(global_user_v3_array,2)+1
            redim preserve global_user_v3_array(3,var_cur_num)
            global_user_v3_array(0,var_cur_num) = grpName
            global_user_v3_array(1,var_cur_num) = objRsuser("Acc_Ref")
            global_user_v3_array(2,var_cur_num) = objRsuser("user_details")
            global_user_v3_array(3,var_cur_num) = "N"

            objRsuser.movenext
	    Wend
        'if session("pref_task_structure_roles") = "Y" then 
        response.write "</optgroup>"
    end if
end sub


function returnRoleNameforLevel(temp_grp_id)
    select case temp_grp_id
        case 1
            temp_grp_name = "Group Roles"
        case 2
            temp_grp_name = session("CORP_TIER2") & " Roles"
        case 3
            temp_grp_name = session("CORP_TIER3") & " Roles"
        case 4
            temp_grp_name = session("CORP_TIER4") & " Roles"
        case 5
            temp_grp_name = session("CORP_TIER5") & " Roles"
        case 6
            temp_grp_name = session("CORP_TIER6") & " Roles"    
        case else 
            temp_grp_name = ""                                
    end select 
    returnRoleNameforLevel = temp_grp_name
end function
%>