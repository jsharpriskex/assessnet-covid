<%

' This Routine will create a history fingerprint in the global database, so you can track user activity.

SUB CREATEFingerprint(varHisMod,varHisRelationRef,varHisType)

varHisDateTime = Date() & " " & Time()
varHisBy = Session("YOUR_ACCOUNT_ID")
varCorp_code = Session("CORP_CODE")

Objcommand.CommandText =	"INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " " & _
							"(corp_code,HisMod,HisRelationRef,HisDateTime,HisBy,HisType) " & _
							"VALUES('" & varCorp_code & "','" & varHisMod & "','" & varHisRelationRef & "','" & varHisDateTime & "','" & varHisBy & "','" & varHisType & "') "
Objcommand.Execute

END SUB

SUB CREATEFprintNOTE(varNoteMod,varNoteText,varNoteRelationRef)

varNoteDateTime = Date() & " " & Time()
varNoteBy = Session("YOUR_ACCOUNT_ID")
varNoteText_chg = Replace(varNoteText,"'","`")
varCorp_code = Session("CORP_CODE")

Objcommand.CommandText =	"INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINTNOTES") & " " & _
							"(corp_code,NoteMod,NoteRelationRef,NoteDateTime,NoteBy,NoteText) " & _
							"VALUES('" & varCorp_code & "','" & varNoteMod & "','" & varNoteRelationRef & "','" & varNoteDateTime & "','" & varNoteBy & "','" & varNoteText_chg & "')"
Objcommand.Execute

END SUB

%>