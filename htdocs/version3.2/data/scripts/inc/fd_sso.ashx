﻿<%@ WebHandler Language="C#" Class="fd_sso" %>

using System;
using System.Web;
using ANET;
using System.Web.SessionState;

public class fd_sso : IHttpHandler, IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {

        try
        {
            string Strname = "", email = "";

            Strname = context.Session["YOUR_FULLNAME"].ToString();
            email = context.Session["YOUR_EMAIL"].ToString();

            if (Strname != null)
            {
                if (Strname.Length > 0)
                {
                    string path = FreshdeskSSO.HelpLogin(Strname, email);

                    context.Response.Redirect(path);
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("Your session wasn't configured properly");
                }
            }
        }
        catch(Exception e)
        {
            context.Response.Write(e.Message);
            context.Response.End();
        }


        

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}