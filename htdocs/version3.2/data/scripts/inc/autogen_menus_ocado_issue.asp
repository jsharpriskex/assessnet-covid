<%
dim sub_tier4_value

    

  sub showHistoryBox(var_hist_reference)
   
            if instr(var_hist_reference, "|") > 0 then
   
                str_array = split(var_hist_reference, "|")      
                var_hist_reference = str_array(0)
                var_mod_code = str_array(1)        
            end if

             response.Write "<table width='100%' cellpadding='3' class='section2' >"  & _        
                              "<tr>" & _
                               "<th><textarea class='textarea' rows='6'  style='width: 99%' readonly>"
            
                Objcommand.commandtext =	"SELECT History.*, hrusers.per_fname, hrusers.per_sname, tier4.struct_name FROM " & Application("DBTABLE_GLOBAL_FPRINT") & " as History " & _
							                "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as hrusers " & _
                                            "  ON History.corp_code = hrusers.corp_code " & _
                                            "  AND History.HisBy = hrusers.acc_ref " & _  
                                            "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                            "  ON hrusers.corp_code = tier4.corp_code " & _
                                            "  AND CAST(hrusers.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) " & _                                  
                                            "WHERE History.HisRelationref = '" & var_hist_reference & "' AND History.corp_code='" & Session("corp_code") & "' AND History.HisType NOT LIKE 'VIEWED%' "
                if len(var_mod_code) > 0 then Objcommand.commandtext = Objcommand.commandtext & " and hismod='" & var_mod_code & "' "
                Objcommand.commandtext = Objcommand.commandtext & " ORDER BY History.ID DESC"
                SET Objrs = Objcommand.execute
                
                if objrs.eof then        
                     Response.write "No history could be found for this assessment"   
                else
	                while not Objrs.eof
                    var_hist_type = Objrs("HisType")
                    var_hist_DateTime = Objrs("HisDateTime")
                    var_hist_by = Objrs("per_fname") & " " & Objrs("per_sname") & " - ( " & Objrs("struct_name") & " )"
                            
                    ' error correction
                    if (var_hist_type = " ") or (len(var_hist_type) < 1) or (IsNull(var_hist_type) = true) then var_hist_type = "Unknown" end if
                    if (var_hist_Datetime = " ") or (len(var_hist_DateTime) < 1) or (IsNull(var_hist_DateTime) = true) then var_hist_DateTime = "Unknown" end if
                    if (var_hist_by = " ") or (len(var_hist_by) < 1) or (IsNull(var_hist_by) = true) then var_hist_by = "Unknown" end if
                    
                    Response.write  var_hist_DateTime & " | " & Ucase(var_hist_type) & " by " & var_hist_by &  vbCrLf
                            

                    Objrs.movenext
                    wend
                end if
                
               response.Write "</textarea></th>" & _ 
                                "</tr>"          & _         
                              "</table>"
        end sub
        

sub showHistoryPlusTaskNotesBox(var_hist_reference, var_hist_module, switch1, switch2)

    'switch1 - hide creator name
    'switch2 - UNUSED

    response.Write "<table width='100%' cellpadding='3' class='section2' >"  & _        
                   "<tr>" & _
                   "<th><textarea class='textarea' rows='6' style='width: 99%' readonly>"
            
          
    select case var_hist_module
        case "RA"
            Objcommand.commandtext = "EXEC Module_global.dbo.sp_ra_history '" & Session("corp_code") & "', '" & var_hist_reference & "' WITH RECOMPILE"

        case "RR"
            Objcommand.commandtext = "EXEC Module_global.dbo.sp_rr_history '" & Session("corp_code") & "', '" & var_hist_reference & "' WITH RECOMPILE"

        case "SA"
            Objcommand.commandtext = "SELECT History.hisType AS note_text, History.hisDateTime AS datetimestamp, History.HisBy AS accountid, module_hr.dbo.fn_return_user_details(History.corp_code, History.HisBy) as user_details FROM " & Application("DBTABLE_GLOBAL_FPRINT") & " as History " & _
							         "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as hrusers " & _
                                     "  ON History.corp_code = hrusers.corp_code " & _
                                     "  AND History.HisBy = hrusers.acc_ref " & _                                                                      
                                     "WHERE History.HisRelationref = '" & var_hist_reference & "' AND History.corp_code='" & Session("corp_code") & "' AND History.HisType NOT LIKE '%VIEWED%' ORDER BY History.ID DESC"

        case else '"PR", "HAZR"
            Objcommand.commandtext = "SELECT History.hisType AS note_text, History.hisDateTime AS datetimestamp, History.HisBy AS accountid, module_hr.dbo.fn_return_user_details(History.corp_code, History.HisBy) as user_details FROM " & Application("DBTABLE_GLOBAL_FPRINT") & " as History " & _
							         "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as hrusers " & _
                                     "  ON History.corp_code = hrusers.corp_code " & _
                                     "  AND History.HisBy = hrusers.acc_ref " & _                                                                      
                                     "WHERE History.HisMod = '" & var_hist_module & "' AND History.HisRelationref = '" & var_hist_reference & "' AND History.corp_code='" & Session("corp_code") & "' AND History.HisType NOT LIKE '%VIEWED%' ORDER BY History.ID DESC"
    end select
  
   SET Objrs = Objcommand.execute
    if objrs.eof then        
        Response.write "No history could be found for this assessment"   
    else
                
        var_history_no = 0 
                    
	    while not Objrs.eof
            var_hist_type = rtrim(Objrs("note_text"))
            var_hist_DateTime = Objrs("datetimestamp")
            if var_hist_module = "RA" or var_hist_module = "RR" then 
                var_hist_by = rtrim(Objrs("first_name")) & " " & rtrim(Objrs("surname")) & " - ( " & rtrim(Objrs("tier4_name")) & " )"         
            else
                var_hist_by = rtrim(Objrs("user_details")) 
            end if
            var_hist_by_id = Objrs("accountid")
            
                ' error correction
                if (var_hist_type = " ") or (len(var_hist_type) < 1) or (IsNull(var_hist_type) = true) then var_hist_type = "Unknown" end if
                if (var_hist_Datetime = " ") or (len(var_hist_DateTime) < 1) or (IsNull(var_hist_DateTime) = true) then var_hist_DateTime = "Unknown" end if
                if (var_hist_by = " ") or (len(var_hist_by) < 1) or (IsNull(var_hist_by) = true) then var_hist_by = "Unknown" end if
                    
                if switch1 = "Anonymous" and var_hist_type = "CREATED" then
                    var_hist_by = switch1
                end if

                Response.write  var_hist_DateTime & " | " & Ucase(var_hist_type) & " by " & var_hist_by & vbCrLf     
                           
                var_history_no =  var_history_no + 1 
              
            Objrs.movenext
        wend
    end if
                
    response.Write "</textarea></th>" & _ 
                   "</tr>"          & _         
                   "</table>"
end sub
              
        

SUB HAZ_MENU(HazValue,ObjNAME)

    objCommand.commandText = "SELECT opt_name FROM " & "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'haztype' " & _
                             "ORDER BY opt_name"
    set objRs_opts = objCommand.execute
    if not objRs_opts.EOF then

        response.Write "<select name='" & ObjNAME & "' class='select'"
        if left(ObjNAME,3) = "ra_" or left(ObjNAME,5) = "sura_" then
            response.Write " onchange='javascript:updateHelperLink(this)' "
        end if
        response.Write ">"
        
        if HazValue = "S" then
            Response.Write "<option value='any'>any hazard type</option>"
        else
            response.Write "<option value='any'>Select a Hazard ...</option>"
        end if

        while not objRs_opts.EOF
            if HazValue = objRs_opts("opt_name") then
                response.Write "<option value='" & objRs_opts("opt_name") & "' style='background-color: #FFF000' selected>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            else
                response.Write "<option value='" & objRs_opts("opt_name") & "'>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            end if
            
            objRs_opts.movenext
        wend
        
        response.Write "</select>"
    else
        response.Write "<select name='" & ObjNAME & "' class='select'>" & _
                       "<option value='any'>There are no hazard types available</option>" & _
                       "</select>"
    end if
    set objRs_opts = nothing
	
END SUB



SUB HAZ_MENU2(HazValue, ObjNAME, status, switch1, switch2)

    objCommand.commandText = "SELECT opt_name FROM " & "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'haztype' " & _
                             "ORDER BY opt_name"
    set objRs_opts = objCommand.execute
    if not objRs_opts.EOF then
        response.Write "<select name='" & ObjNAME & "' class='select'"
        if left(ObjNAME,3) = "ra_" or left(ObjNAME,5) = "sura_" or left(ObjNAME,3) = "hp_" then
            response.Write " onchange='javascript:updateHelperLink(this)' "
        end if        
        response.Write status & ">"
        
        if HazValue = "S" then
            Response.Write "<option value='any'>any hazard type</option>"
        else
            response.Write "<option value='any'>Select a Hazard ...</option>"
        end if

        while not objRs_opts.EOF
            if HazValue = objRs_opts("opt_name") then
                response.Write "<option value='" & objRs_opts("opt_name") & "' style='background-color: #FFF000' selected>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            else
                response.Write "<option value='" & objRs_opts("opt_name") & "'>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            end if
            
            objRs_opts.movenext
        wend
        
        response.Write "</select>"
    else
        response.Write "<select name='" & ObjNAME & "' class='select'>" & _
                       "<option value='any'>There are no hazard types available</option>" & _
                       "</select>"
    end if
    set objRs_opts = nothing

	
END SUB


SUB RR_HAZ_MENU(HazValue,ObjNAME)

    objCommand.commandText = "SELECT opt_name FROM " & "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'rr_haztype' " & _
                             "ORDER BY opt_name"
    set objRs_opts = objCommand.execute
    if not objRs_opts.EOF then

        response.Write "<select name='" & ObjNAME & "' class='select'"
        if left(ObjNAME,3) = "ra_" or left(ObjNAME,5) = "sura_" then
            response.Write " onchange='javascript:updateHelperLink(this)' "
        end if
        response.Write ">"
        
        if HazValue = "S" then
            Response.Write "<option value='any'>any risk type</option>"
        else
            response.Write "<option value='any'>Select a Risk ...</option>"
        end if

        while not objRs_opts.EOF
            if HazValue = objRs_opts("opt_name") then
                response.Write "<option value='" & objRs_opts("opt_name") & "' style='background-color: #FFF000' selected>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            else
                response.Write "<option value='" & objRs_opts("opt_name") & "'>" & objRs_opts("opt_name") & "</option>" & vbcrlf
            end if
            
            objRs_opts.movenext
        wend
        
        response.Write "</select>"
    else
        response.Write "<select name='" & ObjNAME & "' class='select'>" & _
                       "<option value='any'>There are no risk types available</option>" & _
                       "</select>"
    end if
    set objRs_opts = nothing
	
END SUB


	DIM PAFFECTED(22), paffARRAY

	PAFFECTED(0) = "Cleaners"
	PAFFECTED(1) = "Contractors"
	PAFFECTED(2) = "Employees"
	PAFFECTED(3) = "Engineers"
	PAFFECTED(4) = "Lone Workers"
	PAFFECTED(5) = "Machine Operators"
	PAFFECTED(6) = "Maintenance Staff"
	PAFFECTED(7) = "Members of the Public"
	PAFFECTED(8) = "Office Staff"
	PAFFECTED(9) = "Outdoor Workers"
	PAFFECTED(10) = "Patient"
	PAFFECTED(11) = "Pregnant Women"
	PAFFECTED(12) = "Production"
	PAFFECTED(13) = "Staff"
	PAFFECTED(14) = "Staff with Disabilities"
    PAFFECTED(15) = "Students"
	PAFFECTED(16) = "Students with Disabilities"
	PAFFECTED(17) = "Resident / Tenant"
	PAFFECTED(18) = "Trainees / Young Persons"
	PAFFECTED(19) = "Visitors"
	PAFFECTED(20) = "Volunteers"
	PAFFECTED(21) = "Warehouse Operators"
	PAFFECTED(22) = "Other"

SUB PER_AFFECTED(ObjNAME)



	Response.Write("<select name='" & ObjNAME & "' class='select'>")
	Response.Write(vbCrLf & "  <option>Select ...</option>")
	
    For paffARRAY = 0 to ubound(PER_AFFECTED)
        Response.Write(vbCrLf & "  <option value='" & PAFFECTED(paffARRAY) & "'>" & PAFFECTED(paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB

SUB PER_AFFECTED2(sub_name, sub_val, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)
    
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Empty
    'sub_sw3 = Javascript
    'sub_sw4 = Empty
    'sub_sw5 = Empty



	Response.Write("<select name='" & sub_name & "' class='select'>")
	Response.Write(vbCrLf & "  <option value='0'>Any person affected</option>")
	
    For paffARRAY = 0 to ubound(PAFFECTED)
        Response.Write(vbCrLf & "  <option value='" & PAFFECTED(paffARRAY) & "' ")
        
        if cstr(sub_val) = cstr(PAFFECTED(paffARRAY)) then
             response.write " selected "
        end if
         Response.Write("  >" & PAFFECTED(paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB


DIM SU_PAFFECTED(17), SU_paffARRAY

	SU_PAFFECTED(0) = "Children"
	SU_PAFFECTED(1) = "Cleaners"
	SU_PAFFECTED(2) = "Contractors"
	SU_PAFFECTED(3) = "Employees"
	SU_PAFFECTED(4) = "Lone Workers"
	SU_PAFFECTED(5) = "Maintenance Staff"
	SU_PAFFECTED(6) = "Members of the Public"
	SU_PAFFECTED(7) = "Office Staff"
	SU_PAFFECTED(8) = "Parent / Carer"
	SU_PAFFECTED(9) = "Pregnant Women"
	SU_PAFFECTED(10) = "Staff with Disabilities"
	SU_PAFFECTED(11) = "Students"
	SU_PAFFECTED(12) = "Students with Disabilities"
	SU_PAFFECTED(13) = "Resident / Tenant"
	SU_PAFFECTED(14) = "Trainee / Young Persons"
    SU_PAFFECTED(15) = "Visitors"
	SU_PAFFECTED(16) = "Volunteers"
	SU_PAFFECTED(17) = "Other"

SUB SU_PER_AFFECTED(ObjNAME)



	Response.Write("<select name='" & ObjNAME & "' class='select'>")
	Response.Write(vbCrLf & "  <option>Select ...</option>")
	
    For SU_paffARRAY = 0 to 18
        Response.Write(vbCrLf & "  <option value='" & SU_PAFFECTED(SU_paffARRAY) & "'>" & SU_PAFFECTED(SU_paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB

Sub autogenBodyPart(myBodyPart, outputFormat) 
   
    varScriptOutput = ""

    if Session("MODULE_ACCB") = "2" then	
        objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '0' order by opt_value asc"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
            while not objrsBP.eof
                if cstr(myBodyPart) =  cstr(objrsBP("opt_id")) then
                    varScriptOutput = varScriptOutput & "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "'  selected >" &  objrsBP("opt_value") & "</option>"
                else
                    varScriptOutput = varScriptOutput & "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "'>" &  objrsBP("opt_value") & "</option>"
                end if
                
                 if objrsBP("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsBP("opt_id") & "' order by opt_value asc"
                        set objrsBPSUB = objcommand.execute
                        
                        if not objrsBPSUB.eof then 
                            while not objrsBPSUB.eof
                               if cstr(myBodyPart) =  cstr(objrsBPSUB("opt_id")) then
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "' selected>-- " & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
                                else
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "'>-- " & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsBPSUB.movenext
                            wend
                        
                        end if
                    
                    end if
                objrsBP.movenext
            wend
        end if
    
    
    else
        Dim dictBodyPart
        Set dictBodyPart = CreateObject("Scripting.Dictionary")
        dictBodyPart.Add "0", "Ankle"
        dictBodyPart.Add "1", "Arm"
        dictBodyPart.Add "2", "Back (Lower)"
        dictBodyPart.Add "3", "Back (Upper)"
        dictBodyPart.Add "25", "Back - Unspecific"
        dictBodyPart.Add "4", "Buttocks"
        dictBodyPart.Add "5", "Chest"
        dictBodyPart.Add "6", "Eye"
        dictBodyPart.Add "7", "Face"
        dictBodyPart.Add "8", "Finger(s)"
        dictBodyPart.Add "9", "Foot"
        dictBodyPart.Add "10", "Groin"
        dictBodyPart.Add "11", "Hand injury"
        dictBodyPart.Add "12", "Head"
        dictBodyPart.Add "13", "Hip"
        dictBodyPart.Add "14", "Internal injuries"
        dictBodyPart.Add "15", "Knee"
        dictBodyPart.Add "24", "Leg - Unspecific"
        dictBodyPart.Add "16", "Multiple injuries"
        dictBodyPart.Add "17", "Neck"
        dictBodyPart.Add "18", "Shin"
        dictBodyPart.Add "19", "Shoulder"
        dictBodyPart.Add "20", "Stomach"
        dictBodyPart.Add "21", "Thigh"
        dictBodyPart.Add "22", "Toe(s)"
        dictBodyPart.Add "23", "Wrist"
        dictBodyPart.Add "na", "[ Not Specified ]" 
    
    
        myBodyPart = "" & myBodyPart
       For Each key In dictBodyPart
            varScriptOutput = varScriptOutput & "<option value='" & key & "'"
            If myBodyPart = key Then 
                varScriptOutput = varScriptOutput & " selected" 
            End if
            varScriptOutput = varScriptOutput & ">" & dictBodyPart.item(key) & "</option>" & vbCrLf
        Next
    end if

    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
    end if

    response.write varScriptOutput   
  
End Sub

Function getBodyPartForCode(myBodyPart)

     if isnull(myBodyPart) = true or myBodyPart = "" or myBodyPart = "na" then
        myBodyPart = 0
    end if

    if Session("MODULE_ACCB") ="2"  then
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("parent_value")) > 0 then
                getBodyPartForCode = objrsBP("parent_value") & " > " & objrsBP("opt_value")
           else
                getBodyPartForCode = objrsBP("opt_value")
           end if      
        else
           getBodyPartForCode =  "Details could not be found"
        end if
    else
    
      Dim dictBodyPart
            Set dictBodyPart = CreateObject("Scripting.Dictionary")
            dictBodyPart.Add "0", "Ankle"
            dictBodyPart.Add "1", "Arm"
            dictBodyPart.Add "2", "Back (Lower)"
            dictBodyPart.Add "3", "Back (Upper)"
            dictBodyPart.Add "25", "Back - Unspecific"
            dictBodyPart.Add "4", "Buttocks"
            dictBodyPart.Add "5", "Chest"
            dictBodyPart.Add "6", "Eye"
            dictBodyPart.Add "7", "Face"
            dictBodyPart.Add "8", "Finger(s)"
            dictBodyPart.Add "9", "Foot"
            dictBodyPart.Add "10", "Groin"
            dictBodyPart.Add "11", "Hand injury"
            dictBodyPart.Add "12", "Head"
            dictBodyPart.Add "13", "Hip"
            dictBodyPart.Add "14", "Internal injuries"
            dictBodyPart.Add "15", "Knee"
            dictBodyPart.Add "24", "Leg - Unspecific"
            dictBodyPart.Add "16", "Multiple injuries"
            dictBodyPart.Add "17", "Neck"
            dictBodyPart.Add "18", "Shin"
            dictBodyPart.Add "19", "Shoulder"
            dictBodyPart.Add "20", "Stomach"
            dictBodyPart.Add "21", "Thigh"
            dictBodyPart.Add "22", "Toe(s)"
            dictBodyPart.Add "23", "Wrist"
            dictBodyPart.Add "na", "[ Not Specified ]" 
    
        myBodyPart = "" & myBodyPart
        getBodyPartForCode = dictBodyPart.Item(myBodyPart)
    end if

    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
    end if

    response.write varScriptOutput
   
End function

Function getBodyPartColourForCode(myBodyPart)

    if isnull(myBodyPart) = true or myBodyPart = "" or myBodyPart = "na" then
        myBodyPart = 0
    end if

    if Session("MODULE_ACCB") = "2" then
        objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("opt_colour")) > 0 and not isnull(objrsBP("opt_colour"))  then
                getBodyPartColourForCode = replace(objrsBP("opt_colour"), "#", "") 
           else
                getBodyPartColourForCode = ""
           end if      
        else
           getBodyPartColourForCode = ""
        end if
    else
        getBodyPartColourForCode = ""
    end if
   
End function


Sub autogenEmpStatus(opt_value, field_name, field_disabled, filter_list_item)

	if isnull(opt_value) = true or opt_value = "" or opt_value = "na" then
		opt_value = 0
    end if
	
	objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
							 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
							 "  AND related_to = 'emp_status' " & _
							 "  AND parent_id = 0"
	if len(filter_list_item) > 0 then
		objCommand.commandText = objCommand.commandText & "  AND opt_value = (SELECT opt_target FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
																		     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
																		     "AND related_to = 'acc_status' " & _
																		     "AND opt_id = " & filter_list_item & ")"
	end if
	set objRsREL = objCommand.execute
	if not objRsREL.EOF then
		response.write "<select class='select' name='" & field_name & "' " & field_disabled & "><option value='na'>Please Select</option>"

		while not objRsREL.EOF
			
			if objRsREL("child_present") = 1 then
				response.write "<optgroup label='" & objRsREL("opt_value") & "'>"

				objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
										 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
										 "  AND related_to = 'emp_status' " & _
										 "  AND parent_id = " & objRsREL("opt_id")
				set objRsREL2 = objCommand.execute
				if not objRsREL2.EOF then
					while not objRsREL2.EOF
						if cint(opt_value) = cint(objRsREL2("opt_id")) then
							response.write "<option value='" & objRsREL2("opt_id") & "' selected>" & objRsREL2("opt_value") & "</option>"
						else
							response.write "<option value='" & objRsREL2("opt_id") & "'>" & objRsREL2("opt_value") & "</option>"
						end if
						objRsREL2.movenext
					wend
				end if
				set objRsREL2 = nothing
				response.write "</optgroup>"
			else
				if cint(opt_value) = cint(objRsREL("opt_id")) then
					response.write "<option value='" & objRsREL("opt_id") & "' selected>" & objRsREL("opt_value") & "</option>"
				else
					response.write "<option value='" & objRsREL("opt_id") & "'>" & objRsREL("opt_value") & "</option>"
				end if
			end if

			objRsREL.movenext
		wend

		response.write "</select>"
	end if
	set objRsREL = nothing

End Sub

function getEmpStatusForCode(myType)

    if isnull(myType) = true or myType = "" or myType = "na" then
        myType = 0
    end if
	
	objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
							 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
							 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
							 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='emp_status' and a.opt_id='" & clng(myType) & "'"	
	set objRsEMP = objCommand.execute
	if not objRsEMP.EOF then
		objRsEMP.movefirst
		getEmpStatusForCode = objRsEMP("parent_value") & " - " & objRsEMP("opt_value")
	else
		getEmpStatusForCode = "<span class='info'>Not an Employee</span>"
	end if
	set objRsEMP = nothing
	
end function


' -------- Type of injury ---------

Sub autogenInjType(myType, outputFormat)

    varScriptOutput = ""

    if Session("MODULE_ACCB") = "2" then
     
        objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '0' order by opt_value asc"
        set objrsIT = objcommand.execute

        if not objrsIT.eof then 
            while not objrsIT.eof
                if cstr(myType) =  cstr(objrsIT("opt_id")) then
                    varScriptOutput = varScriptOutput & "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "' selected >" &  objrsIT("opt_value") & "</option>"
                else
                    varScriptOutput = varScriptOutput & "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "'>" &  objrsIT("opt_value") & "</option>"
                end if
                
                 if objrsIT("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '" & objrsIT("opt_id") & "' order by opt_value asc"
                        set objrsITSUB = objcommand.execute
                        
                        if not objrsITSUB.eof then 
                            while not objrsITSUB.eof
                               if cstr(myType) =  cstr(objrsITSUB("opt_id")) then
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "' selected>-- " & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
                                else
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "'>-- " & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsITSUB.movenext
                            wend
                        
                        end if
                    
                    end if
                objrsIT.movenext
            wend
        end if
    else
        Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"

            myType = "" & myType
            For Each key In dictInjType
                varScriptOutput = varScriptOutput & "<option value='" & key & "'"
                If myType = key Then 
                    varScriptOutput = varScriptOutput & " selected" 
                End if
                varScriptOutput = varScriptOutput & ">" & dictInjType.item(key) & "</option>" & vbCrLf
            Next
    end if   

    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
    end if

    response.write varScriptOutput    
         
End Sub

Function getInjTypeForCode(myType)

     if isnull(myType) = true or myType = "" or myType = "na" then
        myType = 0
    end if
   
     if  Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("parent_value")) > 0 then
                getInjTypeForCode = objrsIT("parent_value") & " > " & objrsIT("opt_value")
           else
                getInjTypeForCode = objrsIT("opt_value")
           end if                   
        else
           getInjTypeForCode =  "Injury type could not be found"
        end if
    else
         Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "17", "Multiple Injuries"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"
    
         myType = "" & myType
        getInjTypeForCode = dictInjType.Item(myType)
    end if
End function

Function getInjTypeColourForCode(myType)
   
    if isnull(myType) = true or myType = "" or myType = "na" then
        myType = 0
    end if


     if  Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                  " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("opt_colour")) > 0 and not isnull(objrsIT("opt_colour")) then
                getInjTypeColourForCode = replace(objrsIT("opt_colour"), "#", "")
           else
                getInjTypeColourForCode = ""
           end if                   
        else
           getInjTypeColourForCode = ""
        end if
    else

    end if
End function


' --------- Apparent Cause of Injury ----------

Sub autogenApparentCause(myCause, outputFormat)

    varScriptOutput = ""

    if Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '0' order by opt_value asc"
        set objrsAC = objcommand.execute

        if not objrsAC.eof then 
            while not objrsAC.eof
                if cstr(myCause) =  cstr(objrsAC("opt_id")) then
                    varScriptOutput = varScriptOutput & "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "'  selected >" &  objrsAC("opt_value") & "</option>"
                else
                    varScriptOutput = varScriptOutput & "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "'>" &  objrsAC("opt_value") & "</option>"
                end if
                
                 if objrsAC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '" & objrsAC("opt_id") & "' order by opt_value asc"
                        set objrsACSUB = objcommand.execute

                        if not objrsACSUB.eof then 
                            while not objrsACSUB.eof
                               if cstr(myCause) =  cstr(objrsACSUB("opt_id")) then
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "' selected>-- " & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
                                else
                                    varScriptOutput = varScriptOutput & "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "'>-- " & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsACSUB.movenext
                            wend
                        end if
                    
                    end if
                objrsAC.movenext
            wend
        end if
    else

        Dim dictApparentCause
        Set dictApparentCause = CreateObject("Scripting.Dictionary")

        dictApparentCause.Add "0", "Animal Related"
        dictApparentCause.Add "1", "Collision"
        dictApparentCause.Add "3", "Contact with electricity"
        dictApparentCause.Add "4", "Contact with machinery"
        dictApparentCause.Add "2", "Contact with sharp object"
        dictApparentCause.Add "15", "Exposure to explosion"
        dictApparentCause.Add "6", "Exposure to fire"
        dictApparentCause.Add "5", "Exposure to hazardous substance"
        dictApparentCause.Add "7", "Exposure to temperature extremes"
        dictApparentCause.Add "16", "Fainting / Fit"
        dictApparentCause.Add "8", "Fall from height"
        dictApparentCause.Add "9", "Manual Handling"
        dictApparentCause.Add "10", "Repetitive actions"
        dictApparentCause.Add "17", "Self inflicted"
        dictApparentCause.Add "11", "Slip, trip or fall"
        dictApparentCause.Add "12", "Struck by object"
        dictApparentCause.Add "13", "Struck by vehicle"
        dictApparentCause.Add "18", "Trapped by object"
        dictApparentCause.Add "14", "Violence"
        dictApparentCause.Add "na", "[ Not Specified ]"

            myCause = "" & myCause
            For Each key In dictApparentCause
                varScriptOutput = varScriptOutput & "<option value='" & key & "'"
                If myCause = key Then 
                    varScriptOutput = varScriptOutput & " selected" 
                End if
                varScriptOutput = varScriptOutput & ">" & dictApparentCause.item(key) & "</option>" & vbCrLf
            Next
    end if


    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
    end if

    response.write varScriptOutput
        
End Sub
	
Function getApparentCauseForCode(myCause)
  
    if isnull(mycause) or mycause = "" then
        getApparentCauseForCode = "Details could not be found"
    else
         if  Session("MODULE_ACCB") ="2" then

             objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                     " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                     " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
                                     " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "'"
               
        
            set objrsAC = objcommand.execute
                   
            if not objrsAC.eof then 
               if len(objrsAC("parent_value")) > 0 then
                    getApparentCauseForCode = objrsAC("parent_value") & " > " & objrsAC("opt_value")
               else
                    getApparentCauseForCode = objrsAC("opt_value")
               end if                   
            else
               getApparentCauseForCode =  "Details could not be found"
            end if
        else
        
            Dim dictApparentCause
            Set dictApparentCause = CreateObject("Scripting.Dictionary")

            dictApparentCause.Add "0", "Animal Related"
            dictApparentCause.Add "1", "Collision"
            dictApparentCause.Add "3", "Contact with electricity"
            dictApparentCause.Add "4", "Contact with machinery"
            dictApparentCause.Add "2", "Contact with sharp object"
            dictApparentCause.Add "15", "Exposure to explosion"
            dictApparentCause.Add "6", "Exposure to fire"
            dictApparentCause.Add "5", "Exposure to hazardous substance"
            dictApparentCause.Add "7", "Exposure to temperature extremes"
            dictApparentCause.Add "16", "Fainting / Fit"
            dictApparentCause.Add "8", "Fall from height"
            dictApparentCause.Add "9", "Manual Handling"
            dictApparentCause.Add "10", "Repetitive actions"
            dictApparentCause.Add "17", "Self inflicted"
            dictApparentCause.Add "11", "Slip, trip or fall"
            dictApparentCause.Add "12", "Struck by object"
            dictApparentCause.Add "13", "Struck by vehicle"
            dictApparentCause.Add "18", "Trapped by object"
            dictApparentCause.Add "14", "Violence"
            dictApparentCause.Add "na", "[ Not Specified ]"
    
            myCause = "" & myCause
            getApparentCauseForCode = dictApparentCause.Item(myCause)
        end if
    end if
End function

Function getApparentCauseColourForCode(myCause)

    if isnull(mycause) or mycause = "" then
        getApparentCauseColourForCode = ""
    else

        if Session("MODULE_ACCB") ="2" then
            objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                     " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "'"
           set objrsAC = objcommand.execute
        
            if not objrsAC.eof then 
               if len(objrsAC("opt_colour")) > 0 and not isnull(objrsAC("opt_colour"))  then
                    getApparentCauseColourForCode = replace(objrsAC("opt_colour"), "#", "") 
               else
                    getApparentCauseColourForCode = ""
               end if                   
            else
               getApparentCauseColourForCode =  ""
            end if
        else

        end if
    end if
End function


sub autogenInjStatus(myStatus)

      
        
         objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '0' order by opt_value asc"
        set objrsIS = objcommand.execute

        if not objrsIS.eof then 
            while not objrsIS.eof
                if cstr(myStatus) =  cstr(objrsIS("opt_id")) then
                    response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "' selected>" &  objrsIS("opt_value") & "</option>"
                else
                    response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
                end if
                
                 if objrsIS("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '" & objrsIS("opt_id") & "' order by opt_value asc"
                        set objrsISSUB = objcommand.execute

                        if not objrsISSUB.eof then 
                            while not objrsISSUB.eof
                               if cstr(myStatus) =  cstr(objrsISSUB("opt_id")) then
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "' selected>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                else
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "'>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsISSUB.movenext
                            wend
                        end if
                        
                        
                    
                    end if
                
                objrsIS.movenext
            wend
        end if
        
        
end sub


Sub autogenSchoolYear(mySchoolYear)
    if isnull(mySchoolYear) then
        mySchoolYear = ""
    end if
    
    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_sch_year' and parent_id= '0' order by opt_id asc"
    'response.Write objcommand.commandtext
    'response.end    
    set objrsVI = objcommand.execute

        if not objrsVI.eof then 
            while not objrsVI.eof
                if cstr(mySchoolYear) =  cstr(objrsVI("opt_id")) then
                    response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "' selected='selected' >" &  objrsVI("opt_value") & "</option>"
                else
                    response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "'>" &  objrsVI("opt_value") & "</option>"
                end if
                
                 if objrsVI("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsVI("opt_id") & "' order by opt_value asc"
                        set objrsVISUB = objcommand.execute
                        
                        if not objrsVISUB.eof then 
                            while not objrsVISUB.eof
                               if cstr(mySchoolYear) =  cstr(objrsVISUB("opt_id")) then
                                    response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "' selected>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                else
                                    response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "'>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsVISUB.movenext
                            wend
                        
                        end if
                    
                    end if
                objrsVI.movenext
            wend
        end if
    end if

end sub


function getAccidentStatusForCode(myStatus)
  objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("parent_value")) > 0 then
                getAccidentStatusForCode = objrsIS("parent_value") & " > " & objrsIS("opt_value")
           else
                getAccidentStatusForCode = objrsIS("opt_value")
           end if                   
        else
           getAccidentStatusForCode =  "Details could not be found"
        end if
end function

function getAccidentStatusColourForCode(myStatus)
  objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                           " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("opt_colour")) > 0 and not isnull(objrsIS("opt_colour"))  then
                getAccidentStatusColourForCode = replace(objrsIS("opt_colour"), "#", "") 
           else
                getAccidentStatusColourForCode = ""
           end if                   
        else
           getAccidentStatusColourForCode = ""
        end if
end function



sub autogenHB_RP(selectedValue)

      
        
         objcommand.commandtext = "select opt_id, opt_value, opt_description, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pi_practice' and parent_id= '0' order by opt_value asc"
        set objrsIS = objcommand.execute

        if not objrsIS.eof then 
            while not objrsIS.eof
                if cstr(selectedValue) =  cstr(objrsIS("opt_id")) then
                    response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "' selected>" &  objrsIS("opt_value") & "</option>"
                else
                    response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
                end if
                
                 if objrsIS("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pi_practice' and parent_id= '" & objrsIS("opt_id") & "' order by opt_value asc"
                        set objrsISSUB = objcommand.execute

                        if not objrsISSUB.eof then 
                            while not objrsISSUB.eof
                               if cstr(selectedValue) =  cstr(objrsISSUB("opt_id")) then
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "' selected>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                else
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "'>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsISSUB.movenext
                            wend
                        end if
                        
                        
                    
                    end if
                
                objrsIS.movenext
            wend
        end if
        
        
end sub

function getHB_RPForCode(selectedValue)
  objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='pi_practice' and a.opt_id='" & clng(selectedValue) & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("parent_value")) > 0 then
                getHB_RPForCode = objrsIS("parent_value") & " > " & objrsIS("opt_value")
           else
                getHB_RPForCode = objrsIS("opt_value")
           end if                   
        else
           getHB_RPForCode =  "Details could not be found"
        end if
end function

function getHB_RPColourForCode(selectedValue)
  objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                           " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='pi_practice' and a.opt_id='" & clng(selectedValue) & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("opt_colour")) > 0 and not isnull(objrsIS("opt_colour"))  then
                getAccidentStatusColourForCode = replace(objrsIS("opt_colour"), "#", "") 
           else
                getAccidentStatusColourForCode = ""
           end if                   
        else
           getAccidentStatusColourForCode = ""
        end if
end function



function getTier1Name()
    
    objcommand.commandtext = "select struct_name from " & Application("DBTABLE_STRUCT_TIER1") & " where corp_code='" & session("corp_code") & "'"
    set objrsTIER1 = objcommand.execute
    
    if objrsTIER1.eof then 
        getTier1Name = "Error: Could not find name"
    else
        getTier1Name = objrsTIER1("struct_name")
    end if
    
end function

function getTier2Name(func_tier2_ref)
    
    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER2") & " where tier2_ref='" & func_tier2_ref & "' and corp_code='" & session("corp_code") & "'"
    set objrsTIER2 = objcommand.execute
    
    if objrsTIER2.eof then 
        getTier2Name = "Error: Could not find name"
    else
        getTier2Name = objrsTIER2("struct_name")
        if objrsTIER2("struct_deleted") = true then
            getTier2Name = getTier2Name & " (DISABLED)"
        end if
    end if
    
end function

function getTier3Name(func_tier3_ref)
    
    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER3") & " where tier3_ref='" & func_tier3_ref & "' and corp_code='" & session("corp_code") & "'"
    set objrsTIER3 = objcommand.execute
    
    if objrsTIER3.eof then 
        getTier3Name = "Error: Could not find name"
    else
        getTier3Name = objrsTIER3("struct_name")
        if objrsTIER3("struct_deleted") = true then
            getTier3Name = getTier3Name & " (DISABLED)"
        end if
    end if
    
end function

function getTier4Name(func_tier4_ref)
    
    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER4") & " where tier4_ref='" & func_tier4_ref & "' and corp_code='" & session("corp_code") & "'"
    set objrsTIER4 = objcommand.execute
    
    if objrsTIER4.eof then 
        getTier4Name = "Error: Could not find name"
    else
        getTier4Name = objrsTIER4("struct_name")
        if objrsTIER4("struct_deleted") = true then
            getTier4Name = getTier4Name & " (DISABLED)"
        end if
    end if    
end function

function getTier5Name(func_tier5_ref)
    
    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER5") & " where tier5_ref='" & func_tier5_ref & "' and corp_code='" & session("corp_code") & "'"
    set objrsTIER5 = objcommand.execute
    
    if objrsTIER5.eof then 
        getTier5Name = "Error: Could not find name"
    else
        getTier5Name = objrsTIER5("struct_name")
        if objrsTIER5("struct_deleted") = true then
            getTier5Name = getTier5Name & " (DISABLED)"
        end if
    end if    
end function


function getTier6Name(func_tier6_ref)
    
    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER6") & " where tier6_ref='" & func_tier6_ref & "' and corp_code='" & session("corp_code") & "'"
    set objrsTIER6 = objcommand.execute
    
    if objrsTIER6.eof then 
        getTier6Name = "Error: Could not find name"
    else
        getTier6Name = objrsTIER6("struct_name")
        if objrsTIER6("struct_deleted") = true then
            getTier6Name = getTier6Name & " (DISABLED)"
        end if
    end if    
end function


'Experiment for structure groups TP APR 2016
sub DisplayGroups()
       objCommand.commandText = "SELECT group_cat_code, group_cat_name FROM  MODULE_PEP.dbo.structure_group_cats " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY group_cat_name"
   
      
        set objRsGroupCats = objCommand.execute

        while not objRsGroupCats.eof 
            
                response.write "<tr><th  width='150px' nowrap>" & objRsGroupCats("group_cat_name") & "</th><td>"
    
                CALL DisplayGroupInfoExperiment("", "ra_frm_srch_group_" & objRsGroupCats("group_cat_code") , "", "S", "", objRsGroupCats("group_cat_code"), objRsGroupCats("group_cat_name"))

                response.write "</td></tr>"

            objRsGroupCats.movenext
        wend



end sub

sub DisplayGroups2()
       objCommand.commandText = "SELECT group_cat_code, group_cat_name FROM  MODULE_PEP.dbo.structure_group_cats " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY group_cat_name"
   
      
        set objRsGroupCats = objCommand.execute

        while not objRsGroupCats.eof 
            
                response.write "<tr><th style='width: 200px' nowrap>" & objRsGroupCats("group_cat_name") & "</th>"
    
                response.write "<td style='width: 100px'><select class='select' onchange='switchGroup(""" & objRsGroupCats("group_cat_code") & """, this)'><option value='ignore'>Ignore</option><option value='filter'>Filter</option></select></td>"
                response.write "<td><div style='display: none;' id='div_values_" & objRsGroupCats("group_cat_code") & "' >"
    
                CALL DisplayGroupInfoExperiment2("", "ra_frm_srch_group_" & objRsGroupCats("group_cat_code") , "", "S", "", objRsGroupCats("group_cat_code"), objRsGroupCats("group_cat_name"))

                response.write "</div></td></tr>"

            objRsGroupCats.movenext
        wend



end sub

sub DisplayGroups3()
       objCommand.commandText = "SELECT group_cat_code, group_cat_name FROM  MODULE_PEP.dbo.structure_group_cats " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY group_cat_name"
   
      
        set objRsGroupCats = objCommand.execute

        while not objRsGroupCats.eof 
            
                response.write "<tr><th  width='150px' nowrap>" & objRsGroupCats("group_cat_name") & "</th><td>"
    
                CALL DisplayGroupInfoExperiment2("", "ra_frm_srch_group_" & objRsGroupCats("group_cat_code") , "", "S", "", objRsGroupCats("group_cat_code"), objRsGroupCats("group_cat_name"))

                response.write "</td></tr>"

            objRsGroupCats.movenext
        wend



end sub

    

sub DisplayGroupInfoExperiment2(sub_group_value, sub_group_name, isDisabled, TypeSwitch, Javascript, cat_code, cat_name)

    'TypeSwitch options    
    'TypeSwitch  (S is for search engines)

    if isnull(sub_group_value) or len(sub_group_value) = 0 then sub_group_value = 0
    
    response.Write "<select  id='" & sub_group_name & "' name='" & sub_group_name & "' class=' sol' size='1' " & isDisabled & " " & Javascript & " multiple='multiple'>"
    
    objCommand.commandText = "SELECT group_code, group_name FROM " & Application("DBTABLE_STRUCT_GROUPS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' and group_cat_code = '" & cat_code & "'  " & _
                             "ORDER BY group_name"
    set objRsGroups = objCommand.execute
    if objRsGroups.EOF then
        if TypeSwitch <> "S" then
            response.Write "<option value='0'>No groups found</option>"
        end if
    else
        while not objRsGroups.EOF
            response.Write "<option value='" & objRsGroups("group_code") & "'"
            if objRsGroups("group_code") = sub_group_value then response.Write " selected "
            response.Write ">" & objRsGroups("group_name") & "</option>"
            
            objRsGroups.movenext
        wend
    end if
    
    response.Write "</select>"

end sub


sub DisplayGroupInfoExperiment(sub_group_value, sub_group_name, isDisabled, TypeSwitch, Javascript, cat_code, cat_name)

    'TypeSwitch options    
    'TypeSwitch  (S is for search engines)

    if isnull(sub_group_value) or len(sub_group_value) = 0 then sub_group_value = 0
    
    response.Write "<select name='" & sub_group_name & "' class='select' size='1' " & isDisabled & " " & Javascript & ">"
    
    if TypeSwitch = "S" then
        'search engine
        response.Write "<option value='0'>Any " & cat_name & "</option>"
    elseif TypeSwitch = "S_ALL" then
        response.Write "<option value='0'>All companies</option>"
    else
        response.Write "<option value='0'>Please select a group</option>"
    end if
    
    
    objCommand.commandText = "SELECT group_code, group_name FROM " & Application("DBTABLE_STRUCT_GROUPS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' and group_cat_code = '" & cat_code & "'  " & _
                             "ORDER BY group_name"
    set objRsGroups = objCommand.execute
    if objRsGroups.EOF then
        if TypeSwitch <> "S" then
            response.Write "<option value='0'>No groups found</option>"
        end if
    else
        while not objRsGroups.EOF
            response.Write "<option value='" & objRsGroups("group_code") & "'"
            if objRsGroups("group_code") = sub_group_value then response.Write " selected "
            response.Write ">" & objRsGroups("group_name") & "</option>"
            
            objRsGroups.movenext
        wend
    end if
    
    response.Write "</select>"

end sub

'End of Experiment

sub DisplayGroupInfo(sub_group_value, sub_group_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines)
    'sub_sw3 = Javascript
    'sub_sw4 = Empty
    'sub_sw5 = Empty

    if isnull(sub_group_value) or len(sub_group_value) = 0 then sub_group_value = 0
    
    response.Write "<select name='" & sub_group_name & "' class='select' size='1' " & sub_sw1 & " " & sub_sw3 & ">"
    
    if sub_sw2 = "S" then
        'search engine
        response.Write "<option value='0'>Any group</option>"
    elseif sub_sw2 = "S_ALL" then
        response.Write "<option value='0'>All companies</option>"
    else
        response.Write "<option value='0'>Please select a group</option>"
    end if
    
    
    objCommand.commandText = "SELECT group_code, group_name FROM " & Application("DBTABLE_STRUCT_GROUPS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY group_name"
    set objRsGroups = objCommand.execute
    if objRsGroups.EOF then
        if sub_sw2 <> "S" then
            response.Write "<option value='0'>No groups found</option>"
        end if
    else
        while not objRsGroups.EOF
            response.Write "<option value='" & objRsGroups("group_code") & "'"
            if objRsGroups("group_code") = sub_group_value then response.Write " selected "
            response.Write ">" & objRsGroups("group_name") & "</option>"
            
            objRsGroups.movenext
        wend
    end if
    
    response.Write "</select>"

end sub

sub DisplayTier2Info(sub_tier2_value, sub_tier2_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines, FI is for fire incidents)
    'sub_sw3 = Javascript
    'sub_sw4 = id
    'sub_sw5 = Empty

    if isnull(sub_tier2_value) or len(sub_tier2_value) = 0 then sub_tier2_value = 0
    if isnull(sub_sw2) or len(sub_sw2) = 0 then sub_sw2 = ""


    response.Write "<select name='" & sub_tier2_name & "' id='" & sub_sw4 & "' class='select' size='1' " & sub_sw1 & " " & sub_sw3 & ">"
    
    
    if sub_sw2 = "S" then
        'search engine
        response.Write "<option value='0'>Any " & session("CORP_TIER2") & "</option>"
    elseif sub_sw2 = "S_ALL" then
        response.Write "<option value='0'>All " & session("CORP_TIER2_PLURAL") & "</option>"
    else
        response.Write "<option value='0'>Please select a " & lcase(session("CORP_TIER2")) & "</option>"
    end if

    if sub_sw2 = "S" or sub_sw2 = "S_ALL" then
        objcommand.commandtext = "SELECT tier2_ref, struct_name, struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                                 "WHERE corp_code = '" & session("corp_code") & "' " & _
                                 "ORDER BY struct_name ASC"
    elseif sub_sw2 = "FI" then
        objcommand.commandtext = "SELECT DISTINCT tier2.tier2_ref, tier2.struct_name, tier2.struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER2") & " AS tier2 " & _
                                 "INNER JOIN " & Application("DBTABLE_HR_DATA_ABOOK") & " AS abooks " & _
                                 "  ON tier2.corp_code = abooks.corp_code " & _
                                 "  AND tier2.tier2_ref = abooks.rec_bu " & _
                                 "WHERE tier2.corp_code = '" & session("corp_code") & "' " & _
                                 "  AND (tier2.struct_deleted = 0 OR tier2.tier2_ref = '" & sub_tier2_value & "') " & _
                                 "ORDER BY tier2.struct_name ASC"
    else
        objcommand.commandtext = "SELECT tier2_ref, struct_name, struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                                 "WHERE corp_code = '" & session("corp_code") & "' " & _
                                 "  AND (struct_deleted = 0 OR tier2_ref = '" & sub_tier2_value & "') " & _
                                 "ORDER BY struct_name ASC"
    end if
    set objrsTIER2 = objcommand.execute
    
    if objrsTIER2.eof then 
        if sub_sw2 <> "S" then
            response.Write "<option value='0'>No " & lcase(session("CORP_TIER2_PLURAL")) & " found</option>"
        end if
    else
        while not objrsTIER2.eof
            if objrsTIER2("struct_deleted") = true then
                struct_deleted = " (DISABLED)"
            else
                struct_deleted = ""
            end if
            
            response.Write "<option value='" & objrsTIER2("tier2_ref") & "'"
            if cint(objrsTIER2("tier2_ref")) = cint(sub_tier2_value) then response.Write "  selected "
            response.Write ">" & objrsTIER2("struct_name") & struct_deleted & "</option>"

            objrsTIER2.movenext
        wend 
    end if
    
    response.Write "</select>"

end sub    


sub DisplayTier3Info(sub_tier3_value, sub_tier2_value, sub_tier3_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines, FI is for fire incident)
    'sub_sw3 = Javascript
    'sub_sw4 = id
    'sub_sw5 = Empty

    if isnull(sub_tier3_value) or len(sub_tier3_value)= 0 then sub_tier3_value = 0
    if isnull(sub_sw2) or len(sub_sw2) = 0 then sub_sw2 = ""


    response.Write "<select name='" & sub_tier3_name & "' id='" & sub_sw4 & "' class='select' size='1' " & sub_sw1 & " " & sub_sw3 & ">"
    
    
    if sub_sw2 = "S" then
        'search engine
        response.Write "<option value='0'>Any " & session("CORP_TIER3") & "</option>"
    elseif sub_sw2 = "S_ALL" then
        response.Write "<option value='0'>All " & session("CORP_TIER3_PLURAL") & "</option>"
    else
        response.Write "<option value='0'>Please select a " & lcase(session("CORP_TIER3")) & "</option>"
    end if
    
    
    if sub_sw2 = "S" or sub_sw2 = "S_ALL" then
        objcommand.commandtext = "SELECT tier3_ref, struct_name, struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                 "WHERE corp_code = '" & session("corp_code") & "' " & _
                                 "  AND tier2_ref = '" & sub_tier2_value & "' " & _
                                 "ORDER BY struct_name ASC"
    elseif sub_sw2 = "FI" then
        objcommand.commandtext = "SELECT DISTINCT tier3.tier3_ref, tier3.struct_name, tier3.struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER3") & " AS tier3 " & _
                                 "INNER JOIN " & Application("DBTABLE_HR_DATA_ABOOK") & " AS abooks " & _
                                 "  ON tier3.corp_code = abooks.corp_code " & _
                                 "  AND tier3.tier3_ref = abooks.rec_bl " & _
                                 "  AND tier3.tier2_ref = abooks.rec_bu " & _
                                 "WHERE tier3.corp_code = '" & session("corp_code") & "' " & _
                                 "  AND tier3.tier2_ref = '" & sub_tier2_value & "' " & _
                                 "  AND (tier3.struct_deleted = 0 OR tier3.tier3_ref = '" & sub_tier3_value & "') " & _
                                 "ORDER BY tier3.struct_name ASC"
    else
        objcommand.commandtext = "SELECT tier3_ref, struct_name, struct_deleted " & _
                                 "FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                 "WHERE corp_code = '" & session("corp_code") & "' " & _
                                 "  AND tier2_ref = '" & sub_tier2_value & "' " & _
                                 "  AND (struct_deleted = 0 OR tier3_ref = '" & sub_tier3_value & "') " & _
                                 "ORDER BY struct_name ASC"
    end if
    set objrsTIER3 = objcommand.execute
    
    if objrsTIER3.eof then 
        if sub_sw2 <> "S" then
            response.Write  "<option value='0'>No " & lcase(session("CORP_TIER3_PLURAL")) & " found</option>"
        end if
    else
        while not objrsTIER3.eof
            if objrsTIER3("struct_deleted") = true then
                struct_deleted = " (DISABLED)"
            else
                struct_deleted = ""
            end if
            
            response.Write  "<option value='" & objrsTIER3("tier3_ref") & "'"
            if cint(objrsTIER3("tier3_ref")) = cint(sub_tier3_value) then response.Write "  selected "
            response.Write  ">" & objrsTIER3("struct_name") & struct_deleted & "</option>"

            objrsTIER3.movenext
        wend 
    end if
    
    response.Write "</select>"

end sub    


sub DisplayTier4Info(sub_tier4_value, sub_tier3_value, sub_tier2_value, sub_tier4_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)
    
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines, SAS is for Safety Audit Search, SAF is for Safety Audit File, PRS is for PUWER Search, PRF is for PUWER File)
    'sub_sw3 = Javascript
    'sub_sw4 = id
    'sub_sw5 = Empty
    
    if isnull(sub_tier4_value) or len(sub_tier4_value)= 0 then sub_tier4_value = 0
    if isnull(sub_sw2) or len(sub_sw2) = 0 then sub_sw2 = ""


    
    response.Write "<select name='" & sub_tier4_name & "' id='" & sub_sw4 & "' class='select' size='1' " & sub_sw1 & " " & sub_sw3 & ">"
    
    
    if sub_sw2 = "S" then
        'search engine
        response.Write "<option value='0'>Any " & session("CORP_TIER4") & "</option>"
    elseif sub_sw2 = "S_ALL" then
        response.Write "<option value='0'>All " & session("CORP_TIER4_PLURAL") & "s</option>"
    elseif sub_sw2 = "SAS" or sub_sw2 = "PRS" then
        'Safety Audit search engine
        response.Write "<option value='0'>Any " & session("CORP_TIER4") & "</option>"
        
         if sub_tier2_value > 0 and sub_tier3_value > 0 then
              response.Write "<option value='x' "
                        
            if sub_tier4_value = "x" then
              sub_tier4_value = 0
              response.Write " selected  "
            end if
        
        response.Write ">All " & session("CORP_TIER4_PLURAL") & "</option>"
        
        end if
          	
    elseif sub_sw2 = "SAF" or sub_sw2 = "PRF" then
        'Safety Audit file
        response.Write  "<option value='0'>Please select a " & lcase(session("CORP_TIER4")) & "</option>" 
        
        if sub_tier2_value > 0 and sub_tier3_value > 0 then
            response.Write "<option value='x' "
                            
            if sub_tier4_value = "x" then
              sub_tier4_value = 0
              response.Write " selected  "
            end if
            response.Write ">All " & session("CORP_TIER4_PLURAL") & "</option>"
      
        end if
        
    else
        response.Write "<option value='0'>Please select a " & lcase(session("CORP_TIER4")) & "</option>"
    end if


    if sub_sw2 = "S" or sub_sw2 = "S_ALL" or sub_sw2 = "SAS" or sub_sw2 = "PRS" or sub_sw2 = "SAF" or sub_sw2 = "PRF" then
        objCommand.commandText = "SELECT a.tier4_ref, a.struct_name, a.struct_deleted FROM " & Application("DBTABLE_STRUCT_TIER4") & " AS a " & _
                                 "INNER JOIN " & Application("DBTABLE_STRUCT_TIER3") & " AS b " & _
                                 "  ON a.corp_code = b.corp_code " & _
                                 "  AND a.tier3_ref = b.tier3_ref " & _
                                 "WHERE a.corp_code = '" & session("corp_code") & "' " & _
                                 "  AND CAST(a.tier3_ref AS nvarchar(50)) = CAST('" & sub_tier3_value & "' AS nvarchar(50)) " & _
                                 "  AND CAST(b.tier2_ref AS nvarchar(50)) = CAST('" & sub_tier2_value & "' AS nvarchar(50)) " & _
                                 "ORDER BY a.struct_name asc"
    else
        'show all non-deleted departments AND the one being passed into the function so that saved records still display a department if it has been marked as deleted
        objCommand.commandText = "SELECT a.tier4_ref, a.struct_name, a.struct_deleted FROM " & Application("DBTABLE_STRUCT_TIER4") & " AS a " & _
                                 "INNER JOIN " & Application("DBTABLE_STRUCT_TIER3") & " AS b " & _
                                 "  ON a.corp_code = b.corp_code " & _
                                 "  AND a.tier3_ref = b.tier3_ref " & _
                                 "WHERE a.corp_code = '" & session("corp_code") & "' " & _
                                 "  AND CAST(a.tier3_ref AS nvarchar(50)) = CAST('" & sub_tier3_value & "' AS nvarchar(50)) " & _
                                 "  AND CAST(b.tier2_ref AS nvarchar(50)) = CAST('" & sub_tier2_value & "' AS nvarchar(50)) " & _
                                 "  AND (a.struct_deleted = 0 OR CAST(a.tier4_ref AS nvarchar(50)) = CAST('" & sub_tier4_value & "' AS nvarchar(50))) " & _
                                 "ORDER BY a.struct_name asc"
    end if
    set objrsTIER4 = objcommand.execute
    
    if objrsTIER4.eof then
        if sub_sw2 <> "S" then 
            response.Write "<option value='0'>No " & lcase(session("CORP_TIER4_PLURAL")) & " found</option>"
        end if
    else
    
    'to prevent any errors
    if len(sub_tier4_value) = 0 then sub_tier4_value = 0   
    
        while not objrsTIER4.eof
            if objrsTIER4("struct_deleted") = true then
                struct_deleted = " (DISABLED)"
            else
                struct_deleted = ""
            end if

            response.Write  "<option value='" & objrsTIER4("tier4_ref") & "'"
            if cint(objrsTIER4("tier4_ref")) = cint(sub_tier4_value) then response.Write "  selected "
            response.Write  ">" & objrsTIER4("struct_name") & struct_deleted & "</option>"

            objrsTIER4.movenext
        wend 
    end if
    
    response.Write "</select>"

end sub   


function getDefaultTier2(existingValue, resetValue)
    if session("CORP_TIER_SEARCH_DEFAULT") = "Y" and ((existingValue = "" or len(existingValue) = 0 or isnull(existingValue)) or resetValue = "Clear") then
        getDefaultTier2 = session("YOUR_COMPANY")
    else
        getDefaultTier2 = existingValue
    end if
end function

function getDefaultTier3(existingValue, resetValue)
    if session("CORP_TIER_SEARCH_DEFAULT") = "Y" and ((existingValue = "" or len(existingValue) = 0 or isnull(existingValue)) or resetValue = "Clear") then
        getDefaultTier3 = session("YOUR_LOCATION")
    else
        getDefaultTier3 = existingValue
    end if
end function

function getDefaultTier4(existingValue, resetValue)
    if session("CORP_TIER_SEARCH_DEFAULT") = "Y" and ((existingValue = "" or len(existingValue) = 0 or isnull(existingValue)) or resetValue = "Clear") then
        getDefaultTier4 = session("YOUR_DEPARTMENT")
    else
        getDefaultTier4 = existingValue
    end if
end function

function getDefaultTier5(existingValue, resetValue)
    if session("CORP_TIER_SEARCH_DEFAULT") = "Y" and ((existingValue = "" or len(existingValue) = 0 or isnull(existingValue)) or resetValue = "Clear") then
        getDefaultTier5 = session("YOUR_TIER5")
    else
        getDefaultTier5 = existingValue
    end if
end function

function getDefaultTier6(existingValue, resetValue)
    if session("CORP_TIER_SEARCH_DEFAULT") = "Y" and ((existingValue = "" or len(existingValue) = 0 or isnull(existingValue)) or resetValue = "Clear") then
        getDefaultTier6 = session("YOUR_TIER6")
    else
        getDefaultTier6 = existingValue
    end if
end function


sub DisplayUSERInfo(sub_user_value, sub_user_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5, sub_sw6)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine) (RV is for review tasks)
    'sub_sw3 = Javascript
    'sub_sw4 = Show disabled users
    'sub_sw5 = if instr hide these users: (TM - Task manager, RO - Read Only)
    'sub_sw6 = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox

    if len(sub_sw6) > 0 then
        var_multiple = " size='" & sub_sw6 & "' multiple" 
    else
        var_multiple = " size='1'"
    end if

    Response.Write  "<select name='" & sub_user_name & "' id='" & sub_user_name & "' class='select' " & sub_sw1 & " " & sub_sw3 & var_multiple & ">"
                    
                    select case sub_sw2 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>" 

                            if sub_sw4 <> "N" then
                                var_show_disabled_users = true
                            end if
                        case "T"
                          response.Write    "<option value='0'>Select person</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"
                            var_show_disabled_users = false
                        case "N"
                            'do nothing
                            var_show_disabled_users = false
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if
                            var_show_disabled_users = false
                        case "RV"
                            response.Write    "<option value='0'>Please select a user</option>"
                            If Session("ra_process_control") <> "Y" Then
                                Response.write "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                             response.write ">One off assessment</option>"      
                            End If                   
                            var_show_disabled_users = false
                        case else
                            response.Write    "<option value='0'>Please select a user</option>"
                            var_show_disabled_users = false       
                    end select 
        
    if  lcase(sub_sw1) = "disabled" then 
        objCommand.Commandtext = "SELECT distinct data.Acc_Ref, display_name  as user_details, acc_status FROM " & Application("DBTABLE_USER_DATA") & " as data  " & _
                                 " where data.corp_code='" & Session("corp_code") & "' AND data.Acc_level NOT LIKE '5' and acc_ref = '" & sub_user_value & "' " 
    else 
         '                   module_hr.dbo.fn_return_user_details(data.corp_code, data.acc_ref)
	    objCommand.Commandtext = "SELECT distinct data.Acc_Ref, display_name  as user_details, acc_status FROM " & Application("DBTABLE_USER_DATA") & " as data " & _
	                           "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                          "on data.corp_code = tier4.corp_code " & _
                           "and data.corp_bd = cast(tier4.tier4_ref as varchar(30)) "    

        if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") and session("global_usr_perm") <> "Y" then
            'do nothing

        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
            objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                              "  ON data.corp_code = org.corp_code " & _
                                                              "  AND data.acc_ref = org.user_acc_ref "
        end if

        if session("user_perms_active") = 1 and session("YOUR_ACCESS") <> "0" and (session("tm_limit_assessor_tier2") = "Y") then
            objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" & trim(session("CORP_CODE")) & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
							                                  "  on data.corp_bu = perms.tier2_ref " & _
							                                  "  and perms.perm_read = 1 "
        end if

        objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


        if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
            objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
        end if
	
	    if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") and session("global_usr_perm") <> "Y" then
            objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" and session("global_usr_perm") <> "Y"  then
	        objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	    end if
                    
                    if sub_sw4 = "N" or var_show_disabled_users = false then
                        'objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                        objCommand.commandText = objCommand.commandText & " AND (data.acc_status <> 'D' OR (data.acc_status = 'D' AND data.acc_ref = '" & sub_user_value & "')) "
                    end if 
                    
                    if instr(sub_sw5, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(sub_sw5, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                    
          objCommand.Commandtext = objCommand.Commandtext & "  ORDER BY data.user_details ASC"
          
    end if         
 
    response.write objcommand.commandtext

	SET objRsuser = objCommand.Execute
	
   	'While NOT objRsuser.EOF
    '    
    '    if objRsuser("acc_status") <> "D" or (var_show_disabled_users = true and objRsuser("acc_status") = "D") or (instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 and objRsuser("acc_status") = "D") then
   ' 
   '         Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
   '         If instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
   '             Response.Write " selected"
   '         End if
   '         if objRsuser("acc_status") = "D" then
   '             response.write " style='color:#B0B0B0'"
   '         end if
   '         Response.Write ">" & objRsuser("user_details") & "</option>"
    '    
    '    end if

    '    objRsuser.movenext
	'Wend


    if not objRsuser.eof then
        users = objRsuser.getrows()
        users_eof = false
    end if

    acc_ref = 0
    display_name = 1
    acc_status = 2

    if not users_eof then

        'response.write users(acc_status,1)

        for record=0 to UBound(users,2)
            if CStr(users(acc_status,record)) <> "D" or (var_show_disabled_users = true and CStr(users(acc_status,record)) = "D") or (instr(1,sub_user_value,users(acc_ref,record)) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 and CStr(users(acc_status,record)) = "D") then
    
                Response.Write "<option value='" & users(acc_ref,record) & "'"
                If instr(1,sub_user_value,users(acc_ref,record)) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
                    Response.Write " selected"
                End if
                if users(acc_ref,record) = "D" then
                    response.write " style='color:#B0B0B0'"
                end if
                Response.Write ">" & users(display_name,record) & "</option>"
        
            end if
        next
    end if


    response.Write "</select>"
end sub


SUB DisplayUSERInfo_restricted(sub_user_value, sub_user_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5, sub_sw6, sub_sw7)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_E for LogBook Viewers)
    'sub_sw3 = Javascript
    'sub_sw4 = Show disabled users
    'sub_sw5 = if instr hide these users: (TM - Task manager, RO - Read Only)
    'sub_sw6 = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox    
    'sub_sw7 = restricted user list (comma seperated list which becomes an array in this function
    
    if len(sub_sw6) > 0 then
        var_multiple = " size='" & sub_sw6 & "' multiple" 
    else
        var_multiple = " size='1'"
    end if
    
    Response.Write  "<select name='" & sub_user_name & "' class='select' " & sub_sw1 & " " & sub_sw3 & var_multiple & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Select person</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"
                        case "N"
                            'do nothing
                        case "LB_E"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All viewers</option>"                           
                            else
                                response.Write    "<option value='0'>All viewers</option>"       
                            end if

                        case else
                            response.Write    "<option value='0'>Please select a user</option>"       
                    end select 

    if  lcase(sub_sw1) = "disabled" then 
        objCommand.Commandtext = "SELECT distinct data.Acc_Ref, display_name  as user_details FROM " & Application("DBTABLE_USER_DATA") & " as data  " & _
                                 " where data.corp_code='" & Session("corp_code") & "' AND data.Acc_level NOT LIKE '5' and acc_ref = '" & sub_user_value & "' " 
    else 

	    objCommand.Commandtext = "SELECT data.Acc_Ref,data.Per_fname,data.Per_sname, tier4.struct_name FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                        "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                          "on data.corp_code = tier4.corp_code " & _
                           "and data.corp_bd = tier4.tier4_ref "     
    
        if session("user_perms_active") = 1 and session("YOUR_ACCESS") <> "0" and (session("tm_limit_assessor_tier2") = "Y") then
            objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" & trim(session("CORP_CODE")) & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
							                                  "  on data.corp_bu = perms.tier2_ref " & _
							                                  "  and perms.perm_read = 1 "
        end if       
        
        objCommand.commandText = objCommand.commandText & "  WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "
                           
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if 
                    
                    if instr(sub_sw5, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(sub_sw5, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                    
                    if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                        objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
                    end if
                    
                    ' restrict user list
                    if sub_sw7 <> "0" then
                        var_user_arry = split(sub_sw7, ", ")
                        for j = 0 to ubound(var_user_arry)
                            if len(var_sql_addition) > 0 then
                                var_sql_addition = var_sql_addition & " OR data.acc_ref = '" & var_user_arry(j) & "' "
                            else
                                var_sql_addition = "data.acc_ref = '" & var_user_arry(j) & "' "
                            end if 
                        next
                        if len(var_sql_addition) > 0 then
                            objCommand.commandText = objCommand.commandText & " AND (" & var_sql_addition & ") "
                        end if
                    end if
                    
                    objCommand.commandText = objCommand.commandText & "ORDER BY data.per_sname"

    end if

	SET objRsuser = objCommand.Execute
	
   	While NOT objRsuser.EOF
        Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
        'If cstr(objRsuser("Acc_ref")) = cstr(sub_user_value) Then
        If instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
            'response.Write "..." & cstr(objRsuser("Acc_ref")) & "..." & cstr(sub_user_value) & "...<br />"
            Response.Write " selected"
        End if
        Response.Write ">" & Ucase(objRsuser("Per_sname")) & ", " & objRsuser("Per_fname") & " (" & objRsuser("struct_name") & ")</option>"
        
        objRsuser.movenext
	Wend
    response.Write "</select>"
    
END SUB


sub DisplayUSERInfo_location(sub_user_value, sub_user_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5, sub_sw6, sub_sw7, sub_sw8, sub_sw9, sub_sw10, sub_sw11, sub_sw12)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) (T is for task user lists) (N for no default text) (LB_V for logbook viewers) (ST is for tasklist search engine) (RV for new review dates)
    'sub_sw3 = Javascript
    'sub_sw4 = Show disabled users
    'sub_sw5 = if instr hide these users: (TM - Task manager, RO - Read Only)
    'sub_sw6 = dropdown or listbox - value passed is the number of rows to display. anything greater than 1 will produce a multiselect listbox
    'sub_sw7 = company
    'sub_sw8 = location
    'sub_sw9 = UNUSED
    'sub_sw10 = UNUSED
    'sub_sw11 = UNUSED
    'sub_sw12 = UNUSED
    
    if len(sub_sw6) > 0 then
        var_multiple = " size='" & sub_sw6 & "' multiple" 
    else
        var_multiple = " size='1'"
    end if
   
    Response.Write  "<select name='" & sub_user_name & "' class='select' " & sub_sw1 & " " & sub_sw3 & var_multiple & ">"
                    
                    select case sub_sw2 
                        case "S", "ST"
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Select person</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"
                        case "N"
                            'do nothing
                        case "LB_V"
                            if cstr(sub_user_value) = "0" then
                                response.Write    "<option value='0' selected>All users</option>"                           
                            else
                                response.Write    "<option value='0'>All users</option>"       
                            end if
                    case "RV"
                            response.Write    "<option value='0'>Please select a user</option>"  & _  
                                            "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected "
                                             response.write ">One off assessment</option>"                               
                        case else
                            response.Write    "<option value='0'>Please select a user</option>"       
                    end select 
                   
     if  lcase(sub_sw1) = "disabled" then 
        objCommand.Commandtext = "SELECT data.Acc_Ref,data.Per_fname,data.Per_sname, tier4.struct_name, 0 AS queryOrder FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                 "  on data.corp_code = tier4.corp_code " & _
                                 "  and CAST(data.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) " & _
                                 " where data.corp_code='" & Session("corp_code") & "' AND data.Acc_level NOT LIKE '5' and acc_ref = '" & sub_user_value & "' " 
    else 

	    objCommand.Commandtext = "SELECT data.Acc_Ref,data.Per_fname,data.Per_sname, tier4.struct_name, 0 AS queryOrder FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                             "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                 "  on data.corp_code = tier4.corp_code " & _
                                 "  and CAST(data.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) "

        if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            'do nothing
        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
            objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                              "  ON data.corp_code = org.corp_code " & _
                                                              "  AND data.acc_ref = org.user_acc_ref "
        end if

        if session("user_perms_active") = 1 and session("YOUR_ACCESS") <> "0" and (session("tm_limit_assessor_tier2") = "Y") then
            objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" & trim(session("CORP_CODE")) & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
							                                  "  on data.corp_bu = perms.tier2_ref " & _
							                                  "  and perms.perm_read = 1 "
        end if

        objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


        if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
            objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
        end if
	
	    if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
            objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
        elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
	        objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	    end if
                    
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if 
                    
                    if instr(sub_sw5, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(sub_sw5, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                    
        
        if len(sub_sw7) > 0 and sub_sw7 <> "0" then
            objCommand.commandText = objCommand.commandText & " AND data.corp_bu = '" & sub_sw7 & "' " & _
                                                              " AND data.acc_level <> '0' "
        end if
        if len(sub_sw8) > 0 and sub_sw8 <> "0" then
            objCommand.commandText = objCommand.commandText & " AND data.corp_bl = '" & sub_sw8 & "' " & _
                                                              " AND data.acc_level <> '0' "
        end if


        
        
        if len(sub_sw7) > 0 or len(sub_sw8) > 0 then
            objCommand.commandText = objCommand.Commandtext & " UNION SELECT data.Acc_Ref,data.Per_fname,data.Per_sname, tier3.struct_name, 1 AS queryOrder FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                                 "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3") & "  AS tier3 " & _
                                     "  on data.corp_code = tier3.corp_code " & _
                                     "  and CAST(data.corp_bl AS nvarchar(50)) = CAST(tier3.tier3_ref AS nvarchar(50)) "
    
            if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                'do nothing
            elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
                objCommand.commandText = objCommand.commandText & "LEFT OUTER JOIN " & Application("DBTABLE_HR_ORG_STRUCTURE") & " AS org " & _
                                                                  "  ON data.corp_code = org.corp_code " & _
                                                                  "  AND data.acc_ref = org.user_acc_ref "
            end if

            objCommand.Commandtext = objCommand.Commandtext & "WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "


            if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
            end if
    	
	        if sub_sw2 = "ST" and (session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y") then
                objCommand.commandText = objCommand.commandText & "  and tier4.tier3_ref = '" & session("YOUR_LOCATION") & "' "
            elseif sub_sw2 = "ST" and session("YOUR_ACCESS") <> "0" then
	            objCommand.commandText = objCommand.commandText & "  AND (org.sup_acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' OR data.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') "
	        end if
                    
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    end if  
            
            objCommand.commandText = objCommand.commandText & " AND data.acc_level = '0' "   
            
            objCommand.commandText = objCommand.commandText & " ORDER BY queryOrder ASC, data.Per_sname ASC"
            
        else
            objCommand.Commandtext = objCommand.Commandtext & " ORDER BY data.Per_sname ASC"
        end if
        
    end if    

        
	SET objRsuser = objCommand.Execute
	
	var_list_order = ""
	
   	While NOT objRsuser.EOF
   	    
   	    if var_list_order <> objRsuser("queryOrder") then
   	        if var_list_order <> "" or (var_list_order = "" and objRsuser("queryOrder") = "1") then
   	            response.Write "<option value='0'> </option><option value='0'> ---- Corporate Administrators ---- </option>"
   	        end if
   	        var_list_order = objRsuser("queryOrder")
   	    end if
   	    
        Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
        'If cstr(objRsuser("Acc_ref")) = cstr(sub_user_value) Then
        If instr(1,sub_user_value,objRsuser("Acc_ref")) > 0 and cstr(sub_user_value) <> "0" and len(sub_user_value) > 0 then
            'response.Write "..." & cstr(objRsuser("Acc_ref")) & "..." & cstr(sub_user_value) & "...<br />"
            Response.Write " selected"
        End if
        Response.Write ">" & Ucase(objRsuser("Per_sname")) & ", " & objRsuser("Per_fname") & " (" & objRsuser("struct_name") & ")</option>"
        
        objRsuser.movenext
	Wend
    response.Write "</select>"
end sub


sub DisplayUSERInfo_custom(var_name, var_id, var_value, var_user_type, var_disabled, var_jscript, var_default_val, var_default_text, var_default2_val, var_default2_text)

    if ucase(var_disabled) = "Y" or var_disabled = "disabled" then
        var_disabled = " disabled " 
    else
        var_disabled = "" 
    end if
        
    select case var_user_type
        case 1
            sql_variables = " and acc_status <> 'D'"
        case else
            sql_variables = ""
    end select

    Response.Write "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_jscript & ">" 
    if var_value = var_default_val  then              
        response.Write "<option value='" & var_default_val & "' selected='selected'>" & var_default_text & "</option>" 
    else      
        response.Write "<option value='" & var_default_val & "'>" & var_default_text & "</option>" 
    end if
              
    if var_value = var_default2_val  then              
        response.Write "<option value='" & var_default2_val & "' selected='selected'>" & var_default2_text & "</option>" 
    else      
        response.Write "<option value='" & var_default2_val & "'>" & var_default2_text & "</option>" 
    end if
              
    
    if  ucase(var_disabled) = "Y" or var_disabled = "disabled" then 
        objCommand.Commandtext = "SELECT distinct data.Acc_Ref, display_name  as user_details FROM " & Application("DBTABLE_USER_DATA") & " as data  " & _
                                 " where data.corp_code='" & Session("corp_code") & "' AND data.Acc_level NOT LIKE '5' and acc_ref = '" & var_value & "' " 
    else       

	    objCommand.Commandtext = "SELECT hr.Acc_Ref,hr.Per_fname,hr.Per_sname, tier4.struct_name FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _
	                             " LEFT OUTER JOIN " &  Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                                 " ON hr.corp_code = tier4.corp_code AND cast(hr.corp_bd as varchar(25)) = cast(tier4.tier4_ref as varchar(25)) "

        if session("user_perms_active") = 1 and session("YOUR_ACCESS") <> "0" and (session("tm_limit_assessor_tier2") = "Y") then
            objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" & trim(session("CORP_CODE")) & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
							                                  "  on hr.corp_bu = perms.tier2_ref " & _
							                                  "  and perms.perm_read = 1 "
        end if

        objCommand.commandText = objCommand.commandText & " WHERE hr.corp_code='" & Session("corp_code")& "' AND hr.Corp_BG='" & Session("YOUR_GROUP") & "' AND hr.Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY hr.Per_sname ASC"
    end if
    SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & ucase(objRs("Per_sname")) & " " & objRs("Per_fname") & " (" & objRs("struct_name") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"

end sub




sub Pers_Affected_report(mod_type, ref, ccode, sub_ref)

    var_pa_ref = ref
    var_pa_mod = mod_type
    var_pa_corp_code = ccode ' written this way to allow for archives
    var_pa_sub_ref = sub_ref
    
    select case var_pa_mod
        case "RA_T" 'RA Template
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_RAMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "' and a.temp_version = '" & sub_ref & "'  order by position, opt_name "      
         case "RA"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_RAMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "' order by position, opt_name "      
     '   case "CCERA"
      '        objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_CCERAMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  and a.haz_id = '" & var_pa_sub_ref & "' order by position, opt_name "      
        case "MH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.position  from " & Application("DBTABLE_MHMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.person_type = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "FS"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.position  from " & Application("DBTABLE_FSMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.person_type = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "' and a.loc_ref = '" & var_pa_sub_ref & "'  order by position, opt_name "      
        case "COSHH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  from " & Application("DBTABLE_COSHH_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "CAT"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  from " & Application("DBTABLE_COSHH_TEMPLATE_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "SIRA"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  from " & Application("DBTABLE_SURA_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
          case "MS"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_MS_PA_V2") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b  on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE b.corp_code = '" & session("corp_code") & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
      case else
            response.Write "Incorrect module code. Please contact support via the help tab."
            exit sub
    end select

    SET objrspaff = objcommand.execute
        
    if objrspaff.eof then
    
    Response.Write  "<div class='warning'>" & _
                    "<table width='100%' cellpadding='2px' cellspacing='2px'>" & _
                    "<tr><td height='40'><strong>WARNING</strong><br />There are currently NO persons affected that have been identified in this "
                    if var_pa_mod = "FS" then
                        Response.Write "zone"
                    else
                        Response.Write "assessment" 
                    end if
    Response.Write  ".</td></tr>" & _
                    "</table>" & _
                    "</div>"
    
    else            
        while not objrspaff.eof
            ' if var_pa_mod = "CCERA" then
             if session("pa_checkbox") = "Y" then
     response.Write  "<div class='floatdiv'><table width='180' cellpadding='1px' cellspacing='2px' class='showtd'><tr><th width='210'>" & objrspaff("col_persons") & "</td>" & _                     
                    "</tr></table></div>"
             else
                response.Write  "<div class='floatdiv'><table width='220' cellpadding='1px' cellspacing='2px' class='showtd'><tr><th width='210'>" & objrspaff("col_persons") & "</td>" & _                
                    "<td width='40' nowrap>x " & objrspaff("col_persons_amount") & "</td>" & _           
                    "</tr></table></div>"
             end if
            objrspaff.Movenext
        Wend    
        response.Write "<div style='clear: both'></div>"                   
    end if 


end sub

SUB Pers_Affected_Search(sub_name, sub_val, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)
    
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Empty
    'sub_sw3 = Javascript
    'sub_sw4 = Empty
    'sub_sw5 = Empty


    objcommand.commandtext = "select opt_name, opt_ref from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " where corp_code='" & session("corp_code") & "' order by position, opt_name "
    set objrs = objcommand.execute

	Response.Write("<select name='" & sub_name & "' class='select'>")
	Response.Write(vbCrLf & "  <option value='0'>Any person affected</option>")
	
    while not objrs.eof 
        Response.Write(vbCrLf & "  <option value='" & objrs("opt_ref") & "' ")
        
        if cstr(sub_val) = objrs("opt_ref") then
             response.write " selected "
        end if
         Response.Write("  >" & objrs("opt_name") & "</option>")
        objrs.movenext
    wend
		
	Response.Write(vbCrLf & "</select>")

END SUB


SUB Pers_Affected_file(sub_name, mod_type, sub_sw1, var_pa_ref, sub_sw3, var_pa_sub_ref, sub_sw5, sub_sw6)

 'sub parameters names
    'sub_sw1 = Disabled
    'var_pa_ref = reference 
    'sub_sw3 = Javascript
    'var_sub_pa_ref = sub ref for fire zones
    'sub_sw5 = Empty
    'sub_sw6 = Empty
    
    var_pa_mod = mod_type
    
 if  var_pa_mod = "FS" then
    var_text = "<strong>MAXIMUM</strong>"
 else
    var_text = "average"
 end if
    
 if session("pa_checkbox") = "Y" then
   response.Write "<p>Please identify the persons affected by this activity</p>"
 else
    response.Write "<p>Please provide " & var_text & " amounts of persons affected for each type that is relevant below</p>"
 end if
  response.Write  "<table width='100%' cellpadding='1px' cellspacing='2px'>" & _
        "<tr>"

        
    select case var_pa_mod
        case "RA"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_RAMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
    '    case "CCERA"
     '       if len(var_pa_sub_ref) > 0 then
      '        objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_CCERAMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' and a.haz_id = '" & var_pa_sub_ref & "' WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
       '     else
        '     objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b  WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
        '   end if
        case "MH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_MHMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.person_type = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "FS"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_FSMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.person_type = b.opt_ref and a.recordreference = '" & var_pa_ref & "' and a.loc_ref = '" & var_pa_sub_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "COSHH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_COSHH_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
       case "CAT"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_COSHH_TEMPLATE_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "SIRA"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_SURA_PERSONS") & " as a  on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "MS"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.opt_ref, b.position  FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_MS_PA_V2") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
        case else
            response.Write "<td>Incorrect module code. Please contact support via the help tab.</td></tr></table>"
            exit sub
    end select
  
  set objrs = objcommand.execute 

  while not objrs.eof
        
            if row_count = 3 then
                response.Write "</tr>"
                response.Write "<tr>"
                row_count = 0
            end if
                
            row_count = row_count + 1
            
            if isnull(objrs("col_persons_amount")) then
                var_paff_value = "0"
            else
                var_paff_value = objrs("col_persons_amount")
            end if
             
            response.Write "<th>" & objrs("col_persons") & "</th><td>"
    '        if var_pa_mod = "CCERA" then 
            if session("pa_checkbox") = "Y" then
                response.write "<input type='checkbox'  class='checkbox' name='" & sub_name & objrs("opt_ref") & "' value='1' "
                if var_paff_value > 0 then response.write " checked "
                response.write  sub_sw1 & " />"
            else
                response.write "<input type='text' size='3' class='text' name='" & sub_name & objrs("opt_ref") & "' value='" & var_paff_value & "' " & sub_sw1 & "/>"
            end if
            
            response.write "</td>"
    objrs.movenext
   wend
        
         if row_count = 3 then
            response.Write "</tr>"
         else
            for i = row_count to 3
                 response.Write "<td></td>"
            next
            response.Write "</tr>"
         end if
    response.Write "</table>"
END SUB



sub autogenActivityFrequency(fieldName, myFrequency, outputStyle, var_javascript, var_disabled)
    
    
    if outputStyle <> "report" then
        response.write "<select class='select' name='" & fieldName & "' " & var_javascript & " " & var_disabled & ">"
        
        if outputStyle = "form" then
            response.Write "<option value='na'>Please select</option>"
        elseif outputStyle = "search" then
            response.Write "<option value='all'>All Activity Frequencies</option>"
        end if
        
        objCommand.commandText = "SELECT opt_name, opt_ref FROM " & "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" & " where corp_code='" & session("CORP_CODE") & "' and related_to='frequency' order by opt_order asc"
        set objRs = objCommand.execute

        if not objRs.EOF then 
            while not objRs.eof
                if cstr(myFrequency) =  cstr(objRs("opt_ref")) then
                    response.Write  "<option value='" & objRs("opt_ref") & "' selected>" &  objRs("opt_name") & "</option>"
                else
                    response.Write  "<option value='" & objRs("opt_ref") & "'>" &  objRs("opt_name") & "</option>"
                end if

                objRs.movenext
            wend
        end if
        set objRs = nothing
        
        response.Write "</select>"
    else
        objCommand.commandText = "SELECT opt_name FROM " & "Module_GLOBAL.dbo.GLOBAL_Risk_Assessment_Opts" & " where corp_code='" & session("CORP_CODE") & "' and opt_ref = '" & myFrequency & "' and related_to='frequency' order by id asc"
        set objRs = objCommand.execute
        if not objRs.EOF then
            response.Write objRs("opt_name")
        else
            response.Write ""
        end if
        set objRs = nothing
    end if
end sub

sub showCustomMatrixGrid(showBoundingDiv, showScoreDescriptions)

    if showBoundingDiv = true then
        if showScoreDescriptions = true then
            response.write "<div id='divMatrix' style='display:block; margin:25px auto 10px auto; width: 850px;'>"
        else
            response.write "<div id='divMatrix' style='display:block; margin:25px auto 10px auto; width: 550px;'>"
        end if
    end if

    'output a risk matrix
    Dim arrCellsDesc
    Set arrCellsDesc=Server.CreateObject("Scripting.Dictionary")

    Dim arrCellsColour
    Set arrCellsColour = Server.CreateObject("Scripting.Dictionary")

    objCommand.commandText = "SELECT overall.severity_value, overall.likelihood_value, overall.[description], overall.graph_colour, overall.severity_value*overall.likelihood_value AS grid_score " & _
                             "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                             "WHERE overall.corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY overall.likelihood_value, overall.severity_value"
    set objRsOverall = objCommand.execute
    if not objRsOverall.EOF then

        while not objRsOverall.EOF

            cellKey = objRsOverall("severity_value") & "_" & objRsOverall("likelihood_value") & "_" & objRsOverall("grid_score")
            cellItemDesc = objRsOverall("description")
            cellItemColour = objRsOverall("graph_colour")

            arrCellsDesc.Add cellKey, cellItemDesc
            arrCellsColour.Add cellKey, cellItemColour

            objRsOverall.movenext
        wend
    end if
    set objRsOverall = nothing

    objCommand.commandText = "SELECT COUNT(title) AS num_sev FROM " & Application("GLOBAL_RISK_MATRIX_severity") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0 "
    set objRsSev = objCommand.execute
    var_sev_count = objRsSev("num_sev")

    objCommand.commandText = "SELECT COUNT(title) AS num_lik FROM " & Application("GLOBAL_RISK_MATRIX_likelihood") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0 "
    set objRsLik = objCommand.execute
    var_lik_count = objRsLik("num_lik")


    objCommand.commandText = "SELECT title, score, [description] " & _
                             "FROM " & Application("GLOBAL_RISK_MATRIX_severity") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0 " & _
                             "ORDER BY score"

    set objRsSev = objCommand.execute

    objCommand.commandText = "SELECT title, score, [description] " & _
                             "FROM " & Application("GLOBAL_RISK_MATRIX_likelihood") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "ORDER BY score"
    set objRsLik = objCommand.execute

    if not objRsSev.EOF then

        response.write "<style> .rotate { " & _
                       " -moz-transform: rotate(-90.0deg);  /* FF3.5+ */ " & _
                       " -o-transform: rotate(-90.0deg);  /* Opera 10.5 */ " & _
                       " -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */ " & _
                       " filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */ " & _
                       " -ms-filter: ""progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)""; /* IE8  " & _
                       " -ms-transform:rotate(-90deg); /* IE 9 */ " & _
                       " transform: rotate(-90deg); " & _
                       "} " & _
                       " .matrix_header { " & _
                       " font-size:1.3em; " & _
                       "} </style>"

        response.write "<div><div style='float:left;'>"

        response.write "<table>"

        response.write "<tr><td colspan='3'>&nbsp;</td><td colspan='" & var_sev_count  & "' class='matrix_header'><strong>" & session("RISK_MATRIX_SEV") & "</strong></td></tr>"

        table_row = ""
        table_row1 = ""

        while not objRsSev.EOF
           
            table_row = table_row & "<td class='c' title='" & objRsSev("description") & "'>" & objRsSev("score") & "</td>"
            table_row1 = table_row1 & "<td style='width:70px;' class='c' title='" & objRsSev("description") & "'><strong>" & objRsSev("title") & "</strong></td>"
            'writing-mode: tb-rl;

            objRsSev.movenext
        wend

        response.write "<tr><td colspan='3'>" & table_row & "</tr>"
        response.write "<tr><td colspan='3'>" & table_row1 & "</tr>"

        counter = 1
        row_head = ""

        objRsSev.movefirst

        while not objRsSev.EOF

            if not objRsLik.EOF then

                table_row = ""

                if counter = 1 then
                    row_head = "<td rowspan='" & var_lik_count & "' class='rotate matrix_header'><strong>" & session("RISK_MATRIX_LIKE") & "</strong></td>"
                end if
        
                while not objRsLik.EOF

                    if counter = cint(objRsLik("score")) then
                        row_head = row_head & "<td class='c' title='" & objRsSev("description") & "'>" & objRsLik("score") & "</td><td class='r' title='" & objRsSev("description") & "'><strong>" & objRsLik("title") & "</strong></td>"
                    end if

                    cell_score = cint(objRsSev("score")) * cint(objRsLik("score"))

                    cell_details = arrCellsDesc.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
                    cell_colour = arrCellsColour.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
    
                    table_row = table_row & "<td class='c' style='background-color:#" & cell_colour & ";' title='" & cell_details & "'>" & cell_score & "</td>"

                    objRsLik.movenext
                wend

                table_row = "<tr style='height:30px;'>" & row_head & table_row & "</tr>"

                objRsLik.movefirst
            end if

            response.write table_row

            row_head = ""
            counter = counter + 1
            objRsSev.movenext

        wend

        response.write "</table>"

        response.write "</div>"



        if showScoreDescriptions = true then
            response.write "<div style='float:left; padding-left:30px; padding-top:46px;'>"

            objCommand.commandText = "SELECT DISTINCT overall.icon_rating, overall.[value], CAST(overall.[description] AS nvarchar(MAX)) AS overall_desc, overall.graph_colour " & _
                                     "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                                     "WHERE overall.corp_code = '" & session("CORP_CODE") & "' and overall.severity_value <> 0 " & _
                                     "ORDER BY overall.icon_rating "
            set objRsOverall = objCommand.execute
            if not objRsOverall.EOF then

                response.write "<table>"

                response.write "<tr><td colspan='2' class='matrix_header l'><strong>Risk Level</strong></td></tr>"

                while not objRsOverall.EOF 
                        
                    response.write "<tr><td style='background-color:#" & objRsOverall("graph_colour") & "; padding:5px;' class='l'>" & objRsOverall("value") & "</td>"
                    if len(objRsOverall("overall_desc")) > 0 then
                        response.write "<td class='l'>" & objRsOverall("overall_desc") & "</td>"
                    end if
                    response.write "</tr>"

                    objRsOverall.movenext
                wend

                response.write "</table>"
            end if
            set objRsOverall = nothing

            response.write "</div>"
        end if

        response.write "<div style='clear:both;'></div></div>"

    end if
    set objRsSev = nothing
    set objRsLik = nothing

    if showBoundingDiv = true then
        response.write "</div>"
    end if

end sub


sub showCustomMatrixResidual()

    if left(ucase(session("YOUR_USERNAME")), 8) = "SYSADMIN" then

        objCommand.commandText = "SELECT DISTINCT overall.icon_folder, overall.icon_name, overall.icon_rating " & _
                                 "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                                 "WHERE overall.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "ORDER BY overall.icon_rating"
        set objRsOverall = objCommand.execute
        if not objRsOverall.EOF then

            response.write "<div><p>Residual Risk Graphics for reports</p></div>"

            while not objRsOverall.EOF 

                response.write "<div style='padding: 5px;'><img src='../../../modules/hsafety/risk_assessment/app/img/" & objRsOverall("icon_folder") & "/lrg/" & objRsOverall("icon_name") & ".png' /></div>"

                objRsOverall.movenext
            wend

        end if

    end if

end sub
    
Function HTMLDecode(sText)
    Dim regEx
    Dim matches
    Dim match
    sText = Replace(sText, "&quot;", Chr(34))
    sText = Replace(sText, "&lt;"  , Chr(60))
    sText = Replace(sText, "&gt;"  , Chr(62))
    sText = Replace(sText, "&amp;" , Chr(38))
    sText = Replace(sText, "&nbsp;", Chr(32))


    Set regEx= New RegExp

    With regEx
     .Pattern = "&#(\d+);" 'Match html unicode escapes
     .Global = True
    End With

    Set matches = regEx.Execute(sText)

    'Iterate over matches
    For Each match in matches
        'For each unicode match, replace the whole match, with the ChrW of the digits.

        sText = Replace(sText, match.Value, ChrW(match.SubMatches(0)))
    Next

    HTMLDecode = sText
End Function




%>