<!-- #INCLUDE FILE="../../../../data/scripts/inc/auto_logout_frameset.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/autogen_datetime.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/autogen_counties.asp" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/autogen_countries.asp" -->
<% 

dim objrs
dim objconn
dim objcommand

call startconnections()

var_command = request("cmd")
var_id = request("id")

var_ab_companyid = request("company_id")

Objcommand.commandtext = "SELECT comp.*, users.per_fname, users.per_sname FROM " & Application("DBTABLE_CMS_COMPANY") & " as comp " & _
                         "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as users " & _
                         "ON comp.managed_by = users.acc_ref " & _
                         "AND comp.corp_code = users.corp_code " & _
                         "WHERE comp.corp_code='" & Session("corp_code") & "' and comp.id='" & var_ab_companyid & "'"
SET Objrs = Objcommand.execute

If Objrs.eof then

else

    var_ab_companyname = objrs("name")
    var_ab_companyaddress = objrs("address1")
    
    if len(objrs("town")) > 0 then
    var_ab_companyaddress = var_ab_companyaddress & objrs("town") & vbCrlf
    end if
    
    if len(objrs("county")) > 0 then
    var_ab_companyaddress = var_ab_companyaddress & dictCounty.Item(objrs("county")) & vbCrlf
    end if
    
    if len(objrs("postcode")) > 0 then
    var_ab_companyaddress = var_ab_companyaddress & Ucase(objrs("postcode")) & vbCrlf
    end if
    
    if len(objrs("country")) > 0 then
    var_ab_companyaddress = var_ab_companyaddress & dictCountry.Item(objrs("country")) & vbCrlf
    end if
    
    var_ab_companyaddress = Replace(var_ab_companyaddress,",","")
    var_ab_companyaddress = Replace(var_ab_companyaddress,vbCrlf,",<br />")
    
    var_ab_companyphone = objrs("switchboard")
    var_ab_companywebsite = objrs("website")
    var_ab_companyactivity = objrs("activity")
    var_ab_companymanagedby = objrs("per_fname") & " " & objrs("per_sname")

end if





%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>AssessNET Addressbook : Company Details</title>
    <link rel='stylesheet' href="../../../hsafety/risk_assessment/app/default.css" media="all" />
    <script type='text/javascript' src='../../../../data/scripts/js/global.js'></script>
</head>

<body>
<form action='addressbook_bookdemo.asp' method='post' id='demoform' name='demoform'>   

 <div id='section_0' class='optionsection' align='right'>
    <table width='100%'>
    <tr>
        <td class='l title'>Address Book <strong>Company Details</strong><br /><span class='info'>Real-time look at your current client base.</span></td>
        <td width='120' class='ur_icon l link' ><a id='menu2' href='add_company.asp'><span>Add Client</span><br />to the address book</a></td>
        <td width='100' class='sr_icon l link' ><a id='menu3' href='adv_search.asp'><span>Search</span><br />for a client</a></td>
    </tr>
    </table>
 </div>

<!-- Window style starts -->
<div id='windowtitle'>
<h5>AssessNET Demo Manager</h5>
<!-- hidden drop -->
<div id='windowdrop'>
    <table width='100%'>
    <tr>
        <th>Hidden filter</th><td></td>
    </tr>   
    </table>
</div>
<!-- hidden drop end -->
    <div id='windowmenu'>
        <span><a href='default.asp'>Back to main menu</a></span>
        <span><a href='addressbook_search.asp?view=all'>Browse contacts</a></span>
    </div>
    <!-- body of window -->
    <div id='windowbody' class='pad'>  
      
    <div id='section_1' class='sectionarea'>
    <a href='#' name='section1' id='section1'>&nbsp;</a>  
        
    <h3>Details for <% =var_ab_companyname %></h3>

    <table width='100%' cellpadding='5px' cellspacing='2px' width='500'>
    <tr>
        <th width='200' height='90' valign='top'>Business Address:</th><td valign='top'><% =var_ab_companyaddress %></td>
    </tr>
    <tr>
        <th width='200'>Phone (Switchboard):</th><td><% =var_ab_companyphone %></td>
    </tr>
    <tr>
        <th width='200'>Business Website:</th><td><% =var_ab_companywebsite %></td>
    </tr>
    <tr>
        <th width='200'>Business Activity:</th><td>http://<% =var_ab_companyactivity %></td>
    </tr>
    <tr>
        <th width='200'>Client Managed By:</th><td><% =var_ab_companymanagedby %></td>
    </tr>
    </table>
        
    </div>

    <div id='section_2' class='sectionarea'>
    <a href='#' name='section2' id='section2'>&nbsp;</a>  
    
    <h3>Contacts for <% =var_ab_companyname %></h3>

    
    </div>
    
    </div>
    <!-- end of body -->
</div>    
<!-- end window style -->
</form>

</body>
</html>
<% call endconnections() %>