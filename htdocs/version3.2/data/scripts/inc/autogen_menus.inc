<%

SUB HAZ_MENU(HazValue,ObjNAME)

	DIM HazCategory(40),HazARRAY

	HazCategory(0) = "Access / Egress"
	HazCategory(1) = "Adverse Weather"
	HazCategory(2) = "Animal"
	HazCategory(3) = "Biological"
	HazCategory(4) = "Collapse of Structure"
	HazCategory(5) = "Compressed Air"
	HazCategory(6) = "Confined Spaces"
	HazCategory(7) = "Drowning / Asphyxiation"
	HazCategory(8) = "D.S.E / V.D.U Usage"
	HazCategory(9) = "Electricity"
	HazCategory(10) = "Energy Release"
	HazCategory(11) = "Environmental"
	HazCategory(12) = "Ergonomics"
	HazCategory(13) = "Excavation"
	HazCategory(14) = "Explosion"
	HazCategory(15) = "Fall of Object from Height"
	HazCategory(16) = "Fall of Persons from  Height"
	HazCategory(17) = "Fire Safety"
	HazCategory(18) = "Food Hygiene"
	HazCategory(19) = "Gas"
	HazCategory(20) = "Hazardous Substance"
	HazCategory(21) = "Housekeeping"
	HazCategory(22) = "Human Factors"
	HazCategory(23) = "Lifting Equipment"
	HazCategory(24) = "Lighting"
	HazCategory(25) = "Machinery"
	HazCategory(26) = "Manual Handling"
	HazCategory(27) = "Noise"
	HazCategory(28) = "Pressure"
	HazCategory(29) = "Radiation"
	HazCategory(30) = "Sharp Objects"
	HazCategory(31) = "Slip / Trip / Fall"
	HazCategory(32) = "Storage"
	HazCategory(33) = "Stress"
	HazCategory(34) = "Temperature Extremes"
	HazCategory(35) = "Vehicles"
	HazCategory(36) = "Ventilation"
	HazCategory(37) = "Vibration"
	HazCategory(38) = "Violence to staff"
	HazCategory(39) = "Work Equipment"
	HazCategory(40) = "Other (See: Hazard Details)"

	Response.Write("<SELECT Name='" & ObjNAME & "'>")
	
	If HazValue = "S" Then
	    Response.Write("<option value'any hazard type'>any hazard type</option>")
	Else
		Response.Write("<option value='any'>Select a Hazard ...</option>")
	End if
	
    For HazARRAY = 0 to 40
        If HazValue = HazCategory(HazARRAY) Then
            Response.Write("<option value='" & HazCategory(HazARRAY) & "' style='background-color: #FFF000' selected='selected'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        Else
            Response.Write("<option value='" & HazCategory(HazARRAY) & "'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        End if
    Next
		
	Response.Write("</select>")
	
END SUB



SUB PER_AFFECTED(ObjNAME)

	DIM PAFFECTED(22), paffARRAY

	PAFFECTED(0) = "Cleaners"
	PAFFECTED(1) = "Contractors"
	PAFFECTED(2) = "Employees"
	PAFFECTED(3) = "Engineers"
	PAFFECTED(4) = "Lone Workers"
	PAFFECTED(5) = "Machine Operators"
	PAFFECTED(6) = "Maintenance Staff"
	PAFFECTED(7) = "Members of the Public"
	PAFFECTED(8) = "Office Staff"
	PAFFECTED(9) = "Outdoor Workers"
	PAFFECTED(10) = "Patient"
	PAFFECTED(11) = "Pregnant Women"
	PAFFECTED(12) = "Production"
	PAFFECTED(13) = "Staff"
	PAFFECTED(14) = "Staff with Disabilities"
	PAFFECTED(15) = "Students"
	PAFFECTED(16) = "Students with Disabilities"
	PAFFECTED(17) = "Resident / Tenant"
	PAFFECTED(18) = "Trainees / Young Persons"
	PAFFECTED(19) = "Visitors"
	PAFFECTED(20) = "Volunteers"
	PAFFECTED(21) = "Warehouse Operators"
	PAFFECTED(22) = "Other"

	Response.Write("<select name='" & ObjNAME & "'>")
	Response.Write(vbCrLf & "  <option>Select ...</option>")
	
    For paffARRAY = 0 to 22
        Response.Write(vbCrLf & "  <option value='" & PAFFECTED(paffARRAY) & "'>" & PAFFECTED(paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB

Sub autogenBodyPart(myBodyPart, outputFormat, showDisabled) 
   
	varScriptOutput = ""
	
	if  Session("MODULE_ACCB") ="2" then	

	objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
            while not objrsBP.eof
				if objrsBP("opt_disabled") = false or showDisabled = true or cstr(myBodyPart) = cstr(objrsBP("opt_id")) then  
					if cstr(myBodyPart) =  cstr(objrsBP("opt_id")) then
						varScriptOutput = varScriptOutput & "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "' selected='selected' >" &  objrsBP("opt_value") & "</option>"
					else
						varScriptOutput = varScriptOutput & "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "'>" &  objrsBP("opt_value") & "</option>"
					end if
                
					if objrsBP("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsBP("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsBPSUB = objcommand.execute
                        
                        if not objrsBPSUB.eof then 
                            while not objrsBPSUB.eof
								if objrsBPSUB("opt_disabled") = false or showDisabled = true or cstr(myBodyPart) = cstr(objrsBPSUB("opt_id")) then  
									if cstr(myBodyPart) =  cstr(objrsBPSUB("opt_id")) then
										varScriptOutput = varScriptOutput & "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "' selected>-- " & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
									else
										varScriptOutput = varScriptOutput & "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "'>-- " & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
									end if
	                            end if
                                objrsBPSUB.movenext
                            wend
                        end if
                    
                    end if
				end if
                objrsBP.movenext
            wend
        end if
	else
		Dim dictBodyPart
		Set dictBodyPart = CreateObject("Scripting.Dictionary")
		dictBodyPart.Add "0", "Ankle"
		dictBodyPart.Add "1", "Arm"
		dictBodyPart.Add "2", "Back (Lower)"
		dictBodyPart.Add "3", "Back (Upper)"
		dictBodyPart.Add "25", "Back - Unspecific"
		dictBodyPart.Add "4", "Buttocks"
		dictBodyPart.Add "5", "Chest"
		dictBodyPart.Add "6", "Eye"
		dictBodyPart.Add "7", "Face"
		dictBodyPart.Add "8", "Finger(s)"
		dictBodyPart.Add "9", "Foot"
		dictBodyPart.Add "10", "Groin"
		dictBodyPart.Add "11", "Hand injury"
		dictBodyPart.Add "12", "Head"
		dictBodyPart.Add "13", "Hip"
		dictBodyPart.Add "14", "Internal injuries"
		dictBodyPart.Add "15", "Knee"
		dictBodyPart.Add "24", "Leg - Unspecific"
		dictBodyPart.Add "16", "Multiple injuries"
		dictBodyPart.Add "17", "Neck"
		dictBodyPart.Add "18", "Shin"
		dictBodyPart.Add "19", "Shoulder"
		dictBodyPart.Add "20", "Stomach"
		dictBodyPart.Add "21", "Thigh"
		dictBodyPart.Add "22", "Toe(s)"
		dictBodyPart.Add "23", "Wrist"
		dictBodyPart.Add "na", "[ Not Specified ]" 
		
		
		myBodyPart = "" & myBodyPart
	   For Each key In dictBodyPart
			varScriptOutput = varScriptOutput & "<option value='" & key & "'"
			If myBodyPart = key Then 
				varScriptOutput = varScriptOutput & " selected='selected'" 
			End if
			varScriptOutput = varScriptOutput & ">" & dictBodyPart.item(key) & "</option>" & vbCrLf
		Next
	end if

	if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
		varScriptOutPut = replace(varScriptOutPut, vbCrLf, " \")
		varScriptOutPut = replace(varScriptOutPut, vbCr, " \")
    end if

    response.write varScriptOutput	
	
End Sub

Function getBodyPartForCode(myBodyPart)
    if  Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("parent_value")) > 0 then
                getBodyPartForCode = objrsBP("parent_value") & " > " & objrsBP("opt_value")
           else
                getBodyPartForCode = objrsBP("opt_value")
           end if      
        else
           getBodyPartForCode =  "Body part could not be found"
        end if
             
    else
    
      Dim dictBodyPart
            Set dictBodyPart = CreateObject("Scripting.Dictionary")
            dictBodyPart.Add "0", "Ankle"
            dictBodyPart.Add "1", "Arm"
            dictBodyPart.Add "2", "Back (Lower)"
            dictBodyPart.Add "3", "Back (Upper)"
            dictBodyPart.Add "25", "Back - Unspecific"
            dictBodyPart.Add "4", "Buttocks"
            dictBodyPart.Add "5", "Chest"
            dictBodyPart.Add "6", "Eye"
            dictBodyPart.Add "7", "Face"
            dictBodyPart.Add "8", "Finger(s)"
            dictBodyPart.Add "9", "Foot"
            dictBodyPart.Add "10", "Groin"
            dictBodyPart.Add "11", "Hand injury"
            dictBodyPart.Add "12", "Head"
            dictBodyPart.Add "13", "Hip"
            dictBodyPart.Add "14", "Internal injuries"
            dictBodyPart.Add "15", "Knee"
            dictBodyPart.Add "24", "Leg - Unspecific"
            dictBodyPart.Add "16", "Multiple injuries"
            dictBodyPart.Add "17", "Neck"
            dictBodyPart.Add "18", "Shin"
            dictBodyPart.Add "19", "Shoulder"
            dictBodyPart.Add "20", "Stomach"
            dictBodyPart.Add "21", "Thigh"
            dictBodyPart.Add "22", "Toe(s)"
            dictBodyPart.Add "23", "Wrist"
            dictBodyPart.Add "na", "[ Not Specified ]" 
    
        myBodyPart = "" & myBodyPart
        getBodyPartForCode = dictBodyPart.Item(myBodyPart)
    end if
   
End function

Function getBodyPartColourForCode(myBodyPart)
    if Session("MODULE_ACCB") = "2" then
        objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("opt_colour")) > 0 then
                getBodyPartColourForCode = objrsBP("opt_colour")
           else
                getBodyPartColourForCode = ""
           end if      
        else
           getBodyPartColourForCode = ""
        end if
    else
        getBodyPartColourForCode = ""
    end if
   
End function

Sub autogenEmpStatus(opt_value, field_name, field_disabled, filter_list_item, showDisabled)

	if isnull(opt_value) = true or opt_value = "" or opt_value = "na" then
		opt_value = 0
    end if
	
	objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
							 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
							 "  AND related_to = 'emp_status' " & _
							 "  AND parent_id = 0 " & _
						     "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " 
	if len(filter_list_item) > 0 then
		objCommand.commandText = objCommand.commandText & "  AND opt_value = (SELECT opt_target FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
																		     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
																		     "AND related_to = 'acc_status' " & _
																		     "AND opt_id = " & filter_list_item & " " & _
																			 "AND opt_language='" & session("YOUR_LANGUAGE") & "')"
	end if
	
	set objRsREL = objCommand.execute
	if not objRsREL.EOF then
		response.write "<select class='select' name='" & field_name & "' " & field_disabled & "><option value='na'>Please Select</option>"

		while not objRsREL.EOF
			
			if objRsREL("child_present") = 1 then
				response.write "<optgroup label='" & objRsREL("opt_value") & "'>"

				objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
										 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
										 "  AND related_to = 'emp_status' " & _
										 "  AND parent_id = " & objRsREL("opt_id") & " " & _
										 "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
				set objRsREL2 = objCommand.execute
				if not objRsREL2.EOF then
					while not objRsREL2.EOF
						if objrsREL2("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL2("opt_id")) then  
							if cint(opt_value) = cint(objRsREL2("opt_id")) then
								response.write "<option value='" & objRsREL2("opt_id") & "' selected>" & objRsREL2("opt_value") & "</option>"
							else
								response.write "<option value='" & objRsREL2("opt_id") & "'>" & objRsREL2("opt_value") & "</option>"
							end if
						end if
						objRsREL2.movenext
					wend
				end if
				set objRsREL2 = nothing
				response.write "</optgroup>"
			else
				if objrsREL("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL("opt_id")) then  
					if cint(opt_value) = cint(objRsREL("opt_id")) then
						response.write "<option value='" & objRsREL("opt_id") & "' selected>" & objRsREL("opt_value") & "</option>"
					else
						response.write "<option value='" & objRsREL("opt_id") & "'>" & objRsREL("opt_value") & "</option>"
					end if
				end if
			end if

			objRsREL.movenext
		wend

		response.write "</select>"
	end if
	set objRsREL = nothing

End Sub

function getEmpStatusForCode(myType)

    if isnull(myType) = true or myType = "" or myType = "na" then
        myType = 0
    end if
	
	objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
							 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
							 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
							 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='emp_status' and a.opt_id='" & myType & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"	
	set objRsEMP = objCommand.execute
	if not objRsEMP.EOF then
		objRsEMP.movefirst
		getEmpStatusForCode = objRsEMP("parent_value") & " - " & objRsEMP("opt_value")
	else
		getEmpStatusForCode = "<span class='info'>Not an Employee</span>"
	end if
	set objRsEMP = nothing
	
end function

' -------- Type of injury ---------

Sub autogenInjType(myType, outputFormat, showDisabled)

	varScriptOutput = ""

	if Session("MODULE_ACCB") = "2" then

		objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsIT = objcommand.execute

        if not objrsIT.eof then 
            while not objrsIT.eof
				if objrsIT("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsIT("opt_id")) or cstr(lcase(myType)) = cstr(lcase(objrsIT("opt_value"))) then  
					if cstr(myType) =  cstr(objrsIT("opt_id")) or cstr(lcase(myType)) =  cstr(lcase(objrsIT("opt_value"))) then
						varScriptOutput = varScriptOutput & "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "'  selected='selected' >" &  objrsIT("opt_value") & "</option>"
					else
						varScriptOutput = varScriptOutput & "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "'>" &  objrsIT("opt_value") & "</option>"
					end if
                
					if objrsIT("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '" & objrsIT("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsITSUB = objcommand.execute
                        
                        if not objrsITSUB.eof then 
                            while not objrsITSUB.eof
								if objrsITSUB("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsITSUB("opt_id")) or cstr(lcase(myType)) = cstr(lcase(objrsITSUB("opt_value"))) then  
									if cstr(myType) =  cstr(objrsITSUB("opt_id"))  or cstr(lcase(myType)) =  cstr(lcase(objrsITSUB("opt_value"))) then
										varScriptOutput = varScriptOutput & "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "' selected>-- " & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
									else
										varScriptOutput = varScriptOutput & "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "'>-- " & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
									end if
                                end if
                                objrsITSUB.movenext
                            wend
                        end if
                    
                    end if
				end if
                objrsIT.movenext
            wend
        end if
	else
        Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "17", "Multiple Injuries"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"

            myType = "" & myType
            For Each key In dictInjType
                varScriptOutput = varScriptOutput & "<option value='" & key & "'"
                If myType = key or lcase(myType) = lcase(dictInjType.item(key)) Then 
                    varScriptOutput = varScriptOutput & " selected='selected'" 
                End if
                varScriptOutput = varScriptOutput & ">" & dictInjType.item(key) & "</option>" & vbCrLf
            Next
    end if 

    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
		varScriptOutPut = replace(varScriptOutPut, vbCrLf, " \")
		varScriptOutPut = replace(varScriptOutPut, vbCr, " \")
    end if

    response.write varScriptOutput		
	
End Sub

Function getInjTypeForCode(myType)
   
     if  Session("MODULE_ACCB") ="2" then
           objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("parent_value")) > 0 then
                getInjTypeForCode = objrsIT("parent_value") & " > " & objrsIT("opt_value")
           else
                getInjTypeForCode = objrsIT("opt_value")
           end if                   
        else
           getInjTypeForCode =  "Injury type could not be found"
        end if
    else
         Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"
    
         myType = "" & myType
        getInjTypeForCode = dictInjType.Item(myType)
    end if
End function

Function getInjTypeColourForCode(myType)
   
     if  Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                  " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("opt_colour")) > 0 then
                getInjTypeColourForCode = objrsIT("opt_colour")
           else
                getInjTypeColourForCode = ""
           end if                   
        else
           getInjTypeColourForCode = ""
        end if
    else

    end if
End function

' --------- Apparent Cause of Injury ----------

Sub autogenApparentCause(myCause, outputFormat, showDisabled)

	varScriptOutput = ""

    if  Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsAC = objcommand.execute

        if not objrsAC.eof then 
            while not objrsAC.eof
				if objrsAC("opt_disabled") = false or showDisabled = true or cstr(myCause) = cstr(objrsAC("opt_id")) or cstr(lcase(myCause)) = cstr(lcase(objrsAC("opt_value"))) then  
					if cstr(myCause) =  cstr(objrsAC("opt_id")) or  cstr(lcase(myCause)) = lcase(objrsAC("opt_value")) then
						varScriptOutput = varScriptOutput & "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "' selected>" &  objrsAC("opt_value") & "</option>"
					else
						varScriptOutput = varScriptOutput & "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "'>" &  objrsAC("opt_value") & "</option>"
					end if
                
					if objrsAC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '" & objrsAC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsACSUB = objcommand.execute

                        if not objrsACSUB.eof then 
                            while not objrsACSUB.eof
								if objrsACSUB("opt_disabled") = false or showDisabled = true or cstr(myCause) = cstr(objrsACSUB("opt_id")) or cstr(lcase(myCause)) = cstr(lcase(objrsACSUB("opt_value"))) then  
									if cstr(myCause) =  cstr(objrsACSUB("opt_id")) or cstr(lcase(myCause)) = lcase(objrsACSUB("opt_value")) then
										varScriptOutput = varScriptOutput & "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "' selected>-- " & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
									else
										varScriptOutput = varScriptOutput & "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "'>-- " & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
									end if
                                end if
                                objrsACSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsAC.movenext
            wend
        end if
    else

        Dim dictApparentCause
        Set dictApparentCause = CreateObject("Scripting.Dictionary")

        dictApparentCause.Add "0", "Animal Related"
        dictApparentCause.Add "1", "Collision"
        dictApparentCause.Add "3", "Contact with electricity"
        dictApparentCause.Add "4", "Contact with machinery"
        dictApparentCause.Add "2", "Contact with sharp object"
        dictApparentCause.Add "15", "Exposure to explosion"
        dictApparentCause.Add "6", "Exposure to fire"
        dictApparentCause.Add "5", "Exposure to hazardous substance"
        dictApparentCause.Add "7", "Exposure to temperature extremes"
        dictApparentCause.Add "16", "Fainting / Fit"
        dictApparentCause.Add "8", "Fall from height"
        dictApparentCause.Add "9", "Manual Handling"
        dictApparentCause.Add "10", "Repetitive actions"
        dictApparentCause.Add "17", "Self inflicted"
        dictApparentCause.Add "11", "Slip, trip or fall"
        dictApparentCause.Add "12", "Struck by object"
        dictApparentCause.Add "13", "Struck by vehicle"
        dictApparentCause.Add "18", "Trapped by object"
        dictApparentCause.Add "14", "Violence"
        dictApparentCause.Add "na", "[ Not Specified ]"

            myCause = "" & myCause
            For Each key In dictApparentCause
                varScriptOutput = varScriptOutput & "<option value='" & key & "'"
                If myCause = key or lcase(myCause) = lcase(dictApparentCause.item(key)) Then 
                    varScriptOutput = varScriptOutput & " selected='selected'" 
                End if
                varScriptOutput = varScriptOutput & ">" & dictApparentCause.item(key) & "</option>" & vbCrLf
            Next
    end if
	
    if outputFormat = "javascript" then
        varScriptOutput = replace(varScriptOutput, "'", """")
		varScriptOutPut = replace(varScriptOutPut, vbCrLf, " \")
		varScriptOutPut = replace(varScriptOutPut, vbCr, " \")
    end if

    response.write varScriptOutput	
	
End Sub
	
Function getApparentCauseForCode(myCause)


     if  Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsAC = objcommand.execute
                   
        if not objrsAC.eof then 
           if len(objrsAC("parent_value")) > 0 then
                getApparentCauseForCode = objrsAC("parent_value") & " > " & objrsAC("opt_value")
           else
                getApparentCauseForCode = objrsAC("opt_value")
           end if                   
        else
           getApparentCauseForCode =  "Apparent cause could not be found"
        end if
    else
        
        Dim dictApparentCause
        Set dictApparentCause = CreateObject("Scripting.Dictionary")

        dictApparentCause.Add "0", "Animal Related"
        dictApparentCause.Add "1", "Collision"
        dictApparentCause.Add "3", "Contact with electricity"
        dictApparentCause.Add "4", "Contact with machinery"
        dictApparentCause.Add "2", "Contact with sharp object"
        dictApparentCause.Add "15", "Exposure to explosion"
        dictApparentCause.Add "6", "Exposure to fire"
        dictApparentCause.Add "5", "Exposure to hazardous substance"
        dictApparentCause.Add "7", "Exposure to temperature extremes"
        dictApparentCause.Add "16", "Fainting / Fit"
        dictApparentCause.Add "8", "Fall from height"
        dictApparentCause.Add "9", "Manual Handling"
        dictApparentCause.Add "10", "Repetitive actions"
        dictApparentCause.Add "17", "Self inflicted"
        dictApparentCause.Add "11", "Slip, trip or fall"
        dictApparentCause.Add "12", "Struck by object"
        dictApparentCause.Add "13", "Struck by vehicle"
        dictApparentCause.Add "18", "Trapped by object"
        dictApparentCause.Add "14", "Violence"
        dictApparentCause.Add "na", "[ Not Specified ]"
    
        myCause = "" & myCause
        getApparentCauseForCode = dictApparentCause.Item(myCause)
    end if
End function

Function getApparentCauseColourForCode(myCause)

    if Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsAC = objcommand.execute
                   
        if not objrsAC.eof then 
           if len(objrsAC("opt_colour")) > 0 then
                getApparentCauseColourForCode = objrsAC("opt_colour")
           else
                getApparentCauseColourForCode = ""
           end if                   
        else
           getApparentCauseForCode =  ""
        end if
    else

    end if
End function



sub autogenInjStatus(myStatus, showDisabled)      
        
         objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsIS = objcommand.execute

        if not objrsIS.eof then 
            while not objrsIS.eof
                if isnull(myStatus) = true then
					response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
				else
					if objrsIS("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objrsIS("opt_id")) then  
						if cstr(myStatus) =  cstr(objrsIS("opt_id")) then
							response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "' selected>" &  objrsIS("opt_value") & "</option>"
						else
							response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
						end if
					end if
				end if	
						                
				if objrsIS("child_present") = "1" then
                    'There are sub options available
                    objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '" & objrsIS("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                    set objrsISSUB = objcommand.execute

                    if not objrsISSUB.eof then 
                        while not objrsISSUB.eof
							if objrsISSUB("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objrsISSUB("opt_id")) then  
								if cstr(myStatus) =  cstr(objrsISSUB("opt_id")) then
									response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "' selected>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
								else
									response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "'>-- " & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
								end if
                            end if
                            objrsISSUB.movenext
                        wend
                    end if
                    
                end if
                objrsIS.movenext
            wend
        end if
        
end sub

function getAccidentStatusForCode(myStatus)
   
    
     objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("parent_value")) > 0 then
                getAccidentStatusForCode = objrsIS("parent_value") & " > " & objrsIS("opt_value")
           else
                getAccidentStatusForCode = objrsIS("opt_value")
           end if                   
        else
           getAccidentStatusForCode =  "Accident status could not be found"
        end if
end function

function getAccidentStatusColourForCode(myStatus)
  objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                           " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("opt_colour")) > 0 then
                getAccidentStatusColourForCode = objrsIS("opt_colour")
           else
                getAccidentStatusColourForCode = ""
           end if                   
        else
           getAccidentStatusColourForCode = ""
        end if
end function



Sub autogenSchoolYear(mySchoolYearm, showDisabled)
    if isnull(mySchoolYear) then
        mySchoolYear = ""
    end if
    
    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_sch_year' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_id asc"
    'response.Write objcommand.commandtext
    'response.end    
    set objrsVI = objcommand.execute

        if not objrsVI.eof then 
            while not objrsVI.eof
				if objrsVI("opt_disabled") = false or showDisabled = true or cstr(mySchoolYear) = cstr(objrsVI("opt_id")) then 
					if cstr(mySchoolYear) =  cstr(objrsVI("opt_id")) then
						response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "' selected='selected' >" &  objrsVI("opt_value") & "</option>"
					else
						response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "'>" &  objrsVI("opt_value") & "</option>"
					end if
					                
					if objrsVI("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsVI("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsVISUB = objcommand.execute
                        
                        if not objrsVISUB.eof then 
                            while not objrsVISUB.eof
								if objrsVISUB("opt_disabled") = false or showDisabled = true or cstr(mySchoolYear) = cstr(objrsVISUB("opt_id")) then 
									if cstr(mySchoolYear) =  cstr(objrsVISUB("opt_id")) then
										response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "' selected>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
									else
										response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "'>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
									end if
								end if
                                objrsVISUB.movenext
                            wend
                        end if
                    
                    end if
				end if
                objrsVI.movenext
            wend
        end if
    end if

end sub


%>