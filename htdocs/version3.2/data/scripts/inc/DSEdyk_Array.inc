<%
Dim DSEdykarray(26)

DSEdykarray(0) = "When using your DSE workstation, your chair should be fixed to a stable 5 caster base."
DSEdykarray(1) = "Most long term back problems are caused by bad posture when using DSE Equipment."
DSEdykarray(2) = "If your workstation or environment changes you should perform another DSE Assessment."
DSEdykarray(3) = "For every 2 hours you use your DSE workstation, you are entitled to a 15 min break."
DSEdykarray(4) = "Taking regular breaks from your DSE workstation can reduce the risk of injury."
DSEdykarray(5) = "You blink less when concentrating on your Display Screen for long periods, which increases the risk of eye damage."
DSEdykarray(6) = "Focusing on distant objects when using your DSE workstation for long periods can reduce the risk of eye damage and future focusing difficulties."
DSEdykarray(7) = "Your Display Screen should be positioned at a comfortable eye level to reduce future neck and posture problems."
DSEdykarray(8) = "You should be able to rest your forearms on the desk or some other support to allow for a comfortable working position, reducing the risk of wrist injury."
DSEdykarray(9) = "Your forearms should be horizontal when typing."
DSEdykarray(10) = "Your employer should provide sufficient training to operate your system."
DSEdykarray(11) = "Your thighs should not tilt upwards whilst seated."
DSEdykarray(12) = "Your chair back should support the curve in your lower back in an upright position."
DSEdykarray(13) = "Your shoulders and body should be relaxed."
DSEdykarray(14) = "Your feet should be flat on the floor or on an angled support."
DSEdykarray(15) = "Support your wrists when you are not typing."
DSEdykarray(16) = "You should not grip the mouse tightly."
DSEdykarray(17) = "You should break up repetitive actions, and remember to take regular breaks."
DSEdykarray(18) = "Always report any aches or pains to your employer as soon as possible."
DSEdykarray(19) = "You Take regular breaks which involve changing your body position. Get up and walk around for a few minutes."
DSEdykarray(20) = "Exercise your fingers by flexing and extending them."
DSEdykarray(21) = "Exercise your wrists by rotating them in both directions."
DSEdykarray(22) = "Exercise - stretch your arms above your head to exercise your arms and shoulders."
DSEdykarray(23) = "Exercise your shoulders by rolling them forwards and backwards."
DSEdykarray(24) = "Exercise your lower back by sitting forward in your chair and gently rocking your pelvis forward and backward."
DSEdykarray(25) = "Exercise your legs by bending and straightening your knees whilst seated."
DSEdykarray(26) = "If you are experiencing eye discomfort you may require an eyesight test."

SUB DYKrandom()

Randomize()
intRangeSize = UBound(DSEdykarray) + 1
sngRandomValue = intRangeSize * Rnd()
sngRandomValue = sngRandomValue 
intRandomInteger = Int(sngRandomValue)

Response.write DSEdykarray(intRandomInteger)

END SUB

%>