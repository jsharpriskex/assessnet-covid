<%
SUB createCalender()

' ----------- S T A R T   C A L E N D E R   O B J E C T  --------------

Response.Write  "<table width='90%' border='0' cellpadding='2' cellspacing='0' class='month'>" & vbCrLf & _
                "  <tr>" & vbCrLf
For y = 1 to 7   
    Response.Write "<th width='13%' class='c weekday'>" & left(WeekDayName(y,true),1) & "</td>" & vbCrLf
Next

Response.Write  "</tr>" & vbCrLf

startOfMonth = "01/" & Month(sentDate) & "/" & Year(sentDate)
startOfFirstWeek = DateAdd("d",1 - Weekday(startOfmonth),startOfMonth)  


' C O L L E C T   E V E N T S   F R O M   D B  ----------------

Dim myHours(6,23)
Dim colorIndex
colorIndex = 1


If Len(Request("cal_user")) > 0 Then
    thisUserId = Request("cal_user")
Else
    thisUserId = Session("YOUR_ACCOUNT_ID")
End if

objCommand.CommandText =  "SELECT ID, task_due_date FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " WHERE TASK_ACCOUNT = '" & Session("YOUR_ACCOUNT_ID") & "' ORDER BY TASK_DUE_DATE ASC"    


Set events = objCommand.Execute
Set dictTasks = CreateObject("Scripting.Dictionary")


ON ERROR RESUME NEXT

While NOT events.EOF

    thisId = events("id")
    myDueDate = events("task_due_date")  
    
   
		dictTasks.add myDueDate,thisId

		
    events.MoveNext

Wend

' -------------------------------------------------------


dateIncr = 0
monthValid = 1
startAnotherWeek = 1

While monthValid = 1
    If 1 = startAnotherWeek Then
    
    IF Len(iDate) > 0 Then
		'Get last day of week 
		iDate_ws = DateAdd("d",1,iDate)
		noDate_start = 0
		Else
		noDate_start = 1
    End IF
    
        Response.Write "<tr>" & vbCrLf
        
        For i = 1 to 7
            iDate = DateAdd("d",dateIncr,startOfFirstWeek)
            
            If Month(iDate) = thisMonth Then
                If Cdate(sentdate) = iDate Then
                    Response.Write "<td class='c top today_task' ><strong><a href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='whttxt'>" & Day(iDate) & "</span></a></strong>"
                ElseIF (dictTasks(iDate) <> "") AND (Cdate(iDate) < Cdate(sentdate)) Then
                    Response.Write "<td class='c top taskday' title='OVERDUE!' ><strong><a  href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='whttxt'>" & Day(iDate) & "</span></a></strong>"
                ElseIF dictTasks(iDate) <> "" Then
                    Response.Write "<td class='c top taskfuture' title='Tasks' ><strong><a href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='redtxt'>" & Day(iDate) & "</span></a></strong>"
                Elseif left(WeekDayName(i),1) = "S" Then
                    Response.Write "<td class='c top weekend_task' ><a href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='grytxt'>" & Day(iDate) & "</span></a>"
                Elseif InStr(strPublicHolidays_uk,Left(iDate,10)) Then
                    Response.Write "<td class='c top pubhol_task'  title='Public Holiday'><a class='blink' href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='grytxt'>" & Day(iDate) & "</span></a>"
                Else
                    Response.Write "<td class='c top validday_task' ><a href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='blktxt'>" & Day(iDate) & "</span></a>"
                End If
            Else
                Response.Write "<td class='c top invalidday_task' ><a href='task_main.asp?date=" & iDate & CMD_URL & "&amp;CMD=showday'><span class='grytxt'>" & Day(iDate) & "</span></a>"
            End If
        

            Response.Write "</td>"
            
            dateIncr = dateIncr + 1
        Next
        
        
        If Month(iDate) > thisMonth OR Year(iDate) > thisYear Then
            monthValid = 0
        End if
        
        If Day(iDate) > Day(DateAdd("d",1,iDate)) Then
            startAnotherWeek = 0
        End if
        
        ' Get First day of the week
        IF noDate_start = 1 Then iDate_ws = DateAdd("d",-6,iDate)
        
        Response.Write "<td class='nostyle c mid'><a href='task_main.asp?CMD=showweek&date=" & iDate_ws & "&date_end=" & iDate & CMD_URL & "'><img src='scripts/img/tm_srch_sml.png' /></a></td>"
        Response.Write "</tr>" & vbCrLf
    Else
        monthValid = 0
    End if
Wend

Response.Write "</table>"

    If Month(sentdate) = "1" Then
        prevMonth = "12"
        prevYear = Year(sentdate) -1 
    else
        prevMonth = Month(sentdate) -1
        prevYear = Year(sentdate) 
    End If
    
    If Month(sentdate) = "12" Then
        nextMonth = "01"
        nextYear = Year(sentdate) +1 
    else
        nextMonth = Month(sentdate) + 1
        nextYear = Year(sentdate) 
    End If
    
'*****NEW******
Response.write "<p class='c'><a href='task_main.asp?CMD=showmonth&date=01/" & prevMonth & "/" & prevYear & CMD_URL & "'><img src='scripts/img/circle_prev.png' align='absmiddle' /></a> <strong><a href='task_main.asp?CMD=showmonth&date=01/" & Month(sentdate) & "/" & Year(sentdate) & CMD_URL & "'>" & monthname(month(sentdate)) & "</a></strong> <a href='task_main.asp?CMD=showmonth&date=01/" & nextMonth & "/" & nextYear & CMD_URL & "'><img src='scripts/img/circle_next.png' align='absmiddle' /></a>&nbsp;|&nbsp;"
Response.write "<a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) -1 & CMD_URL & "'><img src='scripts/img/circle_prev.png' align='absmiddle' /></a> <strong><a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) & CMD_URL & "'>" & Year(sentdate) & "</a></strong> <a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) + 1 & CMD_URL & "'><img src='scripts/img/circle_next.png' align='absmiddle' /></a><br />"

'****old***
'Response.write "<p class='c'><a href='task_main.asp?CMD=showmonth&date=01/" & Month(sentdate) - 1 & "/" & Year(sentdate) & CMD_URL & "'><img src='scripts/img/circle_prev.png' align='absmiddle' /></a> <strong><a href='task_main.asp?CMD=showmonth&date=01/" & Month(sentdate) & "/" & Year(sentdate) & CMD_URL & "'>" & monthname(month(sentdate)) & "</a></strong> <a href='task_main.asp?CMD=showmonth&date=01/" & Month(sentdate) + 1 & "/" & Year(sentdate) & CMD_URL & "'><img src='scripts/img/circle_next.png' align='absmiddle' /></a>&nbsp;|&nbsp;"
'Response.write "<a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) -1 & CMD_URL & "'><img src='scripts/img/circle_prev.png' align='absmiddle' /></a> <strong><a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) & CMD_URL & "'>" & Year(sentdate) & "</a></strong> <a href='task_main.asp?CMD=showyear&date=01/" & Month(sentdate) & "/" & Year(sentdate) + 1 & CMD_URL & "'><img src='scripts/img/circle_next.png' align='absmiddle' /></a><br />"


Response.write "<strong><a href='task_main.asp?date=" & date() & "'>Show All</a></strong></p>"

' ----------- E N D   C A L E N D E R   O B J E C T  --------------

END SUB

SUB menu_taskstatus(varTaskStatus)

	For I = LBound(ary_taskstatus) to UBound(ary_taskstatus)
		Response.Write "<option value='" & I & "' "
		IF varTaskStatus = CStr(I) Then Response.Write "SELECTED" END IF
		Response.Write " >" & ary_taskstatus(I) & "</option>"
	Next

END SUB

SUB menu_taskrelation(varTaskRelation)

	For I = LBound(ary_taskRelation) to UBound(ary_taskRelation)
		Response.Write "<option value='" & ary_taskRelation_abbr(I) & "' "
		IF varTaskRelation = ary_taskRelation_abbr(I) Then Response.Write "SELECTED" END IF
		Response.Write " >" & ary_taskRelation(I) & "</option>"
	Next

END SUB

SUB menu_recBU(varTaskBU)

     objCommand.CommandText = "SELECT DISTINCT BU FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " " & _
                              "WHERE corp_code='" & Session("corp_code")& "' " & _
                              " AND BU IS NOT NULL" & _
                              " ORDER BY BU ASC"    

    Set objRs = objCommand.Execute	
    
    Response.Write 	"<option value='any'>Any Company</option>" 
    
    
    While NOT objRs.EOF
        Response.Write "<option value='" & objRs("BU") & "' "
        IF varTaskBU = Objrs("BU") Then Response.Write "SELECTED" END IF
        Response.Write " >" & objRs("BU") & "</option>"
        objRs.MoveNext
    Wend

END SUB

SUB menu_recBL(varTaskBL)

     objCommand.CommandText = "SELECT DISTINCT BL FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " " & _
                              "WHERE corp_code='" & Session("corp_code")& "' " & _
                              " AND BL IS NOT NULL" & _
                              " ORDER BY BL ASC"    

    Set objRs = objCommand.Execute	
    
    Response.Write 	"<option value='any'>Any Location</option>" 
    
    
    While NOT objRs.EOF
        Response.Write "<option value='" & objRs("BL") & "' "
        IF varTaskBL = Objrs("BL") Then Response.Write "SELECTED" END IF
        Response.Write " >" & objRs("BL") & "</option>"
        objRs.MoveNext
    Wend

END SUB

SUB menu_recBD(varTaskBD)

     objCommand.CommandText = "SELECT DISTINCT BD FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " " & _
                              "WHERE corp_code='" & Session("corp_code")& "' " & _
                              " AND BD IS NOT NULL" & _
                              " ORDER BY BD ASC"    

    Set objRs = objCommand.Execute	
    
    Response.Write 	"<option value='any'>Any Department</option>" 
    
    
    While NOT objRs.EOF
        Response.Write "<option value='" & objRs("BD") & "' "
        IF varTaskBD = objrs("BD") Then Response.Write "SELECTED" END IF
        Response.Write " >" & objRs("BD") & "</option>"
        objRs.MoveNext
    Wend

END SUB

SUB menu_users(varTaskuser)

	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_Bl,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code='" & Session("corp_code")& "' AND Corp_BG='" & Session("YOUR_GROUP") & "' AND Acc_level NOT LIKE '5' and acc_status NOT LIKE 'D' ORDER BY Per_fname ASC"
	SET objRs = objCommand.Execute
	
	Response.Write "<option value='ALL'>----- ALL USERS ----</option>"
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
			IF varTaskuser = Objrs("Acc_ref") Then
				Response.Write "SELECTED"
			End IF        
        Response.Write ">" & objRs("Per_sname") & ", " & objRs("Per_fname") & " :: (" & objRs("Corp_BL") & ") - " & objRs("Corp_BD") & "</option>"
        objRs.MoveNext
	Wend

END SUB

SUB menu_risk(varTaskrisk)

Response.Write 	"<option value='any'>Any risk rating</option>" 

	For I = LBound(ary_risk) to UBound(ary_risk)
		Response.Write "<option value='" & I & "' "
		IF varTaskrisk = CStr(I) Then Response.Write "SELECTED" END IF
		Response.Write " >" & ary_risk(I) & "</option>"
	Next

End SUB

SUB get_task_history(varTaskID)

END SUB

SUB ADD_NOTE(varTaskNote,varTaskID)

	' OK lets insert the note
	Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_TASK_NOTE") & " " & _
								"(corp_code,task_id,task_note_by,task_note,gendatetime) " & _
								"VALUES('" & Session("CORP_CODE") & "','" & varTaskID & "','" & Session("YOUR_ACCOUNT_ID") & "','" & varTaskNote & "','" & date() & " " & Time() & "')"
	Objcommand.execute
	
	viewtype = "tinfo"

END SUB

SUB ADD_HISTORY(varTaskhistory,varTaskID,varSystem)

IF varSystem = "Y" Then
    varAccountID = "SYSTEM"
ELSE
	varAccountID = Session("YOUR_ACCOUNT_ID")
END IF

	' OK lets insert the note
	Objcommand.commandtext =	"INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " " & _
								"(corp_code,task_id,task_history_by,task_history,gendatetime) " & _
								"VALUES('" & Session("CORP_CODE") & "','" & varTaskID & "','" & varAccountID & "','" & varTaskhistory & "','" & date() & " " & Time() & "')"
	Objcommand.execute

END SUB


SUB get_task_notes(varTaskID)

Objcommand.commandtext =	"SELECT Tnotes.*, USERS.per_fname, USERS.per_sname FROM " & Application("DBTABLE_TASK_NOTE") & " as Tnotes " & _
							"LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as USERS " & _
							"ON Tnotes.CORP_CODE = USERS.corp_code " & _
							"AND Tnotes.task_note_by = USERS.acc_ref " & _
							" WHERE Tnotes.CORP_CODE = '" & SESSION("CORP_CODE") & "' AND Tnotes.Task_ID='" & varTaskID & "' ORDER BY Tnotes.gendatetime DESC"
SET Objrs = Objcommand.execute


Response.Write	"<table cellpadding='3' cellspacing='0' width='100%' class='tasknotes'>" & _
				"<tr>" & _
					"<th>Note</th>" & _
					"<th width='10%'>Date</th>" & _
				"</tr>"

IF Objrs.EOF Then
' No notes for this task, tell user
Response.Write	"<tr>" & _
				"<td colspan='2'>There are currently no notes associated with this task.</td>" & _
				"</tr>"

	ELSE
	While NOT Objrs.EOF
	
	
	Response.Write	"<tr>" & _
					"<td><span class='noteby'>Added by " & Objrs("per_fname") & " " & Objrs("per_sname") & "</span><br />" & objrs("task_note") & "</td>" & _
					"<td class='notedate c'>" & left(Objrs("gendatetime"),10) & "</td>" & _
					"</tr>"
	
	
	Objrs.movenext
	Wend
END IF

Response.Write	"</table>" 

END SUB


SUB get_task_history(varTaskID)

Objcommand.commandtext =	"SELECT Thistory.*, USERS.per_fname, USERS.per_sname FROM " & Application("DBTABLE_TASK_HISTORY") & " as Thistory " & _
							"LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as USERS " & _
							"ON Thistory.CORP_CODE = USERS.corp_code " & _
							"AND Thistory.task_history_by = USERS.acc_ref " & _
							" WHERE Thistory.CORP_CODE = '" & SESSION("CORP_CODE") & "' AND Thistory.Task_ID='" & varTaskID & "' ORDER BY Thistory.gendatetime DESC"
SET Objrs = Objcommand.execute


Response.Write	"<table cellpadding='3' cellspacing='0' width='100%' class='taskhistory'>" & _
				"<tr>" & _
					"<th>History</th>" & _
					"<th width='10%'>Date</th>" & _
				"</tr>"

IF Objrs.EOF Then
' No notes for this task, tell user
Response.Write	"<tr>" & _
				"<td colspan='2'>There is not history for this action .</td>" & _
				"</tr>"

	ELSE
	While NOT Objrs.EOF
	
	
	Response.Write	"<tr>" & _
					"<td><span class='noteby'>Change made by " & Objrs("per_fname") & " " & Objrs("per_sname") & "</span><br />" & objrs("task_history") & "</td>" & _
					"<td class='notedate c'>" & left(Objrs("gendatetime"),10) & "</td>" & _
					"</tr>"
	
	
	Objrs.movenext
	Wend
END IF

Response.Write	"</table>" 

END SUB

Function FullDateTitle(varDate)

	SELECT CASE right(Day(varDate),1)
		CASE "1"
		IF Day(varDate) <> 11 Then
		varDate_value = Day(varDate) & "st"
			ELSE
			varDate_value = Day(varDate) & "th"
		END IF
		CASE "2"
		IF Day(varDate) <> 12 Then
		varDate_value = Day(varDate) & "nd"
			ELSE
			varDate_value = Day(varDate) & "th"
		END IF
		CASE "3"
		IF Day(varDate) <> 13 Then
		varDate_value = Day(varDate) & "rd"
			ELSE
			varDate_value = Day(varDate) & "th"
		END IF
		CASE ELSE
		varDate_value = Day(varDate) & "th"
	END SELECT
	
	varDate_value = varDate_value & " " & monthname(month(varDate))
	varDate_value = varDate_value & " " & year(varDate)
	
	fullDateTitle = varDate_value
End Function

%>