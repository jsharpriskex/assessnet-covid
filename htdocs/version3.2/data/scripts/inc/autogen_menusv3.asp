<%



SUB HAZ_MENU(HazValue,ObjNAME)

	DIM HazCategory(40),HazARRAY

	HazCategory(0) = "Access / Egress"
	HazCategory(1) = "Adverse Weather"
	HazCategory(2) = "Animal"
	HazCategory(3) = "Biological"
	HazCategory(4) = "Collapse of Structure"
	HazCategory(5) = "Compressed Air"
	HazCategory(6) = "Confined Spaces"
	HazCategory(7) = "Drowning / Asphyxiation"
	HazCategory(8) = "D.S.E / V.D.U Usage"
	HazCategory(9) = "Electricity"
	HazCategory(10) = "Energy Release"
	HazCategory(11) = "Environmental"
	HazCategory(12) = "Ergonomics"
	HazCategory(13) = "Excavation"
	HazCategory(14) = "Explosion"
	HazCategory(15) = "Fall of Object from Height"
	HazCategory(16) = "Fall of Persons from  Height"
	HazCategory(17) = "Fire Safety"
	HazCategory(18) = "Food Hygiene"
	HazCategory(19) = "Gas"
	HazCategory(20) = "Hazardous Substance"
	HazCategory(21) = "Housekeeping"
	HazCategory(22) = "Human Factors"
	HazCategory(23) = "Lifting Equipment"
	HazCategory(24) = "Lighting"
	HazCategory(25) = "Machinery"
	HazCategory(26) = "Manual Handling"
	HazCategory(27) = "Noise"
	HazCategory(28) = "Pressure"
	HazCategory(29) = "Radiation"
	HazCategory(30) = "Sharp Objects"
	HazCategory(31) = "Slip / Trip / Fall"
	HazCategory(32) = "Storage"
	HazCategory(33) = "Stress"
	HazCategory(34) = "Temperature Extremes"
	HazCategory(35) = "Vehicles"
	HazCategory(36) = "Ventilation"
	HazCategory(37) = "Vibration"
	HazCategory(38) = "Violence to staff"
	HazCategory(39) = "Work Equipment"
	HazCategory(40) = "Other (See: Hazard Details)"

	Response.Write("<SELECT Name='" & ObjNAME & "' class='select'>")
	
	If HazValue = "S" Then
	    Response.Write("<option value='any'>any hazard type</option>")
	Else
		Response.Write("<option value='any'>Select a Hazard ...</option>")
	End if
	
    For HazARRAY = 0 to 40
        If HazValue = HazCategory(HazARRAY) Then
            Response.Write("<option value='" & HazCategory(HazARRAY) & "' style='background-color: #FFF000' selected='selected'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        Else
            Response.Write("<option value='" & HazCategory(HazARRAY) & "'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        End if
    Next
		
	Response.Write("</select>")
	
END SUB

	DIM PAFFECTED(22), paffARRAY

	PAFFECTED(0) = "Cleaners"
	PAFFECTED(1) = "Contractors"
	PAFFECTED(2) = "Employees"
	PAFFECTED(3) = "Engineers"
	PAFFECTED(4) = "Lone Workers"
	PAFFECTED(5) = "Machine Operators"
	PAFFECTED(6) = "Maintenance Staff"
	PAFFECTED(7) = "Members of the Public"
	PAFFECTED(8) = "Office Staff"
	PAFFECTED(9) = "Outdoor Workers"
	PAFFECTED(10) = "Patient"
	PAFFECTED(11) = "Pregnant Women"
	PAFFECTED(12) = "Production"
	PAFFECTED(13) = "Staff"
	PAFFECTED(14) = "Staff with Disabilities"
    PAFFECTED(15) = "Students"
	PAFFECTED(16) = "Students with Disabilities"
	PAFFECTED(17) = "Resident / Tenant"
	PAFFECTED(18) = "Trainees / Young Persons"
	PAFFECTED(19) = "Visitors"
	PAFFECTED(20) = "Volunteers"
	PAFFECTED(21) = "Warehouse Operators"
	PAFFECTED(22) = "Other"

SUB PER_AFFECTED(ObjNAME)



	Response.Write("<select name='" & ObjNAME & "' class='select'>")
	Response.Write(vbCrLf & "  <option>Select ...</option>")
	
    For paffARRAY = 0 to 18
        Response.Write(vbCrLf & "  <option value='" & PAFFECTED(paffARRAY) & "'>" & PAFFECTED(paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB

' -------- Body Parts in Injury ---------


Sub autogenBodyPart(myBodyPart) 
   
if  Session("MODULE_ACCB") ="2" then	

    
  objcommand.commandtext = "select opt_id, opt_value, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
            while not objrsBP.eof
                if cstr(myBodyPart) =  cstr(objrsBP("opt_id")) then
                    response.Write  "<option value='" & objrsBP("opt_id") & "'  selected='selected' >" &  objrsBP("opt_value") & "</option>"
                else
                    response.Write  "<option value='" & objrsBP("opt_id") & "'>" &  objrsBP("opt_value") & "</option>"
                end if
                
                 if objrsBP("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsBP("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsBPSUB = objcommand.execute
                        
                        if not objrsBPSUB.eof then 
                            while not objrsBPSUB.eof
                               if cstr(myBodyPart) =  cstr(objrsBPSUB("opt_id")) then
                                    response.Write  "<option value='" & objrsBPSUB("opt_id") & "' selected>--" & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
                                else
                                    response.Write  "<option value='" & objrsBPSUB("opt_id") & "'>--" & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>"
                                end if
                                                                   
                                objrsBPSUB.movenext
                            wend
                        
                        end if
                    
                    end if
                objrsBP.movenext
            wend
        end if
    
else
    Dim dictBodyPart
    Set dictBodyPart = CreateObject("Scripting.Dictionary")
    dictBodyPart.Add "0", "Ankle"
    dictBodyPart.Add "1", "Arm"
    dictBodyPart.Add "2", "Back (Lower)"
    dictBodyPart.Add "3", "Back (Upper)"
    dictBodyPart.Add "25", "Back - Unspecific"
    dictBodyPart.Add "4", "Buttocks"
    dictBodyPart.Add "5", "Chest"
    dictBodyPart.Add "6", "Eye"
    dictBodyPart.Add "7", "Face"
    dictBodyPart.Add "8", "Finger(s)"
    dictBodyPart.Add "9", "Foot"
    dictBodyPart.Add "10", "Groin"
    dictBodyPart.Add "11", "Hand injury"
    dictBodyPart.Add "12", "Head"
    dictBodyPart.Add "13", "Hip"
    dictBodyPart.Add "14", "Internal injuries"
    dictBodyPart.Add "15", "Knee"
    dictBodyPart.Add "24", "Leg - Unspecific"
    dictBodyPart.Add "16", "Multiple injuries"
    dictBodyPart.Add "17", "Neck"
    dictBodyPart.Add "18", "Shin"
    dictBodyPart.Add "19", "Shoulder"
    dictBodyPart.Add "20", "Stomach"
    dictBodyPart.Add "21", "Thigh"
    dictBodyPart.Add "22", "Toe(s)"
    dictBodyPart.Add "23", "Wrist"
    dictBodyPart.Add "na", "[ Not Specified ]" 
    
    
    myBodyPart = "" & myBodyPart
   For Each key In dictBodyPart
        Response.Write "<option value='" & key & "'"
        If myBodyPart = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictBodyPart.item(key) & "</option>" & vbCrLf
    Next
end if


  
End Sub

Function getBodyPartForCode(myBodyPart)
    if  Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("parent_value")) > 0 then
                getBodyPartForCode = objrsBP("parent_value") & " > " & objrsBP("opt_value")
           else
                getBodyPartForCode = objrsBP("opt_value")
           end if      
        else
           getBodyPartForCode =  "Body part could not be found"
        end if
    else
        myBodyPart = "" & myBodyPart
        getBodyPartForCode = dictBodyPart.Item(myBodyPart)
    end if
   
End function

' -------- Type of injury ---------

Dim dictInjType
Set dictInjType = CreateObject("Scripting.Dictionary")

dictInjType.Add "0", "Abrasion"
dictInjType.Add "1", "Asphyxiation"
dictInjType.Add "2", "Amputation"
dictInjType.Add "3", "Bite / Sting"
dictInjType.Add "4", "Bruise / Bump"
dictInjType.Add "5", "Burn / Scald"
dictInjType.Add "6", "Crush"
dictInjType.Add "7", "Cut / laceration"
dictInjType.Add "8", "Dislocation"
dictInjType.Add "9", "Electrical shock"
dictInjType.Add "10", "Fracture"
dictInjType.Add "11", "Internal injury"
dictInjType.Add "12", "Loss of consciousness"
dictInjType.Add "13", "Loss of sight"
dictInjType.Add "14", "Other injury"
dictInjType.Add "15", "Puncture"
dictInjType.Add "16", "Strain / Sprain"
dictInjType.Add "na", "[ Not Specified ]"

Sub autogenInjType(myType)
    myType = "" & myType
    For Each key In dictInjType
        Response.Write "<option value='" & key & "'"
        If myType = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictInjType.item(key) & "</option>" & vbCrLf
    Next
End Sub

Function getInjTypeForCode(myType)
    myType = "" & myType
    getInjTypeForCode = dictInjType.Item(myType)
End function


' --------- Apparent Cause of Injury ----------

Dim dictApparentCause
Set dictApparentCause = CreateObject("Scripting.Dictionary")

dictApparentCause.Add "0", "Animal Related"
dictApparentCause.Add "1", "Collision"
dictApparentCause.Add "3", "Contact with electricity"
dictApparentCause.Add "4", "Contact with machinery"
dictApparentCause.Add "2", "Contact with sharp object"
dictApparentCause.Add "15", "Exposure to explosion"
dictApparentCause.Add "6", "Exposure to fire"
dictApparentCause.Add "5", "Exposure to hazardous substance"
dictApparentCause.Add "7", "Exposure to temperature extremes"
dictApparentCause.Add "16", "Fainting / Fit"
dictApparentCause.Add "8", "Fall from height"
dictApparentCause.Add "9", "Manual Handling"
dictApparentCause.Add "10", "Repetitive actions"
dictApparentCause.Add "17", "Self inflicted"
dictApparentCause.Add "11", "Slip, trip or fall"
dictApparentCause.Add "12", "Struck by object"
dictApparentCause.Add "13", "Struck by vehicle"
dictApparentCause.Add "18", "Trapped by object"
dictApparentCause.Add "14", "Violence"
dictApparentCause.Add "na", "[ Not Specified ]"


Sub autogenApparentCause(myCause)
    myCause = "" & myCause
    For Each key In dictApparentCause
        Response.Write "<option value='" & key & "'"
        If myCause = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictApparentCause.item(key) & "</option>" & vbCrLf
    Next
End Sub
	
Function getApparentCauseForCode(myCause)
    myCause = "" & myCause
    getApparentCauseForCode = dictApparentCause.Item(myCause)
End function


'User type variables
' 0 All users
' 1 active users

sub AutoGenUserList(var_name, var_id, var_value, var_user_type, var_disabled, var_auto_submit)

         if ucase(var_auto_submit) = "Y" then
            var_auto_submit = " onchange='submit()' " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
        select case var_user_type
            case 1
                sql_variables = " and acc_status <> 'D'"
            case else
                sql_variables = ""
        end select

       Response.Write  "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_auto_submit & ">" & _
                    "<option value='0'>any user</option>"

	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_Bl,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code='" & Session("corp_code")& "' AND Corp_BG='" & Session("YOUR_GROUP") & "' AND Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY Per_sname ASC"
	SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & Ucase(objRs("Per_sname")) & ", " & objRs("Per_fname") & " (" & objRs("Corp_BD") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"
end sub

sub AutoGenCompanyList(var_name, var_id, var_value, var_disabled, var_auto_submit)
        if ucase(var_auto_submit) = "Y" then
           var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
         response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' > any " & session("CORP_TIER2") & "</option>"
		                        	
                            
	                            objCommand.Commandtext = "SELECT DISTINCT BU FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & _
	                                    " WHERE BG='" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & _
	                                    "' AND BU IS NOT NULL ORDER BY BU ASC"
	                            Set objRs = objCommand.Execute	
                            			
	                            while NOT objRs.EOF			
	                                if len(objRs("BU")) > 30 then
	                                    comp_len = "..."
	                                else
	                                    comp_len = ""
	                                end if
		                            if var_value = objRs("BU") then
			                            Response.Write 	"<option value='" & objRs("BU") & "' selected >" & left(objRs("BU"),30) & comp_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BU") & "'>" & left(objRs("BU"),30)  & comp_len &  "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                            
                                
	 response.Write "</select>"
        

end sub

sub AutoGenLocationList(var_name, var_id, var_value, var_disabled, var_auto_submit, var_comp_value)

        if ucase(var_auto_submit) = "Y" then
            var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if

    response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' >any " & session("CORP_TIER3") & "</option>"
		                           
                                    	
                            
	                             objCommand.Commandtext = "SELECT DISTINCT BL FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE BG = '" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & "' AND BU='" & var_comp_value & "' AND BL IS NOT NULL ORDER BY BL ASC"
	                                Set objRs = objCommand.Execute
                            			
	                            while NOT objRs.EOF			
		                            if len(objRs("BL")) > 30 then
	                                    loc_len = "..."
	                                else
	                                    loc_len = ""
	                                end if
		                        
		                            if var_value = objRs("BL") then
			                            Response.Write 	"<option value='" & objRs("BL") & "' selected >" & left(objRs("BL"),30) & loc_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BL") & "'>" & left(objRs("BL"),30) & loc_len & "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                                                           
	 response.Write "</select>"
end sub

sub AutoGenDepartmentList(var_name, var_id, var_value, var_disabled, var_auto_submit, var_comp_value, var_loc_value)
        if ucase(var_auto_submit) = "Y" then
                var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
          response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' >any " & session("CORP_TIER4") & "</option>"
		                           
                                    	
                            
	                             objCommand.Commandtext = "SELECT DISTINCT BD FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE BG = '" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & "' AND BU='" & var_comp_value & "' AND BL='" & var_loc_value & "' AND BD IS NOT NULL ORDER BY BD ASC"
	                                Set objRs = objCommand.Execute
                            			
	                            while NOT objRs.EOF			
		                            if len(objRs("BD")) > 30 then
	                                    dep_len = "..."
	                                else
	                                    dep_len = ""
	                                end if
		                        
		                            if var_value = objRs("BD") then
			                            Response.Write 	"<option value='" & objRs("BD") & "' selected >" & left(objRs("BD"),30) & dep_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BD") & "'>" & left(objRs("BD"),30) & dep_len & "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                                                           
	 response.Write "</select>"


end sub


sub AutoGenUserList2(var_name, var_id, var_value, var_user_type, var_disabled, var_jscript, var_default_val, var_default_text, var_default2_val, var_default2_text)

        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
        select case var_user_type
            case 1
                sql_variables = " and acc_status <> 'D'"
            case else
                sql_variables = ""
        end select

       Response.Write  "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_jscript & ">" 
              if var_value = var_default_val  then              
                    response.Write "<option value='" & var_default_val & "' selected='selected'>" & var_default_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default_val & "'>" & var_default_text & "</option>" 
              end if
              
              if var_value = var_default2_val  then              
                    response.Write "<option value='" & var_default2_val & "' selected='selected'>" & var_default2_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default2_val & "'>" & var_default2_text & "</option>" 
              end if
                    

	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_Bl,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code='" & Session("corp_code")& "' AND Corp_BG='" & Session("YOUR_GROUP") & "' AND Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY Per_sname ASC"
	SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & Ucase(objRs("Per_sname")) & ", " & objRs("Per_fname") & " (" & objRs("Corp_BD") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"
end sub

sub nmiss_class(val_sub_value,val_sub_name)
    DIM CLASS_Category(39),classARRAY

	CLASS_Category(0) = "Access / Egress"
	CLASS_Category(1) = "Adverse Weather"
	CLASS_Category(2) = "Animal"
	CLASS_Category(3) = "Biological"
	CLASS_Category(4) = "Collapse of Structure"
	CLASS_Category(5) = "Compressed Air"
	CLASS_Category(6) = "Confined Spaces"
	CLASS_Category(7) = "Drowning / Asphyxiation"
	CLASS_Category(8) = "D.S.E / V.D.U Usage"
	CLASS_Category(9) = "Electricity"
	CLASS_Category(10) = "Energy Release"
	CLASS_Category(11) = "Environmental"
	CLASS_Category(12) = "Ergonomics"
	CLASS_Category(13) = "Excavation"
	CLASS_Category(14) = "Explosion"
	CLASS_Category(15) = "Fall of Object from Height"
	CLASS_Category(16) = "Fall of Persons from  Height"
	CLASS_Category(17) = "Fire Safety"
	CLASS_Category(18) = "Food Hygiene"
	CLASS_Category(19) = "Gas"
	CLASS_Category(20) = "Hazardous Substance"
	CLASS_Category(21) = "Housekeeping"
	CLASS_Category(22) = "Human Factors"
	CLASS_Category(23) = "Lifting Equipment"
	CLASS_Category(24) = "Lighting"
	CLASS_Category(25) = "Machinery"
	CLASS_Category(26) = "Manual Handling"
	CLASS_Category(27) = "Noise"
	CLASS_Category(28) = "Pressure"
	CLASS_Category(29) = "Radiation"
	CLASS_Category(30) = "Sharp Objects"
	CLASS_Category(31) = "Slip / Trip / Fall"
	CLASS_Category(32) = "Storage"
	CLASS_Category(33) = "Stress"
	CLASS_Category(34) = "Temperature Extremes"
	CLASS_Category(35) = "Vehicles"
	CLASS_Category(36) = "Ventilation"
	CLASS_Category(37) = "Vibration"
	CLASS_Category(38) = "Violence"
	CLASS_Category(39) = "Work Equipment"

	Response.Write("<SELECT Name='" & val_sub_name & "' class='select'>")
	

		Response.Write("<option value='any'>Please select a classification ...</option>")
	
	
    For classARRAY = 0 to 39
        if len(val_sub_value) > 0 then
            If clng(val_sub_value) = clng(classARRAY) Then
                Response.Write("<option value='" & classARRAY & "' selected='selected'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            Else
                Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            End if
        Else
            Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
        End if
    Next
		
	Response.Write("</select>")
end sub 


Dim dictNMissInj
Set dictNMissInj = CreateObject("Scripting.Dictionary")
	
dictNMissInj.Add "1", "Death"
dictNMissInj.Add "4", "Dismemberment"
dictNMissInj.Add "2", "Fracture"
dictNMissInj.Add "3", "Cut"
dictNMissInj.Add "5", "Burn"
dictNMissInj.Add "6", "Bruise"
dictNMissInj.Add "7", "Physcological"
dictNMissInj.Add "8", "Illness / Disease"
dictNMissInj.Add "9", "Multiple Injuries"

Sub nmiss_inj_type(var_sub_name,var_sub_value)

    response.write "<select name='" & var_sub_name & "'  class='select'>" & _
                "<option value='0'>Please select an injury type</option>"
    var_sub_value = "" & var_sub_value
    For Each key In dictNMissInj
        Response.Write "<option value='" & key & "'"
        If var_sub_value = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictNMissInj.item(key) & "</option>" & vbCrLf
    Next
    response.Write "</select>"
End Sub
	
Function get_nmiss_inj_type(myinjtype)
    myinjtype = "" & myinjtype
    nmiss_inj_type = dictNMissInj.Item(myinjtype)
End function


Dim dictNMissPDam
Set dictNMissPDam = CreateObject("Scripting.Dictionary")
	
dictNMissPDam.Add "1", "Collapse of structure"
dictNMissPDam.Add "2", "High damage cost"
dictNMissPDam.Add "3", "Low damage cost"

Sub nmiss_prop_dam(var_sub_name,var_sub_value)

    response.write "<select name='" & var_sub_name & "'  class='select'>" & _
                "<option value='0'>Please select a property damage type</option>"
    var_sub_value = "" & var_sub_value
    For Each key In dictNMissPDam
        Response.Write "<option value='" & key & "'"
        If var_sub_value = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictNMissPDam.item(key) & "</option>" & vbCrLf
    Next
    response.Write "</select>"
End Sub
	
Function get_nmiss_prop_dam(myproptype)
    myproptype = "" & myproptype
    nmiss_prop_dam = dictNMissInj.Item(myproptype)
End function

'sub AutoGenIncCentreList()

'end sub

'sub AutoGenIncGroupList()

'end sub
%>