<%

'arrays
dim var_arry_vehicle_make(4)

var_arry_vehicle_make(1) = "Ford"
var_arry_vehicle_make(2) = "Renault"
var_arry_vehicle_make(3) = "Mercedes"
var_arry_vehicle_make(4) = "BMW"


dim var_arry_vehicle_colour(5)

var_arry_vehicle_colour(1) = "Red"
var_arry_vehicle_colour(2) = "Green"
var_arry_vehicle_colour(3) = "Blue"
var_arry_vehicle_colour(4) = "Black"
var_arry_vehicle_colour(5) = "Silver"


dim var_arry_vehicle_tax_status(3)

var_arry_vehicle_tax_status(1) = "Awaiting Delivery"
var_arry_vehicle_tax_status(2) = "Active in Vehicle"
var_arry_vehicle_tax_status(3) = "In Office"


dim var_arry_vehicle_serv_type(2)
var_arry_vehicle_serv_type(1) = "Full"
var_arry_vehicle_serv_type(2) = "Interim"

dim var_arry_vehicle_fuel(4)
var_arry_vehicle_fuel(1) = "Unleaded"
var_arry_vehicle_fuel(2) = "Diesel"
var_arry_vehicle_fuel(3) = "LPG"
var_arry_vehicle_fuel(4) = "Ethanol"

dim var_arry_vehicle_licence_type(13)
var_arry_vehicle_licence_type(1) = "A"
var_arry_vehicle_licence_type(2) = "A1"
var_arry_vehicle_licence_type(3) = "B"
var_arry_vehicle_licence_type(4) = "B1"
var_arry_vehicle_licence_type(5) = "C"
var_arry_vehicle_licence_type(6) = "C1"
var_arry_vehicle_licence_type(7) = "D"
var_arry_vehicle_licence_type(8) = "D1"
var_arry_vehicle_licence_type(9) = "F"
var_arry_vehicle_licence_type(10) = "G"
var_arry_vehicle_licence_type(11) = "H"
var_arry_vehicle_licence_type(12) = "K"
var_arry_vehicle_licence_type(13) = "P"

dim var_arry_vehicle_tax_class(22)
var_arry_vehicle_tax_class(1) = "Petrol car (TC48) and Diesel car (TC49) - A"
var_arry_vehicle_tax_class(2) = "Petrol car (TC48) and Diesel car (TC49) - B"
var_arry_vehicle_tax_class(3) = "Petrol car (TC48) and Diesel car (TC49) - C"
var_arry_vehicle_tax_class(4) = "Petrol car (TC48) and Diesel car (TC49) - D"
var_arry_vehicle_tax_class(5) = "Petrol car (TC48) and Diesel car (TC49) - E"
var_arry_vehicle_tax_class(6) = "Petrol car (TC48) and Diesel car (TC49) - F"
var_arry_vehicle_tax_class(7) = "Petrol car (TC48) and Diesel car (TC49) - G"
var_arry_vehicle_tax_class(8) = "Alternative fuel car (TC59) - A"
var_arry_vehicle_tax_class(9) = "Alternative fuel car (TC59) - B"
var_arry_vehicle_tax_class(10) = "Alternative fuel car (TC59) - C"
var_arry_vehicle_tax_class(11) = "Alternative fuel car (TC59) - D"
var_arry_vehicle_tax_class(12) = "Alternative fuel car (TC59) - E"
var_arry_vehicle_tax_class(13) = "Alternative fuel car (TC59) - F"
var_arry_vehicle_tax_class(14) = "Alternative fuel car (TC59) - G"
var_arry_vehicle_tax_class(15) = "Light goods vehicles (TC39)"
var_arry_vehicle_tax_class(16) = "Euro 4 light goods vehicles (TC36)"
var_arry_vehicle_tax_class(17) = "Motorcycle (with or without a sidecar) (TC17) <= 150cc"
var_arry_vehicle_tax_class(18) = "Motorcycle (with or without a sidecar) (TC17) 151 - 400cc"
var_arry_vehicle_tax_class(19) = "Motorcycle (with or without a sidecar) (TC17) 401 - 600cc"
var_arry_vehicle_tax_class(20) = "Motorcycle (with or without a sidecar) (TC17) > 600cc"
var_arry_vehicle_tax_class(21) = "Tricycles (not over 450kg unladen) (TC50) - <= 150cc"
var_arry_vehicle_tax_class(22) = "Tricycles (not over 450kg unladen) (TC50) - All other Tricycles"



dim var_arry_vehicle_ownership_type(5)
var_arry_vehicle_ownership_type(1) = "Business"
var_arry_vehicle_ownership_type(2) = "Rented"
var_arry_vehicle_ownership_type(3) = "Leased"
var_arry_vehicle_ownership_type(4) = "Private"
var_arry_vehicle_ownership_type(5) = "Courtesy car"



dim var_arry_vehicle_status(6)
var_arry_vehicle_status(1) = "Active"
var_arry_vehicle_status(2) = "Awaiting Repair"
var_arry_vehicle_status(3) = "Undergoing Repair"
var_arry_vehicle_status(4) = "SORN"
var_arry_vehicle_status(5) = "Impounded"
var_arry_vehicle_status(6) = "Stolen"


'functions




'subs
sub autogen_vehicle_make(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any make</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a make</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_make) + 1 to ubound(var_arry_vehicle_make)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_make(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
end sub


sub autogen_vehicle_colour(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any colour</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a colour</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_colour) + 1 to ubound(var_arry_vehicle_colour)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_colour(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub
 
 
 sub autogen_vehicle_tax_status(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any status</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a status</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_tax_status) + 1 to ubound(var_arry_vehicle_tax_status)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_tax_status(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
end sub

 
 sub autogen_vehicle_serv_type(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any service type</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a service type</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_serv_type) + 1 to ubound(var_arry_vehicle_serv_type)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_serv_type(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
end sub

sub autogen_vehicle_fuel(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any fuel type</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a fuel type</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_fuel) + 1 to ubound(var_arry_vehicle_fuel)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_fuel(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub



sub autogen_vehicle_licence_type(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any licence category</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a licence category</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_licence_type) + 1 to ubound(var_arry_vehicle_licence_type)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_licence_type(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub
 
 
sub autogen_vehicle_tax_class(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any tax class</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a tax class</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_tax_class) + 1 to ubound(var_arry_vehicle_tax_class)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_tax_class(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub
 
 sub autogen_vehicle_ownership_type(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any ownership type</option>"        
                        case else
                            response.Write    "<option value='0'>Please select an ownership type</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_ownership_type) + 1 to ubound(var_arry_vehicle_ownership_type)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_ownership_type(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub


 sub autogen_vehicle_status(sub_vehicle_value, sub_vehicle_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) 
    'sub_sw3 = Javascript
    'sub_sw5 = Empty
    'sub_sw6 = Empty

    if isnull(sub_vehicle_value) then sub_vehicle_value = ""

 Response.Write  "<select size='1' name='" & sub_vehicle_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any status</option>"        
                        case else
                            response.Write    "<option value='0'>Please select a status</option>"       
                    end select 
                    
                    '************************************************************
                    'Initially written from array, convert to db model eventually
                    '************************************************************
                    
                    for i = lbound(var_arry_vehicle_status) + 1 to ubound(var_arry_vehicle_status)
                        response.Write "<option value='" & i & "' " 
                        if cstr(i) = cstr(sub_vehicle_value) then
                            response.Write " selected "              
                        end if
                    response.Write ">" & var_arry_vehicle_status(i) & "</option>"
                    next
                        
                   
 Response.Write  "</select>"
 
 end sub

%>