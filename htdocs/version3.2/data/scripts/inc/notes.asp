<%

sub pr_notes_save(var_frm_notetext, var_frm_noterelationref, var_frm_notemod)

    ' Ok just incase validation fails
    if len(var_frm_notetext) > 0 then
        Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINTNOTES") & " (" & _
                                 "corp_code," & _ 
                                 "noterelationref," & _
                                 "notemod," & _
                                 "notedatetime," & _
                                 "noteby," & _
                                 "notetext) " & _ 
                                 "VALUES(" & _
                                 "'" & Session("corp_code") & "'," & _
                                 "'" & var_frm_noterelationref & "'," & _
                                 "'" & var_frm_notemod & "'," & _
                                 "'" & date() & " " & time() & "'," & _
                                 "'" & Session("YOUR_ACCOUNT_ID") & "'," & _
                                 "'" & var_frm_notetext & "')"
        Objcommand.execute
     end if
end sub

sub pr_notes_display_html(var_module, section_name, allow_add, reference)
    ' This script writes out the html that displays the note

    response.write "<div id='" & section_name & "' class='sectionarea notearea'>" & _
   " <a href='#' name='" & section_name & "' id='" & section_name & "'></a>" & _
   " <form action='/version3.2/data/scripts/inc/notes_scripts.asp?cmd=notes_sav&mod=DSE&reference=" & reference & "' method='post'>"  & _    
        "<h3>Record Notes / History</h3>"
  
  
         Response.Write "<div class='addnote' id='addnote' style='display: none;'>" & _
        "<table width='100%' cellpadding='2px' cellspacing='2px' class='highlight'>" & _
        "<tr><th>Enter the information you wish to save as a note</th></tr>" & _
        "</table><table width='100%' cellpadding='2px' cellspacing='2px' class='showtd highlight'>" & _
        "<tr><td width='40' class='c noborder'><img src='/version3.2/modules/hsafety/risk_assessment/app/img/icon_memo_new.png' title='Note related to this assessment' /></td><td class='c'><textarea name='note_text' id='note_text' class='textarea' rows='5'></textarea></td><th width='130' class='c'><input type='submit' name='frm_submit' id='frm_submit' class='submit' value='Save note' /></th></tr>" & _
        "</table>" & _
        "</div>"

        objcommand.commandtext =    "SELECT Notes.notetext as col_note, Notes.notedatetime as col_notedate, hrusers.per_fname + ' ' + hrusers.per_sname as col_notename FROM " & Application("DBTABLE_GLOBAL_FPRINTNOTES") & " as Notes " & _
							        "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as hrusers " & _
                                    "ON notes.corp_code = hrusers.corp_code " & _
                                    "AND notes.NoteBy = hrusers.acc_ref " & _
							        "WHERE Notes.NoteRelationref = '" & reference & "' AND Notes.corp_code='" & Session("corp_code") & "' ORDER BY Notes.ID DESC"
        SET Objrs = Objcommand.execute
       
        
        if objrs.eof then
        
                           ' ok show add note 
                
          
                Response.Write  "<div class='warning'>" & _
                                "<table width='100%' cellpadding='2px' cellspacing='2px'>" & _
                                "<tr><td height='40'><strong>NOTICE</strong><br />There are currently no notes associated with the assessment." 
                                
                  if allow_add then response.Write "If you would like to add a note to keep a history of any relevant information or changes, click on &quotAdd note&quot to begin.</td><td><a href='#" & section_name & "' onclick='add_note()'><img src='img/sml_add_note.png' /></a>" 
                              
                  Response.Write    "</td></tr></table>" & _
                                "</div>"
          
        
        else                         
               

                Response.Write  "<table width='100%' cellpadding='5px' cellspacing='2px'>" & _
                                "<tr><td><p>Listed below in date order (<em>descending</em>) are any notes/history that are associated with this assessment.</p></td>"
                                
                if allow_add then response.Write   "<td class='c yel_th_box' valign='middle' width='140'><a href='#" & section_name & "' onclick='add_note()'><img src='img/sml_add_note.png' /></a></td>"
                               
                Response.Write "</tr></table><br />"
           
            while not objrs.eof
        
            var_frm_note = objrs("col_note")
            var_frm_user = objrs("col_notename")
            var_frm_date = objrs("col_notedate")
            var_frm_noteimg = "<img src='/version3.2/modules/hsafety/risk_assessment/app/img/icon_memo_norm.png' title='Note related to this assessment' />"
        
            ' error correction
            if (var_frm_user = " ") or (len(var_frm_user) < 1) or (IsNull(var_frm_user) = true) then var_frm_user = "Unknown" end if
            if (var_frm_date = " ") or (len(var_frm_date) < 1) or (IsNull(var_frm_date) = true) then var_frm_date = "Unknown" end if
            if (var_frm_note = " ") or (len(var_frm_note) < 1) or (IsNull(var_frm_note) = true) then var_frm_note = "No note information" end if
        
            Response.write  "<table width='100%' cellpadding='1px' cellspacing='2px' class='showtd shownotes'>" & _
                            "<tr><td width='40' class='c noborder' valign='top'>" & var_frm_noteimg & "</td><td>" & var_frm_note & "<p>Added by " & var_frm_user & " on " & var_frm_date & "&nbsp;</p></tr>" & _
                            "</table>"

            Objrs.movenext
            wend
        end if
      
    response.write "</form>" & _
    "</div>"

end sub









%>