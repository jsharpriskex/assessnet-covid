<%

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Sub (checkUSER) instructions of use                    ''
'' This function requires,                                ''
''                        varfrmUSER = Username form input''
''                        varfrmPASS = Password from input''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub checkUSER(varfrmUSER,varfrmPASS)
    Objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_USER_DATA") & " " & _
                             "WHERE (acc_name = '" & varfrmUSER & "') AND (acc_pass = '" & varfrmPASS & "') "
    Set objRs = Objcommand.execute
    
    If objRs.EOF Then
        '' User details invalid make log entry
        Call useLOG("2","Username or Password invalid",varfrmUSER)
        Call errCode("0001")
    Else
        ' Make Session elements
        Session.TimeOut = 20
        Session("YOUR_FULLNAME") = objRs("Per_fname") & " " & objRs("Per_sname")
        Session("YOUR_EMAIL") = objRs("Corp_email")
        Session("YOUR_ACCESS") = objRs("Acc_level")
        Session("YOUR_GROUP") = objRs("Corp_BG")
        Session("YOUR_COMPANY") = objRs("Corp_BU")
        Session("YOUR_LOCATION") = objRs("Corp_BL")
        Session("YOUR_DEPARTMENT") = objRs("Corp_BD")
        Session("YOUR_PASSWORD") = objRs("Acc_pass")
        Session("YOUR_MQUESTION") = objRs("Acc_mquestion")
        Session("YOUR_MANSWER") = objRs("Acc_manswer")
        Session("YOUR_USERNAME") = objRs("Acc_name")
        Session("YOUR_LICENCE") = objRs("Acc_licence")
        Session("YOUR_ACCOUNT_ID") = objRs("Acc_ref")
        Session("YOUR_FIRSTNAME") = objRs("Per_fname")
        Session("YOUR_MIDDLENAME") = objRs("Per_mname")
        Session("YOUR_SURNAME") = objRs("Per_sname")
        Session("YOUR_TITLE") = objRs("Per_title")
         Session("YOUR_ADDR_STREET") = objRs("Per_street")
        Session("YOUR_ADDR_AREA") = objRs("Per_town")
        Session("YOUR_ADDR_COUNTY") = objRs("Per_county")
        Session("YOUR_ADDR_COUNTRY") = objRs("Per_country")
        Session("YOUR_ADDR_POSTCODE") = objRs("Per_pcode")
        Session("YOUR_PHONE") = objRs("Corp_phone")
        Session("YOUR_FAX") = objRs("Corp_fax")
        Session("CORP_CODE") = objRs("Corp_code")
        Session("CORP_PIN") = objRs("Corp_pin")
        Session("CORP_ACCBOOK") = objRs("Corp_Accbook")
        Session("YOUR_JOB_TITLE") = objRs("per_job_title")
        Session("YOUR_ACCESS_STATUS") = objRs("Acc_status")
        Session("YOUR_IP_ADDRESS") = Request.ServerVariables("REMOTE_ADDR")
        Session("SA_CONTRACTS") = objRs("sa_contracts")
        Session("SA_ADMIN") = objRs("acc_sadmin")
        
        '' Build SD elements
        If Len(objRs("sd_users_shown")) > 0 Then Session("SD_USERS_SHOWN") = objRs("sd_users_shown") & ""
        If Len(objRs("sd_users_allowed")) > 0 Then Session("SD_USERS_ALLOWED") = objRs("sd_users_allowed") & "" 
        If Len(objRs("sd_start_hour")) > 0 Then Session("SD_START_HOUR") = objRs("sd_start_hour")
        If Len(objRs("sd_end_hour")) > 0 Then Session("SD_END_HOUR") = objRs("sd_end_hour")
        
        '' Add additional accident data
        If Len(Session("CORP_ACCBOOK")) > 1 THEN
            ObjCommand.Commandtext = "SELECT REC_BU,REC_BL FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " WHERE ACCB_CODE='" & Session("CORP_ACCBOOK") & "' AND CORP_CODE='" & Session("CORP_CODE") & "' "
            Set objRs = objcommand.execute

            If objRs.EOF THEN
                ' Remove un-needed session variables
                Session.Contents.Remove("CORP_ACCBOOK")
                Session.Contents.Remove("YOUR_ADDR_STREET")                        
                Session.Contents.Remove("YOUR_ADDR_AREA")
                Session.Contents.Remove("YOUR_ADDR_COUNTY")
                Session.Contents.Remove("YOUR_ADDR_POSTCODE")
                Session.Contents.Remove("YOUR_JOB_TITLE")
            Else 
                Session("CORP_ACCBOOK_UNIT") = objRs("REC_BU")
                Session("CORP_ACCBOOK_LOC") = objRs("REC_BL")
            End if   
        End if
    
		                    Session.Contents.Remove("MODULE_RA")
                            Session.Contents.Remove("MODULE_DSE")
                            Session.Contents.Remove("MODULE_ACCB")
                            Session.Contents.Remove("MODULE_TRA")
                            Session.Contents.Remove("MODULE_MH")
                            Session.Contents.Remove("MODULE_SP")
                            Session.Contents.Remove("MODULE_FS")
                            Session.Contents.Remove("MODULE_SA")
                            Session.Contents.Remove("MODULE_CMS")
                            Session.Contents.Remove("MODULE_SD")
                            Session.Contents.Remove("MODULE_PTW")
        
        ' Build licence information for admins
        If (Session("YOUR_ACCESS") < 2) Then
            objCommand.Commandtext = "SELECT corp_licence_type, corp_licence_version FROM " & Application("DBTABLE_MODULES_DATA") & " WHERE corp_code='" & Session("CORP_CODE") & "' "
            Set objRs = objcommand.execute

            If objRs.EOF Then
                ' No modules - tell user of error
                IF Len(Session("SA_CONTRACTS")) < 1 Then
                Call errCode("0002")
                End IF
            Else
                While NOT objRs.EOF
                    SELECT CASE objRs("corp_licence_type") 
                         CASE "RA"
                            Session("MODULE_RA") = objRs("corp_licence_version")
                         CASE "DSE"
                            Session("MODULE_DSE") = objRs("corp_licence_version")
                         CASE "ACCB"
                            Session("MODULE_ACCB") = objRs("corp_licence_version")
                         CASE "TRA"
                            Session("MODULE_TRA") = objRs("corp_licence_version")
                         CASE "MH"
                            Session("MODULE_MH") = objRs("corp_licence_version")
                         CASE "SP"
                            Session("MODULE_SP") = objRs("corp_licence_version")
                         CASE "FS"
                            Session("MODULE_FS") = objRs("corp_licence_version")
                         CASE "SA"
                            Session("MODULE_SA") = objRs("corp_licence_version")
                         CASE "CMS"
                            Session("MODULE_CMS") = objRs("corp_licence_version")
                         CASE "SD"
                            Session("MODULE_SD") = objRs("corp_licence_version")
                         CASE "PTW"
							Session("MODULE_PTW") = objRs("corp_licence_version")
				         CASE "COSHH"
							Session("MODULE_COSHH") = objRs("corp_licence_version")
					     CASE "MS"
							Session("MODULE_MS") = objRs("corp_licence_version")
					     CASE "FC"
							Session("MODULE_FC") = objRs("corp_licence_version")
                    End Select
                objRs.MoveNext
                Wend
            End IF
            
        End IF

        ' Check for disabled users
        If Session("YOUR_ACCESS_STATUS") = "D" Then
            Call useLOG("2","User Disabled",varfrmUSER)
            Call errCode("0004")
        ' Are you a VAR account?
        ElseiF UCASE(right(varfrmUSER,3)) = "VAR" Then
            Call useLOG("2","Access OK", varfrmUSER)
            Session("AUTH") = "Y"
            LoginURL = "../../data/layout/"

            'OK your a VAR so disable the account to stop usage once logged out
            objCommand.CommandText = "UPDATE " & Application("DBTABLE_USER_DATA") & " SET Acc_status = 'D' WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ACC_NAME='" & varfrmUSER & "'"
            objCommand.Execute       

        ElseiF len(Session("SA_CONTRACTS")) > 0 Then
            Session("PW_OK") = "Y"
            Session("SA_OK") = "Y"
            LoginURL = "../Loginv2/frm_lg_meminfo.asp"
        ' If Access requires more security, set phase 2 flag (mem info) or is R-only
        Elseif (Session("YOUR_ACCESS") < 2) OR (Len(Session("CORP_ACCBOOK")) > 0) Then
            Session("PW_OK") = "Y"
            LoginURL = "../Loginv2/frm_lg_meminfo.asp"                
        Elseif Session("YOUR_ACCESS") = "2" Then
            Session("AUTH") = "Y"
            CALL useLOG("1","Access OK",varfrmUSER)
            LoginURL = "../Loginv2/frm_lg_access.asp?CMD=L"
        ' If read only then check IP address
        Elseif ipCheck(Session("YOUR_IP_ADDRESS")) = "TRUE" Then
            Session("AUTH") = "Y"
            Call useLOG("1","Access OK",varfrmUSER)
            LoginURL = "../Loginv2/frm_lg_access.asp?CMD=L"
        Else
            ' IP security failed
            Call useLOG("2","Security Failed to Validate",varfrmUSER)
            Call errCode("0003")
        End if            
    End if
End Sub

%>