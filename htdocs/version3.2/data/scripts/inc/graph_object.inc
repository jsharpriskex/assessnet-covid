<%
Sub endConnections()

	Objconn.close
	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING

End Sub

Sub startConnections()

	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH

	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn
End Sub

Sub makeGRAPH(varTitle,vartype,varResults(),varTopics(),varSAVE)

    ON ERROR RESUME NEXT

    'Drop object in memory
    Set MSGraph = Server.CreateObject("MSGraph.Application.8")
    'Create new data and chart
    Set ds = MSGraph.Application.DataSheet
    Set chart = MSGraph.Application.Chart
    
    'Make sure data is clear
    ds.Cells.Clear
           
    'Create topics from passed array
    For I = LBound(varTopics) to UBound(varTopics) ' Find upper and lower array value
        Cellloc = I + 1 ' ReSync to suit data
        ds.Cells(Cellloc,1).Value = varTopics(I) ' Create topic cell, insert data
    Next  

    'Set Loop values
    Ipos = 0 ' data count in sync with topic amount
    Cellrow = 2 ' hold value of current row
    varChart_height = 100 ' hold value of chart plotarea
    
    'Create results from passed array
    For I = LBound(varResults) + 1 to UBound(varResults) ' Find upper and lower array value
        Cellloc = I + 1 ' ReSync to suit data
        If Ipos > UBound(varTopics) Then ' Sort results by topic amount
            Ipos = 0 ' Reset count
            Cellrow = Cellrow + 1 ' Restart new row of data
        End if
        ds.Cells(Ipos + 1,Cellrow).Value = varResults(I) ' Create result cell, insert data				
        Ipos = Ipos + 1 ' increment result count
        varChart_height = varChart_height + 15
    Next
        
    'Set chart properties
    chart.Width = 650
    chart.Height = varChart_height
    'chart.Axes(1).HasTitle = True
    'chart.Axes(1).AxisTitle.Caption = "Departments"
    chart.ChartType = vartype ' Type of chart defined by user
    chart.HasLegend = True
    chart.Legend.Font.Size = 8
    chart.Legend.Font.bold = False
    chart.Legend.AutoScaleFont = False
    chart.ChartArea.Font.Size = 8
    chart.ChartArea.Font.bold = False
    chart.ChartArea.AutoScaleFont = False
    
    IF Len(varTitle) > 0 Then
        chart.HasTitle = True
        chart.ChartTitle.Text = varTitle
        chart.ChartTitle.Font.Bold = True
    End IF
    
    chart.ApplyDataLabels
   
    'Create topics formatting
    For I = LBound(varTopics) to UBound(varTopics) ' Find upper and lower array value
        Cellloc = I + 1 ' ReSync to suit data
        IF cellloc =< UBound(varTopics) Then
        chart.SeriesCollection(Cellloc).DataLabels.AutoScaleFont = FALSE
        chart.SeriesCollection(Cellloc).DataLabels.Font.Size = 8
        chart.SeriesCollection(Cellloc).DataLabels.Font.bold = False
        END IF
    Next  

    chart.PlotArea.Width = 350
    chart.PlotArea.Height = varChart_height
    chart.PlotArea.Interior.ColorIndex = 0
    'Generate filename as AccountID, Date and Time with replace to make it a whole number.
    
    varFile_name = Session("YOUR_ACCOUNT_ID") & Date() & Time()
    varFile_name = Replace(varFile_name,"/","")
    varFile_name = Replace(varFile_name,":","")
    
    strFile_Graph = "/_temp/" & varFile_name & ".gif"
    tFile = Server.MapPath(strFile_Graph)
    chart.Export tFile
    
    'If error occurred during proccessing, do not make graph, make report
    If ERR Then
    
        Response.Write "<p><b>Error</b> - Graph cannot be generated" & _
                       "<p><b>Chart Type :: </b>" & vartype & _
                       "<p><b>Topics:</b> "
                        
        For i = LBound(varTopics) to UBound(varTopics) ' Find upper and lower array value
            Response.write varTopics(i)  & ", "  ' show data
        Next
                   
        Response.write "<p>" & _
                       "<b>Results:</b> "
                       
        For I = LBound(varResults) to UBound(varResults) ' Find upper and lower array value
            Response.write varResults(I)  & ", "  ' show data
        Next
    Else
        If varSAVE = "on" Then
            Call startCONNECTIONS()
            Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_ARCMOD_GRAPH_SAVED") & " " & _
                                     "(CORP_CODE,RECORDCREATOR,GRAPHTITLE,GRAPHTYPE,GRAPHFILENAME,GRAPHSAVED) " & _
                                     "VALUES('" & SESSION("CORP_CODE") & "','" & SESSION("YOUR_ACCOUNT_ID") & "','" & varTitle & "','" & ACC & "','" & STRFILE_GRAPH & "','" & Date() & " " & Time() & "')"
            Objcommand.execute
            Call endCONNECTIONS()
        End if
        Redirect_URL = "result.asp?loc=" & strFile_Graph & "&gendate=" & Date() & "&gentime=" & Time()
        Response.Redirect(Redirect_URL)
    End if
End sub
%>