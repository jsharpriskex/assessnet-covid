<%


sub display_custom_guidance_docs(module_code)

     objcommand.commandtext = "module_global.dbo.sp_return_attachments '" & session("corp_code") & "', '" & module_code & "_GU'"
     set objrs = objcommand.execute
     
     if not objrs.eof or session("YOUR_ACCESS") = "0" then

     %>
     <div id='windowtitle' class='blu_bar' >
            <h5>Downloadable Guidance</h5>
             <div id='windowdrop'>
                <table width='100%'>
                <tr>
                    <th>Hidden filter</th><td></td>
                </tr>   
                </table>
             </div>
          <!-- hidden drop end -->
          <div id='windowmenu'>
            <%if session("YOUR_ACCESS") = "0" then %>
            <a href='javascript:OpenAttach("<%=module_code%>_GU")'>Options</a>
            <%end if %>
          </div>
          <!-- body of window -->
          <div id='windowbody' class='pad'>
                
                   <%  

                  
                  if objrs.eof then                     
                        'print standard 
                         response.write "<div class='warning' >" & _
                            "<table width='100%' cellpadding='2px' cellspacing='2px' style='height: 50px;'>   "
                        response.write "<tr>" & _
                                "<th width='40px'>&nbsp;</th>" & _                            
                            "<td valign='middle'><strong>No Guidance Added</strong><br />N.B. This section will not appear for non Global Administrator accounts whilst no guidance is attached.</td>" & _                           
                        "</tr></table>"
                    else
                         response.write "<div class='news' style='max-height: 200px; overflow: auto'>"
                           
                        while not objrs.eof
                        
                            var_db_col_name = objrs("UP_STOR_name")
                            var_db_col_ext = replace(objrs("UP_STOR_EXT"),".","")
                            if var_db_col_ext <> "pdf" then
                                var_db_col_ext = "std"                          
                            end if
                            var_db_col_path = objrs("UP_STOR_VPATH")
                            var_db_col_title = objrs("UP_STOR_title") & " (" & var_db_col_name & ")"
                            

                           response.write "<table width='100%' cellpadding='2px' cellspacing='2px'><tr>" & _
                                "<td width='3' valign='middle'><img src='" & Application("CONFIG_PROVIDER_SECURE") & "/version" & Application("CONFIG_VERSION")  & "/images/icons/" & var_db_col_ext & "_icon_yellow.png' /></td>" & _
                                "<td valign='middle'>" & var_db_col_title & "</td>" & _
                                "<td width='5'><a href='" & Application("CONFIG_PROVIDER") & "/uploads/" & var_db_col_path & var_db_col_name & "' ' target='_blank'><img src='" & Application("CONFIG_PROVIDER_SECURE") & "/version" & Application("CONFIG_VERSION")  & "/images/icons/sml_viewclr.gif' title='Open this PDF' /></a></td>" & _
                            "</tr></table>"
                            objrs.movenext
                        wend
                    end if
                    %>
                    
                </div>
           
            </div>

        </div>

<%
    end if
end sub

sub display_custom_guidance_docs_DOTNET(module_code)

     objcommand.commandtext = "module_global.dbo.sp_return_attachments '" & session("corp_code") & "', '" & module_code & "_GU'"
    set objrs = objcommand.execute
     
     if not objrs.eof or session("YOUR_ACCESS") = "0" then

     %>
     <div class="panel_wrapper">
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-bars" aria-hidden="true"></i> Downloadable Guidance<div class="panel-options"><%if session("YOUR_ACCESS") = "0" then %><a class="btn btn-primary btn-sm panel_button" href='javascript:OpenAttach("<%=module_code%>_GU")'>Manage</a><% end if %></div></div>
                <div class="panel-body">

                    <table class="table table-bordered">
                        <tbody>
                            <%  if objrs.eof then  %>
                            <tr><td>No Guidance Added. This section will not appear for non Global Administrator accounts whilst no guidance is attached.</td></tr>
                            <% else %>
                                
                                <% while not objrs.eof 
                                        var_db_col_name = objrs("UP_STOR_name")
                                        var_db_col_ext = replace(objrs("UP_STOR_EXT"),".","")
                                        if var_db_col_ext <> "pdf" then
                                            var_db_col_ext = "std"                          
                                        end if
                                        var_db_col_path = objrs("UP_STOR_VPATH")
                                        var_db_col_title = objrs("UP_STOR_title") '& " (" & var_db_col_name & ")"
                                    
                                        response.write "<tr><td><i class='fa fa-file-pdf-o margin-right-5'> </i><a class='red-link' href='" & Application("CONFIG_PROVIDER") & "/uploads/" & var_db_col_path & var_db_col_name & "' ' target='_blank'>" & var_db_col_title & "</a></td></tr>"
                                        objrs.movenext
                                    wend
                            end if
                                    
                                    %>     
                                  
                            
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

<%
    end if
end sub


%>