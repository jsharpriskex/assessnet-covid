


<%

sub isSessionValid()

	Dim objConn_session
	Dim objCommand_session
	Dim objRs_session

	SET objconn_session = Server.CreateObject("ADODB.Connection")
	WITH objconn_session
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH

	SET objCommand_session = Server.CreateObject("ADODB.Command")
	SET objCommand_session.ActiveConnection = objconn_session
	objcommand_session.CommandTimeout = 300

	userSessionGUID = Request.Cookies("sync")

	if len(userSessionGUID) > 0 then

		objCommand_session.commandText = "EXEC Module_LOG.dbo.checkSessionStatus '" & userSessionGUID & "'"
		set objRs_session = objCommand_session.execute
	
		if not objRs_session.EOF then

			if objRs_session("status") = true and len(session("YOUR_FIRSTNAME")) = 0 then
				session("CORP_CODE") = objRs_session("corp_code")
				session("YOUR_ACCOUNT_ID") = objRs_session("acc_ref")
				session("AUTH") = "Y"

				if isnull(objRs_session("sessionTimeout")) = false then
					session.timeout = objRs_session("sessionTimeout")
                    call rebuildSession()
                end if
			elseif objRs_session("status") = false then
                session("AUTH") = ""
            end if
        else
            session("AUTH") = ""
		end if

		set objRs_session = nothing
	end if

	
	Objconn_session.close
	SET objCommand_session.ActiveConnection = NOTHING
	SET objcommand_session = NOTHING
	SET objconn_session = NOTHING
	SET objrs_session = NOTHING

end sub







Sub rebuildSession()

	Dim objConn_session
	Dim objCommand_session
	Dim objRs_session


	SET objconn_session = Server.CreateObject("ADODB.Connection")
	WITH objconn_session
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH

	SET objCommand_session = Server.CreateObject("ADODB.Command")
	SET objCommand_session.ActiveConnection = objconn_session
	objcommand_session.CommandTimeout = 300


	    objCommand_session.commandtext = "SELECT hr.*, tier1.struct_name as tier1_name, tier2.struct_name as tier2_name, tier3.struct_name as tier3_name, tier4.struct_name as tier4_name, platforms.domain, platforms.platform   FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _     
									"LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER1")& "  AS tier1 " & _
									 "on hr.corp_code = tier1.corp_code " & _
									 "and hr.corp_bg = tier1.tier1_ref " & _  
									 "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER2")& "  AS tier2 " & _
									 "on hr.corp_code = tier2.corp_code " & _
									 "and hr.corp_bu = tier2.tier2_ref " & _         
									 "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER3")& "  AS tier3 " & _
									 "on hr.corp_code = tier3.corp_code " & _
									 "and hr.corp_bl = tier3.tier3_ref " & _                                      
									 "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4")& "  AS tier4 " & _
									 "on hr.corp_code = tier4.corp_code " & _
									 "and hr.corp_bd = tier4.tier4_ref " & _     
									 "LEFT OUTER JOIN " & "Module_HR.dbo.HR_Systems" & " AS systems " & _
									 "on hr.corp_code = systems.corp_code " & _
									 "LEFT OUTER JOIN " & Application("DBTABLE_HR_PLATFORMS") & " AS platforms " & _
									 "on systems.platform = platforms.platform " & _
									 "WHERE (hr.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' AND hr.corp_code = '" & session("CORP_CODE") & "') AND (hr.acc_level < '5')" 
	    set objRs_session = objCommand_session.execute

        session("SESSION_HOME_DOMAIN") = Application("CONFIG_PROVIDER")
        session("SESSION_HOME_PLATFORM") = objRs_session("platform")


        ' record which server we're connected to by reference rather than by name

       objCommand_session.commandText = "SELECT platform FROM " & Application("DBTABLE_HR_PLATFORMS") & " WHERE domain = '" & Application("CONFIG_PROVIDER") & "'"
        set objRs_platforms = objCommand_session.execute
        if not objRs_platforms.EOF then
            session("SESSION_CURRENT_PLATFORM") = objRs_platforms("platform")
        end if



        ' Make Session elements

   
        Session("YOUR_FULLNAME") = objRs_session("Per_fname") & " " & objRs_session("Per_sname")
        Session("YOUR_EMAIL") = objRs_session("Corp_email")
        Session("YOUR_ACCESS") = objRs_session("Acc_level")
        Session("YOUR_GROUP") = objRs_session("Corp_BG")
        Session("YOUR_COMPANY") = objRs_session("Corp_BU")
        Session("YOUR_LOCATION") = objRs_session("Corp_BL")
        Session("YOUR_DEPARTMENT") = objRs_session("Corp_BD")        
        Session("YOUR_TIER5") = objRs_session("tier5_ref")
        Session("YOUR_TIER6") = objRs_session("tier6_ref")
        Session("YOUR_GROUP_NAME") = objRs_session("tier1_name")
        Session("YOUR_COMPANY_NAME") = objRs_session("tier2_name")
        Session("YOUR_LOCATION_NAME") = objRs_session("tier3_name")
        Session("YOUR_DEPARTMENT_NAME") = objRs_session("tier4_name")
        Session("YOUR_PASSWORD") = objRs_session("Acc_pass")
        Session("YOUR_MQUESTION") = objRs_session("Acc_mquestion")
        Session("YOUR_MANSWER") = objRs_session("Acc_manswer")
        Session("YOUR_USERNAME") = objRs_session("Acc_name")
        Session("YOUR_ACCOUNT_ID") = objRs_session("Acc_ref")
        Session("YOUR_LICENCE") = objRs_session("Acc_licence")
        'if Session("YOUR_ACCESS") = "0" or session("YOUR_ACCOUNT_ID") = "DO2tbpqvrRuVShM" then Session("YOUR_LICENCE")  = left(Session("YOUR_LICENCE"),28) & "1" & right(Session("YOUR_LICENCE"),len(Session("YOUR_LICENCE"))-29)
        'response.Write "1234567890123456789012345678<span style='color: Red;'>9</span>012345678901234567890<br />"
       ' response.Write Session("YOUR_LICENCE")
        'response.end

        Session("YOUR_LANGUAGE") = "en-gb"

        Session("YOUR_ACCOUNT_ID_RESELLER") = objRs_session("RN_Acc_Ref")
        Session("YOUR_FIRSTNAME") = objRs_session("Per_fname")
        Session("YOUR_MIDDLENAME") = objRs_session("Per_mname")
        Session("YOUR_SURNAME") = objRs_session("Per_sname")
        Session("YOUR_TITLE") = objRs_session("Per_title")
        Session("YOUR_ADDR_STREET") = objRs_session("Per_street")
        Session("YOUR_ADDR_AREA") = objRs_session("Per_town")
        Session("YOUR_ADDR_COUNTY") = objRs_session("Per_county")
        'Session("YOUR_ADDR_COUNTY_NAME") = getCountyForCode2(objRs_session("Per_county"))
        Session("YOUR_ADDR_COUNTRY") = objRs_session("Per_country")
        Session("YOUR_ADDR_POSTCODE") = objRs_session("Per_pcode")
        Session("YOUR_UNIQUE_ID") = objRs_session("Per_idnumber")
        Session("YOUR_PHONE") = objRs_session("Corp_phone")
        Session("YOUR_FAX") = objRs_session("Corp_fax")
        Session("CORP_CODE") = objRs_session("Corp_code")
        Session("CORP_PIN") = objRs_session("Corp_pin")
        Session("CORP_ACCBOOK") = objRs_session("Corp_Accbook")

        Session("YOUR_JOB_TITLE") = objRs_session("per_job_title")
        Session("YOUR_ACCESS_STATUS") = objRs_session("Acc_status")
        Session("YOUR_IP_ADDRESS") = Request.ServerVariables("REMOTE_ADDR")
        Session("SA_CONTRACTS") = objRs_session("sa_contracts")
        Session("SA_ADMIN") = objRs_session("acc_sadmin")
        var_tm_access = objRs_session("tm_access")
    
        Session("FONT_PREFERENCE") = objRs_session("font_preference")
 
        if session("corp_code") = "005004" or session("corp_code") = "021205" then
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user / pupil"
        else
            session("RIDDOR_PERSTYPE_1") = "employee / trainee / self employed person"
            session("RIDDOR_PERSTYPE_2") = "member of the public / volunteer / service user"
        end if
        
        
        Session("USER_CHECK_SETTINGS") = objRs_session("settings_check")
        Session("ALLOW_SUPPORT_REQUEST") = objRs_session("allow_support_request")
        Session("TM_DATE_DEFAULT") = objRs_session("tm_date_default")
        


        
        '' Build SD elements
        If Len(objRs_session("sd_users_shown")) > 0 Then Session("SD_USERS_SHOWN") = objRs_session("sd_users_shown") & ""
        If Len(objRs_session("sd_users_allowed")) > 0 Then Session("SD_USERS_ALLOWED") = objRs_session("sd_users_allowed") & "" 
        If Len(objRs_session("sd_start_hour")) > 0 Then Session("SD_START_HOUR") = objRs_session("sd_start_hour")
        If Len(objRs_session("sd_end_hour")) > 0 Then Session("SD_END_HOUR") = objRs_session("sd_end_hour")
        

        '********Global preferences************
        session("default_min_date") = ""
       objCommand_session.commandText = "SELECT * FROM " & Application("DBTABLE_HR_PREFERENCE") & " WHERE corp_code = '" & session("corp_code") & "' and (user_ref = '" &  session("YOUR_ACCOUNT_ID") & "' or  LOWER(user_ref) = 'all') and (language='en-gb' or LOWER(language) = 'all')"
        set objRs_session = objCommand_session.execute
        while not objRs_session.eof           
                session(objRs_session("pref_id")) = objRs_session("pref_switch")           
            objRs_session.movenext
        wend
        set objRs_session = nothing


        '********Global preference - priority*********
        ' if pref_task_priority is set and user is a local or global admin, set this preference too
        if session("PREF_TASK_PRIORITY") = "Y" and (session("YOUR_ACCESS") = "0" or session("YOUR_ACCESS") = "1") then
            session("PREF_TASK_PRIORITY_FREEDATE") = "Y"
        end if
        
        if session("default_min_date") <> "i" and session("default_min_date") <> "" then 
            session("default_min_date") = "01/01/" & (2012 - session("default_min_date")) 
        end if
        '*********************************************
        
        '******* Custom Label Changes ********
        if len(session("coshh_label_change")) = 0 then
            session("coshh_label_change") = "COSHH"
        end if

        if len(session("non_conformance_label_change")) = 0 then
            session("non_conformance_label_change") = "Non Conformance"
        end if

        if len(session("violence_label_change")) = 0 then
            session("violence_label_change") = "Violence"
        end if

        '*************************************

        session("user_perms_active") = 0
        if session("global_usr_perm") = "Y" then
            session("user_tm_active") =  var_tm_access 
        else
            session("global_usr_perm") = "N"
        end if


        if session("global_usr_perm") = "Y" and session("YOUR_ACCESS") <> "0"  then  
            
           
            session("user_perms_active") = 1
            '*********BUILD THE TEMP TABLE FOR PERMISSIONS*********
           objCommand_session.commandText = "EXEC Module_HR.dbo.sp_build_perms '" & session("corp_code") & "', '" &  session("YOUR_ACCOUNT_ID") & "', 'GEN'"          
            objCommand_session.execute

            session("allow_create") = true
            session("allow_edit") = true
    
           objCommand_session.commandText = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where perm_create = 1"          
            set objRs_session = objCommand_session.execute

            if  objRs_session.eof then 
                session("allow_create") = False
            end if

           objCommand_session.commandText = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where perm_edit = 1"          
            set objRs_session = objCommand_session.execute

            if  objRs_session.eof then 
                session("allow_edit") = False
            end if
       
            
            session("allow_dse_view") = true
            session("allow_dse_review") = true
            session("allow_dse_admin") = true

           objCommand_session.commandText = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_read = 1"          
            set objRs_session = objCommand_session.execute

            if  objRs_session.eof then 
                session("allow_dse_view") = False
            end if

           objCommand_session.commandText = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_review = 1"          
            set objRs_session = objCommand_session.execute

            if  objRs_session.eof then 
                session("allow_dse_review") = False
            end if

           objCommand_session.commandText = "select top 1 temp_id from TempPermDB.dbo.TEMP_" & session("corp_code") & "_" &  session("YOUR_ACCOUNT_ID") & " where dse_admin = 1"          
            set objRs_session = objCommand_session.execute

            if  objRs_session.eof then 
                session("allow_dse_admin") = False
            end if
       
        end if

            


        
        '********JOB ROLE ASSIGNMENT***********
       objCommand_session.commandText = "SELECT roles.session_role FROM " & Application("DBTABLE_JOB_ROLES") & " AS roles " & _
                                 "INNER JOIN " & Application("DBTABLE_USER_JOB_ROLES") & " AS userroles " & _
                                 "  ON roles.corp_code = userroles.corp_code " & _
                                 "  AND roles.role_ref = userroles.job_role " & _
                                 "WHERE userroles.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND userroles.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                 "  AND roles.session_role IS NOT NULL"
        set objRs_session = objCommand_session.execute
        if not objRs_session.EOF then
            while not objRs_session.EOF
                session(objRs_session("session_role")) = "Y"
                objRs_session.movenext
            wend
        end if
        set objRs_session = nothing

       


        'CORPORATE STRUCTURE NAMING DEFAULTS
        if len(session("CORP_TIER2")) = 0 or isnull(session("CORP_TIER2")) = true then
            session("CORP_TIER2") = "Company"
            session("CORP_TIER2_PLURAL") = "Companies"
        end if
        if len(session("CORP_TIER3")) = 0 or isnull(session("CORP_TIER3")) = true then
            session("CORP_TIER3") = "Location"
            session("CORP_TIER3_PLURAL") = "Locations"
        end if
        if len(session("CORP_TIER4")) = 0 or isnull(session("CORP_TIER4")) = true then
            session("CORP_TIER4") = "Department"
            session("CORP_TIER4_PLURAL") = "Departments"
        end if


		
		'********Accident Status Title*********
		if len(session("ACC_STATUS_COL_TITLE")) = 0 then
			session("ACC_STATUS_COL_TITLE") = "Status"
		end if		

        '********Password Expired?**************
        if len(session("PASSWORD_EXPIRY")) = 0 then
            var_password_expiry = 0 'password never expires (or preference hasn't been set in preferences table)
        else
            var_password_expiry = cint(session("PASSWORD_EXPIRY"))
        end if
        
        if var_password_expiry > 0 then  
           objCommand_session.commandText = "SELECT TOP 1 id FROM " & Application("DBTABLE_USER_PASSWORD") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                     "  AND acc_pass = '" & session("YOUR_PASSWORD") & "' " & _
                                     "  AND (GETDATE() >= gendatetime+" & var_password_expiry & " OR sysacc_pass = 1)" & _
                                     "ORDER BY gendatetime DESC"
            objCommand_session.execute
            set objRs_session = objCommand_session.execute
            if not objRs_session.EOF and (instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 or instr(1,ucase(session("YOUR_USERNAME")),"RTECHNICIAN") = 0) then
                Session("PASSWORD_EXPIRED") = True
            end if
            set objRs_session = nothing
        elseif var_password_expiry = 0 then  
            'check for password resets from the 'forgot password' page
           objCommand_session.commandText = "SELECT TOP 1 id FROM " & Application("DBTABLE_USER_PASSWORD") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                     "  AND acc_pass = '" & session("YOUR_PASSWORD") & "' " & _
                                     "  AND sysacc_pass = 1 " & _
                                     "ORDER BY gendatetime DESC"
            objCommand_session.execute
            set objRs_session = objCommand_session.execute
            if not objRs_session.EOF and (instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 or instr(1,ucase(session("YOUR_USERNAME")),"RTECHNICIAN") = 0) then
                Session("PASSWORD_EXPIRED") = True
            end if
            set objRs_session = nothing
        end if        
        
        '*********Never Logged in before*********
        if ucase(session("CORP_PIN")) <> "BETA" then
           objCommand_session.commandText = "SELECT top 1 id FROM " & Application("DBTABLE_LOG_ACCESS") & " " & _
                                     "WHERE USERNAME = '" & session("YOUR_USERNAME") & "' " & _
                                     "  AND success='1' " & _
                                     "ORDER BY ID DESC"
            set objRs_session = objCommand_session.execute
            if objRs_session.EOF and instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN") = 0 then
                Session("USER_CHECK_SETTINGS") = True
            end if
            set objRs_session = nothing
        end if


        
        '******** logo ,  maintenance , structure locking ************
       objCommand_session.commandText = "select logo, maintenance, lock_structure from " & Application("DBTABLE_HR_SYSTEMS") & " where corp_code='" & session("corp_code") & "'"
        set objRs_session = objCommand_session.execute
        
        if not objRs_session.eof then
            if objRs_session("logo") = "Y" then
                session("corp_logo") = session("corp_code") & ".png"
            else
                session("corp_logo") = "ASSN_default_logo.png"
            end if 

            'maintenance switch
            if objRs_session("maintenance") and left(ucase(session("YOUR_USERNAME")),8) <> "SYSADMIN" then
                Call errCode("0009")
            end if

            'structure locking switch
            if instr(1, session("YOUR_USERNAME"), "rtechnician.") > 0 OR instr(1, session("YOUR_USERNAME"), "radmin.") > 0 OR instr(1, session("YOUR_USERNAME"), "sysadmin.") > 0 then
                session("LOCK_STRUCTURE_CHANGES") = false
            else
                session("LOCK_STRUCTURE_CHANGES") = objRs_session("lock_structure")
            end if
        end if

        
        
        '**********************DSE Preferences********************************
       objCommand_session.commandText = "exec MODULE_Global.dbo.sp_DSE_Training_Pref_GET '" & session("corp_code") & "'"
        set objRs_session = objCommand_session.execute
    
        if objRs_session.eof then
            'enter defaults
            '*******************Please note that these defaults are also set in the dse_settings.asp page in the admin area
            session("dse_training_type") = "2"   
            session("dse_training_years") = "1"            
            session("dse_exam") = True
            session("dse_exam_set") = True
            session("dse_pass_rate") = "70"
            session("dse_no_questions") = "10"
            session("dse_attempts") = "3"
            session("attempt") = 0
            session("dse_on_fail") = "4"
            session("dse_on_final_fail") = "4"
            session("dse_on_fail_email") = ""
            session("dse_certificate") = True
            session("issues_box") = "Contact the Administration Manager to arrange your eyesight test"
        else
            session("dse_training_type") = objRs_session("training_type")
            session("dse_training_years") = objRs_session("training_years")                     
            session("dse_exam") = objRs_session("exam_flag")
            session("dse_exam_set") = objRs_session("exam_flag")
            session("dse_pass_rate") = objRs_session("pass_rate")
            session("dse_no_questions") = objRs_session("no_questions")
            session("dse_attempts") = objRs_session("attempts")
            session("attempt") = 0
            session("dse_on_fail") = objRs_session("on_fail")
            session("dse_on_final_fail") = objRs_session("on_final_fail")
            session("dse_on_fail_email") = objRs_session("on_fail_email")
            session("dse_certificate") = objRs_session("certificate_flag")
            session("issues_box") = objRs_session("issues_box")
        end if
           
                   
        
        '*************************Task Accept Switch******************************
       objCommand_session.commandText = "SELECT pref_switch FROM " & Application("DBTABLE_HR_PREFERENCE") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND pref_id = 'task_accept' " & _
                             "  AND (user_ref = 'all' OR user_ref = '" & session("YOUR_ACCOUNT_ID") & "')"
        set objRs_session = objCommand_session.execute
        if not objRs_session.EOF then
            session("TASK_ACCEPT_ENABLED") = objRs_session("pref_switch")
        else
            session("TASK_ACCEPT_ENABLED") = "N"
        end if
          '*******************End of Task Accept Switch**************************  
                


        
    
		                    Session.Contents.Remove("MODULE_RA")
                            Session.Contents.Remove("MODULE_DSE")
                            Session.Contents.Remove("MODULE_ACCB")
                            Session.Contents.Remove("MODULE_TRA")
                            Session.Contents.Remove("MODULE_MH")
                            Session.Contents.Remove("MODULE_SP")
                            Session.Contents.Remove("MODULE_COSHH")
                            Session.Contents.Remove("MODULE_FS")
                            Session.Contents.Remove("MODULE_SA")
                            Session.Contents.Remove("MODULE_QA")
                            Session.Contents.Remove("MODULE_CMS")
                            Session.Contents.Remove("MODULE_SD")
                            Session.Contents.Remove("MODULE_PTW")
                            Session.Contents.Remove("MODULE_FC")
                            Session.Contents.Remove("MODULE_MS")
                            Session.Contents.Remove("MODULE_INS")
				            Session.Contents.Remove("MODULE_HAZR")
                            Session.Contents.Remove("MODULE_HP")
				            Session.Contents.Remove("MODULE_MAINR")
				            Session.Contents.Remove("MODULE_COMPL")
				            Session.Contents.Remove("MODULE_ASSET")
                            Session.Contents.Remove("MODULE_ASB")
                            Session.Contents.Remove("MODULE_RR")
                            Session.Contents.Remove("MODULE_PUWER")
				            Session.Contents.Remove("MODULE_TRAINING")
				            Session.Contents.Remove("MODULE_LOGBOOK")
				            Session.Contents.Remove("MODULE_CHECKLIST")
                            Session.Contents.Remove("MODULE_SURA")
                            Session.Contents.Remove("MODULE_TOOLS")
                            ' Edition string for EDITION
                            Session.Contents.Remove("EDITION")
                            Session.Contents.Remove("MODULE_RA_RO")
                            Session.Contents.Remove("MODULE_DSE_RO")
                            Session.Contents.Remove("MODULE_ACCB_RO")
                            Session.Contents.Remove("MODULE_MH_RO")
                            Session.Contents.Remove("MODULE_MS_RO")
                            Session.Contents.Remove("MODULE_INS_RO")
                            Session.Contents.Remove("MODULE_COSHH_RO")
                            Session.Contents.Remove("MODULE_FS_RO")
                            Session.Contents.Remove("MODULE_SA_RO")
                            Session.Contents.Remove("MODULE_QA_RO")
                            Session.Contents.Remove("MODULE_PTW_RO")
                            Session.Contents.Remove("MODULE_SELF")

                            Session.Contents.Remove("MODULE_TM")
        
        ' Build licence information for admins
           objCommand_session.commandText = "SELECT corp_licence_type, corp_licence_version, read_only FROM " & Application("DBTABLE_MODULES_DATA") & " WHERE corp_code='" & Session("CORP_CODE") & "' "
            Set objRs_session = objCommand_session.execute

           
            
            If objRs_session.EOF Then
                ' No modules - tell user of error
                IF Len(Session("SA_CONTRACTS")) < 1 Then
                Call errCode("0002")
                End IF
            Else
                While NOT objRs_session.EOF
                    SELECT CASE objRs_session("corp_licence_type") 
                         CASE "RA"
                            Session("MODULE_RA") = objRs_session("corp_licence_version")
                            session("MODULE_RA_RO") = objRs_session("read_only")
                         CASE "MS"
                            Session("MODULE_MS") = objRs_session("corp_licence_version")
                            session("MODULE_MS_RO") = objRs_session("read_only")
                            
                            session("MS_AUTH") = "Y"
                         CASE "MH"
                            Session("MODULE_MH") = objRs_session("corp_licence_version")
                            session("MODULE_MH_RO") = objRs_session("read_only")
                         CASE "DSE"
                            Session("MODULE_DSE") = objRs_session("corp_licence_version")
                            session("MODULE_DSE_RO") = objRs_session("read_only")
                            
                            session("DSE_AUTH") = "Y"
                         CASE "FS"
                            Session("MODULE_FS") = objRs_session("corp_licence_version")
                            session("MODULE_FS_RO") = objRs_session("read_only")
                         CASE "SP"
                            Session("MODULE_SP") = objRs_session("corp_licence_version")
                         CASE "COSHH"
                            Session("MODULE_COSHH") = objRs_session("corp_licence_version")
                            session("MODULE_COSHH_RO") = objRs_session("read_only")
                         CASE "SA"
                            Session("MODULE_SA") = objRs_session("corp_licence_version")
                            session("MODULE_SA_RO") = objRs_session("read_only")
                         CASE "QA"
                            Session("MODULE_QA") = objRs_session("corp_licence_version")
                            session("MODULE_QA_RO") = objRs_session("read_only")
                         CASE "PTW"
							Session("MODULE_PTW") = objRs_session("corp_licence_version")
							session("MODULE_PTW_RO") = objRs_session("read_only")
                         CASE "ACCB"
                            Session("MODULE_ACCB") = objRs_session("corp_licence_version")
                            session("MODULE_ACCB_RO") = objRs_session("read_only")
                         CASE "TRA"
                            Session("MODULE_TRA") = objRs_session("corp_licence_version")
                         CASE "CMS"
                            Session("MODULE_CMS") = objRs_session("corp_licence_version")
                         CASE "SD"
                            Session("MODULE_SD") = objRs_session("corp_licence_version")
                         CASE "FC"
							Session("MODULE_FC") = objRs_session("corp_licence_version")
					     CASE "INS"
							Session("MODULE_INS") = objRs_session("corp_licence_version")
							session("MODULE_INS_RO") = objRs_session("read_only")
						 CASE "SURA"
							Session("MODULE_SURA") = objRs_session("corp_licence_version")
							session("MODULE_SURA_RO") = objRs_session("read_only")
					     CASE "TOOLS"
							Session("MODULE_TOOLS") = objRs_session("corp_licence_version")
					     CASE "REPS"
							Session("MODULE_REPS") = objRs_session("corp_licence_version")
						 CASE "HAZR"
							Session("MODULE_HAZR") = objRs_session("corp_licence_version")
    			         CASE "HP"
							Session("MODULE_HP") = objRs_session("corp_licence_version")
						 CASE "MAINR"
							Session("MODULE_MAINR") = objRs_session("corp_licence_version")
					     CASE "COMPL"
					        Session("MODULE_COMPL") = objRs_session("corp_licence_version")
					     CASE "ASSET"
					        Session("MODULE_ASSET") = objRs_session("corp_licence_version")
                         CASE "ASB"
                            Session("MODULE_ASB") = objRs_session("corp_licence_version")
                         CASE "RR"
                            Session("MODULE_RR") = objRs_session("corp_licence_version")
                         CASE "PUWER"
                            Session("MODULE_PUWER") = objRs_session("corp_licence_version")
					     CASE "LB"
					        Session("MODULE_LOGBOOK") = objRs_session("corp_licence_version")
					     CASE "CL"
					        Session("MODULE_CHECKLIST") = objRs_session("corp_licence_version")
					     CASE "PORTAL"
					        Session("MODULE_PORTAL") = objRs_session("corp_licence_version")
					     CASE "TMX"
					        Session("MODULE_TRAINING") = objRs_session("corp_licence_version")
					     CASE "SME"
					        Session("EDITION") = "SME"
					     CASE "RN"
					        Session("EDITION") = "RN"
					     CASE "DEMO"
					        Session("EDITION") = "DEMO"
                         CASE "TM"
                            Session("MODULE_TM") = objRs_session("corp_licence_version")
                         CASE "SELF"
                            Session("MODULE_SELF") =  objRs("corp_licence_version")
                    End Select
                objRs_session.MoveNext
                Wend
            End IF
 


	Objconn_session.close
	SET objCommand_session.ActiveConnection = NOTHING
	SET objcommand_session = NOTHING
	SET objconn_session = NOTHING
	SET objrs_session = NOTHING

End Sub



'if session("YOUR_ACCOUNT_ID") = "LbdHyMoiNefoEGe" then
'    session.timeout = 1
'    response.write session.timeout
'end if    

    
    
%>