<%


  sub showHistoryBox2(var_hist_reference, var_hist_reference_1, var_hist_reference_2)
             response.Write "<table width='100%' cellpadding='3' class='section2' >"  & _        
                              "<tr>" & _
                               "<th><textarea class='textarea' rows='6'  style='width: 99%' readonly>"
            
                Objcommand.commandtext =	"SELECT History.*, hrusers.per_fname, hrusers.per_sname, hrusers.corp_bd, tier4.struct_name FROM " & Application("DBTABLE_GLOBAL_FPRINT") & " as History " & _
							                "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " as hrusers " & _
                                            "ON History.corp_code = hrusers.corp_code " & _
                                            "AND History.HisBy = hrusers.acc_ref " & _                 
                                            " LEFT OUTER JOIN " &   Application("DBTABLE_STRUCT_TIER4")& "  AS tier4 " & _
                                            " ON hrusers.corp_code = tier4.corp_code AND hrusers.corp_bd = tier4.tier4_ref " & _                   
                                            "WHERE (History.HisRelationref = '" & var_hist_reference & "' or History.tier2ref = '" & var_hist_reference_1 & "') AND History.tier1ref = '" & var_hist_reference_2 & "' AND History.corp_code='" & Session("corp_code") & "' ORDER BY History.ID DESC"
                 
                SET Objrs = Objcommand.execute
                
                if objrs.eof then        
                     Response.write "No history could be found for this assessment"   
                else
	                while not Objrs.eof
                    var_hist_type = Objrs("HisType")
                    var_hist_DateTime = Objrs("HisDateTime")
                    var_hist_by = Objrs("per_fname") & " " & Objrs("per_sname") & " - ( " & Objrs("struct_name") & " )"
                            
                    ' error correction
                    if (var_hist_type = " ") or (len(var_hist_type) < 1) or (IsNull(var_hist_type) = true) then var_hist_type = "Unknown" end if
                    if (var_hist_Datetime = " ") or (len(var_hist_DateTime) < 1) or (IsNull(var_hist_DateTime) = true) then var_hist_DateTime = "Unknown" end if
                    if (var_hist_by = " ") or (len(var_hist_by) < 1) or (IsNull(var_hist_by) = true) then var_hist_by = "Unknown" end if
                    
                    Response.write  var_hist_DateTime & " | " & Ucase(var_hist_type) & " by " & var_hist_by &  vbCrLf
                            

                    Objrs.movenext
                    wend
                end if
                
               response.Write "</textarea></th>" & _ 
                                "</tr>"          & _         
                              "</table>"
        end sub
              

SUB HAZ_MENU(HazValue,ObjNAME)

	DIM HazCategory(40),HazARRAY

	HazCategory(0) = "Access / Egress"
	HazCategory(1) = "Adverse Weather"
	HazCategory(2) = "Animal"
	HazCategory(3) = "Biological"
	HazCategory(4) = "Collapse of Structure"
	HazCategory(5) = "Compressed Air"
	HazCategory(6) = "Confined Spaces"
	HazCategory(7) = "Drowning / Asphyxiation"
	HazCategory(8) = "D.S.E / V.D.U Usage"
	HazCategory(9) = "Electricity"
	HazCategory(10) = "Energy Release"
	HazCategory(11) = "Environmental"
	HazCategory(12) = "Ergonomics"
	HazCategory(13) = "Excavation"
	HazCategory(14) = "Explosion"
	HazCategory(15) = "Fall of Object from Height"
	HazCategory(16) = "Fall of Persons from  Height"
	HazCategory(17) = "Fire Safety"
	HazCategory(18) = "Food Hygiene"
	HazCategory(19) = "Gas"
	HazCategory(20) = "Hazardous Substance"
	HazCategory(21) = "Housekeeping"
	HazCategory(22) = "Human Factors"
	HazCategory(23) = "Lifting Equipment"
	HazCategory(24) = "Lighting"
	HazCategory(25) = "Machinery"
	HazCategory(26) = "Manual Handling"
	HazCategory(27) = "Noise"
	HazCategory(28) = "Pressure"
	HazCategory(29) = "Radiation"
	HazCategory(30) = "Sharp Objects"
	HazCategory(31) = "Slip / Trip / Fall"
	HazCategory(32) = "Storage"
	HazCategory(33) = "Stress"
	HazCategory(34) = "Temperature Extremes"
	HazCategory(35) = "Vehicles"
	HazCategory(36) = "Ventilation"
	HazCategory(37) = "Vibration"
	HazCategory(38) = "Violence to staff"
	HazCategory(39) = "Work Equipment"
	HazCategory(40) = "Other (See: Hazard Details)"

	Response.Write("<SELECT Name='" & ObjNAME & "' class='select'>")
	
	If HazValue = "S" Then
	    Response.Write("<option value='any'>any hazard type</option>")
	Else
		Response.Write("<option value='any'>Select a Hazard ...</option>")
	End if
	
    For HazARRAY = 0 to 40
        If HazValue = HazCategory(HazARRAY) Then
            Response.Write("<option value='" & HazCategory(HazARRAY) & "' style='background-color: #FFF000' selected='selected'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        Else
            Response.Write("<option value='" & HazCategory(HazARRAY) & "'>" & HazCategory(HazARRAY) & "</option>" & vbCrLf)
        End if
    Next
		
	Response.Write("</select>")
	
END SUB

	DIM PAFFECTED(22), paffARRAY

	PAFFECTED(0) = "Cleaners"
	PAFFECTED(1) = "Contractors"
	PAFFECTED(2) = "Employees"
	PAFFECTED(3) = "Engineers"
	PAFFECTED(4) = "Lone Workers"
	PAFFECTED(5) = "Machine Operators"
	PAFFECTED(6) = "Maintenance Staff"
	PAFFECTED(7) = "Members of the Public"
	PAFFECTED(8) = "Office Staff"
	PAFFECTED(9) = "Outdoor Workers"
	PAFFECTED(10) = "Patient"
	PAFFECTED(11) = "Pregnant Women"
	PAFFECTED(12) = "Production"
	PAFFECTED(13) = "Staff"
	PAFFECTED(14) = "Staff with Disabilities"
    PAFFECTED(15) = "Students"
	PAFFECTED(16) = "Students with Disabilities"
	PAFFECTED(17) = "Resident / Tenant"
	PAFFECTED(18) = "Trainees / Young Persons"
	PAFFECTED(19) = "Visitors"
	PAFFECTED(20) = "Volunteers"
	PAFFECTED(21) = "Warehouse Operators"
	PAFFECTED(22) = "Other"

SUB PER_AFFECTED(ObjNAME)



	Response.Write("<select name='" & ObjNAME & "' class='select'>")
	Response.Write(vbCrLf & "  <option>Select ...</option>")
	
    For paffARRAY = 0 to 18
        Response.Write(vbCrLf & "  <option value='" & PAFFECTED(paffARRAY) & "'>" & PAFFECTED(paffARRAY) & "</option>")
    Next
		
	Response.Write(vbCrLf & "</select>")

END SUB



Sub autogenWeapons(myWeapon, showDisabled)
    if isnull(myWeapon) then
        myWeapon = ""
    end if
    
    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='vi_weapon' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsVI = objcommand.execute

        if not objrsVI.eof then 
            while not objrsVI.eof
                if objrsVI("opt_disabled") = false or showDisabled = true or cstr(myWeapon) = cstr(objrsVI("opt_id")) then
                    if cstr(myWeapon) =  cstr(objrsVI("opt_id")) then
                        response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "' selected='selected' >" &  objrsVI("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "'>" &  objrsVI("opt_value") & "</option>"
                    end if
                    
                    if objrsVI("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsVI("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsVISUB = objcommand.execute
                        
                        if not objrsVISUB.eof then 
                            while not objrsVISUB.eof
                                if objrsVISUB("opt_disabled") = false or showDisabled = true or cstr(myWeapon) = cstr(objrsVISUB("opt_id")) then
                                    if cstr(myWeapon) =  cstr(objrsVISUB("opt_id")) then
                                        response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "' selected>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "'>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsVISUB.movenext
                            wend
                        end if
                    
                    end if
                end if

                objrsVI.movenext
            wend
        end if
    end if

end sub


Sub autogenSchoolYear(mySchoolYear, showDisabled)
    if isnull(mySchoolYear) then
        mySchoolYear = ""
    end if
    
    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_sch_year' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_id asc"
    'response.Write objcommand.commandtext
    'response.end    
    set objrsVI = objcommand.execute

        if not objrsVI.eof then 
            while not objrsVI.eof
                if objrsVI("opt_disabled") = false or showDisabled = true or cstr(mySchoolYear) = cstr(objrsVI("opt_id")) then
                    if cstr(mySchoolYear) =  cstr(objrsVI("opt_id")) then
                        response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "' selected='selected' >" &  objrsVI("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsVI("opt_id") & "' title='" & objrsVI("opt_description") & "'>" &  objrsVI("opt_value") & "</option>"
                    end if
                
                    if objrsVI("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsVI("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsVISUB = objcommand.execute
                        
                        if not objrsVISUB.eof then 
                            while not objrsVISUB.eof
                                if objrsVISUB("opt_disabled") = false or showDisabled = true or cstr(mySchoolYear) = cstr(objrsVISUB("opt_id")) then
                                    if cstr(mySchoolYear) =  cstr(objrsVISUB("opt_id")) then
                                        response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "' selected>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsVISUB("opt_id") & "' title='" & objrsVISUB("opt_description") & "'>--" & objrsVI("opt_value") & " (" &  objrsVISUB("opt_value") & ")</option>"
                                    end if
                                end if                               
                                objrsVISUB.movenext
                            wend
                        end if
                    
                    end if
                end if

                objrsVI.movenext
            wend
        end if
    end if

end sub


Sub autogenRelationship(myRelationship, showDisabled)
    if isnull(myRelationship) then
        myRelationship = ""
    end if

    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='vi_relationship' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsREL = objcommand.execute

        if not objrsREL.eof then 
            while not objrsREL.eof
                if objrsREL("opt_disabled") = false or showDisabled = true or cstr(myRelationship) = cstr(objrsREL("opt_id")) then
                    if cstr(myRelationship) = cstr(objrsREL("opt_id")) then
                        response.Write  "<option value='" & objrsREL("opt_id") & "' title='" & objrsREL("opt_description") & "' selected='selected' >" &  objrsREL("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsREL("opt_id") & "' title='" & objrsREL("opt_description") & "'>" &  objrsREL("opt_value") & "</option>"
                    end if
                
                    if objrsREL("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsREL("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsRELSUB = objcommand.execute
                        
                        if not objrsRELSUB.eof then 
                            while not objrsRELSUB.eof
                                if objrsRELSUB("opt_disabled") = false or showDisabled = true or cstr(myRelationship) = cstr(objrsRELSUB("opt_id")) then
                                    if cstr(myRelationship) =  cstr(objrsRELSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsRELSUB("opt_id") & "' title='" & objrsRELSUB("opt_description") & "' selected>--" & objrsREL("opt_value") & " (" &  objrsRELSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsRELSUB("opt_id") & "' title='" & objrsRELSUB("opt_description") & "'>--" & objrsREL("opt_value") & " (" &  objrsRELSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsRELSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsREL.movenext
            wend
        end if
    end if

end sub


Sub autogenViolenceType(myType, showDisabled)
    if isnull(myType) then
        myType = ""
    end if
    
    if  Session("MODULE_ACCB") ="2" then	

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='vi_type' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsVT = objcommand.execute

        if not objrsVT.eof then 
            while not objrsVT.eof
                if objrsVT("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsVT("opt_id")) then
                    if cstr(myType) =  cstr(objrsVT("opt_id")) then
                        response.Write  "<option value='" & objrsVT("opt_id") & "' title='" & objrsVT("opt_description") & "' selected='selected' >" &  objrsVT("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsVT("opt_id") & "' title='" & objrsVT("opt_description") & "'>" &  objrsVT("opt_value") & "</option>"
                    end if
                
                    if objrsVT("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsVT("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsVTSUB = objcommand.execute
                        
                        if not objrsVTSUB.eof then 
                            while not objrsVTSUB.eof
                                if objrsVTSUB("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsVTSUB("opt_id")) then
                                    if cstr(myType) =  cstr(objrsVTSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsVTSUB("opt_id") & "' title='" & objrsVTSUB("opt_description") & "' selected>--" & objrsVT("opt_value") & " (" &  objrsVTSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsVTSUB("opt_id") & "' title='" & objrsVTSUB("opt_description") & "'>--" & objrsVT("opt_value") & " (" &  objrsVTSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsVTSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsVT.movenext
            wend
        end if
    end if

end sub


Sub autogenBodyPart(myBodyPart, showDisabled) 
   
if  Session("MODULE_ACCB") ="2" then	

     objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
            while not objrsBP.eof
                if objrsBP("opt_disabled") = false or showDisabled = true or cstr(myBodyPart) = cstr(objrsBP("opt_id")) then
                    if cstr(myBodyPart) =  cstr(objrsBP("opt_id")) then
                        response.Write  "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "' selected='selected' >" &  objrsBP("opt_value") & "</option>" & vbcrlf
                    else
                        response.Write  "<option value='" & objrsBP("opt_id") & "' title='" & objrsBP("opt_description") & "'>" &  objrsBP("opt_value") & "</option>" & vbcrlf
                    end if
                
                    if objrsBP("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_disabled, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsBP("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsBPSUB = objcommand.execute
                        
                        if not objrsBPSUB.eof then 
                            while not objrsBPSUB.eof
                                if objrsBPSUB("opt_disabled") = false or showDisabled = true or cstr(myBodyPart) = cstr(objrsBPSUB("opt_id")) then
                                    if cstr(myBodyPart) =  cstr(objrsBPSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "' selected>--" & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>" & vbcrlf
                                    else
                                        response.Write  "<option value='" & objrsBPSUB("opt_id") & "' title='" & objrsBPSUB("opt_description") & "'>--" & objrsBP("opt_value") & " (" &  objrsBPSUB("opt_value") & ")</option>" & vbcrlf
                                    end if
                                end if

                                objrsBPSUB.movenext
                            wend
                        end if
                    
                    end if
                end if

                objrsBP.movenext
            wend
        end if
    
    
else
    Dim dictBodyPart
    Set dictBodyPart = CreateObject("Scripting.Dictionary")
    dictBodyPart.Add "0", "Ankle"
    dictBodyPart.Add "1", "Arm"
    dictBodyPart.Add "2", "Back (Lower)"
    dictBodyPart.Add "3", "Back (Upper)"
    dictBodyPart.Add "25", "Back - Unspecific"
    dictBodyPart.Add "4", "Buttocks"
    dictBodyPart.Add "5", "Chest"
    dictBodyPart.Add "6", "Eye"
    dictBodyPart.Add "7", "Face"
    dictBodyPart.Add "8", "Finger(s)"
    dictBodyPart.Add "9", "Foot"
    dictBodyPart.Add "10", "Groin"
    dictBodyPart.Add "11", "Hand injury"
    dictBodyPart.Add "12", "Head"
    dictBodyPart.Add "13", "Hip"
    dictBodyPart.Add "14", "Internal injuries"
    dictBodyPart.Add "15", "Knee"
    dictBodyPart.Add "24", "Leg - Unspecific"
    dictBodyPart.Add "16", "Multiple injuries"
    dictBodyPart.Add "17", "Neck"
    dictBodyPart.Add "18", "Shin"
    dictBodyPart.Add "19", "Shoulder"
    dictBodyPart.Add "20", "Stomach"
    dictBodyPart.Add "21", "Thigh"
    dictBodyPart.Add "22", "Toe(s)"
    dictBodyPart.Add "23", "Wrist"
    dictBodyPart.Add "na", "[ Not Specified ]" 
    
    
    myBodyPart = "" & myBodyPart
   For Each key In dictBodyPart
        Response.Write "<option value='" & key & "'"
        If myBodyPart = key Then 
            Response.Write " selected='selected'" 
        End if
        Response.Write ">" & dictBodyPart.item(key) & "</option>" & vbCrLf
    Next
end if


  
End Sub

Function getBodyPartForCode(myBodyPart)
    if  Session("MODULE_ACCB") ="2" then
       objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("parent_value")) > 0 then
                getBodyPartForCode = objrsBP("parent_value") & " > " & objrsBP("opt_value")
           else
                getBodyPartForCode = objrsBP("opt_value")
           end if      
        else
           getBodyPartForCode =  "Body part could not be found"
        end if
    else
    
      Dim dictBodyPart
            Set dictBodyPart = CreateObject("Scripting.Dictionary")
            dictBodyPart.Add "0", "Ankle"
            dictBodyPart.Add "1", "Arm"
            dictBodyPart.Add "2", "Back (Lower)"
            dictBodyPart.Add "3", "Back (Upper)"
            dictBodyPart.Add "25", "Back - Unspecific"
            dictBodyPart.Add "4", "Buttocks"
            dictBodyPart.Add "5", "Chest"
            dictBodyPart.Add "6", "Eye"
            dictBodyPart.Add "7", "Face"
            dictBodyPart.Add "8", "Finger(s)"
            dictBodyPart.Add "9", "Foot"
            dictBodyPart.Add "10", "Groin"
            dictBodyPart.Add "11", "Hand injury"
            dictBodyPart.Add "12", "Head"
            dictBodyPart.Add "13", "Hip"
            dictBodyPart.Add "14", "Internal injuries"
            dictBodyPart.Add "15", "Knee"
            dictBodyPart.Add "24", "Leg - Unspecific"
            dictBodyPart.Add "16", "Multiple injuries"
            dictBodyPart.Add "17", "Neck"
            dictBodyPart.Add "18", "Shin"
            dictBodyPart.Add "19", "Shoulder"
            dictBodyPart.Add "20", "Stomach"
            dictBodyPart.Add "21", "Thigh"
            dictBodyPart.Add "22", "Toe(s)"
            dictBodyPart.Add "23", "Wrist"
            dictBodyPart.Add "na", "[ Not Specified ]" 
    
        myBodyPart = "" & myBodyPart
        getBodyPartForCode = dictBodyPart.Item(myBodyPart)
    end if
   
End function


Function getBodyPartColourForCode(myBodyPart)
    if Session("MODULE_ACCB") = "2" then
        objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='bp_affected' and a.opt_id='" & clng(myBodyPart) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsBP = objcommand.execute

        if not objrsBP.eof then 
           if len(objrsBP("opt_colour")) > 0 then
                getBodyPartColourForCode = objrsBP("opt_colour")
           else
                getBodyPartColourForCode = ""
           end if      
        else
           getBodyPartColourForCode = ""
        end if
    else
        getBodyPartColourForCode = ""
    end if
   
End function



Sub autogenEmpStatus(opt_value, field_name, field_disabled, showDisabled)

	if isnull(opt_value) = true or opt_value = "" or opt_value = "na" then
		opt_value = 0
    end if
	
	objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
							 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
							 "  AND related_to = 'emp_status' " & _
							 "  AND parent_id = 0 " & _
                             "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
	set objRsREL = objCommand.execute
	if not objRsREL.EOF then
		response.write "<select class='select' name='" & field_name & "' " & field_disabled & "><option value='na'>Please Select</option>"

		while not objRsREL.EOF
			
			if objRsREL("child_present") = 1 then
				response.write "<optgroup label='" & objRsREL("opt_value") & "'>"

				objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
										 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
										 "  AND related_to = 'emp_status' " & _
										 "  AND parent_id = " & objRsREL("opt_id") & " " & _
                                         "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
				set objRsREL2 = objCommand.execute
				if not objRsREL2.EOF then
					while not objRsREL2.EOF
                        if objrsREL2("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL2("opt_id")) then
						    if cint(opt_value) = cint(objRsREL2("opt_id")) then
							    response.write "<option value='" & objRsREL2("opt_id") & "' selected>" & objRsREL2("opt_value") & "</option>"
						    else
							    response.write "<option value='" & objRsREL2("opt_id") & "'>" & objRsREL2("opt_value") & "</option>"
						    end if
                        end if
						objRsREL2.movenext
					wend
				end if
				set objRsREL2 = nothing
				response.write "</optgroup>"
			else
                if objrsREL("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL("opt_id")) then
				    if cint(opt_value) = cint(objRsREL("opt_id")) then
					    response.write "<option value='" & objRsREL("opt_id") & "' selected>" & objRsREL("opt_value") & "</option>"
				    else
					    response.write "<option value='" & objRsREL("opt_id") & "'>" & objRsREL("opt_value") & "</option>"
				    end if
			    end if
            end if

			objRsREL.movenext
		wend

		response.write "</select>"
	end if
	set objRsREL = nothing

End Sub

function getEmpStatusForCode(myType)

    if isnull(myType) = true or myType = "" or myType = "na" then
        myType = 0
    end if

	objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
							 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
							 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
							 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='emp_status' and a.opt_id='" & clng(myType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"	
	set objRsEMP = objCommand.execute
	if not objRsEMP.EOF then
		objRsEMP.movefirst
		getEmpStatusForCode = objRsEMP("parent_value") & " - " & objRsEMP("opt_value")
	else
		getEmpStatusForCode = "<span class='info'>Not an Employee</span>"
	end if
	set objRsEMP = nothing
	
end function





Sub autogenEmpStatus1(opt_value, field_name, field_disabled, filter_list_item, showDisabled)

	if isnull(opt_value) = true or opt_value = "" or opt_value = "na" then
		opt_value = 0
    end if
	
	objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
							 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
							 "  AND related_to = 'emp_status' " & _
							 "  AND parent_id = 0 " & _
                             "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
	if len(filter_list_item) > 0 then
		objCommand.commandText = objCommand.commandText & "  AND opt_value = (SELECT opt_target FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
																		     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
																		     "AND related_to = 'acc_status' " & _
																		     "AND opt_id = " & filter_list_item & " " & _
                                                                             "AND opt_language='" & session("YOUR_LANGUAGE") & "')"
	end if
	
	set objRsREL = objCommand.execute
	if not objRsREL.EOF then
		response.write "<select class='select' name='" & field_name & "' " & field_disabled & "><option value='na'>Please Select</option>"

		while not objRsREL.EOF
			
			if objRsREL("child_present") = 1 then
				response.write "<optgroup label='" & objRsREL("opt_value") & "'>"

				objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
										 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
										 "  AND related_to = 'emp_status' " & _
										 "  AND parent_id = " & objRsREL("opt_id") & " " & _
                                         "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
				set objRsREL2 = objCommand.execute
				if not objRsREL2.EOF then
					while not objRsREL2.EOF
                        if objrsREL2("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL2("opt_id")) then
						    if cint(opt_value) = cint(objRsREL2("opt_id")) then
							    response.write "<option value='" & objRsREL2("opt_id") & "' selected>" & objRsREL2("opt_value") & "</option>"
						    else
							    response.write "<option value='" & objRsREL2("opt_id") & "'>" & objRsREL2("opt_value") & "</option>"
						    end if
                        end if
						objRsREL2.movenext
					wend
				end if
				set objRsREL2 = nothing
				response.write "</optgroup>"
			else
                if objrsREL("opt_disabled") = false or showDisabled = true or cstr(opt_value) = cstr(objrsREL("opt_id")) then
				    if cint(opt_value) = cint(objRsREL("opt_id")) then
					    response.write "<option value='" & objRsREL("opt_id") & "' selected>" & objRsREL("opt_value") & "</option>"
				    else
					    response.write "<option value='" & objRsREL("opt_id") & "'>" & objRsREL("opt_value") & "</option>"
				    end if
			    end if
            end if

			objRsREL.movenext
		wend

		response.write "</select>"
	end if
	set objRsREL = nothing

End Sub


' -------- Type of injury ---------

Sub autogenInjType(myType, showDisabled)

if  Session("MODULE_ACCB") ="2" then
     
      objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsIT = objcommand.execute

        if not objrsIT.eof then 
            while not objrsIT.eof
                if objrsIT("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsIT("opt_id")) or lcase(cstr(myType)) = lcase(cstr(objrsIT("opt_value"))) then
                    if cstr(myType) =  cstr(objrsIT("opt_id")) or lcase(cstr(myType)) =  lcase(cstr(objrsIT("opt_value"))) then
                        response.Write  "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "' selected='selected' >" &  objrsIT("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsIT("opt_id") & "' title='" & objrsIT("opt_description") & "'>" &  objrsIT("opt_value") & "</option>"
                    end if
                                
                    if objrsIT("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_type' and parent_id= '" & objrsIT("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsITSUB = objcommand.execute
                        
                        if not objrsITSUB.eof then 
                            while not objrsITSUB.eof
                                if objrsITSUB("opt_disabled") = false or showDisabled = true or cstr(myType) = cstr(objrsITSUB("opt_id")) or lcase(cstr(myType)) = lcase(cstr(objrsITSUB("opt_value"))) then
                                    if cstr(myType) =  cstr(objrsITSUB("opt_id")) or lcase(cstr(myType)) =  lcase(cstr(objrsITSUB("opt_value"))) then
                                        response.Write  "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "' selected>--" & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsITSUB("opt_id") & "' title='" & objrsITSUB("opt_description") & "'>--" & objrsIT("opt_value") & " (" &  objrsITSUB("opt_value") & ")</option>"
                                    end if
                                end if                                                                   
                                objrsITSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsIT.movenext
            wend
        end if
else
        Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "17", "Multiple injuries"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"

            myType = "" & myType
            For Each key In dictInjType
                Response.Write "<option value='" & key & "'"
                If myType = key Then 
                    Response.Write " selected='selected'" 
                End if
                Response.Write ">" & dictInjType.item(key) & "</option>" & vbCrLf
            Next
    end if        
End Sub

Function getInjTypeForCode(myType)
 
    if Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("parent_value")) > 0 then
                getInjTypeForCode = objrsIT("parent_value") & " > " & objrsIT("opt_value")
           else
                getInjTypeForCode = objrsIT("opt_value")
           end if                   
        else
           getInjTypeForCode =  "Injury type could not be found"
        end if
    else
         Dim dictInjType
        Set dictInjType = CreateObject("Scripting.Dictionary")

        dictInjType.Add "0", "Abrasion"
        dictInjType.Add "1", "Asphyxiation"
        dictInjType.Add "2", "Amputation"
        dictInjType.Add "3", "Bite / Sting"
        dictInjType.Add "4", "Bruise / Bump"
        dictInjType.Add "5", "Burn / Scald"
        dictInjType.Add "6", "Crush"
        dictInjType.Add "7", "Cut / laceration"
        dictInjType.Add "8", "Dislocation"
        dictInjType.Add "9", "Electrical shock"
        dictInjType.Add "10", "Fracture"
        dictInjType.Add "11", "Internal injury"
        dictInjType.Add "12", "Loss of consciousness"
        dictInjType.Add "13", "Loss of sight"
        dictInjType.Add "14", "Other injury"
        dictInjType.Add "15", "Puncture"
        dictInjType.Add "16", "Strain / Sprain"
        dictInjType.Add "na", "[ Not Specified ]"
    
         myType = "" & myType
        getInjTypeForCode = dictInjType.Item(myType)
    end if
End function


Function getInjTypeColourForCode(myType)
   
     if  Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                  " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_type' and a.opt_id='" & clng(myType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIT = objcommand.execute
                   
        if not objrsIT.eof then 
           if len(objrsIT("opt_colour")) > 0 then
                getInjTypeColourForCode = objrsIT("opt_colour")
           else
                getInjTypeColourForCode = ""
           end if                   
        else
           getInjTypeColourForCode = ""
        end if
    else

    end if
End function

' --------- Apparent Cause of Injury ----------

Sub autogenApparentCause(myCause, showDisabled)

    if  Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsAC = objcommand.execute

        if not objrsAC.eof then 
            while not objrsAC.eof
                if objrsAC("opt_disabled") = false or showDisabled = true or cstr(myCause) = cstr(objrsAC("opt_id")) or lcase(cstr(myCause)) = lcase(cstr(objrsAC("opt_value"))) then            
                    if cstr(myCause) =  cstr(objrsAC("opt_id")) or lcase(cstr(myCause)) =  lcase(cstr(objrsAC("opt_value"))) then
                        response.Write  "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "' selected>" &  objrsAC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsAC("opt_id") & "' title='" & objrsAC("opt_description") & "'>" &  objrsAC("opt_value") & "</option>"
                    end if
                
                    if objrsAC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='inj_cause' and parent_id= '" & objrsAC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsACSUB = objcommand.execute

                        if not objrsACSUB.eof then 
                            while not objrsACSUB.eof
                                if objrsACSUB("opt_disabled") = false or showDisabled = true or cstr(myCause) = cstr(objrsACSUB("opt_id")) or lcase(cstr(myCause)) = lcase(cstr(objrsACSUB("opt_value"))) then
                                    if cstr(myCause) =  cstr(objrsACSUB("opt_id")) or lcase(cstr(myCause)) =  lcase(cstr(objrsACSUB("opt_value")))  then
                                        response.Write  "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "' selected>--" & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsACSUB("opt_id") & "' title='" & objrsACSUB("opt_description") & "'>--" & objrsAC("opt_value") & " (" &  objrsACSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsACSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsAC.movenext
            wend
        end if
    else

        Dim dictApparentCause
        Set dictApparentCause = CreateObject("Scripting.Dictionary")

        dictApparentCause.Add "0", "Animal Related"
        dictApparentCause.Add "1", "Collision"
        dictApparentCause.Add "3", "Contact with electricity"
        dictApparentCause.Add "4", "Contact with machinery"
        dictApparentCause.Add "2", "Contact with sharp object"
        dictApparentCause.Add "15", "Exposure to explosion"
        dictApparentCause.Add "6", "Exposure to fire"
        dictApparentCause.Add "5", "Exposure to hazardous substance"
        dictApparentCause.Add "7", "Exposure to temperature extremes"
        dictApparentCause.Add "16", "Fainting / Fit"
        dictApparentCause.Add "8", "Fall from height"
        dictApparentCause.Add "9", "Manual Handling"
        dictApparentCause.Add "10", "Repetitive actions"
        dictApparentCause.Add "17", "Self inflicted"
        dictApparentCause.Add "11", "Slip, trip or fall"
        dictApparentCause.Add "12", "Struck by object"
        dictApparentCause.Add "13", "Struck by vehicle"
        dictApparentCause.Add "18", "Trapped by object"
        dictApparentCause.Add "14", "Violence"
        dictApparentCause.Add "na", "[ Not Specified ]"

            myCause = "" & myCause
            For Each key In dictApparentCause
                Response.Write "<option value='" & key & "'"
                If myCause = key Then 
                    Response.Write " selected='selected'" 
                End if
                Response.Write ">" & dictApparentCause.item(key) & "</option>" & vbCrLf
            Next
    end if
End Sub
	
Function getApparentCauseForCode(myCause)


     if Session("MODULE_ACCB") ="2" then
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsAC = objcommand.execute
                   
        if not objrsAC.eof then 
           if len(objrsAC("parent_value")) > 0 then
                getApparentCauseForCode = objrsAC("parent_value") & " > " & objrsAC("opt_value")
           else
                getApparentCauseForCode = objrsAC("opt_value")
           end if                   
        else
           getApparentCauseForCode =  "Apparent cause could not be found"
        end if
    else
        
        Dim dictApparentCause
        Set dictApparentCause = CreateObject("Scripting.Dictionary")

        dictApparentCause.Add "0", "Animal Related"
        dictApparentCause.Add "1", "Collision"
        dictApparentCause.Add "3", "Contact with electricity"
        dictApparentCause.Add "4", "Contact with machinery"
        dictApparentCause.Add "2", "Contact with sharp object"
        dictApparentCause.Add "15", "Exposure to explosion"
        dictApparentCause.Add "6", "Exposure to fire"
        dictApparentCause.Add "5", "Exposure to hazardous substance"
        dictApparentCause.Add "7", "Exposure to temperature extremes"
        dictApparentCause.Add "16", "Fainting / Fit"
        dictApparentCause.Add "8", "Fall from height"
        dictApparentCause.Add "9", "Manual Handling"
        dictApparentCause.Add "10", "Repetitive actions"
        dictApparentCause.Add "17", "Self inflicted"
        dictApparentCause.Add "11", "Slip, trip or fall"
        dictApparentCause.Add "12", "Struck by object"
        dictApparentCause.Add "13", "Struck by vehicle"
        dictApparentCause.Add "18", "Trapped by object"
        dictApparentCause.Add "14", "Violence"
        dictApparentCause.Add "na", "[ Not Specified ]"
    
        myCause = "" & myCause
        getApparentCauseForCode = dictApparentCause.Item(myCause)
    end if
End function

Function getApparentCauseColourForCode(myCause)

    if Session("MODULE_ACCB") ="2" then
        objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inj_cause' and a.opt_id='" & clng(myCause) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsAC = objcommand.execute
                   
        if not objrsAC.eof then 
           if len(objrsAC("opt_colour")) > 0 then
                getApparentCauseColourForCode = objrsAC("opt_colour")
           else
                getApparentCauseColourForCode = ""
           end if                   
        else
           getApparentCauseForCode =  ""
        end if
    else

    end if
End function




sub autogenInjStatus(myStatus, showDisabled)

        if isnull(myStatus) = true then myStatus = ""

        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsIS = objcommand.execute

        if not objrsIS.eof then 
            while not objrsIS.eof
                if isnull(myStatus) = true then
                    response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
                else
                    if objrsIS("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objrsIS("opt_id")) or lcase(cstr(myStatus)) = lcase(cstr(objrsIS("opt_value"))) then  
                        if cstr(myStatus) =  cstr(objrsIS("opt_id"))  or lcase(cstr(myStatus)) =  lcase(cstr(objrsIS("opt_value"))) then
                            response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "' selected>" &  objrsIS("opt_value") & "</option>"
                        else
                            response.Write  "<option value='" & objrsIS("opt_id") & "' title='" & objrsIS("opt_description") & "'>" &  objrsIS("opt_value") & "</option>"
                        end if
                    end if
                end if
                
                if objrsIS("child_present") = "1" then
                    'There are sub options available
                    objcommand.commandtext = "select opt_id, opt_value, opt_disabled, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='acc_status' and parent_id= '" & objrsIS("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                    set objrsISSUB = objcommand.execute

                    if not objrsISSUB.eof then 
                        while not objrsISSUB.eof
                            if objrsISSUB("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objrsISSUB("opt_id")) or lcase(cstr(myStatus)) = lcase(cstr(objrsISSUB("opt_value"))) then  
                                if cstr(myStatus) =  cstr(objrsISSUB("opt_id")) or lcase(cstr(myStatus)) =  lcase(cstr(objrsISSUB("opt_value")))  then
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "' selected>--" & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                else
                                    response.Write  "<option value='" & objrsISSUB("opt_id") & "' title='" & objrsISSUB("opt_description") & "'>--" & objrsIS("opt_value") & " (" &  objrsISSUB("opt_value") & ")</option>"
                                end if
                            end if
                                                                   
                            objrsISSUB.movenext
                        wend
                    end if
                end if
                
                objrsIS.movenext
            wend
        end if

end sub


function getAccidentStatusForCode(myStatus)
  if len(myStatus) > 0 then
     objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("parent_value")) > 0 then
                getAccidentStatusForCode = objrsIS("parent_value") & " > " & objrsIS("opt_value")
           else
                getAccidentStatusForCode = objrsIS("opt_value")
           end if                   
        else
           getAccidentStatusForCode =  "Accident status could not be found"
        end if
    else
           getAccidentStatusForCode =  "Accident status could not be found"
    end if
end function

function getAccidentStatusColourForCode(myStatus)
  objcommand.commandtext = "select a.opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                           " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='acc_status' and a.opt_id='" & clng(myStatus) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("opt_colour")) > 0 then
                getAccidentStatusColourForCode = objrsIS("opt_colour")
           else
                getAccidentStatusColourForCode = ""
           end if                   
        else
           getAccidentStatusColourForCode = ""
        end if
end function





Sub autogenSecType(mySecType, showDisabled) 
   

     objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='sec_type' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsOpt = objcommand.execute

        if not objrsOpt.eof then 
            while not objrsOpt.eof
                if objrsOpt("opt_disabled") = false or showDisabled = true or cstr(mySecType) = cstr(objrsOpt("opt_id")) then  
                    if cstr(mySecType) =  cstr(objrsOpt("opt_id")) then
                        response.Write  "<option value='" & objrsOpt("opt_id") & "' title='" & objrsOpt("opt_description") & "' selected='selected' >" &  objrsOpt("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsOpt("opt_id") & "' title='" & objrsOpt("opt_description") & "'>" &  objrsOpt("opt_value") & "</option>"
                    end if
                
                    if objrsOpt("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='bp_affected' and parent_id= '" & objrsBP("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsOptSUB = objcommand.execute
                        
                        if not objrsOptSUB.eof then 
                            while not objrsOptSUB.eof
                                if objrsOptSUB("opt_disabled") = false or showDisabled = true or cstr(mySecType) = cstr(objrsOptSub("opt_id")) then  
                                    if cstr(mySecType) =  cstr(objrsOptSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsOptSUB("opt_id") & "' title='" & objrsOptSUB("opt_description") & "' selected>--" & objrsBP("opt_value") & " (" &  objrsOptSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsOptSUB("opt_id") & "' title='" & objrsOptSUB("opt_description") & "'>--" & objrsOptSUB("opt_value") & " (" &  objrsOptSUB("opt_value") & ")</option>"
                                    end if
                                end if                                                                   
                                objrsOptSUB.movenext
                            wend
                        end if
                    
                    end if
                end if
                objrsOpt.movenext
            wend
        end if
  
  
End Sub

Function getSecTypeForCode(mySecType)
    
    if isnull(mySecType) then
        getSecTypeForCode =  "Security Category could not be found"
    else
       objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='sec_type' and a.opt_id='" & clng(mySecType) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
        set objrsOpt = objcommand.execute

        if not objrsOpt.eof then 
           if len(objrsOpt("parent_value")) > 0 then
                getSecTypeForCode = objrsOpt("parent_value") & " > " & objrsOpt("opt_value")
           else
                getSecTypeForCode = objrsOpt("opt_value")
           end if      
        else
           getSecTypeForCode =  "Security Category could not be found"
        end if
   end if
   
End function


sub autogenDefCategories(myStatus, field_name, field_status, var_javascript, showDisabled)

    response.write "<select name='" & field_name & "' id='" & field_name & "' class='select' " & field_status & " " & var_javascript & ">" & _
                   "<option value=''>Please select...</option>"

    if isnull(myStatus) or len(myStatus) = 0 then mystatus = "0"

    objCommand.commandText = "SELECT opt_id, opt_value, opt_description, opt_disabled FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                "  AND related_to = 'inc_type_defined' " & _
                                "  AND parent_id = '0' " & _
                                "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " & _
                                "ORDER BY opt_id"
    set objOpts = objCommand.execute
    if not objOpts.EOF then
        while not objOpts.EOF
            if objOpts("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objOpts("opt_id")) then  
                if cstr(myStatus) = cstr(objOpts("opt_id")) then
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "' selected>" & objOpts("opt_value") & "</option>"
                else
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "'>" & objOpts("opt_value") & "</option>"
                end if
            end if
            objOpts.movenext
        wend
    end if
    set objOpts = nothing

    response.write "</select>"

end sub

function getDefCategoriesForCode(myStatus)
  if len(myStatus) > 0 then
     objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                              " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                              " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                              " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='inc_type_defined' and a.opt_id='" & clng(myStatus) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsIS = objcommand.execute
                   
        if not objrsIS.eof then 
           if len(objrsIS("parent_value")) > 0 then
                getDefCategoriesForCode = objrsIS("parent_value") & " > " & objrsIS("opt_value")
           else
                getDefCategoriesForCode = objrsIS("opt_value")
           end if                   
        else
           getDefCategoriesForCode =  "Accident status could not be found"
        end if
    else
           getDefCategoriesForCode =  "Accident status could not be found"
    end if
end function


sub autogenDefSubCategories(myStatus, field_name, field_status, parent_id, showDisabled)

    response.write "<select name='" & field_name & "' id='" & field_name & "' class='select' " & field_status & ">" & _
                   "<option value=''>Please select...</option>"

    if isnull(myStatus) or len(myStatus) = 0 then mystatus = "0"
    if isnull(parent_id) or len(parent_id) = 0 or parent_id = "0" then parent_id = "-1"

    objCommand.commandText = "SELECT opt_id, opt_value, opt_description, opt_disabled FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                "  AND related_to = 'inc_type_defined' " & _
                                "  AND parent_id = '" & parent_id & "' " & _
                                "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " & _
                                "ORDER BY opt_id"
    set objOpts = objCommand.execute
    if not objOpts.EOF then
        while not objOpts.EOF
            if objOpts("opt_disabled") = false or showDisabled = true or cstr(myStatus) = cstr(objOpts("opt_id")) then  
                if cstr(myStatus) = cstr(objOpts("opt_id")) then
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "' selected>" & objOpts("opt_value") & "</option>"
                else
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "'>" & objOpts("opt_value") & "</option>"
                end if
            end if
            objOpts.movenext
        wend
    end if
    set objOpts = nothing

    response.write "</select>"

end sub


'User type variables
' 0 All users
' 1 active users

sub AutoGenUserList(var_name, var_id, var_value, var_user_type, var_disabled, var_auto_submit)

         if ucase(var_auto_submit) = "Y" then
            var_auto_submit = " onchange='submit()' " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
        select case var_user_type
            case 1
                sql_variables = " and acc_status <> 'D'"
            case else
                sql_variables = ""
        end select

       Response.Write  "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_auto_submit & ">" & _
                    "<option value='0'>any user</option>"

	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_Bl,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code='" & Session("corp_code")& "' AND Corp_BG='" & Session("YOUR_GROUP") & "' AND Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY Per_sname ASC"
	SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & Ucase(objRs("Per_sname")) & ", " & objRs("Per_fname") & " (" & objRs("Corp_BD") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"
end sub

sub AutoGenCompanyList(var_name, var_id, var_value, var_disabled, var_auto_submit)
        if ucase(var_auto_submit) = "Y" then
           var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
         response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' > any " & session("CORP_TIER2") & "</option>"
		                        	
                            
	                            objCommand.Commandtext = "SELECT DISTINCT BU FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & _
	                                    " WHERE BG='" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & _
	                                    "' AND BU IS NOT NULL ORDER BY BU ASC"
	                            Set objRs = objCommand.Execute	
                            			
	                            while NOT objRs.EOF			
	                                if len(objRs("BU")) > 30 then
	                                    comp_len = "..."
	                                else
	                                    comp_len = ""
	                                end if
		                            if var_value = objRs("BU") then
			                            Response.Write 	"<option value='" & objRs("BU") & "' selected >" & left(objRs("BU"),30) & comp_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BU") & "'>" & left(objRs("BU"),30)  & comp_len &  "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                            
                                
	 response.Write "</select>"
        

end sub

sub AutoGenLocationList(var_name, var_id, var_value, var_disabled, var_auto_submit, var_comp_value)

        if ucase(var_auto_submit) = "Y" then
            var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if

    response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' >any " & session("CORP_TIER3") & "</option>"
		                           
                                    	
                            
	                             objCommand.Commandtext = "SELECT DISTINCT BL FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE BG = '" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & "' AND BU='" & var_comp_value & "' AND BL IS NOT NULL ORDER BY BL ASC"
	                                Set objRs = objCommand.Execute
                            			
	                            while NOT objRs.EOF			
		                            if len(objRs("BL")) > 30 then
	                                    loc_len = "..."
	                                else
	                                    loc_len = ""
	                                end if
		                        
		                            if var_value = objRs("BL") then
			                            Response.Write 	"<option value='" & objRs("BL") & "' selected >" & left(objRs("BL"),30) & loc_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BL") & "'>" & left(objRs("BL"),30) & loc_len & "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                                                           
	 response.Write "</select>"
end sub

sub AutoGenDepartmentList(var_name, var_id, var_value, var_disabled, var_auto_submit, var_comp_value, var_loc_value)
        if ucase(var_auto_submit) = "Y" then
                var_auto_submit = " onchange=""submit()"" " 
        else
            var_auto_submit = "" 
        end if
        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
          response.Write "<select id='" & var_id & "' class='select' name='" & var_name & "' " & var_disabled & " " & var_auto_submit & " ><option value='0' >any " & session("CORP_TIER4") & "</option>"
		                           
                                    	
                            
	                             objCommand.Commandtext = "SELECT DISTINCT BD FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE BG = '" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & "' AND BU='" & var_comp_value & "' AND BL='" & var_loc_value & "' AND BD IS NOT NULL ORDER BY BD ASC"
	                                Set objRs = objCommand.Execute
                            			
	                            while NOT objRs.EOF			
		                            if len(objRs("BD")) > 30 then
	                                    dep_len = "..."
	                                else
	                                    dep_len = ""
	                                end if
		                        
		                            if var_value = objRs("BD") then
			                            Response.Write 	"<option value='" & objRs("BD") & "' selected >" & left(objRs("BD"),30) & dep_len & "</option>" 
		                            else									
			                            Response.Write 	"<option value='" & objRs("BD") & "'>" & left(objRs("BD"),30) & dep_len & "</option>" 
		                            End if
		                            objRs.movenext
	                            wend		
	                                                           
	 response.Write "</select>"


end sub


sub AutoGenUserList2(var_name, var_id, var_value, var_user_type, var_disabled, var_jscript, var_default_val, var_default_text, var_default2_val, var_default2_text)

        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
        select case var_user_type
            case 1
                sql_variables = " and acc_status <> 'D'"
            case else
                sql_variables = ""
        end select

       Response.Write  "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_jscript & ">" 
              if var_value = var_default_val  then              
                    response.Write "<option value='" & var_default_val & "' selected='selected'>" & var_default_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default_val & "'>" & var_default_text & "</option>" 
              end if
              
              if var_value = var_default2_val  then              
                    response.Write "<option value='" & var_default2_val & "' selected='selected'>" & var_default2_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default2_val & "'>" & var_default2_text & "</option>" 
              end if
                    

	objCommand.Commandtext = "SELECT hr.Acc_Ref,hr.Per_fname,hr.Per_sname, tier4.struct_name FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _
	                         " LEFT OUTER JOIN " &  Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                             " ON hr.corp_code = tier4.corp_code AND cast(hr.corp_bd as varchar(25)) = cast(tier4.tier4_ref as varchar(25)) " & _
	                         " WHERE hr.corp_code='" & Session("corp_code")& "' AND hr.Corp_BG='" & Session("YOUR_GROUP") & "' AND hr.Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY hr.Per_sname ASC"

    SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & ucase(objRs("Per_sname")) & " " & objRs("Per_fname") & " (" & objRs("struct_name") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"
end sub

sub AutoGenUserList3(var_name, var_id, var_value, var_user_type, var_disabled, var_jscript, var_default_val, var_default_text, var_default2_val, var_default2_text, var_default3_val, var_default3_text)

        
        if ucase(var_disabled) = "Y" then
            var_disabled = " disabled " 
        else
            var_disabled = "" 
        end if
        
        select case var_user_type
            case 1
                sql_variables = " and acc_status <> 'D'"
            case else
                sql_variables = ""
        end select

       Response.Write  "<select size='1' name='" & var_name & "' class='select' id='" & var_id & "' " & var_disabled & " " & var_jscript & ">" 
              if var_value = var_default_val  then              
                    response.Write "<option value='" & var_default_val & "' selected='selected'>" & var_default_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default_val & "'>" & var_default_text & "</option>" 
              end if
              
              if var_value = var_default2_val  then              
                    response.Write "<option value='" & var_default2_val & "' selected='selected'>" & var_default2_text & "</option>" 
              else      
                    response.Write "<option value='" & var_default2_val & "'>" & var_default2_text & "</option>" 
              end if
              
              if var_value = var_default3_val  then
                    response.Write "<option value='" & var_default3_val & "' selected='selected'>" & var_default3_text & "</option>"
              else
                    response.Write "<option value='" & var_default3_val & "'>" & var_default3_text & "</option>"
              end if
                    

	objCommand.Commandtext = "SELECT hr.Acc_Ref,hr.Per_fname,hr.Per_sname, tier4.struct_name FROM " & Application("DBTABLE_USER_DATA") & " as hr " & _
	                         " LEFT OUTER JOIN " &  Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                             " ON hr.corp_code = tier4.corp_code AND CAST(hr.corp_bd AS nvarchar(50)) = CAST(tier4.tier4_ref AS nvarchar(50)) " & _
	                         " WHERE hr.corp_code='" & Session("corp_code")& "' AND hr.Corp_BG='" & Session("YOUR_GROUP") & "' AND hr.Acc_level NOT LIKE '5'  " & sql_variables & " ORDER BY hr.Per_sname ASC"
	SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        
        If objRs("Acc_ref") = var_value Then  Response.Write " selected "  End if
        
        Response.Write ">" & ucase(objRs("Per_sname")) & " " & objRs("Per_fname") & " (" & objRs("struct_name") & ")</option>"
        objRs.MoveNext
	Wend
    response.Write "</select>"
end sub

sub nmiss_class(val_sub_value,val_sub_name, showDisabled)
    Response.Write("<SELECT Name='" & val_sub_name & "' class='select'>")
    Response.Write("<option value='any'>Please select a classification ...</option>")
    
    if  Session("MODULE_ACCB") ="2" then	
        if isnull(val_sub_value) then 
            val_sub_value = ""
        end if
        
         objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMC = objcommand.execute

        if not objrsNMC.eof then 
            while not objrsNMC.eof
                if objrsNMC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMC("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "' selected>" &  objrsNMC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "'>" &  objrsNMC("opt_value") & "</option>"
                    end if
                
                    if objrsNMC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '" & objrsNMC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMCSUB = objcommand.execute

                        if not objrsNMCSUB.eof then 
                            while not objrsNMCSUB.eof
                                if objrsNMCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMCSUB("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "' selected>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "'>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsNMCSUB.movenext
                            wend
                        end if
                    end if
                    
                 end if
                
                objrsNMC.movenext
            wend
        end if
        
      
    else
       
        DIM CLASS_Category(39),classARRAY

	    CLASS_Category(0) = "Access / Egress"
	    CLASS_Category(1) = "Adverse Weather"
	    CLASS_Category(2) = "Animal"
	    CLASS_Category(3) = "Biological"
	    CLASS_Category(4) = "Collapse of Structure"
	    CLASS_Category(5) = "Compressed Air"
	    CLASS_Category(6) = "Confined Spaces"
	    CLASS_Category(7) = "Drowning / Asphyxiation"
	    CLASS_Category(8) = "D.S.E / V.D.U Usage"
	    CLASS_Category(9) = "Electricity"
	    CLASS_Category(10) = "Energy Release"
	    CLASS_Category(11) = "Environmental"
	    CLASS_Category(12) = "Ergonomics"
	    CLASS_Category(13) = "Excavation"
	    CLASS_Category(14) = "Explosion"
	    CLASS_Category(15) = "Fall of Object from Height"
	    CLASS_Category(16) = "Fall of Persons from  Height"
	    CLASS_Category(17) = "Fire Safety"
	    CLASS_Category(18) = "Food Hygiene"
	    CLASS_Category(19) = "Gas"
	    CLASS_Category(20) = "Hazardous Substance"
	    CLASS_Category(21) = "Housekeeping"
	    CLASS_Category(22) = "Human Factors"
	    CLASS_Category(23) = "Lifting Equipment"
	    CLASS_Category(24) = "Lighting"
	    CLASS_Category(25) = "Machinery"
	    CLASS_Category(26) = "Manual Handling"
	    CLASS_Category(27) = "Noise"
	    CLASS_Category(28) = "Pressure"
	    CLASS_Category(29) = "Radiation"
	    CLASS_Category(30) = "Sharp Objects"
	    CLASS_Category(31) = "Slip / Trip / Fall"
	    CLASS_Category(32) = "Storage"
	    CLASS_Category(33) = "Stress"
	    CLASS_Category(34) = "Temperature Extremes"
	    CLASS_Category(35) = "Vehicles"
	    CLASS_Category(36) = "Ventilation"
	    CLASS_Category(37) = "Vibration"
	    CLASS_Category(38) = "Violence"
	    CLASS_Category(39) = "Work Equipment"

    	    	
        For classARRAY = 0 to 39
            if len(val_sub_value) > 0 then
                If clng(val_sub_value) = clng(classARRAY) Then
                    Response.Write("<option value='" & classARRAY & "' selected='selected'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                Else
                    Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                End if
            Else
                Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            End if
        Next
        
    end  if		
	Response.Write("</select>")
end sub 


sub nmiss_class2(val_sub_value, showDisabled)
   
    if  Session("MODULE_ACCB") ="2" then	
        if isnull(val_sub_value) then 
            val_sub_value = ""
        end if
        
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMC = objcommand.execute

        if not objrsNMC.eof then 
            while not objrsNMC.eof
                if objrsNMC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMC("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "' selected>" &  objrsNMC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "'>" &  objrsNMC("opt_value") & "</option>"
                    end if
                
                    if objrsNMC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '" & objrsNMC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMCSUB = objcommand.execute

                        if not objrsNMCSUB.eof then 
                            while not objrsNMCSUB.eof
                                if objrsNMCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMCSUB("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "' selected>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "'>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    end if
                                end if                                                                   
                                objrsNMCSUB.movenext
                            wend
                        end if
                        
                    end if    
                 end if
                
                objrsNMC.movenext
            wend
        end if
        
    else
       
        DIM CLASS_Category(39),classARRAY

	    CLASS_Category(0) = "Access / Egress"
	    CLASS_Category(1) = "Adverse Weather"
	    CLASS_Category(2) = "Animal"
	    CLASS_Category(3) = "Biological"
	    CLASS_Category(4) = "Collapse of Structure"
	    CLASS_Category(5) = "Compressed Air"
	    CLASS_Category(6) = "Confined Spaces"
	    CLASS_Category(7) = "Drowning / Asphyxiation"
	    CLASS_Category(8) = "D.S.E / V.D.U Usage"
	    CLASS_Category(9) = "Electricity"
	    CLASS_Category(10) = "Energy Release"
	    CLASS_Category(11) = "Environmental"
	    CLASS_Category(12) = "Ergonomics"
	    CLASS_Category(13) = "Excavation"
	    CLASS_Category(14) = "Explosion"
	    CLASS_Category(15) = "Fall of Object from Height"
	    CLASS_Category(16) = "Fall of Persons from  Height"
	    CLASS_Category(17) = "Fire Safety"
	    CLASS_Category(18) = "Food Hygiene"
	    CLASS_Category(19) = "Gas"
	    CLASS_Category(20) = "Hazardous Substance"
	    CLASS_Category(21) = "Housekeeping"
	    CLASS_Category(22) = "Human Factors"
	    CLASS_Category(23) = "Lifting Equipment"
	    CLASS_Category(24) = "Lighting"
	    CLASS_Category(25) = "Machinery"
	    CLASS_Category(26) = "Manual Handling"
	    CLASS_Category(27) = "Noise"
	    CLASS_Category(28) = "Pressure"
	    CLASS_Category(29) = "Radiation"
	    CLASS_Category(30) = "Sharp Objects"
	    CLASS_Category(31) = "Slip / Trip / Fall"
	    CLASS_Category(32) = "Storage"
	    CLASS_Category(33) = "Stress"
	    CLASS_Category(34) = "Temperature Extremes"
	    CLASS_Category(35) = "Vehicles"
	    CLASS_Category(36) = "Ventilation"
	    CLASS_Category(37) = "Vibration"
	    CLASS_Category(38) = "Violence"
	    CLASS_Category(39) = "Work Equipment"

    	    	
        For classARRAY = 0 to 39
            if len(val_sub_value) > 0 then
                If clng(val_sub_value) = clng(classARRAY) Then
                    Response.Write("<option value='" & classARRAY & "' selected='selected'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                Else
                    Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                End if
            Else
                Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            End if
        Next
        
    end  if		
	
end sub 

sub nmiss_class3(val_sub_value,val_sub_name, val_parent_category, val_sub_additional, showDisabled)
    
    if val_sub_additional = true then
        response.Write "<SELECT Name='" & val_sub_name & "' class='select' onchange='checkTrigger(this)'>"
    else
        Response.Write("<SELECT Name='" & val_sub_name & "' class='select'>")
    end if
    Response.Write("<option value='any'>Please select a classification ...</option>")
    
    if  Session("MODULE_ACCB") ="2" then	
        if isnull(val_sub_value) then 
            val_sub_value = ""
        end if
        
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' "
        if len(val_parent_category) > 0 and var_parent_category <> 0 and isnull(var_parent_category) = false then
            objCommand.commandText = objCommand.commandText & " AND opt_target = '" & val_parent_category & "' "
        end if
        objcommand.commandtext = objcommand.commandText & " order by opt_value asc"
        set objrsNMC = objcommand.execute

        if not objrsNMC.eof then 
            while not objrsNMC.eof
                if objrsNMC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMC("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "' selected>" &  objrsNMC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "'>" &  objrsNMC("opt_value") & "</option>"
                    end if
                
                    if objrsNMC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '" & objrsNMC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMCSUB = objcommand.execute

                        if not objrsNMCSUB.eof then 
                            while not objrsNMCSUB.eof
                                if objrsNMCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMCSUB("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "' selected>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "'>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    end if
                                end if

                                objrsNMCSUB.movenext
                            wend
                        end if
                        
                    end if 
                 end if
                
                objrsNMC.movenext
            wend
        end if
        
      
    else
       
        DIM CLASS_Category(39),classARRAY

	    CLASS_Category(0) = "Access / Egress"
	    CLASS_Category(1) = "Adverse Weather"
	    CLASS_Category(2) = "Animal"
	    CLASS_Category(3) = "Biological"
	    CLASS_Category(4) = "Collapse of Structure"
	    CLASS_Category(5) = "Compressed Air"
	    CLASS_Category(6) = "Confined Spaces"
	    CLASS_Category(7) = "Drowning / Asphyxiation"
	    CLASS_Category(8) = "D.S.E / V.D.U Usage"
	    CLASS_Category(9) = "Electricity"
	    CLASS_Category(10) = "Energy Release"
	    CLASS_Category(11) = "Environmental"
	    CLASS_Category(12) = "Ergonomics"
	    CLASS_Category(13) = "Excavation"
	    CLASS_Category(14) = "Explosion"
	    CLASS_Category(15) = "Fall of Object from Height"
	    CLASS_Category(16) = "Fall of Persons from  Height"
	    CLASS_Category(17) = "Fire Safety"
	    CLASS_Category(18) = "Food Hygiene"
	    CLASS_Category(19) = "Gas"
	    CLASS_Category(20) = "Hazardous Substance"
	    CLASS_Category(21) = "Housekeeping"
	    CLASS_Category(22) = "Human Factors"
	    CLASS_Category(23) = "Lifting Equipment"
	    CLASS_Category(24) = "Lighting"
	    CLASS_Category(25) = "Machinery"
	    CLASS_Category(26) = "Manual Handling"
	    CLASS_Category(27) = "Noise"
	    CLASS_Category(28) = "Pressure"
	    CLASS_Category(29) = "Radiation"
	    CLASS_Category(30) = "Sharp Objects"
	    CLASS_Category(31) = "Slip / Trip / Fall"
	    CLASS_Category(32) = "Storage"
	    CLASS_Category(33) = "Stress"
	    CLASS_Category(34) = "Temperature Extremes"
	    CLASS_Category(35) = "Vehicles"
	    CLASS_Category(36) = "Ventilation"
	    CLASS_Category(37) = "Vibration"
	    CLASS_Category(38) = "Violence"
	    CLASS_Category(39) = "Work Equipment"

    	    	
        For classARRAY = 0 to 39
            if len(val_sub_value) > 0 then
                If clng(val_sub_value) = clng(classARRAY) Then
                    Response.Write("<option value='" & classARRAY & "' selected='selected'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                Else
                    Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                End if
            Else
                Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            End if
        Next
        
    end  if		
	Response.Write("</select>")
end sub 


Function getNearMissClassForCode(myNearMissClass)
    if  Session("MODULE_ACCB") ="2" then
        
        objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='nmiss_class' and a.opt_id='" & clng(myNearMissClass) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsNMC = objcommand.execute
                   
        if not objrsNMC.eof then 
           if len(objrsNMC("parent_value")) > 0 then
                getNearMissClassForCode = objrsNMC("parent_value") & " > " & objrsNMC("opt_value")
           else
                getNearMissClassForCode = objrsNMC("opt_value")
           end if                   
        else
           getNearMissClassForCode =  "Near miss classification could not be found"
        end if
       
    else
    
      DIM CLASS_Category(39),classARRAY

	    CLASS_Category(0) = "Access / Egress"
	    CLASS_Category(1) = "Adverse Weather"
	    CLASS_Category(2) = "Animal"
	    CLASS_Category(3) = "Biological"
	    CLASS_Category(4) = "Collapse of Structure"
	    CLASS_Category(5) = "Compressed Air"
	    CLASS_Category(6) = "Confined Spaces"
	    CLASS_Category(7) = "Drowning / Asphyxiation"
	    CLASS_Category(8) = "D.S.E / V.D.U Usage"
	    CLASS_Category(9) = "Electricity"
	    CLASS_Category(10) = "Energy Release"
	    CLASS_Category(11) = "Environmental"
	    CLASS_Category(12) = "Ergonomics"
	    CLASS_Category(13) = "Excavation"
	    CLASS_Category(14) = "Explosion"
	    CLASS_Category(15) = "Fall of Object from Height"
	    CLASS_Category(16) = "Fall of Persons from  Height"
	    CLASS_Category(17) = "Fire Safety"
	    CLASS_Category(18) = "Food Hygiene"
	    CLASS_Category(19) = "Gas"
	    CLASS_Category(20) = "Hazardous Substance"
	    CLASS_Category(21) = "Housekeeping"
	    CLASS_Category(22) = "Human Factors"
	    CLASS_Category(23) = "Lifting Equipment"
	    CLASS_Category(24) = "Lighting"
	    CLASS_Category(25) = "Machinery"
	    CLASS_Category(26) = "Manual Handling"
	    CLASS_Category(27) = "Noise"
	    CLASS_Category(28) = "Pressure"
	    CLASS_Category(29) = "Radiation"
	    CLASS_Category(30) = "Sharp Objects"
	    CLASS_Category(31) = "Slip / Trip / Fall"
	    CLASS_Category(32) = "Storage"
	    CLASS_Category(33) = "Stress"
	    CLASS_Category(34) = "Temperature Extremes"
	    CLASS_Category(35) = "Vehicles"
	    CLASS_Category(36) = "Ventilation"
	    CLASS_Category(37) = "Vibration"
	    CLASS_Category(38) = "Violence"
	    CLASS_Category(39) = "Work Equipment"
    
        myNearMissClass = "" & myNearMissClass
        getNearMissClassForCode = CLASS_Category(myNearMissClass)
    end if
   
End function


sub nmiss_category(val_sub_value,val_sub_name, val_sub_additional, showDisabled)
    

    if len(val_sub_additional) > 0 then
        Response.Write("<SELECT Name='" & val_sub_name & "' class='select' onchange='" & val_sub_additional & "'>")
    else
        Response.Write("<SELECT Name='" & val_sub_name & "' class='select'>")
    end if

    Response.Write("<option value='any'>Please select the potential incident type...</option>")
    
    if  Session("MODULE_ACCB") ="2" then	
        if isnull(val_sub_value) then 
            val_sub_value = ""
        end if
        
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_category' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMC = objcommand.execute

        if not objrsNMC.eof then 
            while not objrsNMC.eof
                if objrsNMC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMC("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "' selected>" &  objrsNMC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "'>" &  objrsNMC("opt_value") & "</option>"
                    end if
                
                    if objrsNMC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_category' and parent_id= '" & objrsNMC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMCSUB = objcommand.execute

                        if not objrsNMCSUB.eof then 
                            while not objrsNMCSUB.eof
                                if objrsNMCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMCSUB("opt_id")) then           
                                    if cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "' selected>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "'>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                    end if
                                end if                               
                                objrsNMCSUB.movenext
                            wend
                        end if

                    end if
                 end if
                
                objrsNMC.movenext
            wend
        end if
    end if		
	Response.Write("</select>")
end sub 

Function getNearMissCategoryForCode(myNearMissCategory)
 
    if len(myNearMissCategory) > 0 then
        objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='nmiss_category' and a.opt_id='" & clng(myNearMissCategory) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsNMC = objcommand.execute
                   
        if not objrsNMC.eof then 
           if len(objrsNMC("parent_value")) > 0 then
                getNearMissCategoryForCode = objrsNMC("parent_value") & " > " & objrsNMC("opt_value")
           else
                getNearMissCategoryForCode = objrsNMC("opt_value")
           end if                   
        else
           getNearMissCategoryForCode =  "Near miss category could not be found"
        end if
    else
        getNearMissCategoryForCode =  "Near miss category could not be found"
    end if

end function


Sub nmiss_inj_type(var_sub_name,val_sub_value, showDisabled)
 response.write "<select name='" & var_sub_name & "'  class='select'>" & _
                "<option value='0'>Please select an injury type</option>"
    
    if  Session("MODULE_ACCB") ="2" then
      
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_injType' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMINJ = objcommand.execute

        if not objrsNMINJ.eof then 
            while not objrsNMINJ.eof
                if objrsNMINJ("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMINJ("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMINJ("opt_id")) then
                        response.Write  "<option value='" & objrsNMINJ("opt_id") & "' title='" & objrsNMINJ("opt_description") & "' selected>" &  objrsNMINJ("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMINJ("opt_id") & "' title='" & objrsNMINJ("opt_description") & "'>" &  objrsNMINJ("opt_value") & "</option>"
                    end if
                
                    if objrsNMINJ("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_injType' and parent_id= '" & objrsNMINJ("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMINJSUB = objcommand.execute

                        if not objrsNMINJSUB.eof then 
                            while not objrsNMINJSUB.eof
                                if objrsNMINJ("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMINJ("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMINJSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMINJSUB("opt_id") & "' title='" & objrsNMINJSUB("opt_description") & "' selected>--" & objrsNMINJ("opt_value") & " (" &  objrsNMINJSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMINJSUB("opt_id") & "' title='" & objrsNMINJSUB("opt_description") & "'>--" & objrsNMINJ("opt_value") & " (" &  objrsNMINJSUB("opt_value") & ")</option>"
                                    end if
                                end if                            
                                objrsNMINJSUB.movenext
                            wend
                        end if
                        
                    end if
                 end if
                
                objrsNMINJ.movenext
            wend
        end if
      
       
    else
                            
            Dim dictNMissInj
            Set dictNMissInj = CreateObject("Scripting.Dictionary")
            	
            dictNMissInj.Add "1", "Death"
            dictNMissInj.Add "4", "Dismemberment"
            dictNMissInj.Add "2", "Fracture"
            dictNMissInj.Add "3", "Cut"
            dictNMissInj.Add "5", "Burn"
            dictNMissInj.Add "6", "Bruise"
            dictNMissInj.Add "7", "Physcological"
            dictNMissInj.Add "8", "Illness / Disease"
            dictNMissInj.Add "9", "Multiple Injuries"

           
            var_sub_value = "" & var_sub_value
            For Each key In dictNMissInj
                Response.Write "<option value='" & key & "'"
                If var_sub_value = key Then 
                    Response.Write " selected='selected'" 
                End if
                Response.Write ">" & dictNMissInj.item(key) & "</option>" & vbCrLf
            Next
            
    end if
        
    response.Write "</select>"
End Sub


Sub nmiss_inj_type2(val_sub_value, showDisabled)
    
    if  Session("MODULE_ACCB") ="2" then
      
      objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_injType' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMINJ = objcommand.execute

        if not objrsNMINJ.eof then 
            while not objrsNMINJ.eof
                if objrsNMINJ("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMINJ("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMINJ("opt_id")) then
                        response.Write  "<option value='" & objrsNMINJ("opt_id") & "' title='" & objrsNMINJ("opt_description") & "' selected>" &  objrsNMINJ("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMINJ("opt_id") & "' title='" & objrsNMINJ("opt_description") & "'>" &  objrsNMINJ("opt_value") & "</option>"
                    end if
                
                    if objrsNMINJ("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_injType' and parent_id= '" & objrsNMINJ("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMINJSUB = objcommand.execute

                        if not objrsNMINJSUB.eof then 
                            while not objrsNMINJSUB.eof
                                if objrsNMINJSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMINJSUB("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMINJSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMINJSUB("opt_id") & "' title='" & objrsNMINJSUB("opt_description") & "' selected>--" & objrsNMINJ("opt_value") & " (" &  objrsNMINJSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMINJSUB("opt_id") & "' title='" & objrsNMINJSUB("opt_description") & "'>--" & objrsNMINJ("opt_value") & " (" &  objrsNMINJSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsNMINJSUB.movenext
                            wend
                        end if
                 
                    end if  
                 end if
                
                objrsNMINJ.movenext
            wend
        end if
   
    else
                            
            Dim dictNMissInj
            Set dictNMissInj = CreateObject("Scripting.Dictionary")
            	
            dictNMissInj.Add "1", "Death"
            dictNMissInj.Add "4", "Dismemberment"
            dictNMissInj.Add "2", "Fracture"
            dictNMissInj.Add "3", "Cut"
            dictNMissInj.Add "5", "Burn"
            dictNMissInj.Add "6", "Bruise"
            dictNMissInj.Add "7", "Physcological"
            dictNMissInj.Add "8", "Illness / Disease"
            dictNMissInj.Add "9", "Multiple Injuries"

           
            var_sub_value = "" & var_sub_value
            For Each key In dictNMissInj
                Response.Write "<option value='" & key & "'"
                If var_sub_value = key Then 
                    Response.Write " selected='selected'" 
                End if
                Response.Write ">" & dictNMissInj.item(key) & "</option>" & vbCrLf
            Next
            
    end if
        
End Sub
	
Function get_nmiss_inj_type(myNMinjtype)

  if  Session("MODULE_ACCB") ="2" then
             
         objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='nmiss_injType' and a.opt_id='" & clng(myNMinjtype) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsNMIT = objcommand.execute
                   
        if not objrsNMIT.eof then 
           if len(objrsNMIT("parent_value")) > 0 then
                get_nmiss_inj_type = objrsNMIT("parent_value") & " > " & objrsNMIT("opt_value")
           else
                get_nmiss_inj_type = objrsNMIT("opt_value")
           end if                   
        else
           get_nmiss_inj_type =  "Near miss injury type could not be found"
        end if
   else    
    
        Dim dictNMissInj
        Set dictNMissInj = CreateObject("Scripting.Dictionary")
        	
        dictNMissInj.Add "1", "Death"
        dictNMissInj.Add "4", "Dismemberment"
        dictNMissInj.Add "2", "Fracture"
        dictNMissInj.Add "3", "Cut"
        dictNMissInj.Add "5", "Burn"
        dictNMissInj.Add "6", "Bruise"
        dictNMissInj.Add "7", "Physcological"
        dictNMissInj.Add "8", "Illness / Disease"
        dictNMissInj.Add "9", "Multiple Injuries"

        myinjtype = "" & myinjtype
        get_nmiss_inj_type = dictNMissInj.Item(myNMinjtype)
    end if
End function




Sub nmiss_prop_dam(var_sub_name,val_sub_value, showDisabled)
   
   response.write "<select name='" & var_sub_name & "'  class='select'>" & _
                    "<option value='0'>Please select a property damage type</option>"
                    
   if  Session("MODULE_ACCB") ="2" then
     
        
        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_pdam' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMPD = objcommand.execute

        if not objrsNMPD.eof then 
            while not objrsNMPD.eof
                if objrsNMPD("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMPD("opt_id")) then          
                    if cstr(val_sub_value) =  cstr(objrsNMPD("opt_id")) then
                        response.Write  "<option value='" & objrsNMPD("opt_id") & "' title='" & objrsNMPD("opt_description") & "' selected>" &  objrsNMPD("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMPD("opt_id") & "' title='" & objrsNMPD("opt_description") & "'>" &  objrsNMPD("opt_value") & "</option>"
                    end if
                
                    if objrsNMPD("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_pdam' and parent_id= '" & objrsNMPD("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMPDSUB = objcommand.execute

                        if not objrsNMPDSUB.eof then 
                            while not objrsNMPDSUB.eof
                                if objrsNMPDSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMPDSUB("opt_id")) then  
                                    if cstr(val_sub_value) =  cstr(objrsNMPDSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMPDSUB("opt_id") & "' title='" & objrsNMPDSUB("opt_description") & "' selected >--" & objrsNMPD("opt_value") & " (" &  objrsNMPDSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMPDSUB("opt_id") & "' title='" & objrsNMPDSUB("opt_description") & "'>--" & objrsNMPD("opt_value") & " (" &  objrsNMPDSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsNMPDSUB.movenext
                            wend
                        end if
                    end if    
                end if
                
                objrsNMPD.movenext
            wend
        end if
   
   
    else

        Dim dictNMissPDam
        Set dictNMissPDam = CreateObject("Scripting.Dictionary")
        	
        dictNMissPDam.Add "1", "Collapse of structure"
        dictNMissPDam.Add "2", "High damage cost"
        dictNMissPDam.Add "3", "Low damage cost"
                
        var_sub_value = "" & var_sub_value
        For Each key In dictNMissPDam
            Response.Write "<option value='" & key & "'"
            If var_sub_value = key Then 
                Response.Write " selected='selected'" 
            End if
            Response.Write ">" & dictNMissPDam.item(key) & "</option>" & vbCrLf
        Next
        
    end if
    
    response.Write "</select>"
    
End Sub




Sub nmiss_prop_dam2(val_sub_value, showDisabled)
   
   if  Session("MODULE_ACCB") ="2" then
       
       objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_pdam' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsNMPD = objcommand.execute

        if not objrsNMPD.eof then 
            while not objrsNMPD.eof
                if objrsNMPD("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMPD("opt_id")) then  
                    if cstr(val_sub_value) =  cstr(objrsNMPD("opt_id")) then
                        response.Write  "<option value='" & objrsNMPD("opt_id") & "' title='" & objrsNMPD("opt_description") & "' selected >" &  objrsNMPD("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMPD("opt_id") & "' title='" & objrsNMPD("opt_description") & "'>" &  objrsNMPD("opt_value") & "</option>"
                    end if
                
                    if objrsNMPD("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_pdam' and parent_id= '" & objrsNMPD("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsNMPDSUB = objcommand.execute

                        if not objrsNMPDSUB.eof then 
                            while not objrsNMPDSUB.eof
                                if objrsNMPDSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsNMPDSUB("opt_id")) then                              
                                    if cstr(val_sub_value) =  cstr(objrsNMPDSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsNMPDSUB("opt_id") & "' title='" & objrsNMPDSUB("opt_description") & "' selected >--" & objrsNMPD("opt_value") & " (" &  objrsNMPDSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsNMPDSUB("opt_id") & "' title='" & objrsNMPDSUB("opt_description") & "'>--" & objrsNMPD("opt_value") & " (" &  objrsNMPDSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsNMPDSUB.movenext
                            wend
                        end if

                    end if        
                end if
                
                objrsNMPD.movenext
            wend
        end if
    else

        Dim dictNMissPDam
        Set dictNMissPDam = CreateObject("Scripting.Dictionary")
        	
        dictNMissPDam.Add "1", "Collapse of structure"
        dictNMissPDam.Add "2", "High damage cost"
        dictNMissPDam.Add "3", "Low damage cost"
                
        var_sub_value = "" & var_sub_value
        For Each key In dictNMissPDam
            Response.Write "<option value='" & key & "'"
            If var_sub_value = key Then 
                Response.Write " selected='selected'" 
            End if
            Response.Write ">" & dictNMissPDam.item(key) & "</option>" & vbCrLf
        Next
        
    end if
    
End Sub
	
Function get_nmiss_prop_dam(myproptype)

  if  Session("MODULE_ACCB") ="2" then
        
        objcommand.commandtext = "select a.opt_value, b.opt_value AS parent_value from " & Application("DBTABLE_HR_ACC_OPTS") & " AS a " & _
                                 " LEFT OUTER JOIN " & Application("DBTABLE_HR_ACC_OPTS") & " AS b " & _
                                 " ON a.corp_code = b.corp_code AND a.related_to = b.related_to AND b.opt_id = a.parent_id AND a.opt_language = b.opt_language " & _
                                 " where a.corp_code='" & session("CORP_CODE") & "' and a.related_to='nmiss_pdam' and a.opt_id='" & clng(myproptype) & "' and a.opt_language='" & session("YOUR_LANGUAGE") & "'"
               
        
        set objrsNMPD = objcommand.execute
                   
        if not objrsNMPD.eof then 
           if len(objrsNMPD("parent_value")) > 0 then
                get_nmiss_prop_dam = objrsNMPD("parent_value") & " > " & objrsNMPD("opt_value")
           else
                get_nmiss_prop_dam = objrsNMPD("opt_value")
           end if                   
        else
           get_nmiss_prop_dam =  "Near miss injury type could not be found"
        end if
   
   else    
        Dim dictNMissPDam
        Set dictNMissPDam = CreateObject("Scripting.Dictionary")
        	
        dictNMissPDam.Add "1", "Collapse of structure"
        dictNMissPDam.Add "2", "High damage cost"
        dictNMissPDam.Add "3", "Low damage cost"
        myproptype = "" & myproptype
        get_nmiss_prop_dam = dictNMissPDam.Item(myproptype)
  end if
End function


function get_nmiss_severity(var_input_value)

    if len(var_input_value) > 0 then
   
   

    dim serious_array(3)
    serious_array(1) = "Lucky no injury or illness resulted"
    serious_array(2) = "Lucky no property damage and / or financial loss sustained"
    serious_array(3) = "Not serious and / or unlikely to re-occur"

    objCommand.commandText = "SELECT opt_description FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'nmiss_serious' " & _
                             "  AND opt_id = '" & var_input_value & "' " & _
                             "  AND opt_language='" & session("YOUR_LANGUAGE") & "' "
    set objRsNM = objCommand.execute

    if not objRsNM.EOF then
        get_nmiss_severity = objRsNM("opt_description")
    else
        get_nmiss_severity = serious_array(var_input_value)
    end if
    set objRsNM = nothing
     end if
end function

sub get_nmiss_severity1(var_input_name,var_input_value, var_viewtype)

    dim serious_array(3)
    serious_array(1) = "Lucky no injury or illness resulted"
    serious_array(2) = "Lucky no property damage and / or financial loss sustained"
    serious_array(3) = "Not serious and / or unlikely to re-occur"

    dim seriousval_array(3)
    seriousval_array(1) = 1
    seriousval_array(2) = 2
    seriousval_array(3) = 3

    response.Write "<select class='select' name='" & var_input_name & "' onchange='toggle_serious(this.value)'>"
    if var_viewtype = "search" then
        response.write "<option value='All'>Any type of seriousness</option>"
    else
        response.write "<option value='NA'>Please select how serious the incident could have been</option>"
    end if
    
    objCommand.commandText = "SELECT opt_id, opt_description FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'nmiss_serious' " & _
                             "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " & _
                             "ORDER BY opt_id ASC"
    set objRsNM = objCommand.execute
    if not objRsNM.EOF then
        'use custom options
        while not objRsNM.EOF
            if var_input_value <> "NA" and var_input_value <> "All" then
                if cint(objRsNM("opt_id")) = cint(var_input_value) then
                    select_val = " SELECTED "
                else
                    select_val = " "
                end if
            else
                select_val = " "
            end if
            response.Write "<option value='" & objRsNM("opt_id") & "' " & select_val & ">" & objRsNM("opt_description") & "</option>"
            objRsNM.movenext
        wend
    else
        'use system options
        for i=1 to ubound(serious_array) 
            if var_input_value <> "NA" and var_input_value <> "All" then
                if cint(seriousval_array(i)) = cint(var_input_value) then  
                    select_val = " SELECTED "
                else
                    select_val = " "
                end if
            else
                select_val = " "
            end if
            response.Write "<option value='" & seriousval_array(i) & "' " & select_val & ">" & serious_array(i) & "</option>"
        next 
    end if
    set objRsNM = nothing
    
    response.Write "</select>"
end sub



sub pdam_class(val_sub_value,val_sub_name,val_sub_type, val_sub_disabled, switch1,switch2, showDisabled)
     if isnull(val_sub_value) then 
            val_sub_value = ""
     end if
    
    Response.Write("<SELECT Name='" & val_sub_name & "' class='select' " &  val_sub_disabled &  " >")
    
    if val_sub_type = "S" then 'search
        Response.Write("<option value='all'>Any classification</option>")
    else
        Response.Write("<option value='any'>Please select a classification ...</option>")
    end if
     
      objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pdam_class' and parent_id= '0' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsPDC = objcommand.execute

        if not objrsPDC.eof then 
            while not objrsPDC.eof
                if objrsPDC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsPDC("opt_id")) then                 
                    if cstr(val_sub_value) =  cstr(objrsPDC("opt_id")) then
                        response.Write  "<option value='" & objrsPDC("opt_id") & "' title='" & objrsPDC("opt_description") & "' selected >" &  objrsPDC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsPDC("opt_id") & "' title='" & objrsPDC("opt_description") & "'>" &  objrsPDC("opt_value") & "</option>"
                    end if
                
                    if objrsPDC("child_present") = "1" then
                        'There are sub options available
                        objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pdam_class' and parent_id= '" & objrsPDC("opt_id") & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
                        set objrsPDCSUB = objcommand.execute

                        if not objrsPDCSUB.eof then 
                            while not objrsPDCSUB.eof
                                if objrsPDCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) = cstr(objrsPDCSUB("opt_id")) then                              
                                    if cstr(val_sub_value) =  cstr(objrsPDCSUB("opt_id")) then
                                        response.Write  "<option value='" & objrsPDCSUB("opt_id") & "' title='" & objrsPDCSUB("opt_description") & "' selected >--" & objrsPDC("opt_value") & " (" &  objrsPDCSUB("opt_value") & ")</option>"
                                    else
                                        response.Write  "<option value='" & objrsPDCSUB("opt_id") & "' title='" & objrsPDCSUB("opt_description") & "'>--" & objrsPDC("opt_value") & " (" &  objrsPDCSUB("opt_value") & ")</option>"
                                    end if
                                end if
                                objrsPDCSUB.movenext
                            wend
                        end if
            
                    end if
                 end if
                
                objrsPDC.movenext
            wend
        end if
    
	Response.Write("</select>")
end sub 


Function getPDamClassForCode(myPDamClass)

        'objcommand.commandtext = '"select parent_id, opt_value from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pdam_class'  and opt_id='" & myPDamClass & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        
    objcommand.commandtext =    "select b.opt_value as parentValue, a.opt_value as childValue, b.opt_id   " & _
                                "from " & Application("DBTABLE_HR_ACC_OPTS") & " a                        " & _
                                "LEFT JOIN (                                                              " & _
	                            "    SELECT corp_code, related_to, opt_id, opt_language, opt_value        " & _
	                            "    from " & Application("DBTABLE_HR_ACC_OPTS") & "                      " & _
                                ") b                                                                      " & _
	                            "    ON a.corp_code = b.corp_code                                         " & _
	                            "    and a.related_to = b.related_to                                      " & _
	                            "    and a.opt_language = b.opt_language                                  " & _
	                            "    and a.parent_id = b.opt_id                                           " & _
                                "where a.corp_code='" & session("CORP_CODE") & "'                         " & _
	                            "    and a.related_to='pdam_class'                                        " & _
	                            "    and a.opt_id='" & myPDamClass & "'                                   " & _
	                            "    and a.opt_language='" & session("YOUR_LANGUAGE") & "'                " & _
                                "order by a.opt_value asc                                                 "

        set objrsPDC = objcommand.execute

        if not objrsPDC.eof then 

            if IsNull(objrsPDC("parentValue")) then
                getPDAMClassForCode = objrsPDC("childValue")
            else
                getPDAMClassForCode = objrsPDC("parentValue") + " - " + objrsPDC("childValue")
            end if

           
        else
           getPDamClassForCode =  "Property damage classification could not be found"
        end if
   
End function

Function getPDamClassColourForCode(myPDamClass)

        objcommand.commandtext = "select opt_colour from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='pdam_class' and parent_id= '0' and opt_id='" & myPDamClass & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
        set objrsPDC = objcommand.execute

        if not objrsPDC.eof then 
           getPDAMClassColourForCode = objrsPDC("opt_colour")
        else
           getPDamClassColourForCode =  ""
        end if
   
End function


Sub autogenEnviroOpts(myType, myvalue, showDisabled)

    if isnull(myType) then
        myType = ""
    end if
    if isnull(myvalue) then
        myvalue = ""
    end if

    objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='" & mytype & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
    set objrs = objcommand.execute

    response.write "<option value='0'>-- Select Option --</option>"

    if not objRs.EOF then
        while not objRs.EOF
            if objrs("opt_disabled") = false or showDisabled = true or cstr(myvalue) = cstr(objrs("opt_id")) then                 
                response.write "<option value='" & objrs("opt_id") & "' title='" & objrs("opt_description") & "' " 
                
                if isnull(objrs("opt_id")) = false then
                    If myvalue = Cstr(objrs("opt_id")) then
                        response.write "SELECTED"
                    End if
                end if

                response.write " >" & objrs("opt_value") & "</option>"
            end if
            
            objrs.movenext
        wend
    end if
            
end sub

Sub autogenEnviroOptsChk(myType,myvalue, showDisabled)

    if isnull(myType) then
        myType = ""
    end if
    if isnull(myvalue) then
        myvalue = ""
    end if

    objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='" & mytype & "' and opt_language='" & session("YOUR_LANGUAGE") & "' order by opt_value asc"
    set objrs = objcommand.execute

    if not objRs.EOF then
        while not objRs.EOF
            if objrs("opt_disabled") = false or showDisabled = true or instr(1, cstr(myvalue), ":" & cstr(objrs("opt_id")) & ":") > 0 then      
                response.write "<div style='width: auto; float:left; postion:absolute; padding-right:15px;'><p><input type='checkbox' value='" & objrs("opt_id") & "' id='" & myType & "_chk' name='" & myType & "_chk' "

                if isnull(objrs("opt_id")) = false then
                    If InStr(1,myvalue,":" & Cstr(objrs("opt_id")) & ":") Then
                        response.write "CHECKED"
                    End if
                end if

                response.write " >&nbsp;" & objrs("opt_value") & "</p></div>"
            end if
            objrs.movenext
        wend
    end if

            
end sub


sub getDisplayShifts(selected_value, field_name, showDisabled) 

    if isnull(selected_value) = true or selected_value = "" then
        selected_value = 0
    end if

    objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'inc_shift' " & _
                             "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " & _
                             "ORDER BY opt_id"
    set objRsShifts = objCommand.execute
    if not objRsShifts.EOF then
    
        response.write "<tr><th>Shift Pattern</th><td nowrap='nowrap'>"
        response.write "<select class='select' name='" & field_name & "'>"
        response.write "<option value='0'>Please Select</option>"

        while not objRsShifts.EOF
            if objrsShifts("opt_disabled") = false or showDisabled = true or cstr(selected_value) = cstr(objrsShifts("opt_id")) then
                if cint(selected_value) = cint(objRsShifts("opt_id")) then
                    response.write "<option value='" & objRsShifts("opt_id") & "' selected>" & objRsShifts("opt_value") & "</option>"
                else
                    response.write "<option value='" & objRsShifts("opt_id") & "'>" & objRsShifts("opt_value") & "</option>"
                end if
            end if
            objRsShifts.movenext
        wend

        response.write "</select>"
        response.write "&nbsp; &nbsp;"
        call getDisplayDayNight(var_frm_acc_days, "frm_acc_daynight", showDisabled)
        response.write "</td></tr>"

    end if
    set objRsShifts = nothing
end sub

sub getDisplayDayNight(selected_value, field_name, showDisabled)

    if isnull(selected_value) = true or selected_value = "" then
        selected_value = 0
    end if

    objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND related_to = 'inc_time' " & _
                             "  AND opt_language = '" & session("YOUR_LANGUAGE") & "' " & _
                             "ORDER BY opt_id"
    set objRsTime = objCommand.execute
    if not objRsTime.EOF then
        
        response.write "<select class='select' name='" & field_name & "'>"
        response.write "<option value='0'>Please Select</option>"

        while not objRsTime.EOF
            if objrsTime("opt_disabled") = false or showDisabled = true or cstr(selected_value) = cstr(objrsTime("opt_id")) then
                if cint(selected_value) = cint(objRsTime("opt_id")) then
                    response.write "<option value='" & objRsTime("opt_id") & "' selected>" & objRsTime("opt_value") & "</option>"
                else
                    response.write "<option value='" & objRsTime("opt_id") & "'>" & objRsTime("opt_value") & "</option>"
                end if
            end if
            objRsTime.movenext
        wend

        response.write "</select>"

    end if
    set objRsTime = nothing

end sub



'sub AutoGenIncCentreList()

'end sub

'sub AutoGenIncGroupList()

'end sub

'copied from autogen_menus.asp for use in Property damage module
sub DisplayUSERInfo(sub_user_value, sub_user_name, sub_sw1, sub_sw2, sub_sw3, sub_sw4, sub_sw5, sub_sw6)
    
    if isnull(sub_user_value) then
        sub_user_value = ""
    end if
    'sub parameters names
    'sub_sw1 = Disabled
    'sub_sw2 = Type (S is for search engines) (T is for task user lists)
    'sub_sw3 = Javascript
    'sub_sw4 = Show disabled users
    'sub_sw5 = if instr hide these users: (TM - Task manager, RO - Read Only)
    'sub_sw6 = Empty

    Response.Write  "<select size='1' name='" & sub_user_name & "' class='select' " & sub_sw1 &  " " & sub_sw3 & ">"
                    
                    select case sub_sw2 
                        case "S" 
                            response.Write    "<option value='0'>Any user</option>"                        
                        case "T"
                          response.Write    "<option value='0'>Select person</option>" 
                                      response.Write  "<option value='nreq' "
                                             If cstr("nreq") = cstr(sub_user_value) Then response.Write " selected='selected' "
                                      response.Write   ">Nobody required at this time</option>" &_                            
                                        "<option value='0'>----------------------------</option>"
                        case else
                            response.Write    "<option value='0'>Please select a user</option>"       
                    end select 
                   

	objCommand.Commandtext = "SELECT DISTINCT data.Acc_Ref,data.Per_fname,data.Per_sname, tier4.struct_name FROM " & Application("DBTABLE_USER_DATA") & " as data " & _ 
	                        "LEFT OUTER JOIN  " & Application("DBTABLE_STRUCT_TIER4") & "  AS tier4 " & _
                          "on data.corp_code = tier4.corp_code " & _
                           "and cast(data.corp_bd as varchar(50)) = cast(tier4.tier4_ref as varchar(50)) "           

    if session("user_perms_active") = 1 and session("YOUR_ACCESS") <> "0" and (session("tm_limit_assessor_tier2") = "Y") then
        objCommand.commandText = objCommand.commandText & "INNER JOIN TempPermDB.dbo.TEMP_" & trim(session("CORP_CODE")) & "_" & session("YOUR_ACCOUNT_ID") & " as perms " & _
      					                                  "  on data.corp_bu = perms.tier2_ref " & _
							                              "  and perms.perm_read = 1 "
    end if

    objCommand.commandText = objCommand.commandText & "  WHERE data.corp_code='" & Session("corp_code")& "' AND data.Acc_level NOT LIKE '5' "
                           
                    if instr(1,ucase(session("YOUR_USERNAME")),"SYSADMIN.") = False then
                        objCommand.commandText = objCommand.commandText & "  AND (data.acc_name NOT LIKE 'sysadmin.%') "
                    end if                    
                    
                    if sub_sw4 = "N" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status not like 'D' "
                    elseif  sub_sw4 = "Y" then
                        objCommand.Commandtext = objCommand.Commandtext & " and data.Acc_status like 'D' "
                    end if 
                    
                       if instr(sub_sw5, "RO") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '3' "
                    end if      
                    
                    if instr(sub_sw5, "TM") then
                        objCommand.Commandtext = objCommand.Commandtext & " AND data.Acc_level NOT LIKE '4' "
                    end if
                          
          objCommand.Commandtext = objCommand.Commandtext & "  ORDER BY data.Per_sname ASC"
          
        
	SET objRsuser = objCommand.Execute
	
   	While NOT objRsuser.EOF
        Response.Write "<option value='" & objRsuser("Acc_Ref") & "'"
        If cstr(objRsuser("Acc_ref")) = cstr(sub_user_value) Then
            Response.Write " selected='selected'"
        End if
        Response.Write ">" & Ucase(objRsuser("Per_sname")) & ", " & objRsuser("Per_fname") & " (" & objRsuser("struct_name") & ")</option>"
        
        objRsuser.movenext
	Wend
    response.Write "</select>"
end sub

SUB pers_affected_file(sub_name, mod_type, sub_sw1, var_pa_ref, sub_sw3, var_pa_sub_ref, sub_sw5, sub_sw6)

 'sub parameters names
    'sub_sw1 = Disabled
    'var_pa_ref = reference/accident book
    'sub_sw3 = Javascript
    'var_sub_pa_ref = sub ref for fire zones
    'sub_sw5 = accident_reference
    'sub_sw6 = damage_reference
    
    var_pa_mod = mod_type
    
 if  var_pa_mod = "FS" then
    var_text = "<strong>MAXIMUM</strong>"
 else
    var_text = "average"
 end if
 
    if var_pa_mod = "PDAM_VEHICLE_PART" then
        response.write "<p>Details of any Vehicle Damage</p>"
    elseif var_pa_mod = "CCERA" then 
        response.Write "<p>Please identify the persons affected by this activity</p>"
    else
        response.Write "<p>Please provide " & var_text & " amounts of persons affected for each type that is relevant below</p>"
    end if
    
    response.Write  "<table width='100%' cellpadding='1px' cellspacing='2px'>" & _
        "<tr>"

        
    select case var_pa_mod
        case "RA"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_RAMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
        case "CCERA"
            if len(var_pa_sub_ref) > 0 then
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_CCERAMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' and a.haz_id = '" & var_pa_sub_ref & "' WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
            else
             objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, b.opt_ref, b.position FROM " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b  WHERE b.corp_code = '" & session("corp_code") & "'  order by position, opt_name "      
           end if
        case "MH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_MHMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.person_type = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "FS"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_FSMOD_PERSONS") & " as a on a.corp_code = b.corp_code and a.person_type = b.opt_ref and a.recordreference = '" & var_pa_ref & "' and a.loc_ref = '" & var_pa_sub_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "COSHH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_COSHH_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "SIRA"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.opt_ref, b.position from " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b left join " & Application("DBTABLE_SURA_PERSONS") & " as a on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref and a.recordreference = '" & var_pa_ref & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case "PDAM_VEHICLE_PART"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpartamount as col_persons_amount, b.opt_ref, b.position from " & Application("DBTABLE_VEHICLE_OPTS") & " as b left join " & Application("DBTABLE_DAM_VEHICLES") & " as a on a.corp_code = b.corp_code and a.recordpart = b.opt_ref and b.related_to = 'pdam_vehicle_part' and a.accb_code = '" & var_pa_ref & "' and a.recordreference = '" & sub_sw5 & "' and a.damreference = '" & sub_sw6 & "' WHERE b.corp_code = '" & session("corp_code") & "' order by position, opt_name"      
        case else
            response.Write "<td>Incorrect module code. Please contact support via the help tab.</td></tr></table>"
            exit sub
    end select
  
  set objrs = objcommand.execute 

  while not objrs.eof
        if row_count = 3 then
            response.Write "</tr>"
            response.Write "<tr>"
            row_count = 0
        end if
                
        row_count = row_count + 1
            
        if isnull(objrs("col_persons_amount")) then
            var_paff_value = "0"
        else
            var_paff_value = objrs("col_persons_amount")
        end if
             
        response.Write "<th>" & objrs("col_persons") & "</th><td>"
        if var_pa_mod = "CCERA" or var_pa_mod = "PDAM_VEHICLE_PART" then 
            response.write "<input type='checkbox' class='checkbox' name='" & sub_name & objrs("opt_ref") & "' value='1' "
            if var_paff_value > 0 then response.write " checked "
            response.write  sub_sw1 & " />"
        else
            response.write "<input type='text' size='3' class='text' name='" & sub_name & objrs("opt_ref") & "' value='" & var_paff_value & "' " & sub_sw1 & "/>"
        end if
            
        response.write "</td>"
        
        objrs.movenext
   wend
        
    if row_count = 3 then
        response.Write "</tr>"
    else
        for i = row_count to 3
                response.Write "<td></td>"
        next
        response.Write "</tr>"
    end if
    response.Write "</table>"
END SUB

sub Pers_Affected_report(mod_type, ref1, ref2, ref3, ccode, sub_ref)

    var_pa_ref = ref1
    var_pa_mod = mod_type
    var_pa_corp_code = ccode ' written this way to allow for archives
    var_pa_sub_ref = sub_ref
    
    var_no_data_text = "persons affected"

    select case var_pa_mod
        case "RA"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_RAMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "CCERA"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_CCERAMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  and a.haz_id = '" & var_pa_sub_ref & "' order by position, opt_name "      
        case "MH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.position  from " & Application("DBTABLE_MHMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.person_type = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "FS"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, person_qty as col_persons_amount, b.position  from " & Application("DBTABLE_FSMOD_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.person_type = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "' and a.loc_ref = '" & var_pa_sub_ref & "'  order by position, opt_name "      
        case "COSHH"
              objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  from " & Application("DBTABLE_COSHH_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "SIRA"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpersonsamount as col_persons_amount, b.position  from " & Application("DBTABLE_SURA_PERSONS") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case "PDAM_VEHICLE_PART"
            objcommand.commandtext = "Select distinct b.opt_name as col_persons, recordpartamount as col_persons_amount, b.position  from " & Application("DBTABLE_DAM_VEHICLES") & " as a left join " & Application("DBTABLE_VEHICLE_OPTS") & " as b on a.corp_code = b.corp_code and a.recordpart = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.accb_code = '" & ref1 & "' and a.recordreference = '" & ref2 & "' and a.damreference = '" & ref3 & "' order by position, opt_name "      
            var_no_data_text = "Vehicle Parts"
        case "MS"
              objcommand.commandtext = "SELECT DISTINCT b.opt_name as col_persons, a.recordpersonsamount as col_persons_amount, b.position  FROM " & Application("DBTABLE_MS_PA_V2") & " as a left join " & APPLICATION("DBTABLE_PERS_AFFECTED") & " as b on a.corp_code = b.corp_code and a.recordpersons = b.opt_ref WHERE a.corp_code = '" & var_pa_corp_code & "' and a.recordreference = '" & var_pa_ref & "'  order by position, opt_name "      
        case else
            response.Write "Incorrect module code. Please contact support via the help tab."
            exit sub
    end select
   
    SET objrspaff = objcommand.execute
        
    if objrspaff.eof then
    
    Response.Write  "<div class='warning'>" & _
                    "<table width='100%' cellpadding='2px' cellspacing='2px'>" & _
                    "<tr><td height='40'><strong>WARNING</strong><br />There are currently NO " & var_no_data_text & " that have been identified in this "
                    if var_pa_mod = "FS" then
                        Response.Write "zone"
                    elseif var_pa_mod = "PDAM_VEHICLE_PART" then
                        Response.Write "incident"
                    else
                        Response.Write "assessment" 
                    end if
    Response.Write  ".</td></tr>" & _
                    "</table>" & _
                    "</div>"
    
    else            
        while not objrspaff.eof
             if var_pa_mod = "CCERA" or var_pa_mod = "PDAM_VEHICLE_PART" then
               response.Write  objrspaff("col_persons") & "<br />"
             else
                response.Write  "<div class='floatdiv'><table width='220' cellpadding='1px' cellspacing='2px' class='showtd'><tr><th width='210'>" & objrspaff("col_persons") & "</td>" & _                
                    "<td width='40' nowrap>x " & objrspaff("col_persons_amount") & "</td>" & _           
                    "</tr></table></div>"
             end if
            objrspaff.Movenext
        Wend    
        response.Write "<div style='clear: both'></div>"                   
    end if 


end sub



sub showCustomMatrixGrid(showBoundingDiv, showScoreDescriptions)

    if ucase(session("RISK_MATRIX")) = "C" then

        if showBoundingDiv = true then
            if showScoreDescriptions = true then
                response.write "<div id='divMatrix' style='display:block; margin:25px auto 10px auto; width: 850px;'>"
            else
                response.write "<div id='divMatrix' style='display:block; margin:25px auto 10px auto; width: 550px;'>"
            end if
        end if

        'output a risk matrix
        Dim arrCellsDesc
        Set arrCellsDesc=Server.CreateObject("Scripting.Dictionary")

        Dim arrCellsColour
        Set arrCellsColour = Server.CreateObject("Scripting.Dictionary")

        Dim arrCellsFont
        Set arrCellsFont = Server.CreateObject("Scripting.Dictionary")

        objCommand.commandText = "SELECT overall.severity_value, overall.likelihood_value, overall.[description], overall.graph_colour, overall.font_colour, overall.likelihood_value*overall.severity_value AS grid_score " & _
                                 "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                                 "WHERE overall.corp_code = '" & session("CORP_CODE") & "' and display_language='en-gb' " & _
                                 "ORDER BY overall.likelihood_value, overall.severity_value"
        set objRsOverall = objCommand.execute
        if not objRsOverall.EOF then

            while not objRsOverall.EOF

                cellKey = objRsOverall("severity_value") & "_" & objRsOverall("likelihood_value") & "_" & objRsOverall("grid_score")
                cellItemDesc = objRsOverall("description")
                cellItemColour = objRsOverall("graph_colour")
                cellFontColour = objRsOverall("font_colour")

                arrCellsDesc.Add cellKey, cellItemDesc
                arrCellsColour.Add cellKey, cellItemColour
                arrCellsFont.Add cellKey, cellFontColour

                objRsOverall.movenext
            wend
        end if
        set objRsOverall = nothing

        objCommand.commandText = "SELECT COUNT(title) AS num_sev FROM " & Application("GLOBAL_RISK_MATRIX_severity") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0 and display_language='en-gb' "
        set objRsSev = objCommand.execute
        var_sev_count = objRsSev("num_sev")

        objCommand.commandText = "SELECT COUNT(title) AS num_lik FROM " & Application("GLOBAL_RISK_MATRIX_likelihood") & " WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0 and display_language='en-gb' "
        set objRsLik = objCommand.execute
        var_lik_count = objRsLik("num_lik")


        response.write "<style> .rotate { " & _
                        " -moz-transform: rotate(-90.0deg);  /* FF3.5+ */ " & _
                        " -o-transform: rotate(-90.0deg);  /* Opera 10.5 */ " & _
                        " -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */ " & _
                        " filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */ " & _
                        " -ms-filter: ""progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)""; /* IE8  " & _
                        " -ms-transform:rotate(-90deg); /* IE 9 */ " & _
                        " transform: rotate(-90deg); " & _
                        "} " & _
                        " .matrix_header { " & _
                        " font-size:1.3em; " & _
                        "} </style>"

        dim var_Sev_arry(20)
        dim var_Sev_arry_counter
        var_Sev_arry_counter = 0

        dim var_Lik_arry(20)
        dim var_Lik_arry_counter
        var_Lik_arry_counter = 0
    

        if session("RISK_MATRIX_SHOW_REVERSE") = "Y" then

            objCommand.commandText = "SELECT title, score, [description] " & _
                                     "FROM " & Application("GLOBAL_RISK_MATRIX_severity") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0  and display_language='en-gb'" & _
                                     "ORDER BY score DESC"
            set objRsSev = objCommand.execute
            while not objRsSev.EOF
                var_Sev_arry_counter = var_Sev_arry_counter + 1
                var_Sev_arry(var_Sev_arry_counter) = cint(objRsSev("score"))

                objRsSev.movenext        
            wend
            objRsSev.movefirst


            objCommand.commandText = "SELECT title, score, [description] " & _
                                     "FROM " & Application("GLOBAL_RISK_MATRIX_likelihood") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "'  and display_language='en-gb'" & _
                                     "ORDER BY score DESC"
            set objRsLik = objCommand.execute
            while not objRsLik.EOF
                var_Lik_arry_counter = var_Lik_arry_counter + 1
                var_Lik_arry(var_Lik_arry_counter) = cint(objRsLik("score"))

                objRsLik.movenext        
            wend
            objRsLik.movefirst


            if not objRsLik.EOF then

                response.write "<div><div style='float:left;'>"

                response.write "<table>"

                response.write "<tr><td colspan='3'>&nbsp;</td><td colspan='" & var_lik_count  & "' class='matrix_header'><strong>" & session("RISK_MATRIX_LIKE") & "</strong></td></tr>"

                table_row = ""
                table_row1 = ""

                while not objRsLik.EOF
           
                    table_row = table_row & "<td class='c' title='" & objRsLik("description") & "'>" & objRsLik("score") & "</td>"
                    table_row1 = table_row1 & "<td style='width:70px;' class='c' title='" & objRsLik("description") & "'><strong>" & objRsLik("title") & "</strong></td>"
                    'writing-mode: tb-rl;

                    objRsLik.movenext
                wend

                response.write "<tr><td colspan='3'>" & table_row & "</tr>"
                response.write "<tr><td colspan='3'>" & table_row1 & "</tr>"

                counter = 1
                row_head = ""

                objRsLik.movefirst

                while not objRsSev.EOF

                    if not objRsLik.EOF then

                        table_row = ""

                        if counter = 1 then
                            row_head = "<td rowspan='" & var_sev_count & "' class='rotate matrix_header'><strong>" & session("RISK_MATRIX_SEV") & "</strong></td>"
                        end if
       
                        while not objRsLik.EOF

                            if var_Lik_arry(counter) = cint(objRsLik("score")) then
                                row_head = row_head & "<td class='c' title='" & objRsLik("description") & "'>" & objRsSev("score") & "</td><td class='r' title='" & objRsLik("description") & "'><strong>" & objRsSev("title") & "</strong></td>"
                            end if

                            cell_score = cint(objRsLik("score")) * cint(objRsSev("score"))

                            cell_details = arrCellsDesc.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
                            cell_colour = arrCellsColour.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
                            font_colour = arrCellsFont.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
    
                            table_row = table_row & "<td class='c' style='background-color:#" & cell_colour & "; color:#" & font_colour & "' title='" & cell_details & "'>" & cell_score & "</td>"

                            objRsLik.movenext
                        wend

                        table_row = "<tr style='height:30px;'>" & row_head & table_row & "</tr>"

                        objRsLik.movefirst
                    end if

                    response.write table_row

                    row_head = ""
                    counter = counter + 1
                    objRsSev.movenext

                wend

                response.write "</table>"

                response.write "</div>"


            end if


        else

            objCommand.commandText = "SELECT title, score, [description] " & _
                                     "FROM " & Application("GLOBAL_RISK_MATRIX_severity") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' AND score != 0  and display_language='en-gb' " & _
                                     "ORDER BY score"
            set objRsSev = objCommand.execute
            if not objRsSev.EOF then
                while not objRsSev.EOF
                    var_Sev_arry_counter = var_Sev_arry_counter + 1
                    var_Sev_arry(var_Sev_arry_counter) = cint(objRsSev("score"))

                    objRsSev.movenext        
                wend
                objRsSev.movefirst
            end if

            objCommand.commandText = "SELECT title, score, [description] " & _
                                     "FROM " & Application("GLOBAL_RISK_MATRIX_likelihood") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' and display_language='en-gb' " & _
                                     "ORDER BY score"
            set objRsLik = objCommand.execute
            if not objRsLik.EOF then
                while not objRsLik.EOF
                    var_Lik_arry_counter = var_Lik_arry_counter + 1
                    var_Lik_arry(var_Lik_arry_counter) = cint(objRsLik("score"))

                    objRsLik.movenext        
                wend
                objRsLik.movefirst
            end if


            if not objRsSev.EOF then

                response.write "<div><div style='float:left;'>"

                response.write "<table>"

                response.write "<tr><td colspan='3'>&nbsp;</td><td colspan='" & var_sev_count  & "' class='matrix_header'><strong>" & session("RISK_MATRIX_SEV") & "</strong></td></tr>"

                table_row = ""
                table_row1 = ""

                while not objRsSev.EOF
           
                    table_row = table_row & "<td class='c' title='" & objRsSev("description") & "'>" & objRsSev("score") & "</td>"
                    table_row1 = table_row1 & "<td style='width:70px;' class='c' title='" & objRsSev("description") & "'><strong>" & objRsSev("title") & "</strong></td>"
                    'writing-mode: tb-rl;

                    objRsSev.movenext
                wend

                response.write "<tr><td colspan='3'>" & table_row & "</tr>"
                response.write "<tr><td colspan='3'>" & table_row1 & "</tr>"

                counter = 1
                row_head = ""

                objRsSev.movefirst

                while not objRsLik.EOF

                    if not objRsSev.EOF then

                        table_row = ""

                        if counter = 1 then
                            row_head = "<td rowspan='" & var_lik_count & "' class='rotate matrix_header'><strong>" & session("RISK_MATRIX_LIKE") & "</strong></td>"
                        end if
        
                        while not objRsSev.EOF

                            if var_Sev_arry(counter) = cint(objRsSev("score")) then
                                row_head = row_head & "<td class='c' title='" & objRsSev("description") & "'>" & objRsLik("score") & "</td><td class='r' title='" & objRsSev("description") & "'><strong>" & objRsLik("title") & "</strong></td>"
                            end if

                            cell_score = cint(objRsSev("score")) * cint(objRsLik("score"))

                            cell_details = arrCellsDesc.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
                            cell_colour = arrCellsColour.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
                            font_colour = arrCellsFont.Item(objRsSev("score") & "_" & objRsLik("score") & "_" & cell_score)
    
                            table_row = table_row & "<td class='c' style='background-color:#" & cell_colour & "; color:#" & font_colour & "' title='" & cell_details & "'>" & cell_score & "</td>"

                            objRsSev.movenext
                        wend

                        table_row = "<tr style='height:30px;'>" & row_head & table_row & "</tr>"

                        objRsSev.movefirst
                    end if

                    response.write table_row

                    row_head = ""
                    counter = counter + 1
                    objRsLik.movenext

                wend

                response.write "</table>"

                response.write "</div>"

            end if
        end if
        set objRsSev = nothing
        set objRsLik = nothing


        if showScoreDescriptions = true and session("RISK_MATRIX_HIDE_SCORES") <> "Y" then
            response.write "<div style='float:left; padding-left:30px; padding-top:46px;'>"

            objCommand.commandText = "SELECT DISTINCT overall.icon_rating, overall.[value], CAST(overall.[description] AS nvarchar(MAX)) AS overall_desc, overall.graph_colour " & _
                                        "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                                        "WHERE overall.corp_code = '" & session("CORP_CODE") & "' and overall.severity_value <> 0 " & _
                                        "ORDER BY overall.icon_rating "
            set objRsOverall = objCommand.execute
            if not objRsOverall.EOF then

                response.write "<table>"

                response.write "<tr><td colspan='2' class='matrix_header l'><strong>Risk Level</strong></td></tr>"

                while not objRsOverall.EOF 
                        
                    response.write "<tr><td style='background-color:#" & objRsOverall("graph_colour") & "; padding:5px;' class='l'>" & objRsOverall("value") & "</td>"
                    if len(objRsOverall("overall_desc")) > 0 then
                        response.write "<td class='l'>" & objRsOverall("overall_desc") & "</td>"
                    end if
                    response.write "</tr>"

                    objRsOverall.movenext
                wend

                response.write "</table>"
            end if
            set objRsOverall = nothing

            response.write "</div>"
        end if

        response.write "<div style='clear:both;'></div></div>"


        if showBoundingDiv = true then
            response.write "</div>"
        end if

    end if

end sub

sub showCustomMatrixResidual()


    if session("RISK_MATRIX_HIDE_SCORES") <> "Y" then

    if left(ucase(session("YOUR_USERNAME")), 8) = "SYSADMIN" then

        objCommand.commandText = "SELECT DISTINCT overall.icon_folder, overall.icon_name, overall.icon_rating, overall.[value] " & _
                                    "FROM " & Application("GLOBAL_RISK_MATRIX_overall") & " AS overall " & _
                                    "WHERE overall.corp_code = '" & session("CORP_CODE") & "' " & _
                                    "ORDER BY overall.icon_rating"
        set objRsOverall = objCommand.execute
        if not objRsOverall.EOF then

            response.write "<div><p>Residual Risk Graphics for reports</p></div>"

            response.write "<table width='100%'>"

            while not objRsOverall.EOF 

                response.write "<tr><td width='50%' class='r'><img src='../../../version3.2/modules/hsafety/risk_assessment/app/img/" & objRsOverall("icon_folder") & "/lrg/" & objRsOverall("icon_name") & ".png' /></td><td width='50%' class='l'><p>" & objRsOverall("value") & "</p></td></div>"

                objRsOverall.movenext
            wend

            response.write "</table>"

        end if
    end if

    end if

end sub



%>