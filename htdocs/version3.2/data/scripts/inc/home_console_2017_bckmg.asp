<%

'Resolves bug of storing data

Response.buffer=true
Response.Expires = -1
Response.ExpiresAbsolute = Now() -1 
Response.AddHeader "pragma", "no-store"
Response.AddHeader "cache-control","no-store, no-cache, must-revalidate"

'dictionary
dim mods
Set mods=Server.CreateObject("Scripting.Dictionary")

mods.add "RA", "Risk Assessments"
mods.add "TM", "Your Tasks"
mods.add "ACC", "Accident / Incident Reporting"
mods.add "MH", "Manual Handling"
mods.add "MS", "Method Statements"
mods.add "FS", "Fire Risk Assessments"
mods.add "COSHH", session("coshh_label_change") & " Assessments"
mods.add "MSDS", "Material Safety Data Sheets (MSDS)"
mods.add "DSE", "Display Screen Equipment Assessments"
mods.add "INS", "Safety Inspections"
mods.add "PTW", "Permits To Work"
mods.add "SA", "Safety Audits"
mods.add "PUWER", "PUWER Assessments"


public public_scrolling_content
public public_first_load_content
public public_tm_content




sub add_perm(module_code, position)
    objcommand.commandtext = "insert into " & Application("DBTABLE_GLOBAL_CONSOLE") & " (corp_code, acc_ref, module, position) values " & _
                             "('" & session("corp_code") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & module_code & "', '" & position & "')"
    objcommand.execute
end sub



'*****************************************************************************************************************
'*****************************************************************************************************************
'
'   Procedure to setup the modules to be displayed in the scrolling section
'
'*****************************************************************************************************************
'*****************************************************************************************************************

sub populate_console(mod_type, rec_tier2, rec_tier3, sub_sw1, sub_sw2, sub_sw3)

    '"mod_type = Module - if all then show all mods
    '"rec_tier2 = Tier2 ref to restrict results too
    '"rec_tier3 = Tier3 ref to restrict results too
    '"sub_sw2 = Empty
    '"sub_sw3 = Empty
    
    mycount = 0
     
    ' currently this looks at the access level of the viewer but an update will allow this to be set so GA""s can look at their own tasks for example
    var_view_access = session("YOUR_ACCESS")  


    'have the modules you have access to been set?
    objcommand.commandtext = "select console_set from " & Application("DBTABLE_USER_DATA") & " where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "'" 
    set objperm = objcommand.execute
    
    if isnull(objperm("console_set")) or objperm("console_set") = False then
        modcount = 0
        
        if mid(session("YOUR_LICENCE"),1,1) = "1" then
            modcount = modcount + 1
            call add_perm ("RA", modcount)
        end if
        
        objcommand.commandtext = "select id from " & APPLICATION("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code='" & session("corp_code") & "' and user_ref='" & session("YOUR_ACCOUNT_ID") & "'"
        set objrs = objcommand.execute
        if  not objrs.eof then 
            modcount = modcount + 1
            call add_perm ("ACC", modcount)
        end if
        
        if mid(session("your_licence"),2,1) = "1" then  
            modcount = modcount + 1
            call add_perm ("MH", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),5,1) = "1" then
            modcount = modcount + 1
            call add_perm ("MS", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),4,1) = "1" then
            modcount = modcount + 1
            call add_perm ("FS", modcount)
        end if
        
         if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2"  then
            modcount = modcount + 1
            call add_perm ("COSHH", modcount)
            modcount = modcount + 1
            call add_perm ("MSDS", modcount)
        end if
        
         if (mid(session("your_licence"),3,1) = "1" or mid(session("your_licence"),10,1) = "1" or mid(session("your_licence"),18,1) = "1") and Session("MODULE_DSE") = "2" then  
            modcount = modcount + 1
            call add_perm ("DSE", modcount)
        end if      
        
        if mid(session("YOUR_LICENCE"),15,1) = "1" then
            modcount = modcount + 1
            call add_perm ("INS", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),14,1) = "1" and Session("MODULE_PTW") = "2" then
            modcount = modcount + 1
            call add_perm ("PTW", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),6,1) = "1" and Session("MODULE_SA") = "2" then
            modcount = modcount + 1
            call add_perm ("SA", modcount)
        end if

        if mid(session("YOUR_LICENCE"),34,1) = "1" then
            modcount = modcount + 1
            call add_perm ("PUWER", modcount)
        end if
        
        objcommand.commandtext = "update " & Application("DBTABLE_USER_DATA") & " set console_set = 1 where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "'" 
        objcommand.execute
    end if
    
    
    
    '"grab module that the user has access too
    
    objcommand.commandtext = "select * from " & Application("DBTABLE_GLOBAL_CONSOLE") & " where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "' order by position" 
    set objrsmods = objcommand.execute
    
    if not objrsmods.eof then

        while not objrsmods.eof
            
            select case objrsmods("module")
                case "RA"
                    if mid(session("YOUR_LICENCE"),1,1) = "1" and (mod_type = "RA" or mod_type = "ALL") then 
                        call populate_module ("RA", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

                case "ACC"          
                    objcommand.commandtext = "select id from " & APPLICATION("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code='" & session("corp_code") & "' and user_ref='" & session("YOUR_ACCOUNT_ID") & "'"
                    set objrs = objcommand.execute
                    if  not objrs.eof and (mod_type = "ACC" or mod_type = "ALL") then 
                        call populate_module ("ACC", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                 '   response.Flush

                case "MH"
                    if mid(session("YOUR_LICENCE"),2,1) = "1" and (mod_type = "MH" or mod_type = "ALL") then 
                        call populate_module ("MH", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                 '   response.Flush

                case "MS"
                    if mid(session("YOUR_LICENCE"),5,1) = "1" and (mod_type = "MS" or mod_type = "ALL") then 
                        call populate_module ("MS", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

                case "FS"
                    if mid(session("YOUR_LICENCE"),4,1) = "1" and (mod_type = "FS" or mod_type = "ALL") then 
                        call populate_module ("FS", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                    'response.Flush

                case "COSHH"
                    if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2" and (mod_type = "COSHH" or mod_type = "ALL") then 
                        call populate_module ("COSHH", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                    'response.Flush

                case "MSDS"
                    if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2" and (mod_type = "MSDS" or mod_type = "ALL") then 
                        call populate_module ("MSDS", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

                case "DSE"
                     if (mid(session("your_licence"),3,1) = "1" or mid(session("your_licence"),10,1) = "1" or mid(session("your_licence"),18,1) = "1") and Session("MODULE_DSE") = "2"  and (mod_type = "DSE" or mod_type = "ALL") then  
                        call populate_module ("DSE", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                    'response.Flush

                case "INS"
                    if mid(session("YOUR_LICENCE"),15,1) = "1" and (mod_type = "INS" or mod_type = "ALL") then 
                        call populate_module ("INS", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

                case "PTW"
                    if mid(session("YOUR_LICENCE"),14,1) = "1" and Session("MODULE_PTW") = "2" and (mod_type = "PTW" or mod_type = "ALL") then 
                        call populate_module ("PTW", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

                case "SA"
                    if mid(session("YOUR_LICENCE"),6,1) = "1"  and Session("MODULE_SA") = "2" and (mod_type = "SA" or mod_type = "ALL") then 
                        call populate_module ("SA", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1
                    end if

                    'response.Flush

                case "PUWER"
                    if mid(session("YOUR_LICENCE"),34,1) = "1" and (mod_type = "PUWER" or mod_type = "ALL") then 
                        call populate_module ("PUWER", var_view_access, mycount, rec_tier2, rec_tier3, "", "", "")
                        mycount = mycount + 1 
                    end if

                   ' response.Flush

           end select

           objrsmods.movenext
        wend
    end if        

end sub





'*****************************************************************************************************************
'*****************************************************************************************************************
'
'   Procedure to calculate and output the statistics
'
'*****************************************************************************************************************
'*****************************************************************************************************************

sub populate_module(mod_code, view_access, mycount, rec_tier2, rec_tier3, sub_sw1, sub_sw2, sub_sw3)
    
    'mod_code = module
    '"view_access = access level you wish to view
    'mycount = value for the position of the java script array
    'rec_tier2 = Tier2 value to restrict search results too
    'rec_tier3 = Tier3 value to restrict search results too
    'sub_sw1 = Empty
    'sub_sw2 = Empty
    'sub_sw3 = Empty

    var_scrolling_box_content = "<div id=""stats_mod_" & mod_code & """ class=""stats_module"">" & _
                                    "<div id=""stats_mod_" & mod_code & "_inner"" class=""stats_module_inner"">" & _
                                        "<h1>" & mods.Item(mod_code) & "</h1>" & _
                                        "<table class=""stats_content"">" & _
                                        "<tr><td class=""stats_content_left""><i class=""fa fa-tasks"" style=""color: #c00c00; font-size: 50px;"" aria-hidden=""true""></i></td><td class=""stats_content_right"">" 
                
    if mod_code = "TM" then  
         
        objcommand.commandtext = "exec MODULE_GLOBAL.dbo.TM_Console_Stats '" & session("TASK_ACCEPT_ENABLED") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("corp_code") & "' "
    'response.write objcommand.commandtext    
    set objrs = objcommand.execute
                
        if not objrs.eof then
            var_tm_1 = objrs("mycount")  
            set objrs = objrs.nextrecordset
            if not objrs.eof then
                var_tm_2 = objrs("mycount")  
                set objrs = objrs.nextrecordset
                if not objrs.eof then
                    var_tm_3 = objrs("mycount")  
                    if session("TASK_ACCEPT_ENABLED") = "Y" then
                        set objrs = objrs.nextrecordset
                        var_tm_4 = objrs("mycount")                            
                    end if
                else
                    var_tm_3 = "Error"
                end if   
            else
                var_tm_2 = "Error"
                var_tm_3 = "Error"
            end if                    
        else
            var_tm_1 = "Error"
            var_tm_2 = "Error"
            var_tm_3 = "Error"
        end if
    
        if var_tm_1 > 0 then 
            var_tm_1 = "<span style='color:#c00c00 !important;  font-size:1.25em;'><strong>" & var_tm_1 & "</strong></span>"
        end if
               
        'show overdue records
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/taskmanager_v2/tasks.asp?showfilter=Y&srch_but=reset&frm_operator=bef&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"">Overdue Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_1 & "</div></div><div class=""clear""></div></div>"
            
        'show Todays records
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/taskmanager_v2/tasks.asp?showfilter=Y&srch_but=reset&frm_operator=equ&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"">Today`s Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_2 & "</div></div><div class=""clear""></div></div>"
    
        var_14_days = dateadd("d",15,now())
                
        'show tasks due in the next 14 days
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/taskmanager_v2/tasks.asp?showfilter=Y&srch_but=reset&frm_operator=bet&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & "&frm_date2_d=" & day(var_14_days) & "&frm_date2_m=" & month(var_14_days) & "&frm_date2_y=" & year(var_14_days) & """ target=""_parent"">Tasks Due in the Next 14 Days</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_3 & "</div></div><div class=""clear""></div></div>"
    
        if session("TASK_ACCEPT_ENABLED") = "Y" then
            'show tasks that haven""t been accepted
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/taskmanager_v2/tasks.asp""  target=""_parent"">Tasks Requiring Your Attention</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_4 & "</div></div><div class=""clear""></div></div>"
        end if

         if session("PREF_TASK_SIGNOFF") = "Y" then
                var_awo_count  = 0
                objcommand.commandtext = "select count(id) as awo_count from " & Application("DBTABLE_TASK_MANAGEMENT") & " where corp_code = '" & session("corp_code") & "' and task_status = '0' and Task_signoff = '0' and (task_by = '" & session("YOUR_ACCOUNT_ID") & "' or  '0' = '" & session("YOUR_ACCESS") & "')"
             
                set objrs_awo =  objcommand.execute
                var_awo_count = objrs_awo("awo_count")
            
            if var_awo_count > 0 then
            'Show how many tasks are awaiting sign off
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/taskmanager_v2/tasks.asp?ref=&action=&showfilter=Y&frm_taskuser=0&frm_taskbyuser=0&frm_taskrisk=ALL&frm_user_tier2=0&frm_user_tier3=0&frm_user_tier4=0&frm_user_tier5=0&frm_user_tier6=0&frm_taskstatus=3&frm_taskrelation=ALL&frm_taskreference=&frm_operator=any&srch_but=Apply+filter&bt_assign=NA&bt_status=na&bt_date=na&bt_date_d=DD&bt_date_m=MM&bt_date_y=YYYY&bt_date_atd_int=1&bt_date_atd_ddl=d#results"" target=""_parent"">Tasks Requiring Sign Off</a></div><div class=""dots""><div class=""statistic_value"">" & var_awo_count & "</div></div><div class=""clear""></div></div>"
            end if
        end if

    elseif mod_code = "RA" then  

        if session("CCERA") = "Y" then 
                 
            'Grab data from DB via SP
            objcommand.commandtext = "exec MODULE_CCERA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"     
            set objRs = objcommand.execute

            if not objrs.eof then
                var_ra_1 = objrs("mycount")  
                set objrs = objrs.nextrecordset
                if not objrs.eof then
                    var_ra_2 = objrs("mycount")
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ra_3 = objRs("mycount")
                            set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ra_4 = objRs("mycount")
                            set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                var_ra_5 = objRs("mycount")
                            else
                                var_ra_5 = "Error"
                            end if
                        else
                            var_ra_4 = "Error"
                            var_ra_5 = "Error"
                        end if
                    else
                        var_ra_3 = "Error"
                        var_ra_4 = "Error"
                        var_ra_5 = "Error"
                    end if
                else
                    var_ra_2 = "Error"
                    var_ra_3 = "Error"
                    var_ra_4 = "Error"
                    var_ra_5 = "Error"
                end if                    
            else
                var_ra_1 = "Error"
                var_ra_2 = "Error"
                var_ra_3 = "Error"
                var_ra_4 = "Error"
                var_ra_5 = "Error"
            end if 
              
            'show incomplete records
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"

            if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B"
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
                
            if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Copied&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Copied&ra_srch_type_flag=B"                
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_3 & "</div></div><div class=""clear""></div></div>"
            
            'show overdue records
            if session("YOUR_ACCESS") = "1" then 
                var_search_criteria = "ra_frm_srch_bu=" & session("YOUR_COMPANY") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
            elseif session("YOUR_ACCESS") = "2" then 
                var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
            else
                var_search_criteria = "ra_frm_srch_showop=All&ra_srch_type_flag=A"
            end if
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"
           
            var_search_criteria = "ra_frm_srch_showop=Awaiting Sign Off&ra_srch_type_flag=B"
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Awaiting Sign Off</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_5 & "</div></div><div class=""clear""></div></div>"
                
        else ' not coke                
          
            'Grab data from DB via SP
            objcommand.commandtext = "exec MODULE_RA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"                         
            if session("YOUR_ACCOUNT_ID")  = "2WDJTI6sT6omE6P" and Request.ServerVariables("REMOTE_ADDR")  = "195.60.7.88" then  response.write objcommand.commandtext
   
    set objrs = objcommand.execute

            if not objRs.EOF then
                var_Complete = objRs("countComplete")
            else
                var_Complete = "Err"
            end if

            set objRs = objRs.NextRecordset
            if not objRs.EOF then
                var_ReviewOverdue = objRs("countReviewOverdue")
            else
                var_ReviewOverdue = "Err"
            end if

            set objRs = objRs.NextRecordset
            if not objRs.EOF then
                var_ReviewDue = objRs("countReviewDue")
            else
                var_ReviewDue = "Err"
            end if

            set objRs= objRs.NextRecordset
            if not objRs.EOF then
                var_SignOff = objRs("countSignoff")
            else
                var_SignOff = "Err"
            end if
            set objRs = objRs.NextRecordset

            if not objRs.EOF then
                var_Incomplete = objRs("countIncomplete")
            else
                var_Incomplete = "Err"
            end if
            set objRs = nothing



            var_search_criteria = "../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?src=stats&ra_srch_type_flag=B&ra_frm_srch_submit=Search for assessment"
            if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
            if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_user_tier2=" & rec_tier2
            if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_user_tier3=" & rec_tier3


            var_search_criteria_comp = var_search_criteria & "&ra_frm_srch_showop=Complete" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

            var_search_criteria_rev_req = var_search_criteria & "&ra_frm_srch_showop=Review Overdue" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"

            var_search_criteria_rev_up = var_search_criteria & "&ra_frm_srch_showop=Review Upcoming" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_up & """ target=""_parent"">Review Upcoming</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"

            if session("ASSESSMENT_SIGNOFF") = "Y" then
                var_search_criteria_sign = var_search_criteria & "&ra_frm_srch_showop=Sign-off Required" 
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_sign & """ target=""_parent"">Sign-off Required</a></div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
            end if

            var_search_criteria_incomp = var_search_criteria & "&ra_frm_srch_showop=Incomplete"
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"

        end if




    elseif mod_code = "COSHH" then
        
        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_COSHH.dbo.COSHH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"               
        set objrs = objcommand.execute



        if not objRs.EOF then
            var_Complete = objRs("countComplete")
        else
            var_Complete = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewOverdue = objRs("countReviewOverdue")
        else
            var_ReviewOverdue = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewDue = objRs("countReviewDue")
        else
            var_ReviewDue = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_SignOff = objRs("countSignoff")
        else
            var_SignOff = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = nothing



        var_search_criteria = "../../../../version3.2/modules/hsafety/coshh/app/coshh_search.asp?src=stats&coshh_srch_type_flag=B&coshh_frm_srch_submit=Search for assessment"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_user_tier2=" & rec_tier2
        if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_user_tier3=" & rec_tier3


        var_search_criteria_comp = var_search_criteria & "&coshh_frm_srch_showop=Complete" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_req = var_search_criteria & "&coshh_frm_srch_showop=Review Overdue" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_up = var_search_criteria & "&coshh_frm_srch_showop=Review Upcoming" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_up & """ target=""_parent"">Review Upcoming</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"

        if session("ASSESSMENT_SIGNOFF") = "Y" then
            var_search_criteria_sign = var_search_criteria & "&coshh_frm_srch_showop=Sign-off Required" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_sign & """ target=""_parent"">Sign-off Required</a></div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
        end if

        var_search_criteria_incomp = var_search_criteria & "&coshh_frm_srch_showop=Incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"



    
          
    elseif mod_code = "MSDS" then

        objcommand.commandtext = "exec MODULE_COSHH.dbo.MSDS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', '', '" & session("MSDS_AGE_LIMIT") & "'"               
        'response.write objcommand.commandtext
        set objRs = objcommand.execute



        if not objRs.EOF then
            var_Complete = objRs("countComplete")
        else
            var_Complete = "Err"
        end if

        if len(session("MSDS_AGE_LIMIT")) > 0 then
            set objRs = objRs.NextRecordset
            if not objRs.EOF then
                var_ReviewOverdue = objRs("countReviewOverdue")
            else
                var_ReviewOverdue = "Err"
            end if
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = nothing



        var_search_criteria = "../../../../version3.2/modules/hsafety/coshh/msds/msds_search.asp?src=stats&search=Search+MSDS"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&msds_frm_srch_comp=" & rec_tier2
        if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&msds_frm_srch_loc=" & rec_tier3


        var_search_criteria_comp = var_search_criteria & "&msds_frm_srch_showop=1"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

        if len(session("MSDS_AGE_LIMIT")) > 0 then
            var_search_criteria_rev_req = var_search_criteria & "&msds_frm_srch_showop=5" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"
        end if

        var_search_criteria_incomp = var_search_criteria & "&msds_frm_srch_showop=3"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"



    elseif mod_code = "MH" then  

        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_MH.dbo.MH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"                         
        set objrs = objcommand.execute



        if not objRs.EOF then
            var_Complete = objRs("countComplete")
        else
            var_Complete = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewOverdue = objRs("countReviewOverdue")
        else
            var_ReviewOverdue = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewDue = objRs("countReviewDue")
        else
            var_ReviewDue = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
    if isnull(objRs("countSignoff")) then
    var_SignOff = 0
         else
           var_SignOff = objRs("countSignoff")
    end if
        else
            var_SignOff = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = nothing



        var_search_criteria = "../../../../version3.2/modules/hsafety/manual_handling/app/mh_search.asp?src=stats&mh_srch_type_flag=B&mh_submit=Search Assessments"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_pcompany=" & rec_tier2
        if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_plocation=" & rec_tier3


        var_search_criteria_comp = var_search_criteria & "&mh_frm_srch_showop=Complete" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_req = var_search_criteria & "&mh_frm_srch_showop=Review Overdue" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_up = var_search_criteria & "&mh_frm_srch_showop=Review Upcoming" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_up & """ target=""_parent"">Review Upcoming</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"

        if session("ASSESSMENT_SIGNOFF") = "Y" then
            var_search_criteria_sign = var_search_criteria & "&mh_frm_srch_showop=Sign-off Required" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_sign & """ target=""_parent"">Sign-off Required</a></div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
        end if

        var_search_criteria_incomp = var_search_criteria & "&mh_frm_srch_showop=Incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"




    elseif mod_code = "PUWER" then  

        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_PUWER.dbo.PUWER_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"                               
        set objrs = objcommand.execute



        if not objRs.EOF then
            var_Complete = objRs("countComplete")
        else
            var_Complete = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewOverdue = objRs("countReviewOverdue")
        else
            var_ReviewOverdue = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewDue = objRs("countReviewDue")
        else
            var_ReviewDue = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_SignOff = objRs("countSignoff")
        else
            var_SignOff = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = nothing



        var_search_criteria = "../../../../version3.2/modules/hsafety/puwer/app/puwer_search.asp?src=stats&puwer_srch_type_flag=B&puwer_frm_srch_submit=Search+Assessments"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_user_tier2=" & rec_tier2
        if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_user_tier3=" & rec_tier3


        var_search_criteria_comp = var_search_criteria & "&puwer_frm_srch_showop=Complete" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_req = var_search_criteria & "&puwer_frm_srch_showop=Review Overdue" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_up = var_search_criteria & "&puwer_frm_srch_showop=Review Upcoming" 
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_up & """ target=""_parent"">Review Upcoming</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"

        if session("ASSESSMENT_SIGNOFF") = "Y" then
            var_search_criteria_sign = var_search_criteria & "&puwer_frm_srch_showop=Sign-off Required" 
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_sign & """ target=""_parent"">Sign-off Required</a></div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
        end if

        var_search_criteria_incomp = var_search_criteria & "&puwer_frm_srch_showop=Incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"




    elseif mod_code = "FS" then

        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_FS.dbo.FS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"                          
        set objrs = objcommand.execute



        if not objRs.EOF then
            var_Complete = objRs("countComplete")
        else
            var_Complete = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewOverdue = objRs("countReviewOverdue")
        else
            var_ReviewOverdue = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_ReviewDue = objRs("countReviewDue")
        else
            var_ReviewDue = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_SignOff = objRs("countSignoff")
        else
            var_SignOff = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = nothing



        if session("FS_VERSION") = "2" then
            var_search_criteria = "../../../../version3.2/modules/hsafety/fire_safety_v2/app/firesafety_search.asp?src=stats&fs_srch_type_flag=B&fs_frm_srch_submit=Search for assessment"
            if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
            if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_user_tier2=" & rec_tier2
            if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_user_tier3=" & rec_tier3
        else
            var_search_criteria = "../../../../version3.2/modules/hsafety/fire_safety/app/firesafety_search.asp?fs_srch_type_flag=B&fs_frm_srch_submit=Search for assessment"
            if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
            if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&fs_frm_srch_bu=" & rec_tier2
            if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&fs_frm_srch_bl=" & rec_tier3
        end if

        var_search_criteria_comp = var_search_criteria & "&fs_frm_srch_show=Complete" 
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Complete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_Complete & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_req = var_search_criteria & "&fs_frm_srch_show=Review Overdue" 
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_req & """ target=""_parent"">Review Overdue</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Review Overdue</div><div class=""dots""><div class=""statistic_value"">" & var_ReviewOverdue & "</div></div><div class=""clear""></div></div>"

        var_search_criteria_rev_up = var_search_criteria & "&fs_frm_srch_show=Review Upcoming" 
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_rev_up & """ target=""_parent"">Review Upcoming</a></div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Review Upcoming</div><div class=""dots""><div class=""statistic_value"">" & var_ReviewDue & "</div></div><div class=""clear""></div></div>"

        if session("ASSESSMENT_SIGNOFF") = "Y" then
            var_search_criteria_sign = var_search_criteria & "&fs_frm_srch_show=Sign-off Required" 
            'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""" & var_search_criteria_sign & """ target=""_parent"">Sign-off Required</a></div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Sign-off Required</div><div class=""dots""><div class=""statistic_value"">" & var_Signoff & "</div></div><div class=""clear""></div></div>"
        end if

        var_search_criteria_incomp = var_search_criteria & "&fs_frm_srch_show=Incomplete"
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"




    elseif mod_code = "SA" then

'commented out because of an issue with the SA search engine. Works fine on Live


'        'Grab data from DB via SP
'        objcommand.commandtext = "exec MODULE_SA.dbo.SA_Console_Stats_ML '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''"    
'    response.write objcommand.commandtext
'    response.end
'        set objrs = objcommand.execute
'        if not objRs.EOF then
'            var_Complete = objRs("mycount")
'        else
'            var_Complete = "Err"
'        end if
'
'        set objRs = objRs.NextRecordset
'        if not objRs.EOF then
'            var_Incomplete = objRs("mycount")
'        else
'            var_Incomplete = "Err"
'        end if
'        set objRs = nothing


        var_search_criteria = "../../../../version3.2/modules/hsafety/safety_audit/v2/app/saudit_search.asp?sa_frm_srch_submit=Search Audits"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&frm_user_tier2=" & rec_tier2
        if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&frm_user_tier3=" & rec_tier3

               
        'show complete records
        var_search_criteria_comp = var_search_criteria & "&sa_frm_srch_showop=complete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_comp & """ target=""_parent"">Complete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_complete & "</div></div><div class=""clear""></div></div>"
            
        'show incomplete records
        var_search_criteria_incomp = var_search_criteria & "&sa_frm_srch_showop=incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_incomplete & "</div></div><div class=""clear""></div></div>"




    elseif mod_code = "PTW" then
      
        'This module has been part-built to work with the HPSTATS_CHANGABLE option, but has been completed - hence, stats are fixed to top tier level
      
            
        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_PTW.dbo.PTW_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "', '" & session("YOUR_TIER5") & "', '" & session("YOUR_TIER6") & "', '" & rec_tier2 & "', '" & rec_tier3 & "', '', '', ''" 
        set objrs = objcommand.execute


        if not objRs.EOF then
            var_Future = objRs("countFuture")
        else
            var_Future = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_Expired = objRs("countExpired")
        else
            var_Expired = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_Active = objRs("countActive")
        else
            var_Active = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_Incomplete = objRs("countIncomplete")
        else
            var_Incomplete = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_Waiting = objRs("countWaiting")
        else
            var_Waiting = "Err"
        end if
        set objRs = nothing


        var_search_criteria = "../../../../version3.2/modules/hsafety/permit_to_work_v2/app/ptw_search.asp?ptw_srch_type_flag=A&ptw_frm_srch_submit=Search Permits"
        if session("YOUR_ACCESS") <> "0" and session("YOUR_ACCESS") <> "1" then var_search_criteria = var_search_criteria & "&vtype=user"
        'if len(rec_tier2) > 0 and rec_tier2 <> "%" and rec_tier2 <> "personal_tier2" then var_search_criteria = var_search_criteria & "&ptw_frm_srch_comp=" & rec_tier2
        'if len(rec_tier3) > 0 and rec_tier3 <> "%" and rec_tier3 <> "personal_tier3" then var_search_criteria = var_search_criteria & "&ptw_frm_srch_loc=" & rec_tier3

               
        'show future records
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Future Dated Permits</div><div class=""dots""><div class=""statistic_value"">" & var_Future & "</div></div><div class=""clear""></div></div>"
            
        'show expired projects
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Expired Permits</div><div class=""dots""><div class=""statistic_value"">" & var_Expired & "</div></div><div class=""clear""></div></div>"
            
        'show complete active
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Active Permits</div><div class=""dots""><div class=""statistic_value"">" & var_Active & "</div></div><div class=""clear""></div></div>"
                
        'show incomplete future
        var_search_criteria_incomp = var_search_criteria & "ptw_frm_srch_showop=Incomplete"
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_incomp & """ target=""_parent"">Incomplete Permits</a></div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Incomplete Permits</div><div class=""dots""><div class=""statistic_value"">" & var_Incomplete & "</div></div><div class=""clear""></div></div>"
          
        'show complete awaiting authorisation
        var_search_criteria_awaiting = var_search_criteria & "ptw_frm_srch_showop=Awaiting Authorisation"
        'var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""" & var_search_criteria_awaiting & """ target=""_parent"">Permits Awaiting Authorisation</a></div><div class=""dots""><div class=""statistic_value"">" & var_Waiting & "</div></div><div class=""clear""></div></div>"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Permits Awaiting Authorisation</div><div class=""dots""><div class=""statistic_value"">" & var_Waiting & "</div></div><div class=""clear""></div></div>"

        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if


    elseif mod_code = "DSE" then
        
        objcommand.commandtext = "select auto_email from " & Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Training_pref where corp_code='" & session("corp_code") & "' and active = 1"
        set objrs = objcommand.execute

        if objrs.eof then
            auto_email = True
        else
            auto_email = objrs("auto_email")
        end if 
                
        '"Run search to determine wether or not this user is assigned as a reviewer in a group 
        objcommand.commandtext = "select b.group_name, b.recordreference from " & Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_dse_reviewers as a " & _
                                    "left join " & Application("CONFIG_DBASE")& "_HR.dbo.HR_Data_dse_groups as b on a.corp_code = b.corp_code and a.dse_group_code = b.recordreference " & _
                                    "where a.corp_code='" & session("corp_code") & "' and a.user_code='" & session("YOUR_ACCOUNT_ID") & "'"
        set objrsdse = objcommand.execute

        if not objrsdse.eof then
            'produce list of groups
            var_dse_stats_mygrpids = "''" & objrsdse("recordreference") & "''"
            objrsdse.movenext
            while not objrsdse.eof
                var_dse_stats_mygrpids = var_dse_stats_mygrpids & ",''" & objrsdse("recordreference") & "''"
                objrsdse.movenext
            wend
            sql_tag =  " and userdata.dse_grp_ref  in (" & var_dse_stats_mygrpids & ")"
        else
            if mid(session("YOUR_LICENCE"),20,1) = "1" then
                sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "'' and userdata.corp_bd=''" & session("YOUR_DEPARTMENT") & "''"
                var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION") & "&dse_frm_s1_dept=" & session("YOUR_DEPARTMENT")
            elseif mid(session("YOUR_LICENCE"),19,1) = "1" then
                sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "''"
                var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION")
            elseif mid(session("YOUR_LICENCE"),22,1) = "1" then
                sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "''"
                var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY")
            end if
        end if

            
        objcommand.commandtext = "exec MODULE_DSE.dbo.DSE_Console_Stats  '" & sql_tag & "' ,'" & session("corp_code") & "'"
        set objrs = objcommand.execute
                
'        if not objrs.eof then
'            var_dse_1 = objrs("mycount")  
'            set objrs = objrs.nextrecordset
'            if not objrs.eof then
'                var_dse_2 = objrs("mycount")    
'                if auto_email then
'                    set objrs = objrs.nextrecordset
'                    if not objrs.eof then
'                        var_dse_3 = objrs("mycount")                              
'                    else
'                        var_dse_3 = "Error"
'                    end if  
'                else
'                        
'                end if 
'            else
'                var_dse_2 = "Error"
'                var_dse_3 = "Error"
'            end if                    
'        else
'            var_dse_1 = "Error"
'            var_dse_2 = "Error"
'            var_dse_3 = "Error"
'        end if 

        if not objRs.EOF then
            var_Total = objRs("countTotal")
        else
            var_Total = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_AwaitingReview = objRs("countAwaitingReview")
        else
            var_AwaitingReview = "Err"
        end if
        set objRs = objRs.NextRecordset

        if not objRs.EOF then
            var_NotComplete = objRs("countNotComplete")
        else
            var_NotComplete = "Err"
        end if
        set objRs = nothing

               
        'show total users
        if mid(session("YOUR_LICENCE"),10,1) = "1" then
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&search_but=Search for account" & var_search_criteria & """ target=""_parent"">DSE Users in Your Area</a></div><div class=""dots""><div class=""statistic_value"">" & var_Total & "</div></div><div class=""clear""></div></div>"
        else
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">DSE Users in Your Area</div><div class=""dots""><div class=""statistic_value"">" & var_dse_1 & "</div></div><div class=""clear""></div></div>"
        end if
                 
        'show users with assessments awaiting review
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&srch_dsestatus=5&search_but=Search for account" & var_search_criteria & """ target=""_parent"">Assessments Awaiting Review</a></div><div class=""dots""><div class=""statistic_value"">" & var_AwaitingReview & "</div></div><div class=""clear""></div></div>"
                 

        if auto_email then
            'show users who haven""t completed the assessment despite receiving 3 emails
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&frm_dse_email_dis=Y&search_but=Search for account" & var_search_criteria & """ target=""_parent"">User`s who haven`t completed an assessment despite 3 emails being sent</a></div><div class=""dots""><div class=""statistic_value"">" & var_NotComplete & "</div></div><div class=""clear""></div></div>"
        end if


        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if


    elseif mod_code = "ACC" then 
         
        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_ACC.dbo.ACC_Console_Stats '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "', '" & Session("acc_incident_classification_permissions") & "'"
        'if UCASE(session("YOUR_USERNAME")) = "SYSADMIN.BBC" then
        '    response.write objcommand.commandtext
        'end if
        'response.write objcommand.commandtext
        set objrs = objcommand.execute


        if not objRs.EOF then
            var_acc_1 = objRs("mycount")
        else
            var_acc_1 = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_acc_2 = objRs("mycount")
        else
            var_acc_2 = "Err"
        end if

        set objRs = objRs.NextRecordset
        if not objRs.EOF then
            var_acc_3 = objRs("mycount")
        else
            var_acc_3 = "Err"
        end if

        set objRs= objRs.NextRecordset
        if not objRs.EOF then
            var_acc_4 = objRs("mycount")
        else
            var_acc_4 = "Err"
        end if
        set objRs = objRs.NextRecordset
    
        if not objRs.EOF then
            var_acc_5 = objRs("mycount")
        else
            var_acc_5 = "Err"
        end if
        set objRs = nothing
        

           
        'show current year records
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_dateop=4&acc_frm_srch_date1_d=01&acc_frm_srch_date1_m=01&acc_frm_srch_date1_y=" & year(now()) & "&acc_frm_srch_date2_d=31&acc_frm_srch_date2_m=12&acc_frm_srch_date2_y=" & year(now()) & "&acc_frm_srch_acc_lta_type=any&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">Total Incidents for " & year(date()) & "</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_1 & "</div></div><div class=""clear""></div></div>"
            
                 
        'show incomplete records
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_vtype=incomp&acc_frm_srch_acc_lta_type=any&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">Incomplete Incidents</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_2 & "</div></div><div class=""clear""></div></div>"
            
        'show overdue riddors
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=6&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_acc_lta_type=any&acc_frm_srch_riddor_adv=Y&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">Incomplete RIDDORs</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_3 & "</div></div><div class=""clear""></div></div>"   '<a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=6&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">
                
        'show overdue investigations
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Incomplete Investigations</div><div class=""dots""><div class=""statistic_value"">" & var_acc_4 & "</span></div></div><div class=""clear""></div></div>"    '<a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=7&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">
      
        'show portal temporary records
        if len(session("MODULE_PORTAL")) > 0 then
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_vtype=temp&acc_frm_srch_acc_lta_type=any&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" target=""_parent"">Incomplete Portal Records</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_5 & "</span></div></div><div class=""clear""></div></div>"
        end if
      
        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if
             
                  
               
    elseif mod_code = "MS" then

    if Session("MODULE_MS") = "2"  then 
         'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_MS.dbo.MS_Console_Stats2 '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
        set objrs = objcommand.execute
                
        if not objrs.eof then
            var_ms_1 = objrs("mycount")  
            set objrs = objrs.nextrecordset
            if not objrs.eof then
                var_ms_2 = objrs("mycount")  
                set objrs = objrs.nextrecordset
                if not objrs.eof then
                    var_ms_3 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ms_4 = objrs("mycount")  
                    else
                        var_ms_4 = "Error"
                    end if
                else
                    var_ms_3 = "Error"
                    var_ms_4 = "Error"
                end if
            else
                var_ms_2 = "Error"
                var_ms_3 = "Error"
                var_ms_4 = "Error"
            end if                    
        else
            var_ms_1 = "Error"
            var_ms_2 = "Error"
            var_ms_3 = "Error"
            var_ms_4 = "Error"
        end if 
               
        'show incomplete records
        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ms_frm_srch_showop=Incomplete" else  var_search_criteria = "vtype=user&ms_frm_srch_showop=Incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/method_statement_v2/app/mstatement_search.asp?" & var_search_criteria & "&ms_frm_srch_submit=Search for statement"" target=""_parent"">Incomplete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_1 & "</div></div><div class=""clear""></div></div>"
            
        'show complete projects
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/method_statement_v2/app/mstatement_search.asp?ms_frm_srch_showop=Complete&ms_frm_srch_submit=Search for statement"" target=""_parent"">Complete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_2 & "</div></div><div class=""clear""></div></div>"
            
        'show complete current
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Projects Currently Active</div><div class=""dots""><div class=""statistic_value"">" & var_ms_3 & "</div></div><div class=""clear""></div></div>"
                
        'show complete future
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Future Projects</div><div class=""dots""><div class=""statistic_value"">" & var_ms_4 & "</div></div><div class=""clear""></div></div>"
          
        
        
        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if

    else
        'Grab data from DB via SP
        objcommand.commandtext = "exec MODULE_MS.dbo.MS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
        set objrs = objcommand.execute
                
        if not objrs.eof then
            var_ms_1 = objrs("mycount")  
            set objrs = objrs.nextrecordset
            if not objrs.eof then
                var_ms_2 = objrs("mycount")  
                set objrs = objrs.nextrecordset
                if not objrs.eof then
                    var_ms_3 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ms_4 = objrs("mycount")  
                    else
                        var_ms_4 = "Error"
                    end if
                else
                    var_ms_3 = "Error"
                    var_ms_4 = "Error"
                end if
            else
                var_ms_2 = "Error"
                var_ms_3 = "Error"
                var_ms_4 = "Error"
            end if                    
        else
            var_ms_1 = "Error"
            var_ms_2 = "Error"
            var_ms_3 = "Error"
            var_ms_4 = "Error"
        end if 
               
        'show incomplete records
        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ms_frm_srch_showop=Incomplete" else  var_search_criteria = "vtype=user&ms_frm_srch_showop=Incomplete"
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?" & var_search_criteria & "&ms_frm_srch_submit=Search for statement"" target=""_parent"">Incomplete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_1 & "</div></div><div class=""clear""></div></div>"
            
        'show complete projects
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?ms_frm_srch_showop=Complete&ms_frm_srch_submit=Search for statement"" target=""_parent"">Complete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_2 & "</div></div><div class=""clear""></div></div>"
            
        'show complete current
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Projects Currently Active</div><div class=""dots""><div class=""statistic_value"">" & var_ms_3 & "</div></div><div class=""clear""></div></div>"
                
        'show complete future
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Future Projects</div><div class=""dots""><div class=""statistic_value"">" & var_ms_4 & "</div></div><div class=""clear""></div></div>"
          
        
        
        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if
      end if 
      
                 
          
    elseif mod_code = "INS" then
        objcommand.commandtext = "exec MODULE_INS.dbo.INS_Console_Stats  '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
        set objrs = objcommand.execute
                
        if not objrs.eof then
            var_ins_1 = objrs("mycount")  
            set objrs = objrs.nextrecordset
            if not objrs.eof then
                var_ins_2 = objrs("mycount")    
                      
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ins_3 = objrs("mycount")
                    else
                        var_ins_3 = "Error"
                    end if  
            else
                var_ins_2 = "Error"
                var_ins_3 = "Error"
            end if                    
        else
            var_ins_1 = "Error"
            var_ins_2 = "Error"
            var_ins_3 = "Error"
        end if 
               
        'show complete
        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?ins_frm_srch_showop=Complete&ins_srch_type_flag=A&submit_but=Search for inspection(s)"" target=""_parent"">Complete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_1 & "</div></div><div class=""clear""></div></div>"
        

        if Session("safety_inspection_remove_lock") = "Y" Then
            var_lock_text = ""
        else
            var_lock_text = " (Locked) "
        end if

        'show tasks allocated
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Complete Inspections with Tasks Assigned " & var_lock_text & "</div><div class=""dots""><div class=""statistic_value"">" & var_ins_2 & "</div></div><div class=""clear""></div></div>"
                                  
        'show incompletes
        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A" else var_search_criteria = "vtype=user&ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A"
            var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?" & var_search_criteria & "&submit_but=Search for inspection(s)"" target=""_parent"">Incomplete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_3 & "</div></div><div class=""clear""></div></div>"


        if session("HPSTATS_CHANGEABLE") = "Y" then
            var_scrolling_box_content = var_scrolling_box_content & "*this information is unaffected by the department dropdown list"
        end if
          
                
          
    end if

    var_scrolling_box_content = var_scrolling_box_content & "</td></tr></table></div></div>"


    if mod_code = "TM" then
        public_tm_content = public_tm_content & var_scrolling_box_content & "<hr />"
    else
        public_scrolling_content = public_scrolling_content & "pausecontent[" & mycount & "]= '" & var_scrolling_box_content & "' " &vbcrlf
        public_first_load_content = public_first_load_content & var_scrolling_box_content & "<br />"
    end if

      
end sub







sub console_dropdown(view_access)


    response.Write "<option value='na'>All " & session("CORP_TIER3_PLURAL") & " you have access to</option>"
    response.Write "<option value='na'>----------------------------</option>"


    if view_access = "0" then
        objCommand.commandText = "SELECT struct_name, tier2_ref FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "ORDER BY struct_name"
    else
        objCommand.commandText = "SELECT struct_name, tier2_ref FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND tier2_ref IN (SELECT corp_bu FROM Module_HR.dbo.HR_Data_users_structure WHERE corp_code = '" & session("CORP_CODE") & "' AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' UNION SELECT corp_bu FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" & session("CORP_CODE") & "' AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') " & _
                                 "ORDER BY struct_name"
    end if
    set objRs = objCommand.execute
    if not objRs.EOF then
    
        while not objRs.EOF
            if session("STATS_TIER_LEVEL") = "T2/" & objRs("tier2_ref") then var_selected = "selected" else var_selected = ""

            response.Write "<option value='T2/" & objRs("tier2_ref") & "' " & var_selected & ">" & objRs("struct_name") & "</option>"

            if view_access = "0" then
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier2_ref = '" & objRs("tier2_ref") & "' " & _
                                         "ORDER BY struct_name"
            else            
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier2_ref = '" & objRs("tier2_ref") & "' " & _
                                         "  AND tier3_ref IN (SELECT corp_bl FROM Module_HR.dbo.HR_Data_users_structure WHERE corp_code = '" & session("CORP_CODE") & "' AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' UNION SELECT corp_bl FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" & session("CORP_CODE") & "' AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "') " & _
                                         "ORDER BY struct_name"
            end if
            set objRs1 = objCommand.execute
            if not objRs1.EOF then
                while not objRs1.EOF
                    if session("STATS_TIER_LEVEL") = "T3/" & objRs("tier2_ref") & "_" & objRs1("tier3_ref") then var_selected = "selected" else var_selected = ""

                    response.Write "<option value='T3/" & objRs("tier2_ref") & "_" & objRs1("tier3_ref") & "' " & var_selected & ">&nbsp;&nbsp;&nbsp;&nbsp;" & objRs1("struct_name") & "</option>"
                    objRs1.movenext
                wend
            end if
            
            objRs.movenext
        wend
    end if

end sub














'*****************************************************************************************************************
'*****************************************************************************************************************
'
'   Functions for use by the Detailed Statistics homepages stats (non-scrolling, dropdown list enabled) IKEA
'
'*****************************************************************************************************************
'*****************************************************************************************************************

function output_sub_stats(objRs2, current_tier_ref, div_id)
    dim sub_stats_text
    
    sub_stats_text = "<div class='r'><a id='exp" & current_tier_ref & "' href='javascript:toggleHC(" & current_tier_ref & ")'>"
        if cstr(div_id) = cstr(current_tier_ref) then sub_stats_text = sub_stats_text & "collapse</a></div>" else sub_stats_text = sub_stats_text & "expand</a></div>" end if
        
    sub_stats_text = sub_stats_text & "<div id='" & current_tier_ref & "' style='padding:0 0 20px 20px; " 
        if cstr(div_id) = cstr(current_tier_ref) then sub_stats_text = sub_stats_text & "display:block;'>" else sub_stats_text = sub_stats_text & "display:none;'>" end if

    current_location_name = ""
    while not objRs2.EOF
        if cstr(objRs2("tier2_ref")) <> cstr(current_tier_ref) then
            objRs2.movenext
        else
            if current_location_name <> objRs2("Struct_name") then
                current_location_name = objRs2("Struct_name")
                sub_stats_text = sub_stats_text & "<a href='#' id='" & current_location_name & current_tier_ref & "'></a><h3 style='padding-top:4px;'>" & current_location_name & "</h3>"
            end if

            select case objRs2("resulttype")
                case "Total"
                    var_search_criteria = "ra_frm_srch_bu=" & objRs2("tier2_ref") & "&ra_frm_srch_bl=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                case "Incomplete"
                    var_search_criteria = "ra_frm_srch_bu=" & objRs2("tier2_ref") & "&ra_frm_srch_bl=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                Case "Copied"
                    var_search_criteria = "ra_frm_srch_bu=" & objRs2("tier2_ref") & "&ra_frm_srch_bl=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                case "Complete"
                    var_search_criteria = "ra_frm_srch_bu=" & objRs2("tier2_ref") & "&ra_frm_srch_bl=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
            end select
            
            if objRs2("resulttype") = "Copied" and objRs2("numrecords") = 0 then
                'do nothing
            else
                sub_stats_text = sub_stats_text & "<div class='statistic_holder' style='padding-left:10px;'><div class='statistic_desc'><a href='../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target=""_parent"">" & objRs2("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs2("numrecords") & "</div></div><div class='clear'></div></div>"
            end if
            objRs2.movenext
        end if
    wend

    sub_stats_text = sub_stats_text & "</div>"
    
    output_sub_stats = sub_stats_text
end function



sub console_dropdown_hp(view_access)

    if session("fully_global") = "Y" then
        var_company = "%"
    else
        var_company = session("YOUR_COMPANY")
    end if
    
    if view_access = "0" then
        var_location = "%"
    else    
        var_location = session("YOUR_LOCATION")
    end if

    objCommand.commandText = "SELECT struct_name, tier2_ref FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND tier2_ref LIKE '" & var_company & "' " & _
                             "ORDER BY struct_name"
    set objRs = objCommand.execute
    if not objRs.EOF then
    
        while not objRs.EOF
            response.Write "<option value='na'>&nbsp</option>"
            response.Write "<option value='" & objRs("tier2_ref") & "'>" & objRs("struct_name") & "</option>"
            
            if var_location = "%" then
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier2_ref = " & objRs("tier2_ref") & " " & _
                                         "ORDER BY struct_name"
            else
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier3_ref = " & session("YOUR_LOCATION") & " " & _
                                         "ORDER BY struct_name"
            end if        
            response.Write "..." & objCommand.commandText & "..."        
            set objRs1 = objCommand.execute
            if not objRs1.EOF then
                while not objRs1.EOF
                    response.Write "<option value='" & objRs("tier2_ref") & "'>&nbsp;&nbsp;&nbsp;&nbsp;" & objRs1("struct_name") & "</option>"
                    objRs1.movenext
                wend
            end if
            
            objRs.movenext
        wend
    end if

end sub

sub populate_module_detailed(mod_code, view_access, mycount, div_id, sub_sw5)
    
    'mod_code = module
    '"view_access = access level you wish to view
    'mycount = value for the position of the java script array
    'div_id = used for detailed statistics (ikea) - expands the selected hidden div when jumping to specific anchor
    'sub_sw5 = Empty


    response.write "<div id=""stats_mod_" & mod_code & """ class=""stats_module"" >" & _
                            "<div class=""stats_module_inner"">" & _
                                "<h1>" & mods.Item(mod_code) &  "</h1>" & _
                                    "<table class=""stats_content"">" & _
                    "<tr><td class=""stats_content_left""><i class=""fa fa-tasks"" aria-hidden=""true""></i></td><td class=""stats_content_right"">" 

    if view_access = "0" then
        var_location = "%"
    else    
        var_location = session("YOUR_LOCATION")
    end if
    response.end
    
    if mod_code = "RA" then  
        ' FULLY GLOBAL - This is a session variable to go in the preferences table. Currently it doesn't exist.
        ' It should be assigned to a specific user, and allows them to report stats for all companies in the contract, not just the one they're 
        ' assigned too in user manager.
        ' FULLY GLOBAL is also referenced in the SUB (console_dropdown)
        
        if session("fully_global") = "Y" then
            objCommand.commandText = "EXEC MODULE_RA.dbo.RA_Console_Stats_Detailed '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("CORP_CODE") & "', '%', '%', '" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"
        else
            objCommand.commandText = "EXEC MODULE_RA.dbo.RA_Console_Stats_Detailed '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("CORP_CODE") & "', '" & session("YOUR_COMPANY") & "', '" & var_location & "', '" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"
    end if 
        set objRs1 = objCommand.execute
        
        if view_access = "0" then
            if not objRs1.EOF then
                current_tier2_ref = "0"
                while not objRs1.EOF
                    
                    if cstr(current_tier2_ref) <> cstr(objRs1("tier2_ref")) then
                        if view_access = "0" and current_tier2_ref <> "0" then
                            set objRs2 = objCommand.execute
                            set objRs2 = objRs2.nextRecordset
                            response.write output_sub_stats(objRs2, current_tier2_ref, div_id)
                        end if
                        
                        response.Write "<a href='#' id='" & objRs1("struct_name") & objRs1("tier2_ref") & "'></a><h3>" & objRs1("struct_name") & "</h3>"
                        current_tier2_ref = objRs1("tier2_ref")
                    end if
                    
                    select case objRs1("resulttype")
                        case "Total"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                        case "Incomplete"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                        case "Copied"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                        case "Complete"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
                    end select
                    
                    if objRs1("resulttype") = "Copied" and objRs1("numrecords") = 0 then
                        'do nothing
                    else
                        response.Write "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target='_parent'>" & objRs1("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs1("numrecords") & "</div></div><div class='clear'></div></div>"
                    end if
                    objRs1.movenext
                wend
                
            end if

            set objRs2 = objCommand.execute
            set objRs2 = objRs2.nextRecordset
            response.write output_sub_stats(objRs2, current_tier2_ref, div_id)
            
        elseif view_access = "1" then
            if not objRs1.EOF then
                current_tier3_ref = "0"
                while not objRs1.EOF
                    
                    if cstr(current_tier3_ref) <> cstr(objRs1("tier3_ref")) then
                        response.Write "<a href='#' id='" & objRs1("struct_name") & objRs1("tier3_ref") & "'></a><h3>" & objRs1("struct_name") & "</h3>"
                        current_tier3_ref = objRs1("tier3_ref")
                    end if
                    
                    select case objRs1("resulttype")
                        case "Total"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_bl=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                        case "Incomplete"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_bl=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                        case "Copied"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_bl=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                        case "Complete"
                            var_search_criteria = "ra_frm_srch_bu=" & objRs1("tier2_ref") & "&ra_frm_srch_bl=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
                    end select
                    
                    if objRs1("resulttype") = "Copied" and objRs1("numrecords") = 0 then
                        'do nothing
                    else
                        response.Write "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target=""_parent"">" & objRs1("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs1("numrecords") & "</div></div><div class='clear'></div></div>"
                    end if
                    objRs1.movenext
                wend
                
            end if        
        
        else
            if not objRs1.EOF then
                var_ra_1 = objRs1("mycount")  
                set objRs1 = objRs1.nextRecordset
                if not objRs1.eof then
                    var_ra_2 = objrs1("mycount")  
                else
                    var_ra_2 = "Error"
                end if                    
            else
                var_ra_1 = "Error"
                var_ra_2 = "Error"
            end if
           
            'show incomplete records
            if session("YOUR_ACCESS") = "0" then var_search_criteria = "incomps=n" else  var_search_criteria = "vtype=user"
            response.Write  "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&comps=n&ra_frm_srch_submit=Search for assessment' target='parent'>Incomplete Assessments</a></div><div class='dots'><div class='statistic_value'>" & var_ra_1 & "</div></div><div class='clear'></div></div>"
        
            'show overdue records
            if session("YOUR_ACCESS") = "1" then 
                var_search_criteria = "ra_frm_srch_bu=" & session("YOUR_COMPANY") & "&" 
            elseif session("YOUR_ACCESS") = "2" then 
              var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&" 
            end if
            response.Write  "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment' target='parent'>Overdue Assessments</a></div><div class='dots'><div class='statistic_value'>" & var_ra_2 & "</div></div><div class='clear'></div></div>"
           
        end if

    end if
        
    response.Write  "</td></tr></table></div></div><hr />"

end sub


%>