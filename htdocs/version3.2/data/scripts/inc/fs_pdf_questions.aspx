<%

alarm_questions(0) = ""
alarm_questions(1) = "Are the means above sufficient to alert all occupants in the event of a fire?"
alarm_questions(2) = "Are all occupants aware of the alert systems?"

moe_questions(0) = ""
moe_questions(1) = "Are all final fire exits fully operational and free of obstruction/defect?"
moe_questions(2) = "Are all internal fire exits fully operational and free of obstruction/defect?"
moe_questions(3) = "Do all Automatic Fire Doors operate correctly and doors close correctly?"
moe_questions(4) = "Are all maintenance doors locked and is correct fire safety signage attached?"
moe_questions(5) = "Are all doors locked or fastened so as to not cause a problem?"
moe_questions(6) = "Are all fire doors and exits maintained and inspected, with formal records maintained?"
moe_questions(7) = "Are all fire routes clear of obstruction, defects and combustible material?"
moe_questions(8) = "Do all fire doors and protected routes meet current Building Regulations (Approved Document B)?"
moe_questions(9) = "Are the premises free from any structural alterations which would be detrimental to escape? Have they been validated in the current fire certificate?"
moe_questions(10) = "If electrical locking systems are provided, do they fail safe open when the fire alarm is activated?"
moe_questions(11) = "Are all security access system (e.g. key card or swipe card), free from causing and escape issue in an emergency?"
moe_questions(12) = "Are all other locking systems free from causing an escape issue in an emergency?"
moe_questions(13) = "Are safe refuges provided as means of escape?"
moe_questions(14) = "Are any refuges clearly marked on the current fire certificate?"
moe_questions(15) = "Is there a good management system for mobility impaired?"


ffe_equipment(0) = "Auto-suppresion system (FM200 / CO2)"
ffe_equipment(1) = "Auto-suppresion system (Foam)"
ffe_equipment(2) = "Auto-suppresion system (Powder)"
ffe_equipment(3) = "Auto-suppresion system (Water)"        
ffe_equipment(4) = "Blanket (light-duty)"
ffe_equipment(5) = "Blanket (heavy-duty)"
ffe_equipment(6) = "Sprinkler system"
ffe_equipment(7) = "Extinguisher (CO2)"
ffe_equipment(8) = "Extinguisher (Foam)"
ffe_equipment(9) = "Extinguisher (Powder)"
ffe_equipment(10) = "Extinguisher (Water)"
ffe_equipment(11) = "Hose reel"
ffe_equipment(12) = "Fire bucket (sand)"


ovs_questions(0) = ""
ovs_questions(1) = "Do any piped air or oxygen systems that feed the room isolate when the Fire Alarm activates?"
ovs_questions(2) = "Are manual Isolation switches provided (Firemans Switch), if so do they operate and are they accessible?"
ovs_questions(3) = "Do the Air Conditioning/Air Circulating/Fresh Air Fans systems isolate when the Fire Alarm activates?"


ffe_questions(0) = ""
ffe_questions(1) = "Is the fire fighting equipment adequate for the risks present?"
ffe_questions(2) = "Is all fire fighting equipment sited correctly?"
ffe_questions(3) = "Does all fire fighting equipment have the correct signage?"
ffe_questions(4) = "Are fire routes or fire doors free from obstruction / risk from fire fighting equipment?"
ffe_questions(5) = "If present, are extinguishers on brackets or stands?"


train_questions(0) = ""
train_questions(1) = "Are all staff given induction training and instruction on means of escape?"
train_questions(2) = "If required, are staff trained to use the fire fighting equipment provided and communicated to the workforce?"
train_questions(3) = "Is a No Smoking policy enforced?"
train_questions(4) = "Is the area free from any evidence of smoking in unauthorised areas?"
train_questions(5) = "Are receptacles in place for correct disposal of smoking materials?"


mntc_questions(0) = ""
mntc_questions(1) = "Is there a weekly inspection of all fire fighting equipment and a formal record maintained?"
mntc_questions(2) = "Is an annual check/test undertaken?"
mntc_questions(3) = "Are checks done by a competent person?"
mntc_questions(4) = "Is the fire alarm tested weekly?"
mntc_questions(5) = "Is the alarm tested quarterly and annually, or as described within the current Fire Certificate?"
mntc_questions(6) = "Are formal records maintained for weekly test of the fire alarm?"
mntc_questions(7) = "Are all fire detection systems periodically tested?"
mntc_questions(8) = "Is all emergency lighting periodically tested?"
mntc_questions(9) = "Are all relevant appliances PAT tested?"
mntc_questions(10) = "Is housekeeping in good order?"
mntc_questions(11) = "Is waste removed from the premises on a daily basis?"
mntc_questions(12) = "Are external waste bins stored away from the building and clear of emergency routes?"
mntc_questions(13) = "Are waste areas free from being a possible target for arson?"


proc_questions(0) = ""
proc_questions(1) = "Does the policy incorporate fire safety?"
proc_questions(2) = "Does the policy identify a person to take overall responsibility in a fire evacuation?"
proc_questions(3) = "Does the policy identify a fire warden's and other key personnel's duties in the event of a fire evacuation?"
proc_questions(4) = "Does the policy identify how the emergency services are contacted in the event of an emergency?"
proc_questions(5) = "Is there an Emergency Plan in place and is it up to date?"
proc_questions(6) = "Have all key staff identified in the policy received formal training to allow their responsibilities to be carried out with respect to fire safety and in the event of an emergency?"
proc_questions(7) = "Is the assembly point sited and signed correctly?"
proc_questions(8) = "Are Emergency procedures posted throughout the workplace and completed correctly?"
proc_questions(9) = "Is there a procedure of method of informing visitors of emergency procedures?"
proc_questions(10) = "Are Contractors controlled when working on premises?"


log_questions(0) = ""
log_questions(1) = "Are periodical Fire Alarm tests recorded in the fire log book?"
log_questions(2) = "Are periodical Emergency Lighting tests recorded in the fire log book?"
log_questions(3) = "Are periodical inspection of Fire Fighting Equipment recorded in the fire log book?"
log_questions(4) = "Are periodical tests of fire detection systems (e.g. smoke detectors) recorded in the fire log book?"
log_questions(5) = "Are staff fire training records recorded in the fire log book?"
log_questions(6) = "Are fire drills and actions for non-compliance identified during drills recorded in the fire log book?"
%>