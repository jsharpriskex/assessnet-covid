<!-- #INCLUDE FILE="../../../../data/scripts/inc/auto_logout.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/autogen_datetime.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../../../data/scripts/inc/autogen_menus.asp" -->
<% 

dim objrs
dim objconn
dim objcommand

'variables
var_acc_frm_srch_submit = request("acc_frm_srch_submit")
var_acc_frm_srch_grpby = request("acc_frm_srch_grpby")
var_acc_frm_srch_as_accb = request("acc_frm_srch_as_accb")
var_acc_frm_srch_as_grp = request("acc_frm_srch_as_grp")
var_acc_frm_srch_bd_accb = request("acc_frm_srch_bd_accb")
var_acc_frm_srch_bd_grp = request("acc_frm_srch_bd_grp")
var_acc_frm_srch_keyword = replace(request("acc_frm_srch_keyword"),"'","`")
var_acc_frm_inc_type = request("acc_frm_inc_type")
var_acc_frm_srch_hc_accb = replace(request("acc_frm_srch_hc_accb"),"'","`")
var_acc_frm_srch_as_ref = replace(request("acc_frm_srch_as_ref"),"'","`")
var_acc_frm_srch_hc_ref = replace(request("acc_frm_srch_hc_ref"),"'","`")
var_acc_frm_srch_fname = replace(request("acc_frm_srch_fname"),"'","`")
var_acc_frm_srch_sname = replace(request("acc_frm_srch_sname"),"'","`")
var_acc_frm_srch_sop = request("acc_frm_srch_sop")
var_acc_frm_srch_eby = request("acc_frm_srch_eby")
var_acc_frm_srch_order = request("acc_frm_srch_order")
var_acc_frm_srch_dateop = request("acc_frm_srch_dateop") 
var_acc_frm_srch_status = request("acc_frm_srch_status")
var_acc_frm_srch_gender = request("acc_frm_srch_gender")
var_acc_frm_srch_inj_type = request("acc_frm_srch_inj_type")
var_acc_frm_srch_inj_cause = request("acc_frm_srch_inj_cause")
var_acc_frm_srch_inj_body = request("acc_frm_srch_inj_body")
var_acc_frm_srch_adv = request("acc_frm_srch_adv")
var_acc_frm_srch_date1_d  = request("acc_frm_srch_date1_d")
var_acc_frm_srch_date1_m = request("acc_frm_srch_date1_m") 
var_acc_frm_srch_date1_y = request("acc_frm_srch_date1_y")  
var_acc_frm_srch_date2_d = request("acc_frm_srch_date2_d") 
var_acc_frm_srch_date2_m = request("acc_frm_srch_date2_m") 
var_acc_frm_srch_date2_y = request("acc_frm_srch_date2_y")  
var_acc_frm_srch_vtype =  request("vtype") 

var_page = request("Page")
if var_page > 0 then

else
var_page = 1
end if

'defaults
if request("acc_frm_srch_date1_d") = "DD" or request("acc_frm_srch_date1_m" = "MM") or request("acc_frm_srch_date1_y" = "YYYY") or len(request("acc_frm_srch_date1_d")) =0 then
    var_acc_frm_srch_date1 = "NONE"
else
    var_acc_frm_srch_date1 = request("acc_frm_srch_date1_d") & "/" & request("acc_frm_srch_date1_m") & "/" & request("acc_frm_srch_date1_y")
end if

if request("acc_frm_srch_date2_d") = "DD" or request("acc_frm_srch_date2_m") = "MM" or request("acc_frm_srch_date2_y" = "YYYY")  or len(request("acc_frm_srch_date2_d")) =0 then
    var_acc_frm_srch_date2 = "NONE"
else
    var_acc_frm_srch_date2 = request("acc_frm_srch_date2_d") & "/" & request("acc_frm_srch_date2_m") & "/" & request("acc_frm_srch_date2_y")
end if
if len(var_acc_frm_srch_adv) = 0 then
    var_acc_frm_srch_adv = 0
end if


Redirect_string =   "acc_frm_srch_grpby=" & var_acc_frm_srch_grpby &_ 
                    "&acc_frm_srch_as_accb=" & var_acc_frm_srch_as_accb &_  
                    "&acc_frm_srch_as_grp=" & var_acc_frm_srch_as_grp &_  
                    "&acc_frm_srch_bd=" & var_acc_frm_srch_bd &_  
                    "&acc_frm_srch_keyword=" & var_acc_frm_srch_keyword &_  
                    "&acc_frm_inc_type=" & var_acc_frm_inc_type &_  
                    "&acc_frm_srch_hc_accb=" & var_acc_frm_srch_hc_accb &_  
                    "&acc_frm_srch_as_ref=" & var_acc_frm_srch_as_ref &_  
                    "&acc_frm_srch_hc_ref=" & var_acc_frm_srch_hc_ref &_ 
                    "&acc_frm_srch_fname=" & var_acc_frm_srch_fname &_  
                    "&acc_frm_srch_sname=" & var_acc_frm_srch_sname &_  
                    "&acc_frm_srch_sop=" & var_acc_frm_srch_sop &_  
                    "&acc_frm_srch_eby=" & var_acc_frm_srch_eby &_ 
                    "&acc_frm_srch_order=" & var_acc_frm_srch_order &_ 
                    "&acc_frm_srch_dateop=" &  var_acc_frm_srch_dateop &_
                    "&acc_frm_srch_date1_d=" &  var_acc_frm_srch_date1_d &_
                    "&acc_frm_srch_date1_m=" &  var_acc_frm_srch_date1_m &_
                    "&acc_frm_srch_date1_y=" &  var_acc_frm_srch_date1_y &_
                    "&acc_frm_srch_date2_d=" &  var_acc_frm_srch_date2_d &_
                    "&acc_frm_srch_date2_m=" &  var_acc_frm_srch_date2_m &_
                    "&acc_frm_srch_date2_y=" &  var_acc_frm_srch_date2_y &_
                    "&acc_frm_srch_status=" &  var_acc_frm_srch_status &_
                    "&acc_frm_srch_gender=" &  var_acc_frm_srch_gender &_
                    "&acc_frm_srch_inj_type=" &  var_acc_frm_srch_inj_type &_
                    "&acc_frm_srch_inj_cause=" &  var_acc_frm_srch_inj_cause &_
                    "&acc_frm_srch_inj_body=" &  var_acc_frm_srch_inj_body &_
                    "&acc_frm_srch_adv=" & var_acc_frm_srch_adv & _
                    "&acc_frm_srch_vtype=" & var_acc_frm_srch_vtype & _
                    "&acc_frm_srch_submit=" & var_acc_frm_srch_submit 

'Default values

if len(var_acc_frm_srch_sop) < 1 then var_acc_frm_srch_sop = "recorddate" end if
if len(var_acc_frm_srch_order) < 1 then var_acc_frm_srch_order = "desc" end if

call startconnections()


Dim inctype_array(7)
inctype_array(1) = "an Injury"
inctype_array(2) = "an Illness / Disease"
inctype_array(3) = "a Dangerous Occurrence"
inctype_array(4) = "a Property Damage"
inctype_array(5) = "a Near Miss"
inctype_array(6) = "a Riddor"
inctype_array(7) = "an Investigation"

Dim dateop_arry(4)
dateop_arry(1) = "equal to"
dateop_arry(2) = "before"
dateop_arry(3) = "after"
dateop_arry(4) = "between"

Dim sortop_arry(5)
sortop_arry(1) = "date"
sortop_arry(2) = "reference"
sortop_arry(3) = "No. of Injuries"
sortop_arry(4) = "No. of Illness / Disease"
sortop_arry(5) = "Location / Deparment"

DIM sortopval_arry(5)
sortopval_arry(1) = "acc_core.recorddate"
sortopval_arry(2) = "acc_core.srt_accb x, acc_core.srt_accref"
sortopval_arry(3) = "acc_core.inj_count x, acc_core.srt_accb x, acc_core.srt_accref"
sortopval_arry(4) = "acc_core.id_count x , acc_core.srt_accb x, acc_core.srt_accref"
sortopval_arry(5) = "acc_CORE.REC_INCLOC, acc_CORE.REC_BD"

sub inc_type(sub_sel_input,sub_sel_name)

        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select'>" & _
                        "<option value='0'>any type of incident</option>"
        
        for i = 1 to Ubound(inctype_array)
            Response.Write "<option value='" & i & "' "
                if sub_sel_input = Cstr(i) then response.Write "selected" end if
            Response.Write " >" & inctype_array(i) & "</option>"
        next
        
        Response.Write  "</select>"
end sub

sub dateoperator(sub_sel_input,sub_sel_name)

        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select' onchange='toggle(this);'>" & _
                        "<option value='0'>any</option>"
        
        for i = 1 to Ubound(dateop_arry)
            Response.Write "<option value='" & i & "' "
                if sub_sel_input = Cstr(i) then response.Write "selected" end if
            Response.Write " >" & dateop_arry(i) & "</option>"
        next
        
        Response.Write  "</select>"
end sub

sub sortoperator(sub_sel_input,sub_sel_name)

        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select'>" 
        
        for i = 1 to Ubound(sortop_arry)
            Response.Write "<option value='" & sortopval_arry(i) & "' "
                if sub_sel_input = sortopval_arry(i) then response.Write "selected" end if
            Response.Write " >" & sortop_arry(i) & "</option>"
        next
        
        Response.Write  "</select>"
end sub


sub accblist(sub_sel_input,sub_sel_name,sub_sel_disp)
    
    objCommand.Commandtext = "SELECT ACCB_CODE,REC_BU,REC_BL FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " WHERE REC_BG = '" & Session("YOUR_GROUP") & "' AND Corp_code ='" & Session("Corp_code") & "' ORDER BY ACCB_CODE ASC"
    Set objRs = objCommand.Execute  
   
  
   
        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select' " & sub_sel_disp & "  onchange='submit()'>" 
       
    if not objrs.eof then                  
        response.Write "<option value='ALL'>All Incident Centres</option>"

    	
    	
   	    While NOT objRs.EOF
           If Len(sub_sel_input) > 0 Then
                If sub_sel_input = objRs("ACCB_CODE") Then
                    Response.Write  "<option value='" & objRs("ACCB_CODE") & "' selected='selected'>" & objRs("ACCB_CODE") & " [" & objRs("REC_BU") & "] : (" & objRs("REC_BL") & ")</option>"
                Else
                    Response.Write  "<option value='" & objRs("ACCB_CODE") & "'>" & objRs("ACCB_CODE") & " [" & objRs("REC_BU") & "] : (" & objRs("REC_BL") & ")</option>"
                End if
            Else
                If Session("CORP_ACCBOOK") = objRs("ACCB_CODE") Then
                    Response.Write  "<option value='" & objRs("ACCB_CODE") & "' selected='selected'>" & objRs("ACCB_CODE") & " [" & objRs("REC_BU") & "] : (" & objRs("REC_BL") & ")</option>"
                Else
                    Response.Write  "<option value='" & objRs("ACCB_CODE") & "'>" & objRs("ACCB_CODE") & " [" & objRs("REC_BU") & "] : (" & objRs("REC_BL") & ")</option>"
                End if
            End if
            objRs.MoveNext
	    Wend
   else
       response.Write "<option value='ERR'>No incident centres have been created please contact your global administrator</option>"
   end if 
   
   response.Write "</select>"
end sub



sub grplist(sub_sel_input,sub_sel_name,sub_sel_disp)

   
        objCommand.Commandtext = "SELECT ACCB_GROUP_CODE, ACCB_GROUP_NAME FROM " & Application("DBTABLE_ACC_GROUP") & " WHERE Corp_code ='" & Session("Corp_code") & "' ORDER BY ACCB_GROUP_NAME "
        Set objRs = objCommand.Execute  
    
        
        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select' " & sub_sel_disp & " onchange='submit()'>"
       if not objrs.eof then                  
            response.Write "<option value='ALL'>All Incident Groups</option>"
        
            While NOT objRs.EOF
                If Len(sub_sel_input) > 0 Then
                    If sub_sel_input = objRs("ACCB_GROUP_CODE") Then
                        Response.Write  "<option value='" & objRs("ACCB_GROUP_CODE") & "' selected >" & objrs("ACCB_GROUP_NAME") & "</option>"
                    Else
                        Response.Write  "<option value='" & objRs("ACCB_GROUP_CODE") & "'>" & objrs("ACCB_GROUP_NAME") & "</option>"
                    End if
                else
                 Response.Write  "<option value='" & objRs("ACCB_GROUP_CODE") & "'>" & objrs("ACCB_GROUP_NAME") & "</option>"
                End if
                objRs.MoveNext                                
            Wend
        else
            response.Write "<option value='ERR'>No incident groups have been created please contact your global administrator</option>"
        end if 
   response.Write "</select>"
end sub

  

sub userlist(sub_sel_input,sub_sel_name)

    Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select'>" & _
                    "<option value='any'>any user</option>"

	objCommand.Commandtext = "SELECT Acc_Ref,Per_fname,Per_sname,Corp_Bl,Corp_Bd FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code='" & Session("corp_code")& "' AND Corp_BG='" & Session("YOUR_GROUP") & "' AND Acc_level NOT LIKE '5'  ORDER BY Per_sname ASC"
	SET objRs = objCommand.Execute
	
   	While NOT objRs.EOF
        Response.Write "<option value='" & objRs("Acc_Ref") & "'"
        If objRs("Acc_ref") = sub_sel_input Then
            Response.Write " selected='selected'"
        End if
        Response.Write ">" & Ucase(objRs("Per_sname")) & ", " & objRs("Per_fname") & " (" & objRs("Corp_BD") & ")</option>"
        objRs.MoveNext
	Wend
response.Write "</select>"
end sub


sub departmentList(sub_sel_input,sub_sel_name,sub_sel_type,sub_sel_grp_val)

    Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select'>" & _
                    "<option value='any'>any department</option>"

    if sub_sel_type ="grp" then 
  
        objCommand.CommandText = "SELECT b.BG, b.BU, b.BL, b.BD" & _
        " FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " AS a LEFT OUTER JOIN " & _
        Application("DBTABLE_HR_DATA_STRUCTURE") & " AS b ON a.CORP_CODE = b.Corp_code AND a.REC_BG = b.BG AND a.REC_BU = b.BU AND a.REC_BL = b.BL" & _
        "  WHERE (a.CORP_CODE = '" & session("corp_code") & "')"
    
        if sub_sel_grp_val <> "any" and len(sub_sel_grp_val) > 0 then
            objCommand.CommandText = objCommand.CommandText & " and (a.ACCB_GROUP_CODE = '" & sub_sel_grp_val & "')"
        end if
      '  objCommand.CommandText = "SELECT DISTINCT BD FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE corp_code='" & Session("corp_code")& "' AND BD IS NOT NULL ORDER BY BD ASC"      
    else 
        objCommand.CommandText = "SELECT     b.BG, b.BU, b.BL, b.BD" & _
        " FROM " & Application("DBTABLE_HR_DATA_ABOOK") & " AS a LEFT OUTER JOIN " & _
        Application("DBTABLE_HR_DATA_STRUCTURE") & " AS b ON a.CORP_CODE = b.Corp_code AND a.REC_BG = b.BG AND a.REC_BU = b.BU AND a.REC_BL = b.BL" & _
        "  WHERE     (a.CORP_CODE = '" & session("corp_code") & "')"
        
        if sub_sel_grp_val <> "any" and len(sub_sel_grp_val) > 0 then
            objCommand.CommandText = objCommand.CommandText & " and (a.accb_code = '" & sub_sel_grp_val & "')"
        end if
     'objCommand.CommandText = "SELECT DISTINCT BD FROM " & Application("DBTABLE_HR_DATA_STRUCTURE") & " WHERE corp_code='" & Session("corp_code")& "' AND BD IS NOT NULL ORDER BY BD ASC"      
    end if
    
    Set objRs = objCommand.Execute	
    
    While NOT objRs.EOF
        Response.Write "<option value='" & objRs("BD") & "' "
            if sub_sel_input = objRs("BD") then response.Write "selected" end if
        Response.Write " >" & objRs("BD") & "</option>"
        objRs.MoveNext
    Wend
response.Write "</select>"
end sub




'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' BUILD SEARCH QUERY
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


if len(var_acc_frm_srch_submit) <> 0 then
varSEARCH = "Y"
    
     if var_acc_frm_srch_grpby <> "grp" then
        If var_acc_frm_srch_as_accb <> "ALL" Then
            If Session("YOUR_ACCESS") = "0" Then
                if len(var_acc_frm_srch_as_accb) > 0 then
                searchQuery = searchQuery & " AND " & _ 
                                    "acc_CORE.ACCB_CODE = '" & var_acc_frm_srch_as_accb & "' "
                else
                searchQuery = searchQuery & " AND " & _ 
                                    "acc_CORE.ACCB_CODE = '" & Session("CORP_ACCBOOK") & "' "
                end if
            Else
                searchQuery = searchQuery & " AND " & _ 
                                    "acc_CORE.ACCB_CODE = '" & Session("CORP_ACCBOOK") & "' "
            End if
        End if
        
         If var_acc_frm_srch_bd_grp <> "any"  and len(var_acc_frm_srch_bd_grp) > 0 Then
       searchQuery = searchQuery & "AND " & _ 
                  "acc_CORE.REC_BD = '" & var_acc_frm_srch_bd_grp & "' "
    End if
   else
        If var_acc_frm_srch_as_grp <> "ALL" Then
            
                searchQuery = searchQuery & " AND " & _ 
                            "acc_GRP.ACCB_GROUP_CODE = '" & var_acc_frm_srch_as_grp & "' "
                
                Extra_tables =  " LEFT OUTER JOIN " & _
                                  Application("DBTABLE_HR_DATA_ABOOK") & _
                                  " acc_GRP ON acc_CORE.CORP_CODE = acc_GRP.CORP_CODE AND " & _
                                  "acc_CORE.ACCB_CODE = acc_GRP.ACCB_CODE"
        End if
        
         If var_acc_frm_srch_bd_accb <> "any"  and len(var_acc_frm_srch_bd_accb) > 0 Then
       searchQuery = searchQuery & "AND " & _ 
                  "acc_CORE.REC_BD = '" & var_acc_frm_srch_bd_accb & "' "
    End if
   
   end if
   
   
  
   
    If Len(var_acc_frm_srch_as_ref) > 0 Then
        searchQuery = searchQuery & " AND " & _
                    "acc_core.RECORDREFERENCE = '" & var_acc_frm_srch_as_ref & "' "
    End if
    
    If Len(var_acc_frm_srch_fname) > 0 Then
        searchQuery = searchQuery & "AND " & _ 
                    "(acc_INJ.INJFNAME = '" & var_acc_frm_srch_fname & "' OR " & _
                    "acc_ID.IDFNAME = '" & var_acc_frm_srch_fname & "') "
    End if
    
    If Len(var_acc_frm_srch_sname) > 0 Then
        searchQuery = searchQuery & "AND " & _
                    "(acc_INJ.INJSNAME = '" & var_acc_frm_srch_sname & "' OR " & _
                    "acc_ID.IDSNAME = '" & var_acc_frm_srch_sname & "') "
    End if
    
    If var_acc_frm_srch_eby <> "any" and len(var_acc_frm_srch_eby) > 0  Then
        searchQuery = searchQuery & " AND " & _
                    "acc_core.CREATORID = '" & var_acc_frm_srch_eby & "' "
    End if
    
    If var_acc_frm_srch_dateop => 1  Then
        if (var_acc_frm_srch_dateop = 4 and var_acc_frm_srch_date2 <> "NONE") or (var_acc_frm_srch_date1 <> "NONE") then
           Select Case var_acc_frm_srch_dateop
              Case 1
                searchQuery = searchQuery & "AND " & _
                                    "acc_CORE.RECORDDATE BETWEEN '" & var_acc_frm_srch_date1 & " 00:00:00' AND '" & var_acc_frm_srch_date1 & " 23:59:59'"
              Case 2
                searchQuery = searchQuery & "AND " & _
                                    "acc_CORE.RECORDDATE < '" & var_acc_frm_srch_date1 & " 23:59:59'"
              Case 3
                searchQuery = searchQuery & "AND " & _
                                    "acc_CORE.RECORDDATE > '" & var_acc_frm_srch_date1 & " 00:00:00'"
              Case 4
                searchQuery = searchQuery & "AND " & _
                                    "acc_CORE.RECORDDATE BETWEEN '" & var_acc_frm_srch_date1 & " 00:00:00' AND '" & var_acc_frm_srch_date2 & " 23:59:59'"
            end select
        end if
    End if
    
                      
                   
    if len(var_acc_frm_srch_keyword) > 0 then
        searchQuery = searchQuery & " AND (acc_INJ.INJADDRESS1 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.INJADDRESS2 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.INJADDRESS3 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.INJPOSTCODE like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.INJOCCUPATION like '%" & var_acc_frm_srch_keyword & "%' OR  " & _
                                    "acc_INJ.injstage4 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.injstage4b like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.injstage5 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPFNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPSNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPADDRESS1 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPADDRESS2 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPADDRESS3 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPPOSTCODE like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_INJ.REPOCCUPATION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_CORE.REC_INCLOC like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDADDRESS1 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDADDRESS2 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDADDRESS3 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDPOSTCODE like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDOCCUPATION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDSTAGE4 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDSTAGE4B like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.IDSTAGE5 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPFNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPSNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPADDRESS1 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPADDRESS2 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPADDRESS3 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPPOSTCODE like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_ID.REPOCCUPATION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DAM.dam_what_happened like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DAM.dam_cause like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DAM.dam_what_damage like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DODESCRIPTION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPFNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPSNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPADDRESS1 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPADDRESS2 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPADDRESS3 like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPPOSTCODE like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_DO.DOREPOCCUPATION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_NM.NMDESCRIPTION like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_NM.NMACTIONS like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                    "acc_NM.NMREPFNAME like '%" & var_acc_frm_srch_keyword & "%' OR " & _
                                     "acc_NM.NMREPSNAME like '%" & var_acc_frm_srch_keyword & "%')"
    end if
    
     If Len(var_acc_frm_srch_hc_accb) > 0 Then
        searchQuery = searchQuery & " AND " & _ 
                    "acc_core.REC_ABNUM LIKE '%" & var_acc_frm_srch_hc_accb & "%' "
    End if
      
    If Len(var_acc_frm_srch_hc_ref) > 0 Then
        searchQuery = searchQuery & " AND " & _
                    "(acc_INJ.INJ_ABREF LIKE '%" & var_acc_frm_srch_hc_ref & "%' OR " & _
                    "acc_ID.ID_ABREF LIKE '%" & var_acc_frm_srch_hc_ref & "%') "
    End if
    
    Select Case var_acc_frm_inc_type
      Case "1"
        searchQuery = searchQuery & "AND acc_CORE.INJ_VALUE = 'Y' "
      Case "2"
        searchQuery = searchQuery & "AND acc_CORE.ID_VALUE = 'Y' "
      Case "3"
        searchQuery = searchQuery & "AND acc_CORE.DO_VALUE = 'Y' "
      Case "4"
        searchQuery = searchQuery & "AND acc_CORE.DAM_VALUE = 'Y' "
      Case "5"
        searchQuery = searchQuery & "AND acc_CORE.NM_VALUE = 'Y' "
      Case "6"
        searchQuery = searchQuery & "AND acc_CORE.RR_VALUE = 'Y' "
      Case "7"
        searchQuery = searchQuery & "AND acc_CORE.INV_VALUE = 'Y' "
    End select
   
  
   if  var_acc_frm_srch_adv = 1 then
        If Len(var_acc_frm_srch_inj_type) > 0 and var_acc_frm_srch_inj_type <> "ALL" Then
            searchQuery = searchQuery & "AND " & _
                        "acc_INJ.INJTYPE = '" & var_acc_frm_srch_inj_type & "' "
        End if
        
        If Len(var_acc_frm_srch_inj_cause) > 0  and var_acc_frm_srch_inj_cause <> "ALL" Then
            searchQuery = searchQuery & "AND " & _
                        "acc_INJ.INJCAUSE = '" & var_acc_frm_srch_inj_cause & "' "
        End if
        
        If Len(var_acc_frm_srch_inj_body) > 0 and var_acc_frm_srch_inj_body <> "ALL"  Then
            searchQuery = searchQuery & "AND " & _ 
                        "acc_INJ.INJBODY = '" & var_acc_frm_srch_inj_body & "' "
        End if
      
         If var_acc_frm_srch_status <> "ALL" Then
            searchQuery = searchQuery & "AND " & _
                        "(acc_INJ.INJSTATUS = '" & var_acc_frm_srch_status & "' OR " & _
                        "acc_ID.IDSTATUS = '" & var_acc_frm_srch_status & "') "
        End if
        
        If var_acc_frm_srch_gender <> "ALL" Then
            searchQuery = searchQuery & "AND " & _
                        "(acc_INJ.INJGENDER = '" & var_acc_frm_srch_gender & "' OR " & _
                        "acc_ID.IDGENDER = '" & var_acc_frm_srch_gender & "') "
        End if
   
   end if
    
    ' sort orders
   
        searchQuery = searchQuery & " ORDER BY " & replace(var_acc_frm_srch_sop,"x", var_acc_frm_srch_order)
   
  
        searchQuery = searchQuery & " " & var_acc_frm_srch_order
  

else
    varSEARCH = "N"
end if



%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="../../risk_assessment/app/default.css" media="all" />
    <script type='text/javascript' src='../../../../data/scripts/js/global.js'></script>
    <script language='javascript'>
    
    function chkForm(myForm)
    {
        if (myForm.acc_frm_srch_submit.value == "Search for Accident / Incident")
	    {
            dateValid = 1
		    valid = 1
		    col0 = '#99ccff'
		    col1 = '#ffffff'
		    errMsg = "There is an error in your form. Please correct the following fields:\n"
    		 
		    if (4 == myForm.acc_frm_inc_type.value && (myForm.acc_frm_srch_fname.value.length > 0 || myForm.acc_frm_srch_sname.value.length > 0)) 
		    {
  			    valid = 0
  	  		    errMsg += "\n - You can not enter an injured person if the incident type is near miss"
  			    myForm.acc_frm_inc_type.style.background = col0
  			    myForm.acc_frm_srch_fname.style.background = col0
  			    myForm.acc_frm_srch_sname.style.background = col0
		    }
		    else 
		    {
	 		    myForm.acc_frm_inc_type.style.background = col1
  			    myForm.acc_frm_srch_fname.style.background = col1
  			    myForm.acc_frm_srch_sname.style.background = col1
  		    }	
  		    
  		   
  		    
  		    if (myForm.acc_frm_srch_dateop.value > 0)
  		    {
  		         if (!isvalidDate(myForm.acc_frm_srch_date1_d.value, myForm.acc_frm_srch_date1_m.value, myForm.acc_frm_srch_date1_y.value)) 
		        {
  			        valid = 0
  			        dateValid = 0
  	  		        errMsg += "\n - Invalid Date"
  			        myForm.acc_frm_srch_date1_d.style.background = col0
  			        myForm.acc_frm_srch_date1_m.style.background = col0
  			        myForm.acc_frm_srch_date1_y.style.background = col0
		        }
		        else 
		        {
	 		        myForm.acc_frm_srch_date1_d.style.background = col1
  			        myForm.acc_frm_srch_date1_m.style.background = col1
  			        myForm.acc_frm_srch_date1_y.style.background = col1
  		        }	
  		    }
  		    
  		    if (4 == myForm.acc_frm_srch_dateop.value)
  		    {
  		         if (!isvalidDate(myForm.acc_frm_srch_date2_d.value, myForm.acc_frm_srch_date2_m.value, myForm.acc_frm_srch_date2_y.value)) 
		        {
  			        valid = 0
  			        dateValid = 0
  	  		        errMsg += "\n - Invalid Date"
  			        myForm.acc_frm_srch_date2_d.style.background = col0
  			        myForm.acc_frm_srch_date2_m.style.background = col0
  			        myForm.acc_frm_srch_date2_y.style.background = col0
		        }
		        else 
		        {
	 		        myForm.acc_frm_srch_date2_d.style.background = col1
  			        myForm.acc_frm_srch_date2_m.style.background = col1
  			        myForm.acc_frm_srch_date2_y.style.background = col1
  		        }	
  		        
  		        if (dateValid != 0)
  		        {
  		            
  		           date1 = new Date ( myForm.acc_frm_srch_date1_y.value, myForm.acc_frm_srch_date1_m.value-1, myForm.acc_frm_srch_date1_d.value );
  		           date2 = new Date ( myForm.acc_frm_srch_date2_y.value, myForm.acc_frm_srch_date2_m.value-1, myForm.acc_frm_srch_date2_d.value );
  		           
  		           if (date1 > date2)
  		           {
  		                valid = 0
  	  		            errMsg += "\n - Second date must be later than the first date"
  	  		            myForm.acc_frm_srch_date1_d.style.background = col0
  			            myForm.acc_frm_srch_date1_m.style.background = col0
  			            myForm.acc_frm_srch_date1_y.style.background = col0
  			            myForm.acc_frm_srch_date2_d.style.background = col0
  			            myForm.acc_frm_srch_date2_m.style.background = col0
  			            myForm.acc_frm_srch_date2_y.style.background = col0
  			       }
  			       else
  			       {
  			            myForm.acc_frm_srch_date1_d.style.background = col1
  			            myForm.acc_frm_srch_date1_m.style.background = col1
  			            myForm.acc_frm_srch_date1_y.style.background = col1
  			            myForm.acc_frm_srch_date2_d.style.background = col1
  			            myForm.acc_frm_srch_date2_m.style.background = col1
  			            myForm.acc_frm_srch_date2_y.style.background = col1
  		           }
  		           
  		        }
  		    }
  		    
  		    if (!valid)
		    {
			    alert(errMsg)
			    return false
		    }
		    else 
		    {
			    return true
		    }			
  		    
        }
    }
    
      
    function toggle(opt) 
    {
        myEle1 = document.getElementById("date1")
        myEle2 = document.getElementById("date2")
        if (0 == opt.value) {
            myEle1.style.display = "none"
            myEle2.style.display = "none"
        } else if (4 == opt.value) {
            myEle1.style.display = "inline"
            myEle2.style.display = "inline"
        } else {
            myEle1.style.display = "inline"
            myEle2.style.display = "none"
        }
    }
    
    function toggleGroupon()
    {
        
        document.getElementById("grp_val").style.display = "block";
         document.getElementById("grp_name").style.display = "block";
        document.getElementById("inc_val").style.display = "none";
        document.getElementById("inc_name").style.display = "none";
       
    }
    
    function  toggleIncon()
    {
        
        document.getElementById("inc_val").style.display = "block";
        document.getElementById("inc_name").style.display = "block";
        document.getElementById("grp_val").style.display = "none";
        document.getElementById("grp_name").style.display = "none";
       
    }
      
    function  toggleAdvSearch()
    {
       
        if (0==document.getElementById("acc_frm_srch_adv").value)
        {
            document.getElementById("hid_adv_div").value = "Hide Criteria";
            document.getElementById("acc_frm_srch_adv").value = 1;
            document.getElementById("adv_div").style.display = "block";
         //   document.getElementById("hid_nm_adv_div").value = "Show Criteria";
          //  document.getElementById("acc_frm_srch_nm_adv").value = 0;
           // document.getElementById("nm_adv_div").style.display = "none";
        }
        else
        {
            document.getElementById("hid_adv_div").value = "Show Criteria";
             document.getElementById("acc_frm_srch_adv").value = 0;
             document.getElementById("adv_div").style.display = "none";
        }
             
    }
    
      function  toggleAdvNMSearch()
    {
       
        if (0==document.getElementById("acc_frm_srch_nm_adv").value)
        {
            document.getElementById("hid_nm_adv_div").value = "Hide Criteria";
            document.getElementById("acc_frm_srch_nm_adv").value = 1;
            document.getElementById("nm_adv_div").style.display = "block";
            document.getElementById("hid_adv_div").value = "Show Criteria";
             document.getElementById("acc_frm_srch_adv").value = 0;
             document.getElementById("adv_div").style.display = "none";
        }
        else
        {
             document.getElementById("hid_nm_adv_div").value = "Show Criteria";
             document.getElementById("acc_frm_srch_nm_adv").value = 0;
             document.getElementById("nm_adv_div").style.display = "none";
        }
             
    }
   
    
    </script>
    
</head>

<body>
<form action='acc_search.asp' method='get' onsubmit='return chkForm(this)'>

 <div id='section_0' class='optionsection' align='right'>
    <table width='100%'>
    <tr>
        <td class='l title'>Accident / Incident <strong>search</strong><br /><span class='info'>Real-time search for live accident incident data.</span></td>
        <%  
       
            response.Write "<td width='100' class='sr_icon l link' ><a id='menu3' href='acc_statistics.asp?" & Redirect_string & "' ><span>Ranking</span><br />statistics</a></td>"
     
       'If Mid(Session("YOUR_LICENCE"),16,1) = 1 Then    
           'response.Write "<td width='100' class='pt_icon l link' ><a id='A1' href='../../statistical_analysis/'><span>Graphical</span><br />Analysis</a></td>"
        'end if %>
        <td width='100' class='pt_icon l link' ><a id='menu4' href='javascript:window.print()'><span>Print</span><br />my search results</a></td>
    </tr>
    </table>
 </div>

<!-- Window style starts -->
<div id='windowtitle'>
<h5>Accident /  Incident Search Facility</h5>
<!-- hidden drop -->
<div id='windowdrop' style='display:block;'>
    <table width='100%'>
    
    <tr>
        <th>Group by</th>
        <td colspan='5'><input type='radio' name='acc_frm_srch_grpby' value='inc' <% if var_acc_frm_srch_grpby <> "grp" then response.write "checked"  %> <% if Session("YOUR_ACCESS") > 0 then response.write " disabled"  %> onclick='toggleIncon()' /> Incident Centres <input type='radio' name='acc_frm_srch_grpby' value='grp' <% if var_acc_frm_srch_grpby = "grp" then response.write "checked"  %>  <% if Session("YOUR_ACCESS") > 0 then response.write " disabled"  %> onclick='toggleGroupon()' /> Groups</td>
    </tr>
    <tr>
        <th><div id='inc_name' <% if var_acc_frm_srch_grpby = "grp" then response.write "style='display: none;'" %>>Incident Centre</div><div id='grp_name' <% if var_acc_frm_srch_grpby <> "grp" then response.write "style='display: none;'" %>>Incident Group</div></th>
        <td colspan='3'>
            <div id='inc_val' <% if var_acc_frm_srch_grpby = "grp" then response.write "style='display: none;'" %>>
                <% if Session("YOUR_ACCESS") > 0 then acc_frm_srch_as_accb_disp =  "disabled"
                call accblist(var_acc_frm_srch_as_accb,"acc_frm_srch_as_accb",acc_frm_srch_as_accb_disp ) %>
            </div>
            <div id='grp_val' <% if var_acc_frm_srch_grpby <> "grp" then response.write "style='display: none;'" %>>
                <% if Session("YOUR_ACCESS") > 0 then acc_frm_srch_as_grp_disp =  "disabled"
                call grplist(var_acc_frm_srch_as_grp,"acc_frm_srch_as_grp",acc_frm_srch_as_grp_disp ) %>
            </div>
        </td>
    </tr>
    <tr>
        <th>Department</th>
        <td> <div id='inc_val2' <% if var_acc_frm_srch_grpby = "grp" then response.write "style='display: none;'" %>>
              <% call departmentList(var_acc_frm_srch_bd_accb,"acc_frm_srch_bd_accb",var_acc_frm_srch_grpby,var_acc_frm_srch_as_accb) %>
            </div>
            <div id='grp_val2' <% if var_acc_frm_srch_grpby <> "grp" then response.write "style='display: none;'" %>>
                <% call departmentList(var_acc_frm_srch_bd_grp,"acc_frm_srch_bd_grp",var_acc_frm_srch_grpby,var_acc_frm_srch_as_grp)%>
            </div>
            
             </td>
        <td width='5'>&nbsp;</td>
         <th>Keyword search</th>
        <td><input type='text' size='30' class='text' name='acc_frm_srch_keyword' value='<% =var_acc_frm_srch_keyword %>' /></td>
  </tr>
    <tr>
        <th>Incident contains</th>
        <td><%Call inc_type(var_acc_frm_inc_type,"acc_frm_inc_type")%></td>
        <td width='5'>&nbsp;</td> 
        <th>Hard copy accident book number</th>
        <td><input type='text' size='30' class='text' name='acc_frm_srch_hc_accb' value='<% =var_acc_frm_srch_hc_accb %>' /></td>
   </tr>
        
    <tr>
        <th width='20%'>AssessNET reference</th>
        <td  width='27%'><input type='text' size='30' class='text' name='acc_frm_srch_as_ref' value='<% =var_acc_frm_srch_as_ref %>' /></td>
        <td width='6%'>&nbsp;</td>
        <th width='20%'>Hard copy accident reference</th>
        <td  width='27%'><input type='text' size='30' class='text' name='acc_frm_srch_hc_ref' value='<% =var_acc_frm_srch_hc_ref %>' /></td>
    </tr>
    <tr>
        <th rowspan=2>Name of injured person</th>
        <td rowspan=2 style="padding: 0px;">
            <table style="padding: 0px; margin: 0px">
                <tr>
                    <td>First Name</td>
                    <td width=2>&nbsp;</td>
                    <td>Surname</td>
                </tr>
                <tr>
                    <td><input type='text' class='text' name='acc_frm_srch_fname' value='<% =var_acc_frm_srch_fname %>' /></td>
                    <td></td>
                    <td><input type='text' class='text' name='acc_frm_srch_sname' value='<% =var_acc_frm_srch_sname %>' /></td>
                </tr>
            </table>
        </td>
         <td width='5' rowspan=2>&nbsp;</td>
        <th>Sort results by</th>
        <td><% call sortoperator(var_acc_frm_srch_sop,"acc_frm_srch_sop") %></td>
    </tr>
    <tr>
        <th>Order results in</th>
         <td><select name='acc_frm_srch_order' class='select'><option value='desc' <% if var_acc_frm_srch_order = "desc" then response.write "selected" end if %>>decending order</option><option value='asc' <% if var_acc_frm_srch_order = "asc" then response.write "selected" end if %>>ascending order</option></select></td>
   
    </tr>
    <tr>
        <th>Entered by</th>
        <td><% call userlist(var_acc_frm_srch_eby,"acc_frm_srch_eby") %></td>
        <td width='5'>&nbsp;</td>
        <th>Only show records that <strong>you manage</strong></th>
        <td><input type='checkbox' value='user' name='vtype' id='vtype' class='checkbox' <% if var_acc_frm_srch_vtype = "user" then response.write "checked" end if %>/></td>
    </tr>    
    <tr>
        <th>Incident date is</th>
        <td colspan=5><%call dateoperator(var_acc_frm_srch_dateop, "acc_frm_srch_dateop") %>&nbsp;
        <div id="date1" <% if var_acc_frm_srch_dateop  > 0  then response.write "style='display: inline;'" else response.write "style='display: none;'" %>>
         <%
            Call AutoDATE(var_acc_frm_srch_date1,"acc_frm_srch_date1_d","acc_frm_srch_date1_m","acc_frm_srch_date1_y","P") 
         %>
        </div>
        <div id="date2" <% if var_acc_frm_srch_dateop  = 4  then response.write "style='display: inline;'" else response.write "style='display: none;'" %>>
          and   
        <%
             Call AutoDATE(var_acc_frm_srch_date2,"acc_frm_srch_date2_d","acc_frm_srch_date2_m","acc_frm_srch_date2_y","P")
        %>
        </div>
        </td>
    </tr>
   </table>
   &nbsp;
   
   <table>
   <tr><td>
   <h4>Advanced Injury Criteria</h4></td><td> <input type='button' class='button' name='hid_adv_div' id='hid_adv_div' onclick='toggleAdvSearch()' value='<% if var_acc_frm_srch_adv = 1 then response.write "Hide Criteria" else response.write "Show Criteria" %>'> <input type='hidden' id='acc_frm_srch_adv' name='acc_frm_srch_adv' value='<%=var_acc_frm_srch_adv %>' />
   </td></tr></table>
   <div id='adv_div' <% if var_acc_frm_srch_adv = 0 then response.write "style='display: none'" else response.write "style='display: block'" %>>
   <table width='100%'>
    <tr>
        <th width='20%'>Status of injured person</th>
        <td  width='27%'><select class='select' name="acc_frm_srch_status">
          <option value="ALL">Any Status</option>
          
          <%if  Session("MODULE_ACCB") ="2"  then
               Call autogenInjStatus(var_acc_frm_srch_status)
            else
          %>
              <option value="0"<% If "0" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Employee</option>
              <option value="1"<% If "1" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Contractor</option>
              <option value="2"<% If "2" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Member of Public / Visitor</option>
              <option value="4"<% If "4" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Patient</option>
              <option value="5"<% If "5" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Resident / Tenant</option>
              <option value="6"<% If "6" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Volunteer</option>
              <option value="7"<% If "7" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Pupil/Student</option>
              <option value="3"<% If "3" = var_acc_frm_srch_status Then Response.Write " selected='selected'" %>>Other</option> 
          <%end if%>
        </select>
        </td>
        <td width='6%'>&nbsp;</td>
        <th width='20%'>Gender of injured person</th>
        <td  width='27%'>
            <select class='select' name="acc_frm_srch_gender">
                <option value="ALL">Any Gender</option>
                <option value="0"<% If "0" = var_acc_frm_srch_gender Then Response.Write " selected='selected'" %>>Male</option>
                <option value="1"<% If "1" = var_acc_frm_srch_gender Then Response.Write " selected='selected'" %>>Female</option>
            </select>
        </td>
    </tr>
     <tr>
        <th>Injury type</th>
        <td>
            <select class='select'  name='acc_frm_srch_inj_type' >
        <option value='ALL'>Any Injury Type</option>
        <% Call autogenInjType(var_acc_frm_srch_inj_type) %>
        </select>
        </td>
        <td width='5'>&nbsp;</td>
        <th>Apparent cause</th>
        <td><select class='select'  name='acc_frm_srch_inj_cause' >
        <option value='ALL'>Any Injury Cause</option>
        <% Call autogenApparentCause(var_acc_frm_srch_inj_cause) %>
        </select></td>
    </tr>
     <tr>
        <th>Body part affected</th>
        <td>
            <select class='select'  name='acc_frm_srch_inj_body' >
        <option value='ALL'>Any Body Part</option>
        <% Call autogenBodyPart(var_acc_frm_srch_inj_body) %>
        </select>
        </td>
        <td  colspan=3>&nbsp;</td>
        
    </tr>
    </table>
     </div>
    <%if session("corp_code") = "222999" then %>
             <table>
           <tr><td>
           <h4>Advanced Near Miss Criteria</h4></td><td><input type='button' class='button' name='hid_nm_adv_div' id='hid_nm_adv_div' onclick='toggleAdvNMSearch()' value='<% if var_acc_frm_srch_nm_adv = 1 then response.write "Hide Criteria" else response.write "Show Criteria" %>'> <input type='hidden' id='acc_frm_srch_nm_adv' name='acc_frm_srch_nm_adv' value='<%=var_acc_frm_srch_nm_adv %>' />
           </td></tr></table>
           <div id='nm_adv_div' <% if var_acc_frm_srch_nm_adv = 0 then response.write "style='display: none'" else response.write "style='display: block'" %>>
           <table width='100%'>
            <tr>
                <th width='20%'>Hazard Classification</th>
                <td  width='27%'><% 
              '  var_acc_frm_nmiss_class = 0
              '     call nmiss_class(var_acc_frm_nmiss_class,"acc_frm_nmiss_class")
                %>
                </td>
                <td width='6%'>&nbsp;</td>
                <th width='20%'>How Serious</th>
                <td  width='27%'>
                    <select class='select' name="acc_frm_srch_gender">
                        <option value="ALL">Any Gender</option>
                        <option value="0"<% If "0" = var_acc_frm_srch_gender Then Response.Write " selected='selected'" %>>Male</option>
                        <option value="1"<% If "1" = var_acc_frm_srch_gender Then Response.Write " selected='selected'" %>>Female</option>
                    </select>
                </td>
            </tr>
             
            </table>
    
    </div><%end if %>
    <table width='100%'>
    <tr>
       <td class='r' colspan=5><input type='submit' class='submit' name='acc_frm_srch_submit' value='Search for Accident / Incident' /></td>
    </tr>
    </table>
</div>
<!-- hidden drop end -->
<div id='windowmenu'>
    <span><a href='../default.asp'>Back to main menu</a></span>
</div>
    <!-- body of window -->
    <div id='windowbody' class='pad'>
    
    <div id='search results' class='searcharea'>
    <h3>Search results</h3>
    
    <%
    
 
    
    if varSEARCH = "Y" then

    
    StartTime = Timer
    
 
    
    objCommand.CommandText = "SELECT DISTINCT acc_CORE.Recordreference,acc_CORE.ID,acc_CORE.RecordDate,acc_CORE.ACCB_CODE," & _
                             "   acc_CORE.REC_BD,acc_CORE.REC_INCLOC,acc_CORE.NM_VALUE,acc_CORE.DAM_VALUE,acc_CORE.DO_VALUE," & _
                             "   acc_CORE.ATTACH_COUNT,acc_CORE.RR_VALUE,acc_CORE.INV_VALUE,acc_CORE.DAM_VALUE,acc_CORE.ID_COUNT," & _
                             "   acc_CORE.INJ_COUNT,acc_CORE.RR_COUNT, cast(RIGHT(acc_CORE.ACCB_CODE, LEN(acc_CORE.ACCB_CODE) - 4) as integer) AS srt_accb, " & _ 
                            "cast(RIGHT(acc_CORE.RECORDREFERENCE, LEN(acc_CORE.RECORDREFERENCE) - 1) as integer) AS srt_accref, accb_dets.REC_BU, accb_dets.REC_BL, acc_core.RECORDSTATUS,acc_core.RECORDTASKSTATUS " & _
                             "FROM " & Application("DBTABLE_ACC_DATA") & " as acc_CORE " & _
                             "LEFT JOIN " & Application("DBTABLE_HR_DATA_ABOOK") & " as accb_dets " & _
                             " ON acc_core.ACCB_CODE = accb_dets.ACCB_CODE " & _
                             " AND acc_core.CORP_CODE = accb_dets.CORP_CODE " & _
                             "LEFT JOIN " & Application("DBTABLE_INJ_DATA") & " as acc_INJ " & _
                             "   ON acc_CORE.Recordreference = acc_INJ.Recordreference " & _
                             "   AND acc_CORE.ACCB_CODE = acc_INJ.ACCB_CODE " & _
                             "   AND acc_CORE.CORP_CODE = acc_INJ.CORP_CODE " 
                             if var_acc_frm_srch_vtype <> "user" then
                                objCommand.CommandText = objCommand.CommandText & " and acc_inj.RECORDSTATUS='1' " 
                             end if
   objCommand.CommandText = objCommand.CommandText & " LEFT JOIN " & Application("DBTABLE_ID_DATA") & " as acc_ID " & _
                             "   ON acc_CORE.Recordreference = acc_ID.Recordreference " & _
                             "   AND acc_CORE.ACCB_CODE = acc_ID.ACCB_CODE " & _
                             "   AND acc_CORE.CORP_CODE = acc_ID.CORP_CODE " 
                             if var_acc_frm_srch_vtype <> "user"  then
                                objCommand.CommandText = objCommand.CommandText & "   AND acc_id.recordstatus = '1' "
                             end if
   objCommand.CommandText = objCommand.CommandText &  " LEFT JOIN " & Application("DBTABLE_DO_DATA") & " as acc_DO " & _
                             "   ON acc_CORE.Recordreference = acc_DO.Recordreference " & _
                             "   AND acc_CORE.ACCB_CODE = acc_DO.ACCB_CODE " & _
                             "   AND acc_CORE.CORP_CODE = acc_DO.CORP_CODE " 
                             if var_acc_frm_srch_vtype <> "user"  then
                                objCommand.CommandText = objCommand.CommandText & " and acc_do.RECORDSTATUS='1' "
                             end if
   objCommand.CommandText = objCommand.CommandText & " LEFT JOIN " & Application("DBTABLE_DAM_DATA") & " as acc_DAM " & _
                             "   ON acc_CORE.Recordreference = acc_DAM.Recordreference " & _
                             "   AND acc_CORE.ACCB_CODE = acc_DAM.ACCB_CODE " & _
                             "   AND acc_CORE.CORP_CODE = acc_DAM.CORP_CODE "
                             if var_acc_frm_srch_vtype <> "user"  then
                                objCommand.CommandText = objCommand.CommandText & " and acc_dam.RECORDSTATUS='1' "
                             end if
   objCommand.CommandText = objCommand.CommandText & " LEFT JOIN " & Application("DBTABLE_NM_DATA") & " as acc_NM " & _
                             "   ON acc_CORE.Recordreference = acc_NM.Recordreference " & _
                             "   AND acc_CORE.ACCB_CODE = acc_NM.ACCB_CODE " & _
                             "   AND acc_CORE.CORP_CODE = acc_NM.CORP_CODE "
                             if var_acc_frm_srch_vtype <> "user"  then
                                objCommand.CommandText = objCommand.CommandText & " and acc_nm.RECORDSTATUS='1' " 
                             end if
   objCommand.CommandText = objCommand.CommandText &   Extra_tables & _
                             " WHERE " & _
                             "acc_CORE.CORP_CODE='" & Session("CORP_CODE") & "' " 
                             if var_acc_frm_srch_vtype <> "user"  then
                                objCommand.CommandText = objCommand.CommandText & " and acc_CORE.RECORDSTATUS='1' "
                             end if
   objCommand.CommandText = objCommand.CommandText &     searchQUERY 
    
    
        ' Create and open ADO recordset for paging
        Set Objrs = Server.CreateObject("ADODB.Recordset")
        Objrs.CursorLocation = 3 ' adUseClient
        Objrs.Open objCommand.CommandText, objconn
 
    if objrs.eof then
    
        response.Write "<div class='warning'>" & _
                            "<table width='100%' cellpadding='2px' cellspacing='2px'>" & _
                            "<tr><td class='c'><strong>NO RECORDS HAVE BEEN FOUND</strong><br />No records have been found in the database using the above criteria. Try removing or changing some of the critera you are searching under to produce a wider search.</td></tr>" & _
                            "</table>" & _
                        "</div>"
    
    else
        
        var_db_col_inj_all = 0
        var_db_col_ill_all = 0
        var_db_col_do_all = 0
        var_db_col_nm_all = 0
        var_db_col_rid_all = 0
        var_db_col_inv_all = 0
        var_db_col_pd_all = 0
        
        Objrs.PageSize = 20
        var_PageSize = Objrs.PageSize
        var_Pagecount = Objrs.PageCount
        
        ' Error protection against idiots
        If Cint(var_Page) > Cint(var_Pagecount) then
        var_Page = 1
        end if
        
        objrs.AbsolutePage = var_Page
        var_AbsolutePage = objrs.AbsolutePage
        var_db_col_recordcount = 0
    
        Response.Write  "<div class='l'><table width='350px' cellpadding='2px' cellspacing='2px' class='showtd l'><tr><th colspan='2'>Key:</th>" & _
                         "<td width='30px' style='background-color:#FFF6D1;'>&nbsp;</td><td nowrap='nowrap'> = Incomplete Task</td>" & _
                         "<td width='30px' style='background-color:#FBEEEE;'>&nbsp;</td><td nowrap='nowrap'> = Incomplete Incident</td></tr>" & _
                        "</table></div>"
                        
                        
        ' build col table header
        Response.Write  "<table width='100%' cellpadding='2px' cellspacing='2px'>"
        
        If cint(var_Pagecount) > 1 then
        Response.Write  "<tr><th colspan='14' class='r'>"
        Response.Write "Page: "

        if Cint((var_Absolutepage + 9)) > Cint(var_Pagecount) then
        var_top_page = var_Pagecount
            else
            var_top_page = var_absolutepage + 9
        end if
        
        if Cint(var_pagecount) > 9 then
        var_base_page = var_AbsolutePage
        
            if Cint(var_AbsolutePage) > 1 then
            Response.Write "<a href='acc_search.asp?" & Redirect_string & "&Page=1'>1</a> ... "
            end if
        
            else
            var_base_page = 1
        end if
        
        for Page = var_base_page to var_top_page
        
            If Cint(Page) = Cint(var_AbsolutePage) then
            Response.Write Page & " "
            else
            Response.Write "<a href='acc_search.asp?" & Redirect_string & "&Page=" & Page & "'>" & Page & "</a> "
            end if
        
        next
        
        if Cint(var_Pagecount) > cint(var_top_page) then
        Response.Write " ... <a href='acc_search.asp?" & Redirect_string & "&Page=" & var_Pagecount & "'>" & var_Pagecount & "</a>"
        end if
        
        Response.Write  "&nbsp;</th></tr>"
        End If
        
        Response.write  "<tr><th width='5'><img src='../../risk_assessment/app/img/sml_tick.png' /></th><th width='70' nowrap>Date</th><th width='90' nowrap>Full Ref.</th><th colspan=2>Location (Department)</th><th width='30' nowrap title='Injuries'>INJ</th><th width='30' nowrap title='Illness / Disease'>I/D</th><th width='30' nowrap title='Dangerous Occurences'>D/O</th><th width='30' nowrap title='Property Damage'>PD</th><th width='30' nowrap title='Near Misses'>NM</th><th width='30' nowrap title='RIDDOR Reports'>RiD</th><th width='30' nowrap title='Investigations'>INV</th><th width='30' nowrap  title='Attachments'><img src='../../../../images/icons/sml_attach.png' /></th><th width='50'>Options</th></tr>"
        
        while not objrs.eof and var_db_col_recordcount =< var_PageSize
        
            var_db_col_recordcount = var_db_col_recordcount + 1
            
            var_db_col_id = objrs("id")
            var_db_col_date = Left(objRs("RECORDDATE"),10)
            var_db_col_ref_alt = objrs("REC_BU") & " : " & objrs("REC_BL")
            var_db_col_ref =  objRs("ACCB_CODE") & " / " & objRs("RECORDREFERENCE") 
            var_db_col_loc = objRs("REC_INCLOC")  
            var_db_col_dep = objRs("REC_BD")
            
                      
            If Cint(objRs("INJ_COUNT")) > 0 Then
                var_db_col_inj = "<strong class='alt'>" & objRs("INJ_COUNT") & "</strong>"
                var_db_col_inj_all = var_db_col_inj_all + Cint(Objrs("INJ_COUNT"))
            Else
                var_db_col_inj = "0"
            End if
               
            If CInt(objRs("ID_COUNT")) > 0 Then
                var_db_col_ill = "<strong class='alt'>" & objRs("ID_COUNT") & "</strong>"
                var_db_col_ill_all = var_db_col_ill_all + Cint(Objrs("ID_COUNT"))
            Else
                var_db_col_ill = "0"
            End if
            
            If objRs("DO_VALUE") = "Y" Then
                var_db_col_do = "<img src='../../../../images/icons/sml_tick.png' />"
                var_db_col_do_all = var_db_col_do_all + 1
            Else
                var_db_col_do = "-"
            End if
            
             If objRs("DAM_VALUE") = "Y" Then
                var_db_col_pd = "<img src='../../../../images/icons/sml_tick.png' />"
                var_db_col_pd_all = var_db_col_pd_all + 1
            Else
                var_db_col_pd = "-"
            End if
            
            
            If objRs("NM_VALUE") = "Y" Then
                var_db_col_nm = "<img src='../../../../images/icons/sml_tick.png' />"
                var_db_col_nm_all = var_db_col_nm_all + 1
            Else
                var_db_col_nm = "-"
            End if
                       
            
            If objRs("RR_VALUE") = "Y" Then
                var_db_col_RiD = "<img src='../../../../images/icons/sml_tick.png' />"
                var_db_col_RiD_all = var_db_col_RiD_all + 1
            Else
                var_db_col_RiD = "-"
            End if
            
            If objRs("INV_VALUE") = "Y" Then
                var_db_col_inv = "<img src='../../../../images/icons/sml_tick.png' />"
                var_db_col_inv_all = var_db_col_inv_all + 1
            Else
                var_db_col_inv = "-"
            End if
            
            If objRs("ATTACH_COUNT") > 0 Then
                var_db_col_attach = "<strong class='alt'>" & objRs("ATTACH_COUNT") & "</strong>"
            Else
                var_db_col_attach = "-"
            End if

            
            
           
          If objRs("RecordStatus") = "1" Then
        
                IF ObjRS("RecordTaskStatus") = "0" Then
                   'incomplete task     
                  var_row_colour = "#FFF6D1"
                   var_row_icon = "edit_"
                Else
                    'complete
                    var_row_colour = "#FFFFFF"
                    var_row_icon = ""
                End IF
            
        Else
            'incomplete
            var_row_colour = "#FBEEEE"
            var_row_icon = "edit_"
        End if
          
               
         
            
            'Set options
             var_db_col_options = "<a href='../accident_details.asp?IC=" & objRs("ACCB_CODE") & "&amp;INFOID=" & objRs("RECORDREFERENCE") & "'><img border='0' src='../../risk_assessment/app/img/report_med_but.png' alt='View Incident Details'></a>"
            
            Response.Write  "<tr style='background-color:" & var_row_colour & ";' onmouseover='this.style.backgroundColor = ""#FFF6D1""' onmouseout='this.style.backgroundColor = """ & var_row_colour & """'><td class='c'><input type='checkbox' class='checkbox' name='acc_frm_srch_" & var_db_col_id & "' /></td><td class='c'>" & var_db_col_date & "</td><td class='c' width='90' alt='" & var_db_col_ref_alt & "'>" & var_db_col_ref & "</td><td width='10' valign='middle' class='c'><img src='../../risk_assessment/app/img/rec_icon_" & var_row_icon & "sml.png' /></td><td>" & var_db_col_loc & "&nbsp;(<span class='alt'>" & var_db_col_dep & "</span>)</td><td class='c' title='Injuries'>" & var_db_col_inj & "</td><td class='c' title='Illness / Disease'>" & var_db_col_ill & "</td><td class='c' title='Dangerous Occurences'>" & var_db_col_do & "</td><td class='c'  title='Property Damage'>" & var_db_col_pd & "</td><td class='c'  title='Near Misses'>" & var_db_col_nm & "</td><td class='c' title='RIDDOR Reports'>" & var_db_col_RiD & "</td><td class='c' title='Investigations'>" & var_db_col_inv & "</td><td class='c'  title='Attachments'>" & var_db_col_attach  & "</td><td width='50'>" & var_db_col_options & "</td></tr>"
            
            objrs.movenext
        wend
        
        Response.Write  "<tr><th colspan='14' class='r'>"
   
        If cint(var_Pagecount) > 1 then
        Response.Write "Page: "

        if Cint((var_Absolutepage + 9)) > Cint(var_Pagecount) then
        var_top_page = var_Pagecount
            else
            var_top_page = var_absolutepage + 9
        end if
        
        if Cint(var_pagecount) > 9 then
        var_base_page = var_AbsolutePage
        
            if Cint(var_AbsolutePage) > 1 then
            Response.Write "<a href='acc_search.asp?" & Redirect_string & "&Page=1'>1</a> ... "
            end if
        
            else
            var_base_page = 1
        end if
        
        for Page = var_base_page to var_top_page
        
            If Cint(Page) = Cint(var_AbsolutePage) then
            Response.Write Page & " "
            else
            Response.Write "<a href='acc_search.asp?" & Redirect_string & "&Page=" & Page & "'>" & Page & "</a> "
            end if
        
        next
        
        if Cint(var_Pagecount) > cint(var_top_page) then
        Response.Write " ... <a href='acc_search.asp?" & Redirect_string & "&Page=" & var_Pagecount & "'>" & var_Pagecount & "</a>"
        end if
        
        End If

        Response.Write  "&nbsp;</th></tr><table>"

        EndTime = Timer

        Response.Write  "<div class='r'><table width='350px' cellpadding='2px' cellspacing='2px' class='showtd l'><tr><th colspan='2'>Key:</th>" & _
                         "<td width='30px' style='background-color:#FFF6D1;'>&nbsp;</td><td nowrap='nowrap'> = Incomplete Task</td>" & _
                         "<td width='30px' style='background-color:#FBEEEE;'>&nbsp;</td><td nowrap='nowrap'> = Incomplete Incident</td></tr>" & _
                        "</table></div><br />"
       
        
    
    end if
    

    end if
    
    %>

    </div> 

    <% if varSEARCH <> "Y" then %>
    
    <div class='warning'>
        <table width='100%' cellpadding='2px' cellspacing='2px'>
        <tr><th width='40' valign='middle'>&nbsp;</th><td><strong>NOTE</strong><br />There are currently no search results displayed, please use the menus above to select the critera you wish to search for then click &quot;Search for Accident / Incident&quot; to continue.. Remember!, selecting as much crietria as possible will produce better and faster search results.</td></tr>
        </table>
    </div>
    
    <% end if %>

    </div>
    <!-- end of body -->
</div>    
<!-- end window style -->
<br />

</form>


</body>
</html>
<% call endconnections() %>