﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative.ExcelLib;
using ASNETNSP;


public partial class auditSummaryReport : System.Web.UI.Page
{



    public string corpCode = "222999";
    public int dateYear_int = 2017;
    public int dateMonth_int = 3;
    public string datetime_text = "";
    public DateTime StartDateTime;
    public DateTime EndDateTime;

    protected void Page_Load(object sender, EventArgs e)
    {
       corpCode = Convert.ToString(Request.QueryString["corpcode"]);
       dateYear_int = Convert.ToInt32(Request.QueryString["year"]);
       dateMonth_int = Convert.ToInt32(Request.QueryString["month"]);

        datetime_text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(dateMonth_int) + " " + dateYear_int;

        Page.Server.ScriptTimeout = 200;

        EndDateTime = new DateTime(dateYear_int, dateMonth_int, 1);
        StartDateTime = EndDateTime.AddYears(-1);



        generateSMTReport();

    }

    

    private void generateSMTReport()
    {
        // get the Excel workbook format
        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xlsx_2007;

        // create the workbook in the desired format with a single worksheet
        ExcelWorkbook workbook = new ExcelWorkbook(workbookFormat);

        // set the license key before saving the workbook
        workbook.LicenseKey = "ZOr66/j66/r56/jl++v4+uX6+eXy8vLy";

        // set workbook description properties
        workbook.DocumentProperties.Subject = "Monthly Report";
        workbook.DocumentProperties.Comments = "Monthly Report";

        ExcelCellStyle titleStyle = workbook.Styles.AddStyle("WorksheetTitleStyle");
        // center the text in the title area
        titleStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        titleStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        // set the title area borders
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Top].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Left].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Right].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Medium;
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
        {
            // set the solid fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.SolidFill;
            titleStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(255, 255, 204);
        }
        else
        {
            // set the gradient fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.GradientFill;
            titleStyle.Fill.GradientFillOptions.Color1 = Color.FromArgb(255, 255, 204);
            titleStyle.Fill.GradientFillOptions.Color2 = Color.White;
        }
        // set the title area font 
        titleStyle.Font.Size = 14;
        titleStyle.Font.Bold = true;
        titleStyle.Font.UnderlineType = ExcelCellUnderlineType.Single;



      

        #region Add a style used for accessed cells and ranges

        ExcelCellStyle accessedRangeStyle = workbook.Styles.AddStyle("ΑccessedRangeStyle");
        accessedRangeStyle.Font.Size = 10;
        accessedRangeStyle.Font.Bold = true;
        accessedRangeStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        accessedRangeStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        accessedRangeStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        accessedRangeStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(204, 255, 204);
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        #endregion


        ExcelCellStyle sectionHeaderStyle = workbook.Styles.AddStyle("sectionHeaderStyle");
        sectionHeaderStyle.Font.Size = 12;
        sectionHeaderStyle.Font.Bold = true;
        sectionHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        sectionHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(248, 104, 93);
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle headerStyle = workbook.Styles.AddStyle("headerStyle");
        headerStyle.Font.Size = 10;
        headerStyle.Font.Bold = false;
        headerStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        headerStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        headerStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle headerStyleNoBorder = workbook.Styles.AddStyle("headerStyleNoBorder");
        headerStyleNoBorder.Font.Size = 10;
        headerStyleNoBorder.Font.Bold = false;
        headerStyleNoBorder.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        headerStyleNoBorder.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;



        ExcelCellStyle sectionColumnHeaderStyle = workbook.Styles.AddStyle("sectionColumnHeaderStyle");
        sectionColumnHeaderStyle.Font.Size = 10;
        
        sectionColumnHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionColumnHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionColumnHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionColumnHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(91, 0, 148);
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle sectionContent = workbook.Styles.AddStyle("sectionContent");
        sectionContent.Font.Size = 9;
        sectionContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Alignment.WrapText = true;

        ExcelCellStyle sectionWeightingContent = workbook.Styles.AddStyle("sectionWeightingContent");
        sectionWeightingContent.Font.Size = 11;
        sectionColumnHeaderStyle.Font.Bold = true;
        sectionWeightingContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionWeightingContent.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;



        // Specific report colours
        ExcelCellStyle sectionHeaderDate = workbook.Styles.AddStyle("sectionHeaderDate");
        sectionHeaderDate.Font.Color = Color.White;
        sectionHeaderDate.Font.Size = 20;
        sectionHeaderDate.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionHeaderDate.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionHeaderDate.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionHeaderDate.Fill.SolidFillOptions.BackColor = Color.FromArgb(96, 73, 122);


        ExcelCellStyle sectionHeaderSummary = workbook.Styles.AddStyle("sectionHeaderSummary");
        sectionHeaderSummary.Font.Size = 20;
        sectionHeaderSummary.Font.Bold = true;
        sectionHeaderSummary.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionHeaderSummary.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;

        ExcelCellStyle sectionSubHeader = workbook.Styles.AddStyle("sectionSubHeader");
        sectionSubHeader.Font.Size = 10;
        sectionSubHeader.Font.Bold = true;
        sectionSubHeader.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionSubHeader.Fill.SolidFillOptions.BackColor = Color.FromArgb(184, 204, 228);

        ExcelCellStyle sectionSubBasic = workbook.Styles.AddStyle("sectionSubBasic");
        sectionSubBasic.Font.Size = 8;
        sectionSubBasic.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionSubBasic.Fill.SolidFillOptions.BackColor = Color.FromArgb(220, 230, 241);

        ExcelCellStyle totalHeader = workbook.Styles.AddStyle("totalHeader");
        totalHeader.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        totalHeader.Font.Size = 10;
        totalHeader.Alignment.Orientation = 90;
        totalHeader.Alignment.WrapText = true;

        ExcelCellStyle totalContent = workbook.Styles.AddStyle("totalContent");
        totalContent.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        totalContent.Font.Size = 10;
        totalContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        totalContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        totalContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        totalContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle totalTitles = workbook.Styles.AddStyle("totalTitles");
        totalTitles.Font.Size = 10;
        

        // get the first worksheet in the workbook
        ExcelWorksheet worksheet = workbook.Worksheets[0];

        ExcelWorksheet secondWorksheet = workbook.Worksheets.AddWorksheet(
            "Simple Chart");


        // set the default worksheet name
        worksheet.Name = "Accident Report";
        secondWorksheet.Name = "Raw Data";

        #region WORKSHEET PAGE SETUP


        // set worksheet paper size and orientation, margins, header and footer






        worksheet.PageSetup.PaperSize = ExcelPagePaperSize.PaperA4;
        worksheet.PageSetup.Orientation = ExcelPageOrientation.Landscape;
        worksheet.PageSetup.LeftMargin = 0.2;
        worksheet.PageSetup.RightMargin = 0.2;
        worksheet.PageSetup.TopMargin = 1;
        worksheet.PageSetup.BottomMargin = 0.2;


        worksheet.PageSetup.RightHeaderFormat = "&G";
        worksheet.PageSetup.LeftHeaderFormat = "&A";
        worksheet.PageSetup.CenterFooterFormat = "&P";
        worksheet.PageSetup.LeftFooterFormat = "&F";



        if (File.Exists(Server.MapPath("../../../../../version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg")))
        {
            string corpLogo = Server.MapPath("../../../../../version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg");
            System.Drawing.Image logoImg = System.Drawing.Image.FromFile(corpLogo);
            worksheet.PageSetup.RightHeaderPicture = logoImg;
        }



        #endregion


        const int revertXAxis = 2;
        const int revertYAxis = 10;

        int currentXAxis = revertXAxis;
        int currentYAxis = revertYAxis;

        int maximumXAxis = 0;
        int maximumYAxis = 0;
        int sectionYAxis = 0;

        //remember score corodinates for styling later
        int scoreXAxis = 0;
        int scoreYAxis = 0;

        #region WRITE THE WORKSHEET TOP TITLE

        //worksheet["B2"].Text = "Accident Report";

        #endregion



        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.usp_stats_SMTREPORT_TotalIncidents", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@date_year", dateYear_int));
                    cmd.Parameters.Add(new SqlParameter("@date_month", dateMonth_int));
                    rdr = cmd.ExecuteReader();

                    DataTable resultsDataTable = new DataTable();
                    resultsDataTable.Load(rdr);

                    rdr.Dispose();
                    cmd.Dispose();


                    //string sql_text = "SELECT * FROM Module_ACC.dbo.SMT_Data_Stats_ML4 WHERE location_name not in ('another test home', 'Any site test', 'New Care Home', 'Linkage', 'The Chocolate Quarter')";
                    //
                    //SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    //SqlDataReader objrs = sqlComm.ExecuteReader();
                    //
                    //DataTable resultsDataTable = new DataTable();
                    //resultsDataTable.Load(objrs);
                    //
                    //objrs.Dispose();
                    //sqlComm.Dispose();


                    worksheet[currentYAxis, currentXAxis].Text = datetime_text;
                    worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Merge();
                    worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Style = sectionHeaderDate;

                    currentXAxis += 20;
                    worksheet[currentYAxis, currentXAxis].Text = "Incident Report Summary";
                    worksheet[currentYAxis, currentXAxis].Style = sectionHeaderSummary;


                    // We need:
                    // A list of each structure element.
                    // A list of each individual stat.
                    // We then look through each structure elem

                    var title_List = from x in resultsDataTable.AsEnumerable()
                        group x by (string)x["title"]
                        into g
                        select g;

                    var locationName_List = from x in resultsDataTable.AsEnumerable()
                        group x by (string)x["location_name"]
                        into g
                        select g;


                    currentXAxis = 17;
                    currentYAxis += 4;

                    foreach (var x in locationName_List)
                    {
                        string locationName = x.Key;

                        worksheet[currentYAxis, currentXAxis].Text = locationName;
                        worksheet.SetRowHeightInPoints(currentYAxis, 75);
                        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 2].Merge();
                        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 2].Style = totalHeader;
                        currentXAxis += 3;
                    }

                    

                    currentXAxis = revertXAxis;
                    currentYAxis += 1;

                    foreach (var y in title_List)
                    {
                        string title = y.Key;

                        worksheet[currentYAxis, currentXAxis].Text = title;
                        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 10].Style.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
                        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 10].Style.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
                        worksheet[currentYAxis, currentXAxis].Style.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
                        worksheet[currentYAxis, currentXAxis + 10].Style.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;


                        currentXAxis += 15;

                        foreach (var x in locationName_List)
                        {
                            string locationName = x.Key;

                            //We have the location name, now lets pull the stat!
                            var currentDataRow_List = resultsDataTable.AsEnumerable()
                                .Where(q => (string)q["title"] == title
                                            && (string)q["location_name"] == locationName)
                                .ToList();

                            foreach (var z in currentDataRow_List)
                            {
                                string recordCount = z["record_count"].ToString();
                                worksheet[currentYAxis, currentXAxis].Text = recordCount;
                            }

                            worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 2].Merge();
                            worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 2].Style = totalContent;
                            currentXAxis += 3;
                        }


                        currentXAxis = revertXAxis;
                        currentYAxis += 1;

                    }





                    resultsDataTable.Dispose();

                    currentYAxis += 1;
                    currentYAxis += 1;
                    currentYAxis += 1;
                    currentYAxis += 1;
                    currentXAxis = revertXAxis;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }




        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    

                    const int reset_columnCounter = 1;
                    const int reset_rowCounter = 1;

                    int columnCounter = reset_columnCounter;
                    int rowCounter = reset_rowCounter;



                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.usp_stats_SMTREPORT_YearlyIncidentSummary_v2", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@startdate", StartDateTime.ToString("yyyy-dd-MM HH:mm:ss.fff")));
                    cmd.Parameters.Add(new SqlParameter("@enddate", EndDateTime.ToString("yyyy-dd-MM HH:mm:ss.fff")));
                    rdr = cmd.ExecuteReader();

                    DataTable resultsDataTable = new DataTable();
                    resultsDataTable.Load(rdr);

                    rdr.Dispose();
                    cmd.Dispose();

                  //  string sql_text = "SELECT * FROM Module_ACC.dbo.SMT_Scrolling_Stats";
                  //  
                  //  SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                  //  SqlDataReader objrs = sqlComm.ExecuteReader();
                  //
                  //  DataTable resultsDataTable = new DataTable();
                  //  resultsDataTable.Load(objrs);
                  //
                  //  objrs.Dispose();
                  //  sqlComm.Dispose();



                    // We need a list of all distinct Month years  used first.
                    // We then need a list of all distinct structure tier.

                    // We then loop through and put the values in.

                    var title_List = from x in resultsDataTable.AsEnumerable()
                        group x by (string)Convert.ToString(x["split_name"])
                        into g
                        select g;

                    var month_list = from x in resultsDataTable.AsEnumerable()
                        group x by (string) x["x_axis"].ToString()
                        into g
                        select g;


                    //We first need to loop through the titles

                    foreach (var x in title_List)
                    {
                        columnCounter += 1;
                        string title = x.Key;
                        secondWorksheet[rowCounter, columnCounter].Text = title;
                    }
                    columnCounter = reset_columnCounter;

                    rowCounter += 1;


                    foreach (var x in month_list)
                    {
                        string monthValue = x.Key;
                        secondWorksheet[rowCounter, columnCounter].Text = monthValue;
                        columnCounter += 1;

                        foreach (var y in title_List)
                        {
                            string title = y.Key;

                            //We have the location name, now lets pull the stat!
                            var currentDataRow_List = resultsDataTable.AsEnumerable()
                                .Where(q => (string)Convert.ToString(q["split_name"]) == title
                                            && (string)q["x_axis"] == monthValue)
                                .ToList();

                            foreach (var z in currentDataRow_List)
                            {
                                string recordCount = z["y_axis"].ToString();
                                secondWorksheet[rowCounter, columnCounter].Style.Number.NumberFormatString = "0";
                                secondWorksheet[rowCounter, columnCounter].Text = recordCount;
                                



                            }

                            columnCounter += 1;


                        }
                        columnCounter = Convert.ToInt32(reset_columnCounter);
                        rowCounter += 1;

                    }

                    columnCounter = reset_columnCounter + 1;

                    resultsDataTable.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }




        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {



                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.usp_stats_SMTREPORT_Detailed_Breakdown_v2", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@person_status", "8,5,"));
                    cmd.Parameters.Add(new SqlParameter("@date_year", dateYear_int));
                    cmd.Parameters.Add(new SqlParameter("@date_month", dateMonth_int));
                    cmd.Parameters.Add(new SqlParameter("@tier_ref", 1));
                    cmd.Parameters.Add(new SqlParameter("@tier_level", 2));
                    cmd.Parameters.Add(new SqlParameter("@incident_type", ""));
                    rdr = cmd.ExecuteReader();
                    
                    DataTable resultsDataTable = new DataTable();
                    resultsDataTable.Load(rdr);
                    
                    rdr.Dispose();
                    cmd.Dispose();

                    //string sql_text = "SELECT * FROM Module_ACC.dbo.SMT_Data_Stats WHERE structureParent NOT IN('The Chocolate Quarter')";
                    //
                    //SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    //SqlDataReader objrs = sqlComm.ExecuteReader();
                    //
                    //DataTable resultsDataTable = new DataTable();
                    //resultsDataTable.Load(objrs);
                    //
                    //objrs.Dispose();
                    //sqlComm.Dispose();

                    //worksheet["B4"].Text = "No Errors, Lets go!";


                    // Work out the Person Status's. We need two sets of data for each person status.
                    var personStatus_List = from x in resultsDataTable.AsEnumerable()
                                            group x by (string)x["accStatus_Text"]
                                            into g
                                            select g;

                    // Work out the Person Status's. We need two sets of data for each person status.
                    var incidentType_List = from x in resultsDataTable.AsEnumerable()
                        group x by (string)x["incidentType"]
                        into g
                        select g;


                    // The first thing we need is the unique structureParent column.
                    // From stackoverflow, this creates a list to be iterated through based on the
                    // distinct values that lie within structureParent column.

                    var structureParent_List = from x in resultsDataTable.AsEnumerable()
                                                group x by (string) x["structureParent"]
                                                into g
                                                select g;

                    foreach(var x in structureParent_List)
                    {

                        string structureParent = x.Key;

                        int currentRowPosition = 1;
                        const int revertRowPosition = 4;

                        


                        //Loop against the incident type

                        foreach (var type in incidentType_List)
                        {
                            string incidentType = type.Key;
                            string literalIncidentType = "";
                            string incidentType_String = "";

                            switch (incidentType)
                            {
                                case "INJ":
                                    literalIncidentType = ": Injury Summary Report";
                                    incidentType_String = "Injury";
                                    break;
                                case "NM":
                                    literalIncidentType = ": Near Miss Summary Report";
                                    incidentType_String = "Near Miss";
                                    break;
                                case "DEF":
                                    literalIncidentType = ": Defined Incident Summary Report";
                                    incidentType_String = "Defined Incident";
                                    break;
                                default:
                                    literalIncidentType = ": Error";
                                    incidentType_String = "Error";
                                    break;


                            }

                            worksheet[currentYAxis, currentXAxis].Text = datetime_text;
                            worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Merge();
                            worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Style = sectionHeaderDate;

                            currentXAxis += 20;
                            worksheet[currentYAxis, currentXAxis].Text = structureParent + literalIncidentType;
                            worksheet[currentYAxis, currentXAxis].Style = sectionHeaderSummary;


                            currentYAxis += 1;
                            currentYAxis += 1;
                            currentYAxis += 1;
                            currentYAxis += 1;
                            currentXAxis = revertXAxis;


                            // Each time we loop against the person status, defined incident runs the same amount of times.
                            // As defined incidents is against the structure rather than a person status, more logic is needed.

                            bool definedIncident_Used = false;

                            //Loop against the person status.
                            foreach (var status in personStatus_List)
                            {
                                string currentStatus = status.Key;

                                
                                if ((currentStatus != "N/A" && definedIncident_Used == false) || (incidentType == "DEF" && definedIncident_Used == false))
                                {

                                    //Create another list that has the children elements against the parent.
                                    // Uses LINQ .Notation (much eaisier to read than above).
                                    // The only bit thats hard to understand here is the select acts as a distinct, from the GroupBy.
                                    var structureChild_List = resultsDataTable.AsEnumerable()
                                        .Where(q => (string)q["structureParent"] == x.Key && (string)q["incidentType"] == incidentType)
                                        .GroupBy(structure => structure["structureChild"])
                                        .Select(structure => structure.First())
                                        .ToList();

                                    
                                    int maximumRowPosition_ForCount = 0;
                                    int resetRowPosition_ForCount = 0;

                                    foreach (var y in structureChild_List)
                                    {

                                        string current_StructureChild = y["structureChild"].ToString();
                                        

                                        //If there are more than 4 columns , revert X axis and push the y axis down.
                                        if (currentRowPosition > revertRowPosition)
                                        {

                                            int differenceInYAxis_Count = maximumRowPosition_ForCount - currentYAxis;
                                            if (differenceInYAxis_Count > 3)
                                            {

                                                currentYAxis += differenceInYAxis_Count + 5;
       
                                            }
                                            else
                                            {
                                                currentYAxis += 5;
                                            }

                                            currentXAxis = revertXAxis;

                                            currentRowPosition = 1;

                                            

                                        }

                                        resetRowPosition_ForCount = currentYAxis;

                                        //Set the basic information for the row.
                                        worksheet[currentYAxis, currentXAxis].Text = current_StructureChild;
                                        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Style = sectionSubHeader;

                                        if (incidentType == "DEF")
                                        {
                                            worksheet[currentYAxis - 1, currentXAxis + 5].Text = incidentType_String;
                                            currentStatus = "N/A";
                                        }
                                        else
                                        {
                                            worksheet[currentYAxis - 1, currentXAxis + 5].Text = currentStatus + " " + incidentType_String;
                                        }
                                        



                                        //overrides of style
                                        worksheet[currentYAxis - 1, currentXAxis + 5, currentYAxis - 1, currentXAxis + 8].Style = sectionSubBasic;
                                        worksheet[currentYAxis - 1, currentXAxis + 5, currentYAxis - 1, currentXAxis + 8].Style.Font.Bold = true;
                                        worksheet[currentYAxis - 1, currentXAxis + 5].Style.Font.Color = Color.FromArgb(99, 37, 35); ;
                                        worksheet[currentYAxis - 1, currentXAxis + 5, currentYAxis - 1, currentXAxis + 8].Style.Font.Size =8;


                                        // So we've got:
                                        // Incident Type (Injury, Near Miss, Defined Incident)
                                        // Person Status (Text for now)
                                        // Structure Parent (tier 3 atm)
                                        // Structure Child (tier 4 atm)

                                        // Now lets loop through the actual data (totalCount and textCount)
                                        var currentDataRow_List = resultsDataTable.AsEnumerable()
                                            .Where(q => (string)q["structureParent"] == x.Key 
                                                    && (string)q["incidentType"] == incidentType
                                                    && (string)q["structureChild"] == current_StructureChild
                                                    && (string)q["accStatus_Text"] == currentStatus)
                                            .ToList();

                                        // We must ensure that every structure is atleast 10 long
                                        int currentPosition = 1;
                                        const int minPosition = 10;


                                        foreach (var z in currentDataRow_List)
                                        {

                                            // This loop goes through each option and adds it in.

                                            string currentTotal = z["totalCount"].ToString();
                                            string currentText = z["textCount"].ToString();
                                            if (currentTotal != "0")
                                            {
                                                currentYAxis += 1;
                                                currentPosition += 1;

                                                worksheet[currentYAxis, currentXAxis].Text = currentTotal;
                                                worksheet[currentYAxis, currentXAxis + 1].Text = currentText;
                                                worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Style = sectionSubBasic;

                                                if (maximumRowPosition_ForCount < currentYAxis)
                                                {
                                                    maximumRowPosition_ForCount = currentYAxis;
                                                }

                                                if (maximumRowPosition_ForCount == 0)
                                                {
                                                    maximumRowPosition_ForCount = currentYAxis;
                                                }
                                            }
                                            
                                        }


                                        // as there is less than 10 items, lets add them in so the report looks nicer.
                                        while (currentPosition < minPosition)
                                        {

                                            
                                            currentYAxis += 1;
                                            worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 8].Style = sectionSubBasic;

                                            //If the position is 1 then no data has been put in.
                                            if (currentPosition == 1)
                                            {
                                                worksheet[currentYAxis, currentXAxis].Text = "0";
                                                worksheet[currentYAxis, currentXAxis + 1].Text = "No Data Found";
                                            }

                                            if (maximumRowPosition_ForCount < currentYAxis)
                                            {
                                                maximumRowPosition_ForCount = currentYAxis;
                                            }

                                            currentPosition += 1;
                                        }

                                  

                                        


                                        currentYAxis = resetRowPosition_ForCount;
                                        currentXAxis += 10;
                                        currentRowPosition += 1;

                                    }

                                    
                                    

                                    int differenceInYAxis_Structure = maximumRowPosition_ForCount - currentYAxis;
                                    if (differenceInYAxis_Structure > 3)
                                    {
                                        currentYAxis += differenceInYAxis_Structure + 5;
                                    }
                                    else
                                    {
                                        currentYAxis += 5;
                                    }
                                    
                                    currentXAxis = revertXAxis;
                                    currentRowPosition = 1;

                                    if (incidentType == "DEF")
                                    {
                                        definedIncident_Used = true;
                                    }

                                }

                                

                            }

                        }

                        

 
                        //revert the X axis to column "B"
                        currentXAxis = revertXAxis;

                        // Ensure thre is a gap between the rows.
                        currentYAxis += 10;

                    }

                    resultsDataTable.Dispose();


                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }





        worksheet[currentYAxis, currentXAxis, currentYAxis, currentXAxis + 50].ColumnWidthInChars = 4;


        // SAVE THE WORKBOOK

        // Save the Excel document in the current HTTP response stream

        string outFileName = workbookFormat == ExcelWorkbookFormat.Xls_2003 ?  "Monthly Report.xls" :  "Monthly Report.xlsx";

        HttpResponse httpResponse = HttpContext.Current.Response;

        // Prepare the HTTP response stream for saving the Excel document

        // Clear any data that might have been previously buffered in the output stream  
        httpResponse.Clear();

        // Set output stream content type for Excel 97-2003 (.xls) or Excel 2007(.xlsx)
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
                httpResponse.ContentType = "Application/x-msexcel";
            else
                httpResponse.ContentType = "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            // Add the HTTP header to announce the Excel document either as an attachment or inline
        httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", outFileName));

            // Save the workbook to the current HTTP response output stream
            // and close the workbook after save to release all the allocated resources
        try
            {
                workbook.Save(httpResponse.OutputStream);
            }
            catch (Exception ex)
            {
                // report any error that might occur during save
                Session["ErrorMessage"] = ex.Message;

                Response.Write(ex.Message);
                Response.End();
                //Response.Redirect("ErrorPage.aspx");
            }
            finally
            {
                // close the workbook and release the allocated resources
                workbook.Close();

            }

            // End the response and finish the execution of this page
            httpResponse.End();

        }

}















