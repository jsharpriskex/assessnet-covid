<%

sub assessment_review_signoff(var_module, var_reference, var_sub_reference, var_action_to, var_rev_date, var_tier2_id, var_tier3_id, var_assessment_status, var_command, var_edit_permission, var_displayuser_control, var_displayuser_validation, var_sw1, var_sw2, var_sw3)

    select case var_module
        case "RA"
            var_save_review_form_action = "rassessment_report.asp?ref=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "rassessment_report.asp?ref=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "rassessment_report.asp?ref=" & var_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 35, 1)

    case "RA_OC"
            var_save_review_form_action = "rassessment_extended_summary_report.asp?ref=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "rassessment_extended_summary_report.asp?ref=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "rassessment_extended_summary_report.asp?ref=" & var_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 35, 1)


        case "MH"
            var_save_review_form_action = "mh_report.asp?frm_reference=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "mh_report.asp?frm_reference=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "mh_report.asp?frm_reference=" & var_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 36, 1)

        case "COSHH"
            var_save_review_form_action = "coshh_report.asp?ref=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "coshh_report.asp?ref=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "coshh_report.asp?ref=" & var_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 37, 1)

        case "PR"
            var_save_review_form_action = "puwer_report.asp?reference=" & var_reference & "&temp_ref=" & var_sub_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "puwer_report.asp?reference=" & var_reference & "&temp_ref=" & var_sub_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "puwer_report.asp?reference=" & var_reference & "&temp_ref=" & var_sub_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 38, 1)

        case "FS"
            var_save_review_form_action = "firesafety_report.asp?ref=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "firesafety_report.asp?ref=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "firesafety_report.asp?ref=" & var_reference & "&cmd=sign"

            var_signoff_permission = mid(session("YOUR_LICENCE"), 39, 1)

        case "MS"
            var_save_review_form_action = "method_report.asp?frm_reference=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "method_report.asp?frm_reference=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "method_report.asp?frm_reference=" & var_reference & "&cmd=sign"

         '   var_signoff_permission = mid(session("YOUR_LICENCE"), 35, 1)

        case "HP"
            var_save_review_form_action = "hprofile_summary_report.asp?ref=" & var_reference & "&cmd=sav_rev_date"
            var_set_review_form_action = "hprofile_summary_report.asp?ref=" & var_reference & "&cmd=set_rev_date"
            var_set_signoff_form_action = "hprofile_summary_report.asp?ref=" & var_reference & "&cmd=sign"

         '   var_signoff_permission = mid(session("YOUR_LICENCE"), 35, 1)

        case else
            response.write "module type unknown for review control"
            exit sub
    end select


    if var_command = "set_rev_date" and var_edit_permission = true then

        if isnull(var_action_to) = true or var_action_to = "" then
            var_action_to = session("YOUR_ACCOUNT_ID")
        end if

        if var_rev_date = "Not Specified" or isnull(var_rev_date) = true or var_rev_date = "" then
            var_rev_date = date()
        end if

        if isdate(var_rev_date) then
            if var_module = "HP" then
                var_new_rev_date = dateadd("m", 1, now())
                var_response_text = "The next review date is automatically set for 1 months time"
            elseif len(session("ASSESSMENT_DEFAULT_REVIEW")) > 0 then
                var_new_rev_date = dateadd("m", session("ASSESSMENT_DEFAULT_REVIEW"), now())
                var_response_text = "The next review date is automatically set for " & session("ASSESSMENT_DEFAULT_REVIEW") & " months time, but can be altered now if required"
            else
                var_new_rev_date = var_rev_date
                var_response_text = "Unless previously specified the next review date is automatically set to the due date of the task that must be completed first"
            end if
        else
            var_response_text = "Please select the date for this assessment to be next reviewed"
        end if

        if var_module = "HP" then
            response.write "<h3>Profile Review Details</h3></div>"
        else
            response.write "<h3>Assessment Review Details</h3></div>"
        end if

            %>     


            <form name='frm_review' action='<% =var_save_review_form_action %>' onsubmit='return val_assessment_review(this)' method='post'>

            <div class='warning'>
                <table width='100%' cellpadding='2px' cellspacing='2px'>
                    <tr><td class='c'><br />To be next Reviewed by 
                        <% if session("LOCALISED_USERS") = "Y" then
                                call DisplayUSERInfo_location(var_action_to, "frm_review_owner", "", var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "", var_tier2_id, var_tier3_id, "", "", "", "")
                            else
                                call DisplayUSERInfo(var_action_to, "frm_review_owner", "", var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "") 
                            end if %> 
                        on 
                        <%
                            if var_module = "HP" then
                                response.write "<strong>" & left(var_new_rev_date,10) & "</strong><input type='hidden' name='frm_review_date_d' value='" & left(var_new_rev_date,2) & "' /><input type='hidden' name='frm_review_date_m' value='" & mid(var_new_rev_date,4,2) & "' /><input type='hidden' name='frm_review_date_y' value='" & mid(var_new_rev_date,7,4) & "' />"
                            else
                                call Autodate(var_new_rev_date,"frm_review_date_d","frm_review_date_m","frm_review_date_y","F") 
                            end if
                        %>
                    
                    <ul>
                        <li>Ownership of this <% if var_module="HP" then response.write "profile" else response.write "assessment" end if %> will be transferred to the person stated above.</li>
                        <li><% =var_response_text %></li>
                    </ul>
                    </td></tr>
                </table>
            </div>

            <div class='sectionarea'>
                <p class='c'><input type='submit' class='submit' name='frm_submit' value='Save Changes' /></p>
            </div>

            <%
                if var_displayuser_control = "RV" and var_action_to = "nreq" then
                    response.write "<script> document.getElementById(""frm_review_date_d"").disabled = true; document.getElementById(""frm_review_date_m"").disabled = true; document.getElementById(""frm_review_date_y"").disabled = true; </script>"
                end if
            %>

            </form>
                        
            <div class='sectionarea'>
            <% 
    elseif var_rev_date <> "Not Specified" then

        if datediff("d", dateadd("d", 30, date()), var_rev_date) < 0 and var_assessment_status <> "4" then
                   
            response.write "<div class='warningSP sig1'>" & _
                                "<img src='../../safety_programme/app/img/id-icon.gif' />" & _
                                "<h1>REVIEW REQUIRED</h1>"
            if var_module = "HP" then
                if datediff("d", date(), var_rev_date) < 0 then
                    response.write "<p>This profile has been identified as being <strong>OVERDUE</strong> for review.</p>"
                else
                    response.write "<p>This profile is due for review by " & var_rev_date & ".</p>"
                end if
            else
                if datediff("d", date(), var_rev_date) < 0 then
                    response.write "<p>This assessment has been identified as being <strong>OVERDUE</strong> for review.</p>"
                else
                    response.write "<p>This assessment is due for review by " & var_rev_date & ".</p>"
                end if
            end if
            if (var_action_to = session("YOUR_ACCOUNT_ID") or var_edit_permission = true) and session("disallow_report_review") <> "Y" then
                response.write "<form action='" & var_set_review_form_action & "' method='post'><input type='submit' name='frm_submit' value='Review' /></form>"
            end if
            response.write "</div> <br />"

        else

            if var_assessment_status = "4" and session("ASSESSMENT_SIGNOFF") = "Y" then

                objCommand.commandText = "EXEC Module_PEP.dbo.sp_get_structure_signoff_permission '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & var_module & "', '" & var_reference & "', '" & session("global_usr_perm") & "'"
               'response.write objcommand.commandtext
                set objRs = objCommand.execute
                allow_signoff = objRs("allow_signoff")
             '   response.write "..." & objrs("col1")
                set objRs = nothing

                'Return values
                '   2 - Location doesn't have signoff applied. Assessment is still status 4 because location previously had it assigned, so show the signoff box. Assessment status 4 is kept for legacy data on old assessments, if sign off was required before the new structure sign off permissions came in
                '   1 - Sign off enabled for the location and user has permissions on the location.  -  down to the signoff permissions (var_signoff_permission) above to determine if the button can be seen
                '   0 - Sign off enabled for the location but user doesn't have permissions on the location  -  signoff button won't show

                'these are the conditions that should allow sign off as of 26/10/2016:
                '- Does the area have sign off feature enabled to begin with 
                '- Do I as a user have access to sign off for that module in user manager 
                '- Do I as a user have access to edit the file 

                'if allow_signoff < 2 then
                    response.write "<div class='warningSP sig1'>" & _
                                        "<img src='../../safety_programme/app/img/id-icon.gif' />" & _
                                        "<h1>SIGN-OFF REQUIRED</h1>" & _
                                        "<p>This assessment must be digitally signed by an Authorised Person.</p>"
              ' response.write "..." & allow_signoff & "..." & var_signoff_permission & "..."
                    if (allow_signoff = 1 or allow_signoff = 2) and var_signoff_permission = "1" then
                        response.write "<form action='" & var_set_signoff_form_action & "' method='post'><input type='submit' name='frm_submit' value='Sign-off' /></form>"
                    end if
                    response.write "</div> <br />"
                'end if
            end if

        end if 

    else

            if var_assessment_status = "4" and session("ASSESSMENT_SIGNOFF") = "Y" then

                objCommand.commandText = "EXEC Module_PEP.dbo.sp_get_structure_signoff_permission '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & var_module & "', '" & var_reference & "', '" & session("global_usr_perm") & "'"
               'response.write objcommand.commandtext
                set objRs = objCommand.execute
                allow_signoff = objRs("allow_signoff")
             '   response.write "..." & objrs("col1")
                set objRs = nothing

                'Return values
                '   2 - Location doesn't have signoff applied. Assessment is still status 4 because location previously had it assigned, so show the signoff box. Assessment status 4 is kept for legacy data on old assessments, if sign off was required before the new structure sign off permissions came in
                '   1 - Sign off enabled for the location and user has permissions on the location.  -  down to the signoff permissions (var_signoff_permission) above to determine if the button can be seen
                '   0 - Sign off enabled for the location but user doesn't have permissions on the location  -  signoff button won't show

                'these are the conditions that should allow sign off as of 26/10/2016:
                '- Does the area have sign off feature enabled to begin with 
                '- Do I as a user have access to sign off for that module in user manager 
                '- Do I as a user have access to edit the file 

                'if allow_signoff < 2 then
                    response.write "<div class='warningSP sig1'>" & _
                                        "<img src='../../safety_programme/app/img/id-icon.gif' />" & _
                                        "<h1>SIGN-OFF REQUIRED</h1>" & _
                                        "<p>This assessment must be digitally signed by an Authorised Person.</p>"
              ' response.write "..." & allow_signoff & "..." & var_signoff_permission & "..."
                    if (allow_signoff = 1 or allow_signoff = 2) and var_signoff_permission = "1" then
                        response.write "<form action='" & var_set_signoff_form_action & "' method='post'><input type='submit' name='frm_submit' value='Sign-off' /></form>"
                    end if
                    response.write "</div> <br />"
                'end if
            end if

    end if

end sub


sub save_assessment_review_signoff (var_module, var_reference, var_sub_reference, var_command, var_sw1, var_sw2, var_sw3, var_sw4, var_sw5)

    select case var_module
        case "RA"
            var_assessment_mod_data = application("DBTABLE_RAMOD_DATA")
            var_assessment_review_mod_code = "RAR"
            var_assessment_type = "risk"
            var_assessment_search_reset = "SRCH_RA_RESET"
            var_assessment_report_page = "rassessment_report.asp?ref=" & var_reference

            TaskPri = calculateResidualRisk() 'specific to the module
        case "RA_OC"
            var_assessment_mod_data = application("DBTABLE_RAMOD_DATA")
            var_assessment_review_mod_code = "RAR"
            var_assessment_type = "risk"
            var_assessment_search_reset = "SRCH_RA_RESET"
            var_assessment_report_page = "rassessment_extended_summary_report.asp?ref=" & var_reference

            TaskPri = calculateResidualRisk() 'specific to the module

        case "MH"
            var_assessment_mod_data = application("DBTABLE_MHMOD_DATA")
            var_assessment_review_mod_code = "MHR"
            var_assessment_type = "manual handling"
            var_assessment_search_reset = "SRCH_MH_RESET"
            var_assessment_report_page = "mh_report.asp?frm_reference=" & var_reference

            TaskPri = calculateResidualRisk() 'specific to the module

        case "COSHH"
            var_assessment_mod_data = application("DBTABLE_COSHH_ENTRIES")
            var_assessment_review_mod_code = "CA2R"
            var_assessment_type = "COSHH"
            var_assessment_search_reset = "SRCH_COSHH_RESET"
            var_assessment_report_page = "coshh_report.asp?ref=" & var_reference

            TaskPri = "Not Stated"

        case "PR"
            var_reference = left(var_reference, len(var_reference)-2)

            var_assessment_mod_data = application("DBTABLE_PUWER_ENTRIES")
            var_assessment_review_mod_code = "PRR"
            var_assessment_type = "PUWER"
            var_assessment_search_reset = "SRCH_PUWER_RESET"
            var_assessment_report_page = "puwer_report.asp?reference=" & var_reference & "&temp_ref=" & var_sub_reference

            TaskPri = "Not Stated"

        case "FS"
            var_assessment_mod_data = application("DBTABLE_FSMOD_DATA")
            var_assessment_review_mod_code = "FSR"
            var_assessment_type = "fire risk"
            var_assessment_search_reset = "SRCH_FS_RESET"
            var_assessment_report_page = "firesafety_report.asp?ref=" & var_reference

            TaskPri = calculateResidualRisk() 'specific to the module
         case "MS"
            var_assessment_mod_data = application("DBTABLE_MS_DATA_V2")
            var_assessment_review_mod_code = "MSR"
            var_assessment_type = "method statement"
            var_assessment_search_reset = "SRCH_MS_RESET"
            var_assessment_report_page = "method_report.asp?frm_reference=" & var_reference

           TaskPri = "Not Stated"'specific to the module

        case "HP"
            var_assessment_mod_data = application("DBTABLE_HP_DATA")
            var_assessment_review_mod_code = "HPR"
            var_assessment_type = "hazard profile"
            var_assessment_search_reset = "SRCH_HP_RESET"
            var_assessment_report_page = "hprofile_summary_report.asp?ref=" & var_reference

            TaskPri = "Not Stated" 'calculateResidualRisk() 'specific to the module

        case else
        
      
        
            response.write "module unknown for review control"
            exit sub
    end select


    if var_command = "sav_rev_date" then

        if var_module = "HP" then
            var_review_date = dateadd("m",1,now())
        elseif request("frm_review_date_d") = "DD" or request("frm_review_date_m") = "MM" or request("frm_review_date_y") = "YYYY" then
            var_review_date = "NULL"
        else
            var_review_date = request("frm_review_date_d") & "/" & request("frm_review_date_m") & "/" & request("frm_review_date_y")
        end if
    
        var_record_owner = request("frm_review_owner")
        if var_record_owner = "nreq" then ' start of check if not req 
            'one off assessment

            if var_module = "PR" then var_reference = var_reference & "PR"
            'update history
            objCommand.Commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " (HisRelationref,corp_code,HisBy,HisType,HisDateTime) values('" & var_reference & "','" & Session("corp_code") & "','" & Session("YOUR_ACCOUNT_ID") & "','Reviewer set to One Off " & var_assessment_type & " Assessment', getdate())"
            objcommand.execute
            if var_module = "PR" then var_reference = left(var_reference, len(var_reference)-2)


           


            ' grab existing task
            objCommand.Commandtext = "select a.review_task_code, b.id as task_id from " & var_assessment_mod_data & " as a " & _
                                     "left join " & application("DBTABLE_TASK_MANAGEMENT") & " as b " & _
                                     "on a.corp_code = b.corp_code " & _
                                     "and a.review_task_code = b.task_mod_action_ref " & _
                                     " where a.corp_code ='" & session("corp_code") & "' and a.recordreference='" & var_reference & "' " 
            set objtaskrs = objcommand.execute
            
            if not objtaskrs.eof then 
                var_existing_task_code = objtaskrs("review_task_code")
                var_existing_task_id =  objtaskrs("task_id")
            else
                'we don't even have an Assessment
                call formError()
            end if

            if var_existing_task_code <> "nreq" then '--only necessary if there is a task there already 
                ' set task to complete and due date to today also set auto_kill = "1"
                objCommand.Commandtext =    "update " & application("DBTABLE_TASK_MANAGEMENT") & " set task_status='0', task_due_date=getdate(), auto_kill='1'  where corp_code ='" & session("corp_code") & "' and task_mod_action_ref='" & var_existing_task_code & "' " 
                objcommand.execute
                                   
                ' add note to existing task
                if len(var_existing_task_id) > 0 then
    	            Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " " & _
						                     "(corp_code,task_id,task_history_by,task_history,gendatetime) " & _
						                     "VALUES('" & Session("CORP_CODE") & "','" & var_existing_task_id & "','" & session("YOUR_ACCOUNT_ID") & "','" & "This task is no longer valid and was removed automatically','" & date() & " " & Time() & "')"
	                Objcommand.execute
                end if 
            end if
        
            ' update the main entry
            objcommand.commandtext = "UPDATE " & var_assessment_mod_data & " SET  status='1', review_task_code = 'nreq' WHERE corp_code = '" & session("corp_code") & "' and recordreference = '" & var_reference & "'"
            objcommand.execute

        else        
        
            objcommand.commandtext  = "SELECT per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " WHERE corp_code = '" & session("corp_code") & "' AND acc_ref = '" & var_record_owner & "'"
            set objrs = objcommand.execute
            if objrs.eof then
                var_record_owner_name = "Unknown"
            else
                var_record_owner_name = objrs("per_fname") & " " & objrs("per_sname")
            end if
    

            if var_module = "PR" then var_reference = var_reference & "PR"
            'update history
            objCommand.Commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " (HisMod, HisRelationref, corp_code, HisBy, HisType, HisDateTime) VALUES ('" & var_module & "', '" & var_reference & "', '" & Session("corp_code") & "', '" & Session("YOUR_ACCOUNT_ID") & "', 'Reviewer set to " & var_record_owner_name & " / Review Date set for " & var_review_date & "', getdate())"
            objcommand.execute
            if var_module = "PR" then var_reference = left(var_reference, len(var_reference)-2)
    

            ' grab existing task
            objCommand.Commandtext = "SELECT a.review_task_code, b.id AS task_id FROM " & var_assessment_mod_data & " AS a " & _
                                     "LEFT JOIN " & application("DBTABLE_TASK_MANAGEMENT") & " AS b " & _
                                     "  ON a.corp_code = b.corp_code " & _
                                     "  AND a.recordreference = b.task_mod_ref " & _
                                     "  AND a.review_task_code = b.task_mod_action_ref " & _
                                     "  AND b.auto_kill IS NULL " & _
                                     "WHERE a.corp_code = '" & session("corp_code") & "' AND a.recordreference = '" & var_reference & "' "     
            set objtaskrs = objcommand.execute
            if not objtaskrs.eof then 
                var_existing_task_code = objtaskrs("review_task_code")
                var_existing_task_id = objtaskrs("task_id")


                if isnull(var_existing_task_code) = false and isnull(var_existing_task_id) = false then
                    ' set task to complete and due date to today also set auto_kill = "1"
                    objCommand.Commandtext = "UPDATE " & application("DBTABLE_TASK_MANAGEMENT") & " SET task_status = '0', task_due_date = GETDATE(), auto_kill = '1' WHERE corp_code = '" & session("CORP_CODE") & "' AND task_mod_action_ref = '" & var_existing_task_code & "' AND id = '" & var_existing_task_id & "' "  
                    objcommand.execute
                                   
                    ' add note to existing task
                    if len(var_existing_task_id) > 0 then
    	                Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_TASK_HISTORY") & " " & _
            						             "(corp_code,task_id,task_history_by,task_history,gendatetime) " & _
			            			             "VALUES('" & Session("CORP_CODE") & "','" & var_existing_task_id & "','" & session("YOUR_ACCOUNT_ID") & "','" & "This task is no longer valid and was removed automatically','" & date() & " " & Time() & "')"
	                    Objcommand.execute
                    end if
                end if

            else
                'we don't even have an assessment
                call formError()
            end if
            


            ' get a random reference (50)
            while var_option_valid <> "Y"
                ' Generate option reference
                var_new_ref = generateRandom(12)

                ' ok first check the option reference does not exist
                Objcommand.commandtext = "SELECT id FROM " & var_assessment_mod_data & " WHERE review_task_code = '" & var_new_ref & "' " 
                SET Objrs = Objcommand.execute
                
                if objrs.eof then
                    var_option_valid = "Y"   
                else
                    var_option_valid = "N"
                end if
            wend
        
            ' set review_task_code = random reference
            sql_text = "UPDATE " & var_assessment_mod_data & " " & _
                       "SET review_task_code = '" & var_new_ref & "' "

           
            if session("ASSESSMENT_SIGNOFF") = "Y" and var_module <> "MS" and var_module <> "HP" then
                
                if var_module = "COSHH" then 
                    var_module_display = "CA"
                else
                    var_module_display = var_module
                end if


                objCommand.commandText = "EXEC Module_PEP.dbo.sp_get_structure_signoff_permission '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & var_module_display & "', '" & var_reference & "', '" & session("global_usr_perm") & "'"              
                set objRs = objCommand.execute
                allow_signoff = objRs("allow_signoff")
                set objRs = nothing

                if allow_signoff < 2 then
                    if var_module = "RA" or var_module = "RA_OC" or var_module = "COSHH" or var_module = "HP" then
                        sql_text = sql_text & ", status = '4' "
                    else
                        sql_text = sql_text & ", recordstatus = '4' "
                    end if
                else
                    if var_module = "RA" or var_module = "RA_OC" or var_module = "COSHH" or var_module = "HP"  then
                        sql_text = sql_text & ", status = '1' "
                    else
                        sql_text = sql_text & ", recordstatus = '1' "
                    end if
                end if                    
            elseif  var_module <> "MS" then
                if var_module = "RA" or var_module = "RA_OC" or var_module = "COSHH" or var_module = "HP" then
                    sql_text = sql_text & ", status = '1' "
                else
                    sql_text = sql_text & ", recordstatus = '1' "
                end if
            end if
            sql_text = sql_text & "WHERE corp_code = '" & session("CORP_CODE") & "' AND recordreference = '" & var_reference & "' " 
            objCommand.commandText = sql_text

            objcommand.execute
        
    
            '*****************************************************


            TaskModRef = var_reference ' Assessment Reference
            TaskModActionRef = var_new_ref
            TaskModActionUser = var_record_owner ' Actioned user account ID
            TaskDueDate = var_review_date ' Duh!
            TaskDescription = var_record_owner_name & " has been assigned to review " & var_assessment_type & " assessment " & var_reference ' Action
            TaskModActionEmailTo = ""
            TaskDefaultUserOnEmail = ""
            TaskPriorityText = ""
            TaskPriorityQty = ""
            TaskPriorityType = ""
                  
            'CALL ADD_ACTION    (var_assessment_review_mod_code, TaskModRef, TaskModActionRef, TaskModActionUser, TaskDueDate, TaskPri, TaskDescription)
            CALL ADD_ACTION_V2 (var_assessment_review_mod_code, TaskModRef, TaskModActionRef, TaskModActionUser, TaskDueDate, TaskPri, TaskDescription, TaskModActionEmailTo, TaskDefaultUserOnEmail, TaskPriorityText, TaskPriorityQty, TaskPriorityType, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "")
        end if

        if var_module = "PR" then
            var_reference = var_reference & "PR"
        end if
        call endconnections()
        Session(var_assessment_search_reset) = "Y"  
        response.Redirect(var_assessment_report_page)
        response.End


    elseif var_command = "sign" then

        if var_module = "PR" then var_reference = var_reference & "PR"
        'update history
        objCommand.Commandtext = "INSERT INTO " & Application("DBTABLE_GLOBAL_FPRINT") & " (HisMod, HisRelationref, corp_code, HisBy, HisType, HisDateTime) VALUES ('" & var_module & "', '" & var_reference & "', '" & Session("corp_code") & "', '" & Session("YOUR_ACCOUNT_ID") & "', 'SIGNED OFF', getdate())"
        objcommand.execute
        if var_module = "PR" then var_reference = left(var_reference, len(var_reference)-2)

        'set the assessment as complete
        if var_module = "RA" or var_module = "RA_OC" or var_module = "COSHH" or var_module = "HP" then
            objCommand.commandText = "UPDATE " & var_assessment_mod_data & " SET status = '1' WHERE corp_code = '" & session("CORP_CODE") & "' AND recordreference = '" & var_reference & "'"
        else
            objCommand.commandText = "UPDATE " & var_assessment_mod_data & " SET recordstatus = '1' WHERE corp_code = '" & session("CORP_CODE") & "' AND recordreference = '" & var_reference & "'"
        end if
        objCommand.execute

        if var_module = "PR" then
            var_reference = var_reference & "PR"
        end if
        call endconnections()
        Session(var_assessment_search_reset) = "Y"  
        response.Redirect(var_assessment_report_page)
        response.End

    end if

end sub


sub get_signoff_details (var_reference, var_sub_reference, var_sw1, var_sw2, var_sw3, var_sw4, var_sw5)

    objCommand.commandText = "SELECT TOP 1 fprint.HisType, fprint.HisDateTime, users.per_fname + ' ' + users.per_sname AS per_name FROM " & Application("DBTABLE_GLOBAL_FPRINT") & " AS fprint " & _
                             "INNER JOIN " & Application("DBTABLE_USER_DATA") & " AS users " & _
                             "  ON fprint.corp_code = users.corp_code " & _
                             "  AND fprint.hisby = users.acc_ref " & _
                             "WHERE fprint.corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND fprint.HisRelationRef = '" & var_reference & "' " & _
                             "  AND fprint.HisType NOT LIKE 'View%' " & _
                             "  AND fprint.HisType NOT LIKE '%Download%' " & _
                             "ORDER BY fprint.HisDateTime DESC"
    set objRsHis = objCommand.execute

    var_assessment_signed_by = "This assessment has not yet been signed off"
    var_assessment_signed_date = " -- "

    if not objRsHis.EOF then
        if ucase(objRsHis("HisType")) = "SIGNED OFF" then
            var_assessment_signed_by = objRsHis("per_name")
            var_assessment_signed_date = left(objRsHis("hisdatetime"), 10)
        end if
    end if

end sub




sub assessment_review_signoff_form(var_action_to, var_action_to_name, var_rev_date, var_rev_date_name, var_tier2_id, var_tier3_id, var_section_status, var_displayuser_control, var_displayuser_validation, var_show_reopen, var_section_id, var_recordreference)

    if isnull(var_action_to) = true or var_action_to = "" then
        var_action_to = session("YOUR_ACCOUNT_ID")
    end if

    if var_rev_date = "Not Specified" or isnull(var_rev_date) = true or var_rev_date = "" or var_rev_date = "NONE" then
        var_rev_date = date()
    end if

    if isdate(var_rev_date) then
        if len(session("ASSESSMENT_DEFAULT_REVIEW")) > 0 then
            var_new_rev_date = dateadd("m", session("ASSESSMENT_DEFAULT_REVIEW"), now())
            var_response_text = "The next review date is automatically set for " & session("ASSESSMENT_DEFAULT_REVIEW") & " months time, but can be altered now if required"
        else
            var_new_rev_date = var_rev_date
            var_response_text = "Unless previously specified the next review date is automatically set to the due date of the task that must be completed first"
        end if
    else
        var_response_text = "Please select the date for this assessment to be next reviewed"
    end if

    if var_show_reopen = "Y" then
        response.write "<table class='h3_" & trim(var_section_status) & "' width='100%'><tr><td nowrap class='noborder'><h3 class='noborder' style='margin-bottom: 0px; width: 99%'>Assessment Review Details</h3></td>"
        if trim(var_section_status) = "disabled" and len(var_section_id) > 0 then
            response.write "<td class='r noborder' nowrap><input type='button' class='submit' name='frm_reopen_SR' value='Open this section' onclick='javascript:ReOpenMain(""" & var_recordreference & """, """ & var_section_id & """, """");' /></td>"
        end if
        response.write "</tr></table>"
    
    elseif var_show_reopen = "DISABLED" then
        response.write "<table class='h3_" & trim(var_section_status) & "' width='100%'><tr><td nowrap class='noborder'><h3 class='noborder' style='margin-bottom: 0px; width: 99%'>Assessment Review Details</h3></td>"
        if trim(var_section_status) = "disabled" and len(var_section_id) > 0 then
            response.write "<td class='r noborder' nowrap><input type='button' class='submit' name='frm_reopen_SR' value='Open this section' onclick='javascript:ReOpenMain(""" & var_recordreference & """, """ & var_section_id & """, """");' disabled title='Please use the save and continue button to proceed' /></td>"
        end if
        response.write "</tr></table>"
    else
        response.write "<h3>Assessment Review Details</h3>"
    end if


    %>
        <div class='warning <% =var_section_status %>'>
            <table width='100%' cellpadding='2px' cellspacing='2px'>
                <tr><td class='c'><br />To be next reviewed by 
                    <% if session("LOCALISED_USERS") = "Y" then
                            call DisplayUSERInfo_location(var_action_to, var_action_to_name, var_section_status, var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "", var_tier2_id, var_tier3_id, "", "", "", "")
                        else
                            call DisplayUSERInfo(var_action_to, var_action_to_name, var_section_status, var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "") 
                        end if %> 
                    on <% call Autodate(var_new_rev_date, var_rev_date_name & "_d", var_rev_date_name & "_m", var_rev_date_name & "_y", "F", var_section_status, "", "") %>
                    
                <ul>
                    <li>Ownership of this assessment will be transferred to the person stated above.</li>
                    <li><% =var_response_text %></li>
                </ul>
                </td></tr>
            </table>
        </div>

   <%

    if var_displayuser_control = "RV" and var_action_to = "nreq" then
        response.write "<script> document.getElementById(""" & var_rev_date_name & "_d"").disabled = true; document.getElementById(""" & var_rev_date_name & "_m"").disabled = true; document.getElementById(""" & var_rev_date_name & "_y"").disabled = true; </script>"
    end if

end sub



sub assessment_review_signoff_form_v2(var_action_to, var_action_to_name, var_rev_date, var_rev_date_name, var_tier2_id, var_tier3_id, var_section_status, var_displayuser_control, var_displayuser_validation, var_show_reopen, var_section_id, var_recordreference, var_tier4_id, var_tier5_id, var_tier6_id, var_module)
    
    if var_module = "HP" then
        var_module_type = "Profile"
    else
        var_module_type = "Assessment"
    end if

    if isnull(var_action_to) = true or var_action_to = "" then
        var_action_to = session("YOUR_ACCOUNT_ID")
    end if

    if var_rev_date = "Not Specified" or isnull(var_rev_date) = true or var_rev_date = "" or var_rev_date = "NONE" then
        var_rev_date = date()
    end if

    if isdate(var_rev_date) then
       if var_module = "HP" then
            var_new_rev_date = dateadd("m", 1, now())
            var_response_text = "The next review date is automatically set for 1 months time"
       elseif len(session("ASSESSMENT_DEFAULT_REVIEW")) > 0 then
            var_new_rev_date = dateadd("m", session("ASSESSMENT_DEFAULT_REVIEW"), now())
            var_response_text = "The next review date is automatically set for " & session("ASSESSMENT_DEFAULT_REVIEW") & " months time, but can be altered now if required"
        else
            var_new_rev_date = var_rev_date
            var_response_text = "Unless previously specified the next review date is automatically set to the due date of the task that must be completed first"
        end if
    else
        var_response_text = "Please select the date for this assessment to be next reviewed"
    end if

    if var_show_reopen = "Y" then
        response.write "<table class='h3_" & trim(var_section_status) & "' width='100%'><tr><td nowrap class='noborder'><h3 class='noborder' style='margin-bottom: 0px; width: 99%'>" & var_module_type & " Review Details</h3></td>"
        if trim(var_section_status) = "disabled" and len(var_section_id) > 0 then
            response.write "<td class='r noborder' nowrap><input type='button' class='submit' name='frm_reopen_SR' value='Open this section' onclick='javascript:ReOpenMain(""" & var_recordreference & """, """ & var_section_id & """, """");' /></td>"
        end if
        response.write "</tr></table>"
    
    elseif var_show_reopen = "DISABLED" then
        response.write "<table class='h3_" & trim(var_section_status) & "' width='100%'><tr><td nowrap class='noborder'><h3 class='noborder' style='margin-bottom: 0px; width: 99%'>" & var_module_type & " Review Details</h3></td>"
        if trim(var_section_status) = "disabled" and len(var_section_id) > 0 then
            response.write "<td class='r noborder' nowrap><input type='button' class='submit' name='frm_reopen_SR' value='Open this section' onclick='javascript:ReOpenMain(""" & var_recordreference & """, """ & var_section_id & """, """");' disabled title='Please use the save and continue button to proceed' /></td>"
        end if
        response.write "</tr></table>"

    else
        response.write "<h3>" & var_module_type & " Review Details</h3>"
    end if


    %>
        <div class='warning <% =var_section_status %>'>
            <table width='100%' cellpadding='2px' cellspacing='2px'>
                <tr><td class='c'><br />To be next reviewed by 
                    <%  if session("LOCALISED_USERS") = "Y" then
                            call DisplayUSERInfo_location(var_action_to, var_action_to_name, var_section_status, var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "", var_tier2_id, var_tier3_id, "", "", "", "")
                        
                        else
                            'call DisplayUSERInfo(var_action_to, var_action_to_name, var_section_status, var_displayuser_control, var_displayuser_validation, "N", "TM,RO", "") 
                            call DisplayUSERInfoV3(var_action_to, var_action_to_name, var_section_status, "RV", var_displayuser_validation, "N", "", "", var_tier2_id, var_tier3_id, var_tier4_id, var_tier5_id, var_tier6_id, var_module, "", sub_sw2, sub_sw3, sub_sw4, sub_sw5)
                            
                         

                        end if %> 
                    on 
                    <%
                        if var_module = "HP" then
                            response.write "<strong>" & left(var_new_rev_date,10) & "</strong><input type='hidden' name='frm_review_date_d' value='" & left(var_new_rev_date,2) & "' /><input type='hidden' name='frm_review_date_m' value='" & mid(var_new_rev_date,4,2) & "' /><input type='hidden' name='frm_review_date_y' value='" & mid(var_new_rev_date,7,4) & "' />"
                        else
                            call Autodate(var_new_rev_date, var_rev_date_name & "_d", var_rev_date_name & "_m", var_rev_date_name & "_y", "F", var_section_status, "", "")  
                        end if
                    %>
                    
                <ul style='list-style: none;'>
                    <li>Ownership of this <% if var_module="HP" then response.write "profile" else response.write "assessment" end if %> will be transferred to the person stated above.</li>
                    <li><% =var_response_text %></li>
                </ul>
                </td></tr>
            </table>
        </div>

   <%

    if var_displayuser_control = "RV" and var_action_to = "nreq" then
        response.write "<script> document.getElementById(""" & var_rev_date_name & "_d"").disabled = true; document.getElementById(""" & var_rev_date_name & "_m"").disabled = true; document.getElementById(""" & var_rev_date_name & "_y"").disabled = true; </script>"
    end if

end sub

%>