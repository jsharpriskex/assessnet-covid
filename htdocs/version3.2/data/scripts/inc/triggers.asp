﻿<%
'************Triggers***********************
sub trig_incident_validated(accbref, incidentRef)
    
    'corp codes
    if session("corp_code") = "222999" or session("corp_code") = "055405" then
        call aspNET_Event("event=notify_mondelez_validators_of_validation&corp_code=" & session("corp_code") & "&accbref=" & accbref & "&incidentRef=" & incidentRef & "&validatorid=" & session("YOUR_ACCOUNT_ID"))
       ' call aspNET_Event("event=notify_mondelez_investigators_of_validation&corp_code=" & session("corp_code") & "&accbref=" & accbref & "&incidentRef=" & incidentRef & "&validatorid=" & session("YOUR_ACCOUNT_ID"))
    end if
    'Preferences


    'Generic code

end sub


sub trig_investigation_started(accbref, incidentRef, investigationRef)
    
    'corp codes
    if session("corp_code") = "222999" or session("corp_code") = "055405" then
         call aspNET_Event("event=notify_mondelez_investigators_HSE_Lead_of_investigation_start&corp_code=" & session("corp_code") & "&accbref=" & accbref & "&incidentRef=" & incidentRef & "&investigatorid=" & session("YOUR_ACCOUNT_ID"))
    end if

    'Preferences


    'Generic code

end sub


sub trig_investigation_completed(accbref, incidentRef, investigationRef)
    
    'corp codes
    if session("corp_code") = "222999" or session("corp_code") = "055405" then
        call aspNET_Event("event=notify_mondelez_validators_of_investigation_complete&corp_code=" & session("corp_code") & "&accbref=" & accbref & "&incidentRef=" & incidentRef )
    end if

    'Preferences


    'Generic code

end sub

sub trig_incident_signed_off(accbref, incidentRef)
    
    'corp codes
    if session("corp_code") = "055405" then
        call aspNET_Event("event=notify_mondelez_cat_lead_of_incident_signed_off&corp_code=" & session("corp_code") & "&accbref=" & accbref & "&incidentRef=" & incidentRef )
    end if

    'Preferences
    if session("ACC_SIGNOFF_V2") = "Y" then
        'call aspNET_Event("event=notify_signoff_v2_assignment&corp_code=" & session("CORP_CODE") & "&accbref=" & accbref & "&incidentRef=" & incidentRef )

        'process re-coded as an sql job. runs every 5 minutes on the DB server
        objCommand.commandText = "INSERT INTO Module_ACC.dbo.notification_processing (corp_code, accb_code, recordreference, processing_type, date_added, added_by) " & _
                                 "VALUES ('" & session("CORP_CODE") & "', '" & accbref & "', '" & incidentref & "', 'signoff-roles', GETDATE(), '" & session("YOUR_ACCOUNT_ID") & "')"
        objCommand.execute

    end if

    if session("ACC_SIGNOFF_NOTIFY_IP") = "Y" then
        'process re-coded as an sql job. runs every 5 minutes on the DB server
        objCommand.commandText = "INSERT INTO Module_ACC.dbo.notification_processing (corp_code, accb_code, recordreference, processing_type, date_added, added_by) " & _
                                 "VALUES ('" & session("CORP_CODE") & "', '" & accbref & "', '" & incidentref & "', 'signoff-IP', GETDATE(), '" & session("YOUR_ACCOUNT_ID") & "')"
        objCommand.execute
        
    end if

    'Generic code

end sub


function trig_manual_allocation_of_incident_type(accbref, incidentRef)
   
    var_set_flash_alert = false

    'corp_codes
    'if session("CORP_CODE") = "061606" then
    '    call aspNET_Event("event=notify_incident_type_assignment&corp_code=" & session("CORP_CODE") & "&accbref=" & accbref & "&incidentRef=" & incidentRef )
    '    var_set_flash_alert = true
    'end if

    if session("ACC_SEND_FLASH_ALERT") = "Y" or session("CORP_CODE") = "222999" or session("CORP_CODE") = "065912" or session("CORP_CODE") = "061606" or session("CORP_CODE") = "800002" or session("CORP_CODE") = "053938" then
        'process re-coded as an sql job. runs every 5 minutes on the DB server

        'check if any incident types have been added to the incident.
        'if not, don't send the flash alert and don't popup on screen
        'if they have, check if any of them are complete, otherwise don't popup on screen

        '**************************************************************
        'update these scripts to accomodate new incident type additions
        'also update the flash alert notification sproc to incorporate 
        'the new incident types details in the email notification
        '**************************************************************

        objCommand.commandText = "SELECT inj_value, id_value, do_value, dam_value, nm_value, vi_value, env_value, sec_value, siir_value, defined_value, equip_value " & _
                                 "FROM " & Application("DBTABLE_ACC_DATA") & " " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND accb_code = '" & accbref & "' " & _
                                 "  AND recordreference = '" & incidentref & "' "
        set objRsCount = objCommand.execute
        if not objRsCount.EOF then

            if objRsCount("inj_value") = "Y" or objRsCount("id_value") = "Y" or objRsCount("do_value") = "Y" or objRsCount("dam_value") = "Y" or objRsCount("nm_value") = "Y" or _
            objRsCount("vi_value") = "Y" or objRsCount("env_value") = "Y" or objRsCount("sec_value") = "Y" or objRsCount("siir_value") = "Y" or objRsCount("defined_value") = "Y" or objRsCount("equip_value") = "Y" then
    
                objCommand.commandText = "SELECT COUNT(accb_code) AS recordFound FROM " & Application("DBTABLE_ACC_DATA") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND accb_code = '" & accbref & "' " & _
                                         "  AND recordreference = '" & incidentref & "' " & _
                                         "  AND (comp_inj_total > 0 OR comp_id_total > 0 OR comp_do_total > 0 OR comp_dam_total > 0 OR comp_nm_total > 0 OR comp_vi_total > 0 OR comp_fi_total > 0 OR comp_env_total > 0 OR comp_sec_total > 0 OR comp_siir_total > 0 OR comp_defined_total > 0 OR comp_equip_total > 0)"
                set objRsCount = objCommand.execute
                if objRsCount("recordFound") > 0 then
                    objCommand.commandText = "INSERT INTO Module_ACC.dbo.notification_processing (corp_code, accb_code, recordreference, processing_type, date_added, added_by) " & _
                                             "VALUES ('" & session("CORP_CODE") & "', '" & accbref & "', '" & incidentref & "', 'flash-alert', GETDATE(), '" & session("YOUR_ACCOUNT_ID") & "')"
                    objCommand.execute

                    var_set_flash_alert = true
                else
                    var_set_flash_alert = false
                end if
            else
                var_set_flash_alert = false
            end if

        end if
        set objRsCount = nothing

    end if
    
    'set the values so the flash check doesn't continue to take place.
    if var_set_flash_alert = true then        

        objCommand.commandText = "UPDATE " & Application("DBTABLE_ACC_DATA") & " SET flash_notification_sent = 1 " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND accb_code = '" & accbref & "' " & _
                                 "  AND recordreference = '" & incidentRef & "'"
        objCommand.execute

        'history will be updated when the flash notification is sent (SEE END OF SPROC)

    end if
   
    trig_manual_allocation_of_incident_type = var_set_flash_alert

end function


sub trig_custom_incident_reference(accbref, incidentRef)

    if (session("CORP_CODE") = "222999" or session("CORP_CODE") = "065912") and session("ACC_USE_ALTERNATE_REFERENCE") = "Y" then

        ' format for Ocado custom reference 
        ' [division shortcode][Incident Type][IP Initials][DDMMYYY][HHMM]
        ' where multiple individuals are involved, just initials of first recorded

        'find the division shortcode]
        objCommand.commandText = "SELECT acc_data.alternatereference, upper(left(tier2.internal_reference,2)) AS int_ref, acc_data.recorddate, CASE WHEN inj_value = 'Y' THEN 'AC' ELSE CASE WHEN ID_VALUE = 'Y' THEN 'IH' ELSE CASE WHEN DO_VALUE = 'Y' THEN 'DO' ELSE CASE WHEN NM_VALUE = 'Y' THEN 'NM' ELSE 'IC' END END END END AS event_type " & _
                                 "FROM " & Application("DBTABLE_ACC_DATA") & " AS acc_data " & _
                                 "INNER JOIN " & Application("DBTABLE_HR_DATA_ABOOK") & " AS abooks " & _
	                             "  ON acc_data.corp_code = abooks.corp_code " & _
	                             "  AND acc_data.accb_code = abooks.accb_code " & _
                                 "INNER JOIN " & Application("DBTABLE_STRUCT_TIER2") & " AS tier2 " & _
                                 "  ON acc_data.corp_code = tier2.corp_code " & _
                                 "  AND acc_data.rec_bu = tier2.tier2_ref " & _
                                 "WHERE acc_data.corp_code = '" & session("CORP_CODE") & "' " & _
	                             "  AND acc_data.accb_code = '" & accbref & "' " & _
	                             "  AND acc_data.recordreference = '" & incidentRef & "' "
        set objRsRef = objCommand.execute
        if not objRsRef.EOF then

            if objRsRef("alternatereference") = "UNDEFINED" or isnull(objRsRef("alternatereference")) = true or objRsRef("alternatereference") = "" or instr(1, objRsRef("alternatereference"), "%%") > 0 then

                select case objRsRef("event_type")
                    case "AC" 'injury
                        objCommand.commandText = "SELECT TOP 1 upper(left(injfname,1)) AS per_fname, upper(left(injsname,1)) AS per_sname " & _
                                                 "FROM " & Application("DBTABLE_INJ_DATA") & " " & _
                                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND accb_code = '" & accbref & "' " & _
                                                 "  AND recordreference = '" & incidentRef & "' " & _
                                                 "ORDER BY id ASC "
                        set objRsPer = objCommand.execute
                        if not objRsPer.EOF then
                            if len(objRsPer("per_fname")) > 0 and isnull(objRsPer("per_fname")) = false then
                                var_custom_reference_initials = objRsPer("per_fname") & objRsPer("per_sname")
                            else
                                var_custom_reference_initials = "%%"
                            end if
                        else
                            var_custom_reference_initials = "%%"
                        end if
                        set objRsPer = nothing

                    case "IH" 'illness
                        objCommand.commandText = "SELECT TOP 1 upper(left(idfname,1)) AS per_fname, upper(left(idsname,1)) AS per_sname " & _
                                                 "FROM " & Application("DBTABLE_ID_DATA") & " " & _
                                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND accb_code = '" & accbref & "' " & _
                                                 "  AND recordreference = '" & incidentRef & "' " & _
                                                 "ORDER BY id ASC "
                        set objRsPer = objCommand.execute
                        if not objRsPer.EOF then
                            if len(objRsPer("per_fname")) > 0 and isnull(objRsPer("per_fname")) = false then
                                var_custom_reference_initials = objRsPer("per_fname") & objRsPer("per_sname")
                            else
                                var_custom_reference_initials = "%%"
                            end if
                        else
                            var_custom_reference_initials = "%%"
                        end if
                        set objRsPer = nothing

                    case "NM" 'Near Miss
                        objCommand.commandText = "SELECT TOP 1 upper(left(nmissplus_aff_fname,1)) AS per_fname, upper(left(nmissplus_aff_sname,1)) AS per_sname " & _
                                                 "FROM " & Application("DBTABLE_NM_DATA") & " " & _
                                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                                 "  AND accb_code = '" & accbref & "' " & _
                                                 "  AND recordreference = '" & incidentRef & "' " & _
                                                 "ORDER BY id ASC "

                        set objRsPer = objCommand.execute
                        if not objRsPer.EOF then
                            if len(objRsPer("per_fname")) > 0 and isnull(objRsPer("per_fname")) = false then
                                var_custom_reference_initials = objRsPer("per_fname") & objRsPer("per_sname")
                            else
                                var_custom_reference_initials = "%%"
                            end if
                        else
                            var_custom_reference_initials = "%%"
                        end if
                        set objRsPer = nothing

                    case "DO" 'Dangerous Occurence
                        var_custom_reference_initials = "%%"
                    
                    case "IC" 'Other incident
                        var_custom_reference_initials = "%%"
                    case else
                        var_custom_reference_initials = "%%"
                end select

                var_custom_reference = objRsRef("int_ref") & "/" & objRsRef("event_type") & "/" & var_custom_reference_initials & "/" & mid(objRsRef("recorddate"),1,2) & mid(objRsRef("recorddate"),4,2) & mid(objRsRef("recorddate"),7,4) & "/" & mid(objRsRef("recorddate"),12,2) & mid(objRsRef("recorddate"),15,2)

                'put the new reference into the database
                objCommand.commandText = "UPDATE " & Application("DBTABLE_ACC_DATA") & " SET alternatereference = '" & var_custom_reference & "' " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND accb_code = '" & accbref & "' " & _
                                         "  AND recordreference = '" & incidentRef & "'"
                objCommand.execute

            end if
        else
            'fail input - enter dummy value into DB 
            var_custom_reference = "UNDEFINED"

            'put the new reference into the database
            objCommand.commandText = "UPDATE " & Application("DBTABLE_ACC_DATA") & " SET alternatereference = 'UNDEFINED' " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND accb_code = '" & accbref & "' " & _
                                     "  AND recordreference = '" & incidentRef & "'"
            objCommand.execute
        end if
        set objRsRef = nothing

    end if

end sub


'***********End of Triggers*****************



'****************Events***********************
'Some non classic asp events may be found in version3.2/trigger_events.aspx
sub aspNET_Event(PostData)
    
 '   Set ServerXmlHttp = Server.CreateObject("Msxml2.ServerXMLHTTP.6.0")
  Set ServerXmlHttp = Server.CreateObject("MSXML2.ServerXMLHTTP") 
     
    ServerXmlHttp.open "POST", Application("CONFIG_PROVIDER_SECURE") & "/version" & Application("CONFIG_VERSION") & "/trigger_events.aspx", false
    ServerXmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    ServerXmlHttp.setRequestHeader "Content-Length", Len(PostData)
    ServerXmlHttp.send PostData
       
    Set ServerXmlHttp = Nothing

end sub

'**************End of Events******************
 %>