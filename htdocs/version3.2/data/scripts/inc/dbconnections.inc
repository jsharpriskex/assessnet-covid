<%

SUB endCONNECTIONS()

	Objconn.close
	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING

END SUB

SUB startCONNECTIONS()

	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.Open
	END WITH

	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn
	objcommand.CommandTimeout = 300
	
END SUB

function validateInput(inputValue)
	if not isnull(inputValue) then
		if session("CORP_CODE") = "055405" then
			validateInput = replace(inputValue,"'","`")
		else
			validateInput = replace(server.HTMLEncode(inputValue),"'","`")
		end if	
	else
		validateInput = inputValue
	end if
end function

%>
