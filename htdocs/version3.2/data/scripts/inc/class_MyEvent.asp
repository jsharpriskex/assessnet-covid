<%
' Create event class
Class MyEvent
    Private id
    Private startTime
    Private endTime
    Private category
    Private description
    Private drawingHasBegun
    
    Public Function init(myId,myStartTime,myEndTime,myCategory,myDescription)
        id = myId
        startTime = myStartTime
        endTime = myEndTime
        category = myCategory
        description = myDescription
        drawingHasBegun = false
    End Function
    
    Public Function hasDrawingBegun()
        hasDrawingBegun = drawingHasBegun
    End Function
    
    Public Function startDrawing()
        drawingHasBegun = true
    End Function
    
    Public Function getDescription()
        getDescription = description
    End Function
    
    Public Function getCategory()
        getCategory = category
    End Function
    
    Public Function getId()
        getId = id
    End Function
    
    Public Function getPrintableStartTime
        If Minute(startTime) < 10 Then
            getPrintableStartTime = Hour(startTime) & ":0" & Minute(startTime)
        Else
            getPrintableStartTime = Hour(startTime) & ":" & Minute(startTime)
        End if
    End Function
    
    Public Function getPrintableEndTime
        If Minute(endTime) < 10 Then
            getPrintableEndTime = Hour(endTime) & ":0" & Minute(endTime)
        Else
            getPrintableEndTime = Hour(endTime) & ":" & Minute(endTime)
        End if
    End Function
    
    ' Checks whether event should cover a given cell
    Public Function isInTimeFrame(frameStart,frameEnd)
'        Response.Write " endTime (" & endTime & ") > frameStart(" & frameStart & ") = " & (endTime > frameStart) & "<br />"
'        Response.Write " startTime (" & startTime & ") < frameEnd(" & frameEnd & ") = " & (startTime < frameEnd) & "<br />"
        If (CDate(endTime) > frameStart) AND (CDate(startTime) < frameEnd) Then
            isInTimeFrame = true
        Else
            isInTimeFrame = false
        End if
    End Function
    
    ' Returns number of cells/rows/time slots this event covers
    Public Function getNumberOfFrames(userStartHour,userEndHour)
        Dim numWholeHours, numFrames, numFramesLess
        numWholeHours = Hour(endTime) - Hour(startTime)
        numFrames = (numWholeHours + 1) * 2
        
        ' Now take into account that event may start or finish before or after users chosen hours
        numFramesLess = 0
        If userStartHour > Hour(startTime) Then
            numFramesLess = (userStartHour - Hour(startTime)) * 2
            If Minute(startTime) > 30 Then
                numFramesLess = numFramesLess + 1
            End if
        Else
            If Minute(startTime) > 29 Then
                numFrames = numFrames - 1
            End if
            If Minute(endTime) < 31 Then
                numFrames = numFrames - 1
            End if
        End if
        
'        Response.Write " (NumFrames: " & numFrames - numFramesLess & ")"
        
        getNumberOfFrames = numFrames - numFramesLess
    End Function
End class
%>