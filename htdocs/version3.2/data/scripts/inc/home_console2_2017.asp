<%



Response.buffer=true
Response.Expires = -1
Response.ExpiresAbsolute = Now() -1 
Response.AddHeader "pragma", "no-store"
Response.AddHeader "cache-control","no-store, no-cache, must-revalidate"

'dictionary
dim mods
Set mods=Server.CreateObject("Scripting.Dictionary")

mods.add "RA", "Risk Assessments"
mods.add "TM", "Your Tasks"
mods.add "ACC", "Accident / Incident Reporting"
mods.add "MH", "Manual Handling"
mods.add "MS", "Method Statements"
mods.add "FS", "Fire Risk Assessments"
mods.add "COSHH", "COSHH Assessments"
mods.add "MSDS", "Material Safety Data Sheets (MSDS)"
mods.add "DSE", "Display Screen Equipment Assessments"
mods.add "INS", "Safety Inspections"
mods.add "PTW", "Permits To Work"
mods.add "SA", "Safety Audits"


'""functions

sub add_perm(module_code, position)
    objcommand.commandtext = "insert into " & Application("DBTABLE_GLOBAL_CONSOLE") & " (corp_code, acc_ref, module, position) values " & _
                             "('" & session("corp_code") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & module_code & "', '" & position & "')"
    objcommand.execute
end sub


'subs
sub populate_console(mod_type, sub_sw2, sub_sw3, sub_sw4, sub_sw5)

    if session("HIDE_HOMEPAGE_STATS") <> "Y" then

   '"mod_type = Module - if all then show all mods
    'sub_sw2 = empty
   '"sub_sw3 = empty
    '"sub_sw5 = Empty
    '"sub_sw6 = Empty
    
     mycount = 0
     
    ' currently this looks at the access level of the viewer but an update will allow this to be set so GA""s can look at their own tasks for example
    var_view_access = session("YOUR_ACCESS")  


    'have the modules you have access to been set?
    objcommand.commandtext = "select console_set from " & Application("DBTABLE_USER_DATA") & " where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "'" 
    set objperm = objcommand.execute
    
    if isnull(objperm("console_set")) or objperm("console_set") = False then
        modcount = 0
        
        if mid(session("YOUR_LICENCE"),1,1) = "1" then
            modcount = modcount + 1
            call add_perm ("RA", modcount)
        end if
        
        objcommand.commandtext = "select id from " & APPLICATION("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code='" & session("corp_code") & "' and user_ref='" & session("YOUR_ACCOUNT_ID") & "'"
        set objrs = objcommand.execute
        if  not objrs.eof then 
            modcount = modcount + 1
            call add_perm ("ACC", modcount)
        end if
        
        if mid(session("your_licence"),2,1) = "1" then  
            modcount = modcount + 1
            call add_perm ("MH", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),5,1) = "1" then
            modcount = modcount + 1
            call add_perm ("MS", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),4,1) = "1" then
            modcount = modcount + 1
            call add_perm ("FS", modcount)
        end if
        
         if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2"  then
            modcount = modcount + 1
            call add_perm ("COSHH", modcount)
            modcount = modcount + 1
            call add_perm ("MSDS", modcount)
        end if
        
         if (mid(session("your_licence"),3,1) = "1" or mid(session("your_licence"),10,1) = "1" or mid(session("your_licence"),18,1) = "1") and Session("MODULE_DSE") = "2" then  
            modcount = modcount + 1
            call add_perm ("DSE", modcount)
        end if      
        
        if mid(session("YOUR_LICENCE"),15,1) = "1" then
            modcount = modcount + 1
            call add_perm ("INS", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),14,1) = "1" and Session("MODULE_PTW") = "2" then
            modcount = modcount + 1
            call add_perm ("PTW", modcount)
        end if
        
        if mid(session("YOUR_LICENCE"),6,1) = "1" and Session("MODULE_SA") = "2" then
            modcount = modcount + 1
            call add_perm ("SA", modcount)
        end if
        
        objcommand.commandtext = "update " & Application("DBTABLE_USER_DATA") & " set console_set = 1 where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "'" 
        objcommand.execute
        
    end if
    
    
    
    '"grab module that the user has access too
    
    objcommand.commandtext = "select * from " & Application("DBTABLE_GLOBAL_CONSOLE") & " where corp_code='" & session("corp_code") & "' and acc_ref='" &  session("YOUR_ACCOUNT_ID") & "' order by position" 
    set objrsmods = objcommand.execute
    
    if objrsmods.eof then
    
    
    else
        
        while not objrsmods.eof
            
            select case objrsmods("module")
                case "RA"
                    if mid(session("YOUR_LICENCE"),1,1) = "1" and (mod_type = "RA" or mod_type = "ALL") then 
                        call populate_module ("RA", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "ACC"          
                    objcommand.commandtext = "select id from " & APPLICATION("DBTABLE_HR_ABOOK_USER_LINK") & " where corp_code='" & session("corp_code") & "' and user_ref='" & session("YOUR_ACCOUNT_ID") & "'"
                    set objrs = objcommand.execute
                    if  not objrs.eof and (mod_type = "ACC" or mod_type = "ALL") then 
                        call populate_module ("ACC", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "MH"
                    if mid(session("YOUR_LICENCE"),2,1) = "1" and (mod_type = "MH" or mod_type = "ALL") then 
                        call populate_module ("MH", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "MS"
                    if mid(session("YOUR_LICENCE"),5,1) = "1" and (mod_type = "MS" or mod_type = "ALL") then 
                        call populate_module ("MS", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "FS"
                    if mid(session("YOUR_LICENCE"),4,1) = "1" and (mod_type = "FS" or mod_type = "ALL") then 
                        call populate_module ("FS", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "COSHH"
                    if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2" and (mod_type = "COSHH" or mod_type = "ALL") then 
                        call populate_module ("COSHH", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "MSDS"
                    if mid(session("YOUR_LICENCE"),11,1) = "1" and Session("MODULE_COSHH") = "2" and (mod_type = "MSDS" or mod_type = "ALL") then 
                        call populate_module ("MSDS", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "DSE"
                     if (mid(session("your_licence"),3,1) = "1" or mid(session("your_licence"),10,1) = "1" or mid(session("your_licence"),18,1) = "1") and Session("MODULE_DSE") = "2"  and (mod_type = "DSE" or mod_type = "ALL") then  
                        call populate_module ("DSE", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "INS"
                    if mid(session("YOUR_LICENCE"),15,1) = "1" and (mod_type = "INS" or mod_type = "ALL") then 
                        call populate_module ("INS", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "PTW"
                    if mid(session("YOUR_LICENCE"),14,1) = "1" and Session("MODULE_PTW") = "2" and (mod_type = "PTW" or mod_type = "ALL") then 
                        call populate_module ("PTW", var_view_access, mycount, "", "")
                        mycount = mycount + 1 
                    end if
                case "SA"
                    if mid(session("YOUR_LICENCE"),6,1) = "1"  and Session("MODULE_SA") = "2" and (mod_type = "SA" or mod_type = "ALL") then 
                        call populate_module ("SA", var_view_access, mycount, "", "")
                        mycount = mycount + 1
                    end if
           end select
           objrsmods.movenext
        wend
            
    end if        
    
    end if
 
end sub


sub populate_module(mod_code, view_access, mycount, sub_sw4, sub_sw5)

    'mod_code = module
    '"view_access = access level you wish to view
    'mycount = value for the position of the java script array
    'sub_sw5 = Empty
    'sub_sw6 = Empty
    
    
    
    var_scrolling_box_content = "<div id=""stats_mod_" & mod_code & """ class=""stats_module"">" & _
                            "<div id=""stats_mod_" & mod_code & "_inner"" class=""stats_module_inner"">" & _
                                "<h1>" & mods.Item(mod_code) &  "</h1>" & _
                                    "<table class=""stats_content"">" & _
                    "<tr><td class=""stats_content_left""><div class=""stats_mod_icon""><img src=""../../../version3.2/images/icons/new_icons/" & mod_code & ".png"" /></div></td><td class=""stats_content_right"">" 
                    
                
          if mod_code = "RA" then  

                if session("CCERA") = "Y" then 
                 
                        'Grab data from DB via SP
                        objcommand.commandtext = "exec MODULE_CCERA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"     
                       
                        set objrs = objcommand.execute
                 
                        if not objrs.eof then
                            var_ra_1 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ra_2 = objrs("mycount")
                                set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ra_3 = objRs("mycount")
                                     set objrs = objrs.nextrecordset
                                    if not objrs.eof then
                                        var_ra_4 = objRs("mycount")
                                        'set objrs = objrs.nextrecordset
                                        ' if not objrs.eof then
                                        '    var_ra_5 = objRs("mycount")
                                        'else
                                        '    var_ra_5 = "Error"
                                        'end if
                                    else
                                        var_ra_4 = "Error"
                                        var_ra_5 = "Error"
                                    end if
                                else
                                    var_ra_3 = "Error"
                                    var_ra_4 = "Error"
                                    var_ra_5 = "Error"
                                end if
                            else
                                var_ra_2 = "Error"
                                var_ra_3 = "Error"
                                var_ra_4 = "Error"
                                var_ra_5 = "Error"
                            end if                    
                       else
                            var_ra_1 = "Error"
                            var_ra_2 = "Error"
                            var_ra_3 = "Error"
                            var_ra_4 = "Error"
                            var_ra_5 = "Error"
                       end if 
               
                        'show incomplete records
                        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"

                        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B"
                        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
                
                        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Copied&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Copied&ra_srch_type_flag=B"                
                        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_3 & "</div></div><div class=""clear""></div></div>"
            
                        'show overdue records
                       ' if session("YOUR_ACCESS") = "1" then 
                       '     var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                       ' elseif session("YOUR_ACCESS") = "2" then 
                       '     var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                       ' else
                       '     var_search_criteria = "ra_frm_srch_showop=All&ra_srch_type_flag=A"
                       ' end if
                       ' var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"
           
                        var_search_criteria = "ra_frm_srch_showop=Awaiting Sign Off&ra_srch_type_flag=B"
                        var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Awaiting Sign Off</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"
                
                else ' not coke                
                
               '     'Grab data from DB via SP
               '     objcommand.commandtext = "exec MODULE_RA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"
               ' 
              ' 
              '      set objrs = objcommand.execute
              '   
              '      if not objrs.eof then
              '          var_ra_1 = objrs("mycount")  
              '          set objrs = objrs.nextrecordset
              '          if not objrs.eof then
              '              var_ra_2 = objrs("mycount")  
              '          else
              '              var_ra_2 = "Error"
              '          end if                    
              '     else
              '          var_ra_1 = "Error"
              '          var_ra_2 = "Error"
              '     end if 
              ' 
              '      'show incomplete records
              '      if session("YOUR_ACCESS") = "0" then var_search_criteria = "incomps=n" else  var_search_criteria = "vtype=user"
              '      var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&comps=n&ra_frm_srch_submit=Search for assessment"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"
           ' 
           '         'show overdue records
           '         if session("YOUR_ACCESS") = "1" then 
           '             var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&" 
           '         elseif session("YOUR_ACCESS") = "2" then 
           '           var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&" 
           '         end if
           '          var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria &  "ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"">Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
           
           
                    'Grab data from DB via SP
                    objcommand.commandtext = "exec MODULE_RA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"      
                

                    set objrs = objcommand.execute
                 
                    if not objrs.eof then
                        var_ra_1 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ra_2 = objrs("mycount")
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ra_3 = objRs("mycount")
                                 set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ra_4 = objRs("mycount")
                                else
                                    var_ra_4 = "Error"
                                end if
                            else
                                var_ra_3 = "Error"
                                var_ra_4 = "Error"
                            end if
                        else
                            var_ra_2 = "Error"
                            var_ra_3 = "Error"
                            var_ra_4 = "Error"
                        end if                    
                   else
                        var_ra_1 = "Error"
                        var_ra_2 = "Error"
                        var_ra_3 = "Error"
                        var_ra_4 = "Error"
                   end if 
               
                    'show incomplete records
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"

                    if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B"
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
                
                    if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Copied&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Copied&ra_srch_type_flag=B"                
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target==""_parent"" >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_3 & "</div></div><div class=""clear""></div></div>"
            
                    'show overdue records
                    if session("YOUR_ACCESS") = "1" then 
                        var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                    elseif session("YOUR_ACCESS") = "2" then 
                        var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                    else
                        var_search_criteria = "ra_frm_srch_showop=All&ra_srch_type_flag=A"
                    end if
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"

                end if
                
                                
           elseif mod_code = "TM" then  
         
                objcommand.commandtext = "exec MODULE_GLOBAL.dbo.TM_Console_Stats '" & session("TASK_ACCEPT_ENABLED") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("corp_code") & "' "
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_tm_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_tm_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_tm_3 = objrs("mycount")  
                            if session("TASK_ACCEPT_ENABLED") = "Y" then
                                set objrs = objrs.nextrecordset
                                var_tm_4 = objrs("mycount")                            
                            end if
                        else
                            var_tm_3 = "Error"
                        end if   
                    else
                        var_tm_2 = "Error"
                        var_tm_3 = "Error"
                    end if                    
                else
                    var_tm_1 = "Error"
                    var_tm_2 = "Error"
                    var_tm_3 = "Error"
                end if 
               
                if var_tm_1 > 0 then 
                    var_tm_1 = "<span style='color:#c00c00 !important;  font-size:1.25em;'><strong>" & var_tm_1 & "</strong></span>"
                end if
               
                'show overdue records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=bef&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"" >Overdue Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_1 & "</div></div><div class=""clear""></div></div>"
            
                'show Todays records
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=equ&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"" >Today""s Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_2 & "</div></div><div class=""clear""></div></div>"
                 
                 
                 var_14_days = dateadd("d",15,now())
                
               'show tasks due in the next 14 days
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=bet&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & "&frm_date2_d=" & day(var_14_days) & "&frm_date2_m=" & month(var_14_days) & "&frm_date2_y=" & year(var_14_days) & """ target=""_parent"" >Tasks Due in the Next 14 Days</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_3 & "</div></div><div class=""clear""></div></div>"
                 
                 
                 if session("TASK_ACCEPT_ENABLED") = "Y" then
                    'show tasks that haven""t been accepted
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp"" target=""_parent"">Tasks Requiring Your Attention</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_4 & "</div></div><div class=""clear""></div></div>"
                 end if 
          
           elseif mod_code = "ACC" then 
         
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_ACC.dbo.ACC_Console_Stats '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_acc_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_acc_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_acc_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_acc_4 = objrs("mycount")  
                            else
                                var_acc_4 = "Error"
                            end if
                        else
                            var_acc_3 = "Error"
                            var_acc_4 = "Error"
                        end if
                    else
                        var_acc_2 = "Error"
                        var_acc_3 = "Error"
                        var_acc_4 = "Error"
                    end if                    
               else
                    var_acc_1 = "Error"
                    var_acc_2 = "Error"
                    var_acc_3 = "Error"
                    var_acc_4 = "Error"
               end if 
           
                'show current year records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_dateop=3&acc_frm_srch_date1_d=01&acc_frm_srch_date1_m=01&acc_frm_srch_date1_y=" & year(now()) & "&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >Total Incidents for " & year(date()) & "</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_1 & "</div></div><div class=""clear""></div></div>"
            
                 
                'show incomplete records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"">Incomplete Incidents</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_2 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue riddors
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Incomplete RIDDORs</div><div class=""dots""><div class=""statistic_value"">" & var_acc_3 & "</div></div><div class=""clear""></div></div>"'<a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=6&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >
                
                'show overdue investigations
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Incomplete Investigations</div><div class=""dots""><div class=""statistic_value"">" & var_acc_4 & "</span></div></div><div class=""clear""></div></div>"'<a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=7&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >
          
          
          
          elseif mod_code = "MH" then  
                
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_MH.dbo.MH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_mh_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_mh_2 = objrs("mycount")  
                    else
                        var_mh_2 = "Error"
                    end if                    
               else
                    var_mh_1 = "Error"
                    var_mh_2 = "Error"
               end if 
                
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "mh_frm_srch_showop=Incomplete&mh_srch_type_flag=B" else  var_search_criteria = "vtype=user&mh_frm_srch_showop=Incomplete&mh_srch_type_flag=B"
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/manual_handling/app/mh_search.asp?" & var_search_criteria & "&mh_submit=Search Assessments"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_mh_1 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue records
                ' var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_mh_2 & "</div></div><div class=""clear""></div></div>"
               
          elseif mod_code = "MS" then
            
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_MS.dbo.MS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ms_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ms_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ms_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ms_4 = objrs("mycount")  
                            else
                                var_ms_4 = "Error"
                            end if
                        else
                            var_ms_3 = "Error"
                            var_ms_4 = "Error"
                        end if
                    else
                        var_ms_2 = "Error"
                        var_ms_3 = "Error"
                        var_ms_4 = "Error"
                    end if                    
               else
                    var_ms_1 = "Error"
                    var_ms_2 = "Error"
                    var_ms_3 = "Error"
                    var_ms_4 = "Error"
               end if 
               
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "ms_frm_srch_showop=Incomplete" else  var_search_criteria = "vtype=user&ms_frm_srch_showop=Incomplete"
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?" & var_search_criteria & "&ms_frm_srch_submit=Search for statement"" >Incomplete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_1 & "</div></div><div class=""clear""></div></div>"
            
                'show complete projects
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?ms_frm_srch_showop=Complete&ms_frm_srch_submit=Search for statement"" >Complete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_2 & "</div></div><div class=""clear""></div></div>"
            
                'show complete current
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Projects Currently Active</div><div class=""dots""><div class=""statistic_value"">" & var_ms_3 & "</div></div><div class=""clear""></div></div>"
                
                'show complete future
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder list""><div class=""statistic_desc"">Future Projects</div><div class=""dots""><div class=""statistic_value"">" & var_ms_4 & "</div></div><div class=""clear""></div></div>"
          
                
          elseif mod_code = "FS" then
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_FS.dbo.FS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_fs_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_fs_2 = objrs("mycount")  
                    else
                        var_fs_2 = "Error"
                    end if                    
               else
                    var_fs_1 = "Error"
                    var_fs_2 = "Error"
               end if 
               
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "fs_frm_srch_show=Incomplete&fs_srch_type_flag=B" else  var_search_criteria = "vtype=user&fs_frm_srch_show=Incomplete&fs_srch_type_flag=B"
                if session("fs_version") = "2" then
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/fire_safety_v2/app/firesafety_search.asp?" & var_search_criteria & "&fs_frm_srch_submit=go&frm_user_tier2=0&frm_user_tier3=0&frm_user_tier4=0&frm_user_tier5=0&frm_user_tier6=0"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_fs_1 & "</div></div><div class=""clear""></div></div>"
                else
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/fire_safety/app/firesafety_search.asp?" & var_search_criteria & "&fs_frm_srch_submit=Search for assessment"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_fs_1 & "</div></div><div class=""clear""></div></div>"
                end if
                'show overdue records
                ' var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments<span class=""statistic_value"">" & var_fs_2 & "</div></div><div class=""clear""></div></div>"
          
          elseif mod_code = "COSHH" then
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_COSHH.dbo.COSHH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
              '  if ucase(session("YOUR_USERNAME")) = "SYSADMIN.PICT" then
              '      response.write objCommand.commandText
              '      response.End
              '  end if
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_COSHH_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_COSHH_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_COSHH_3 = objrs("mycount")
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_COSHH_4 = objrs("mycount")
                            else
                                var_COSHH_4 = "Error"
                            end if
                        else
                            var_COSHH_3 = "Error"
                        end if
                    else
                        var_COSHH_2 = "Error"
                    end if                    
               else
                    var_COSHH_1 = "Error"
                    var_COSHH_2 = "Error"
                    var_COSHH_3 = "Error"
                    var_COSHH_4 = "Error"
               end if 
               
                'show total records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_1 & "</div></div><div class=""clear""></div></div>"

                if session("YOUR_ACCESS") = "0" then var_search_criteria = "coshh_frm_srch_showop=3&coshh_srch_type_flag=B" else  var_search_criteria = "vtype=user&coshh_frm_srch_showop=3&coshh_srch_type_flag=B"
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc list""><a href=""../../../version3.2/modules/hsafety/coshh/app/coshh_search.asp?" & var_search_criteria & "&coshh_frm_srch_submit=Search Assessments"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_2 & "</div></div><div class=""clear""></div></div>"

                if session("YOUR_ACCESS") = "0" then var_search_criteria = "coshh_frm_srch_showop=4&coshh_srch_type_flag=B" else  var_search_criteria = "vtype=user&coshh_frm_srch_showop=4&coshh_srch_type_flag=B"
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc list""><a href=""../../../version3.2/modules/hsafety/coshh/app/coshh_search.asp?" & var_search_criteria & "&coshh_frm_srch_submit=Search Assessments"" >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_3 & "</div></div><div class=""clear""></div></div>"

                'show incomplete records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_4 & "</div></div><div class=""clear""></div></div>"
          
          
          elseif mod_code = "MSDS" then
           'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_COSHH.dbo.MSDS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_msds_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_msds_2 = objrs("mycount")  
                    else
                        var_msds_2 = "Error"
                    end if                    
               else
                    var_msds_1 = "Error"
                    var_msds_2 = "Error"
               end if 
               
                'show incomplete records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/coshh/msds/msds_search.asp?msds_frm_srch_showop=3&search=Search+MSDS"" >Incomplete MSDS</a></div><div class=""dots""><div class=""statistic_value"">" & var_msds_1 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue records
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/coshh/msds/msds_search.asp?msds_frm_srch_showop=1&search=Search+MSDS"" >Complete MSDS</a></div><div class=""dots""><div class=""statistic_value"">" & var_msds_2 & "</div></div><div class=""clear""></div></div>"
          
          
          
          elseif mod_code = "DSE" then
          
                objcommand.commandtext = "select auto_email from " & Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Training_pref where corp_code='" & session("corp_code") & "' and active = 1"
                set objrs = objcommand.execute

                if objrs.eof then
                    auto_email = True
                else
                    auto_email = objrs("auto_email")
                end if 
                
                '"Run search to determine wether or not this user is assigned as a reviewer in a group 
                objcommand.commandtext = "select b.group_name, b.recordreference from " & Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_dse_reviewers as a " & _
                                         "left join " & Application("CONFIG_DBASE")& "_HR.dbo.HR_Data_dse_groups as b on a.corp_code = b.corp_code and a.dse_group_code = b.recordreference " & _
                                         "where a.corp_code='" & session("corp_code") & "' and a.user_code='" & session("YOUR_ACCOUNT_ID") & "'"
                set objrsdse = objcommand.execute

                if not objrsdse.eof then
                       
                    'produce list of groups
                    var_dse_stats_mygrpids = "''" & objrsdse("recordreference") & "''"
                    objrsdse.movenext
                    while not objrsdse.eof
                        var_dse_stats_mygrpids = var_dse_stats_mygrpids & ",''" & objrsdse("recordreference") & "''"
                        objrsdse.movenext
                    wend
                    sql_tag =  " and userdata.dse_grp_ref  in (" & var_dse_stats_mygrpids & ")"
                else
                    if mid(session("YOUR_LICENCE"),20,1) = "1" then
                        sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "'' and userdata.corp_bd=''" & session("YOUR_DEPARTMENT") & "''"
                        var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION") & "&dse_frm_s1_dept=" & session("YOUR_DEPARTMENT")
                    elseif mid(session("YOUR_LICENCE"),19,1) = "1" then
                       sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "''"
                       var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION")
                    elseif mid(session("YOUR_LICENCE"),22,1) = "1" then
                       sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "''"
                       var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY")
                    end if
                end if

            
                objcommand.commandtext = "exec MODULE_DSE.dbo.DSE_Console_Stats  '" & sql_tag & "' ,'" & session("corp_code") & "'"
              
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_dse_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_dse_2 = objrs("mycount")    
                        if auto_email then
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_dse_3 = objrs("mycount")                              
                            else
                                var_dse_3 = "Error"
                            end if  
                        else
                        
                        end if 
                    else
                        var_dse_2 = "Error"
                        var_dse_3 = "Error"
                    end if                    
                else
                    var_dse_1 = "Error"
                    var_dse_2 = "Error"
                    var_dse_3 = "Error"
                end if 
               
                 'show total users
                 if mid(session("YOUR_LICENCE"),10,1) = "1" then
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&search_but=Search for account" & var_search_criteria & """ >DSE Users in Your Area</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_1 & "</div></div><div class=""clear""></div></div>"
                 else
                    var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">DSE Users in Your Area</div><div class=""dots""><div class=""statistic_value"">" & var_dse_1 & "</div></div><div class=""clear""></div></div>"
                 end if
                 
                'show users with assessments awaiting review
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&srch_dsestatus=6&search_but=Search for account" & var_search_criteria & """ >Assessments Awaiting Review</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_2 & "</div></div><div class=""clear""></div></div>"
                 
                 if auto_email then
                'show users who haven""t completed the assessment despite receiving 3 emails
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&frm_dse_email_dis=Y&search_but=Search for account" & var_search_criteria & """ >User""s who haven""t completed an assessment despite 3 emails being sent</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_3 & "</div></div><div class=""clear""></div></div>"
                 
                 end if
                 
          
          elseif mod_code = "INS" then
          
               objcommand.commandtext = "exec MODULE_INS.dbo.INS_Console_Stats  '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ins_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ins_2 = objrs("mycount")    
                      
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ins_3 = objrs("mycount")                              
                            else
                                var_ins_3 = "Error"
                            end if  
                    else
                        var_ins_2 = "Error"
                        var_ins_3 = "Error"
                    end if                    
                else
                    var_ins_1 = "Error"
                    var_ins_2 = "Error"
                    var_ins_3 = "Error"
                end if 
               
                'show complete
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?ins_frm_srch_showop=Complete&ins_srch_type_flag=A&submit_but=Search for inspection(s)"" >Complete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_1 & "</div></div><div class=""clear""></div></div>"
            
                'show tasks allocated
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Complete Inspections with Tasks Assigned (Locked)</div><div class=""dots""><div class=""statistic_value"">" & var_ins_2 & "</div></div><div class=""clear""></div></div>"
                                  
                'show incompletes
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A" else var_search_criteria = "vtype=user&ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A"
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?" & var_search_criteria & "&submit_but=Search for inspection(s)"" >Incomplete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_3 & "</div></div><div class=""clear""></div></div>"

          elseif mod_code = "PTW" then
            
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_PTW.dbo.PTW_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ptw_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ptw_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ptw_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ptw_4 = objrs("mycount")  
                                set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ptw_5 = objrs("mycount")  
                                else
                                    var_ptw_5 = "Error"
                                end if
                            else
                                var_ptw_4 = "Error"
                                var_ptw_5 = "Error"
                            end if
                        else
                            var_ptw_3 = "Error"
                            var_ptw_4 = "Error"
                            var_ptw_5 = "Error"
                        end if
                    else
                        var_ptw_2 = "Error"
                        var_ptw_3 = "Error"
                        var_ptw_4 = "Error"
                        var_ptw_5 = "Error"
                    end if                    
               else
                    var_ptw_1 = "Error"
                    var_ptw_2 = "Error"
                    var_ptw_3 = "Error"
                    var_ptw_4 = "Error"
                    var_ptw_5 = "Error"
               end if 
               
                'show future records
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Future Dated Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_1 & "</div></div><div class=""clear""></div></div>"
            
                'show expired projects
                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Expired Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_2 & "</div></div><div class=""clear""></div></div>"
            
                'show complete active
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc"">Active Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_3 & "</div></div><div class=""clear""></div></div>"
                
                'show incomplete future
                 if session("YOUR_ACCESS") = "0" then var_search_criteria = "ptw_frm_srch_showop=Incomplete&ptw_srch_type_flag=A" else  var_search_criteria = "vtype=user&ptw_frm_srch_showop=Incomplete&ptw_srch_type_flag=A"
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/permit_to_work_v2/app/ptw_search.asp?" & var_search_criteria & "&ptw_frm_srch_submit=Search Permits"" >Incomplete Permits</a></div><div class=""dots""><div class=""statistic_value"">" & var_ptw_4 & "</div></div><div class=""clear""></div></div>"
          
                'show complete awaiting authorisation
                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/permit_to_work_v2/app/ptw_search.asp?ptw_frm_srch_showop=Awaiting%20Authorisation&ptw_srch_type_flag=A&ptw_frm_srch_submit=Search Permits"" >Permits Awaiting Authorisation</a></div><div class=""dots""><div class=""statistic_value"">" & var_ptw_5 & "</div></div><div class=""clear""></div></div>"
          
                
'          elseif mod_code = "SA" then
'                'Grab data from DB via SP
'                objcommand.commandtext = "exec MODULE_SA.dbo.SA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
'                set objrs = objcommand.execute
'                
'                if not objrs.eof then
'                    var_sa_1 = objrs("mycount")  
'                    set objrs = objrs.nextrecordset
'                    if not objrs.eof then
'                        var_sa_2 = objrs("mycount")  
'                    else
'                        var_sa_2 = "Error"
'                    end if                    
'               else
'                    var_sa_1 = "Error"
'                    var_sa_2 = "Error"
'               end if 
'               
'                'show complete records
'                var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/safety_audit/v2/app/saudit_search.asp?sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=complete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc&sa_frm_srch_submit=Search Audits"" >Complete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_sa_1 & "</div></div><div class=""clear""></div></div>"
'            
'                'show incomplete records
'                 if session("YOUR_ACCESS") = "0" then var_search_criteria = "sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=Incomplete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc" else  var_search_criteria = "sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=Incomplete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc"
'                 var_scrolling_box_content = var_scrolling_box_content & "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/safety_audit/v2/app/saudit_search.asp?" & var_search_criteria & "&comps=n&sa_frm_srch_submit=Search Audits"" >Incomplete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_sa_2 & "</div></div><div class=""clear""></div></div>"
'          
         end if
      
      
      var_scrolling_box_content = var_scrolling_box_content & "</td></tr></table></div></div>"

      response.Write  "pausecontent[" & mycount & "]= '" & var_scrolling_box_content & "' " &vbcrlf
      
 
 
end sub


function output_sub_stats(objRs2, current_tier_ref, div_id)
    dim sub_stats_text
    
    sub_stats_text = "<div class='r'><a id='exp" & current_tier_ref & "' href='javascript:toggleHC(" & current_tier_ref & ")'>"
        if cstr(div_id) = cstr(current_tier_ref) then sub_stats_text = sub_stats_text & "collapse</a></div>" else sub_stats_text = sub_stats_text & "expand</a></div>" end if
        
    sub_stats_text = sub_stats_text & "<div id='" & current_tier_ref & "' style='padding:0 0 20px 20px; " 
        if cstr(div_id) = cstr(current_tier_ref) then sub_stats_text = sub_stats_text & "display:block;'>" else sub_stats_text = sub_stats_text & "display:none;'>" end if

    current_location_name = ""
    while not objRs2.EOF
        if cstr(objRs2("tier2_ref")) <> cstr(current_tier_ref) then
            objRs2.movenext
        else
            if current_location_name <> objRs2("Struct_name") then
                current_location_name = objRs2("Struct_name")
                sub_stats_text = sub_stats_text & "<a href='#' id='" & current_location_name & current_tier_ref & "'></a><h3 style='padding-top:4px;'>" & current_location_name & "</h3>"
            end if

            select case objRs2("resulttype")
                case "Total"
                    var_search_criteria = "frm_user_tier2=" & objRs2("tier2_ref") & "&frm_user_tier3=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                case "Incomplete"
                    var_search_criteria = "frm_user_tier2=" & objRs2("tier2_ref") & "&frm_user_tier3=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                Case "Copied"
                    var_search_criteria = "frm_user_tier2=" & objRs2("tier2_ref") & "&frm_user_tier3=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                case "Complete"
                    var_search_criteria = "frm_user_tier2=" & objRs2("tier2_ref") & "&frm_user_tier3=" & objRs2("tier3_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
            end select
            
            if objRs2("resulttype") = "Copied" and objRs2("numrecords") = 0 then
                'do nothing
            else
                sub_stats_text = sub_stats_text & "<div class='statistic_holder' style='padding-left:10px;'><div class='statistic_desc'><a href='../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target=""_parent"" >" & objRs2("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs2("numrecords") & "</div></div><div class='clear'></div></div>"
            end if
            objRs2.movenext
        end if
    wend

    sub_stats_text = sub_stats_text & "</div>"
    
    output_sub_stats = sub_stats_text
end function



sub console_dropdown(view_access)

    if session("fully_global") = "Y" then
        var_company = "%"
    else
        var_company = session("YOUR_COMPANY")
    end if
    
    if view_access = "0" then
        var_location = "%"
    else    
        var_location = session("YOUR_LOCATION")
    end if

    objCommand.commandText = "SELECT struct_name, tier2_ref FROM " & Application("DBTABLE_STRUCT_TIER2") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND tier2_ref LIKE '" & var_company & "' " & _
                             "ORDER BY struct_name"
    set objRs = objCommand.execute
    if not objRs.EOF then
    
        while not objRs.EOF
            response.Write "<option value='na'>&nbsp</option>"
            response.Write "<option value='" & objRs("tier2_ref") & "'>" & objRs("struct_name") & "</option>"
            
            if var_location = "%" then
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier2_ref = " & objRs("tier2_ref") & " " & _
                                         "ORDER BY struct_name"
            else
                objCommand.commandText = "SELECT struct_name, tier3_ref FROM " & Application("DBTABLE_STRUCT_TIER3") & " " & _
                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                         "  AND tier3_ref = " & session("YOUR_LOCATION") & " " & _
                                         "ORDER BY struct_name"
            end if        
            response.Write "..." & objCommand.commandText & "..."        
            set objRs1 = objCommand.execute
            if not objRs1.EOF then
                while not objRs1.EOF
                    response.Write "<option value='" & objRs("tier2_ref") & "'>&nbsp;&nbsp;&nbsp;&nbsp;" & objRs1("struct_name") & "</option>"
                    objRs1.movenext
                wend
            end if
            
            objRs.movenext
        wend
    end if



end sub

sub populate_module_detailed(mod_code, view_access, mycount, div_id, sub_sw5)
    
    'mod_code = module
    '"view_access = access level you wish to view
    'mycount = value for the position of the java script array
    'div_id = used for detailed statistics (ikea) - expands the selected hidden div when jumping to specific anchor
    'sub_sw5 = Empty


    response.write "<div id=""stats_mod_" & mod_code & """ class=""stats_module"" >" & _
                            "<div class=""stats_module_inner"">" & _
                                "<h1>" & mods.Item(mod_code) &  "</h1>" & _
                                    "<table class=""stats_content"">" & _
                    "<tr><td class=""stats_content_left""><div class=""stats_mod_icon""><img src=""../../../version3.2/images/icons/new_icons/" & mod_code & ".png"" /></div></td><td class=""stats_content_right"">" 

    if view_access = "0" then
        var_location = "%"
    else    
        var_location = session("YOUR_LOCATION")
    end if
    
    
    if mod_code = "RA" then  
        ' FULLY GLOBAL - This is a session variable to go in the preferences table. Currently it doesn't exist.
        ' It should be assigned to a specific user, and allows them to report stats for all companies in the contract, not just the one they're 
        ' assigned too in user manager.
        ' FULLY GLOBAL is also referenced in the SUB (console_dropdown)
        
        if session("fully_global") = "Y" then
            objCommand.commandText = "EXEC MODULE_RA.dbo.RA_Console_Stats_Detailed '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("CORP_CODE") & "', '%', '%', '" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"
        else
            objCommand.commandText = "EXEC MODULE_RA.dbo.RA_Console_Stats_Detailed '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("CORP_CODE") & "', '" & session("YOUR_COMPANY") & "', '" & var_location & "', '" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"
        end if 
        
        'response.Write objCommand.commandText
        set objRs1 = objCommand.execute
        
        if view_access = "0" then
            if not objRs1.EOF then
                current_tier2_ref = "0"
                while not objRs1.EOF
                    
                    if cstr(current_tier2_ref) <> cstr(objRs1("tier2_ref")) then
                        if view_access = "0" and current_tier2_ref <> "0" then
                            set objRs2 = objCommand.execute
                            set objRs2 = objRs2.nextRecordset
                            response.write output_sub_stats(objRs2, current_tier2_ref, div_id)
                        end if
                        
                        response.Write "<a href='#' id='" & objRs1("struct_name") & objRs1("tier2_ref") & "'></a><h3>" & objRs1("struct_name") & "</h3>"
                        current_tier2_ref = objRs1("tier2_ref")
                    end if
                    
                    select case objRs1("resulttype")
                        case "Total"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                        case "Incomplete"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                        case "Copied"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                        case "Complete"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
                    end select
                    
                    if objRs1("resulttype") = "Copied" and objRs1("numrecords") = 0 then
                        'do nothing
                    else
                        response.Write "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target=""_parent"" >" & objRs1("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs1("numrecords") & "</div></div><div class='clear'></div></div>"
                    end if
                    objRs1.movenext
                wend
                
            end if

            set objRs2 = objCommand.execute
            set objRs2 = objRs2.nextRecordset
            response.write output_sub_stats(objRs2, current_tier2_ref, div_id)
            
        elseif view_access = "1" then
            if not objRs1.EOF then
                current_tier3_ref = "0"
                while not objRs1.EOF
                    
                    if cstr(current_tier3_ref) <> cstr(objRs1("tier3_ref")) then
                        response.Write "<a href='#' id='" & objRs1("struct_name") & objRs1("tier3_ref") & "'></a><h3>" & objRs1("struct_name") & "</h3>"
                        current_tier3_ref = objRs1("tier3_ref")
                    end if
                    
                    select case objRs1("resulttype")
                        case "Total"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&frm_user_tier3=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=All&ra_frm_srch_submit=Search for assessment"
                        case "Incomplete"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&frm_user_tier3=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Incomplete&ra_frm_srch_submit=Search for assessment"
                        case "Copied"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&frm_user_tier3=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Copied&ra_frm_srch_submit=Search for assessment"
                        case "Complete"
                            var_search_criteria = "frm_user_tier2=" & objRs1("tier2_ref") & "&frm_user_tier3=" & objRs1("tier3_ref") & "&ra_frm_srch_showop=Complete&ra_frm_srch_submit=Search for assessment"
                    end select
                    
                    if objRs1("resulttype") = "Copied" and objRs1("numrecords") = 0 then
                        'do nothing
                    else
                        response.Write "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "' target=""_parent"" >" & objRs1("resulttype") & " Assessments</a></div><div class='dots'><div class='statistic_value'>" & objRs1("numrecords") & "</div></div><div class='clear'></div></div>"
                    end if
                    objRs1.movenext
                wend
                
            end if        
        
        else
            if not objRs1.EOF then
                var_ra_1 = objRs1("mycount")  
                set objRs1 = objRs1.nextRecordset
                if not objRs1.eof then
                    var_ra_2 = objrs1("mycount")  
                else
                    var_ra_2 = "Error"
                end if                    
            else
                var_ra_1 = "Error"
                var_ra_2 = "Error"
            end if
           
            'show incomplete records
            if session("YOUR_ACCESS") = "0" then var_search_criteria = "incomps=n" else  var_search_criteria = "vtype=user"
            response.Write  "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&comps=n&ra_frm_srch_submit=Search for assessment' target=""_parent"">Incomplete Assessments</a></div><div class='dots'><div class='statistic_value'>" & var_ra_1 & "</div></div><div class='clear'></div></div>"
        
            'show overdue records
            if session("YOUR_ACCESS") = "1" then 
                var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&" 
            elseif session("YOUR_ACCESS") = "2" then 
              var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&" 
            end if
            response.Write  "<div class='statistic_holder'><div class='statistic_desc'><a href='../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment' target=""_parent"">Overdue Assessments</a></div><div class='dots'><div class='statistic_value'>" & var_ra_2 & "</div></div><div class='clear'></div></div>"
           
        end if

    end if
        
    response.Write  "</td></tr></table></div></div><hr />"

end sub

sub populate_module_asp(mod_code, view_access, mycount, div_id, sub_sw5)

    'mod_code = module
    '"view_access = access level you wish to view
    'mycount = value for the position of the java script array
    'sub_sw4 = Empty
    'sub_sw5 = Empty
    
    
    response.write "<div id=""stats_mod_" & mod_code & """ class=""stats_module"" >" & _
                            "<div class=""stats_module_inner"">" & _
                                "<h1>" & mods.Item(mod_code) &  "</h1>" & _
                                    "<table class=""stats_content"">" & _
                    "<tr><td class=""stats_content_left""><div class=""stats_mod_icon""><img src=""../../../version3.2/images/icons/new_icons/" & mod_code & ".png"" /></div></td><td class=""stats_content_right"">" 
                
         if mod_code = "TM" then  
         
                objcommand.commandtext = "exec MODULE_GLOBAL.dbo.TM_Console_Stats '" & session("TASK_ACCEPT_ENABLED") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("corp_code") & "' "
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_tm_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_tm_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_tm_3 = objrs("mycount")  
                            if session("TASK_ACCEPT_ENABLED") = "Y" then
                                set objrs = objrs.nextrecordset
                                var_tm_4 = objrs("mycount")                            
                            end if
                        else
                            var_tm_3 = "Error"
                        end if   
                    else
                        var_tm_2 = "Error"
                        var_tm_3 = "Error"
                    end if                    
                else
                    var_tm_1 = "Error"
                    var_tm_2 = "Error"
                    var_tm_3 = "Error"
                end if 
               
               
                'show overdue records
                response.Write "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=bef&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"" >Overdue Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_1 & "</div></div><div class=""clear""></div></div>"
            
                'show Todays records
                 response.Write "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=equ&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & """ target=""_parent"" >Today's Tasks</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_2 & "</div></div><div class=""clear""></div></div>"
                 
                 
                 var_14_days = dateadd("d",15,now())
                
               'show tasks due in the next 14 days
                 response.Write "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp?frm_operator=bet&frm_date1_d=" & day(now()) & "&frm_date1_m=" & month(now()) & "&frm_date1_y=" & year(now()) & "&frm_date2_d=" & day(var_14_days) & "&frm_date2_m=" & month(var_14_days) & "&frm_date2_y=" & year(var_14_days) & """ target=""_parent"" >Tasks Due in the Next 14 Days</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_3 & "</div></div><div class=""clear""></div></div>"
                 
                 
                 if session("TASK_ACCEPT_ENABLED") = "Y" then
                    'show tasks that haven""t been accepted
                    response.Write "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/taskmanager_v2/tasks.asp"" target=""_parent"" >Tasks Requiring Your Attention</a></div><div class=""dots""><div class=""statistic_value"">" & var_tm_4 & "</div></div><div class=""clear""></div></div>"
                 end if 
      elseif mod_code = "RA" then  

                 if session("CCERA") = "Y" then 
                 
                                         'Grab data from DB via SP
                        objcommand.commandtext = "exec MODULE_CCERA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"     
                       
                        set objrs = objcommand.execute
                 
                        if not objrs.eof then
                            var_ra_1 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ra_2 = objrs("mycount")
                                set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ra_3 = objRs("mycount")
                                     set objrs = objrs.nextrecordset
                                    if not objrs.eof then
                                        var_ra_4 = objRs("mycount")
                                        set objrs = objrs.nextrecordset
                                         if not objrs.eof then
                                            var_ra_5 = objRs("mycount")
                                        else
                                            var_ra_5 = "Error"
                                        end if
                                    else
                                        var_ra_4 = "Error"
                                        var_ra_5 = "Error"
                                    end if
                                else
                                    var_ra_3 = "Error"
                                    var_ra_4 = "Error"
                                    var_ra_5 = "Error"
                                end if
                            else
                                var_ra_2 = "Error"
                                var_ra_3 = "Error"
                                var_ra_4 = "Error"
                                var_ra_5 = "Error"
                            end if                    
                       else
                            var_ra_1 = "Error"
                            var_ra_2 = "Error"
                            var_ra_3 = "Error"
                            var_ra_4 = "Error"
                            var_ra_5 = "Error"
                       end if 
               
                        'show incomplete records
                        response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"

                        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B"
                        response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment""  target='_parent' >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
                
                        if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Copied&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Copied&ra_srch_type_flag=B"                
                        response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment""  target='_parent' >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_3 & "</div></div><div class=""clear""></div></div>"
            
                        'show overdue records
                        if session("YOUR_ACCESS") = "1" then 
                            var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                        elseif session("YOUR_ACCESS") = "2" then 
                            var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                        else
                            var_search_criteria = "ra_frm_srch_showop=All&ra_srch_type_flag=A"
                        end if
                        response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment""  target='_parent' >Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"
           
                        var_search_criteria = "ra_frm_srch_showop=Awaiting Sign Off&ra_srch_type_flag=B"
                        response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/cce_risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment""  target='_parent' >Awaiting Sign Off</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_5 & "</div></div><div class=""clear""></div></div>"
                
                else ' not coke
                    'Grab data from DB via SP
                   ' objcommand.commandtext = "exec MODULE_RA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                   ' 
                  ' 
                  '  set objrs = objcommand.execute
                  '   
                  '  if not objrs.eof then
                  '      var_ra_1 = objrs("mycount")  
                  '      set objrs = objrs.nextrecordset
                  '      if not objrs.eof then
                  '          var_ra_2 = objrs("mycount")  
                  '      else
                  '          var_ra_2 = "Error"
                  '      end if                    
                  ' else
                  '      var_ra_1 = "Error"
                  '      var_ra_2 = "Error"
                  ' end if 

            
               
                    'show incomplete records
                  '  if session("YOUR_ACCESS") = "0" then var_search_criteria = "incomps=n" else  var_search_criteria = "vtype=user"
                  '  response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&comps=n&ra_frm_srch_submit=Search for assessment"">Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"
            
                    'show overdue records
                  '  if session("YOUR_ACCESS") = "1" then 
                  '      var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&" 
                  '  elseif session("YOUR_ACCESS") = "2" then 
                  '    var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&" 
                  '  end if
                  '  response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"">Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
           
            
                    'Grab data from DB via SP
                    objcommand.commandtext = "exec MODULE_RA.dbo.RA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "', '" & session("YOUR_LOCATION") & "', '" & session("YOUR_DEPARTMENT") & "'"      
                

                    set objrs = objcommand.execute
                 
                    if not objrs.eof then
                        var_ra_1 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ra_2 = objrs("mycount")
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ra_3 = objRs("mycount")
                                 set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ra_4 = objRs("mycount")
                                else
                                    var_ra_4 = "Error"
                                end if
                            else
                                var_ra_3 = "Error"
                                var_ra_4 = "Error"
                            end if
                        else
                            var_ra_2 = "Error"
                            var_ra_3 = "Error"
                            var_ra_4 = "Error"
                        end if                    
                   else
                        var_ra_1 = "Error"
                        var_ra_2 = "Error"
                        var_ra_3 = "Error"
                        var_ra_4 = "Error"
                   end if 
               
                    'show incomplete records
                    response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_ra_1 & "</div></div><div class=""clear""></div></div>"

                    if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Incomplete&ra_srch_type_flag=B"
                    response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_2 & "</div></div><div class=""clear""></div></div>"
                
                    if session("YOUR_ACCESS") = "0" then var_search_criteria = "ra_frm_srch_showop=Copied&ra_srch_type_flag=B" else  var_search_criteria = "vtype=user&ra_frm_srch_showop=Copied&ra_srch_type_flag=B"                
                    response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_submit=Search for assessment"" target=""_parent"" >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_3 & "</div></div><div class=""clear""></div></div>"
            
                    'show overdue records
                    if session("YOUR_ACCESS") = "1" then 
                        var_search_criteria = "frm_user_tier2=" & session("YOUR_COMPANY") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                    elseif session("YOUR_ACCESS") = "2" then 
                        var_search_criteria = "ra_frm_srch_revby=" & session("YOUR_ACCOUNT_ID") & "&ra_frm_srch_showop=All&ra_srch_type_flag=A" 
                    else
                        var_search_criteria = "ra_frm_srch_showop=All&ra_srch_type_flag=A"
                    end if
                    response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/risk_assessment/app/rassessment_search.asp?" & var_search_criteria & "&ra_frm_srch_revdateop=2&ra_frm_srch_revdate_d=" & day(now()) & "&ra_frm_srch_revdate_m=" & month(now()) & "&ra_frm_srch_revdate_y=" & year(now()) & "&ra_frm_srch_submit=Search for assessment"" >Overdue Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_ra_4 & "</div></div><div class=""clear""></div></div>"
           
                end if


           elseif mod_code = "ACC" then 
         
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_ACC.dbo.ACC_Console_Stats '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_acc_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_acc_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_acc_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_acc_4 = objrs("mycount")  
                            else
                                var_acc_4 = "Error"
                            end if
                        else
                            var_acc_3 = "Error"
                            var_acc_4 = "Error"
                        end if
                    else
                        var_acc_2 = "Error"
                        var_acc_3 = "Error"
                        var_acc_4 = "Error"
                    end if                    
               else
                    var_acc_1 = "Error"
                    var_acc_2 = "Error"
                    var_acc_3 = "Error"
                    var_acc_4 = "Error"
               end if 
           
                'show current year records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_dateop=3&acc_frm_srch_date1_d=01&acc_frm_srch_date1_m=01&acc_frm_srch_date1_y=" & year(now()) & "&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >Total Incidents for " & year(date()) & "</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_1 & "</div></div><div class=""clear""></div></div>"
            
                 
                'show incomplete records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"">Incomplete Incidents</a></div><div class=""dots""><div class=""statistic_value"">" & var_acc_2 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue riddors
                 response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc"">Incomplete RIDDORs</div><div class=""dots""><div class=""statistic_value"">" & var_acc_3 & "</div></div><div class=""clear""></div></div>" '<a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=6&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >
                
                'show overdue investigations
                 response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc"">Incomplete Investigations</div><div class=""dots""><div class=""statistic_value"">" & var_acc_4 & "</span></div></div><div class=""clear""></div></div>" '<a href=""../../../version3.2/modules/hsafety/accident/app/acc_search.asp?acc_frm_inc_type=7&acc_frm_srch_vtype=incomp&acc_frm_srch_as_accb=ALL&acc_frm_srch_submit=Search for Accident / Incident"" >
          
          
          
          elseif mod_code = "MH" then  
                
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_MH.dbo.MH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_mh_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_mh_2 = objrs("mycount")  
                    else
                        var_mh_2 = "Error"
                    end if                    
               else
                    var_mh_1 = "Error"
                    var_mh_2 = "Error"
               end if 
                
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "mh_frm_srch_showop=Incomplete&mh_srch_type_flag=B" else  var_search_criteria = "vtype=user&mh_frm_srch_showop=Incomplete&mh_srch_type_flag=B"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/manual_handling/app/mh_search.asp?" & var_search_criteria & "&mh_submit=Search Assessments"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_mh_1 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue records
                ' response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_mh_2 & "</div></div><div class=""clear""></div></div>"
               
          elseif mod_code = "MS" then
            
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_MS.dbo.MS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ms_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ms_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ms_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ms_4 = objrs("mycount")  
                            else
                                var_ms_4 = "Error"
                            end if
                        else
                            var_ms_3 = "Error"
                            var_ms_4 = "Error"
                        end if
                    else
                        var_ms_2 = "Error"
                        var_ms_3 = "Error"
                        var_ms_4 = "Error"
                    end if                    
               else
                    var_ms_1 = "Error"
                    var_ms_2 = "Error"
                    var_ms_3 = "Error"
                    var_ms_4 = "Error"
               end if 
               
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "ms_frm_srch_showop=Incomplete" else  var_search_criteria = "vtype=user&ms_frm_srch_showop=Incomplete"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?" & var_search_criteria & "&comps=n&ms_frm_srch_submit=Search for statement"" >Incomplete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_1 & "</div></div><div class=""clear""></div></div>"
            
                'show complete projects
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/method_statement/app/mstatement_search.asp?ms_frm_srch_submit=Search for statement"" >Complete Method Statements</a></div><div class=""dots""><div class=""statistic_value"">" & var_ms_2 & "</div></div><div class=""clear""></div></div>"
            
                'show complete current
                 response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc"">Projects Currently Active</div><div class=""dots""><div class=""statistic_value"">" & var_ms_3 & "</div></div><div class=""clear""></div></div>"
                
                'show complete future
                 response.Write  "<div class=""statistic_holder list""><div class=""statistic_desc"">Future Projects</div><div class=""dots""><div class=""statistic_value"">" & var_ms_4 & "</div></div><div class=""clear""></div></div>"
          
                
          elseif mod_code = "FS" then
                'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_FS.dbo.FS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_fs_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_fs_2 = objrs("mycount")  
                    else
                        var_fs_2 = "Error"
                    end if                    
               else
                    var_fs_1 = "Error"
                    var_fs_2 = "Error"
               end if 
               
                'show incomplete records
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "fs_frm_srch_show=Incomplete&fs_srch_type_flag=B" else  var_search_criteria = "vtype=user&fs_frm_srch_show=Incomplete&fs_srch_type_flag=B"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/fire_safety/app/firesafety_search.asp?" & var_search_criteria & "&comps=n&fs_frm_srch_submit=Search for assessment"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_fs_1 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue records
                ' response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments<span class=""statistic_value"">" & var_fs_2 & "</div></div><div class=""clear""></div></div>"
          
          elseif mod_code = "COSHH" then
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_COSHH.dbo.COSHH_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
             '   if ucase(session("YOUR_USERNAME")) = "SYSADMIN.PICT" then
             '       response.write objCommand.commandText
             '       response.End
             '   end if
                
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_COSHH_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_COSHH_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_COSHH_3 = objrs("mycount")
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_COSHH_4 = objrs("mycount")
                            else
                                var_COSHH_4 = "Error"
                            end if
                        else
                            var_COSHH_3 = "Error"
                        end if
                    else
                        var_COSHH_2 = "Error"
                    end if                    
               else
                    var_COSHH_1 = "Error"
                    var_COSHH_2 = "Error"
                    var_COSHH_3 = "Error"
                    var_COSHH_4 = "Error"
               end if 
               
                'show total records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">All Incomplete Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_1 & "</div></div><div class=""clear""></div></div>"

                if session("YOUR_ACCESS") = "0" then var_search_criteria = "coshh_frm_srch_showop=3&coshh_srch_type_flag=B" else  var_search_criteria = "vtype=user&coshh_frm_srch_showop=3&coshh_srch_type_flag=B"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc list""><a href=""../../../version3.2/modules/hsafety/coshh/app/coshh_search.asp?" & var_search_criteria & "&coshh_frm_srch_submit=Search Assessments"" >Incomplete Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_2 & "</div></div><div class=""clear""></div></div>"

                if session("YOUR_ACCESS") = "0" then var_search_criteria = "coshh_frm_srch_showop=4&coshh_srch_type_flag=B" else  var_search_criteria = "vtype=user&coshh_frm_srch_showop=4&coshh_srch_type_flag=B"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc list""><a href=""../../../version3.2/modules/hsafety/coshh/app/coshh_search.asp?" & var_search_criteria & "&coshh_frm_srch_submit=Search Assessments"" >Copied Assessments</a></div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_3 & "</div></div><div class=""clear""></div></div>"

                'show incomplete records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">Overdue Assessments</div><div class=""dots""><div class=""statistic_value"">" & var_COSHH_4 & "</div></div><div class=""clear""></div></div>"



          elseif mod_code = "MSDS" then
           'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_COSHH.dbo.MSDS_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_msds_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_msds_2 = objrs("mycount")  
                    else
                        var_msds_2 = "Error"
                    end if                    
               else
                    var_msds_1 = "Error"
                    var_msds_2 = "Error"
               end if 
               
                'show incomplete records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/coshh/msds/msds_search.asp?msds_incomplete=on&search=Search MSDS"" >Incomplete MSDS</a></div><div class=""dots""><div class=""statistic_value"">" & var_msds_1 & "</div></div><div class=""clear""></div></div>"
            
                'show overdue records
                 response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/coshh/msds/msds_search.asp?search=Search MSDS"" >Complete MSDS</a></div><div class=""dots""><div class=""statistic_value"">" & var_msds_2 & "</div></div><div class=""clear""></div></div>"
          
          
          
          elseif mod_code = "DSE" then
          
                objcommand.commandtext = "select auto_email from " & Application("CONFIG_DBASE") & "_GLOBAL.dbo.GLOBAL_Training_pref where corp_code='" & session("corp_code") & "' and active = 1"
                set objrs = objcommand.execute

                if objrs.eof then
                    auto_email = True
                else
                    auto_email = objrs("auto_email")
                end if 
                
                '"Run search to determine wether or not this user is assigned as a reviewer in a group 
                objcommand.commandtext = "select b.group_name, b.recordreference from " & Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_dse_reviewers as a " & _
                                         "left join " & Application("CONFIG_DBASE")& "_HR.dbo.HR_Data_dse_groups as b on a.corp_code = b.corp_code and a.dse_group_code = b.recordreference " & _
                                         "where a.corp_code='" & session("corp_code") & "' and a.user_code='" & session("YOUR_ACCOUNT_ID") & "'"
                set objrsdse = objcommand.execute

                if not objrsdse.eof then
                       
                    'produce list of groups
                    var_dse_stats_mygrpids = "''" & objrsdse("recordreference") & "''"
                    objrsdse.movenext
                    while not objrsdse.eof
                        var_dse_stats_mygrpids = var_dse_stats_mygrpids & ",''" & objrsdse("recordreference") & "''"
                        objrsdse.movenext
                    wend
                    sql_tag =  " and userdata.dse_grp_ref  in (" & var_dse_stats_mygrpids & ")"
                else
                    if mid(session("YOUR_LICENCE"),20,1) = "1" then
                        sql_tag =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "'' and userdata.corp_bd=''" & session("YOUR_DEPARTMENT") & "''"
                        var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION") & "&dse_frm_s1_dept=" & session("YOUR_DEPARTMENT")
                    elseif mid(session("YOUR_LICENCE"),19,1) = "1" then
                       var_search_criteria =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "'' and userdata.corp_bl=''" & session("YOUR_LOCATION") & "''"
                       var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY") & "&dse_frm_s1_location=" & session("YOUR_LOCATION")
                    elseif mid(session("YOUR_LICENCE"),22,1) = "1" then
                       var_search_criteria =  " and userdata.corp_bu=''" & session("YOUR_COMPANY") & "''"
                       var_search_criteria = "&dse_frm_s1_company=" & session("YOUR_COMPANY")
                    end if
                end if

            
                objcommand.commandtext = "exec MODULE_DSE.dbo.DSE_Console_Stats  '" & sql_tag & "' ,'" & session("corp_code") & "'"
              
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_dse_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_dse_2 = objrs("mycount")    
                        if auto_email then
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_dse_3 = objrs("mycount")                              
                            else
                                var_dse_3 = "Error"
                            end if  
                        else
                        
                        end if 
                    else
                        var_dse_2 = "Error"
                        var_dse_3 = "Error"
                    end if                    
                else
                    var_dse_1 = "Error"
                    var_dse_2 = "Error"
                    var_dse_3 = "Error"
                end if 
               
                 'show total users
                 if mid(session("YOUR_LICENCE"),10,1) = "1" then
                    response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&search_but=Search for account" & var_search_criteria & """ >DSE Users in Your Area</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_1 & "</div></div><div class=""clear""></div></div>"
                 else
                    response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">DSE Users in Your Area</div><div class=""dots""><div class=""statistic_value"">" & var_dse_1 & "</div></div><div class=""clear""></div></div>"
                 end if
                 
                'show users with assessments awaiting review
                 response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&srch_dsestatus=6&search_but=Search for account" & var_search_criteria & """ >Assessments Awaiting Review</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_2 & "</div></div><div class=""clear""></div></div>"
                 
                 if auto_email then
                'show users who haven""t completed the assessment despite receiving 3 emails
                 response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/management/control/dse/dse_manage.asp?frm_srch_dse_user=1&frm_dse_email_dis=Y&search_but=Search for account" & var_search_criteria & """ >User's who haven't completed an assessment despite 3 emails being sent</a></div><div class=""dots""><div class=""statistic_value"">" & var_dse_3 & "</div></div><div class=""clear""></div></div>"
                 
                 end if
                 
          
          elseif mod_code = "INS" then
          
               objcommand.commandtext = "exec MODULE_INS.dbo.INS_Console_Stats  '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ins_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ins_2 = objrs("mycount")    
                      
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ins_3 = objrs("mycount")                              
                            else
                                var_ins_3 = "Error"
                            end if  
                    else
                        var_ins_2 = "Error"
                        var_ins_3 = "Error"
                    end if                    
                else
                    var_ins_1 = "Error"
                    var_ins_2 = "Error"
                    var_ins_3 = "Error"
                end if 
               
                'show complete
                response.Write "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?ins_frm_srch_showop=Complete&ins_srch_type_flag=A&submit_but=Search for inspection(s)"" >Complete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_1 & "</div></div><div class=""clear""></div></div>"
            
                'show tasks allocated
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">Complete Inspections with Tasks Assigned (Locked)</div><div class=""dots""><div class=""statistic_value"" >" & var_ins_2 & "</div></div><div class=""clear""></div></div>"
                                  
                'show incompletes
                if session("YOUR_ACCESS") = "0" then var_search_criteria = "ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A" else var_search_criteria = "vtype=user&ins_frm_srch_showop=Incomplete&ins_srch_type_flag=A"
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/inspection/app/insp_search.asp?" & var_search_criteria & "&comps=n&submit_but=Search for inspection(s)"" >Incomplete Inspections</a></div><div class=""dots""><div class=""statistic_value"">" & var_ins_3 & "</div></div><div class=""clear""></div></div>"
                           
          elseif mod_code = "PTW" then
            
               'Grab data from DB via SP
                objcommand.commandtext = "exec MODULE_PTW.dbo.PTW_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
                set objrs = objcommand.execute
                
                if not objrs.eof then
                    var_ptw_1 = objrs("mycount")  
                    set objrs = objrs.nextrecordset
                    if not objrs.eof then
                        var_ptw_2 = objrs("mycount")  
                        set objrs = objrs.nextrecordset
                        if not objrs.eof then
                            var_ptw_3 = objrs("mycount")  
                            set objrs = objrs.nextrecordset
                            if not objrs.eof then
                                var_ptw_4 = objrs("mycount")  
                                set objrs = objrs.nextrecordset
                                if not objrs.eof then
                                    var_ptw_5 = objrs("mycount")  
                                else
                                    var_ptw_5 = "Error"
                                end if
                            else
                                var_ptw_4 = "Error"
                                var_ptw_5 = "Error"
                            end if
                        else
                            var_ptw_3 = "Error"
                            var_ptw_4 = "Error"
                            var_ptw_5 = "Error"
                        end if
                    else
                        var_ptw_2 = "Error"
                        var_ptw_3 = "Error"
                        var_ptw_4 = "Error"
                        var_ptw_5 = "Error"
                    end if                    
               else
                    var_ptw_1 = "Error"
                    var_ptw_2 = "Error"
                    var_ptw_3 = "Error"
                    var_ptw_4 = "Error"
                    var_ptw_5 = "Error"
               end if 
               
                'show future records
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc"">Future Dated Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_1 & "</div></div><div class=""clear""></div></div>"
            
                'show expired projects
                response.Write "<div class=""statistic_holder""><div class=""statistic_desc"">Expired Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_2 & "</div></div><div class=""clear""></div></div>"
            
                'show complete active
                 response.Write "<div class=""statistic_holder""><div class=""statistic_desc"">Active Permits</div><div class=""dots""><div class=""statistic_value"">" & var_ptw_3 & "</div></div><div class=""clear""></div></div>"
                
                'show incomplete future
                 if session("YOUR_ACCESS") = "0" then var_search_criteria = "ptw_frm_srch_showop=Incomplete&ptw_srch_type_flag=A" else  var_search_criteria = "vtype=user&ptw_frm_srch_showop=Incomplete&ptw_srch_type_flag=A"
                 response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/permit_to_work_v2/app/ptw_search.asp?" & var_search_criteria & "&ptw_frm_srch_submit=Search Permits"" >Incomplete Permits</a></div><div class=""dots""><div class=""statistic_value"">" & var_ptw_4 & "</div></div><div class=""clear""></div></div>"
          
                'show complete awaiting authorisation
                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/permit_to_work_v2/app/ptw_search.asp?ptw_frm_srch_showop=Awaiting%20Authorisation&ptw_srch_type_flag=A&ptw_frm_srch_submit=Search Permits"" >Permits Awaiting Authorisation</a></div><div class=""dots""><div class=""statistic_value"">" & var_ptw_5 & "</div></div><div class=""clear""></div></div>"
          
                
'          elseif mod_code = "SA" then
'                'Grab data from DB via SP
'                objcommand.commandtext = "exec MODULE_SA.dbo.SA_Console_Stats '" & view_access & "', '" & session("YOUR_ACCOUNT_ID") & "' ,'" & session("corp_code") & "' ,'" & session("YOUR_COMPANY") & "'"
'                set objrs = objcommand.execute
'                
'                if not objrs.eof then
'                    var_sa_1 = objrs("mycount")  
'                    set objrs = objrs.nextrecordset
'                    if not objrs.eof then
'                        var_sa_2 = objrs("mycount")  
'                    else
'                        var_sa_2 = "Error"
'                    end if                    
'               else
'                    var_sa_1 = "Error"
'                    var_sa_2 = "Error"
'               end if 
'               
'                'show complete records
'                response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/safety_audit/v2/app/saudit_search.asp?sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=complete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc&sa_frm_srch_submit=Search Audits"" >Complete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_sa_1 & "</div></div><div class=""clear""></div></div>"
'            
'                'show incomplete records
'                 if session("YOUR_ACCESS") = "0" then var_search_criteria = "sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=Incomplete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc" else  var_search_criteria = "sa_frm_srch_temp_title=any&sa_frm_srch_dateop=0&sa_frm_srch_showop=Incomplete&sa_frm_srch_sop=audit_datetime&sa_frm_srch_order=desc"
'                 response.Write  "<div class=""statistic_holder""><div class=""statistic_desc""><a href=""../../../version3.2/modules/hsafety/safety_audit/v2/app/saudit_search.asp?" & var_search_criteria & "&sa_frm_srch_submit=Search Audits"" >Incomplete Audits</a></div><div class=""dots""><div class=""statistic_value"">" & var_sa_2 & "</div></div><div class=""clear""></div></div>"
            
        
          
      end if
      
      
      response.Write  "</td></tr></table></div></div><hr />"
      
      
 
 
end sub


%>