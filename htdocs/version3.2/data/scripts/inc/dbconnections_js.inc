<%

SUB endCONNECTIONS()

	Objconn.close
	SET objCommand.ActiveConnection = NOTHING
	SET objcommand = NOTHING
	SET objconn = NOTHING
	SET objrs = NOTHING

END SUB

SUB startCONNECTIONS()

	SET objconn = Server.CreateObject("ADODB.Connection")
	WITH objconn
		.ConnectionString = Application("DBCON_MODULE_HR")
		.ConnectionTimeout = 900
		.Open
	END WITH

	SET objCommand = Server.CreateObject("ADODB.Command")
	SET objCommand.ActiveConnection = objconn
	
END SUB

function validateInput(inputValue)
	if not isnull(inputValue) then
		validateInput = replace(server.HTMLEncode(inputValue),"'","`")
	else
		validateInput = inputValue
	end if
end function

%>