<%

' Sub to print out a form for inserting a new record into the database

' Required variables:
' (date)sentDate - The date sent in the QueryString, or a default date in its absence
' (string)redirect - The page to send user back to
' (string)newEventUsers - 'options' for users

Sub insertNewEvent(sentDate,redirect,newEventUsers)
%>

<br />
<form action='add_event.asp?referer=<%= redirect %>&amp;date=<%= sentDate %>&amp;cal_user=<%= Request("cal_user") %>' method='post' onsubmit='return valAddNewEvent(this)'>

<div id='new_event'<%
If "TRUE" <> Request.QueryString("show_add_event") Then
    Response.Write " style='display: none;'"
End if 
%> class='details'>
<table border='0' class='new_event' width='100%'>
  <tr>
    <th width='2%' nowrap='nowrap'>All day?</th>
    <td><input type='checkbox' name='new_all_day' onclick='toggleTime(this,this.form)' /></td>
    <th width='2%'>Category:</th>
    <td>
        <select name='new_cat'>
<%
    objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_SD_CATEGORY")  & _
                             " WHERE CORP_CODE='" & Session("CORP_CODE") & "'" & _
                             "   AND is_selectable='1' " & _
                             " ORDER BY cat_label"
                
    Set objRs = objCommand.Execute
    While NOT objRs.EOF
        Response.Write "<option value='" & objRs("ID") & "'>" & objRs("cat_label") & "</option>"
        objRs.MoveNext
    Wend
%>
        </select>
    </td>
  </tr>
  <tr>
    <th>Start:</th>
    <td><select name='new_start_d' onchange='checkFromDate(this.form); advanceToDate(this.form,"d")'>
            <option value=''>DD</option>
<%
    For i = 1 to 31
        If Len(i) = 1 Then
            padding = "0"
        Else
            padding = ""
        End if
        If i = Day(sentDate) Then
            Response.Write "<option selected='selected' value='" & padding & i & "'>" & i & "</option>"
        Else
            Response.Write "<option value='" & padding & i & "'>" & i & "</option>"
        End if
    Next
%>
        </select>
        
        <select name='new_start_m' onchange='checkFromDate(this.form); advanceToDate(this.form,"m")'>
            <option value=''>MM</option>
<%
    For i = 1 to 12
        If Len(i) = 1 Then
            padding = "0"
        Else
            padding = ""
        End if
        If i = Month(sentDate) Then
            Response.Write "<option value='" & padding & i & "' selected='selected'>" & MonthName(i,true) & "</option>"
        Else
            Response.Write "<option value='" & padding & i & "'>" & MonthName(i,true) & "</option>"
        End if
    Next
%>      
        </select>
        
        <select name='new_start_y' onchange='checkFromDate(this.form); advanceToDate(this.form,"y")'>
            <option value=''>YYYY</option>
<%
    For i = Year(Date()) to Year(Date()) + 3
        If i = Year(sentDate) Then
            Response.Write "<option selected='selected' value='" & i & "'>" & i & "</option>"
        Else
            Response.Write "<option value='" & i & "'>" & i & "</option>"
        End if
    Next
%>      
        </select>
         at <% call AutoTime("12:00","new_start_h","new_start_min") %></td>
    <th rowspan='4' class='top'>Description:</th>
    <td rowspan='4' width='40%' class='top'><textarea name='new_description' style='width: 97%;' rows='5'><%= Request.QueryString("descr_text") %></textarea></td>
  </tr>
  <tr>
    <th>End:</th>
    <td><select name='new_end_d' onchange='checkToDate(this.form)'>
            <option value=''>DD</option>
<%
    For i = 1 to 31
        If Len(i) = 1 Then
            padding = "0"
        Else
            padding = ""
        End if
        If i = Day(sentDate) Then
            Response.Write "<option selected='selected' value='" & padding & i & "'>" & i & "</option>"
        Else
            Response.Write "<option value='" & padding & i & "'>" & i & "</option>"
        End if
    Next
%>
        </select>
        
        <select name='new_end_m' onchange='checkToDate(this.form)'>
            <option value=''>MM</option>
<%
    For i = 1 to 12
        If Len(i) = 1 Then
            padding = "0"
        Else
            padding = ""
        End if
        If i = Month(sentDate) Then
            Response.Write "<option value='" & padding & i & "' selected='selected'>" & MonthName(i,true) & "</option>"
        Else
            Response.Write "<option value='" & padding & i & "'>" & MonthName(i,true) & "</option>"
        End if
    Next
%>      
        </select>
        
        <select name='new_end_y' onchange='checkToDate(this.form)'>
            <option value=''>YYYY</option>
<%
    For i = Year(Date()) to Year(Date()) + 3
        If i = Year(sentDate) Then
            Response.Write "<option selected='selected' value='" & i & "'>" & i & "</option>"
        Else
            Response.Write "<option value='" & i & "'>" & i & "</option>"
        End if
    Next
%>      
        </select>
         at <% call AutoTime("12:00","new_end_h","new_end_min") %></td>
  </tr>
  <tr>
    <th>For:</th>
    <td><select name='new_person'>
            <option value='me'>Me</option>
            <%= newEventUsers %>
        </select>
    </td>
  </tr>
  <tr>
    <td colspan='2'>&nbsp;</td>
  </tr>
  <tr>
    <th nowrap='nowrap'>Alert me:</th>
    <td><input type='checkbox' name='new_alert' onclick='toggleAlert(this,this.form)' />
      <select name='new_alert_num' disabled='disabled'>
        <option>1</option>
        <option>2</option>
        <option>3</option>
      </select>
      <select name='new_alert_period' disabled='disabled'>
        <option>days</option>
        <option>weeks</option>
        <option>months</option>
      </select> before
    </td>
    <td>&nbsp;</td>
    <td class='l'><input type='image' src='../../../../images/icons/sml_add_event.png' /></td>
  </tr>
</table>

</div>
<a name='add_event'>&nbsp;</a>

</form>
<%
End sub
%>