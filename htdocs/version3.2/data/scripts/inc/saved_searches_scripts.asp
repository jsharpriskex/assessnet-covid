<!-- #INCLUDE FILE="dbconnections.inc" -->
<!-- #INCLUDE FILE="saved_searches.asp" -->
<!-- #INCLUDE FILE="../../functions/func_random.asp" -->
<%


dim objrs
dim objconn
dim objcommand

call startconnections()

if request("cmd") = "ss_sav" then ' request to save search data
    select case request("mod") ' grab module
        case "RA"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_ra")
            Session("srch_ra_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_fs")
            Session("srch_fs_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_sa")
            Session("srch_sa_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_qa")
            Session("srch_qa_result") = "SS_Saved"
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
        case "LB"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "lb_frm")
            Session("lb_frm_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
        case "INS"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_ins")
            Session("srch_ins_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end
        case "HAZR"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_hazr")
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end
        case "TM"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_task")
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end
         case "PUWER"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_puwer")
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end
        case "PTW"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_ptw")
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end
        case "HP"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_hp")
            Session("srch_hp_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end 
        case "ACC"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "srch_acc")
            Session("srch_acc_result") = "SS_Saved"
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end               
        case "SA_REP"
            call pr_ss_script_save(validateinput(request("mod")), validateinput(request("tb_frm_ss_title")), validateinput(request("tb_frm_ss_desc")), validateinput(request("cb_frm_ss_default")), "REP_SA")
            Session("rep_audit_result") = "SS_Saved"
            session("report_sa") = "N"
            response.redirect("../../../modules/btools/audit_report.asp")
            response.end               
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select

elseif request("cmd") = "ss_retrieve" then ' request to use existing saved search data

    select case request("mod")  ' grab module
        case "RA"        
            call pr_ss_script_del_session_vars("srch_ra")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ra_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"        
            call pr_ss_script_del_session_vars("srch_fs")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_fs_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"        
            call pr_ss_script_del_session_vars("srch_sa")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_sa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"        
            call pr_ss_script_del_session_vars("srch_qa")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_qa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
        case "LB"        
            call pr_ss_script_del_session_vars("lb_frm")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
           ' LB doesn't use normal search methods at the moment so always resets
           
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
         case "INS"        
            call pr_ss_script_del_session_vars("srch_ins")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ins_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end
         case "HAZR"        
            call pr_ss_script_del_session_vars("srch_hazr")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hazr_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end
        case "TM"        
            call pr_ss_script_del_session_vars("srch_task")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            'session("srch_tasks_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end      
        case "PUWER"        
            call pr_ss_script_del_session_vars("srch_puwer")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_puwer_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end
        case "PTW"        
            call pr_ss_script_del_session_vars("srch_ptw")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ptw_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end    
        case "HP"        
            call pr_ss_script_del_session_vars("srch_hp")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hp_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end       
        case "ACC"        
            call pr_ss_script_del_session_vars("srch_acc")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_acc_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end         
        case "SA_REP"        
            call pr_ss_script_del_session_vars("REP_SA")
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            Session("report_sa") = "Y"
            session("report_sa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/btools/audit_report.asp")
            response.end      
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select
elseif request("cmd") = "ss_del" then

    select case request("mod")  ' grab module
        case "RA"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_ra_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))      
            Session("srch_fs_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"
          call pr_ss_script_del_sav_search(validateinput(request("ref")))      
            Session("srch_sa_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"
          call pr_ss_script_del_sav_search(validateinput(request("ref")))      
            Session("srch_qa_result") = "SS_Del"
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
       case "LB"
          call pr_ss_script_del_sav_search(validateinput(request("ref")))      
            Session("lb_frm_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
        case "INS"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_ins_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end      
        case "HAZR"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_hazr_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end  
        case "TM"                
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            'Session("srch_tasks_result") = "SS_Del"
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end  
        case "PUWER"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_puwer_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end  
        case "PTW"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_ptw_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end  
        case "HP"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_hp_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end
        case "ACC"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("srch_acc_result") = "SS_Del"
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end
        case "SA_REP"        
            call pr_ss_script_del_sav_search(validateinput(request("ref")))           
            Session("rep_audit_result") = "SS_Del"
            response.redirect("../../../modules/btools/audit_report.asp")
            response.end
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select

elseif request("cmd") = "ss_make_default" then ' mark selected as default and request to use existing saved search data

    select case request("mod")  ' grab module
        case "RA"        
            call pr_ss_script_del_session_vars("srch_ra")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ra_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"        
            call pr_ss_script_del_session_vars("srch_fs")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_fs_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"        
            call pr_ss_script_del_session_vars("srch_sa")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_sa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"        
            call pr_ss_script_del_session_vars("srch_qa")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_qa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
        case "LB"        
            call pr_ss_script_del_session_vars("lb_frm")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
           ' LB doesn't use normal search methods at the moment so always resets
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
         case "INS"        
            call pr_ss_script_del_session_vars("srch_ins")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ins_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end
         case "HAZR"        
            call pr_ss_script_del_session_vars("srch_hazr")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hazr_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end
        case "TM"        
            call pr_ss_script_del_session_vars("srch_task")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            'session("srch_tasks_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end      
        case "PUWER"        
            call pr_ss_script_del_session_vars("srch_puwer")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_puwer_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end
        case "PTW"        
            call pr_ss_script_del_session_vars("srch_ptw")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ptw_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end    
        case "HP"        
            call pr_ss_script_del_session_vars("srch_hp")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hp_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end       
        case "ACC"        
            call pr_ss_script_del_session_vars("srch_acc")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_acc_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end     
        case "SA_REP"        
            call pr_ss_script_del_session_vars("rep_audit")
            call pr_ss_script_make_default(validateinput(request("mod")), validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("report_sa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/btools/audit_report.asp")
            response.end        
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select

elseif request("cmd") = "ss_set_default" then ' mark selected as default and request to use existing saved search data

    select case request("mod")  ' grab module
        case "RA"        
            call pr_ss_script_del_session_vars("srch_ra")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ra_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"        
            call pr_ss_script_del_session_vars("srch_fs")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_fs_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"        
            call pr_ss_script_del_session_vars("srch_sa")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_sa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"        
            call pr_ss_script_del_session_vars("srch_qa")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_qa_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
        case "LB"        
            call pr_ss_script_del_session_vars("lb_frm")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
           ' LB doesn't use normal search methods at the moment so always resets
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
         case "INS"        
            call pr_ss_script_del_session_vars("srch_ins")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ins_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end
         case "HAZR"        
            call pr_ss_script_del_session_vars("srch_hazr")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hazr_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end
        case "TM"        
            call pr_ss_script_del_session_vars("srch_task")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            'session("srch_tasks_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end      
        case "PUWER"        
            call pr_ss_script_del_session_vars("srch_puwer")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_puwer_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end
        case "PTW"        
            call pr_ss_script_del_session_vars("srch_ptw")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_ptw_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end    
        case "HP"        
            call pr_ss_script_del_session_vars("srch_hp")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_hp_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end       
        case "ACC"        
            call pr_ss_script_del_session_vars("srch_acc")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            call pr_ss_script_make_default(validateinput(request("ref")))
            call pr_ss_script_retrieve_session_vars(validateinput(request("ref")))
            session("srch_acc_reset") = "Y" 'force the query reload
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end        
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select

elseif request("cmd") = "ss_remove_default" then ' remove the default flag for the current module (only 1 default per module, so just set all to non-default)

    select case request("mod")  ' grab module
        case "RA"        
            call pr_ss_script_del_session_vars("srch_ra")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_ra_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/risk_assessment/app/rassessment_search.asp")
            response.end
        case "FS2"        
            call pr_ss_script_del_session_vars("srch_fs")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_fs_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/fire_safety_v2/app/firesafety_search.asp")
            response.end
        case "SA"        
            call pr_ss_script_del_session_vars("srch_sa")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_sa_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/safety_audit/v2/app/saudit_search.asp")
            response.end
        case "QA"        
            call pr_ss_script_del_session_vars("srch_qa")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_qa_reset") = "N" 'force the query reload
            response.redirect("../../../modules/quality/quality_audit/v2/app/qaudit_search.asp")
            response.end
        case "LB"        
            call pr_ss_script_del_session_vars("lb_frm")
            call pr_ss_script_remove_default(validateinput(request("mod")))
           ' LB doesn't use normal search methods at the moment so always resets
            response.redirect("../../../modules/hsafety/logbook/v2/app/logbook_search.asp")
            response.end
         case "INS"        
            call pr_ss_script_del_session_vars("srch_ins")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_ins_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/inspection/app/insp_search.asp")
            response.end
         case "HAZR"        
            call pr_ss_script_del_session_vars("srch_hazr")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_hazr_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_register/app/hazard_search.asp")
            response.end
        case "TM"        
            call pr_ss_script_del_session_vars("srch_task")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            'session("srch_tasks_reset") = "N" 'force the query reload
            response.redirect("../../../modules/management/taskmanager_v2/tasks.asp")
            response.end      
        case "PUWER"        
            call pr_ss_script_del_session_vars("srch_puwer")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_puwer_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/puwer/app/puwer_search.asp")
            response.end
        case "PTW"        
            call pr_ss_script_del_session_vars("srch_ptw")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_ptw_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/permit_to_work_v2/app/ptw_search.asp")
            response.end    
        case "HP"        
            call pr_ss_script_del_session_vars("srch_hp")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_hp_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/hazard_profile/app/hprofile_search.asp")
            response.end       
        case "ACC"        
            call pr_ss_script_del_session_vars("srch_acc")
            call pr_ss_script_remove_default(validateinput(request("mod")))
            session("srch_acc_reset") = "N" 'force the query reload
            response.redirect("../../../modules/hsafety/accident/app/acc_search.asp")
            response.end        
        case else ' no module found - go back to last page
            response.redirect(Request.ServerVariables("HTTP_REFERER"))
            response.end
    end select

end if

call endconnections()
%>