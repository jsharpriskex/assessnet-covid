
<%


Dim passDic
Set passDic=Server.CreateObject("Scripting.Dictionary")
passDic.Add "password", "password"
passDic.Add "p@ssword", "p@ssword"
passDic.Add "passw0rd", "passw0rd"
passDic.Add "p@ssw0rd", "p@ssw0rd"
passDic.Add "pa55word", "pa55word"
passDic.Add "p@55word", "p@55word"
passDic.Add "pa55w0rd", "pa55w0rd"
passDic.Add "p@55w0rd", "p@55w0rd"

passDic.Add "findiapass", "findiapass"
passDic.Add "safety", "safety"
passDic.Add "google", "google"
passDic.Add "online", "online"
passDic.Add "assessnet", "assessnet"
passDic.Add "riskex", "riskex"

if passDic.Exists(lcase(session("YOUR_FIRSTNAME"))) = false then  'firstname
    passDic.Add lcase(session("YOUR_FIRSTNAME")), lcase(session("YOUR_FIRSTNAME"))
end if
if passDic.Exists(lcase(session("YOUR_SURNAME"))) = false then  'surname
    passDic.Add lcase(session("YOUR_SURNAME")), lcase(session("YOUR_SURNAME"))
end if
if passDic.Exists(lcase(session("YOUR_MANSWER"))) = false then  'memorable answer
    passDic.Add lcase(session("YOUR_MANSWER")), lcase(session("YOUR_MANSWER"))
end if
if passDic.Exists(lcase(session("YOUR_FIRSTNAME") & " " & session("YOUR_SURNAME"))) = false then  'full name (with space)
    passDic.Add lcase(session("YOUR_FIRSTNAME") & " " & session("YOUR_SURNAME")),  lcase(session("YOUR_FIRSTNAME") & " " & session("YOUR_SURNAME"))
end if
if passDic.Exists(lcase(session("YOUR_FIRSTNAME") & session("YOUR_SURNAME"))) = false then  'full name (without space)
    passDic.Add lcase(session("YOUR_FIRSTNAME") & session("YOUR_SURNAME")),  lcase(session("YOUR_FIRSTNAME") & session("YOUR_SURNAME"))
end if


ary_username = split(session("YOUR_USERNAME"),".")
if passDic.Exists(lcase(ary_username(0))) = false then  'first part of username
    passDic.Add lcase(ary_username(0)), lcase(ary_username(0))
end if
if passDic.Exists(lcase(ary_username(1))) = false then  'second part of username
    passDic.Add lcase(ary_username(1)), lcase(ary_username(1))
end if
if passDic.Exists(lcase(session("YOUR_USERNAME"))) = false then  'full username
    passDic.Add lcase(session("YOUR_USERNAME")), lcase(session("YOUR_USERNAME"))
end if


'further implementation!!!

'http://go.utimaco.com/manual/safeguard-easy/v45/1-142.html
'Enter trivial passwords such as test, system, user etc. in the list. Each password which is significantly similar to 
'the forbidden password will be rejected. "Significantly similar" in this context means that the character sequence of 
'the password must differ in at least 20% from the character sequence of the forbidden password. For example, if "tester" 
'is on the list the password "tester1234" is allowed whereas "tester12" is forbidden.

'You can also use wildcards to define trivial passwords. The only accepted wildcard character is "*" (asterisk). This means 
'that, at the position indicated by the character "*", the password can contain one different character. For example, if 
'you enter "ut*ma*o", any password like "utimaco", "ut1ma2o" is forbidden.



function check_user_password(var_acc_ref, var_acc_username, var_pi_password, var_pi_new_password, var_pi_confirm_password, var_check_existing)

    'var_pi_password            : the users current password
    'var_pi_new_password        : the users new password
    'var_pi_confirm_password    : the users confirmation password
    'var_check_existing         : true/false if at login process set to false, otherwise set to true

   
    
    
    password_blocked = false
    if (len(trim(var_pi_new_password)) = 0) or (len(trim(var_pi_confirm_password)) = 0) then
        'check password and confirmation have been entered
        password_blocked = true
        block_reason = "003"
    elseif (var_pi_new_password <> var_pi_confirm_password) then
        'check password and confirmation match
        password_blocked = true
        block_reason = "001"
    end if
    

    if password_blocked = false and (left(var_pi_new_password,1) = " " or right(var_pi_new_password,1) = " ") then
        'don't allow whitespace at the beginning or end of passwords
        password_blocked = true
        block_reason = "009"
    end if    
    
    if len(var_pi_new_password) > 0 then
        'check the minimum length of the new password
        if len(var_pi_new_password) < 7 then
            password_blocked = true
            block_reason = "006"
        end if

        'check new password against blocklist
        if password_blocked = false then
            if passDic.Exists(lcase(var_pi_new_password)) then    'lcase(var_pi_new_password) = 
                password_blocked = true
                block_reason = "004"
            end if
        end if
        
        'check the new password hasn't been used recently
        if password_blocked = false then
            objCommand.commandText = "SELECT TOP 3 acc_pass FROM " & Application("DBTABLE_USER_PASSWORD") & " " & _
                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND acc_ref = '" & var_acc_ref & "' " & _ 
                                     "ORDER BY gendatetime DESC"
            set objRs = objCommand.execute
            if not objRs.EOF then
                while not objRs.EOF 
                    if objRs("acc_pass") = var_pi_new_password then
                        password_blocked = true
                        block_reason = "005"
                    end if
                    objRs.movenext
                wend
            end if
            set objRs = nothing
        end if
        
    else
        'check that the current password isn't on the blocklist (for login script only)
        if lcase(var_pi_password) = passDic.Item(lcase(var_pi_password)) then
            password_blocked = true
            block_reason = "004"
        end if
    end if

   
    'check user has entered their current password correctly (for user control script only)
    if var_check_existing = True and lcase(session("YOUR_PASSWORD")) <> lcase(var_pi_password) then
        password_blocked = true
        block_reason = "007"
    end if
    
    
    'return block_reason and update if valid
    if password_blocked = true then
        check_user_password = block_reason
    else
        'update the user data table
        objCommand.commandText = "UPDATE " & Application("DBTABLE_USER_DATA") & " " & _
                                 "SET acc_pass = '" & var_pi_new_password & "' " & _
                                 "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND acc_ref = '" & var_acc_ref & "' " & _
                                 "  AND acc_name = '" & var_acc_username & "'"
        objCommand.execute
        
        'update the users password history
        objCommand.commandText = "INSERT INTO " & Application("DBTABLE_USER_PASSWORD") & " (corp_code, acc_ref, acc_pass, gendatetime) " & _
                                 "VALUES ('" & session("CORP_CODE") & "', '" & var_acc_ref & "', '" & var_pi_new_password & "', GETDATE())"
        objCommand.execute
        
        session("YOUR_PASSWORD") = var_pi_new_password
        session("PASSWORD_EXPIRED") = False             'set in syscheckuser_v2, checked again in frm_lg_accepted
        
        check_user_password = ""
    end if
end function






function check_user_current_password(var_pi_password)

    if lcase(var_pi_password) = passDic.Item(lcase(var_pi_password)) then
        check_user_current_password = "002"
    else
        check_user_current_password = ""
    end if
    
end function







function getPasswordBlockReason(block_reason)

    select case block_reason
        case "001"
            getPasswordBlockReason = "** Confirmation password doesn't match. Please retry"
        case "002"
            getPasswordBlockReason = "** Your current password is on a list of prohibited words. Please enter a new one"
        case "003"
            getPasswordBlockReason = "** Please enter your new password and confirmation copy"
        case "004"
            getPasswordBlockReason = "** The password you entered is on a list of prohibited words. Please retry"
        case "005"
            getPasswordBlockReason = "** The password entered has already been used. Please retry"
        case "006"
            getPasswordBlockReason = "** The password entered is to short. Please retry"
        case "007"
            getPasswordBlockReason = "** Your current password is incorrect. Please retry"
        case "008"
            getPasswordBlockReason = "** Your password has expired. Please enter a new one"
        case "009"
            getPasswordBlockReason = "** Your password cannot contain spaces at the beginning or end. Please retry"
        case else
            getPasswordBlockReason = ""
    end select

end function


%>