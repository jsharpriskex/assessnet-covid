<%

'ON ERROR RESUME NEXT

DIM strM, strD, strY, strHr, strMin, menuBG, menuSTATE, strBlnk, strFsetting

' strDATE 		Today`s date. Format:  
' strDname  	Value of HTML `name` attribute (day)
' strMname  	Value of HTML `name` attribute (month)
' strYname  	Value of HTML `name` attribute (Year)
' strFsetting	`F` for future years `C` for going back from 1900 and now showing future years
' switch1		Javascript call

Sub AutoDATE(strDATE,strDname,strMname,strYname,strFsetting,status,switch1,switch2)

strD = 1
strM = 1

var_future_years_setting = 3

if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
elseif strFsetting = "C" then
	strY = 1900	
	var_future_years_setting = 0
	var_dateformat = 4
else
	strY = 1990
end if

'used for limiting the calendar control below
strYStartValue = strY

strBlnk = "N"

If strDATE = "NONE" then
	'Setup Day Menu
	Response.write "<select id='" & strDname & "' name='" & strDname & "' class='select' " & status & " " & switch1 & ">"
	Response.write "<option value='DD' style='background-color: #FFF000' selected='selected' selected>DD</option>" & vbCrLf
	Do until strD > 31
		if strD < 10 then
			strD = "0" & strD
		else
			strD = strD
		End IF	
		Response.write "<option value='" & strD & "'>" & strD & "</option>" & vbCrLf
		strD = strD + 1
	Loop
	Response.Write "</select>&nbsp;"
	
	'Setup Month Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & " " & switch1 & ">"
	Response.write "<option value='MM' style='background-color: #FFF000' selected='selected' selected>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
		ELSE
			strM = strM
		End IF
		    IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'>" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'>" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
			strM = strM + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Year Menu
	Response.write "<select id='" & strYname & "' name='" & strYname & "' class='select' " & status & " " & switch1 & ">"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected='selected' selected>YY</option>" & vbCrLf
		Do until strY > Year(date) + var_future_years_setting	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        if var_dateformat = 4 then
		        Response.write "<option value='" & strY & "'>" & right(strY,4) & "</option>" & vbCrLf
		        else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		        end if    
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else


	DAYstrDATE = Day(strDATE)
	MONTHstrDATE = Month(strDATE)

	If Len(Day(strDATE)) < 2 then
	DAYstrDATE = "0" & DAYstrDATE
	End IF

	If Len(Month(strDATE)) < 2 then
		MONTHstrDATE = "0" & MONTHstrDATE
	End IF

		'Setup Day Menu
		Response.write "<select id='" & strDname & "' name='" & strDname & "'  class='select' " & status & " " & switch1 & ">"
		Response.write "<option value='DD'>DD</option>" & vbCrLf
			Do until strD > 31
		
				If strD < 10 then
				strD = "0" & strD
					ELSE
					strD = strD
				End IF
		
			IF strD = DAYstrDATE then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected='selected'"
		    Else
				menuBG = ""
				menuSTATE = ""
			End IF			
	
			Response.write "<option value='" & strD & "'" & menuBG & menuSTATE & ">" & strD & "</option>" & vbCrLf
			strD = strD + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Month Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & " " & switch1 & ">"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
			ELSE
			strM = strM
		End IF
	
			IF strM = MONTHstrDATE then
                menuBG = " style='background-color: #fff000;'"
                menuSTATE = " selected='selected'"
            Else
				menuSTATE = ""
				menuBG = ""
			End IF
			
			IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
		
			strM = strM + 1
		LOOP
	Response.write "</SELECT>&nbsp;"

	'Setup Year Menu
	Response.write "<SELECT id='" & strYname & "' name='" & strYname & "' class='select' " & status & " " & switch1 & ">"
	
	if var_dateformat = 4 then
	Response.write "<OPTION value='YYYY'>YYYY</option>" & vbCrLf
	    else
	    Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
	end if
	
		Do until strY > Year(date) + var_future_years_setting
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected='selected'"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    if var_dateformat = 4 then
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			        else
			        Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			    end if 
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

	' ****************************************************
	' ****************************************************
	
	'             For the calendar control
	
	' ****************************************************
	' ****************************************************
	
	Randomize
	uniqueNumber = cint(Rnd*1000)
	functionName = "setMultipleValues3_" & uniqueNumber
	calReference = "cal" & uniqueNumber
	
	Response.write  "<div id='calendarDiv" & uniqueNumber & "' style='position:absolute;visibility:hidden;background-color:#FFFFFF !important;z-index: 1' class='overrideStyle'></div>"
	
	
	Response.write 	"<script language='JavaScript'>var " & calReference & " = new CalendarPopup(""calendarDiv" & uniqueNumber & """); " & _ 
					" " & calReference & ".setReturnFunction(""" & functionName & """); " & _
					" " & calReference & ".showYearNavigation(); " & _
					" " & calReference & ".setCssPrefix(""calControl_""); " & _
					" " & calReference & ".addDisabledDates(null,""31/12/" & strYStartValue-1 & """); " & _
					" " & calReference & ".addDisabledDates(""01/01/" & strY & """,null); " & _
					" /*fill in the boxes on the screen*/ " & _
					"function " & functionName & "(y,m,d) { " & _
					"    document.getElementById(""" & strYname & """).value=y; " & _
					"    document.getElementById(""" & strMname & """).selectedIndex=m; " & _
					"	 document.getElementById(""" & strDname & """).selectedIndex=d; } </script>"

	if status = "disabled" then
		Response.write "<span class='calIcon_dis'>&nbsp;</span>"
	else
		Response.Write "<a href='#' class='calIcon' onclick='" & calReference & ".showCalendar(""anchor" & uniqueNumber & """,getDateString(document.getElementById(""" & strYname & """),document.getElementById(""" & strMname & """),document.getElementById(""" & strDname & """))); return false;' title='Display a calendar' NAME='anchor" & uniqueNumber & "' ID='anchor" & uniqueNumber & "'>&nbsp;</a>"
	end if
	
	' ****************************************************
	' ****************************************************
	
	'               end of calendar contol
	
	' ****************************************************
	' ****************************************************



End Sub


Sub AutoTIME(strTIME,strHname,strMname,status,switch1,switch2)
    strHr = 0
    strMin = 0

IF UCase(strTIME) = "NONE" Then

	'Setup Hours Menu
	Response.write "<SELECT id='" & strHname & "' name='" & strHname & "' class='select' " & status & ">"
	Response.write "<option value='HH' SELECTED>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected='selected'"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM' SELECTED>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>"

else
	if instr(strTIME, "/") > 0 then

		'time was set to 00:00 on a previous screen
		strTIME = "00:00"
		HOURstrTIME = 00
		MINstrTIME = 00
	else
		HOURstrTIME = Hour(strTIME)
		MINstrTIME = Minute(strTIME)
	end if
    
    If Len(Hour(strTIME)) < 2 then
        HOURstrTIME = "0" & HOURstrTIME
    End IF
    If Len(Minute(strTIME)) < 2 then
        MINstrTIME = "0" & MINstrTIME
    End if

	'Setup Hours Menu
	Response.write "<SELECT id='" & strHname & "' name='" & strHname & "' class='select' " & status & ">"
	Response.write "<option value='HH'>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected='selected'"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>"

END IF
	
End Sub


Sub AutoTIMEseconds(strTIME,strHname,strMname,strSname,status,switch1,switch2)
    strHr = 0
    strMin = 0

IF UCase(strTIME) = "NONE" Then

	'Setup Hours Menu
	Response.write "<SELECT id='" & strHname & "' name='" & strHname & "' class='select' " & status & ">"
	Response.write "<option value='HH' SELECTED>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected='selected'"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM' SELECTED>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>:"
	
	
	'Setup Secs Menu
	Response.write "<select id='" & strSname & "' name='" & strSname & "' class='select' " & status & ">"
	Response.write "<option value='SS' SELECTED>SS</option>" & vbCrLf
	
		Do until strSec > 59

		If strSec < 10 then
		strSec = "0" & strSec
			ELSE
			strSec = strSec
		End IF

				IF strSec = SECstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strSec & "'" & menuBG & menuSTATE & ">" & strSec & "</option>" & vbCrLf
			strSec = strSec + 1
		LOOP
	Response.write "</select>"	

else
'	if instr(strTIME, "/") > 0 then

'		'time was set to 00:00:00 on a previous screen
'		strTIME = "00:00:00"
'		HOURstrTIME = 00
'		MINstrTIME = 00
'		SECstrTIME = 00
'	else
		HOURstrTIME = Hour(strTIME)
		MINstrTIME = Minute(strTIME)
		SECstrTime = Second(strTIME)
'	end if
    
    If Len(Hour(strTIME)) < 2 then
        HOURstrTIME = "0" & HOURstrTIME
    End IF
    If Len(Minute(strTIME)) < 2 then
        MINstrTIME = "0" & MINstrTIME
    End if
    If Len(Second(strTIME)) < 2 then
        SECstrTIME = "0" & SECstrTIME
    End if	

	'Setup Hours Menu
	Response.write "<SELECT id='" & strHname & "' name='" & strHname & "' class='select' " & status & ">"
	Response.write "<option value='HH'>HH</option>" & vbCrLf
		Do until strHr > 23
			if strHr < 10 then
				strHr = "0" & strHr
			else
				strHr = strHr
			End IF
	
			IF strHr = HOURstrTIME then
				menuBG = " style='background-color: #fff000;'"
				menuSTATE = " selected='selected'"
			Else
				menuSTATE = ""
				menuBG = ""
			End IF
		
			Response.write "<option value='" & strHr & "'" & menuBG & menuSTATE & ">" & strHr & "</option>" & vbCrLf
			strHr = strHr + 1
		Loop
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select id='" & strMname & "' name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
	
		Do until strMin > 59

		If strMin < 10 then
		strMin = "0" & strMin
			ELSE
			strMin = strMin
		End IF

				IF strMin = MINstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strMin & "'" & menuBG & menuSTATE & ">" & strMin & "</option>" & vbCrLf
			strMin = strMin + 1
		LOOP
	Response.write "</select>:"

	'Setup Mins Menu
	Response.write "<select id='" & strSname & "' name='" & strSname & "' class='select' " & status & ">"
	Response.write "<option value='SS'>MM</option>" & vbCrLf
	
		Do until strSec > 59

		If strSec < 10 then
		strSec = "0" & strSec
			ELSE
			strSec = strSec
		End IF

				IF strSec = SECstrTIME then
                    menuBG = " style='background-color: #fff000;'"
                    menuSTATE = " selected='selected'"
                Else
					menuSTATE = ""
					menuBG = ""
				End IF
			
			Response.write "<option value='" & strSec & "'" & menuBG & menuSTATE & ">" & strSec & "</option>" & vbCrLf
			strSec = strSec + 1
		LOOP
	Response.write "</select>"	

END IF
	
End Sub




Sub AutoYEAR(strDATE,strYname,strFsetting,status,switch1,switch2)

strD = 1
strM = 1

if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
else
	strY = 1990
end if

strBlnk = "N"

If strDATE = "NONE" then
	
	
	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select' " & status & ">"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected='selected'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else



	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select' " & status & ">"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected='selected'"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf
			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

End Sub


Sub AutoMONTHYEAR(strDATE,strMname,strYname,strFsetting,status,switch1,switch2)

strD = 1
strM = 1

if strFsetting = "F" then
	strY = Year(date)
elseif strFsetting = "M" then
	strY = 2000
else
	strY = 1990
end if

strBlnk = "N"

If strDATE = "NONE" then
		
	'Setup Month Menu
	Response.write "<select name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM' style='background-color: #FFF000' selected='selected'>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
		ELSE
			strM = strM
		End IF
		    IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'>" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'>" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
			strM = strM + 1
		LOOP
	Response.write "</select>&nbsp;"

	'Setup Year Menu
	Response.write "<select name='" & strYname & "' class='select' " & status & ">"
	Response.write "<option value='YYYY' style='background-color: #FFF000' selected='selected'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3	
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'>" & strY & "</option>" & vbCrLf
		    else
		        Response.write "<option value='" & strY & "'>" & right(strY,2) & "</option>" & vbCrLf
		    end if
			strY = strY + 1
		LOOP
	Response.write "</select>"

else

	MONTHstrDATE = Month(strDATE)

	If Len(Month(strDATE)) < 2 then
		MONTHstrDATE = "0" & MONTHstrDATE
	End IF

	
	'Setup Month Menu
	Response.write "<select name='" & strMname & "' class='select' " & status & ">"
	Response.write "<option value='MM'>MM</option>" & vbCrLf
		Do until strM > 12
	
		If strM < 10 then
			strM = "0" & strM
			ELSE
			strM = strM
		End IF
	
			IF strM = MONTHstrDATE then
                menuBG = " style='background-color: #fff000;'"
                menuSTATE = " selected='selected'"
            Else
				menuSTATE = ""
				menuBG = ""
			End IF
			
			IF strFsetting  = "Fdate" Then
		    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM) & "</option>" & vbCrLf
		    else
			    Response.write "<option value='" & strM & "'" & menuBG & menuSTATE & ">" & MonthName(strM, true) & "</option>" & vbCrLf
		    End if
		
			strM = strM + 1
		LOOP
	Response.write "</SELECT>&nbsp;"

	'Setup Year Menu
	Response.write "<SELECT name='" & strYname & "' class='select' " & status & ">"
	Response.write "<OPTION value='YYYY'>YY</option>" & vbCrLf
		Do until strY > Year(date) + 3
	
			IF strY = Year(strDATE) then
			    menuBG = " style='background-color: #fff000;'"
			    menuSTATE = " selected='selected'"
			Else
			    menuSTATE = ""
			    menuBG = "" 			
			End IF
		
		    IF strFsetting  = "Fdate" Then
			Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & strY & "</option>" & vbCrLf
			else
			    

			    Response.write "<option value='" & strY & "'" & menuBG & menuSTATE & ">" & right(strY,2) & "</option>" & vbCrLf

			end if
			
			strY = strY + 1
		LOOP
	Response.write "</select>"
	
End IF

End Sub

%>