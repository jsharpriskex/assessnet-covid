<%
  

sub AutoGenSwitchTemplateSelector(show_control, sub_sel_name)

    if show_control = "list" then
        call AutoGenListTemplate("0", sub_sel_name, true, true)
    else
        call AutoGenListTemplate("0", sub_sel_name, true, false)
    end if

end sub
    
sub AutoGenListTemplate(sub_sel_input, sub_sel_name, allow_multiple, show_multiple)

    if show_multiple = true then

        call AutoGenListTemplateMultiple(sub_sel_input, sub_sel_name)

    else

        response.write "<select size='1' name='" & sub_sel_name & "' class='select' onchange='reloadTemplateVersions(this.value, """ & sub_sel_input & """)'>" & _
                                   "<option value='any'>Any Template</option>"
               
        objCommand.commandText = "exec module_sa.dbo.sp_return_templates_for_user_search_part1 '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & session("global_usr_perm") & "'"
        response.write objcommand.commandtext    
        SET objrs = objcommand.execute
			   
	    IF NOT objrs.eof then
		    WHILE NOT objrs.eof   						 
					  
		        var_temp_title = shortenOutput(objrs("temp_title"),50)
					 	
		        IF sub_sel_input <> "any" THEN
			        IF cint(sub_sel_input) = cint(objrs("original_template_id")) THEN
				        response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "' selected>" & var_temp_title & "</option>"
			        ELSE
				        response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "'>" & var_temp_title & "</option>"
			        END IF	
		        ELSE
		            response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "'>" & var_temp_title & "</option>" 						
		        END IF
		
			    objrs.movenext
		    WEND
	    END IF
        set objRsTemplates = nothing

        response.write "</select>"
    
        if allow_multiple = true then
            response.write " <a href='#' class='btn btn-xs btn-primary' title='Filter by multiple templates' onclick='switchTemplateSelector(""list"", """ & sub_sel_name & """)'>select multiple</a>"
        end if
    end if

end sub

sub AutoGenListTemplateMultiple(sub_sel_input, sub_sel_name)



    sub_arry_input = split(sub_sel_input, ", ")


    response.write "<select name='" & sub_sel_name & "' size='8' multiple class='select'>"
               
    objCommand.commandText = "exec module_sa.dbo.sp_return_templates_for_user_search_part1 '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & session("global_usr_perm") & "'"
    response.write objcommand.commandtext    
    SET objrs = objcommand.execute
			   
	IF NOT objrs.eof then
		WHILE NOT objrs.eof   						 
					  
		    var_temp_title = shortenOutput(objrs("temp_title"),50)
					 	
		    IF sub_sel_input <> "any" THEN

                option_found = false
                for j = 0 to ubound(sub_arry_input)
			 if sub_arry_input(j) <> "any" then
				IF cint(sub_arry_input(j)) = cint(objrs("original_template_id")) THEN
                        		option_found = true
                   		end if
			end if
                next

                if option_found = true then
				    response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "' selected>" & var_temp_title & "</option>"
			    ELSE
				    response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "'>" & var_temp_title & "</option>"
			    END IF	

		    ELSE
		        response.write "<option value='" & objrs("original_template_id") & "' title='" & objrs("temp_title") & "'>" & var_temp_title & "</option>" 						
		    END IF
		
			objrs.movenext
		WEND
	END IF
    set objRsTemplates = nothing

    response.write "</select> <a href='#' class='btn btn-xs btn-primary' title='Filter by specific template versions' onclick='switchTemplateSelector(""single"", """ & sub_sel_name & """)'>select one</a>"

end sub
     
    
sub AutoGenListTemplatesSub(template_group, selected_template_id, sub_sel_name)

    template_counter = 0
    output_counter = 0

    if selected_template_id = "any" then selected_template_id = 0
    if template_group = "any" then template_group = 0


    objCommand.commandText = "EXEC Module_SA.dbo.sp_return_templates_for_user_search_part2 '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & session("GLOBAL_USR_PERM") & "', " & template_group
    set objRsTemplates = objCommand.execute

    if not objRsTemplates.EOF then
        while not objRsTemplates.EOF
            template_counter = template_counter + 1
            objRsTemplates.movenext
        wend


        if template_counter > 1 then
            objRsTemplates.movefirst

            response.write "<div style='padding-top: 2px;'><select class='select' name='" & sub_sel_name & "' id='" & sub_sel_name & "' onchange='reloadTemplateQuestions(this.value, ""any"")'>"

            if selected_template_id = 0 then
                response.write "<option value='any' title='Search for all audits of this type' selected>Any Version</option>"
            else
                response.write "<option value='any' title='Search for all audits of this type'>Any Version</option>"
            end if

            while not objRsTemplates.EOF

                output_counter = output_counter + 1

                if (cint(output_counter) = cint(template_counter) or cint(selected_template_id) = cint(objRsTemplates("template_id"))) and selected_template_id <> 0 then
                    response.write "<option value='" & objRsTemplates("template_id") & "' selected>Version " & objRsTemplates("temp_revision") & " - Created: " & objRsTemplates("gendatetime") & "</option>"
                    'reset the output counter        
                    output_counter = 0
                else
                    response.write "<option value='" & objRsTemplates("template_id") & "'>Version " & objRsTemplates("temp_revision") & " - Created: " & objRsTemplates("gendatetime") & "</option>"
                end if

                objRsTemplates.movenext
            wend

            response.write "</select></div>"
        else
            objRsTemplates.movefirst
            response.write "<input type='text' class='text' style='display:none' name='" & sub_sel_name & "' id='" & sub_sel_name & "' value='" & objRsTemplates("template_id") & "' />"
        end if
    end if
    set objRsTemplates = nothing
    
end sub    

function returnAllTemplatesList()

    objCommand.commandText = "EXEC Module_SA.dbo.sp_return_templates_for_user_search_part2 '" & session("CORP_CODE") & "', '" & session("YOUR_ACCOUNT_ID") & "', '" & session("YOUR_ACCESS") & "', '" & session("GLOBAL_USR_PERM") & "', -1"
    set objRsTemplates = objCommand.execute

    if not objRsTemplates.EOF then
        while not objRsTemplates.EOF
            
            templateList = templateList & objRsTemplates("template_id") & ":"

            objRsTemplates.movenext
        wend
    end if
    set objRsTemplates = nothing

    if len(templateList) > 0 then
        templateList = left(templateList, len(templateList)-1)
    end if

    returnAllTemplatesList = templateList

end function
  

sub AutoGenListQuestions(sub_sel_input, sub_sel_template, sub_sel_name)

    if len(sub_sel_template) = 0 or sub_sel_template = "any" or sub_sel_template = "0" then
        response.Write("<span class='info'><i>Please select a template version to filter by questions</i></span>")
    else
        sub_sel_disabled = ""

        Response.Write  "<select size='1' name='" & sub_sel_name & "' class='select' onchange='reloadTemplateAnswers(""" & sub_sel_template & """, ""any"")' " & sub_sel_disabled & ">" & _
                        "<option value='any'>Any Question</option>"
                    
                    
                if len(sub_sel_template) > 0 then
               
                    objcommand.commandtext = "SELECT questions.question_reference, questions.question_name, sections.section_title FROM " & APPLICATION("DBTABLE_SA_TEMP_INFO") & " AS template " & _
                                                "INNER JOIN " & Application("DBTABLE_SA_V2_TEMPLATE_SECTIONS") & " AS sections " & _
                                                "  ON template.template_id = sections.template_reference " & _
                                                "INNER JOIN " & Application("DBTABLE_SA_V2_TEMPLATE_QUESTIONS") & " AS questions " & _
                                                "  ON questions.template_reference = sections.template_reference " & _
                                                "  AND questions.section_reference = sections.section_reference "

                    if sub_sel_template <> "any" then
                        objcommand.commandtext = objcommand.commandtext & "WHERE template.corp_code = '" & session("CORP_CODE") & "' AND questions.template_reference = " & sub_sel_template & " "
                    end if
                    objcommand.commandtext = objcommand.commandtext & "ORDER BY template.id, sections.position, questions.position"

                    SET objrs = objcommand.execute
                    
                    IF NOT objrs.eof then
                          WHILE NOT objrs.eof
                                
                                
                            IF var_section <> objrs("section_title") then
                                
                                section_name = shortenOutput(objrs("section_title"),45)
                                
                                'response.Write "<option value='na'>&nbsp;</option>"
                                'response.Write "<option value='na' title='" & objrs("section_title") & "'> --- " & section_name & " --- </option>"

                                response.write "</optgroup>"
                                response.write "<optgroup title='" & objRs("section_title") & "' label='" & section_name & "'>"
                                
                                var_section = objrs("section_title")
                            END IF                          




                            question_name = shortenOutput(objrs("question_name"),45)

                            IF sub_sel_input <> "any" THEN
                                if sub_sel_input = objrs("question_reference") then
                                    response.Write "<option value='" & objrs("question_reference") & "' title='" & objrs("question_name") & "' selected>" & question_name & "</option>"
                                else
                                    response.Write "<option value='" & objrs("question_reference") & "' title='" & objrs("question_name") & "'>" & question_name & "</option>"
                                end if
                            ELSE
                                response.Write "<option value='" & objrs("question_reference") & "' title='" & objrs("question_name") & "'>" & question_name & "</option>"
                            END IF
                            
                            objrs.movenext
                          WEND
                    END IF
                    set objrs = nothing

                end if

          Response.Write "</select>"    
    end if
end sub



sub AutoGenListAnswers(sub_sel_input, sub_sel_question, sub_sel_template, sub_sel_name)
    
    if session("corp_code") = "041605" then
        var_default_answer_text = "Satisfactory"
    else
        var_default_answer_text = "Not Applicable"
    end if

    'response.write sub_sel_input & "..." & sub_Sel_question & "..." & sub_sel_template & "..."

    if (sub_sel_template <> "" and isnull(sub_sel_template) = false and sub_sel_template <> "any" and sub_sel_template <> "0") and len(sub_sel_question) > 0 then
   
        objCommand.commandText = "SELECT weighting_type, weighting_scheme FROM " & APPLICATION("DBTABLE_SA_TEMP_INFO") & " AS template " & _
                                 "WHERE template.corp_code = '" & session("CORP_CODE") & "' " & _
                                 "  AND template.template_id = '" & sub_sel_template & "'"
        set objRs = objCommand.execute
        if not objRs.EOF then
            ' will only return 1 answer so no loop
            weighting_type = objRs("weighting_type")
            weighting_scheme = objRs("weighting_scheme")
        else
            weighting_type = "0"
            weighting_scheme = ""
        end if
        set objRs = nothing


        if weighting_type <> "1" then
            if var_sa_frm_srch_temp_answer_na = "on" or var_sa_frm_srch_temp_answer_na = "CHECKED" then
                var_sa_frm_srch_temp_answer_na = "CHECKED"
            else
                var_sa_frm_srch_temp_answer_na = ""
            end if

            if var_sa_frm_srch_temp_answer_mr = "on" or var_sa_frm_srch_temp_answer_mr = "CHECKED" then
                var_sa_frm_srch_temp_answer_mr = "CHECKED"
            else
                var_sa_frm_srch_temp_answer_mr = ""
            end if

            if var_sa_frm_srch_temp_answer_ar = "on" or var_sa_frm_srch_temp_answer_ar = "CHECKED" then
                var_sa_frm_srch_temp_answer_ar = "CHECKED"
            else
                var_sa_frm_srch_temp_answer_ar = ""
            end if

            if var_sa_frm_srch_temp_answer_nr = "on" or var_sa_frm_srch_temp_answer_nr = "CHECKED" then
                var_sa_frm_srch_temp_answer_nr = "CHECKED"
            else
                var_sa_frm_srch_temp_answer_nr = ""
            end if
            
            var_sa_frm_srch_temp_answer = ""

            
            Response.Write("<input type='checkbox' class='checkbox' name='" & sub_sel_name & "_na' " & var_sa_frm_srch_temp_answer_na & " /> " & var_default_answer_text & " " & _
                            "<input type='checkbox' class='checkbox' name='" & sub_sel_name & "_mr' " & var_sa_frm_srch_temp_answer_mr & " /> Meets Requirements " & _
                            "<input type='checkbox' class='checkbox' name='" & sub_sel_name & "_ar' " & var_sa_frm_srch_temp_answer_ar & " /> Attention Required " & _
                            "<input type='checkbox' class='checkbox' name='" & sub_sel_name & "_nr' " & var_sa_frm_srch_temp_answer_nr & " /> Does not meet requirements")  
        
        else
        
            objCommand.commandText = "SELECT * FROM Module_SA.dbo.sa_v2_template_weighting " & _
                                     "WHERE (corp_code = '" & session("CORP_CODE") & "' OR corp_code = '999999') " & _
                                     "  AND weight_reference = '" & weighting_scheme & "'"
            set objRs = objCommand.execute
            if not objRs.EOF then
                
                response.Write("<select class='select' name='" & sub_sel_name & "'>")
                
                response.Write("<option value='any'>Any Answer</option>")
                
                if sub_sel_input = "x" then
                    response.Write("<option value='x' selected>" & var_default_answer_text & "</option>")
                else
                    response.Write("<option value='x'>" & var_default_answer_text & "</option>")
                end if
                
                while not objRs.EOF
                    
                    if cstr(objRs("weighting")) = sub_sel_input then
                        response.Write("<option value='" & objRs("weighting") & "' class='" & objRs("weight_color") & "' selected>" & objRs("weight_title") & "</option>")
                    else
                        response.Write("<option value='" & objRs("weighting") & "' class='" & objRs("weight_color") & "'>" & objRs("weight_title") & "</option>")
                    end if
                    
                    objRs.movenext
                wend
                
                response.Write("</select>")
            
            end if
            set objRs = nothing
        end if

        response.Write("<input type='hidden' name='sa_frm_srch_temp_weight' value='" & weighting_type & "' />")

    else
        ' don't show anything if no question has been selected
        response.Write("<span class='info'><i>Please select a template version to filter by answer</i></span>")
    end if

end sub



function shortenOutput(original_output, output_length)

 	var_string = original_output
    if len(var_string) > output_length then
        var_string = left(var_string, output_length)
        split_point = InStrRev(var_string, " ") 
        
        if split_point > 0 then
            var_string = left(var_string, split_point-1) & ".."
        else
            var_string = left(original_output, output_length) & ".."
        end if
    end if
    
    shortenOutput = var_string

end function

%>