﻿<!-- #include file="../inc/auto_logout.inc" -->
<!-- #include file="../inc/dbconnections.inc" -->
<!-- #include file="saudit_search_functions.asp" -->
<%   
CALL startCONNECTIONS()
DIM objRs,objConn,objCommand, uid
Dim cmd

cmd = request("cmd")

template_id = request("temp_id")
selected_id = request("sel_id")
sub_sel_name = request("sel_name")
show_control = request("show_control")

select case cmd
    
    case "reload_search_template_versions"        

        call AutoGenListTemplatesSub(template_id, selected_id, sub_sel_name)

    case "reload_search_questions"

        call AutoGenListQuestions(selected_id, template_id, sub_sel_name)

    case "reload_search_answers"

        call AutoGenListAnswers(selected_id, "any", template_id, sub_sel_name)

    case "switch_search_template_selector"

        call AutoGenSwitchTemplateSelector(show_control, sub_sel_name)

    case else
        response.write "Unknown command"   
   
end select

Call   endConnections() 
    
%>