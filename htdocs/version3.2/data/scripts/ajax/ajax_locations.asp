﻿
<%


sub select_structure(var_user_ref, tier2_id, tier3_id, tier4_id, tier5_id, tier6_id, StructureType, var_mode, var_opt_1, var_opt_2, var_opt_3, var_opt_4, var_opt_5)
   'var_opt_1  = Module type
   'var_opt_2 = Template if
   'var_opt_3 = if Y then don't show disabled records
    
    'validation
    if tier2_id = "" or not isnumeric(tier2_id) then tier2_id = 0
    if tier3_id = "" or not isnumeric(tier3_id) then tier3_id = 0
    if tier4_id = "" or not isnumeric(tier4_id) then tier4_id = 0
    if tier5_id = "" or not isnumeric(tier5_id) then tier5_id = 0
    if tier6_id = "" or not isnumeric(tier6_id) then tier6_id = 0
    
    
    if (StructureType = "S" or StructureType = "S_MS") then ' search or  module search
        StructureTypeText = "Any"
        use_plurals = false  
  '  elseif StructureType =  "F" then ' file
  '      StructureTypeText = "All"
  '      use_plurals = true
   else
        StructureTypeText = trans("acc.selecta.var")
        use_plurals = false  
    end if        


   ' response.write "<div id='structure_intro'>Click <a href='#user_access' onclick='openStructure()'>here</a> to restrict this user to areas of your organisation</div>"
    'response.write "<div id='structure_block' style='display: none'>"
      
    if var_opt_1 = "LB" then
        var_tier2_found = false
        var_tier3_found= false
        var_tier4_found= false
        var_tier5_found= false
        var_tier6_found= false

        'this is tricky because it's only allowed to show the locations as allowed under the log book > template_v2_permissions table
        'grab the perms first
         objcommand.commandtext = "select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' "                           
         set  objRs_perm = objCommand.Execute  

         if objrs_perm.eof then
            'work normally
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  and (struct_deleted = 0 or struct_deleted is null)  "                           
             
         elseif objrs_perm("tier2_ref") = "-1" then
            'work_normally 
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  "                           
         else
            'filter
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  and tier2_ref in (select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "')"                           
         end if 
    
            response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
             'objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  "                           
              
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
               
                    response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                        "<option value='0'>" & StructureTypeText & " " 
                                        if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier2_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier2_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if
                   
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier2_id) Then 
                                response.write " selected     "
                                var_tier2_found = true
                            end if
                            response.write ">" & objrs("struct_name") & "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                end if
            response.write "</td></tr>"

            if tier2_id > 0 and var_tier2_found then 
                 objcommand.commandtext = "select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') "                           
                 set  objRs_perm = objCommand.Execute  

                 if objrs_perm.eof then
                    'work normally                                            
                    objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)   "   
                 elseif objrs_perm("tier3_ref") = "-1" then
                    'work_normally 
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                 else
                    'filter
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier3_ref in (select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref='-1' or tier2_ref = '" & tier2_id & "' ))"                           
                 end if 

               
                   ' objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  "                           
                    set  objRs = objCommand.Execute  

                    if not objRs.EOF then
                        response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                        response.write "<select class='select' name='frm_user_tier3' "
                        if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                            response.write ">" & _
                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier3_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier3_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if
                            while not objrs.eof
                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                If cint(objRs("struct_ref")) = cint(tier3_id) Then 
                                    response.write " selected     "
                                    var_tier3_found = true
                                end if
                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                objrs.movenext
                            wend
                        response.write "</select>"
                        response.write "</td></tr>"

                        if tier3_id > 0 and var_tier3_found then 
                               objcommand.commandtext = "select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') "                           
                                set  objRs_perm = objCommand.Execute  

                               

                                if objrs_perm.eof then
                                'work normally                                            
                                objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                elseif objrs_perm("tier4_ref") = "-1" then
                                'work_normally 
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                else
                                'filter
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  and (struct_deleted = 0 or struct_deleted is null)   and tier4_ref in (select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref='-1' or tier3_ref = '" & tier3_id & "' ))"                           
                                end if 
                                                               
                              '  objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  "                           
                                set  objRs = objCommand.Execute  

                                if not objRs.EOF then
                                    response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                    response.write "<select class='select' name='frm_user_tier4' "
                                    if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                    response.write " >" & _                                
                                        "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier4_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier4_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        while not objrs.eof
                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                            If cint(objRs("struct_ref")) = cint(tier4_id) Then
                                            var_tier4_found = true
                                            response.write " selected     "
                                            end if
                                            response.write ">" & objrs("struct_name") & "</option>"
                    
                                            objrs.movenext
                                        wend
                                    response.write "</select>"
                                    response.write "</td></tr>" 

                                    if tier4_id > 0 and var_tier4_found then    
                                    
                                      objcommand.commandtext = "select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and (tier4_ref = '-1' or tier4_ref = '" & tier4_id & "') "                           
                                     set  objRs_perm = objCommand.Execute  

                                     if objrs_perm.eof then
                                        'work normally                                            
                                        objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                     elseif objrs_perm("tier5_ref") = "-1" then
                                        'work_normally 
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                                     else
                                        'filter
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier5_ref in (select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ))"                           
                                     end if          

                                       ' objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  "                           
                                        set  objRs = objCommand.Execute  

                                        if not objRs.EOF then
                                            response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                            response.write "<select class='select' name='frm_user_tier5' "
                                            if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                                "<option value='0'>" & StructureTypeText & " "  
                                            if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                            response.write "</option>" 

                                             if StructureType =  "F" then
                                                response.write "<option value='-1' "
                                                if cint(tier5_id) = -1 then response.write " selected "
                                                response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                            if StructureType =  "S_MS" then
                                               response.write "<option value='-1' "
                                               if cint(tier5_id) = -1 then response.write " selected "
                                               response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                                while not objrs.eof
                                                    response.write  "<option value='" & objrs("struct_ref") & "' "
                                                    If cint(objRs("struct_ref")) = cint(tier5_id) Then 
                                                    var_tier5_found = true
                                                    response.write " selected     "
                                                    end if
                                                    response.write ">" & objrs("struct_name") & "</option>"
                    
                                                    objrs.movenext
                                                wend
                                            response.write "</select>"
                                            response.write "</td></tr>"

                                            if tier5_id > 0 and var_tier5_found  then 
            
                                                    objcommand.commandtext = "select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref = '-1' or tier5_ref = '" & tier5_id & "') "                           
                                                    set  objRs_perm = objCommand.Execute  
                                                   
                                                    if objrs_perm.eof then
                                                    'work normally                                            
                                                    objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                                    elseif objrs_perm("tier6_ref") = "-1" then
                                                    'work_normally 
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                                    else
                                                    'filter
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier6_ref in (select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref='-1' or tier5_ref = '" & tier5_id & "' ))"                           
                                                    end if 

                                                    'objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "'  "                           
                                                    set  objRs = objCommand.Execute  

                                                    if not objRs.EOF then
                                                        response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                        response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                    if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                    response.write "</option>"  

                                                                     if StructureType =  "F" then
                                                                        response.write "<option value='-1' "
                                                                        if cint(tier6_id) = -1 then response.write " selected "
                                                                        response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                                    if StructureType =  "S_MS" then
                                                                       response.write "<option value='-1' "
                                                                       if cint(tier6_id) = -1 then response.write " selected "
                                                                       response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                            while not objrs.eof
                                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                                If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                                                objrs.movenext
                                                            wend
                                                        response.write "</select>"
                                                        response.write "</td></tr>"
                                                    else
                                                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                    end if
                                            else
                                             response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                            end if
                                        else
                                           response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                           response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                        end if                                    

                                else           
                                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                                end if
                                               
                                else
                                    response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                end if

                            

                        else           
                            response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                        end if

                    else
                         response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                         response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                    end if

                

            else
                'we need this value when considering defaults for the time been until the permissions system is built
                    response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
            end if

    
           
        response.write "</table>"

    else
        var_disabled = false

        response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
         objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name, struct_deleted   from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  "                            
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier2_ref='" & tier2_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if                                                    
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
              
            set  objRs = objCommand.Execute  

            if not objRs.EOF then
               
                response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                    "<option value='0'>" & StructureTypeText & " " 
                                    if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier2_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier2_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                    end if
                   
                    while not objrs.eof
                        response.write  "<option value='" & objrs("struct_ref") & "' "
                        If cint(objRs("struct_ref")) = cint(tier2_id) Then response.write " selected     "
                        response.write ">" & objrs("struct_name") 
                        if  objrs("struct_deleted") then ' (StructureType =  "S" or StructureType =  "S_MS") and
                            response.write " (Disabled)"      
                            If cint(objRs("struct_ref")) = cint(tier2_id) Then var_disabled  = true
                        end if                 
                        response.write "</option>"
                    
                        objrs.movenext
                    wend
                response.write "</select>"
            end if
        response.write "</td></tr>"

        if tier2_id > 0 then 
                objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  "                                
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier3_ref='" & tier3_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                      
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
                    response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                    response.write "<select class='select' name='frm_user_tier3' "
                    if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                        response.write ">" & _
                                        "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier3_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier3_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    end if
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier3_id) Then response.write " selected     "
                            response.write ">" & objrs("struct_name") 
                            
                            if (objrs("struct_deleted") or var_disabled)  then ' (StructureType =  "S" or StructureType =  "S_MS") and 
                                response.write " (Disabled)"      
                                If cint(objRs("struct_ref")) = cint(tier3_id) Then var_disabled  = true
                            end if   
                            response.write "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                    response.write "</td></tr>"

                    if tier3_id > 0 then 
                            objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  "                            
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier4_ref='" & tier4_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
                            set  objRs = objCommand.Execute  

                            if not objRs.EOF then
                                response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                response.write "<select class='select' name='frm_user_tier4' "
                                if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                response.write " >" & _                                
                                    "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier4_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier4_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                    end if

                                    while not objrs.eof
                                        response.write  "<option value='" & objrs("struct_ref") & "' "
                                        If cint(objRs("struct_ref")) = cint(tier4_id) Then response.write " selected     "
                                        response.write ">" & objrs("struct_name") 
                            
                                        if  (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                            response.write " (Disabled)"      
                                            If cint(objRs("struct_ref")) = cint(tier4_id) Then var_disabled  = true
                                        end if   
                                        response.write "</option>"
                    
                                        objrs.movenext
                                    wend
                                response.write "</select>"
                                response.write "</td></tr>" 

                                if tier4_id > 0 then             

                                    objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  "                              
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier5_ref='" & tier5_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                        
                                    set  objRs = objCommand.Execute  

                                    if not objRs.EOF then
                                        response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                        response.write "<select class='select' name='frm_user_tier5' "
                                        if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier5_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier5_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                        end if

                                            while not objrs.eof
                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                If cint(objRs("struct_ref")) = cint(tier5_id) Then response.write " selected     "
                                                response.write ">" & objrs("struct_name") 
                            
                                                if (objrs("struct_deleted") or var_disabled)  then'  (StructureType =  "S" or StructureType =  "S_MS") and 
                                                    response.write " (Disabled)"      
                                                    If cint(objRs("struct_ref")) = cint(tier5_id) Then var_disabled  = true
                                                end if   
                                                response.write "</option>"
                    
                                                objrs.movenext
                                            wend
                                        response.write "</select>"
                                        response.write "</td></tr>"

                                        if tier5_id > 0 then 
            
                                                objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "'  "                           
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier6_ref='" & tier6_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"
                                                set  objRs = objCommand.Execute  

                                                if not objRs.EOF then
                                                    response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                    response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                response.write "</option>"  

                                                                 if StructureType =  "F" then
                                                                    response.write "<option value='-1' "
                                                                    if cint(tier6_id) = -1 then response.write " selected "
                                                                    response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                end if

                                                                if StructureType =  "S_MS" then
                                                                   response.write "<option value='-1' "
                                                                   if cint(tier6_id) = -1 then response.write " selected "
                                                                   response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                end if

                                                        while not objrs.eof
                                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                                            If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                            response.write ">" & objrs("struct_name") 
                            
                                                            if (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                                                response.write " (Disabled)"      
                                                                If cint(objRs("struct_ref")) = cint(tier6_id) Then var_disabled  = true
                                                            end if   
                                                            response.write "</option>"
                    
                                                            objrs.movenext
                                                        wend
                                                    response.write "</select>"
                                                    response.write "</td></tr>"
                                                else
                                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                end if
                                        else
                                         response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                        end if
                                    else
                                       response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                       response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                    end if                                    

                            else           
                               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                            end if
                                               
                            else
                                response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                            end if

                            

                    else           
                        response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                    end if

                else
                     response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                     response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                end if

                

        else
            'we need this value when considering defaults for the time been until the permissions system is built
                response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
        end if

    
           
    response.write "</table>"
    end if


               
end sub


sub select_structure2(var_user_ref, tier2_id, tier3_id, tier4_id, tier5_id, tier6_id, StructureType, var_mode, var_opt_1, var_opt_2, var_opt_3, var_opt_4, var_opt_5)
   'var_opt_1  = Module type
   'var_opt_2 = Template if
   'var_opt_3 = if Y then don't show disabled records
    
    'validation
    if tier2_id = "" or not isnumeric(tier2_id) then tier2_id = 0
    if tier3_id = "" or not isnumeric(tier3_id) then tier3_id = 0
    if tier4_id = "" or not isnumeric(tier4_id) then tier4_id = 0
    if tier5_id = "" or not isnumeric(tier5_id) then tier5_id = 0
    if tier6_id = "" or not isnumeric(tier6_id) then tier6_id = 0
    
    
    if StructureType =  "S" or StructureType =  "S_MS" then ' search or  module search
        StructureTypeText = "Any"
        use_plurals = false  
  '  elseif StructureType =  "F" then ' file
  '      StructureTypeText = "All"
  '      use_plurals = true
   else
        StructureTypeText = "Select a"
        use_plurals = false  
    end if        


   ' response.write "<div id='structure_intro'>Click <a href='#user_access' onclick='openStructure()'>here</a> to restrict this user to areas of your organisation</div>"
    'response.write "<div id='structure_block' style='display: none'>"
      
    if var_opt_1 = "LB" then
        var_tier2_found = false
        var_tier3_found= false
        var_tier4_found= false
        var_tier5_found= false
        var_tier6_found= false

        'this is tricky because it's only allowed to show the locations as allowed under the log book > template_v2_permissions table
        'grab the perms first
         objcommand.commandtext = "select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' "                           
         set  objRs_perm = objCommand.Execute  

         if objrs_perm.eof then
            'work normally
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  and (struct_deleted = 0 or struct_deleted is null)  "                           
             
         elseif objrs_perm("tier2_ref") = "-1" then
            'work_normally 
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  "                           
         else
            'filter
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  and tier2_ref in (select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "')"                           
         end if 
    
            response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
             'objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  "                           
              
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
               
                    response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                        "<option value='0'>" & StructureTypeText & " " 
                                        if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier2_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier2_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if
                   
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier2_id) Then 
                                response.write " selected     "
                                var_tier2_found = true
                            end if
                            response.write ">" & objrs("struct_name") & "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                end if
            response.write "</td></tr>"

            if tier2_id > 0 and var_tier2_found then 
                 objcommand.commandtext = "select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') "                           
                 set  objRs_perm = objCommand.Execute  

                 if objrs_perm.eof then
                    'work normally                                            
                    objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)   "   
                 elseif objrs_perm("tier3_ref") = "-1" then
                    'work_normally 
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                 else
                    'filter
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier3_ref in (select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref='-1' or tier2_ref = '" & tier2_id & "' ))"                           
                 end if 

               
                   ' objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  "                           
                    set  objRs = objCommand.Execute  

                    if not objRs.EOF then
                        response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                        response.write "<select class='select' name='frm_user_tier3' "
                        if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                            response.write ">" & _
                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier3_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier3_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if
                            while not objrs.eof
                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                If cint(objRs("struct_ref")) = cint(tier3_id) Then 
                                    response.write " selected     "
                                    var_tier3_found = true
                                end if
                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                objrs.movenext
                            wend
                        response.write "</select>"
                        response.write "</td></tr>"

                        if tier3_id > 0 and var_tier3_found then 
                               objcommand.commandtext = "select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') "                           
                                set  objRs_perm = objCommand.Execute  

                               

                                if objrs_perm.eof then
                                'work normally                                            
                                objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                elseif objrs_perm("tier4_ref") = "-1" then
                                'work_normally 
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                else
                                'filter
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  and (struct_deleted = 0 or struct_deleted is null)   and tier4_ref in (select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref='-1' or tier3_ref = '" & tier3_id & "' ))"                           
                                end if 
                                                               
                              '  objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  "                           
                                set  objRs = objCommand.Execute  

                                if not objRs.EOF then
                                    response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                    response.write "<select class='select' name='frm_user_tier4' "
                                    if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                    response.write " >" & _                                
                                        "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier4_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier4_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        while not objrs.eof
                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                            If cint(objRs("struct_ref")) = cint(tier4_id) Then
                                            var_tier4_found = true
                                            response.write " selected     "
                                            end if
                                            response.write ">" & objrs("struct_name") & "</option>"
                    
                                            objrs.movenext
                                        wend
                                    response.write "</select>"
                                    response.write "</td></tr>" 

                                    if tier4_id > 0 and var_tier4_found then    
                                    
                                      objcommand.commandtext = "select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and (tier4_ref = '-1' or tier4_ref = '" & tier4_id & "') "                           
                                     set  objRs_perm = objCommand.Execute  

                                     if objrs_perm.eof then
                                        'work normally                                            
                                        objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                     elseif objrs_perm("tier5_ref") = "-1" then
                                        'work_normally 
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                                     else
                                        'filter
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier5_ref in (select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ))"                           
                                     end if          

                                       ' objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  "                           
                                        set  objRs = objCommand.Execute  

                                        if not objRs.EOF then
                                            response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                            response.write "<select class='select' name='frm_user_tier5' "
                                            if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                                "<option value='0'>" & StructureTypeText & " "  
                                            if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                            response.write "</option>" 

                                             if StructureType =  "F" then
                                                response.write "<option value='-1' "
                                                if cint(tier5_id) = -1 then response.write " selected "
                                                response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                            if StructureType =  "S_MS" then
                                               response.write "<option value='-1' "
                                               if cint(tier5_id) = -1 then response.write " selected "
                                               response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                                while not objrs.eof
                                                    response.write  "<option value='" & objrs("struct_ref") & "' "
                                                    If cint(objRs("struct_ref")) = cint(tier5_id) Then 
                                                    var_tier5_found = true
                                                    response.write " selected     "
                                                    end if
                                                    response.write ">" & objrs("struct_name") & "</option>"
                    
                                                    objrs.movenext
                                                wend
                                            response.write "</select>"
                                            response.write "</td></tr>"

                                            if tier5_id > 0 and var_tier5_found  then 
            
                                                    objcommand.commandtext = "select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref = '-1' or tier5_ref = '" & tier5_id & "') "                           
                                                    set  objRs_perm = objCommand.Execute  
                                                   
                                                    if objrs_perm.eof then
                                                    'work normally                                            
                                                    objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                                    elseif objrs_perm("tier6_ref") = "-1" then
                                                    'work_normally 
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                                    else
                                                    'filter
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier6_ref in (select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref='-1' or tier5_ref = '" & tier5_id & "' ))"                           
                                                    end if 

                                                    'objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "'  "                           
                                                    set  objRs = objCommand.Execute  

                                                    if not objRs.EOF then
                                                        response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                        response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                    if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                    response.write "</option>"  

                                                                     if StructureType =  "F" then
                                                                        response.write "<option value='-1' "
                                                                        if cint(tier6_id) = -1 then response.write " selected "
                                                                        response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                                    if StructureType =  "S_MS" then
                                                                       response.write "<option value='-1' "
                                                                       if cint(tier6_id) = -1 then response.write " selected "
                                                                       response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                            while not objrs.eof
                                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                                If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                                                objrs.movenext
                                                            wend
                                                        response.write "</select>"
                                                        response.write "</td></tr>"
                                                    else
                                                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                    end if
                                            else
                                             response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                            end if
                                        else
                                           response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                           response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                        end if                                    

                                else           
                                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                                end if
                                               
                                else
                                    response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                end if

                            

                        else           
                            response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                        end if

                    else
                         response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                         response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                    end if

                

            else
                'we need this value when considering defaults for the time been until the permissions system is built
                    response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
            end if

    
           
        response.write "</table>"

    else
        var_disabled = false
        var_show_all_tier2s = false

        response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
         
        objcommand.commandtext = "select distinct tier2_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and arc_flag = 0 "     
        set objrs = objcommand.execute

        if objrs.eof then
            'no perms added for this user
            var_show_all_tier2s = true
        else
            'perms added - are any 0?
           objcommand.commandtext = "select distinct tier2_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and arc_flag = 0 " & _
                                    "and tier2_ref = '0'"
            set objrs = objcommand.execute
            if not objrs.eof then
                var_show_all_tier2s = true
            end if
        end if

         objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as struct_name, struct_deleted   from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2 " 
                                  if var_show_all_tier2s = false then                                 
         objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                    "on tier2.corp_code = perms.corp_code " & _
                                    "and tier2.tier2_ref = perms.tier2_ref " & _
                                    "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and perms.arc_flag = 0 "
                                   end if
         objcommand.commandtext = objcommand.commandtext & "where tier2.corp_code = '" & session("corp_code") &  "'  "                            
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier2.tier2_ref='" & tier2_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if                                                    
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
            
            set  objRs = objCommand.Execute  

            if not objRs.EOF then
               
                response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                    "<option value='0'>" & StructureTypeText & " " 
                                    if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier2_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier2_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                    end if
                   
                    while not objrs.eof
                        response.write  "<option value='" & objrs("struct_ref") & "' "
                        If cint(objRs("struct_ref")) = cint(tier2_id) Then response.write " selected     "
                        response.write ">" & objrs("struct_name") 
                        if  objrs("struct_deleted") then ' (StructureType =  "S" or StructureType =  "S_MS") and
                            response.write " (Disabled)"      
                            If cint(objRs("struct_ref")) = cint(tier2_id) Then var_disabled  = true
                        end if                 
                        response.write "</option>"
                    
                        objrs.movenext
                    wend
                response.write "</select>"
            end if
        response.write "</td></tr>"

        if tier2_id > 0 then 

             var_show_all_tier3s = false

            objcommand.commandtext = "select distinct tier3_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                        "and arc_flag = 0 "     
            set objrs = objcommand.execute

            if objrs.eof then
                'no perms added for this user
                var_show_all_tier3s = true
            else
                'perms added - are any 0?
               objcommand.commandtext = "select distinct tier3_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and arc_flag = 0 " & _
                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                        "and tier3_ref = '0'"
                set objrs = objcommand.execute
                if not objrs.eof then
                    var_show_all_tier3s = true
                end if
            end if

                objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3 "
                                                              
                                    if var_show_all_tier3s = false then                                 
                objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                        "on tier3.corp_code = perms.corp_code " & _
                                        "and (tier3.tier2_ref = perms.tier2_ref or perms.tier2_ref = '0')" & _
                                        "and tier3.tier3_ref = perms.tier3_ref " & _
                                        "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and perms.arc_flag = 0 "
                                   end if
                objcommand.commandtext = objcommand.commandtext & "where tier3.corp_code = '" & session("corp_code") &  "' and tier3.tier2_ref = '" & tier2_id & "'  "  
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier3.tier3_ref='" & tier3_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                      
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
                    response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                    response.write "<select class='select' name='frm_user_tier3' "
                    if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                        response.write ">" & _
                                        "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier3_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier3_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    end if
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier3_id) Then response.write " selected     "
                            response.write ">" & objrs("struct_name") 
                            
                            if (objrs("struct_deleted") or var_disabled)  then ' (StructureType =  "S" or StructureType =  "S_MS") and 
                                response.write " (Disabled)"      
                                If cint(objRs("struct_ref")) = cint(tier3_id) Then var_disabled  = true
                            end if   
                            response.write "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                    response.write "</td></tr>"

                    if tier3_id > 0 then 

                        var_show_all_tier4s = false
                        objcommand.commandtext = "select distinct tier4_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                    "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                    "and arc_flag = 0 "     
                        set objrs = objcommand.execute

                        if objrs.eof then
                            'no perms added for this user
                            var_show_all_tier4s = true
                        else
                            'perms added - are any 0?
                           objcommand.commandtext = "select distinct tier4_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and arc_flag = 0 " & _
                                                    "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                    "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                    "and tier4_ref = '0'"
                            set objrs = objcommand.execute
                            if not objrs.eof then
                                var_show_all_tier4s = true
                            end if
                        end if

                            objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  "
                                    if var_show_all_tier4s = false then                                 
                            objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                    "on tier4.corp_code = perms.corp_code " & _
                                                    "and (tier4.tier3_ref = perms.tier3_ref or perms.tier3_ref = '0')" & _
                                                    "and tier4.tier4_ref = perms.tier4_ref " & _
                                                    "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and perms.arc_flag = 0 "
                                   end if
                             objcommand.commandtext = objcommand.commandtext &  "where tier4.corp_code = '" & session("corp_code") &  "'  and tier4.tier3_ref = '" & tier3_id & "'  "                            
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier4.tier4_ref='" & tier4_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
                            set  objRs = objCommand.Execute  

                            if not objRs.EOF then
                                response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                response.write "<select class='select' name='frm_user_tier4' "
                                if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                response.write " >" & _                                
                                    "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                    response.write "</option>" 

                                     if StructureType =  "F" then
                                        response.write "<option value='-1' "
                                        if cint(tier4_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                    end if

                                    if StructureType =  "S_MS" then
                                       response.write "<option value='-1' "
                                       if cint(tier4_id) = -1 then response.write " selected "
                                       response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                    end if

                                    while not objrs.eof
                                        response.write  "<option value='" & objrs("struct_ref") & "' "
                                        If cint(objRs("struct_ref")) = cint(tier4_id) Then response.write " selected     "
                                        response.write ">" & objrs("struct_name") 
                            
                                        if  (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                            response.write " (Disabled)"      
                                            If cint(objRs("struct_ref")) = cint(tier4_id) Then var_disabled  = true
                                        end if   
                                        response.write "</option>"
                    
                                        objrs.movenext
                                    wend
                                response.write "</select>"
                                response.write "</td></tr>" 

                                if tier4_id > 0 then   
                                
                                    var_show_all_tier5s = false
                                    objcommand.commandtext = "select distinct tier5_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                "and arc_flag = 0 "     
                                    set objrs = objcommand.execute

                                    if objrs.eof then
                                        'no perms added for this user
                                        var_show_all_tier5s = true
                                    else
                                        'perms added - are any 0?
                                       objcommand.commandtext = "select distinct tier5_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and arc_flag = 0 " & _
                                                                "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                "and tier5_ref = '0'"
                                        set objrs = objcommand.execute
                                        if not objrs.eof then
                                            var_show_all_tier5s = true
                                        end if
                                    end if
          

                                    objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  "
                                    if var_show_all_tier5s = false then                                 
                                        objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                                "on tier5.corp_code = perms.corp_code " & _
                                                                "and (tier5.tier4_ref = perms.tier4_ref or perms.tier4_ref = '0')" & _
                                                                "and tier5.tier5_ref = perms.tier5_ref " & _
                                                                "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and perms.arc_flag = 0 "
                                   end if
                                   objcommand.commandtext = objcommand.commandtext &  "where tier5.corp_code = '" & session("corp_code") &  "' and tier5.tier4_ref = '" & tier4_id & "'  "                              
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier5.tier5_ref='" & tier5_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                        
                                    set  objRs = objCommand.Execute  

                                    if not objRs.EOF then
                                        response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                        response.write "<select class='select' name='frm_user_tier5' "
                                        if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier5_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier5_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                        end if

                                            while not objrs.eof
                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                If cint(objRs("struct_ref")) = cint(tier5_id) Then response.write " selected     "
                                                response.write ">" & objrs("struct_name") 
                            
                                                if (objrs("struct_deleted") or var_disabled)  then'  (StructureType =  "S" or StructureType =  "S_MS") and 
                                                    response.write " (Disabled)"      
                                                    If cint(objRs("struct_ref")) = cint(tier5_id) Then var_disabled  = true
                                                end if   
                                                response.write "</option>"
                    
                                                objrs.movenext
                                            wend
                                        response.write "</select>"
                                        response.write "</td></tr>"

                                        if tier5_id > 0 then 

                                            var_show_all_tier6s = false
                                            objcommand.commandtext = "select distinct tier6_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                        "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                        "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                        "and (tier5_ref = '" & tier5_id & "' or tier5_ref='0') " & _
                                                                        "and arc_flag = 0 "     
                                            set objrs = objcommand.execute

                                            if objrs.eof then
                                                'no perms added for this user
                                                var_show_all_tier6s = true
                                            else
                                                'perms added - are any 0?
                                               objcommand.commandtext = "select distinct tier6_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                        "and arc_flag = 0 " & _
                                                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                        "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                        "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                        "and (tier5_ref = '" & tier5_id & "' or tier5_ref='0') " & _
                                                                        "and tier6_ref = '0'"
                                                set objrs = objcommand.execute
                                                if not objrs.eof then
                                                    var_show_all_tier6s = true
                                                end if
                                            end if
            
                                                objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6 "
                                                 if var_show_all_tier5s = false then                                 
                                        objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                                "on tier6.corp_code = perms.corp_code " & _
                                                                "and (tier6.tier5_ref = perms.tier5_ref or perms.tier5_ref = '0')" & _
                                                                "and tier6.tier6_ref = perms.tier6_ref " & _
                                                                "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and perms.arc_flag = 0 "
                                   end if
                                                objcommand.commandtext = objcommand.commandtext & " where tier6.corp_code = '" & session("corp_code") &  "' and tier6.tier5_ref = '" & tier5_id & "'  "                           
                                                if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier6.tier6_ref='" & tier6_id & "')"
                                                elseif var_opt_3 = "Y" then
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"
                                               
                                                set  objRs = objCommand.Execute  

                                                if not objRs.EOF then
                                                    response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                    response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                response.write "</option>"  

                                                                 if StructureType =  "F" then
                                                                    response.write "<option value='-1' "
                                                                    if cint(tier6_id) = -1 then response.write " selected "
                                                                    response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                end if

                                                                if StructureType =  "S_MS" then
                                                                   response.write "<option value='-1' "
                                                                   if cint(tier6_id) = -1 then response.write " selected "
                                                                   response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                end if

                                                        while not objrs.eof
                                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                                            If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                            response.write ">" & objrs("struct_name") 
                            
                                                            if (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                                                response.write " (Disabled)"      
                                                                If cint(objRs("struct_ref")) = cint(tier6_id) Then var_disabled  = true
                                                            end if   
                                                            response.write "</option>"
                    
                                                            objrs.movenext
                                                        wend
                                                    response.write "</select>"
                                                    response.write "</td></tr>"
                                                else
                                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                end if
                                        else
                                         response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                        end if
                                    else
                                       response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                       response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                    end if                                    

                            else           
                               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                            end if
                                               
                            else
                                response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                            end if

                            

                    else           
                        response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                    end if

                else
                     response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                     response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                end if

                

        else
            'we need this value when considering defaults for the time been until the permissions system is built
                response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
        end if

    
           
    response.write "</table>"
    end if


               
end sub



sub select_structure_create(var_user_ref, tier2_id, tier3_id, tier4_id, tier5_id, tier6_id, StructureType, var_mode, var_opt_1, var_opt_2, var_opt_3, var_opt_4, var_opt_5)
   'var_opt_1  = Module type
   'var_opt_2 = Template if
   'var_opt_3 = if Y then don't show disabled records
    
    'validation
    if tier2_id = "" or not isnumeric(tier2_id) then tier2_id = 0
    if tier3_id = "" or not isnumeric(tier3_id) then tier3_id = 0
    if tier4_id = "" or not isnumeric(tier4_id) then tier4_id = 0
    if tier5_id = "" or not isnumeric(tier5_id) then tier5_id = 0
    if tier6_id = "" or not isnumeric(tier6_id) then tier6_id = 0
    
    
   ' if StructureType =  "S" or StructureType =  "S_MS" then ' search or  module search
    '    StructureTypeText = "Any"
     '   use_plurals = false  
  '  elseif StructureType =  "F" then ' file
  '      StructureTypeText = "All"
  '      use_plurals = true
  ' else
        StructureTypeText = "Select a"
        use_plurals = false  
   ' end if        

         
    if var_opt_1 = "LB" then
        var_tier2_found = false
        var_tier3_found= false
        var_tier4_found= false
        var_tier5_found= false
        var_tier6_found= false

        'this is tricky because it's only allowed to show the locations as allowed under the log book > template_v2_permissions table
        'grab the perms first
         objcommand.commandtext = "select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' "                           
         set  objRs_perm = objCommand.Execute  

         if objrs_perm.eof then
            'work normally
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  and (struct_deleted = 0 or struct_deleted is null)  "                           
             
         elseif objrs_perm("tier2_ref") = "-1" then
            'work_normally 
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  "                           
         else
            'filter
            objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "' and (struct_deleted = 0 or struct_deleted is null)  and tier2_ref in (select distinct tier2_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "')"                           
         end if 
    
            response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
             'objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2  where corp_code = '" & session("corp_code") &  "'  "                           
              
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
               
                    response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                        "<option value='0'>" & StructureTypeText & " " 
                                        if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier2_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier2_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                        end if
                   
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier2_id) Then 
                                response.write " selected     "
                                var_tier2_found = true
                            end if
                            response.write ">" & objrs("struct_name") & "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                end if
            response.write "</td></tr>"

            if tier2_id > 0 and var_tier2_found then 
                 objcommand.commandtext = "select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') "                           
                 set  objRs_perm = objCommand.Execute  

                 if objrs_perm.eof then
                    'work normally                                            
                    objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)   "   
                 elseif objrs_perm("tier3_ref") = "-1" then
                    'work_normally 
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                 else
                    'filter
                     objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier3_ref in (select distinct tier3_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref='-1' or tier2_ref = '" & tier2_id & "' ))"                           
                 end if 

               
                   ' objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3  where corp_code = '" & session("corp_code") &  "' and tier2_ref = '" & tier2_id & "'  "                           
                    set  objRs = objCommand.Execute  

                    if not objRs.EOF then
                        response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                        response.write "<select class='select' name='frm_user_tier3' "
                        if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                            response.write ">" & _
                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier3_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier3_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                        end if
                            while not objrs.eof
                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                If cint(objRs("struct_ref")) = cint(tier3_id) Then 
                                    response.write " selected     "
                                    var_tier3_found = true
                                end if
                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                objrs.movenext
                            wend
                        response.write "</select>"
                        response.write "</td></tr>"

                        if tier3_id > 0 and var_tier3_found then 
                               objcommand.commandtext = "select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') "                           
                                set  objRs_perm = objCommand.Execute  

                               

                                if objrs_perm.eof then
                                'work normally                                            
                                objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                elseif objrs_perm("tier4_ref") = "-1" then
                                'work_normally 
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "' and tier3_ref = '" & tier3_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                else
                                'filter
                                    objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  and (struct_deleted = 0 or struct_deleted is null)   and tier4_ref in (select distinct tier4_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref='-1' or tier3_ref = '" & tier3_id & "' ))"                           
                                end if 
                                                               
                              '  objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  where corp_code = '" & session("corp_code") &  "'  and tier3_ref = '" & tier3_id & "'  "                           
                                set  objRs = objCommand.Execute  

                                if not objRs.EOF then
                                    response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                    response.write "<select class='select' name='frm_user_tier4' "
                                    if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                    response.write " >" & _                                
                                        "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                        response.write "</option>" 

                                         if StructureType =  "F" then
                                            response.write "<option value='-1' "
                                            if cint(tier4_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        if StructureType =  "S_MS" then
                                           response.write "<option value='-1' "
                                           if cint(tier4_id) = -1 then response.write " selected "
                                           response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                        end if

                                        while not objrs.eof
                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                            If cint(objRs("struct_ref")) = cint(tier4_id) Then
                                            var_tier4_found = true
                                            response.write " selected     "
                                            end if
                                            response.write ">" & objrs("struct_name") & "</option>"
                    
                                            objrs.movenext
                                        wend
                                    response.write "</select>"
                                    response.write "</td></tr>" 

                                    if tier4_id > 0 and var_tier4_found then    
                                    
                                      objcommand.commandtext = "select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "'  and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and (tier4_ref = '-1' or tier4_ref = '" & tier4_id & "') "                           
                                     set  objRs_perm = objCommand.Execute  

                                     if objrs_perm.eof then
                                        'work normally                                            
                                        objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                     elseif objrs_perm("tier5_ref") = "-1" then
                                        'work_normally 
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  and (struct_deleted = 0 or struct_deleted is null)  "   
                                         
                                     else
                                        'filter
                                         objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier5_ref in (select distinct tier5_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ))"                           
                                     end if          

                                       ' objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  where corp_code = '" & session("corp_code") &  "' and tier4_ref = '" & tier4_id & "'  "                           
                                        set  objRs = objCommand.Execute  

                                        if not objRs.EOF then
                                            response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                            response.write "<select class='select' name='frm_user_tier5' "
                                            if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                                "<option value='0'>" & StructureTypeText & " "  
                                            if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                            response.write "</option>" 

                                             if StructureType =  "F" then
                                                response.write "<option value='-1' "
                                                if cint(tier5_id) = -1 then response.write " selected "
                                                response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                            if StructureType =  "S_MS" then
                                               response.write "<option value='-1' "
                                               if cint(tier5_id) = -1 then response.write " selected "
                                               response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                            end if

                                                while not objrs.eof
                                                    response.write  "<option value='" & objrs("struct_ref") & "' "
                                                    If cint(objRs("struct_ref")) = cint(tier5_id) Then 
                                                    var_tier5_found = true
                                                    response.write " selected     "
                                                    end if
                                                    response.write ">" & objrs("struct_name") & "</option>"
                    
                                                    objrs.movenext
                                                wend
                                            response.write "</select>"
                                            response.write "</td></tr>"

                                            if tier5_id > 0 and var_tier5_found  then 
            
                                                    objcommand.commandtext = "select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref = '-1' or tier5_ref = '" & tier5_id & "') "                           
                                                    set  objRs_perm = objCommand.Execute  
                                                   
                                                    if objrs_perm.eof then
                                                    'work normally                                            
                                                    objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                                    elseif objrs_perm("tier6_ref") = "-1" then
                                                    'work_normally 
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   "   
                                         
                                                    else
                                                    'filter
                                                        objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "' and (struct_deleted = 0 or struct_deleted is null)   and tier6_ref in (select distinct tier6_ref from  " &  Application("DBTABLE_LB_TEMPLATE_LOCS") & "  where corp_code = '" & session("corp_code") &  "' and template_ref = '" & var_opt_2 & "' and (tier2_ref = '-1' or tier2_ref = '" & tier2_id & "') and (tier3_ref = '-1' or tier3_ref = '" & tier3_id & "') and  (tier4_ref='-1' or tier4_ref = '" & tier4_id & "' ) and (tier5_ref='-1' or tier5_ref = '" & tier5_id & "' ))"                           
                                                    end if 

                                                    'objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as  struct_name from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6  where corp_code = '" & session("corp_code") &  "' and tier5_ref = '" & tier5_id & "'  "                           
                                                    set  objRs = objCommand.Execute  

                                                    if not objRs.EOF then
                                                        response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                        response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                    if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                    response.write "</option>"  

                                                                     if StructureType =  "F" then
                                                                        response.write "<option value='-1' "
                                                                        if cint(tier6_id) = -1 then response.write " selected "
                                                                        response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                                    if StructureType =  "S_MS" then
                                                                       response.write "<option value='-1' "
                                                                       if cint(tier6_id) = -1 then response.write " selected "
                                                                       response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                    end if

                                                            while not objrs.eof
                                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                                If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                                response.write ">" & objrs("struct_name") & "</option>"
                    
                                                                objrs.movenext
                                                            wend
                                                        response.write "</select>"
                                                        response.write "</td></tr>"
                                                    else
                                                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                    end if
                                            else
                                             response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                            end if
                                        else
                                           response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                           response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                        end if                                    

                                else           
                                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                                end if
                                               
                                else
                                    response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                end if

                            

                        else           
                            response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                            response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                        end if

                    else
                         response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                         response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                         response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                    end if

                

            else
                'we need this value when considering defaults for the time been until the permissions system is built
                    response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                   response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
            end if

    
           
        response.write "</table>"

    else
        var_disabled = false
        var_show_all_tier2s = false

        response.write "<table><tr><th width='150px'>" & session("CORP_TIER2") & "</th><td>"
         
        objcommand.commandtext = "select distinct tier2_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and (permission = 'W' or permission = 'X') " & _
                                    "and arc_flag = 0 "     
        set objrs = objcommand.execute

        if objrs.eof then
            'no perms added for this user
            var_show_all_tier2s = true
        else
            'perms added - are any 0?
           objcommand.commandtext = "select distinct tier2_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and arc_flag = 0 " & _
                                    "and (permission = 'W' or permission = 'X') " & _
                                    "and tier2_ref = '0'"
            set objrs = objcommand.execute
            if not objrs.eof then
                var_show_all_tier2s = true
            end if
        end if

         objcommand.commandtext = "select tier2.tier2_ref as struct_ref, tier2.struct_name as struct_name, struct_deleted   from  " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2 " 
                                  if var_show_all_tier2s = false then                                 
         objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                    "on tier2.corp_code = perms.corp_code " & _
                                    "and tier2.tier2_ref = perms.tier2_ref " & _
                                    "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                    "and (perms.permission = 'W' or perms.permission = 'X') " & _
                                    "and perms.arc_flag = 0 "
                                   end if
         objcommand.commandtext = objcommand.commandtext & "where tier2.corp_code = '" & session("corp_code") &  "'  "                            
                                             '   if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier2.tier2_ref='" & tier2_id & "')"
                                              '  elseif var_opt_3 = "Y" then
                                               '     objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                                'end if                                                    
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
            
            set  objRs = objCommand.Execute  

            if not objRs.EOF then
               
                response.write "<select class='select' name='frm_user_tier2' onchange='reloadStructure(""" & var_user_ref & """, this.value, ""0"", ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >"  & _
                                    "<option value='0'>" & StructureTypeText & " " 
                                    if use_plurals then response.write session("CORP_TIER2_PLURAL") else response.write session("CORP_TIER2")  
                                    response.write "</option>" 

                                     if StructureType =  "F" and var_show_all_tier2s then
                                        response.write "<option value='-1' "
                                        if cint(tier2_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                    end if

                                    'if StructureType =  "S_MS" then
                                     '  response.write "<option value='-1' "
                                     '  if cint(tier2_id) = -1 then response.write " selected "
                                      ' response.write " >All " & session("CORP_TIER2_PLURAL") & "</option>" 
                                   ' end if
                   
                    while not objrs.eof
                        response.write  "<option value='" & objrs("struct_ref") & "' "
                        If cint(objRs("struct_ref")) = cint(tier2_id) Then response.write " selected     "
                        response.write ">" & objrs("struct_name") 
                       ' if  objrs("struct_deleted") then ' (StructureType =  "S" or StructureType =  "S_MS") and
                        '    response.write " (Disabled)"      
                         '   If cint(objRs("struct_ref")) = cint(tier2_id) Then var_disabled  = true
                        'end if                 
                        response.write "</option>"
                    
                        objrs.movenext
                    wend
                response.write "</select>"
            end if
        response.write "</td></tr>"

        if tier2_id > 0 then 

             var_show_all_tier3s = false

            objcommand.commandtext = "select distinct tier3_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                        "and (permission = 'W' or permission = 'X') " & _
                                        "and arc_flag = 0 "     
            set objrs = objcommand.execute

            if objrs.eof then
                'no perms added for this user
                var_show_all_tier3s = true
            else
                'perms added - are any 0?
               objcommand.commandtext = "select distinct tier3_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and arc_flag = 0 " & _
                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                        "and (permission = 'W' or permission = 'X') " & _
                                        "and tier3_ref = '0'"
                set objrs = objcommand.execute
                if not objrs.eof then
                    var_show_all_tier3s = true
                end if
            end if

                objcommand.commandtext = "select tier3.tier3_ref as struct_ref, tier3.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3 "
                                                              
                                    if var_show_all_tier3s = false then                                 
                objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                        "on tier3.corp_code = perms.corp_code " & _
                                        "and (tier3.tier2_ref = perms.tier2_ref or perms.tier2_ref = '0')" & _
                                        "and tier3.tier3_ref = perms.tier3_ref " & _
                                        "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                        "and (perms.permission = 'W' or perms.permission = 'X') " & _
                                        "and perms.arc_flag = 0 "
                                   end if
                objcommand.commandtext = objcommand.commandtext & "where tier3.corp_code = '" & session("corp_code") &  "' and tier3.tier2_ref = '" & tier2_id & "'  "  
                                          '      if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier3.tier3_ref='" & tier3_id & "')"
                                           '     elseif var_opt_3 = "Y" then
                                            '        objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                             '   end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                      
                set  objRs = objCommand.Execute  

                if not objRs.EOF then
                    response.write "<tr><th width='150px'>" & session("CORP_TIER3") & "</th><td>"
                    response.write "<select class='select' name='frm_user_tier3' "
                    if session("structure_levels") > 3 then response.write "  onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """, this.value, ""0"", ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """  )' " & var_mode & " "
                        response.write ">" & _
                                        "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER3_PLURAL") else response.write session("CORP_TIER3")  
                                    response.write "</option>" 

                                     if StructureType =  "F" and var_show_all_tier3s then
                                        response.write "<option value='-1' "
                                        if cint(tier3_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    end if

                                   ' if StructureType =  "S_MS" then
                                   '    response.write "<option value='-1' "
                                   '    if cint(tier3_id) = -1 then response.write " selected "
                                   '    response.write " >All " & session("CORP_TIER3_PLURAL") & "</option>" 
                                    'end if
                        while not objrs.eof
                            response.write  "<option value='" & objrs("struct_ref") & "' "
                            If cint(objRs("struct_ref")) = cint(tier3_id) Then response.write " selected     "
                            response.write ">" & objrs("struct_name") 
                            
                       '     if (objrs("struct_deleted") or var_disabled)  then ' (StructureType =  "S" or StructureType =  "S_MS") and 
                        '        response.write " (Disabled)"      
                         '       If cint(objRs("struct_ref")) = cint(tier3_id) Then var_disabled  = true
                          '  end if   
                            response.write "</option>"
                    
                            objrs.movenext
                        wend
                    response.write "</select>"
                    response.write "</td></tr>"

                    if tier3_id > 0 then 

                        var_show_all_tier4s = false
                        objcommand.commandtext = "select distinct tier4_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                    "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                    "and (permission = 'W' or permission = 'X') " & _
                                                    "and arc_flag = 0 "     
                        set objrs = objcommand.execute

                        if objrs.eof then
                            'no perms added for this user
                            var_show_all_tier4s = true
                        else
                            'perms added - are any 0?
                           objcommand.commandtext = "select distinct tier4_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                    "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                    "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and arc_flag = 0 " & _
                                                    "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                    "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                    "and (permission = 'W' or permission = 'X') " & _
                                                    "and tier4_ref = '0'"
                            set objrs = objcommand.execute
                            if not objrs.eof then
                                var_show_all_tier4s = true
                            end if
                        end if

                            objcommand.commandtext = "select tier4.tier4_ref as struct_ref, tier4.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4  "
                                    if var_show_all_tier4s = false then                                 
                            objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                    "on tier4.corp_code = perms.corp_code " & _
                                                    "and (tier4.tier3_ref = perms.tier3_ref or perms.tier3_ref = '0')" & _
                                                    "and tier4.tier4_ref = perms.tier4_ref " & _
                                                    "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                    "and (perms.permission = 'W' or perms.permission = 'X') " & _
                                                    "and perms.arc_flag = 0 "
                                   end if
                             objcommand.commandtext = objcommand.commandtext &  "where tier4.corp_code = '" & session("corp_code") &  "'  and tier4.tier3_ref = '" & tier3_id & "'  "                            
                                  '              if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier4.tier4_ref='" & tier4_id & "')"
                                   '             elseif var_opt_3 = "Y" then
                                    '                objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                     '           end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                          
                            set  objRs = objCommand.Execute  

                            if not objRs.EOF then
                                response.write "<tr><th width='150px'>" & session("CORP_TIER4") & "</th><td>"
                                response.write "<select class='select' name='frm_user_tier4' "
                                if session("structure_levels") > 4 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """ , this.value, ""0"", ""0"",""" & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )'  " & var_mode & " " 
                                response.write " >" & _                                
                                    "<option value='0'>" & StructureTypeText & " "  
                                    if use_plurals then response.write session("CORP_TIER4_PLURAL") else response.write session("CORP_TIER4")  
                                    response.write "</option>" 

                                     if StructureType =  "F"  and var_show_all_tier4s then
                                        response.write "<option value='-1' "
                                        if cint(tier4_id) = -1 then response.write " selected "
                                        response.write ">All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                    end if

                                '    if StructureType =  "S_MS" then
                                '       response.write "<option value='-1' "
                                '       if cint(tier4_id) = -1 then response.write " selected "
                                '       response.write " >All " & session("CORP_TIER4_PLURAL") & "</option>" 
                                '    end if

                                    while not objrs.eof
                                        response.write  "<option value='" & objrs("struct_ref") & "' "
                                        If cint(objRs("struct_ref")) = cint(tier4_id) Then response.write " selected     "
                                        response.write ">" & objrs("struct_name") 
                            
                                 '       if  (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                 '           response.write " (Disabled)"      
                                 '           If cint(objRs("struct_ref")) = cint(tier4_id) Then var_disabled  = true
                                 '       end if   
                                        response.write "</option>"
                    
                                        objrs.movenext
                                    wend
                                response.write "</select>"
                                response.write "</td></tr>" 

                                if tier4_id > 0 then   
                                
                                    var_show_all_tier5s = false
                                    objcommand.commandtext = "select distinct tier5_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                "and (permission = 'W' or permission = 'X') " & _
                                                                "and arc_flag = 0 "     
                                    set objrs = objcommand.execute

                                    if objrs.eof then
                                        'no perms added for this user
                                        var_show_all_tier5s = true
                                    else
                                        'perms added - are any 0?
                                       objcommand.commandtext = "select distinct tier5_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and arc_flag = 0 " & _
                                                                "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                "and (permission = 'W' or permission = 'X') " & _
                                                                "and tier5_ref = '0'"
                                        set objrs = objcommand.execute
                                        if not objrs.eof then
                                            var_show_all_tier5s = true
                                        end if
                                    end if
          

                                    objcommand.commandtext = "select tier5.tier5_ref as struct_ref, tier5.struct_name as  struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5  "
                                    if var_show_all_tier5s = false then                                 
                                        objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                                "on tier5.corp_code = perms.corp_code " & _
                                                                "and (tier5.tier4_ref = perms.tier4_ref or perms.tier4_ref = '0')" & _
                                                                "and tier5.tier5_ref = perms.tier5_ref " & _
                                                                "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and (perms.permission = 'W' or perms.permission = 'X') " & _
                                                                "and perms.arc_flag = 0 "
                                   end if
                                   objcommand.commandtext = objcommand.commandtext &  "where tier5.corp_code = '" & session("corp_code") &  "' and tier5.tier4_ref = '" & tier4_id & "'  "                              
                                             '   if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier5.tier5_ref='" & tier5_id & "')"
                                             '   elseif var_opt_3 = "Y" then
                                              '      objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                               ' end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"                        
                                    set  objRs = objCommand.Execute  

                                    if not objRs.EOF then
                                        response.write "<tr><th width='150px'>" & session("CORP_TIER5") & "</th><td>"
                                        response.write "<select class='select' name='frm_user_tier5' "
                                        if session("structure_levels") > 5 then response.write " onchange='reloadStructure(""" & var_user_ref & """, """ & tier2_id & """,  """ & tier3_id & """,  """ & tier4_id & """ , this.value, ""0"", """ & StructureType & """ ,""" & var_mode & """, """ & var_opt_1 & """, """ & var_opt_2 & """, """ & var_opt_3 & """, """ & var_opt_4 & """, """ & var_opt_5 & """ )' " & var_mode & " >" & _
                                                            "<option value='0'>" & StructureTypeText & " "  
                                        if use_plurals then response.write session("CORP_TIER5_PLURAL") else response.write session("CORP_TIER5")  
                                        response.write "</option>" 

                                         if StructureType =  "F" and var_show_all_tier5s then
                                            response.write "<option value='-1' "
                                            if cint(tier5_id) = -1 then response.write " selected "
                                            response.write ">All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                        end if

                                  '      if StructureType =  "S_MS" then
                                  '         response.write "<option value='-1' "
                                  '         if cint(tier5_id) = -1 then response.write " selected "
                                  '         response.write " >All " & session("CORP_TIER5_PLURAL") & "</option>" 
                                  '      end if

                                            while not objrs.eof
                                                response.write  "<option value='" & objrs("struct_ref") & "' "
                                                If cint(objRs("struct_ref")) = cint(tier5_id) Then response.write " selected     "
                                                response.write ">" & objrs("struct_name") 
                            
                                    '            if (objrs("struct_deleted") or var_disabled)  then'  (StructureType =  "S" or StructureType =  "S_MS") and 
                                    '                response.write " (Disabled)"      
                                    '                If cint(objRs("struct_ref")) = cint(tier5_id) Then var_disabled  = true
                                    '            end if   
                                                response.write "</option>"
                    
                                                objrs.movenext
                                            wend
                                        response.write "</select>"
                                        response.write "</td></tr>"

                                        if tier5_id > 0 then 

                                            var_show_all_tier6s = false
                                            objcommand.commandtext = "select distinct tier6_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                        "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                        "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                        "and (tier5_ref = '" & tier5_id & "' or tier5_ref='0') " & _
                                                                        "and (permission = 'W' or permission = 'X') " & _
                                                                        "and arc_flag = 0 "     
                                            set objrs = objcommand.execute

                                            if objrs.eof then
                                                'no perms added for this user
                                                var_show_all_tier6s = true
                                            else
                                                'perms added - are any 0?
                                               objcommand.commandtext = "select distinct tier6_ref from "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " " & _
                                                                        "where corp_code = '" & session("corp_code") & "' " & _                                   
                                                                        "and acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                        "and arc_flag = 0 " & _
                                                                        "and (tier2_ref = '" & tier2_id & "' or tier2_ref='0') " & _
                                                                        "and (tier3_ref = '" & tier3_id & "' or tier3_ref='0') " & _
                                                                        "and (tier4_ref = '" & tier4_id & "' or tier4_ref='0') " & _
                                                                        "and (tier5_ref = '" & tier5_id & "' or tier5_ref='0') " & _
                                                                        "and (permission = 'W' or permission = 'X') " & _
                                                                        "and tier6_ref = '0'"
                                                set objrs = objcommand.execute
                                                if not objrs.eof then
                                                    var_show_all_tier6s = true
                                                end if
                                            end if
            
                                                objcommand.commandtext = "select tier6.tier6_ref as struct_ref, tier6.struct_name as struct_name, struct_deleted from  " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6 "
                                                 if var_show_all_tier5s = false then                                 
                                        objcommand.commandtext = objcommand.commandtext & "inner join "  &  Application("DBTABLE_HR_USER_LOCATIONS") & " as perms " & _
                                                                "on tier6.corp_code = perms.corp_code " & _
                                                                "and (tier6.tier5_ref = perms.tier5_ref or perms.tier5_ref = '0')" & _
                                                                "and tier6.tier6_ref = perms.tier6_ref " & _
                                                                "and perms.acc_ref = '" & session("YOUR_ACCOUNT_ID") & "' " & _
                                                                "and (perms.permission = 'W' or perms.permission = 'X') " & _
                                                                "and perms.arc_flag = 0 "
                                   end if
                                                objcommand.commandtext = objcommand.commandtext & " where tier6.corp_code = '" & session("corp_code") &  "' and tier6.tier5_ref = '" & tier5_id & "'  "                           
                                        '        if (StructureType <>  "S" and StructureType <> "S_MS") then 
                                                    objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null or tier6.tier6_ref='" & tier6_id & "')"
                                         '       elseif var_opt_3 = "Y" then
                                          '          objcommand.commandtext = objcommand.commandtext & " and (struct_deleted = 0 or struct_deleted is null)"
                                           '     end if               
                                                objcommand.commandtext = objcommand.commandtext & " order by struct_name"
                                               
                                                set  objRs = objCommand.Execute  

                                                if not objRs.EOF then
                                                    response.write "<tr><th width='150px'>" & session("CORP_TIER6") & "</th><td>"
                                                    response.write "<select class='select' name='frm_user_tier6'  " & var_mode & " ><option value='0'>" & StructureTypeText & " "  
                                                                if use_plurals then response.write session("CORP_TIER6_PLURAL") else response.write session("CORP_TIER6")  
                                                                response.write "</option>"  

                                                                 if StructureType =  "F" and var_show_all_tier6s then
                                                                    response.write "<option value='-1' "
                                                                    if cint(tier6_id) = -1 then response.write " selected "
                                                                    response.write ">All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                                end if

                                                    '            if StructureType =  "S_MS" then
                                                     '              response.write "<option value='-1' "
                                                      '             if cint(tier6_id) = -1 then response.write " selected "
                                                       '            response.write " >All " & session("CORP_TIER6_PLURAL") & "</option>" 
                                                        '        end if

                                                        while not objrs.eof
                                                            response.write  "<option value='" & objrs("struct_ref") & "' "
                                                            If cint(objRs("struct_ref")) = cint(tier6_id) Then response.write " selected     "
                                                            response.write ">" & objrs("struct_name") 
                            
                                                         '   if (objrs("struct_deleted") or var_disabled)  then '  (StructureType =  "S" or StructureType =  "S_MS") and
                                                          '      response.write " (Disabled)"      
                                                           '     If cint(objRs("struct_ref")) = cint(tier6_id) Then var_disabled  = true
                                                            'end if   
                                                            response.write "</option>"
                    
                                                            objrs.movenext
                                                        wend
                                                    response.write "</select>"
                                                    response.write "</td></tr>"
                                                else
                                                    response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                                end if
                                        else
                                         response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL               
                                        end if
                                    else
                                       response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                       response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                                    end if                                    

                            else           
                               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                            end if
                                               
                            else
                                response.write "<input type='hidden' name='frm_user_tier4' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier5' value='0' />" 'ALL
                                response.write "<input type='hidden' name='frm_user_tier6' value='0' />" 'ALL
                            end if

                            

                    else           
                        response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
                        response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
                    end if

                else
                     response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
                     response.write "<input type='hidden' name='frm_user_tier4'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier5'  value='0' />" 'ALL
                     response.write "<input type='hidden' name='frm_user_tier6'  value='0' />" 'ALL
                end if

                

        else
            'we need this value when considering defaults for the time been until the permissions system is built
                response.write "<input type='hidden' name='frm_user_tier3' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier4' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier5' value='0' />"
               response.write "<input type='hidden' name='frm_user_tier6' value='0' />"
        end if

    
           
    response.write "</table>"
    end if


               
end sub



sub add_structure(var_user_ref, tier2_id, tier3_id, tier4_id, tier5_id, tier6_id, var_permission, var_opt_1, var_opt_2, var_opt_3, var_opt_4, var_opt_5)
 
    'code in here adds a structure element to a user       
   
    'Need checks in here to ensure that higher permissions are respected
    objcommand.commandtext = "select top 1 id from " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code='" & session("corp_code") & "' and acc_ref='" & var_user_ref & "' "
    if var_permission = "X" then
   
    objcommand.commandtext = objcommand.commandtext & " and permission = 'X' "
    elseif var_permission = "W" then
    objcommand.commandtext = objcommand.commandtext & " and (permission = 'W' or permission = 'X') "            
    elseif var_permission = "R" then 
        ' need to update all so no need to add to where
    end if

    if tier2_id = "0" then         
        objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '0' "
    else
        if tier3_id = "0" then         
            objcommand.commandtext = objcommand.commandtext & " and ((tier2_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '0')) "
        else
            if tier4_id = "0" then         
                objcommand.commandtext = objcommand.commandtext & " and ((tier2_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '0')) "
            else
                if tier5_id = "0" then         
                    objcommand.commandtext = objcommand.commandtext & " and ((tier2_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '0')) "
                else
                    if tier6_id = "0" then         
                        objcommand.commandtext = objcommand.commandtext & " and ((tier2_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '" & tier5_id & "' and tier6_ref = '0')) "
                    else
                        objcommand.commandtext = objcommand.commandtext & " and ((tier2_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '" & tier5_id & "' and tier6_ref = '0') or (tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '" & tier5_id & "' and tier6_ref = '" & tier6_id & "')) "
                    end if
                end if
            end if
        end if
    end if
   
    set objrs = objcommand.execute

    if not objrs.eof then
        'higher perms exist at a higher level so just leave this function
    else 
        'Also need checks in here to ensure that if more advanced permissions are given at a higher level that they overwrite those at lower levels. 

         objcommand.commandtext = "update " &  Application("DBTABLE_HR_USER_LOCATIONS") & " set arc_flag='1', arc_by='" & session("YOUR_ACCOUNT_ID") & "', arc_date = getdate()  where corp_code = '" & session("corp_code") & "' and acc_ref = '" & var_user_ref & "' "
         'build where statement for permission level
         if var_permission = "X" then
            ' need to update all so no need to add to where
         elseif var_permission = "W" then
            objcommand.commandtext = objcommand.commandtext & " and (permission = 'W' or permission = 'R') "            
         elseif var_permission = "R" then
            objcommand.commandtext = objcommand.commandtext & " and permission = 'R' "
         end if

         'build where statement for permission location
         if tier2_id = "0" then 
            ' need to update all locations so no need to add to where
         elseif tier3_id = "0" then 
            objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '" & tier2_id & "' "
         elseif tier4_id = "0" then 
            objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "'"
         elseif tier5_id = "0" then 
            objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "'"
         elseif tier6_id = "0" then 
            objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '" & tier5_id & "'"
         else
            objcommand.commandtext = objcommand.commandtext & " and tier2_ref = '" & tier2_id & "' and tier3_ref = '" & tier3_id & "' and tier4_ref = '" & tier4_id & "' and tier5_ref = '" & tier5_id & "' and tier6_ref = '" & tier6_id & "'"
         end if
         
         objcommand.execute


         'now add the new permission
         objcommand.commandtext = "insert into  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " (corp_code, acc_ref, tier2_ref, tier3_ref, tier4_ref, tier5_ref, tier6_ref, permission, created_by, created_datetime) " & _
                                                                                      "values ('" & session("corp_code") & "', '" & var_user_ref & "', '" & tier2_id & "', '" & tier3_id & "', '" & tier4_id & "', '" & tier5_id & "', '" & tier6_id & "', '" & var_permission & "', '" & session("YOUR_ACCOUNT_ID") & "', getdate())"
         objcommand.execute
       
    end if
    'now return the structure
    call return_structure(var_user_ref)
end sub


sub return_structure(var_user_ref)

    
    ' call startconnections()
    
       
            objcommand.commandtext = "select distinct tier2.struct_name, locs.tier2_ref, struct_deleted from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " as locs left join " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2 on locs.corp_code = tier2.corp_code and locs.tier2_ref = tier2.tier2_ref where locs.corp_code = '" & session("corp_code") &  "' and locs.acc_ref='" & var_user_ref & "' and locs.arc_flag = 0 order by tier2.struct_name "                           
            set  objRs_tier2 = objCommand.Execute  

            if not objRs_tier2.EOF then        
                while not objRs_tier2.eof   
                if objrs_tier2("tier2_ref")  = "0" then
                    var_tier2_struct_name = "All Levels"
                else
                    var_tier2_struct_name = objrs_tier2("struct_name")
                    if objrs_tier2("struct_deleted") then var_tier2_struct_name = var_tier2_struct_name & " (Disabled) "
                end if
                response.write "<div class='structure_2'><table width='100%'><tr><td>" & var_tier2_struct_name  & "</td><td width='100px'>" 
                objcommand.commandtext = "select permission, id from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='0' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0'    "                           
                set  objRs_perm = objCommand.Execute  
                var_display_but = True  
                if objRs_perm.eof then 
                    'no permission set 
                    response.write " Read Access </td><td width='100px'>"    'No               
                else
                    select case objRs_perm("permission")
                        case "R"
                            response.write " Read Access "
                        case "W"
                            response.write " Write Access "
                        case "X"
                            response.write " Full Access "
                        case else
                            response.write " Read Access " ' No
                            var_display_but = False
                    end select
                     response.write "</td><td width='100px'>"
                    If var_display_but Then response.write  "<input type='button' class='button' value='Remove' onclick=""removeStructure('" & var_user_ref & "', '" & objRs_perm("id") & "')"" />"                    
                end if
                response.write "</td></tr></table></div>"

                     objcommand.commandtext = "select distinct tier3.struct_name, locs.tier3_ref, struct_deleted from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " as locs left join " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3 on locs.corp_code = tier3.corp_code and locs.tier3_ref = tier3.tier3_ref where locs.corp_code = '" & session("corp_code") &  "' and locs.acc_ref='" & var_user_ref & "' and locs.arc_flag = 0 and tier3.tier2_ref = " & objRs_tier2("tier2_ref") & " order by tier3.struct_name "                           
                    set  objRs_tier3 = objCommand.Execute  

                    if not objRs_tier3.EOF then      
                        while not objRs_tier3.eof             
                         response.write "<div class='structure_3'><table width='100%'><tr><td>" & objRs_tier3("struct_name") 
                         if objRs_tier3("struct_deleted") then response.write " (Disabled) " 
                         response.write "</td><td width='100px'>" 
                        objcommand.commandtext = "select permission, id from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0'    "                           
                        set  objRs_perm = objCommand.Execute  
                        var_display_but = True  
                        if objRs_perm.eof then 
                            'no permission set - look at the options above this tier and see if they have write access set (Full access would have overwritten it)
                            objcommand.commandtext = "select permission from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='0' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0' and permission = 'W'    "                           
                            set  objRs_perm = objCommand.Execute  
                            if objRs_perm.eof then 
                                response.write " Read Access </td><td width='100px'>" 'No
                            else
                                response.write " Write Access </td><td width='100px'>" 
                            end if
                        else
                            select case objRs_perm("permission")
                                case "R"
                                    response.write " Read Access "
                                case "W"
                                    response.write " Write Access "
                                case "X"
                                    response.write " Full Access "
                                case else
                                    response.write " Read Access " ' No
                                    var_display_but = False
                            end select
                            response.write "</td><td width='100px'>"
                            if var_display_but then response.write "<input type='button' class='button' value='Remove' onclick=""removeStructure('" & var_user_ref & "', '" & objRs_perm("id") & "')"" />"
                        end if                        
                        response.write "</td></tr></table></div>"

                            objcommand.commandtext = "select distinct tier4.struct_name, locs.tier4_ref, struct_deleted from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " as locs left join " &  Application("DBTABLE_STRUCT_TIER4") & " as tier4 on locs.corp_code = tier4.corp_code and locs.tier4_ref = tier4.tier4_ref where locs.corp_code = '" & session("corp_code") &  "' and locs.acc_ref='" & var_user_ref & "' and locs.arc_flag = 0 and tier4.tier3_ref = " & objRs_tier3("tier3_ref") & " order by tier4.struct_name "                           
                            set  objRs_tier4 = objCommand.Execute  

                            if not objRs_tier4.EOF then    
                                while not objRs_tier4.eof               
                                 response.write "<div class='structure_4'><table width='100%'><tr><td>" & objRs_tier4("struct_name") 
                                 if objRs_tier4("struct_deleted") then response.write " (Disabled) "
                                 response.write "</td><td width='100px'>" 
                                objcommand.commandtext = "select permission, id from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "'  and tier5_ref='0' and tier6_ref='0'    "                           
                                set  objRs_perm = objCommand.Execute  
                                var_display_but = True  
                                if objRs_perm.eof then 
                                    'no permission set - look at the options above this tier and see if they have write access set (Full access would have overwritten it)
                                     objcommand.commandtext = "select permission from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and ((tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='0' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0')) and permission = 'W'    "                           
                                    set  objRs_perm = objCommand.Execute  
                                    if objRs_perm.eof then 
                                         response.write " Read Access </td><td width='100px'>" 'No
                                    else
                                         response.write " Write Access </td><td width='100px'>" 
                                    end if
                                else
                                    select case objRs_perm("permission")
                                        case "R"
                                            response.write " Read Access "
                                        case "W"
                                            response.write " Write Access "
                                        case "X"
                                            response.write " Full Access "
                                        case else
                                            response.write " Read Access " 'No
                                            var_display_but = False
                                    end select
                                    response.write "</td><td width='100px'>"
                                    if var_display_but then response.write "<input type='button' class='button' value='Remove' onclick=""removeStructure('" & var_user_ref & "', '" & objRs_perm("id") & "')"" />"
                                
                                end if
                                
                                response.write "</td></tr></table></div>"

                                    objcommand.commandtext = "select distinct tier5.struct_name, locs.tier5_ref, struct_deleted from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " as locs left join " &  Application("DBTABLE_STRUCT_TIER5") & " as tier5 on locs.corp_code = tier5.corp_code and locs.tier5_ref = tier5.tier5_ref where locs.corp_code = '" & session("corp_code") &  "' and locs.acc_ref='" & var_user_ref & "' and locs.arc_flag = 0 and tier5.tier4_ref = " & objRs_tier4("tier4_ref") & " order by tier5.struct_name "                           
                                    set  objRs_tier5 = objCommand.Execute  

                                    if not objRs_tier5.EOF then      
                                        while not objRs_tier5.eof             
                                         response.write "<div class='structure_5'><table width='100%'><tr><td>" & objRs_tier5("struct_name") 
                                         if objRs_tier5("struct_deleted") then response.write " (Disabled) " 
                                         response.write "</td><td width='100px'>" 
                                        objcommand.commandtext = "select permission, id from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "'   and tier5_ref='" & objRs_tier5("tier5_ref") & "' and tier6_ref='0'    "                           
                                        set  objRs_perm = objCommand.Execute  
                                        var_display_but = True
                                        if objRs_perm.eof then 
                                           'no permission set - look at the options above this tier and see if they have write access set (Full access would have overwritten it)
                                            objcommand.commandtext = "select permission from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and ((tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='0' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "' and tier5_ref='0' and tier6_ref='0')) and permission = 'W'    "                           
                                            set  objRs_perm = objCommand.Execute  
                                            if objRs_perm.eof then 
                                                    response.write " Read Access </td><td width='100px'>" 'No
                                            else
                                                    response.write " Write Access </td><td width='100px'>" 
                                            end if
                                        else
                                            select case objRs_perm("permission")
                                                case "R"
                                                    response.write " Read Access "
                                                case "W"
                                                    response.write " Write Access "
                                                case "X"
                                                    response.write " Full Access "
                                                case else
                                                    response.write " Read Access " 'No
                                                    var_display_but = False
                                            end select
                                            response.write "</td><td width='100px'>"
                                            if var_display_but then response.write "<input type='button' class='button' value='Remove' onclick=""removeStructure('" & var_user_ref & "', '" & objRs_perm("id") & "')"" />"
                                        end if                                        
                                        response.write "</td></tr></table></div>"

                                            objcommand.commandtext = "select distinct tier6.struct_name, locs.tier6_ref, struct_deleted from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " as locs left join " &  Application("DBTABLE_STRUCT_TIER6") & " as tier6 on locs.corp_code = tier6.corp_code and locs.tier6_ref = tier6.tier6_ref where locs.corp_code = '" & session("corp_code") &  "' and locs.acc_ref='" & var_user_ref & "' and locs.arc_flag = 0 and tier6.tier5_ref = " & objRs_tier5("tier5_ref") & " order by tier6.struct_name "                           
                                            set  objRs_tier6 = objCommand.Execute  

                                            if not objRs_tier6.EOF then   
                                                while not objRs_tier6.eof                
                                                 response.write "<div class='structure_6'><table width='100%'><tr><td>" & objRs_tier6("struct_name") 
                                                 if objRs_tier6("struct_deleted") then response.write " (Disabled) "
                                                 response.write "</td><td width='100px'>" 
                                                objcommand.commandtext = "select permission, id from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "'   and tier5_ref='" & objRs_tier5("tier5_ref") & "'  and tier6_ref='" & objRs_tier6("tier6_ref") & "'   "                           
                                                set  objRs_perm = objCommand.Execute  
                                                var_display_but = True
                                                if objRs_perm.eof then 
                                                    'no permission set - look at the options above this tier and see if they have write access set (Full access would have overwritten it)
                                                    objcommand.commandtext = "select permission from  " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code = '" & session("corp_code") &  "' and acc_ref='" & var_user_ref & "' and arc_flag = 0 and ((tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='0' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='0' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "' and tier5_ref='0' and tier6_ref='0') or (tier2_ref='" & objRs_tier2("tier2_ref") & "' and tier3_ref='" & objRs_tier3("tier3_ref") & "' and tier4_ref='" & objRs_tier4("tier4_ref") & "' and tier5_ref='" & objRs_tier5("tier5_ref") & "' and tier6_ref='0')) and permission = 'W'    "                           
                                                    set  objRs_perm = objCommand.Execute  
                                                    if objRs_perm.eof then 
                                                            response.write " Read Access </td><td width='100px'>" 'No
                                                    else
                                                            response.write " Write Access </td><td width='100px'>" 
                                                    end if
                                                else
                                                    select case objRs_perm("permission")
                                                        case "R"
                                                            response.write " Read Access "
                                                        case "W"
                                                            response.write " Write Access "
                                                        case "X"
                                                            response.write " Full Access "
                                                        case else
                                                            response.write " Read Access " 'No
                                                            var_display_but = False
                                                    end select
                                                    response.write "</td><td width='100px'>"                                                    
                                                    if var_display_but then response.write "<input type='button' class='button' value='Remove' onclick=""removeStructure('" & var_user_ref & "', '" & objRs_perm("id") & "')"" />"
                                                end if
                                                
                                                    response.write "</td></tr></table></div>"

                                                    objRs_tier6.movenext()
                                                wend
                                            end if
                                            
                                            objRs_tier5.movenext()
                                        wend
                                    end if

                                objRs_tier4.movenext()
                            wend
                        end if
                        
                            objRs_tier3.movenext()
                        wend
                    end if
                    objRs_tier2.movenext()
                wend
            else
                response.write "<div class='warning'><table><tr><th width='50px'>&nbsp;</th><td><strong>This user is currently not restricted</strong><br />This user can access assessments, audits etc in any part of your organisation's structure. Use the form above to make any restrictions that you require.</td></tr></table></div>" 
            end if
             

             ' objcommand.commandtext = "select tier2.tier2_ref + ':' + tier3.tier3_ref as struct_ref, tier2.struct_name + ' - ' + tier3.struct_name as struct_name from " &  Application("DBTABLE_STRUCT_TIER3") & " as tier3 left join " &  Application("DBTABLE_STRUCT_TIER2") & " as tier2 on tier3.corp_code = tier2.corp_code and tier3.tier2_ref = tier2.tier2_ref   where tier3.corp_code = '" & session("corp_code") &  "' "                           
              
    
   '  call endconnections()
    


end sub


sub remove_structure(var_user_ref, var_structure_id)

    objcommand.commandtext = "delete from " &  Application("DBTABLE_HR_USER_LOCATIONS") & " where corp_code='" & session("corp_code") & "' and acc_ref='" & var_user_ref & "' and id='" & var_structure_id & "'"
    objcommand.execute

    call return_structure(var_user_ref)
end sub

'******************************************Mike: remove from below this line ****************************************
sub generate_acc_tiers(var_accb_code, var_tier2_ref, var_tier3_ref, var_tier4_ref, var_tier5_ref, var_tier6_ref, var_frm_type, var_th_width, var_opt3, var_opt4, var_opt5)
 
    'var_frm_type S=Search 
    
    if var_frm_type = "S" then 
        var_extra_critera = ""
    else
        var_extra_critera = " and struct_deleted = 0"
    end if

    if len(var_accb_code) = 0 then 
        response.write "<div class='warning'><table width='100%'><tr><th width='40px'></th><td><strong>No incident centre selected</strong><br />Please select an incident centre.</strong></td></tr></table></div>"
    else
      
        'take accb_code and figure out the accb level
        objcommand.commandtext = "select rec_bu as tier2_ref, rec_bl as tier3_ref, tier4_ref, tier5_ref, tier6_ref from " & Application("DBTABLE_HR_DATA_ABOOK") & " where corp_code ='" & session("corp_code") & "' and accb_code ='" & var_accb_code & "'" 
        set objrs = objcommand.execute 

        if objrs.eof then ' doesn't exist
             if var_frm_type <> "S" and var_accb_code <> "ACCB00" then  response.write "<div class='warning'><table width='100%'><tr><th width='40px'></th><td><strong>No Incident Centre Selected</strong><br />Please select an incident centre.</td></tr></table></div>"
        else

            if objrs("tier2_ref") = "-1" then
                var_current_level = 2
                var_tier2_ref = objrs("tier2_ref")
                objcommand.commandtext = "SELECT 0 AS next_lev"

            elseif objrs("tier6_ref") > 0 then 
                var_current_level = 6
                var_tier6_ref = objrs("tier6_ref")
                var_tier5_ref = objrs("tier5_ref")
                var_tier4_ref = objrs("tier4_ref")
                var_tier3_ref = objrs("tier3_ref")
                var_tier2_ref = objrs("tier2_ref")
            else
                if objrs("tier5_ref") > 0 then 
                    var_current_level = 5
                    var_tier5_ref = objrs("tier5_ref")
                    var_tier4_ref = objrs("tier4_ref")
                    var_tier3_ref = objrs("tier3_ref")
                    var_tier2_ref = objrs("tier2_ref")
                    objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER6") &  " where corp_code='" & session("corp_code") & "' and tier5_ref = '" & objrs("tier5_ref") & "' " & var_extra_critera
                else
                    if objrs("tier4_ref") > 0 then 
                        var_current_level = 4
                        var_tier4_ref = objrs("tier4_ref")
                        var_tier3_ref = objrs("tier3_ref")
                        var_tier2_ref = objrs("tier2_ref")
                        objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER5") &  " where corp_code='" & session("corp_code") & "' and  tier4_ref = '" & objrs("tier4_ref") & "' " & var_extra_critera
                    else
                        if objrs("tier3_ref") > 0 then 
                            var_current_level = 3
                            var_tier3_ref = objrs("tier3_ref")
                            var_tier2_ref = objrs("tier2_ref")
                            objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER4") &  " where corp_code='" & session("corp_code") & "' and  tier3_ref = '" & objrs("tier3_ref") & "' " & var_extra_critera
                        else
                            if objrs("tier2_ref") > 0 then 
                                var_current_level = 2
                                var_tier2_ref = objrs("tier2_ref")
                                objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER3") &  " where corp_code='" & session("corp_code") & "' and  tier2_ref = '" & objrs("tier2_ref") & "' " & var_extra_critera
                            end if
                        end if   
                    end if   
                end if   
            end if    
           
            'next figure out if the accb is at the bottom level or whether we need to display child levels
            if var_current_level < 6 then
                set objrs = objcommand.execute

                if objrs("next_lev") > 0 then
                    'extra levels - print them out
                  
                   response.write "<table  width='100%' cellpadding='1px' cellspacing='2px'  class='showtd'>"
                   for i = var_current_level + 1 to 6  

                        select case i
                            case 2 
                                var_current_ref = var_tier2_ref
                                var_parent_ref = 0
                                var_temp_tier2_ref = "this.value"
                                var_temp_tier3_ref = 0
                                var_temp_tier4_ref = 0
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                            case 3
                                var_current_ref = var_tier3_ref
                                var_parent_ref = var_tier2_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = "this.value"
                                var_temp_tier4_ref = 0
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                            case 4
                                var_current_ref = var_tier4_ref
                                var_parent_ref = var_tier3_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = "this.value"
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                            case 5 
                                var_current_ref = var_tier5_ref
                                var_parent_ref = var_tier4_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = """" & var_tier4_ref & """"
                                var_temp_tier5_ref = "this.value"
                                var_temp_tier6_ref = 0
                            case 6
                                var_current_ref = var_tier6_ref
                                var_parent_ref = var_tier5_ref                                
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = """" & var_tier4_ref & """"
                                var_temp_tier5_ref = """" & var_tier5_ref & """"
                                var_temp_tier6_ref = "this.value"
                        end select
                         
                        'check theres a current ref for this level
                        if len(var_parent_ref) > 0 and var_parent_ref <> "0" then 

                             'check there are levels available
                          '  response.write i
                                objcommand.commandtext = " select count(id) as struct_count from " & Application("DBTABLE_STRUCT_TIER" & i) & " where corp_code = '" & session("corp_code") & "'  " & var_extra_critera
                                if i > 2 and var_parent_ref <> "0" then 
                                 objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                end if
                               ' response.write objcommand.commandtext
                                set objrs_sub = objcommand.execute

                                if objrs_sub("struct_count") > 0 then ' further levels exist
                                     if var_frm_type = "S" then
                                        var_top_opt_text = "Any " & session("CORP_TIER" & i) 
                                        if len(var_th_width) = 0 then var_th_width = "20%"
                                     else
                                        var_top_opt_text = "Please select a " & session("CORP_TIER" & i)
                                        if len(var_th_width) = 0 then var_th_width = "40%"
                                     end if

                                    response.write "<tr><th width='" & var_th_width &"'>What <strong>" & session("CORP_TIER" & i) & "</strong> did the incident occur in?</th><td><select class='select' name='frm_structure_tier" & i & "' onchange='reloadACCBStructure(""" & var_accb_code & """, " & var_temp_tier2_ref & ", " & var_temp_tier3_ref & ", " & var_temp_tier4_ref & ", " & var_temp_tier5_ref & ", " & var_temp_tier6_ref & ", """ & var_frm_type & """, """ & var_th_width & """, """", """", """")'><option value='0'>" & var_top_opt_text & "</option>"
                                    'add the options in here 
                                    objcommand.commandtext = "select tier" & cstr(i) & "_ref as struct_ref, struct_name, struct_deleted "
                                    if i = 4 then objcommand.commandtext = objcommand.commandtext & ", internal_reference "                                    
                                    objcommand.commandtext = objcommand.commandtext & " from " & Application("DBTABLE_STRUCT_TIER" & i) & " where corp_code = '" & session("corp_code") & "'  " & var_extra_critera
                                    if i > 2 and var_parent_ref <> "0" then '
                                     objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                    end if
                                   ' response.write objcommand.commandtext
                                    set objrs_sub = objcommand.execute

                                    if not objrs_sub.eof then 
                                        while not objrs_sub.eof
                                            if i = 4 then
                                                if len(objrs_sub("internal_reference")) > 0 then 
                                                    var_internal_reference = "Ref / Space ID: " & objrs_sub("internal_reference")
                                                else
                                                    var_internal_reference = ""
                                                end if
                                            else
                                                var_internal_reference = ""
                                            end if
                                            response.write "<option value='" & objrs_sub("struct_ref") & "' "
                                            if cstr(var_current_ref) = cstr(objrs_sub("struct_ref")) then response.write " selected "
                                            response.write ">" & objrs_sub("struct_name")
                                            if objrs_sub("struct_deleted") then response.write " (DISABLED)" 
                                            response.write "</option>"
                                            objrs_sub.movenext
                                        wend
                                    end if

                                    response.write "</select></td></tr>"

                                else
                                    exit for ' no more levels
                                end if
                        else
                            exit for ' no more levels
                        end if
                   next 

                   response.write "</table>"
                else
                    ' Bottom Level - do nothing
                    'response.write "No extra levels required" 'remove before placing live
                end if
            else
               ' response.write "No extra levels required" 'remove before placing live
            end if

        end if
    end if

end sub   

'sub generate_acc_tiers_by_cld(var_tier2_ref, var_tier3_ref, var_th_width, var_opt1, var_opt2, var_opt3)
'            
'            'next figure out if the accb is at the bottom level or whether we need to display child levels
'             var_current_level = 1 
'                  
'                   response.write "<table  width='100%' cellpadding='0px' cellspacing='2px' >"
'                   for i = var_current_level + 1 to 3  
'                  
'                        select case i
'                            case 2 
'                                var_current_ref = var_tier2_ref
'                                var_parent_ref = 0
'                                var_temp_tier2_ref = "this.value"
'                                var_temp_tier3_ref = 0
'                                var_sql = " and b.rec_bu = a.tier2_ref "
'                            case 3
'                                var_current_ref = var_tier3_ref
'                                var_parent_ref = var_tier2_ref
'                                var_temp_tier2_ref = """" & var_tier2_ref & """"
'                                var_temp_tier3_ref = "this.value"
'                                var_sql = " and b.rec_bu = a.tier2_ref and b.rec_bl = a.tier3_ref "
'                         '   case 4
'                         '       var_current_ref = var_tier4_ref
'                         '       var_parent_ref = var_tier3_ref
'                         '       var_temp_tier3_ref = """" & var_tier3_ref & """"
'
'                        end select
'                         
'                        'check theres a current ref for this level
'                        if (len(var_parent_ref) > 0 and var_parent_ref <> "0") or i = 2 then 
'                       
'                             'check there are levels available
'                          '  response.write i
'
'
'                                objcommand.commandtext = " select count(a.id) as struct_count from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a inner join " & Application("DBTABLE_HR_DATA_ABOOK") & " as b on a.corp_code = b.corp_code " & var_sql &  " where a.corp_code = '" & session("corp_code") & "' and  a.struct_deleted=0  " 
'                           ' response.write objcommand.commandtext
'                                if i > 2 and var_parent_ref <> "0" then 
'                                 objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
'                                end if
'                                set objrs_sub = objcommand.execute
'
'                                if objrs_sub("struct_count") > 0 then ' further levels exist
'                                     
'                                        var_top_opt_text = "Please select a " & session("CORP_TIER" & i)
'                                        if len(var_th_width) = 0 then var_th_width = "40%"
'                                    
'                                    response.write "<tr><th width='" & var_th_width & "' style='border-style: none; background: #FFF; color: gray; padding-left: 5px;'>" & session("CORP_TIER" & i) & ":</th><td style='border-style: none;'><select class='select' name='frm_structure_tier" & i & "' onchange='reloadACCBStructure_from_cld(" & var_temp_tier2_ref & ", " & var_temp_tier3_ref & ", """ & var_th_width & """, """", """", """")'><option value='0'>" & var_top_opt_text & "</option>"
'                                    'add the options in here 
'                                    objcommand.commandtext = "select distinct a.tier" & cstr(i) & "_ref as struct_ref, a.struct_name from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a  inner join " & Application("DBTABLE_HR_DATA_ABOOK") & " as b on a.corp_code = b.corp_code " & var_sql &  " where a.corp_code = '" & session("corp_code") & "'  and a.struct_deleted=0 "
'                                    if i > 2 and var_parent_ref <> "0" then '
'                                     objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
'                                    end if
'                                    set objrs_sub = objcommand.execute
'
'                                    if not objrs_sub.eof then 
'                                        while not objrs_sub.eof                                           
'                                            response.write "<option value='" & objrs_sub("struct_ref") & "' "
'                                            if cstr(var_current_ref) = cstr(objrs_sub("struct_ref")) then response.write " selected "
'                                            response.write ">" & objrs_sub("struct_name") &  "</option>"
'                                            objrs_sub.movenext
'                                        wend
'                                    end if
'
'                                    response.write "</select></td></tr>"
'
'                                else
'                                    exit for ' no more levels
'                                end if
'                        else
'                            exit for ' no more levels
'                        end if
'                   next 
'
'                   response.write "</table>"
'              
'
'end sub   


sub generate_acc_tiers_by_cld(var_tier2_ref, var_tier3_ref, var_tier4_ref, var_tier5_ref, var_tier6_ref, var_th_width, var_opt1, var_opt2, var_opt3)

            
            'next figure out if the accb is at the bottom level or whether we need to display child levels
             var_current_level = 1 
                  
                   response.write "<table  width='100%' cellpadding='0px' cellspacing='2px' >"
                   for i = var_current_level + 1 to 6  

                        select case i
                            case 2 
                                var_current_ref = var_tier2_ref
                                var_parent_ref = 0
                                var_temp_tier2_ref = "this.value"
                                var_temp_tier3_ref = 0
                                var_temp_tier4_ref = 0
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                                var_sql = " and b.rec_bu = a.tier2_ref "
                                var_accb_at_level_sql =  " and rec_bu = '" & var_tier2_ref & "' and (rec_bl = '0' or rec_bl = '-1')"
                            case 3
                                var_current_ref = var_tier3_ref
                                var_parent_ref = var_tier2_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = "this.value"
                                var_temp_tier4_ref = 0
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                                var_sql = " and b.rec_bu = a.tier2_ref and (b.rec_bl = a.tier3_ref  or b.rec_bl = '0' or b.rec_bl = '-1')"
                                var_accb_at_level_sql =  " and ((rec_bu = '" & var_tier2_ref & "' and (rec_bl = '0' or rec_bl = '-1')) "
                               '     if var_temp_tier3_ref <> "0" then var_accb_at_level_sql =  var_accb_at_level_sql &    " or (rec_bl = '" & var_tier3_ref & "' and (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and tier4_ref = '0' or tier4_ref = '-1'))  "  
                                var_accb_at_level_sql =  var_accb_at_level_sql & "  )"
                            case 4
                                var_current_ref = var_tier4_ref
                                var_parent_ref = var_tier3_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = "this.value"
                                var_temp_tier5_ref = 0
                                var_temp_tier6_ref = 0
                                var_sql = " and b.rec_bl = a.tier3_ref and (b.tier4_ref = a.tier4_ref  or b.tier4_ref = '0' or b.tier4_ref = '-1' )"
                                var_accb_at_level_sql =  " and ((rec_bu = '" & var_tier2_ref & "' and (rec_bl = '0' or rec_bl = '-1'))  "  
                                if var_temp_tier4_ref <> "0"  then var_accb_at_level_sql =  var_accb_at_level_sql &    " or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and (tier4_ref = '0' or tier4_ref = '-1'))  "  
                                var_accb_at_level_sql =  var_accb_at_level_sql & "  )"
                            case 5 
                                var_current_ref = var_tier5_ref
                                var_parent_ref = var_tier4_ref
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = """" & var_tier4_ref & """"
                                var_temp_tier5_ref = "this.value"
                                var_temp_tier6_ref = 0
                                var_sql = " and b.tier4_ref = a.tier4_ref  and (b.tier5_ref = a.tier5_ref  or b.tier5_ref = '0' or b.tier5_ref = '-1' ) "
                                var_accb_at_level_sql =  " and ((rec_bu = '" & var_tier2_ref & "' and (rec_bl = '0' or rec_bl = '-1')) "  
                                  if var_temp_tier5_ref <> "0" then var_accb_at_level_sql =  var_accb_at_level_sql &    " or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and (tier4_ref = '0' or tier4_ref = '-1')) or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and tier4_ref = '" & var_tier4_ref & "' and (tier5_ref = '0' or tier5_ref = '-1'))     "  
                                var_accb_at_level_sql =  var_accb_at_level_sql & "  )"
                            case 6
                                var_current_ref = var_tier6_ref
                                var_parent_ref = var_tier5_ref                                
                                var_temp_tier2_ref = """" & var_tier2_ref & """"
                                var_temp_tier3_ref = """" & var_tier3_ref & """"
                                var_temp_tier4_ref = """" & var_tier4_ref & """"
                                var_temp_tier5_ref = """" & var_tier5_ref & """"
                                var_temp_tier6_ref = "this.value"
                                var_sql = " and b.tier5_ref = a.tier5_ref and (b.tier6_ref = a.tier6_ref  or b.tier6_ref = '0' or b.tier6_ref = '-1' ) "
                                var_accb_at_level_sql =  " and ((rec_bu = '" & var_tier2_ref & "' and (rec_bl = '0' or rec_bl = '-1')) "  
                                 if var_temp_tier6_ref <> "0" then var_accb_at_level_sql =  var_accb_at_level_sql &    " or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and (tier4_ref = '0' or tier4_ref = '-1')) or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and tier4_ref = '" & var_tier4_ref & "' and (tier5_ref = '0' or tier5_ref = '-1'))  or (rec_bu = '" & var_tier2_ref & "' and rec_bl = '" & var_tier3_ref & "' and tier4_ref = '" & var_tier4_ref & "' and tier5_ref = '" & var_tier5_ref & "' and (tier6_ref = '0' or tier6_ref = '-1'))   "  ' or tier6_ref = '" & var_tier6_ref & "'
                                var_accb_at_level_sql =  var_accb_at_level_sql & "  )"
                        end select
                         
                        'check theres a current ref for this level
                        if (len(var_parent_ref) > 0 and var_parent_ref <> "0") or i = 2 then 
                       
                             'check there are levels available
                          '  response.write i


                                objcommand.commandtext = " select sum(struct_count) as struct_count from (select count(a.id) as struct_count from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a inner join " & Application("DBTABLE_HR_DATA_ABOOK") & " as b on a.corp_code = b.corp_code " & var_sql &  " and b.deactive is null where a.corp_code = '" & session("corp_code") & "' and  a.struct_deleted=0  " 
                                if i > 2 and var_parent_ref <> "0" then 
                                 objcommand.commandtext = objcommand.commandtext & " and a.tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                 objcommand.commandtext = objcommand.commandtext & " union  select count(a.id) as struct_count from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a  where a.corp_code = '" & session("corp_code") & "' and  a.struct_deleted=0  and a.tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                end if
                                objcommand.commandtext = objcommand.commandtext & ") as counttiers"
     ' response.write objcommand.commandtext & "..."  & var_parent_ref
                                set objrs_sub = objcommand.execute

                                if objrs_sub("struct_count") > 0 then ' further levels exist
                                        var_top_opt_text = trans("acc.pleaseselecta.js") & " " & session("CORP_TIER" & i)
                                        if len(var_th_width) = 0 then var_th_width = "40%"
                                    
                                    
                                    'add the options in here 
                                    objcommand.commandtext = "select distinct a.tier" & cstr(i) & "_ref as struct_ref, a.struct_name from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a  inner join " & Application("DBTABLE_HR_DATA_ABOOK") & " as b on a.corp_code = b.corp_code " & var_sql &  " and b.deactive is null where a.corp_code = '" & session("corp_code") & "'  and a.struct_deleted=0 "
                                    if i > 2 and var_parent_ref <> "0" then '
                                         objcommand.commandtext = objcommand.commandtext & " and a.tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                       
                                        ' objcommand.commandtext = objcommand.commandtext & " union  select  distinct a.tier" & cstr(i) & "_ref as struct_ref, a.struct_name  from " & Application("DBTABLE_STRUCT_TIER" & i) & " as a  where a.corp_code = '" & session("corp_code") & "' and  a.struct_deleted=0  and a.tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                    end if
                        
'response.write "<br />///" & objcommand.commandtext    & "<br />///"                              
                                    set objrs_sub = objcommand.execute

                                    response.write "<tr><th width='" & var_th_width & "' style='border-style: none; background: #FFF; color: gray; padding-left: 5px;'>" & session("CORP_TIER" & i) & ":</th><td style='border-style: none;'><select class='select' name='frm_structure_tier" & i & "' onchange='reloadACCBStructure_from_cld(" & var_temp_tier2_ref & ", " & var_temp_tier3_ref & ", " & var_temp_tier4_ref & ", " & var_temp_tier5_ref & ", " & var_temp_tier6_ref & ", """ & var_th_width & """, """", """", """")'><option value='0'>" & var_top_opt_text & "</option>"
                                    
                                    'allow the user to select that level as where the incident happened
                                    if session("acc_portal_allow_select_at_this_level") = "Y" then
                                        'are we far enough down for this location to be at or under an incident centre
                                        objcommand.commandtext = " select count(id) as cnt from " & Application("DBTABLE_HR_DATA_ABOOK") & " " & _
                                                                 " where corp_code = '" & session("corp_code") & "' and  deactive is NULL  " & var_accb_at_level_sql 
                                         
                                      set objrs_chk = objcommand.execute
            
                                        if objrs_chk("cnt") > 0 then
                                            response.write "<option value='-1' "
                                             if cstr(var_current_ref) = "-1" then response.write " selected "
                                            response.write ">" & trans("acc.incidenthappenedatthislevel.opt") & "</option>"
                                        end if
                                    end if 'end of allow the user to select that level as where the incident happened
                                

                                   if not objrs_sub.eof then 
                                        while not objrs_sub.eof                                           
                                            response.write "<option value='" & objrs_sub("struct_ref") & "' "
                                            if cstr(var_current_ref) = cstr(objrs_sub("struct_ref")) then response.write " selected "
                                            response.write ">" & objrs_sub("struct_name") &  "</option>"
                                            objrs_sub.movenext
                                        wend
                                    end if

                                    response.write "</select></td></tr>"

                                else
                                    exit for ' no more levels
                                end if


                        else
                            exit for ' no more levels
                        end if
                   next 

                   response.write "</table>"
              

end sub   


sub display_acc_tiers(var_accb_code, var_tier2_ref, var_tier3_ref, var_tier4_ref, var_tier5_ref, var_tier6_ref, var_dsp_table, var_th_width, var_opt3, var_opt4, var_opt5)
 
    'var_frm_type S=Search 
    
  '  if var_frm_type = "S" then 
        var_extra_critera = ""
    'else
   '     var_extra_critera = " and struct_deleted = 0"
   ' end if

    if len(var_accb_code) = 0  and var_dsp_table = "Y" then 
        response.write "<div class='warning'><table width='100%'><tr><th width='40px'></th><td><strong>No incident centre selected</strong><br />Please select an incident centre.</td></tr></table></div>"
    else
       
        'take accb_code and figure out the accb level
        objcommand.commandtext = "select rec_bu as tier2_ref, rec_bl as tier3_ref, tier4_ref, tier5_ref, tier6_ref from " & Application("DBTABLE_HR_DATA_ABOOK") & " where corp_code ='" & session("corp_code") & "' and accb_code ='" & var_accb_code & "'" 
        set objrs = objcommand.execute 

        if objrs.eof   then
       
            if var_frm_type <> "S" and var_accb_code <> "ACCB00" then  
                response.write "<div class='warning'><table width='100%'><tr><th width='40px'></th><td>Please select an incident centre.</td></tr></table></div>"
            elseif var_accb_code = "ACCB00" then
                'Fire Incident so Display Location
                objcommand.commandtext =  "select struct_name from " & Application("DBTABLE_STRUCT_TIER2") &  " where corp_code='" & session("corp_code") & "' and tier2_ref='" & var_tier2_ref & "'"
                set objrs = objcommand.execute

                if not objrs.eof then 
                    response.write "<tr><th width='" & var_th_width & "'>" & session("corp_tier2") & "</th><td>" & objrs("struct_name") & "</td></tr>"
                    objcommand.commandtext =  "select struct_name from " & Application("DBTABLE_STRUCT_TIER3") &  " where corp_code='" & session("corp_code") & "' and tier2_ref='" & var_tier2_ref & "' and tier3_ref='" & var_tier3_ref & "'"
                    set objrs = objcommand.execute

                    if not objrs.eof then 
                        response.write "<th width='" & var_th_width & "'>" & session("corp_tier3") & "</th><td>" & objrs("struct_name") & "</td></tr>"
                        objcommand.commandtext =  "select struct_name from " & Application("DBTABLE_STRUCT_TIER4") &  " where corp_code='" & session("corp_code") & "' and tier3_ref='" & var_tier3_ref & "' and tier4_ref='" & var_tier4_ref & "'"
                        set objrs = objcommand.execute

                        if not objrs.eof then 
                            response.write "<tr><th width='" & var_th_width & "'>" & session("corp_tier4") & "</th><td>" & objrs("struct_name") & "</td></tr>"
                            objcommand.commandtext =  "select struct_name from " & Application("DBTABLE_STRUCT_TIER5") &  " where corp_code='" & session("corp_code") & "' and tier4_ref='" & var_tier4_ref & "' and tier5_ref='" & var_tier5_ref & "'"
                            set objrs = objcommand.execute
                            if not objrs.eof then 
                                response.write "<tr><th width='" & var_th_width & "'>" & session("corp_tier5") & "</th><td>" & objrs("struct_name") & "</td></tr>"
                                objcommand.commandtext =  "select struct_name from " & Application("DBTABLE_STRUCT_TIER6") &  " where corp_code='" & session("corp_code") & "' and tier5_ref='" & var_tier5_ref & "' and tier6_ref='" & var_tier6_ref & "'"
                                set objrs = objcommand.execute

                                if not objrs.eof then 
                                    response.write "<tr><th width='" & var_th_width & "'>" & session("corp_tier6") & "</th><td>" & objrs("struct_name") & "</td></tr>"                           
                                end if                            
                            end if
                        end if
                    end if
                    response.write ""
                end if
            end if
        else

            if objrs("tier2_ref") = "-1" then
                var_current_level = 2
                var_tier2_ref = objrs("tier2_ref")
                objcommand.commandtext = "SELECT 0 AS next_lev"

            elseif objrs("tier6_ref") > 0 then 
                var_current_level = 6
                var_tier6_ref = objrs("tier6_ref")
                var_tier5_ref = objrs("tier5_ref")
                var_tier4_ref = objrs("tier4_ref")
                var_tier3_ref = objrs("tier3_ref")
                var_tier2_ref = objrs("tier2_ref")
            else
                if objrs("tier5_ref") > 0 then 
                    var_current_level = 5
                    var_tier5_ref = objrs("tier5_ref")
                    var_tier4_ref = objrs("tier4_ref")
                    var_tier3_ref = objrs("tier3_ref")
                    var_tier2_ref = objrs("tier2_ref")
                    objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER6") &  " where corp_code='" & session("corp_code") & "' and tier5_ref = '" & objrs("tier5_ref") & "' " & var_extra_critera
                else
                    if objrs("tier4_ref") > 0 then 
                        var_current_level = 4
                        var_tier4_ref = objrs("tier4_ref")
                        var_tier3_ref = objrs("tier3_ref")
                        var_tier2_ref = objrs("tier2_ref")
                        objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER5") &  " where corp_code='" & session("corp_code") & "' and  tier4_ref = '" & objrs("tier4_ref") & "' " & var_extra_critera
                    else
                        if objrs("tier3_ref") > 0 then 
                            var_current_level = 3
                            var_tier3_ref = objrs("tier3_ref")
                            var_tier2_ref = objrs("tier2_ref")
                            objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER4") &  " where corp_code='" & session("corp_code") & "' and  tier3_ref = '" & objrs("tier3_ref") & "' " & var_extra_critera
                        else
                            if objrs("tier2_ref") > 0 then 
                                var_current_level = 2
                                var_tier2_ref = objrs("tier2_ref")
                                objcommand.commandtext = "select count(id) as next_lev from " & Application("DBTABLE_STRUCT_TIER3") &  " where corp_code='" & session("corp_code") & "' and  tier2_ref = '" & objrs("tier2_ref") & "' " & var_extra_critera
                            end if
                        end if   
                    end if   
                end if   
            end if    
           
            'next figure out if the accb is at the bottom level or whether we need to display child levels
            if var_current_level < 6 then
                set objrs = objcommand.execute

                if objrs("next_lev") > 0 then
                    'extra levels - print them out
                   if var_dsp_table = "Y" then response.write "<table  width='100%' cellpadding='1px' cellspacing='2px'  class='showtd'>"
                   for i = var_current_level + 1 to 6  

                        select case i
                            case 2 
                                var_current_ref = var_tier2_ref
                                var_parent_ref = 0
                            case 3
                                var_current_ref = var_tier3_ref
                                var_parent_ref = var_tier2_ref
                            case 4
                                var_current_ref = var_tier4_ref
                                var_parent_ref = var_tier3_ref
                            case 5 
                                var_current_ref = var_tier5_ref
                                var_parent_ref = var_tier4_ref
                            case 6
                                var_current_ref = var_tier6_ref
                                var_parent_ref = var_tier5_ref   
                        end select
                         
                        'check theres a current ref for this level
                        if len(var_parent_ref) > 0 and var_parent_ref <> "0" then 

                             'check there are levels available
                          '  response.write i
                                objcommand.commandtext = " select count(id) as struct_count from " & Application("DBTABLE_STRUCT_TIER" & i) & " where corp_code = '" & session("corp_code") & "'  " & var_extra_critera
                                if i > 2 and var_parent_ref <> "0" then 
                                 objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                end if
                               ' response.write objcommand.commandtext
                                set objrs_sub = objcommand.execute

                                if objrs_sub("struct_count") > 0 then ' further levels exist
                                   
                                    response.write "<tr><th width='" & var_th_width &"'>" & session("CORP_TIER" & i) & "</th><td>"
                                    'add the options in here 
                                    objcommand.commandtext = "select struct_name, struct_deleted from " & Application("DBTABLE_STRUCT_TIER" & i) & " where corp_code = '" & session("corp_code") & "' and tier" & cstr(i) & "_ref = '" & var_current_ref & "'  " & var_extra_critera
                                    if i > 2 and var_parent_ref <> "0" then '
                                     objcommand.commandtext = objcommand.commandtext & " and tier" & cstr(i-1) & "_ref='" & var_parent_ref & "'"
                                    end if
                                    'response.write objcommand.commandtext
                                    set objrs_sub = objcommand.execute

                                    if not objrs_sub.eof then 
                                         response.write objrs_sub("struct_name")
                                         if objrs_sub("struct_deleted") then response.write " (Disabled)"
                                    else
                                         response.write "Couldn't find the location"                                         
                                    end if

                                    response.write "</td></tr>"

                                else
                                    exit for ' no more levels
                                end if
                        else
                            exit for ' no more levels
                        end if
                   next 

                  if var_dsp_table = "Y" then response.write "</table>"
                else
                    ' Bottom Level - do nothing
                    'response.write "No extra levels required" 'remove before placing live
                end if
            else
               ' response.write "No extra levels required" 'remove before placing live
            end if

        end if
    end if

end sub



function display_accb_name(var_tmp_accb_code, var_disp_accb,  var_opt1, var_opt2, var_opt3, var_opt4)
   objcommand.commandtext = "select a.*, tier2.struct_name as tier2_name, tier3.struct_name as tier3_name, tier4.struct_name as tier4_name, tier5.struct_name as tier5_name, tier6.struct_name as tier6_name  from " & Application("DBTABLE_HR_DATA_ABOOK") & " as a " & _
                                             "left join " & Application("DBTABLE_STRUCT_TIER2") & " as tier2 " & _
                                             "on a.rec_bu = tier2.tier2_ref " & _
                                             "and a.corp_code = tier2.corp_code " & _
                                             "left join " & Application("DBTABLE_STRUCT_TIER3") & " as tier3 " & _
                                             "on a.rec_bl = tier3.tier3_ref " & _
                                             "and a.corp_code = tier3.corp_code " & _
                                             "left join " & Application("DBTABLE_STRUCT_TIER4") & " as tier4 " & _
                                             "on a.tier4_ref = tier4.tier4_ref " & _
                                             "and a.corp_code = tier4.corp_code " & _
                                             "left join " & Application("DBTABLE_STRUCT_TIER5") & " as tier5 " & _
                                             "on a.tier5_ref = tier5.tier5_ref " & _
                                             "and a.corp_code = tier5.corp_code " & _
                                             "left join " & Application("DBTABLE_STRUCT_TIER6") & " as tier6 " & _
                                             "on a.tier6_ref = tier6.tier6_ref " & _
                                             "and a.corp_code = tier6.corp_code " & _
                                             "where a.corp_code = '" & session("corp_code") & "' and a.accb_code = '" & var_tmp_accb_code & "' "
                     set objrs_accb = objcommand.execute

                     if not objrs_accb.eof then
                        
                        var_acc_details = objrs_accb("tier2_name") 
                        var_level = 2
                        if objrs_accb("rec_bu") = "-1" then 
                            var_acc_details =  "All " & Session("corp_tier2_plural")
                        elseif objrs_accb("rec_bl") > 0 then 
                            var_acc_details = var_acc_details & " > " & objrs_accb("tier3_name") 
                             var_level = 3
                            if objrs_accb("tier4_ref") > 0 then 
                                var_acc_details = var_acc_details & " > " &  objrs_accb("tier4_name") 
                                 var_level = 4
                                if objrs_accb("tier5_ref") > 0 then 
                                    var_acc_details = var_acc_details & " > " &  objrs_accb("tier5_name")
                                     var_level = 5
                                    if objrs_accb("tier6_ref") > 0 then 
                                        var_acc_details = var_acc_details & " > " &  objrs_accb("tier6_name") 
                                         var_level = 6
                                    end if 
                                end if
                            end if
                        end if
                     end if 
      if var_disp_accb = "Y" or cstr(var_disp_accb) = "1" then var_acc_details = var_acc_details & " (" & var_tmp_accb_code & ")"
      display_accb_name = var_acc_details
end function


sub adv_contact_dets(var_curr_status, var_ft, var_dd, var_opt1, var_opt2, var_opt3)

    if var_opt3 = "Y" then var_opt3 = "class='showtd'" else var_opt3 = ""

    if len(var_curr_status) > 0 then
            objCommand.commandText = "SELECT opt_id, opt_target FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                        "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                        "  AND opt_id = " & var_curr_status & " " & _
                                        "  AND related_to = 'acc_status'"
            set objRsINC = objCommand.execute
            if not objRsINC.EOF then
                 if objRsINC("opt_target") = "Freetext" then
                        response.write "<div id='ACCSTATUS_POPUP_FT'>" & _
                                        "<table width='100%' cellpadding='1px' cellspacing='2px' " & var_opt3 & ">" & _
                                           "<tr>" & _
                                                "<th width='180px'>Please Specify</th>" & _
                                                "<td><input type='text' class='text' id='" & var_opt1 & "' name='" & var_opt1 & "' value='" & var_ft & "' /></td>" & _
                                            "</tr>" & _
                                        "</table>" & _
                                    "</div>"
                elseif len(objRsINC("opt_target")) > 0 and isnull(objRsINC("opt_target")) = false  then  
                            response.write "<div id='ACCSTATUS_POPUP_DD' >" & _
                                            "<table width='100%' cellpadding='1px' cellspacing='2px' " & var_opt3 & ">" & _
                                                "<tr>" & _
                                                    "<th width='180px'>Department &amp; Section</th>" & _
                                                    "<td>"
                                                    
                                                    'call autogenEmpStatus(var_frm_inc_per_emp_status_dd, "frm_inc_per_emp_status_dd", "", var_frm_inc_per_status)
                                                   ' Sub autogenEmpStatus(opt_value, field_name, field_disabled, filter_list_item)

	                                                    if isnull(var_dd) = true or var_dd = "" or var_dd = "na" then
		                                                    var_dd = 0
                                                        end if
	
	                                                    objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
							                                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
							                                                     "  AND related_to = 'emp_status' " & _
							                                                     "  AND parent_id = 0 "
	                                                    if len(var_curr_status) > 0 then
		                                                    objCommand.commandText = objCommand.commandText & "  AND opt_value = (SELECT opt_target FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
																		                                                         "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
																		                                                         "AND related_to = 'acc_status' " & _
																		                                                         "AND opt_id = " & var_curr_status & ")"
	                                                    end if
	
	                                                    set objRsREL = objCommand.execute
	                                                    if not objRsREL.EOF then
		                                                    response.write "<select class='select' name='" & var_opt2 & "' id='" & var_opt2 & "' ><option value='na'>Please Select</option>"

		                                                    while not objRsREL.EOF
			
			                                                    if objRsREL("child_present") = 1 then
				                                                    response.write "<optgroup label='" & objRsREL("opt_value") & "'>"

				                                                    objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
										                                                     "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
										                                                     "  AND related_to = 'emp_status' " & _
										                                                     "  AND parent_id = " & objRsREL("opt_id")
				                                                    set objRsREL2 = objCommand.execute
				                                                    if not objRsREL2.EOF then
					                                                    while not objRsREL2.EOF
						                                                    if cint(var_dd) = cint(objRsREL2("opt_id")) then
							                                                    response.write "<option value='" & objRsREL2("opt_id") & "' selected>" & objRsREL2("opt_value") & "</option>"
						                                                    else
							                                                    response.write "<option value='" & objRsREL2("opt_id") & "'>" & objRsREL2("opt_value") & "</option>"
						                                                    end if
						                                                    objRsREL2.movenext
					                                                    wend
				                                                    end if
				                                                    set objRsREL2 = nothing
				                                                    response.write "</optgroup>"
			                                                    else
				                                                    if cint(var_dd) = cint(objRsREL("opt_id")) then
					                                                    response.write "<option value='" & objRsREL("opt_id") & "' selected>" & objRsREL("opt_value") & "</option>"
				                                                    else
					                                                    response.write "<option value='" & objRsREL("opt_id") & "'>" & objRsREL("opt_value") & "</option>"
				                                                    end if
			                                                    end if

			                                                    objRsREL.movenext
		                                                    wend

		                                                    response.write "</select>"
	                                                    end if
	                                                    set objRsREL = nothing

                                                   ' End Sub
                                                        
                                                    response.write "</td>" & _
                                                "</tr>" & _
                                            "</table>" & _
                                        "</div>"                                  
                                    
                end if
              
          end if
            set objRsINC = nothing
       end if
    
        
end sub



sub generate_ajax_nmiss_opts(var_status, var_fieldname, var_nmiss_category)

    showDisabled = false

    if val_sub_additional = true then
        response.Write "<SELECT Name='" & var_fieldname & "' class='select' onchange='checkTrigger(this)'>"
    else
        Response.Write("<SELECT Name='" & var_fieldname & "' class='select'>")
    end if
    Response.Write("<option value='any'>Please select a classification ...</option>")
    
    if  Session("MODULE_ACCB") ="2" then	
        if isnull(val_sub_value) then 
            val_sub_value = ""
        end if
        
         objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled, child_present from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '0' and opt_target = '" & var_nmiss_category & "' and opt_language = '" & session("YOUR_LANGUAGE") & "' order by opt_id asc"
        set objrsNMC = objcommand.execute

        if not objrsNMC.eof then 
            while not objrsNMC.eof
                if objrsNMC("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                    if cstr(val_sub_value) =  cstr(objrsNMC("opt_id")) then
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "' selected>" &  objrsNMC("opt_value") & "</option>"
                    else
                        response.Write  "<option value='" & objrsNMC("opt_id") & "' title='" & objrsNMC("opt_description") & "'>" &  objrsNMC("opt_value") & "</option>"
                    end if
                
                     if objrsNMC("child_present") = "1" then
                            'There are sub options available
                            objcommand.commandtext = "select opt_id, opt_value, opt_description, opt_disabled from " & Application("DBTABLE_HR_ACC_OPTS") & " where corp_code='" & session("CORP_CODE") & "' and related_to='nmiss_class' and parent_id= '" & objrsNMC("opt_id") & "' and opt_target = '" & var_nmiss_category & "' and opt_language = '" & session("YOUR_LANGUAGE") & "' order by opt_id asc"
                            set objrsNMCSUB = objcommand.execute

                            if not objrsNMCSUB.eof then 
                                while not objrsNMCSUB.eof
                                    if objrsNMCSUB("opt_disabled") = false or showDisabled = true or cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                        if cstr(val_sub_value) =  cstr(objrsNMCSUB("opt_id")) then
                                            response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "' selected>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                        else
                                            response.Write  "<option value='" & objrsNMCSUB("opt_id") & "' title='" & objrsNMCSUB("opt_description") & "'>--" & objrsNMC("opt_value") & " (" &  objrsNMCSUB("opt_value") & ")</option>"
                                        end if
                                    end if                                                                   
                                    objrsNMCSUB.movenext
                                wend
                            end if
                    
                     end if
                end if
                
                objrsNMC.movenext
            wend
        end if
        
      
    else
       
        DIM CLASS_Category(39),classARRAY

	    CLASS_Category(0) = "Access / Egress"
	    CLASS_Category(1) = "Adverse Weather"
	    CLASS_Category(2) = "Animal"
	    CLASS_Category(3) = "Biological"
	    CLASS_Category(4) = "Collapse of Structure"
	    CLASS_Category(5) = "Compressed Air"
	    CLASS_Category(6) = "Confined Spaces"
	    CLASS_Category(7) = "Drowning / Asphyxiation"
	    CLASS_Category(8) = "D.S.E / V.D.U Usage"
	    CLASS_Category(9) = "Electricity"
	    CLASS_Category(10) = "Energy Release"
	    CLASS_Category(11) = "Environmental"
	    CLASS_Category(12) = "Ergonomics"
	    CLASS_Category(13) = "Excavation"
	    CLASS_Category(14) = "Explosion"
	    CLASS_Category(15) = "Fall of Object from Height"
	    CLASS_Category(16) = "Fall of Persons from Height"
	    CLASS_Category(17) = "Fire Safety"
	    CLASS_Category(18) = "Food Hygiene"
	    CLASS_Category(19) = "Gas"
	    CLASS_Category(20) = "Hazardous Substance"
	    CLASS_Category(21) = "Housekeeping"
	    CLASS_Category(22) = "Human Factors"
	    CLASS_Category(23) = "Lifting Equipment"
	    CLASS_Category(24) = "Lighting"
	    CLASS_Category(25) = "Machinery"
	    CLASS_Category(26) = "Manual Handling"
	    CLASS_Category(27) = "Noise"
	    CLASS_Category(28) = "Pressure"
	    CLASS_Category(29) = "Radiation"
	    CLASS_Category(30) = "Sharp Objects"
	    CLASS_Category(31) = "Slip / Trip / Fall"
	    CLASS_Category(32) = "Storage"
	    CLASS_Category(33) = "Stress"
	    CLASS_Category(34) = "Temperature Extremes"
	    CLASS_Category(35) = "Vehicles"
	    CLASS_Category(36) = "Ventilation"
	    CLASS_Category(37) = "Vibration"
	    CLASS_Category(38) = "Violence"
	    CLASS_Category(39) = "Work Equipment"

    	    	
        For classARRAY = 0 to 39
            if len(val_sub_value) > 0 then
                If clng(val_sub_value) = clng(classARRAY) Then
                    Response.Write("<option value='" & classARRAY & "' selected='selected'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                Else
                    Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
                End if
            Else
                Response.Write("<option value='" & classARRAY & "'>" & CLASS_Category(classARRAY) & "</option>" & vbCrLf)
            End if
        Next
        
    end  if		
	Response.Write("</select>")
end sub 


sub generate_ajax_defined_sub_opts(field_status, field_name, parent_id)

    if isnull(myStatus) or len(myStatus) = 0 then mystatus = "0"
    if isnumeric(parent_id) = false then parent_id = "-1"
    if isnull(parent_id) or len(parent_id) = 0 then parent_id = "-1"

    objCommand.commandText = "SELECT opt_id, opt_value, opt_description, opt_disabled FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                "  AND related_to = 'inc_type_defined' " & _
                                "  AND parent_id = '" & parent_id & "' " & _
                                "  AND opt_language = '" & session("YOUR_LANGUAGE") & "' " & _
                                "ORDER BY opt_id"
    set objOpts = objCommand.execute
    if not objOpts.EOF then

        response.write "<select name='" & field_name & "' id='" & field_name & "' class='select' " & field_status & ">" & _
                       "<option value=''>Select type...</option>"

        while not objOpts.EOF
            if objOpts("opt_disabled") = false then
                response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "'>" & objOpts("opt_value") & "</option>"
            end if

            objOpts.movenext
        wend

        response.write "</select>"

    end if
    set objOpts = nothing
    

end sub


sub generate_portal_v1_ajax_defined_sub_opts(myStatus, field_name, field_status, parent_id, opt_header)



    if isnull(myStatus) or len(myStatus) = 0 then mystatus = "0"
    if isnull(parent_id) or len(parent_id) = 0 or parent_id = 0 then parent_id = "-1"

    objCommand.commandText = "SELECT opt_id, opt_value, opt_description, opt_disabled FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                "  AND related_to = 'inc_type_defined' " & _
                                "  AND parent_id = '" & parent_id & "' " & _
                                "  AND opt_language='" & session("YOUR_LANGUAGE") & "' " & _
                                "ORDER BY opt_id"
    set objOpts = objCommand.execute
    if not objOpts.EOF then

        response.write "<select name='" & field_name & "' id='" & field_name & "' class='select' " & field_status & ">" & _
                       "<option value=''>" & opt_header & "</option>"

        while not objOpts.EOF
            if objOpts("opt_disabled") = false or cstr(myStatus) = cstr(objOpts("opt_id")) then     
                if cstr(myStatus) = cstr(objOpts("opt_id")) then
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "' selected>" & objOpts("opt_value") & "</option>"
                else
                    response.write "<option value='" & objOpts("opt_id") & "' title='" & objOpts("opt_description") & "'>" & objOpts("opt_value") & "</option>"
                end if
            end if
            objOpts.movenext
        wend

        response.write "</select>"

    end if
    set objOpts = nothing
    

end sub


sub validate_security_code(var_security_code)

    session("ACC_SECURITY_CODE_ENTERED") = ""

    if isnull(var_security_code) or len(var_security_code) = 0 then var_security_code = ""

    if var_security_code = session("ACC_SECURITY_CODE") then
        response.write "True"
        session("ACC_SECURITY_CODE_ENTERED") = "Y"
    else
        response.write "False"
    end if

end sub



'********************************************************************************************************************************************************
'********************************************************************************************************************************************************
'**                                                                                                                                                    **
'**                                     IMPORT DETAILS FROM AN EXTERNAL SOURCE (API CONNECTORS)                                                        **
'**                                                                                                                                                    **
'********************************************************************************************************************************************************
'********************************************************************************************************************************************************


sub acc_HR_Lookup_display_popup(accb_code, var_module)

    response.write "<div id='blanket' style='display:none;'></div>"

    response.write "<!--********START OF IMPORT LOOKUP POPUP******-->" & _
                   "<div id='popUpHRImport' style='display:none;  margin-top: auto; margin-bottom: auto; width: 540px; min-height: 280px; max-height: 600px; overflow:auto; border: 3px solid #C00;' class='c'>" & _
                   "<div id='windowtitle'>" & _
                   "<h5>Select criteria to look up</h5>" & _
                   "<div id='windowdrop'>" & _
                   "&nbsp;" & _
                   "</div>" & _
                   "<div id='windowmenu'>" & _
                   "<span><a href='javascript:popup(""popUpHRImport"")'>Close Lookup Tool</a></span>" & _
                   "</div>" & _
                   "<div id='windowbody' class='pad'>" & _
                   "<div class='sectionarea c'>"

    response.write "<span class='c'>Please provide as much information as possible to reduce the number of names returned</span>"

    response.write "<table width='100%' cellpadding='2px' cellspacing='1px' class='showtd'>" & _
                   "<tr><th width='80%'>ID/Reference Number Lookup</th><td width='20%'><input type='text' class='text' name='popup_frm_ref' id='popup_frm_ref' /></td></tr>" & _
                   "<tr><th>First Name Lookup</th><td><input type='text' class='text' name='popup_frm_fname' id='popup_frm_fname' /></td></tr>" & _
                   "<tr><th>Last Name Lookup</th><td><input type='text' class='text' name='popup_frm_sname' id='popup_frm_sname' /></td></tr>"

    if session("ACC_ACCIDENT_SCHOOL") = "Y" then
        response.write "<tr><th>Year Group</th><td><input type='text' class='text' name='popup_frm_yeargroup' id='popup_frm_yeargroup' /></td></tr>" & _
                       "<tr><th>Registration Class</th><td><input type='text' class='text' name='popup_frm_reggroup' id='popup_frm_reggroup' /></td></tr>"
    end if

    response.write "</table>"

    response.write "<div class='r'><input type='submit' class='submit' name='popup_frm_submit' value='Search' onclick='reload_HR_Lookup_Data(""" & accb_code & """, """ & var_module & """)' /></div>"

    response.write "<div id='div_hr_lookup_results'>&nbsp;</div>"

    response.write "</div>" & _
                   "</div>" & _
                   "</div>" & _
                   "</div>" & _
                   "<!--********END OF IMPORT LOOKUP POPUP******-->"

end sub

sub acc_HR_Lookup_process_popup(accb_code, var_module, popup_ref, popup_fname, popup_sname, popup_yeargroup, popup_reggroup)

    'depending on the contract, the lookup will take place on a local table which will be updated on a specified interval 
    'through a different process, or from an API call to a client DB. Each lookup will need writing for each client directly.
    'The results will be output in the same format for each client and copied into the forms in the same manner.

    select case session("CORP_CODE")

        case "065925", "222999" ' Acadamies Enterprise Trust

            'look up the schools ID reference from the structure information
            objCommand.commandText = "SELECT tier3.internal_reference FROM " & Application("DBTABLE_STRUCT_TIER3") & " AS tier3 " & _
                                     "INNER JOIN " & Application("DBTABLE_HR_DATA_ABOOK") & " AS abooks " & _
                                     "  ON tier3.corp_code = abooks.corp_code " & _
                                     "  AND tier3.tier3_ref = abooks.rec_bl " & _
                                     "WHERE abooks.corp_code = '" & session("CORP_CODE") & "' " & _
                                     "  AND abooks.accb_code = '" & accb_code & "' " & _
                                     "  AND tier3.internal_reference IS NOT NULL"
            set objRsBook = objCommand.execute
            if not objRsBook.EOF then
                var_school_id_ref = objRsBook("internal_reference")
            else
                var_school_id_ref = ""
            end if
            set objRsBook = nothing

            if var_school_id_ref <> "" then

                var_sql_querystring = "corp_code = '" & session("CORP_CODE") & "' AND CAST(school_id AS nvarchar(50)) = '" & var_school_id_ref & "' AND recordStatus = 1"

                if len(popup_ref) > 0 then
                    if len(var_sql_querystring) > 0 then var_sql_querystring = var_sql_querystring & " AND "
                    var_sql_querystring = "mis_id = '" & popup_ref & "'"
                end if
                if len(popup_fname) > 0 then
                    if len(var_sql_querystring) > 0 then var_sql_querystring = var_sql_querystring & " AND "
                    var_sql_querystring = var_sql_querystring & "(legalForename = '" & popup_fname & "' OR preferredForename = '" & popup_fname & "')"
                end if
                if len(popup_sname) > 0 then
                    if len(var_sql_querystring) > 0 then var_sql_querystring = var_sql_querystring & " AND "
                    var_sql_querystring = var_sql_querystring & "(legalSurname = '" & popup_sname & "' OR preferredSurname = '" & popup_sname & "')"
                end if
                if len(popup_yeargroup) > 0 then
                    if len(var_sql_querystring) > 0 then var_sql_querystring = var_sql_querystring & " AND "
                    var_sql_querystring = var_sql_querystring & "year = '" & popup_yeargroup & "'"
                end if
                if len(popup_reggroup) > 0 then
                    if len(var_sql_querystring) > 0 then var_sql_querystring = var_sql_querystring & " AND "
                    var_sql_querystring = var_sql_querystring & "reg = '" & popup_reggroup & "'"
                end if

                objCommand.commandText = "SELECT mis_id, legalForename + ' ' + legalSurname AS per_fullname, streetNo + ' ' + street AS addressLine1 " & _
                                         "FROM " & "Module_API.dbo.API_Data_AET_Stored_Data" & " " & _
                                         "WHERE " & var_sql_querystring & " " & _
                                         "ORDER BY legalSurname ASC"
                set objRsLookup = objCommand.execute
                if not objRsLookup.EOF then            
                    var_found = true

                    while not objRsLookup.EOF
                        recordCounter = recordCounter + 1
                        objRsLookup.movenext
                    wend
                    objRsLookup.movefirst

                    if recordCounter > 5 then
                        var_div_style = "height: 120px; overflow-y: scroll;"
                    end if

                    response.write "<div style='padding-top:15px;" & var_div_style & "'>" & _
                                   "<table width='100%' cellpadding='2px' cellspacing='1px' class='showtd'>" & _
                                   "<tr><th>Reference</th><th>Name</th><th>First line of address</th></tr>"

                    while not objRsLookup.EOF
                        response.write "<tr>" & _
                                       "<td><a href='javascript:retrieve_HR_Lookup_Data(""" & objRsLookup("mis_id") & """, """ & var_module & """)'>" & objRsLookup("mis_id") & "</a></td>" & _
                                       "<td><a href='javascript:retrieve_HR_Lookup_Data(""" & objRsLookup("mis_id") & """, """ & var_module & """)'>" & objRsLookup("per_fullname") & "</a></td>" & _
                                       "<td><a href='javascript:retrieve_HR_Lookup_Data(""" & objRsLookup("mis_id") & """, """ & var_module & """)'>" & objRsLookup("addressLine1") & "</a></td>" & _
                                       "</tr>"

                        objRsLookup.movenext
                    wend

                    response.write "</table>" & _
                                   "</div>"
                else
                    var_found = false
                end if
                set objRsLookup = nothing
            else
                var_found = false
            end if


        case "065912" ' Ocado
            var_found = false

'set xmlDoc = createObject("MSXML2.DOMDocument")
'xmlDoc.async = False
'xmlDoc.setProperty "ServerHTTPRequest", true
'xmlDoc.load("http://somesite.com/api/xml?action=login&login=jon@doe.com&password=foobar")
'response.write xmlDoc.selectSingleNode("//results/status").Attributes.GetNamedItem("code").Text


        case "222999" ' RISEX DEV
            var_found = true

        case else
            var_found = false

    end select


    if var_found = false then
        response.write "<div class='warning'><table><tr><td><p>No records have been found matching your criteria. Please try again or return to the form to enter the persons details manually</p></td></tr></table></div>"
    end if

end sub


sub acc_HR_Lookup_import_details(selected_record_id, var_module)

    'select record out of temporary database table
    'insert values into hidden fields
    'run javascript to copy hidden field contents into base form
    'close popup

    select case session("CORP_CODE")
        case "065925", "222999" 'Acadamies Enterprise Trust

            objCommand.commandText = "SELECT * " & _
                                     "FROM " & "Module_API.dbo.API_Data_AET_Stored_Data" & " " & _
                                     "WHERE mis_id = '" & selected_record_id & "' "
            set objRsLookup = objCommand.execute
            if not objRsLookup.EOF then            
                var_per_fname = objRsLookup("legalForename")
                var_per_mname = objRsLookup("middleNames")
                var_per_sname = objRsLookup("legalSurname")
                var_per_address1 = objRsLookup("streetNo") & " " & objRsLookup("street")
                var_per_address2 = objRsLookup("town")
                var_per_address3 = objRsLookup("county")
                var_per_postcode = objRsLookup("postcode")
                    
                if len(objRsLookup("dob")) > 0 then
                    var_per_age = datediff("yyyy", objRsLookup("dob"), now())
                else
                    var_per_age = objRsLookup("age")
                end if

                if objRsLookup("classification") = "1" then  'student
                    var_per_status = "7"
                    var_per_jtitle = "Student"

                    objCommand.commandText = "SELECT opt_id FROM " & Application("DBTABLE_HR_ACC_OPTS") & " " & _
                                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                                             "  AND related_to = 'acc_sch_year' " & _
                                             "  AND opt_value = '" & objRsLookup("year") & "'"
                    set objRsLookupYear = objCommand.execute
                    if not objRsLookupYear.EOF then
                        var_per_yeargroup = objRsLookupYear("opt_id")
                    else
                        var_per_yeargroup = ""
                    end if
                    set objRsLookupYear = nothing

                    var_per_reggroup = objRsLookup("reg")

                elseif objRsLookup("classification") = "2" then  'staff
                    var_per_status = "8"
                    var_per_jtitle = objRsLookup("occupation")

                    var_per_yeargroup = "0"
                end if

                if objRsLookup("gender") = "M" then
                    var_per_gender = "0"
                else
                    var_per_gender = "1"
                end if

            end if
            set objRsLookup = nothing

    end select

    response.write "<div style='display:none'>"
    
    response.write "<input type='text' name='frm_temp_popup_fname' id='frm_temp_popup_fname' value='" & var_per_fname & "' />"
    response.write "<input type='text' name='frm_temp_popup_sname' id='frm_temp_popup_sname' value='" & var_per_sname & "' />"
    response.write "<input type='text' name='frm_temp_popup_jtitle' id='frm_temp_popup_jtitle' value='" & var_per_jtitle & "' />"
    response.write "<input type='text' name='frm_temp_popup_idref' id='frm_temp_popup_idref' value='' />"
    response.write "<input type='text' name='frm_temp_popup_add1' id='frm_temp_popup_add1' value='" & var_per_address1 & "' />"
    response.write "<input type='text' name='frm_temp_popup_add2' id='frm_temp_popup_add2' value='" & var_per_address2 & "' />"
    response.write "<input type='text' name='frm_temp_popup_add3' id='frm_temp_popup_add3' value='" & var_per_address3 & "' />"
    response.write "<input type='text' name='frm_temp_popup_pcode' id='frm_temp_popup_pcode' value='" & var_per_postcode & "' />"
    response.write "<input type='text' name='frm_temp_popup_phone' id='frm_temp_popup_phone' value='" & var_per_phone & "' />"
    response.write "<input type='text' name='frm_temp_popup_email' id='frm_temp_popup_email' value='" & var_per_email & "' />"
    response.write "<input type='text' name='frm_temp_popup_gen' id='frm_temp_popup_gender' value='" & var_per_gender & "' />"
    response.write "<input type='text' name='frm_temp_popup_age' id='frm_temp_popup_age' value='" & var_per_age & "' />"
    response.write "<input type='text' name='frm_temp_popup_status' id='frm_temp_popup_status' value='" & var_per_status & "' />"

    if session("ACC_ACCIDENT_SCHOOL") = "Y" then
        response.write "<input type='text' name='frm_temp_popup_yeargroup' id='frm_temp_popup_yeargroup' value='" & var_per_yeargroup & "' />"
        response.write "<input type='text' name='frm_temp_popup_reggroup' id='frm_temp_popup_reggroup' value='" & var_per_reggroup & "' />"
    end if

    response.write "</div>"

end sub


'********************************************************************************************************************************************************
'********************************************************************************************************************************************************

%>