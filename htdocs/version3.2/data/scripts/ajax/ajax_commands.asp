﻿<!-- #INCLUDE FILE="../../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../../data/scripts/inc/action_control.asp" -->
<!-- #INCLUDE file="../../../data/functions/func_random.asp" -->
<!-- #INCLUDE FILE="../../../data/scripts/inc/autogen_datetime_v2.inc" -->
<!-- #INCLUDE FILE="../../../data/scripts/inc/autogen_menus.asp" -->
<!-- #INCLUDE FILE="../../../data/functions/func_random.asp" -->
<!-- #INCLUDE FILE="../../../modules/hsafety/permit_to_work_v2/app/scripts/inc/ptw_scripts.asp" -->
<!-- #INCLUDE FILE="ajax_functions.asp" -->
<%
DIM Objrs
DIM Objconn
DIM Objcommand
CALL Startconnections()

'fix for API calls
if len(request("cc")) > 0 then
    session("CORP_CODE") = server.htmlencode(request("cc"))
end if



select case request("cmd") 

    case "load_acc_signoff_v2"
        call load_acc_signoff_v2(request("accb_code"), request("recordreference"), request("allow_signoff"))

    case "five_whys_load_actions"
        call five_whys_load_actions(request("accb_code"), request("recordreference"), request("parent_id"), request("action_id"), request("levid"))

    case "five_whys_remove_action"
        call five_whys_remove_action(request("accb_code"), request("recordreference"), request("parent_id"), request("action_id"), request("action_ref"))

    case "five_whys_edit_action"
        call five_whys_edit_action(request("accb_code"), request("recordreference"), request("parent_id"), request("action_id"), request("action_ref"), request("levid"))

    case "five_whys_count_actions"
        call five_whys_count_actions(request("accb_code"), request("recordreference"), request("parent_id"), request("action_id"))

    case "lookup_hrdb_data"
        call acc_HR_Lookup_process_popup(request("accb"), request("module"), request("ref1"), request("ref2"), request("fname"), request("sname"), request("ygroup"), request("rgroup"))

    case "retrieve_hrdb_data"
        call acc_HR_Lookup_import_details(request("id"), request("module"))

    case "validate_security_code"
        call validate_security_code(request("securitycode"))

    '******************Permit to work *********************************

    case "ptw_showCurrentType"
        call ptw_showCurrentType(request("temp_ref"))

    case "ptw_showDropDownType"
        call ptw_showDropDownType(request("temp_ref"), request("disabled"))

    case "ptw_AddRemType"
        call ptw_AddRemType(request("temp_ref"), request("opt_id"), request("switch"))

    case "count_permit_attachments"
        call count_permit_attachments(request("ptw_ref"), request("temp_ref"))

    case "add_permit_tabular_data"
        call add_permit_tabular_data(request("ptw_ref"), request("temp_ref"), request("field_ref"), request("row"), request("ext"))

    case "add_permit_tabular_row"
        call add_permit_tabular_row(request("ptw_ref"), request("temp_ref"), request("field_ref"), request("ext"), request("addrowonly"))

    '************** Risk Assessment Process Control (IKEA Dev). *****************************

    case "generate_hazards"
        call populateHazardDiv(request("recordreference"), request("hazid"))

    case "addHazardAction"
        call addHazardAction(request("recordreference"), request("hazid"), request("actid"))

    case "remHazardAction"
        call remHazardAction(request("recordreference"), request("hazid"), request("actid"))

    Case "dataHazardAction"
        call dataHazardAction(request("recordreference"), request("hazid"), request("actid"),request("ra_frm_s3c_hazcontrol_2"),request("ra_frm_s3c_duedate_d"),request("ra_frm_s3c_duedate_m"),request("ra_frm_s3c_duedate_y"), request("frmPriorityName1_used"), request("ra_frm_s3c_hazowner"), request("ra_frm_s3c_hazowner_email"))
    
    case "showPotentialRating"
        call showPotentialRating(request("recordreference"), request("hazid"))

    case "savePotentialRating"
        call savePotentialRating(request("recordreference"), request("hazid"), request("ra_frm_s3d_soh_2"), request("ra_frm_s3d_loh_2"))

    case "changeRiskReport"
        call changeRiskReport(request("recordreference"), request("hazid"))

    '*****************************************************************************************

    case "managePortalDomain"
        call managePortalDomain(request("portal_id"), request("domain_id"), request("subcmd"))

    case else
        response.write "Error - No command"
end select


CALL Endconnections()



 %> 