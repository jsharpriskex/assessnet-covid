
function position_HR_Lookup(module) {
    if (module == "VI_PERP") {
        document.getElementById("popUpHRImport").style.top = "850px";
    }
}

function reload_HR_Lookup_Data(corp_code, accb_ref, module) {

    document.getElementById("div_hr_lookup_waiting").style.display = "block";

    var popup_frm_ref1;
    var popup_frm_fname;
    var popup_frm_sname;
    var popup_frm_yeargroup;
    var popup_frm_reggroup;

    popup_frm_ref1 = document.getElementById("popup_frm_ref1").value;
    popup_frm_fname = document.getElementById("popup_frm_fname").value;
    popup_frm_sname = document.getElementById("popup_frm_sname").value;
    if (document.getElementById("popup_frm_yeargroup")) {
        popup_frm_yeargroup = document.getElementById("popup_frm_yeargroup").value;
    } else {
        popup_frm_yeargroup = "";
    }
    if (document.getElementById("popup_frm_reggroup")) {
        popup_frm_reggroup = document.getElementById("popup_frm_reggroup").value;
    } else {
        popup_frm_reggroup = "";
    }

    var offset = "";
    if (module == "VI_VICTIM" || module == "VI_PERP" || module == "DEF") {
        offset = "../";
    }

    // && popup_frm_yeargroup.length == 0 && popup_frm_reggroup.length == 0

    if (popup_frm_ref1.length == 0 && popup_frm_fname.length == 0 && popup_frm_sname.length == 0) {
        alert("Please enter a value to search by");
    } else {
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("div_hr_lookup_results").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("POST", offset + "../../../../data/scripts/ajax/ajax_commands.asp?cmd=lookup_hrdb_data&cc=" + corp_code + "&accb=" + accb_ref + "&module=" + module + "&ref1=" + popup_frm_ref1 + "&ref2=&fname=" + popup_frm_fname + "&sname=" + popup_frm_sname + "&ygroup=" + popup_frm_yeargroup + "&rgroup=" + popup_frm_reggroup, false);
        xmlhttp.send();
    }

    document.getElementById("div_hr_lookup_waiting").style.display = "none";
}


function retrieve_HR_Lookup_Data(selected_record_id, module) {

    var offset = ""
    if (module == "VI_VICTIM" || module == "VI_PERP" || module == "DEF") {
        offset = "../"
    }

    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }
    else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("div_hr_lookup_results").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.open("POST", offset + "../../../../data/scripts/ajax/ajax_commands.asp?cmd=retrieve_hrdb_data&id=" + selected_record_id + "&module=" + module, false);
    xmlhttp.send();


    // copy selected content into the main form
    copy_HR_Lookup_Data(module);
}


function copy_HR_Lookup_Data(module) {

    if (module == "INJ") {

        if (document.getElementById("INJFNAME") && document.getElementById("frm_temp_popup_fname")) {
            document.getElementById("INJFNAME").value = document.getElementById("frm_temp_popup_fname").value;
        }
        if (document.getElementById("INJSNAME") && document.getElementById("frm_temp_popup_sname")) {
            document.getElementById("INJSNAME").value = document.getElementById("frm_temp_popup_sname").value;
        }
        if (document.getElementById("INJOCCUPATION") && document.getElementById("frm_temp_popup_jtitle")) {
            document.getElementById("INJOCCUPATION").value = document.getElementById("frm_temp_popup_jtitle").value;
        }

        if (document.getElementById("INJADDRESSBULK")) {
            document.getElementById("INJADDRESSBULK").value = document.getElementById("frm_temp_popup_add1").value + "\n" + document.getElementById("frm_temp_popup_add2").value + "\n" + document.getElementById("frm_temp_popup_add3").value;
            document.getElementById("frm_inj_per_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
        } else {
            if (document.getElementById("INJADDRESS1") && document.getElementById("frm_temp_popup_add1")) {
                document.getElementById("INJADDRESS1").value = document.getElementById("frm_temp_popup_add1").value;
            }
            if (document.getElementById("INJADDRESS2") && document.getElementById("frm_temp_popup_add2")) {
                document.getElementById("INJADDRESS2").value = document.getElementById("frm_temp_popup_add2").value;
            }
            if (document.getElementById("INJADDRESS3") && document.getElementById("frm_temp_popup_add3")) {
                document.getElementById("INJADDRESS3").value = document.getElementById("frm_temp_popup_add3").value;
            }
            if (document.getElementById("frm_inj_per_postcode") && document.getElementById("frm_temp_popup_pcode")) {
                document.getElementById("frm_inj_per_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
            }
        }

        if (document.getElementById("INJ_ADV_CONTACT_PHONE") && document.getElementById("frm_temp_popup_phone")) {
            document.getElementById("INJ_ADV_CONTACT_PHONE").value = document.getElementById("frm_temp_popup_phone").value;
        }
        if (document.getElementById("INJ_ADV_CONTACT_EMAIL") && document.getElementById("frm_temp_popup_email")) {
            document.getElementById("INJ_ADV_CONTACT_EMAIL").value = document.getElementById("frm_temp_popup_email").value;
        }
        if (document.getElementById("INJ_ADV_EXTRA") && document.getElementById("frm_temp_popup_idref")) {
            if (document.getElementById("frm_temp_popup_idref").value.length > 0) {
                document.getElementById("INJ_ADV_EXTRA").value = document.getElementById("frm_temp_popup_idref").value;
            }
        }

        if (document.getElementById("INJAGE") && document.getElementById("frm_temp_popup_age")) {
            document.getElementById("INJAGE").value = document.getElementById("frm_temp_popup_age").value;
        }
        if (document.getElementById("acc_age_dob_d") && document.getElementById("frm_temp_popup_age_dd")) {
            document.getElementById("acc_age_dob_d").value = document.getElementById("frm_temp_popup_age_dd").value;
        }
        if (document.getElementById("acc_age_dob_m") && document.getElementById("frm_temp_popup_age_mm")) {
            document.getElementById("acc_age_dob_m").value = document.getElementById("frm_temp_popup_age_mm").value;
        }
        if (document.getElementById("acc_age_dob_y") && document.getElementById("frm_temp_popup_age_yyyy")) {
            document.getElementById("acc_age_dob_y").value = document.getElementById("frm_temp_popup_age_yyyy").value;
        }

        if (document.getElementById("INJGENDER") && document.getElementById("frm_temp_popup_gender")) {
            document.getElementById("INJGENDER").value = document.getElementById("frm_temp_popup_gender").value;
        }

        if (document.getElementById("INJSTATUS") && document.getElementById("frm_temp_popup_status")) {
            document.getElementById("INJSTATUS").value = document.getElementById("frm_temp_popup_status").value;
        }

        if (document.getElementById("INJYEAR") && document.getElementById("frm_temp_popup_yeargroup")) {
            document.getElementById("INJYEAR").value = document.getElementById("frm_temp_popup_yeargroup").value;
            if (document.getElementById("INJYEAR").value > 0) {
                document.getElementById("INJREG").disabled = false;
            } else {
                document.getElementById("INJREG").disabled = true;
            }
        }
        if (document.getElementById("INJREG") && document.getElementById("frm_temp_popup_reggroup")) {
            document.getElementById("INJREG").value = document.getElementById("frm_temp_popup_reggroup").value;
        }

    } else if (module == "ID") {

        if (document.getElementById("IDFNAME") && document.getElementById("frm_temp_popup_fname")) {
            document.getElementById("IDFNAME").value = document.getElementById("frm_temp_popup_fname").value;
        }
        if (document.getElementById("IDSNAME") && document.getElementById("frm_temp_popup_sname")) {
            document.getElementById("IDSNAME").value = document.getElementById("frm_temp_popup_sname").value;
        }
        if (document.getElementById("IDOCCUPATION") && document.getElementById("frm_temp_popup_jtitle")) {
            document.getElementById("IDOCCUPATION").value = document.getElementById("frm_temp_popup_jtitle").value;
        }

        if (document.getElementById("IDADDRESSBULK")) {
            document.getElementById("IDADDRESSBULK").value = document.getElementById("frm_temp_popup_add1").value + "\n" + document.getElementById("frm_temp_popup_add2").value + "\n" + document.getElementById("frm_temp_popup_add3").value;
            document.getElementById("frm_id_per_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
        } else {
            if (document.getElementById("IDADDRESS1") && document.getElementById("frm_temp_popup_add1")) {
                document.getElementById("IDADDRESS1").value = document.getElementById("frm_temp_popup_add1").value;
            }
            if (document.getElementById("IDADDRESS2") && document.getElementById("frm_temp_popup_add2")) {
                document.getElementById("IDADDRESS2").value = document.getElementById("frm_temp_popup_add2").value;
            }
            if (document.getElementById("IDADDRESS3") && document.getElementById("frm_temp_popup_add3")) {
                document.getElementById("IDADDRESS3").value = document.getElementById("frm_temp_popup_add3").value;
            }
            if (document.getElementById("IDPOSTCODE") && document.getElementById("frm_temp_popup_pcode")) {
                document.getElementById("IDPOSTCODE").value = document.getElementById("frm_temp_popup_pcode").value;
            }
        }

        if (document.getElementById("ID_ADV_CONTACT_PHONE") && document.getElementById("frm_temp_popup_phone")) {
            document.getElementById("ID_ADV_CONTACT_PHONE").value = document.getElementById("frm_temp_popup_phone").value;
        }
        if (document.getElementById("ID_ADV_CONTACT_EMAIL") && document.getElementById("frm_temp_popup_email")) {
            document.getElementById("ID_ADV_CONTACT_EMAIL").value = document.getElementById("frm_temp_popup_email").value;
        }
        if (document.getElementById("ID_ADV_EXTRA") && document.getElementById("frm_temp_popup_idref")) {
            if (document.getElementById("frm_temp_popup_idref").value.length > 0) {
                document.getElementById("ID_ADV_EXTRA").value = document.getElementById("frm_temp_popup_idref").value;
            }
        }

        if (document.getElementById("IDAGE") && document.getElementById("frm_temp_popup_age")) {
            document.getElementById("IDAGE").value = document.getElementById("frm_temp_popup_age").value;
        }

        if (document.getElementById("IDGENDER") && document.getElementById("frm_temp_popup_gender")) {
            document.getElementById("IDGENDER").value = document.getElementById("frm_temp_popup_gender").value;
        }

        if (document.getElementById("IDSTATUS") && document.getElementById("frm_temp_popup_status")) {
            document.getElementById("IDSTATUS").value = document.getElementById("frm_temp_popup_status").value;
        }

        if (document.getElementById("IDYEAR") && document.getElementById("frm_temp_popup_yeargroup")) {
            document.getElementById("IDYEAR").value = document.getElementById("frm_temp_popup_yeargroup").value;
            if (document.getElementById("IDYEAR").value > 0) {
                document.getElementById("IDREG").disabled = false;
            } else {
                document.getElementById("IDREG").disabled = true;
            }
        }
        if (document.getElementById("IDREG") && document.getElementById("frm_temp_popup_reggroup")) {
            document.getElementById("IDREG").value = document.getElementById("frm_temp_popup_reggroup").value;
        }

    } else if (module == "VI_VICTIM") {

        if (document.getElementById("acc_frm_vi_victim")) {
            document.getElementById("acc_frm_vi_victim").value = "man";
        }

        if (document.getElementById("acc_frm_vi_victim_fname") && document.getElementById("frm_temp_popup_fname")) {
            document.getElementById("acc_frm_vi_victim_fname").value = document.getElementById("frm_temp_popup_fname").value;
        }
        if (document.getElementById("acc_frm_vi_victim_sname") && document.getElementById("frm_temp_popup_sname")) {
            document.getElementById("acc_frm_vi_victim_sname").value = document.getElementById("frm_temp_popup_sname").value;
        }
        if (document.getElementById("acc_frm_vi_victim_occupation") && document.getElementById("frm_temp_popup_jtitle")) {
            document.getElementById("acc_frm_vi_victim_occupation").value = document.getElementById("frm_temp_popup_jtitle").value;
        }

        if (document.getElementById("acc_frm_vi_victim_address_bulk")) {
            document.getElementById("acc_frm_vi_victim_address_bulk").value = document.getElementById("frm_temp_popup_add1").value + "\n" + document.getElementById("frm_temp_popup_add2").value + "\n" + document.getElementById("frm_temp_popup_add3").value;
            document.getElementById("acc_frm_vi_victim_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
        } else {
            if (document.getElementById("acc_frm_vi_victim_address1") && document.getElementById("frm_temp_popup_add1")) {
                document.getElementById("acc_frm_vi_victim_address1").value = document.getElementById("frm_temp_popup_add1").value;
            }
            if (document.getElementById("acc_frm_vi_victim_address2") && document.getElementById("frm_temp_popup_add2")) {
                document.getElementById("acc_frm_vi_victim_address2").value = document.getElementById("frm_temp_popup_add2").value;
            }
            if (document.getElementById("acc_frm_vi_victim_address3") && document.getElementById("frm_temp_popup_add3")) {
                document.getElementById("acc_frm_vi_victim_address3").value = document.getElementById("frm_temp_popup_add3").value;
            }
            if (document.getElementById("acc_frm_vi_victim_postcode") && document.getElementById("frm_temp_popup_pcode")) {
                document.getElementById("acc_frm_vi_victim_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
            }
        }

        if (document.getElementById("acc_frm_vi_victim_adv_contact_phone") && document.getElementById("frm_temp_popup_phone")) {
            document.getElementById("acc_frm_vi_victim_adv_contact_phone").value = document.getElementById("frm_temp_popup_phone").value;
        }
        if (document.getElementById("acc_frm_vi_victim_adv_contact_email") && document.getElementById("frm_temp_popup_email")) {
            document.getElementById("acc_frm_vi_victim_adv_contact_email").value = document.getElementById("frm_temp_popup_email").value;
        }
        if (document.getElementById("acc_frm_vi_victim_adv_extra") && document.getElementById("frm_temp_popup_idref")) {
            if (document.getElementById("frm_temp_popup_idref").value.length > 0) {
                document.getElementById("acc_frm_vi_victim_adv_extra").value = document.getElementById("frm_temp_popup_idref").value;
            }
        }

        if (document.getElementById("acc_frm_vi_victim_age") && document.getElementById("frm_temp_popup_age")) {
            document.getElementById("acc_frm_vi_victim_age").value = document.getElementById("frm_temp_popup_age").value;
        }

        if (document.getElementById("acc_frm_vi_victim_gender") && document.getElementById("frm_temp_popup_gender")) {
            document.getElementById("acc_frm_vi_victim_gender").value = document.getElementById("frm_temp_popup_gender").value;
        }

        if (document.getElementById("acc_frm_vi_victim_status") && document.getElementById("frm_temp_popup_status")) {
            document.getElementById("acc_frm_vi_victim_status").value = document.getElementById("frm_temp_popup_status").value;
        }

        if (document.getElementById("acc_frm_vi_victim_year") && document.getElementById("frm_temp_popup_yeargroup")) {
            document.getElementById("acc_frm_vi_victim_year").value = document.getElementById("frm_temp_popup_yeargroup").value;
            if (document.getElementById("acc_frm_vi_victim_year").value > 0) {
                document.getElementById("acc_frm_vi_victim_reg").disabled = false;
            } else {
                document.getElementById("acc_frm_vi_victim_reg").disabled = true;
            }
        }
        if (document.getElementById("acc_frm_vi_victim_reg") && document.getElementById("frm_temp_popup_reggroup")) {
            document.getElementById("acc_frm_vi_victim_reg").value = document.getElementById("frm_temp_popup_reggroup").value;
        }

    } else if (module == "VI_PERP") {

        if (document.getElementById("acc_frm_vi_perp")) {
            document.getElementById("acc_frm_vi_perp").value = "man";
        }

        if (document.getElementById("acc_frm_vi_perp_fname") && document.getElementById("frm_temp_popup_fname")) {
            document.getElementById("acc_frm_vi_perp_fname").value = document.getElementById("frm_temp_popup_fname").value;
        }
        if (document.getElementById("acc_frm_vi_perp_sname") && document.getElementById("frm_temp_popup_sname")) {
            document.getElementById("acc_frm_vi_perp_sname").value = document.getElementById("frm_temp_popup_sname").value;
        }
        if (document.getElementById("acc_frm_vi_perp_occupation") && document.getElementById("frm_temp_popup_jtitle")) {
            document.getElementById("acc_frm_vi_perp_occupation").value = document.getElementById("frm_temp_popup_jtitle").value;
        }

        if (document.getElementById("acc_frm_vi_perp_address_bulk")) {
            document.getElementById("acc_frm_vi_perp_address_bulk").value = document.getElementById("frm_temp_popup_add1").value + "\n" + document.getElementById("frm_temp_popup_add2").value + "\n" + document.getElementById("frm_temp_popup_add3").value;
            document.getElementById("acc_frm_vi_perp_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
        } else {
            if (document.getElementById("acc_frm_vi_perp_address1") && document.getElementById("frm_temp_popup_add1")) {
                document.getElementById("acc_frm_vi_perp_address1").value = document.getElementById("frm_temp_popup_add1").value;
            }
            if (document.getElementById("acc_frm_vi_perp_address2") && document.getElementById("frm_temp_popup_add2")) {
                document.getElementById("acc_frm_vi_perp_address2").value = document.getElementById("frm_temp_popup_add2").value;
            }
            if (document.getElementById("acc_frm_vi_perp_address3") && document.getElementById("frm_temp_popup_add3")) {
                document.getElementById("acc_frm_vi_perp_address3").value = document.getElementById("frm_temp_popup_add3").value;
            }
            if (document.getElementById("acc_frm_vi_perp_postcode") && document.getElementById("frm_temp_popup_pcode")) {
                document.getElementById("acc_frm_vi_perp_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
            }
        }

        if (document.getElementById("acc_frm_vi_perp_adv_contact_phone") && document.getElementById("frm_temp_popup_phone")) {
            document.getElementById("acc_frm_vi_perp_adv_contact_phone").value = document.getElementById("frm_temp_popup_phone").value;
        }
        if (document.getElementById("acc_frm_vi_perp_adv_contact_email") && document.getElementById("frm_temp_popup_email")) {
            document.getElementById("acc_frm_vi_perp_adv_contact_email").value = document.getElementById("frm_temp_popup_email").value;
        }
        if (document.getElementById("acc_frm_vi_perp_adv_extra") && document.getElementById("frm_temp_popup_idref")) {
            if (document.getElementById("frm_temp_popup_idref").value.length > 0) {
                document.getElementById("acc_frm_vi_perp_adv_extra").value = document.getElementById("frm_temp_popup_idref").value;
            }
        }

        if (document.getElementById("acc_frm_vi_perp_age") && document.getElementById("frm_temp_popup_age")) {
            document.getElementById("acc_frm_vi_perp_age").value = document.getElementById("frm_temp_popup_age").value;
        }

        if (document.getElementById("acc_frm_vi_perp_gender") && document.getElementById("frm_temp_popup_gender")) {
            document.getElementById("acc_frm_vi_perp_gender").value = document.getElementById("frm_temp_popup_gender").value;
        }

        if (document.getElementById("acc_frm_vi_perp_status") && document.getElementById("frm_temp_popup_status")) {
            document.getElementById("acc_frm_vi_perp_status").value = document.getElementById("frm_temp_popup_status").value;
        }

        if (document.getElementById("acc_frm_vi_perp_year") && document.getElementById("frm_temp_popup_yeargroup")) {
            document.getElementById("acc_frm_vi_perp_year").value = document.getElementById("frm_temp_popup_yeargroup").value;
            if (document.getElementById("acc_frm_vi_perp_year").value > 0) {
                document.getElementById("acc_frm_vi_perp_reg").disabled = false;
            } else {
                document.getElementById("acc_frm_vi_perp_reg").disabled = true;
            }
        }
        if (document.getElementById("acc_frm_vi_perp_reg") && document.getElementById("frm_temp_popup_reggroup")) {
            document.getElementById("acc_frm_vi_perp_reg").value = document.getElementById("frm_temp_popup_reggroup").value;
        }

    } else if (module == "DEF") {

        if (document.getElementById("frm_acc_def_fname") && document.getElementById("frm_temp_popup_fname")) {
            document.getElementById("frm_acc_def_fname").value = document.getElementById("frm_temp_popup_fname").value;
        }
        if (document.getElementById("frm_acc_def_sname") && document.getElementById("frm_temp_popup_sname")) {
            document.getElementById("frm_acc_def_sname").value = document.getElementById("frm_temp_popup_sname").value;
        }
        if (document.getElementById("frm_acc_def_occupation") && document.getElementById("frm_temp_popup_jtitle")) {
            document.getElementById("frm_acc_def_occupation").value = document.getElementById("frm_temp_popup_jtitle").value;
        }

        if (document.getElementById("frm_acc_def_address_bulk")) {
            document.getElementById("frm_acc_def_address_bulk").value = document.getElementById("frm_temp_popup_add1").value + "\n" + document.getElementById("frm_temp_popup_add2").value + "\n" + document.getElementById("frm_temp_popup_add3").value;
            document.getElementById("frm_acc_def_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
        } else {
            if (document.getElementById("frm_acc_def_address1") && document.getElementById("frm_temp_popup_add1")) {
                document.getElementById("frm_acc_def_address1").value = document.getElementById("frm_temp_popup_add1").value;
            }
            if (document.getElementById("frm_acc_def_address2") && document.getElementById("frm_temp_popup_add2")) {
                document.getElementById("frm_acc_def_address2").value = document.getElementById("frm_temp_popup_add2").value;
            }
            if (document.getElementById("frm_acc_def_address3") && document.getElementById("frm_temp_popup_add3")) {
                document.getElementById("frm_acc_def_address3").value = document.getElementById("frm_temp_popup_add3").value;
            }
            if (document.getElementById("frm_acc_def_postcode") && document.getElementById("frm_temp_popup_pcode")) {
                document.getElementById("frm_acc_def_postcode").value = document.getElementById("frm_temp_popup_pcode").value;
            }
        }

        if (document.getElementById("frm_acc_def_adv_contact_phone") && document.getElementById("frm_temp_popup_phone")) {
            document.getElementById("frm_acc_def_adv_contact_phone").value = document.getElementById("frm_temp_popup_phone").value;
        }
        if (document.getElementById("frm_acc_def_adv_contact_email") && document.getElementById("frm_temp_popup_email")) {
            document.getElementById("frm_acc_def_adv_contact_email").value = document.getElementById("frm_temp_popup_email").value;
        }
        if (document.getElementById("frm_acc_def_adv_extra") && document.getElementById("frm_temp_popup_idref")) {
            if (document.getElementById("frm_temp_popup_idref").value.length > 0) {
                document.getElementById("frm_acc_def_adv_extra").value = document.getElementById("frm_temp_popup_idref").value;
            }
        }

        if (document.getElementById("frm_acc_def_age") && document.getElementById("frm_temp_popup_age")) {
            document.getElementById("frm_acc_def_age").value = document.getElementById("frm_temp_popup_age").value;
        }

        if (document.getElementById("frm_acc_def_gender") && document.getElementById("frm_temp_popup_gender")) {
            document.getElementById("frm_acc_def_gender").value = document.getElementById("frm_temp_popup_gender").value;
        }

        if (document.getElementById("frm_acc_def_status") && document.getElementById("frm_temp_popup_status")) {
            document.getElementById("frm_acc_def_status").value = document.getElementById("frm_temp_popup_status").value;
        }

        if (document.getElementById("frm_acc_def_year") && document.getElementById("frm_temp_popup_yeargroup")) {
            document.getElementById("frm_acc_def_year").value = document.getElementById("frm_temp_popup_yeargroup").value;
            if (document.getElementById("frm_acc_def_year").value > 0) {
                document.getElementById("frm_acc_def_reg").disabled = false;
            } else {
                document.getElementById("frm_acc_def_reg").disabled = true;
            }
        }
        if (document.getElementById("frm_acc_def_reg") && document.getElementById("frm_temp_popup_reggroup")) {
            document.getElementById("frm_acc_def_reg").value = document.getElementById("frm_temp_popup_reggroup").value;
        }

    }

    // hide the popup and blanket so user can continue
    document.getElementById("blanket").style.display = "none";
    document.getElementById("popUpHRImport").style.display = "none";
}