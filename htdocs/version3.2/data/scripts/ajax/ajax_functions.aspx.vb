﻿Imports System.Net.Mail
Imports System.Net
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports accident
Imports user
Imports System.IO
Imports structure_roles
Imports System.Data
Imports Email
Imports System.Web.Script.Serialization
Imports System.Web

Partial Class ajax_functions
    Inherits System.Web.UI.Page

    Sub Page_load()

        Dim corp_code As String = Request("corp_code")
        Dim accbRef As String = Request("accbRef")
        Dim incidentRef As String = Request("incidentRef")

        Dim randomNumber As String = Request("RndNum")
        Dim uniqueQueryString As String = Request("qString")

        Dim personId1 As String = Request("id1")
        Dim personId2 As String = Request("id2")
        Dim personFname As String = Request("fname")
        Dim personSname As String = Request("sname")

        'only used for AET at present, not used in this script but included for reference
        Dim personYgroup As String = Request("ygroup")
        Dim personRclass As String = Request("rclass")

        Select Case Request("event")

            Case "API_HR_Lookup"
                Call api_hr_lookup(corp_code, randomNumber, personId1, personId2, personFname, personSname)

        End Select


    End Sub


    Sub api_hr_lookup(corp_code As String, randomCallValue As String, personId1 As String, personId2 As String, personFname As String, personSname As String)


        Try

            Dim apicall_query As String

            If Len(personId1) > 0 Then
                apicall_query = "/handscan-number/" & personId1.Trim()
            ElseIf Len(personId2) > 0 Then
                apicall_query = "/person-id/" & personId2.Trim()
            Else
                If Len(personFname) > 0 Then
                    apicall_query = "?query=" & personFname

                    If Len(personSname) > 0 Then
                        apicall_query = apicall_query & " " & personSname
                    End If
                ElseIf Len(personSname) > 0 Then
                    apicall_query = "?query=" & personSname
                End If
            End If

            'Response.Write(apicall_query & "...")
            'Response.End()

            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
            ServicePointManager.DefaultConnectionLimit = 9999

            'Dim apiRequest As HttpWebRequest = HttpWebRequest.Create("https://ocado-test.apigee.net/emcp/v1/people" & apicall_query)
            'Dim apiRequest As HttpWebRequest = HttpWebRequest.Create("https://ocado-prod.apigee.net/emcp-riskex/v1/people" & apicall_query)
            Dim apiRequest As HttpWebRequest = HttpWebRequest.Create("https://api.tech.lastmile.com/emcp-riskex/emcp-riskex/v1/people" & apicall_query)

            'apiRequest.Headers.Add("apikey", "Z5QOZNQ8YADie7G0ZGiwDeRRMR2XWGvG")
	    apiRequest.Headers.Add("X-Api-Key", "KKojZKezg3yRLsP14oLx8b2ub3N2Vfj1DMZGj6lh")
            apiRequest.Headers.Add("RequestId", randomCallValue)
            apiRequest.ContentType = "application/json"

            Dim apiResponse As HttpWebResponse = apiRequest.GetResponse()
            Dim apiEncoding As Encoding = Encoding.GetEncoding(1252)
            Dim apiStream As StreamReader = New StreamReader(apiResponse.GetResponseStream(), apiEncoding)

            'pull the API response stream into something 
            Dim jsonImport As String = apiStream.ReadToEnd()

            'correction for when only 1 record is returned from OCADO
            If left(jsonImport, 1) = "{" Then
                jsonImport = "[" & jsonImport & "]"
            End If


            Dim dbconn1 = New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
            dbconn1.Open()

            Dim sql_text As String
            Dim command As SqlCommand
            Dim adapter As New SqlDataAdapter

            Dim personSerializer = New JavaScriptSerializer()
            Dim person = personSerializer.Deserialize(Of List(Of PersonImport))(jsonImport)
            Dim p As PersonImport
            Dim a As PersonAddress

            For Each p In person
                a = p.personalAddress

                sql_text = "INSERT INTO Module_API.dbo.API_Data_OCADO_Stored_Data (corp_code, request_id, person_Id, handscan_id, title, legalForename, legalSurname, preferredForename, preferredSurname, maidenName, occupation, gender, dob, startDate, street, town, county, country, postcode, recordAdded) " &
                           "VALUES ('" & corp_code & "', '" & randomCallValue & "', '" & p.personId & "', " & p.handscanNumber & ", '" & Replace(p.title, "'", "`") & "', '" & Replace(p.personalName, "'", "`") & "', '" & Replace(p.familyName, "'", "`") & "', '" & Replace(p.personalName, "'", "`") & "', '" & Replace(p.familyName, "'", "`") & "', '" & Replace(p.maidenName, "'", "`") & "', '" & Replace(p.jobTitle, "'", "`") & "', '" & Replace(p.gender, "'", "`") & "', '" & p.dateOfBirth & "', '" & p.startDate & "', '" & a.addressLine1 & "', '" & a.town & "', '" & a.county & "', '" & a.country & "', '" & a.postalCode & "', GETDATE())"
                command = New SqlCommand(sql_text, dbconn1)
                adapter.InsertCommand = command
                adapter.InsertCommand.ExecuteNonQuery()
            Next p

            adapter.Dispose()
            dbconn1.Close()

            'Response.Write("<br /><br />END")

        Catch ex As Exception

            'Response.Write("<br /><br /><br />..." & ex.ToString())

            'Response.Write("<br /><br />END ERR")

        End Try

        Response.End()

    End Sub





    Public Class PersonImport
        Public Property personId() As String
            Get
                Return m_personId
            End Get
            Set(value As String)
                m_personId = value
            End Set
        End Property
        Private m_personId As String

        Public Property handscanNumber() As Integer
            Get
                Return m_handscanNumber
            End Get
            Set(value As Integer)
                m_handscanNumber = value
            End Set
        End Property
        Private m_handscanNumber As Integer

        Public Property title() As String
            Get
                Return m_title
            End Get
            Set(value As String)
                m_title = value
            End Set
        End Property
        Private m_title As String

        Public Property personalName() As String
            Get
                Return m_personalName
            End Get
            Set(value As String)
                m_personalName = value
            End Set
        End Property
        Private m_personalName As String

        Public Property familyName() As String
            Get
                Return m_familyName
            End Get
            Set(value As String)
                m_familyName = value
            End Set
        End Property
        Private m_familyName As String

        Public Property maidenName() As String
            Get
                Return m_maidenName
            End Get
            Set(value As String)
                m_maidenName = value
            End Set
        End Property
        Private m_maidenName As String

        Public Property gender() As String
            Get
                Return m_gender
            End Get
            Set(value As String)
                m_gender = value
            End Set
        End Property
        Private m_gender As String

        Public Property jobTitle() As String
            Get
                Return m_jobTitle
            End Get
            Set(value As String)
                m_jobTitle = value
            End Set
        End Property
        Private m_jobTitle As String

        Public Property dateOfBirth() As Date
            Get
                Return m_dateOfBirth
            End Get
            Set(value As Date)
                m_dateOfBirth = value
            End Set
        End Property
        Private m_dateOfBirth As Date

        Public Property startDate() As Date
            Get
                Return m_startDate
            End Get
            Set(value As Date)
                m_startDate = value
            End Set
        End Property
        Private m_startDate As Date

        Public Property personalAddress As PersonAddress
            Get
                Return m_personalAddress
            End Get
            Set(value As PersonAddress)
                m_personalAddress = value
            End Set
        End Property
        Private m_personalAddress As PersonAddress

    End Class



    Public Class PersonAddress
        Public Property addressLine1() As String
            Get
                Return m_addressLine1
            End Get
            Set(value As String)
                m_addressLine1 = value
            End Set
        End Property
        Private m_addressLine1 As String

        Public Property addressLine2() As String
            Get
                Return m_addressLine2
            End Get
            Set(value As String)
                m_addressLine2 = value
            End Set
        End Property
        Private m_addressLine2 As String

        Public Property addressLine3() As String
            Get
                Return m_addressLine3
            End Get
            Set(value As String)
                m_addressLine3 = value
            End Set
        End Property
        Private m_addressLine3 As String

        Public Property town() As String
            Get
                Return m_town
            End Get
            Set(value As String)
                m_town = value
            End Set
        End Property
        Private m_town As String

        Public Property county() As String
            Get
                Return m_county
            End Get
            Set(value As String)
                m_county = value
            End Set
        End Property
        Private m_county As String

        Public Property country() As String
            Get
                Return m_country
            End Get
            Set(value As String)
                m_country = value
            End Set
        End Property
        Private m_country As String

        Public Property postalCode() As String
            Get
                Return m_postalCode
            End Get
            Set(value As String)
                m_postalCode = value
            End Set
        End Property
        Private m_postalCode As String

    End Class

End Class
