<%@ Page Language="VB" Debug="true" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ import Namespace="System.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Drawing" %>
<%@ import Namespace="Winnovative.ExcelLib" %>

<script runat='server' language='vb'>



    ' ************************************************************
    ' ************************************************************
    '     Data missing from this export
    '     - Persons Contacted (additional options tab)
    '     - SPIRE Data
    '     - Harmful Behaviour Data
    '     - Environmental Data
    '     - Notes
    ' ************************************************************
    ' ************************************************************



    '**********Global vars*************

    Dim Var_corp_code As String
    Dim var_corp_security_code As String = ""
    Dim var_internal_security_code As String = "INVALID CODE"
    Dim var_output_type As String
    Dim var_output_name As String
    Dim var_output_method As String
    Dim var_restrict_locations As String
    Dim var_restrict_to_delta As String
    Dim var_user_ref As String

    Dim var_start_date AS String
    Dim var_end_date As String

    Dim var_cookie As String


    Dim var_corp_tier2 As String = ""
    Dim var_corp_tier3 As String = ""
    Dim var_corp_tier4 As String = ""
    Dim var_corp_tier5 As String = ""
    Dim var_corp_tier6 As String = ""

    Dim var_module_version As String = ""

    '*********end of global vars**********  

    ' get the Excel workbook format
    Public workbookFormat As ExcelWorkbookFormat = ExcelWorkbookFormat.Xlsx_2007
    ' create the workbook in the desired format with a single worksheet
    Public workbook As ExcelWorkbook = New ExcelWorkbook(workbookFormat)


    Public DefaultStyle As ExcelCellStyle = workbook.Styles.AddStyle("DefaultStyle")
    Public CourseStyle As ExcelCellStyle = workbook.Styles.AddStyle("CourseStyle")






    Function outputEntries(var_start_date, var_end_date, var_restrict_locations, var_restrict_to_delta, var_user_ref) As ExcelWorksheet
        Dim worksheet_entries As ExcelWorksheet

        Dim dbConn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn1.Open()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        ''''''''''''''''''''' Entries Starts Here ''''''''''''''''''''''''''''''
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        worksheet_entries = workbook.Worksheets.Item(0)
        worksheet_entries.Name = "Covid-S2D Entries"


        worksheet_entries.Item("A1").ColumnWidthInChars = 1

        'worksheet_entries.Item("B2:K2").Merge()
        'worksheet_entries.Item("b2").Value = "Covid Response Entries Export"
        'worksheet_entries.Item("b2").Style.Font.Size = 30
        'worksheet_entries.Item("B3:F3").Merge()
        'worksheet_entries.Item("b3").Value = "Data valid as of " & Date.Now
        'worksheet_entries.Item("b3").Style.Font.Size = 11


        Dim X As Integer = 1
        Dim Y As Integer = 1


        worksheet_entries.Item(X, Y).Value = "Firstname"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 25
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1
        worksheet_entries.Item(X, Y).Value = "Surname"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 15
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1
        worksheet_entries.Item(X, Y).Value = "Job Title"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 15
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Telephone Number"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Email Address"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Age Range"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1
        worksheet_entries.Item(X, Y).Value = "Account Created"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1
        worksheet_entries.Item(X, Y).Value = "Person Status"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = var_corp_tier2
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 27
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Case Reference"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Case Entry Created"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Case Status"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Case Category"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Case Archived"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Officially tested Positive"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Currently have symptoms"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Previously had symptoms and recovered"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Contacted NHS 111"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Date Symptoms First Occured"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Currently under isolation"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Number of people in household"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "People in household showing symptoms"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "People in household self isolating"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Feeling well enough to work"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Suffer from health conditions"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Means to take your temperature"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1

        worksheet_entries.Item(X, Y).Value = "Comments"
        worksheet_entries.Item(X, Y).Style = CourseStyle
        worksheet_entries.Item(X, Y).ColumnWidthInChars = 30
        worksheet_entries.Item(X, Y).RowHeightInPoints = 20
        Y = Y + 1



        Dim dateFormatString As String
        dateFormatString = "dd/mm/yyyy"


        Try
            Dim dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
            Using dbConn
                dbConn.Open()


                X = 1


                Dim sql_text As String = "EXEC Module_HA.dbo.usp_export_ham_data '" & Var_corp_code & "', '" & var_start_date & "', '" & var_end_date & "' " ', '" & var_restrict_locations & "', '" & var_restrict_to_delta & "', '" & var_user_ref & "' "

                'worksheet_entries.Item(1, 3).Value = sql_text


                Dim sqlComm As New SqlCommand(sql_text, dbConn)
                sqlComm.CommandTimeout = 180
                Dim objrs As SqlDataReader = sqlComm.ExecuteReader()
                While objrs.Read()

                    X = X + 1
                    Y = 1

                    worksheet_entries.Item(X, Y).Value = objrs("acc_firstname").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("acc_surname").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("acc_jobtitle").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("acc_contactnumber").ToString
                    worksheet_entries.Item(X, Y).ColumnWidthInChars = 50
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("acc_contactemail").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1


                    worksheet_entries.Item(X, Y).Value = objrs("acc_agerange").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("person_gendatetime").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    worksheet_entries.Item(X, Y).Style.Number.NumberFormatString = dateFormatString
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("person_status").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("tier2_name").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("case_reference").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("entry_gendatetime").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    worksheet_entries.Item(X, Y).Style.Number.NumberFormatString = dateFormatString
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("case_status").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("case_category").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("archive_status").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1


                    worksheet_entries.Item(X, Y).Value = objrs("q1").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q2").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q3").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q4").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q5").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    worksheet_entries.Item(X, Y).Style.Number.NumberFormatString = dateFormatString
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q6").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q7").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q8").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q9").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q10").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                    worksheet_entries.Item(X, Y).Value = objrs("q11").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1


                    worksheet_entries.Item(X, Y).Value = objrs("case_comments").ToString
                    worksheet_entries.Item(X, Y).Style = DefaultStyle
                    Y = Y + 1

                End While

            End Using

        Catch ex As Exception
            worksheet_entries.Item("E1").Text = ex.ToString
        End Try

        worksheet_entries.AutofitColumns()

        dbConn1.Close()

        Return worksheet_entries
    End Function




    Public Function YesNo(ByVal bValue As Boolean) As String
        If bValue Then YesNo = "Yes" Else YesNo = "No"
    End Function





</script>
<%

    Session.LCID = 2057
    Var_corp_code = Request.QueryString("ccode")
    var_corp_security_code = Request.QueryString("a")
    var_output_type = Request.QueryString("output")
    var_output_method = Request.QueryString("outputto")
    var_user_ref = Request.QueryString("user")

    var_start_date = Request.QueryString("sdate")
    var_end_date = Request.QueryString("edate")

    var_cookie = Request.QueryString("cookie")

    If len(var_start_date) = 0 then
        var_start_date = "01/01/1900"
    end If
    If len(var_end_date) = 0 Then
        var_end_date = "31/12/2100"
    End If

    var_output_type = "ALL"


    var_restrict_to_delta = Request.QueryString("delta")
    If Len(var_restrict_to_delta) = 0 Then
        var_restrict_to_delta = "N"
    End If


    ' set the license key before saving the workbook
    workbook.LicenseKey = "ZOr66/j66/r56/jl++v4+uX6+eXy8vLy"

    ' set workbook description properties
    workbook.DocumentProperties.Subject = "AssessNET Excel Export"
    workbook.DocumentProperties.Comments = "Exported Excel File from AssessNET.co.uk"
    'workbook.DocumentSecurity.ProtectWorkbook(True, True)

    ' get the first worksheet in the workbook
    'Dim worksheet As ExcelWorksheet = workbook.Worksheets.Item(0)

    ' set the default worksheet name
    'worksheet.Name = "Excel Export from AssessNET"
    'worksheet.WorksheetSecurity.ProtectWorksheet(ExcelProtectionAllowFlags.AllowDefault)    



    DefaultStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left
    DefaultStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center
    ' Set border
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Bottom).Color = Color.Black
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Bottom).LineStyle = ExcelCellLineStyle.Thin
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Left).Color = Color.Black
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Left).LineStyle = ExcelCellLineStyle.Thin
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Right).Color = Color.Black
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Right).LineStyle = ExcelCellLineStyle.Thin
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Top).Color = Color.Black
    DefaultStyle.Borders.Item(ExcelCellBorderIndex.Top).LineStyle = ExcelCellLineStyle.Thin
    DefaultStyle.Alignment.WrapText = True
    DefaultStyle.Fill.FillType = ExcelCellFillType.SolidFill
    DefaultStyle.Fill.SolidFillOptions.BackColor = Color.White
    DefaultStyle.Protection.Locked = False


    CourseStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left
    CourseStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center
    ' Set border
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Bottom).Color = Color.Black
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Bottom).LineStyle = ExcelCellLineStyle.Thin
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Left).Color = Color.Black
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Left).LineStyle = ExcelCellLineStyle.Thin
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Right).Color = Color.Black
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Right).LineStyle = ExcelCellLineStyle.Thin
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Top).Color = Color.Black
    CourseStyle.Borders.Item(ExcelCellBorderIndex.Top).LineStyle = ExcelCellLineStyle.Thin
    ' Set background
    CourseStyle.Fill.FillType = ExcelCellFillType.SolidFill
    CourseStyle.Fill.SolidFillOptions.BackColor = Color.LightGray
    CourseStyle.Font.Color = Color.Black
    CourseStyle.Font.Bold = False
    CourseStyle.Font.Size = 10
    CourseStyle.Alignment.ShrinkToFit = True
    CourseStyle.Protection.Locked = True



    ' WORKSHEET PAGE SETUP

    '  set worksheet paper size and orientation, margins, header and footer
    'worksheet.PageSetup.PaperSize = ExcelPagePaperSize.PaperA4
    'worksheet.PageSetup.Orientation = ExcelPageOrientation.Portrait
    'worksheet.PageSetup.LeftMargin = 1
    'worksheet.PageSetup.RightMargin = 1
    'worksheet.PageSetup.TopMargin = 1
    'worksheet.PageSetup.BottomMargin = 1

    ' add header and footer

    'display a logo image in the left part of the header
    Dim imagesPath As String = System.IO.Path.Combine(Server.MapPath("~"), "Images")
    'Dim logoImg As System.Drawing.Image = System.Drawing.Image.FromFile(System.IO.Path.Combine(imagesPath, "logo.jpg"))
    'worksheet.PageSetup.LeftHeaderFormat = "&G"
    ''worksheet.PageSetup.LeftHeaderPicture = logoImg
    '' display worksheet name in the right part of the header
    'worksheet.PageSetup.RightHeaderFormat = "&A"

    '' add worksheet header and footer
    '' display the page number in the center part of the footer
    'worksheet.PageSetup.CenterFooterFormat = "&P"
    '' display the workbook file name in the left part of the footer
    'worksheet.PageSetup.LeftFooterFormat = "&F"
    '' display the current date in the right part of the footer
    'worksheet.PageSetup.RightFooterFormat = "&D"


    Dim worksheet_entries As ExcelWorksheet



    Dim dbConn1 As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
    dbConn1.Open()

    Dim sql_text1 As String

    sql_text1 = "SELECT * FROM " & Application("DBTABLE_HR_PREFERENCE") & " WITH (NOLOCK) " &
                      "WHERE corp_code='" & Var_corp_code & "' "
    Dim sqlComm2 As New SqlCommand(sql_text1, dbConn1)
    Dim objrs2 As SqlDataReader = sqlComm2.ExecuteReader()
    If objrs2.HasRows Then

        While objrs2.Read()
            Select Case objrs2("pref_id")
                Case "corp_tier2"
                    var_corp_tier2 = objrs2("pref_switch")
                Case "corp_tier3"
                    var_corp_tier3 = objrs2("pref_switch")
                Case "corp_tier4"
                    var_corp_tier4 = objrs2("pref_switch")
                Case "corp_tier5"
                    var_corp_tier5 = objrs2("pref_switch")
                Case "corp_tier6"
                    var_corp_tier6 = objrs2("pref_switch")
                Case "data_export_security_code"
                    var_internal_security_code = objrs2("pref_switch")
            End Select
        End While
    End If
    objrs2.Close()
    sqlComm2.Dispose()

    If var_output_method = "screen" Then
        sql_text1 = "SELECT acc_level FROM " & Application("DBTABLE_USER_DATA") & " WITH (NOLOCK) " &
                    "WHERE corp_Code = '" & Var_corp_code & "' " &
                    "  AND acc_ref = '" & var_user_ref & "' "
        Dim sqlComm3 As New SqlCommand(sql_text1, dbConn1)
        Dim objrs3 As SqlDataReader = sqlComm3.ExecuteReader()
        If objrs3.HasRows Then

            objrs3.Read()

            If objrs3("acc_level") = "0" Then
                var_restrict_locations = "N"
            Else
                var_restrict_locations = "Y"
            End If
        Else
            var_restrict_locations = "Y"
        End If
        objrs3.Close()
        sqlComm3.Dispose()
    Else
        var_restrict_locations = "N"
    End If


    '    sql_text1 = "SELECT corp_licence_version FROM Module_HR.dbo.HR_Data_licences WITH (NOLOCK) " &
    '                "WHERE corp_Code = '" & Var_corp_code & "' " &
    '                "  AND corp_licence_type = 'HAZR' "
    '    Dim sqlComm4 As New SqlCommand(sql_text1, dbConn1)
    '    Dim objrs4 As SqlDataReader = sqlComm4.ExecuteReader()
    '    If objrs4.HasRows Then
    '        objrs4.Read()
    '        var_module_version = objrs4("corp_licence_version")
    '    Else
    '        var_module_version = "2"
    '    End If
    '    If var_module_version = "10" Then
    '        var_module_version = "_v10"
    '    Else
    '        var_module_version = ""
    '    End If
    '    objrs4.Close()
    '    sqlComm4.Dispose()
    '
    '
    '    dbConn1.Close()


    If var_internal_security_code = var_corp_security_code Then

        If var_output_type = "ENTRIES" Or var_output_type = "ALL" Then
            worksheet_entries = outputEntries(var_start_date, var_end_date, var_restrict_locations, var_restrict_to_delta, var_user_ref)

        End If

        'If var_restrict_to_delta = "Y" And var_output_type = "ALL" Then
        ' worksheet_Deleted = outputDeleted()
        'End If
    End If



    'update the delta export table now that the data has been exported
    If var_restrict_to_delta = "Y" Then
        Dim dbConn2 As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
        dbConn2.Open()

        sql_text1 = "UPDATE Module_GLOBAL.dbo.GLOBAL_Delta_exports SET date_processed = GETDATE() " &
                 "WHERE corp_code = '" & Var_corp_code & "' " &
                 "  AND module = 'HAZ' " &
                 "  AND date_processed IS NULL"
        Dim sqlCommUpdate As New SqlCommand(sql_text1, dbConn2)
        sqlCommUpdate.ExecuteNonQuery()

        dbConn2.Close()
    End If


    ' SAVE THE WORKBOOK

    ' Save the Excel document in the current HTTP response stream

    Dim outFileName As String = Nothing

    If var_output_type = "ALL" Then
        var_output_name = ""
    ElseIf var_output_type = "ENTRIES" Then
        var_output_name = "Entries "
    ElseIf var_output_type = "ACT" Then
        var_output_name = "Actions"
    Else
        var_output_name = ""
    End If



    If workbookFormat = ExcelWorkbookFormat.Xls_2003 Then
        outFileName = "Response " & var_output_name & "Data Export.xls"
    Else
        outFileName = "Response " & var_output_name & "Data Export.xlsx"
    End If


    'Dim httpResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
    '
    '   ' Prepare the HTTP response stream for saving the Excel document
    '
    '   ' Clear any data that might have been previously buffered in the output stream
    '  httpResponse.Clear()
    '
    '   ' Set output stream content type for Excel 97-2003 (.xls) or Excel 2007 (.xlsx)
    '  If workbookFormat = ExcelWorkbookFormat.Xls_2003 Then
    'HttpResponse.ContentType = "Application/x-msexcel"
    '    Else
    '   HttpResponse.ContentType = "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    'End If
    '
    '   ' Add the HTTP header to announce the Excel document either as an attachment or inline
    '  HttpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", outFileName))
    '
    '   ' Save the workbook to the current HTTP response output stream
    '  ' and close the workbook after save to release all the allocated resources
    '    Try
    'workbook.Save(HttpResponse.OutputStream)
    '  'Catch ex As Exception
    '  ' report any error that might occur during save
    '  '   Session.Item("ErrorMessage") = ex.Message
    '  '  Response.Redirect("ErrorPage.aspx")
    '  Finally
    ' ' close the workbook and release the allocated resources
    ' workbook.Close()
    '
    '    End Try

    Try

        If var_output_method = "ftp" Then
            Dim excelBytes = workbook.Save()

            Dim ms As New MemoryStream(excelBytes)

            'Dim varFilePathLocal As String = "e:\home\documents\assessweb\" & Var_corp_code & "\ftp_output\"
            Dim varFilePathLocal As String = "\\172.25.16.165\uploads\Client-SFTP-Folders\" & Var_corp_code & "\ftp_output\"

            If (Not System.IO.Directory.Exists(varFilePathLocal)) Then
                System.IO.Directory.CreateDirectory(varFilePathLocal)
            End If

            Dim outStream As FileStream = File.OpenWrite(varFilePathLocal & outFileName)
            ms.WriteTo(outStream)
            outStream.Flush()
            outStream.Close()

        ElseIf var_output_method = "email" Then
            Dim excelBytes = workbook.Save()

            Dim ms As New MemoryStream(excelBytes)

            'Dim varFilePathLocal As String = "e:\home\documents\assessweb\" & Var_corp_code & "\email_output\"
            Dim varFilePathLocal As String = "\\172.25.16.165\uploads\Client-SFTP-Folders\" & Var_corp_code & "\ftp_output\"

            If (Not System.IO.Directory.Exists(varFilePathLocal)) Then
                System.IO.Directory.CreateDirectory(varFilePathLocal)
            End If

            Dim outStream As FileStream = File.OpenWrite(varFilePathLocal & outFileName)
            ms.WriteTo(outStream)
            outStream.Flush()
            outStream.Close()

        ElseIf var_output_method = "screen" Then


            Dim httpResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response

            ' Prepare the HTTP response stream for saving the Excel document

            ' Clear any data that might have been previously buffered in the output stream
            httpResponse.Clear()

            httpResponse.AppendCookie(New HttpCookie("fileDownloadToken", var_cookie))

            ' Set output stream content type for Excel 97-2003 (.xls) or Excel 2007 (.xlsx)
            If workbookFormat = ExcelWorkbookFormat.Xls_2003 Then
                httpResponse.ContentType = "Application/x-msexcel"
            Else
                httpResponse.ContentType = "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            End If

            ' Add the HTTP header to announce the Excel document either as an attachment or inline
            httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", outFileName))

            ' Save the workbook to the current HTTP response output stream
            ' and close the workbook after save to release all the allocated resources

            workbook.Save(httpResponse.OutputStream)

            ' End the response and finish the execution of this page
            httpResponse.End()

        End If

    Finally
        ' close the workbook and release the allocated resources
        workbook.Close()

    End Try

%>