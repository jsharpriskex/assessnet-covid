<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

    <meta http-equiv="Content-Language" content="en-gb" />
    <meta http-equiv="pics-label" content='(pics-1.1 "http://www.icra.org/ratingsv02.html" comment "ICRAonline v2.0" l gen true for "http://www.AssessWEB.co.uk"  r (nz 1 vz 1 lz 1 oz 1 cz 1) "http://www.rsac.org/ratingsv01.html" l gen true for "http://www.AssessWEB.co.uk"  r (n 0 s 0 v 0 l 0))' />
    <meta name="author" content="Safe and Sound Management Consultants Ltd" />
    <meta name="description" content="AssessNET Health and Safety System Online Assessments and Management" />
        <meta http-equiv="content-language" content="en" />
    <meta http-equiv="pragma" content="no-cache" />
    <script language="JavaScript">
    function viewEMEMO() {
        parent.main.location.href="../../modules/acentre/ememo_list.asp?TYPE=1"
        parent.leftmenu.location.href="left-menu.asp?menu=AC"
    }
    
    function viewASSESSMENTS() {
        parent.main.location.href="../../modules/acentre/acentre_briefcase.asp?show=Risk"
        parent.leftmenu.location.href="left-menu.asp?menu=AC"
    }
    
    function viewACCDETAILS() {
        parent.main.location.href="../../modules/hresources/modules/cpanel/account_manager/"
        parent.leftmenu.location.href="left-menu.asp?menu=AC"
    }

    function viewWhoIsOnline() {
        window.open("../functions/func_online.asp","who_is_online","width=300,height=500,scrollbars=yes")
    }
    
    function viewMyDocuments() {
        parent.main.location.href="../../modules/acentre/upload/upload_browser.asp"
    }

    function viewEmemo() {
        parent.main.location.href="../../modules/acentre/ememo_list.asp?TYPE=1"
    }
    
    function viewTmanager() {
        parent.main.location.href="../../modules/management/taskmanager/task_main.asp"
    }
    
    function viewSUPPORT() {
        parent.main.location.href="../../modules/acentre/acentre_services.asp"
        parent.leftmenu.location.href="left-menu.asp?menu=AC"
    }
    
    function OpenPrivacy() {
        window.open('../documents/legal/privacy_statement.html', 'window', 'width=650,height=400,screenX=200,screenY=100,left=200,top=100,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
    }
    
    function OpenTerms() {
        window.open('../documents/legal/terms_conditions.html', 'window', 'width=650,height=400,screenX=200,screenY=100,left=200,top=100,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
    }
    
    

    </script>
    <link rel='stylesheet' href='../scripts/css/global.css' type='text/css' media='screen' />
    <style type='text/css'>
      BODY { padding: 0; margin: 0; color: #000; }
      IMG { border: none; }
      IMG.corp_logo { border: 1px solid #000; margin: 5px; }
      A { font-family: verdana; color: #fff; font-size: x-small; } 
      A.alt { color: #000 ! important; } 
      TABLE.footer { background: #93bce3; }
      TABLE.footer TD
      {
       vertical-align: top;
       margin: 0;
       padding: 0;
      }
      TABLE.footer TD.blank
      {
       border: medium none;
       background: #93bce3 url('../../images/menus/v3_bot_bg.gif') repeat-x left top;
       
      }
      TABLE.footer TD A
      {
       font-family: verdana;
       font-size: x-small;
       color: #fff;
       text-decoration: none;
      }
      TABLE.footer TD A:link { color: #fff; }
      TABLE.footer TD A:visited { color: #fff; }
      TABLE.footer TD A:hover { color: #900; }
      .legal { font-family: verdana; font-size: 9px;}
      .r { text-align: right; }
      .top { vertical-align: top; }
      .mid { vertical-align: middle; }
    </style>
</head>

<body topmargin="0" leftmargin="0" border='0'bgcolor="#93BCE3">
  <table width="100%" cellpadding="0" cellspacing="0" class='footer'>
   <tr>
    <td width='2%' class='r blank'><img src='../../images/menus/v3_bot_round.gif' /></td>
    <td class='r blank' nowrap='nowrap'>
    
    
    <% If Session("YOUR_ACCESS") <> "3" Then %>
    
    <a href="javascript:viewTmanager()"><img src="../../images/menus/v3_bot_tmanager.gif" alt="Task Manager" /></a>
    <a href="javascript:viewEmemo()"><img src="../../images/menus/v3_bot_ememo.gif" alt="Ememo Manager" /></a>
    <a href="javascript:viewMyDocuments()"><img src="../../images/menus/v3_bot_documents.gif" alt="My Documents" /></a>
    <a href="javascript:viewASSESSMENTS()"><img src="../../images/menus/v3_bot_assess.gif" alt="View Assessments" /></a>
    <a href="javascript:viewACCDETAILS()"><img src="../../images/menus/v3_bot_account.gif" alt="Account Details" /></a>
    <a href="javascript:viewSUPPORT()"><img src="../../images/menus/v3_bot_support.gif" alt="Support" /></a></td> 
    <td width='2%' rowspan='2' class='blank mid'><a href='../../modules/management/structure/default.asp' target='main'><img class='corp_logo' src='../../images/companies/<%= Session("CORP_CODE") %>.png' /></a></td>
   
    <% END IF %>
   
   </tr>
   <tr>
     <td class='c legal'>Copyright (c)<br />2001 - <% =Year(date) %></td>
     <td class='r'>
     &nbsp;<strong>User :</strong> <span class='alt'><% =Session("YOUR_FULLNAME") %></span> / <strong>Contract : </strong> <span class='alt'><% =Session("YOUR_COMPANY") %></span> / <strong>Date : </strong> <span class='alt'><% =Date %></span>&nbsp;
     
     </td>
   </tr>
  </table>
</body>
</html>