<%
If "http://test.assessweb.co.uk:8080" = Application("CONFIG_PROVIDER") Then
    menus_dir = "menus_test"
    footerBgColor = "f93"
Else
    menus_dir = "menus"    
    footerBgColor = "93bce3"
End if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>AssessNET</title>
        <script language="JavaScript">
        function ac() {
            parent.main.location.href = "../../modules/acentre/"
        }
        function hs() {
            parent.main.location.href = "../../modules/hsafety/"
        }
        function bt(){
            parent.main.location.href = "../../modules/btools/"
        }
        function bm(){
            parent.main.location.href = "../../modules/hresources/"
        }
        function logoff() {
            parent.main.location.href = "../functions/func_exit.asp"
        }
    </script>
    <style type='text/css'>
        HTML, BODY, TABLE { height: 100%; } 
        BODY { padding: 0; margin: 0; font-family: verdana,sans-serif; font-size: x-small; color: #333; }
        IMG { border: none; }
    </style>
</head>

<body topmargin="0" leftmargin="0" background="../../images/<%= menus_dir %>/v3_menu_bg.gif">


  
<% If Session("YOUR_ACCESS") = "3" Then %>

<table border="0" cellpadding="0" cellspacing="0" width="85" height="100%" ID="Table2">
  <tr>
    <td width="100%" height='30'><a target="_parent" href="../functions/func_exit.asp">
    <img border="0" src="../../images/<%= menus_dir %>/v3_logout.gif" /></a></td>
  </tr>
  <tr>
    <td width="100%" height='50%'>&nbsp;</td>
  </tr>
</table>

<% Else %>

<table border="0" cellpadding="0" cellspacing="0" width="85" height="100%" ID="Table1">
  <tr>
    <td width="100%" height="15">&nbsp;</td>
  </tr>
    
      <tr>
        <td width="100%" height='30'>
    <% If Request.Querystring("menu") = "AC" Then %>
        <a href="left-menu.asp?menu=AC">
        <img src="../../images/<%= menus_dir %>/v3_ac_on.gif" onclick="ac()" /></a>
    <% Else %>
        <a href="left-menu.asp?menu=AC">
        <img src="../../images/<%= menus_dir %>/v3_ac_off.gif" onclick="ac()" /></a>
    <% End if %>
        </td>
      </tr>
    
      <tr>
        <td width="100%" height='30'>
    <% If Request.Querystring("menu") = "HS" Then %>
        <a href="left-menu.asp?menu=HS">
        <img src="../../images/<%= menus_dir %>/v3_hs_on.gif" onclick="hs()" /></a>
    <% Else %>
        <a href="left-menu.asp?menu=HS">
        <img src="../../images/<%= menus_dir %>/v3_hs_off.gif" onclick="hs()" /></a>
    <% End if %>
        </td>
      </tr>
      
       <tr>
        <td width="100%" height='30'>
    <% If Request.Querystring("menu") = "BT" Then %>
        <a href="left-menu.asp?menu=BT">
        <img src="../../images/<%= menus_dir %>/v3_bt_on.gif" onclick="bt()" /></a>
    <% Else %>
        <a href="left-menu.asp?menu=BT">
        <img src="../../images/<%= menus_dir %>/v3_bt_off.gif" onclick="bt()" /></a>
    <% End if %>
        </td>
      </tr>
      
<% IF (Ucase(Right(session("YOUR_USERNAME"),3)) = "SAS") OR (Ucase(Right(session("YOUR_USERNAME"),4)) = "SASS") OR (Ucase(Right(session("YOUR_USERNAME"),3)) = "REX") OR (Ucase(Right(session("YOUR_USERNAME"),3)) = "DEV") Then %>
      
      <tr>
        <td width="100%" height='30'>
    <% If Request.QueryString("menu") = "HR" Then %>
        <a href="left-menu.asp?menu=HR">
        <img src="../../images/<%= menus_dir %>/v3_bm_on.gif" onclick="bm()" /></a>
    <% Else %>
        <a href="left-menu.asp?menu=HR">
        <img src="../../images/<%= menus_dir %>/v3_bm_off.gif" onclick="bm()" /></a>
    <% End if %> 
        </td>
      </tr>
      
<% END IF %>     
      
        <tr>
    <td width="100%" height='60'><a target="_parent" href="../functions/func_exit.asp">
    <img border="0" src="../../images/<%= menus_dir %>/v3_logout.gif" /></a></td>
  </tr>
  <tr>
    <td width="100%" height='50%'>&nbsp;</td>
  </tr>
</table>
      
<% End if %>



</body>

</html>