<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
    <title>Who's Online?</title>
    <meta name="author" content="Safe and Sound Management Consultants Ltd" />
    <meta name="description" content="AssessNET Health and Safety System Online Assessments and Management" />
    <script language="JavaScript">
    </script>
    <link rel='stylesheet' href='../scripts/css/global.css' type='text/css' media='screen' />
    <style type='text/css'>
    </style>
</head>

<body topmargin="0" leftmargin="0">
<!-- #include file="scripts/inc/dbconnections.inc" -->
<%
DIM objConn,objRs,objCommand

startConnections()

objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_USER_DATA") & _
    " WHERE CORP_CODE='" & Session("CORP_CODE") & "' AND ACC_REF <> '" & Session("YOUR_ACCOUNT_ID") & "'"
Set objRs = objCommand.Execute

If objRs.EOF Then
    Response.Write "No-one is online"
Else
    While NOT objRs.EOF
        Response.Write objRs("Per_sname") & ", " & objRs("Per_fname") & "<br />"
        objRs.MoveNext
    Wend
End if

endConnections()
%>
</body>
</html>