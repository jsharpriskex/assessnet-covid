﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using ASNETNSP;
using System.Text;

public partial class frm_lg_setup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string sName = Context.Application.Get("SoftwareName").ToString();
        string sOwner = Context.Application.Get("SoftwareOwner").ToString();
        string sOwnerSite = Context.Application.Get("SoftwareOwnerSite").ToString();
        string sYear = DateTime.Now.Year.ToString();
        pageTitle.Text = sName + "&nbsp; Copyright & copy; 2000 - " + sYear + "&nbsp;" + sOwner + "&nbsp;" + sOwnerSite;


        if (!IsPostBack)
        {
            string yourIP = "";

            yourIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();

            if (yourIP == "195.60.7.88" || yourIP == "92.27.159.64")
            {

                string var_corp_code = Server.HtmlEncode(Request.QueryString["c"].ToString());
                string var_acc_name = Server.HtmlEncode(Request.QueryString["u"].ToString());

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    SqlCommand cmd = new SqlCommand("SELECT acc_ref, acc_pass, acc_manswer FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" + var_corp_code + "' AND acc_name = '" + var_acc_name + "' AND acc_pass_salt IS NULL", dbConn);

                    SqlDataReader objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        string var_acc_ref = objrs["acc_ref"].ToString();
                        string var_password = objrs["acc_pass"].ToString();
                        string var_manswer = objrs["acc_manswer"].ToString();

                        string var_password_salt = Authentication.setSalt(512);
                        string var_manswer_salt = Authentication.setSalt(512);

                        string hashedPassword = Authentication.setSHA512Hash(var_password, var_password_salt);
                        string hashedMAnswer = Authentication.setSHA512Hash(var_manswer, var_manswer_salt);

                        SqlCommand cmd1 = new SqlCommand("UPDATE Module_HR.dbo.HR_Data_users SET acc_pass = '" + hashedPassword + "', acc_manswer = '" + hashedMAnswer + "', acc_pass_salt = '" + var_password_salt + "', acc_manswer_salt = '" + var_manswer_salt + "' WHERE corp_code = '" + var_corp_code + "' AND acc_ref = '" + var_acc_ref + "' AND acc_name = '" + var_acc_name + "'", dbConn);
                        cmd1.ExecuteNonQuery();

                        SqlCommand cmd2 = new SqlCommand("UPDATE Module_HR.dbo.HR_Data_users_password SET acc_pass = '" + hashedPassword + "', acc_pass_salt = '" + var_password_salt + "' WHERE corp_code = '" + var_corp_code + "' AND acc_ref = '" + var_acc_ref + "'", dbConn);
                        cmd2.ExecuteNonQuery();

                        Response.Write("Selected User Passwords Encrypted");

                    }
                    else
                    {
                        //not found
                        Response.Write("Unable to process selected user encryptions");
                    }
                    objrs.Close();

                }
            }

        }
 
    }

}