<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_lg_entry.aspx.cs" Inherits="frm_lg_entry" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Riskex - Health Assessment (COVID-19) Admin Area</title>


    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
   <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="robots" content="noindex" />
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
        <meta property="og:title" content="AssessNET: COVID-19 Health Assessment Admin Service" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://covid.riskex.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1700" />
        <meta property="og:image:height" content="850" />
        <meta property="og:type" content="website" />
        <meta property="og:locale" content="en_GB" />
        <meta property="og:url" content="https://covid.riskex.co.uk/admin/" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="description" content="Class-Leading Cloud Based Health and Safety Management Platform" />

         <!--  Non-Essential, But Recommended -->

        <meta property="og:site_name" content="Riskex Limted - AssessNET">
        <meta name="twitter:image:alt" content="Class-Leading Cloud Based Health and Safety Management Platform">


        <!--  Non-Essential, But Required for Analytics -->

        <meta name="twitter:site" content="@AssessNET">

    <link rel='stylesheet' href="css/login.css" media="all" />





    <style>
        .boxwidth { width: 200px }
    </style>

    <script>
        function getinfo() {
            document.getElementById("screenres").value = (screen.width + "x" + screen.height)
        }

        function vallogin(myForm) {
            valid = 1
            col0 = '#99ccff'
            col1 = '#ffffff'
            errMsg = "Please correct the following fields:\n"

            if (!myForm.username.value) {
                valid = 0
                errMsg += "\n - Username"
                myForm.username.style.background = col0
            } else {
                myForm.username.style.background = col1
            }

            if (!myForm.password.value) {
                valid = 0
                errMsg += "\n - Password"
                myForm.password.style.background = col0
            } else {
                myForm.password.style.background = col1
            }

            if (!valid) {
                window.alert(errMsg)
                return false
            } else {
                return true
            }
        }

    </script>
</head>
<body onload="getinfo()" class='banner-before'>

    <div id='tempcontainer'>
        <div class='logoAssessNET'>
            <p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
        </div>

    </div>

    <br />
    <br />

    <!-- Box -->
    <div id='box_top_brn'>
    <div id='box_bottom_brn'>
    <div id='box_main'>

        <form id='reg1' onsubmit='return vallogin(this)' runat="server">

        <div id='reg'>
            <div id="entry" runat="server">
                <h1>Login to your account</h1>
                <h6>Your details will be verified over a secure encrypted connection.</h6>

                <br />

                <h4><asp:Literal id="txt_err_msg" runat="server" /></h4>
                <!-- <h4>AssessNET may become unavailable for short periods between 5.45 and 6.30 this evening for essential maintenance.</h4> -->

                <input type="hidden" id="screenres" name="screenres" runat="server" />

                <table cellpadding="5" cellspacing="0" border="0">
                <tr align="left">
                <th><label for="username">Email Address</label></th><td><asp:TextBox runat="server" tabindex="1" class="text boxwidth" id="username" placeholder="" aria-describedby="username"></asp:TextBox></td><td class="desc">
                    &nbsp;</td></tr>
                <tr align="left">
                <th><label for="password">Password</label></th><td><asp:TextBox type="password" runat="server" tabindex="2" id="password" class="text boxwidth"  placeholder="" aria-describedby="password"></asp:TextBox></td><td class="desc"></td></tr>
                <tr align="left">
                <th><label for="writeCookie">Remember Me</label></th><td><asp:CheckBox ID="writeCookie" tabindex="3" runat="server" /></td><td class="desc">
                    &nbsp;</td></tr>
                <tr align="left">
                <th>&nbsp;</th><td><asp:Button id="btn_login" Text="Login" tabindex="4" OnClick="btn_login_Click" runat="server" /></td><td class="desc"></td></tr>
                </table>
            </div>

            <div id="prevent" runat="server">
                <h1>Unsupported browser detected</h1>
                <p>You are currently using a browser that is no longer supported by AssessNET.</p>
                <p>For the system to be utilised at its highest potential, with the least amount of security risks for both the system and your computer, Riskex have restricted the permitted internet browser list to only allow IE9 and above.</p>
                <p>Older browsers, such as Internet Explorer 8 and below, are no longer supported by Microsoft and therefore have increased security risks from recent vulnerabilities. As new internet standards are released, older unsupported browsers restrict the content that we can provide to you, such as dynamic web pages, vibrant user interfaces and ultimately your overall user experience.</p>
                <p>To use AssessNET, you must update your browser to Internet Explorer 11, Microsoft Edge or another browser such as Google Chrome.</p>
                <p>Below are a selection of browsers recommended for use with AssessNET. Please select an icon to download the associated browser.</p>

                <table cellpadding="5" cellspacing="0" border="0" style="background-image:none !important; background-color:none !important; margin-bottom:25px; margin-top:30px;">
                    <tr style=" border:none !important;">
                        <td style="text-align:center; border:none !important;"><a href="https://www.google.co.uk/chrome/browser/desktop/index.html"><img src="img/browsers/Chrome-Resize.png" alt="Download Google Chrome" /><p style="padding-top:5px;">Google Chrome</p></a></td>
                        <td style="text-align:center; border:none !important;"><a href="http://www.microsoft.com/en-gb/windows/microsoft-edge"><img src="img/browsers/Edge-Resize.png" alt="Download Microsoft Edge" /><p style="padding-top:5px;">Microsoft Edge</p></a></td>
                        <td style="text-align:center; border:none !important;"><a href="https://www.mozilla.org/en-GB/firefox/new/"><img src="img/browsers/Firefox-Resize.png" alt="Download Mozilla Firefox" /><p style="padding-top:5px;">Mozilla Firefox</p></a></td>
                    </tr>
                </table>

                <p>If you are unable to install the browser due to security restrictions, please speak to your Internal IT or Health and Safety teams for further assistance.</p>
                <p>If at any point you would like to talk to our support team, then please email us at helpdesk@assessnet.co.uk</p>
            </div>


        </div>

        </form>

    </div>
    </div>
    </div>

    <!-- end box -->

    <div id='statement'>
        <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all logins are logged and monitored. By logging into AssessNET, you agree with our Terms and Conditions at all time.</p>


        <table cellpadding="0" cellspacing="0" border="0" class='nostyle noborder' id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td align='right'><img src="img/riskex-logo-2012.png" alt="Riskex" /></td><td align='right'><img src="img/bsi-logos.png" alt="BSI / UKAS Certified ISO Standards" /></td>
        </tr>
        <tr>
            <td align='right' width='50px' style="height: 14px">
                &nbsp;</td><td align='right' width='100px' style="height: 14px">IS 599510&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FS599509&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OHS605516&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        </table>

    </div>

    <p>&nbsp;</p>

    <img src="img/bookmark-banner.PNG" style="display: block;margin-left: auto;margin-right: auto;"/>


</body>
</html>
