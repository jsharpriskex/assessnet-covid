

function vallogin(myForm) {
    valid = 1
    col0 = '#99ccff'
    col1 = '#ffffff'
    errMsg = "Please correct the following fields:\n"
  
    if (!myForm.username.value) {
        valid = 0
        errMsg += "\n - Username"
        myForm.username.style.background = col0
    } else {
        myForm.username.style.background = col1
    }
    
    if (!myForm.password.value) {
        valid = 0
        errMsg += "\n - Password"
        myForm.password.style.background = col0
    } else {
        myForm.password.style.background = col1
    }

    if (!valid) {
        window.alert(errMsg)
        return false
    } else {
        return true
    }
}



// ***** Settings Checker Page ******

function toggle_p_area() {
    if (document.getElementById("password_change").style.display == "block") {
        
        document.getElementById("password_change").style.display = "none";
        document.getElementById("password_input").style.display = "block";
        document.getElementById("pstate").value = "change"
        
    } else {

        document.getElementById("password_change").style.display = "block";
        document.getElementById("password_input").style.display = "none";
        document.getElementById("pstate").value = "hidden"
    
    }
}


function validateInput(myForm) {
    valid = 1;
    col0 = '#99ccff';
    col1 = '#ffffff';
    
    errMsg = ""; 
    
    if (myForm.frm_pi_title.selectedIndex == 0) {
        valid = 0;
        myForm.frm_pi_title.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_title.style.background = col1;
    }

    if (myForm.frm_pi_forename.value.length == 0) {
        valid = 0;
        myForm.frm_pi_forename.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_forename.style.background = col1;
    }

    if (myForm.frm_pi_surname.value.length == 0) {
        valid = 0;
        myForm.frm_pi_surname.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_surname.style.background = col1;
    }

    if (myForm.frm_pi_jobtitle.value.length == 0) {
        valid = 0;
        myForm.frm_pi_jobtitle.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_jobtitle.style.background = col1;
    }

    /*if (myForm.frm_pi_emailname.value.length == 0) {
        valid = 0;
        myForm.frm_pi_emailname.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_emailname.style.background = col1;
    }

    if (myForm.frm_pi_phone.value.length == 0) {
        valid = 0;
        myForm.frm_pi_phone.style.background = col0;
        errMsg = "Please correct the highlighted fields"
    } else {
        myForm.frm_pi_phone.style.background = col1;
    } */

    
    if (!valid) {
        document.getElementById("warning_area").innerHTML = errMsg;
        return false;
    } else {
        document.getElementById("warning_area").innerHTML = "";
        return true;  
    }
}