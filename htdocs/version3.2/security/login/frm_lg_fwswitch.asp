<!-- #include file="../../data/scripts/inc/dbconnections.inc" -->
<!-- #include file="../../data/scripts/inc/sys_checkuser_v2.asp" -->
<%

Sub errCode(varERRnumber)
   LoginURL = "../Login/frm_lg_entry.asp?ERR=" & varERRnumber
   
   Session.Abandon
   Call EndConnections()
   
   'Error redirect
   Response.Redirect(LoginURL)
End sub

    if validateInput(request.querystring("cmd")) = "mimick" then
	'used for sysadmins to log in as a user for support tasks

        corp_code = validateInput(request.querystring("c"))
        username = validateInput(request.querystring("user"))
        admin_account = validateInput(request.querystring("af"))
        user_account = validateInput(request.querystring("at"))

        session.contents.RemoveAll

        session("CORP_CODE") = corp_code

        objCommand.commandText = "SELECT users.acc_ref, users.acc_name, users.acc_pass FROM Module_HR.dbo.HR_Data_users AS users " & _
                                 "WHERE users.acc_ref = '" & user_account & "' AND users.corp_code = '" & session("CORP_CODE") & "'"
        set objRs = objCommand.execute
        if not objRs.EOF then

            session("AUTH") = "Y"
            var_loginhash = ""

            session("YOUR_ACCOUNT_ID") = objRs("acc_ref")
            session("YOUR_USERNAME") = objRs("acc_name")
    
            call checkUSER(objRs("acc_name"), objRs("acc_pass"), "", var_loginhash)

            Response.Redirect("frm_lg_accepted.asp")        
            Response.end 
        else
            response.redirect("frm_lg_entry.aspx?err=0003")
            response.End
        end if
        set objRs = nothing        

    end if

    CALL startConnections()

    'Normal Login Processes

    ' Email shortcut vars
    Session("EMAIL_SHORTCUT") = request("EA")
    Session("EMAIL_SHORTCUT_MODCODE") = request("MC")
    Session("EMAIL_SHORTCUT_VAR1") = request("V1")
    Session("EMAIL_SHORTCUT_VAR2") = request("V2")
    Session("EMAIL_SHORTCUT_VAR3") = request("V3")
    Session("EMAIL_SHORTCUT_VAR4") = request("V4")
    Session("EMAIL_SHORTCUT_VAR5") = request("V5")
    Session("EMAIL_SHORTCUT_VAR6") = request("V6")
    Session("EMAIL_SHORTCUT_VAR7") = request("V7")
    Session("EMAIL_SHORTCUT_VAR8") = request("V8")
    Session("EMAIL_SHORTCUT_VAR9") = request("V9")
    Session("EMAIL_SHORTCUT_VAR10") = request("V10")

'    if len(session("EMAIL_SHORTCUT")) > 0 and session("AUTH") = "Y" and len(session("YOUR_USERNAME")) > 0 and len(session("CORP_CODE")) = 6 and len(session("CORP_PIN")) > 0 then
'        'user is already logged in so bypass authorisations
'        response.redirect("frm_lg_accepted.asp")
'        response.End
'    end if
    if len(session("EMAIL_SHORTCUT")) > 0 then

        userSessionGUID = Request.Cookies("sync")
        if len(userSessionGUID) > 0 then
            objCommand.commandText = "SELECT users.acc_ref, users.acc_name, users.acc_pass FROM Module_HR.dbo.HR_Data_users AS users " & _
                                     "INNER JOIN Module_LOG.dbo.LOG_Session_Status AS session " & _
                                     "  ON users.corp_code = session.corp_code " & _
                                     "  AND users.acc_ref = session.acc_ref " & _
                                     "WHERE session.sessionGUID = '" & userSessionGUID & "'"
            set objRs = objCommand.execute
            if not objRs.EOF then
                session("AUTH") = "Y"

                session("YOUR_ACCOUNT_ID") = objRs("acc_ref")
                session("YOUR_USERNAME") = objRs("acc_name")

                call checkUSER(objRs("acc_name"), objRs("acc_pass"), "", "")

                'user is already logged in so bypass authorisations
                response.redirect("frm_lg_accepted.asp")
                response.End
            else
                response.redirect("frm_lg_entry.aspx?EA=" & Session("EMAIL_SHORTCUT") & "&MC=" & Session("EMAIL_SHORTCUT_MODCODE") & "&V1=" & Session("EMAIL_SHORTCUT_VAR1") & "&V2=" & Session("EMAIL_SHORTCUT_VAR2") & "&V3=" & Session("EMAIL_SHORTCUT_VAR3") & "&V4=" & Session("EMAIL_SHORTCUT_VAR4") & "&V5=" & Session("EMAIL_SHORTCUT_VAR5") & "&V6=" & Session("EMAIL_SHORTCUT_VAR6") & "&V7=" & Session("EMAIL_SHORTCUT_VAR7") & "&V8=" & Session("EMAIL_SHORTCUT_VAR8") & "&V9=" & Session("EMAIL_SHORTCUT_VAR9") & "&V10=" & Session("EMAIL_SHORTCUT_VAR10") & "&ERR=0012")
                response.end 
            end if
        end if
    end if


    'Login transfer from .NET to Classic from login pages

    var_user_GUID = validateInput(request.querystring("guid"))
    var_loginhash = validateInput(request.querystring("h"))

    if len(var_user_GUID) = 0 and len(var_loginhash) = 0 then
        if len(session("EMAIL_SHORTCUT")) > 0 then
            response.redirect("frm_lg_entry.aspx?EA=" & Session("EMAIL_SHORTCUT") & "&MC=" & Session("EMAIL_SHORTCUT_MODCODE") & "&V1=" & Session("EMAIL_SHORTCUT_VAR1") & "&V2=" & Session("EMAIL_SHORTCUT_VAR2") & "&V3=" & Session("EMAIL_SHORTCUT_VAR3") & "&V4=" & Session("EMAIL_SHORTCUT_VAR4") & "&V5=" & Session("EMAIL_SHORTCUT_VAR5") & "&V6=" & Session("EMAIL_SHORTCUT_VAR6") & "&V7=" & Session("EMAIL_SHORTCUT_VAR7") & "&V8=" & Session("EMAIL_SHORTCUT_VAR8") & "&V9=" & Session("EMAIL_SHORTCUT_VAR9") & "&V10=" & Session("EMAIL_SHORTCUT_VAR10") & "&ERR=0012")
            response.end 
        else
            response.redirect("frm_lg_entry.aspx?err=0003")
            response.End
        end if
'        response.redirect("frm_lg_entry.aspx?err=0003")
'        response.End
    end if



    'check for a login session from another server
    if len(var_loginhash) > 0 then
        objCommand.commandText = "SELECT users.acc_ref, users.acc_name, users.acc_pass FROM Module_HR.dbo.HR_Data_users AS users " & _
                                 "WHERE users.temp_guid = '" & var_loginhash & "'"
        set objRs = objCommand.execute
        if not objRs.EOF then
            session("AUTH") = "Y"

            session("YOUR_ACCOUNT_ID") = objRs("acc_ref")
            session("YOUR_USERNAME") = objRs("acc_name")
    
            call checkUSER(objRs("acc_name"), objRs("acc_pass"), "", var_loginhash)
            Response.Redirect("frm_lg_accepted.asp")        
            Response.end 
        else
            response.redirect("frm_lg_entry.aspx?err=0003")
            response.End
        end if
        set objRs = nothing

    elseif len(var_user_GUID) > 0 then
        objCommand.commandText = "SELECT users.acc_ref, users.acc_name, users.acc_pass FROM Module_HR.dbo.HR_Data_users AS users " & _
                                 "INNER JOIN Module_LOG.dbo.LOG_Session_Status AS session " & _
                                 "  ON users.corp_code = session.corp_code " & _
                                 "  AND users.acc_ref = session.acc_ref " & _
                                 "WHERE session.sessionGUID = '" & var_user_GUID & "'"
        set objRs = objCommand.execute
        if not objRs.EOF then
            session("AUTH") = "Y"

            session("YOUR_ACCOUNT_ID") = objRs("acc_ref")
            session("YOUR_USERNAME") = objRs("acc_name")


            call checkUSER(objRs("acc_name"), objRs("acc_pass"), "", var_loginhash)
            Response.Redirect("frm_lg_accepted.asp")
            Response.End
        else
            response.redirect("frm_lg_entry.aspx?err=0003")
            response.End
        end if
        set objRs = nothing
    else
        response.redirect("frm_lg_entry.aspx?err=0003")
        response.End
    end if



    CALL endConnections()

    
%>