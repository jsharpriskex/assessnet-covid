﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_lg_meminfo.aspx.cs" Inherits="frm_lg_meminfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal id="pageTitle" runat="server" /></title>
     <link rel='stylesheet' href="css/login.css" media="all" />

    <script>
        function getinfo() {
            document.getElementById("screenres").value = (screen.width + "x" + screen.height)
        }

        function vallogin(myForm) {
            valid = 1
            col0 = '#99ccff'
            col1 = '#ffffff'
            errMsg = "Please correct the following fields:\n"

            if (myForm.mem_answer) {
                if (!myForm.mem_answer.value) {
                    valid = 0
                    errMsg += "\n - Answer"
                    myForm.mem_answer.style.background = col0
                } else {
                    myForm.mem_answer.style.background = col1
                }
            }

            if (myForm.mem_answer1) {
                if (!myForm.mem_answer1.value) {
                    valid = 0
                    errMsg += "\n - Answer"
                    myForm.mem_answer1.style.background = col0
                } else {
                    myForm.mem_answer1.style.background = col1
                }
                if (!myForm.mem_answer2.value) {
                    valid = 0
                    errMsg += "\n - Answer Confirmation"
                    myForm.mem_answer2.style.background = col0
                } else {
                    myForm.mem_answer2.style.background = col1
                }
            }

            if (!valid) {
                window.alert(errMsg);
                return false;
            } else {
                if (myForm.mem_answer) {
                    document.getElementById("login_auth_marker").style.display = "inline";

                    setTimeout(function () {
                        document.getElementById("btn_continue").disabled = true;
                    }, 500);
                }

                return true;
            }
        }


    </script>
</head>
<body onload="getinfo()" class='banner-before'>

    <div id='tempcontainer'>
        <div class='logoAssessNET'>
            <p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
        </div>

    </div>

    <br />
    <br />
    <br />
    <br />

    <!-- Box -->
    <div id='box_top_brn'>
    <div id='box_bottom_brn'>
    <div id='box_main'>

        <form id='reg1' onsubmit='return vallogin(this)' runat="server">

        <div id='reg'>
            <div id="entry" runat="server">
                <h1>Your memorable information</h1>
                <h6>For added security please provide us with your secret memorable information.</h6>

                    <h4><asp:Literal id="txt_err_msg" runat="server" /></h4>    
                    <input type="hidden" id="screenres" name="screenres" runat="server" />
                    <input type="hidden" name="destination" value="/frm_lg_meminfo.asp" runat="server" />
                    <asp:HiddenField ID="username" runat="server" />
           

                <div id="newUser" runat="server">
                     <p>For added security, you must setup a memorable piece of information which you will be prompted for each time you login to AssessNET in the future. </p>

                    <table cellpadding="5" cellspacing="0" border="0">
                    <tr align="left">
                    <th><label for="mem_question">Select a question</label></th><td>
    
                                        <select name='mem_question' id='mem_question' size='1' tabindex="1" runat="server">
                                        <option value='1'>What is your mother's maiden name?</option>
                                        <option value='2'>What town were you born in?</option>
                                        <option value='3'>What is your father's middle name?</option>
                                        <option value='4'>Who is your sporting hero?</option>
                                        <option value='5'>What is the name of your first pet?</option>
                                        </select>
    
                    </td><td class="desc">&nbsp;</td></tr>
                    <tr align="left">
                    <th><label for="mem_answer1">Answer</label></th><td><asp:TextBox type="password" runat="server" tabindex="2" id="mem_answer1" class="text"  placeholder="" aria-describedby="mem_answer1"></asp:TextBox></td><td class="desc">&nbsp;</td></tr>
                    <tr align="left">
                    <th><label for="mem_answer2">Confirm answer</label></th><td><asp:TextBox type="password" runat="server" tabindex="3" id="mem_answer2" class="text"  placeholder="" aria-describedby="mem_answer2"></asp:TextBox></td><td class="desc">&nbsp;</td></tr>
                    <tr align="left">
                    <th>&nbsp;</th><td><asp:Button id="btn_continueNew" Text="Continue" tabindex="4" OnClick="btn_continueNew_Click" runat="server" /></td><td class="desc">&nbsp;</td></tr>
                    </table>
                </div>

                <div id="existingUser" runat="server">
    
                    <table cellpadding="5" cellspacing="0" border="0">
                    <tr align="left">
                    <th>Question</th><td><strong><asp:Literal id="memQuestion" runat="server" /></strong>&nbsp;</td><td class="desc">&nbsp;</td></tr>
                    <tr align="left">
                    <th><label for="mem_answer">Answer</label></th><td><asp:TextBox type="password" runat="server" tabindex="1" id="mem_answer" class="text"  placeholder="" aria-describedby="mem_answer"></asp:TextBox></td><td class="desc">&nbsp;</td></tr>
                    <tr align="left">
                    <th>&nbsp;</th>
                    <td>
                        <asp:Button id="btn_continue" Text="Continue" tabindex="2" OnClick="btn_continue_Click" runat="server" /> 
                        <span id='login_auth_marker' style="display:none;" class="info" runat="server"> <img src="img/loading_sml_trans.gif" height='10px' alt="Authenticating..." /> Authenticating...</span>
                    </td>
                    <td class="desc">&nbsp;</td></tr>
                    </table>

                </div>
                
                <br />
                <p><strong>Why the extra security?</strong><br />By implementing a 'second stage' login it will decrease the chances of your login being used by somebody who knows your password, so memorable information is now used to boost the intergrity of our security system in case you accidentally share your password.</p>
                <br />

            </div>

            <div id="prevent" runat="server">
                <h1>Unsupported browser detected</h1>
                <p>You are currently using a browser that is no longer supported by AssessNET.</p>
                <p>For the system to be utilised at its highest potential, with the least amount of security risks for both the system and your computer, Riskex have restricted the permitted internet browser list to only allow IE9 and above.</p>
                <p>Older browsers, such as Internet Explorer 8 and below, are no longer supported by Microsoft and therefore have increased security risks from recent vulnerabilities. As new internet standards are released, older unsupported browsers restrict the content that we can provide to you, such as dynamic web pages, vibrant user interfaces and ultimately your overall user experience.</p>
                <p>To use AssessNET, you must update your browser to Internet Explorer 11, Microsoft Edge or another browser such as Google Chrome.</p>
                <p>Below are a selection of browsers recommended for use with AssessNET. Please select an icon to download the associated browser.</p>
        
                <table cellpadding="5" cellspacing="0" border="0" style="background-image:none !important; background-color:none !important; margin-bottom:25px; margin-top:30px;">
                    <tr style=" border:none !important;">
                        <td style="text-align:center; border:none !important;"><a href="https://www.google.co.uk/chrome/browser/desktop/index.html"><img src="img/browsers/Chrome-Resize.png" alt="Download Google Chrome" /><p style="padding-top:5px;">Google Chrome</p></a></td>
                        <td style="text-align:center; border:none !important;"><a href="http://www.microsoft.com/en-gb/windows/microsoft-edge"><img src="img/browsers/Edge-Resize.png" alt="Download Microsoft Edge" /><p style="padding-top:5px;">Microsoft Edge</p></a></td>
                        <td style="text-align:center; border:none !important;"><a href="https://www.mozilla.org/en-GB/firefox/new/"><img src="img/browsers/Firefox-Resize.png" alt="Download Mozilla Firefox" /><p style="padding-top:5px;">Mozilla Firefox</p></a></td>
                    </tr>
                </table>
            
                <p>If you are unable to install the browser due to security restrictions, please speak to your Internal IT or Health and Safety teams for further assistance.</p>
                <p>If at any point you would like to talk to our support team, then please email us at helpdesk@assessnet.co.uk</p>
            </div>
    
        </div>

  
        </form>
    
    </div>
    </div>
    </div>

    <!-- end box -->

    <div id='statement'>
        <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all logins are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a>.  &nbsp;&nbsp;By logging into AssessNET, you agree with our Terms and Conditions at all time.</p> 
 
    
        <table cellpadding="0" cellspacing="0" border="0" class='nostyle noborder' id="TABLE1" onclick="return TABLE1_onclick()">
        <tr>
            <td align='right'><img src="img/riskex-logo-2012.png" alt="Riskex" /></td><td align='right'><img src="img/bsi-logos.png" alt="BSI / UKAS Certified ISO Standards" /></td>
        </tr>
        <tr>
            <td align='right' width='50px' style="height: 14px">
                &nbsp;</td><td align='right' width='100px' style="height: 14px">IS 599510&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FS599509&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OHS605516&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td class="isostatement" align="right" colspan="2">Provided by Riskex Ltd under ISO 9001, OHSAS 18001 and ISO 27001 management systems</td>
        </tr>
        </table>
    
    </div>

</body>
</html>
