﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using ASNETNSP;
using System.Text;

public partial class frm_lg_entry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Authentication.allowBrowser())
        {
            entry.Visible = true;
            prevent.Visible = false;

            username.Focus();
        }
        else
        {
            entry.Visible = false;
            prevent.Visible = true;
        }



        //*********************************************************
        //code required eventually to take incoming variables and pass through to the module if a login has already been activated
        //this remains as a call via frm_lg_entry.asp from the email_handler.aspx page (as originally written)
        //*********************************************************



        string sName = Context.Application.Get("SoftwareName").ToString();
        string sOwner = Context.Application.Get("SoftwareOwner").ToString();
        string sOwnerSite = Context.Application.Get("SoftwareOwnerSite").ToString();
        string sYear = DateTime.Now.Year.ToString();

        if (!IsPostBack)
        {
            Session.Clear();
            Session.Abandon();
            //Email shortcut vars
            if (!(Request["EA"] == null)) { Session["EMAIL_SHORTCUT"] = Request["EA"]; } else { Session["EMAIL_SHORTCUT"] = ""; }

            //check for a redirection before processing the rest of the page. If no redirection, reset session variables (below) before continuing to show login page
            if (Session["EMAIL_SHORTCUT"].ToString().Length > 0)
            {
                if (!(Request["MC"] == null)) { Session["EMAIL_SHORTCUT_MODCODE"] = Request["MC"]; } else { Session["EMAIL_SHORTCUT_MODCODE"] = ""; }
                if (!(Request["V1"] == null)) { Session["EMAIL_SHORTCUT_VAR1"] = Request["V1"]; } else { Session["EMAIL_SHORTCUT_VAR1"] = ""; }
                if (!(Request["V2"] == null)) { Session["EMAIL_SHORTCUT_VAR2"] = Request["V2"]; } else { Session["EMAIL_SHORTCUT_VAR2"] = ""; }
                if (!(Request["V3"] == null)) { Session["EMAIL_SHORTCUT_VAR3"] = Request["V3"]; } else { Session["EMAIL_SHORTCUT_VAR3"] = ""; }
                if (!(Request["V4"] == null)) { Session["EMAIL_SHORTCUT_VAR4"] = Request["V4"]; } else { Session["EMAIL_SHORTCUT_VAR4"] = ""; }
                if (!(Request["V5"] == null)) { Session["EMAIL_SHORTCUT_VAR5"] = Request["V5"]; } else { Session["EMAIL_SHORTCUT_VAR5"] = ""; }
                if (!(Request["V6"] == null)) { Session["EMAIL_SHORTCUT_VAR6"] = Request["V6"]; } else { Session["EMAIL_SHORTCUT_VAR6"] = ""; }
                if (!(Request["V7"] == null)) { Session["EMAIL_SHORTCUT_VAR7"] = Request["V7"]; } else { Session["EMAIL_SHORTCUT_VAR7"] = ""; }
                if (!(Request["V8"] == null)) { Session["EMAIL_SHORTCUT_VAR8"] = Request["V8"]; } else { Session["EMAIL_SHORTCUT_VAR8"] = ""; }
                if (!(Request["V9"] == null)) { Session["EMAIL_SHORTCUT_VAR9"] = Request["V9"]; } else { Session["EMAIL_SHORTCUT_VAR9"] = ""; }
                if (!(Request["V10"] == null)) { Session["EMAIL_SHORTCUT_VAR10"] = Request["V10"]; } else { Session["EMAIL_SHORTCUT_VAR10"] = ""; }

                if (Session["AUTH"] == null) { Session["AUTH"] = ""; }
                if (Session["YOUR_USERNAME"] == null) { Session["YOUR_USERNAME"] = ""; }
                if (Session["CORP_CODE"] == null) { Session["CORP_CODE"] = ""; }
                if (Session["CORP_PIN"] == null) { Session["CORP_PIN"] = ""; }

                if (Session["EMAIL_SHORTCUT"].ToString().Length > 0 && Request.QueryString["err"] == null) // && Session["AUTH"].ToString() == "Y" && Session["YOUR_USERNAME"].ToString().Length > 0 && Session["CORP_CODE"].ToString().Length > 0 && Session["CORP_PIN"].ToString().Length > 0)
                {
                    Response.Redirect("frm_lg_fwswitch.asp?EA=" + Session["EMAIL_SHORTCUT"].ToString() + "&MC=" + Session["EMAIL_SHORTCUT_MODCODE"].ToString() + "&V1=" + Session["EMAIL_SHORTCUT_VAR1"].ToString() + "&V2=" + Session["EMAIL_SHORTCUT_VAR2"].ToString() + "&V3=" + Session["EMAIL_SHORTCUT_VAR3"].ToString() + "&V4=" + Session["EMAIL_SHORTCUT_VAR4"].ToString() + "&V5=" + Session["EMAIL_SHORTCUT_VAR5"].ToString() + "&V6=" + Session["EMAIL_SHORTCUT_VAR6"].ToString() + "&V7=" + Session["EMAIL_SHORTCUT_VAR7"].ToString() + "&V8=" + Session["EMAIL_SHORTCUT_VAR8"].ToString() + "&V9=" + Session["EMAIL_SHORTCUT_VAR9"].ToString() + "&V10=" + Session["EMAIL_SHORTCUT_VAR10"].ToString(), true);
                }
            }
            else
            {
                SessionState.clearAllSessions();

                string var_savedLogin = "";
                HttpCookie aCookie;
                string cookieName;
                int limit = Request.Cookies.Count;
                for (int i = 0; i < limit; i++)
                {
                    cookieName = Request.Cookies[i].Name;
                    //aCookie = new HttpCookie(cookieName);
                    aCookie = Request.Cookies[i];
                    if (cookieName == "SavedLogin") { var_savedLogin = aCookie.Value; }
                    if (cookieName == "sync" || cookieName == "SavedLogin") { aCookie.Domain = "assessweb.co.uk"; }
                    aCookie.Value = "";
                    aCookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(aCookie);
                }
                if (var_savedLogin != null && var_savedLogin != "")
                {
                    addCookie("SavedLogin", var_savedLogin, 60);
                }

                string cookieValue = var_savedLogin;
                if (cookieValue.Length > 0)
                {
                    writeCookie.Checked = true;
                    if (cookieValue.IndexOf("USER=") > -1) { cookieValue = cookieValue.Replace("USER=", ""); }
                    if (cookieValue.IndexOf("%2E") > -1) { cookieValue = cookieValue.Replace("%2E", "."); }
                    username.Text = cookieValue;
                }
            }



            if (!(Request.QueryString["err"] == null))
            {
                txt_err_msg.Text = Authentication.returnError(Request.QueryString["err"].ToString(), "", "");
            }

            //string cookieValue = getCookie("SavedLogin");
            //if (cookieValue.Length > 0)
            //{
            //    writeCookie.Checked = true;
            //    if (cookieValue.IndexOf("USER=") > -1) { cookieValue = cookieValue.Replace("USER=", ""); }
            //    if (cookieValue.IndexOf("%2E") > -1) { cookieValue = cookieValue.Replace("%2E", "."); }
            //    username.Text = cookieValue;
            //}
        }

    }

    protected void btn_login_Click(object sender, EventArgs e)
    {

        // get values from user
        // check if SSO account   - TODO
        // hash the password (512)
        // check credentials against the DB   --- TODO > lengthen the time to generate the hash
        // error if invalid - into literal


        string var_errorCode = "";

        string txt_username = Server.HtmlEncode(username.Text.Replace("'", "`"));
        string txt_password = Server.HtmlEncode(password.Text);
        string txt_screenres = Server.HtmlEncode(screenres.Value);


        //find if they're SSO clients and redirect
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getSSODomain", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_username", txt_username));

            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                if (objrs["redirectURL"].ToString() != "none")
                {
                    Response.Redirect("https://" + objrs["redirectUrl"].ToString() + ".assessweb.co.uk", true);
                }
            }
            objrs.Close();
        }



        string hashedPassword = Authentication.setSHA512Hash(txt_password, Authentication.getSalt("password", txt_username));

        txt_password = "";

        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scripthere123", "console.log(\"" + hashedPassword + "\");", true);


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticateUser", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_username", txt_username));
            cmd.Parameters.Add(new SqlParameter("@var_password", hashedPassword));

            SqlDataReader objrs = cmd.ExecuteReader();

            username.Text = "";
            password.Text = "";

            if (objrs.HasRows)
            {
                objrs.Read();
                var_errorCode = objrs["returnValue"].ToString();
                Session["YOUR_ACCOUNT_ID"] = objrs["acc_ref"].ToString();
                Session["CORP_CODE"] = objrs["corp_code"].ToString();
                Session["YOUR_ACCESS"] = objrs["acc_level"].ToString();



            }
            else
            {
                var_errorCode = "0001";
            }
            objrs.Close();


            if (var_errorCode == "0000")
            {
                //valid user
                Session["PW_OK"] = "Y";
                Session["YOUR_USERNAME"] = txt_username;

                if (writeCookie.Checked == true)
                {
                    addCookie("SavedLogin", txt_username, 60);
                }
                else
                {
                    removeCookie("SavedLogin");
                }

                Response.Redirect("frm_lg_meminfo.aspx", true);
            }
            else
            {
                Session["YOUR_ACCOUNT_ID"] = "";
                Session["CORP_CODE"] = "";
                Session["YOUR_ACCESS"] = "";
                txt_err_msg.Text = Authentication.returnError(var_errorCode, txt_username, txt_screenres);
                username.Text = txt_username.Replace("`", "'");
                username.Focus();
            }
        }

    }



    public void addCookie(string cookieName, string cookieValue, int cookieExpiryDays)
    {
        HttpCookie newCookie = new HttpCookie(cookieName);
        newCookie.Value = cookieValue;
        newCookie.Expires = DateTime.Now.AddDays(cookieExpiryDays);
	newCookie.Domain = "assessweb.co.uk";
        Response.Cookies.Add(newCookie);
    }

    public void removeCookie(string cookieName)
    {
        if (Request.Cookies[cookieName] != null)
        {
            HttpCookie oldCookie = HttpContext.Current.Request.Cookies[cookieName];
            HttpContext.Current.Response.Cookies.Remove(cookieName);
            oldCookie.Expires = DateTime.Now.AddDays(-10);
            oldCookie.Value = null;
            HttpContext.Current.Response.SetCookie(oldCookie);
        }
    }

    public string getCookie(string cookieName)
    {
        if (Request.Cookies[cookieName] != null)
        {

            //Response.Write(Request.Cookies[cookieName].Value);
            //return Request.Cookies["SavedLogin"].Value;

            HttpCookie currentCookie = Request.Cookies[cookieName];

            return currentCookie.Value;
        }
        else
        {
            Response.Write("N");
            return "";
        }
    }




}