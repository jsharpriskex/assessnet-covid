﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using ASNETNSP;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Mail;

public partial class frm_lg_meminfo : System.Web.UI.Page
{

    string var_mem_question = "0";
    string corpCode = "";
    string corpPin = "";
    string accRef = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            if (Session["PW_OK"] == null) { Session["PW_OK"] = "N"; }
            if (Session["PW_OK"].ToString() != "Y")
            {
                Session.Abandon();
                Response.Redirect("frm_lg_entry.aspx", true);
            }

            Session["MI_ATTEMPT_COUNT"] = 0;

            username.Value = Session["YOUR_USERNAME"].ToString();

            string sName = Context.Application.Get("SoftwareName").ToString();
            string sOwner = Context.Application.Get("SoftwareOwner").ToString();
            string sOwnerSite = Context.Application.Get("SoftwareOwnerSite").ToString();
            string sYear = DateTime.Now.Year.ToString();
            pageTitle.Text = sName + "&nbsp; Copyright & copy; 2000 - " + sYear + "&nbsp;" + sOwner + "&nbsp;" + sOwnerSite;

        }


        if (Authentication.allowBrowser())
        {
            entry.Visible = true;
            prevent.Visible = false;
        }
        else
        {
            entry.Visible = false;
            prevent.Visible = true;
        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getMemQuestion", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_username", Session["YOUR_USERNAME"]));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", Session["YOUR_ACCOUNT_ID"]));

            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                if (objrs["acc_mquestion"].ToString() != "")
                {
                    var_mem_question = objrs["acc_mquestion"].ToString();
                }
            }
            objrs.Close();

            if (var_mem_question != "0")
            {
                //look up mem question
                switch (var_mem_question)
                {
                    case "1":
                        memQuestion.Text = "What is your mother's maiden name?";
                        break;

                    case "2":
                        memQuestion.Text = "What town were you born in?";
                        break;

                    case "3":
                        memQuestion.Text = "What is your father's middle name?";
                        break;

                    case "4":
                        memQuestion.Text = "Who is your sporting hero?";
                        break;

                    case "5":
                        memQuestion.Text = "What is the name of your first pet?";
                        break;

                    case "99":
                        //error occured - username wasn't provided (for some reason)
                        //string sessionvalues = "Username: " + Session["YOUR_USERNAME"].ToString() + "..." + Session["YOUR_ACCOUNT_ID"].ToString();
                      //  Messages.SendEmail("m.green@riskex.co.uk", "Memorable Info Failure", "Memorable Info Failure", "Mem info failed because sessions weren't provided. " + sessionvalues, "", true);

                        Response.Redirect("frm_lg_entry.aspx?err=0003", true);
                        break;

                }
            }

        }

        if (var_mem_question == "0")
        {
            newUser.Visible = true;
            existingUser.Visible = false;
            mem_answer1.Focus();
        }
        else
        {
            newUser.Visible = false;
            existingUser.Visible = true;
            mem_answer.Focus();
        }

    }

    protected void btn_continue_Click(object sender, EventArgs e)
    {

        // get values from user 
        // hash the meminfo (512)
        // check credentials against the DB   --- TODO > lengthen the time to generate the hash
        // error if invalid - into literal


        string var_errorCode = "";

        string txt_meminfo = Server.HtmlEncode(mem_answer.Text);
        string txt_username = username.Value;   //Session["YOUR_USERNAME"].ToString();
        string txt_screenres = Server.HtmlEncode(screenres.Value);


        string hashedMemInfo = Authentication.setSHA512Hash(txt_meminfo.ToLower(), Authentication.getSalt("memInfo", txt_username));

        txt_meminfo = "";

        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scripthere123", "console.log(\"" + hashedMemInfo + "\");", true);

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticateMemInfo", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_username", txt_username));
                cmd.Parameters.Add(new SqlParameter("@var_memquestion", var_mem_question));
                cmd.Parameters.Add(new SqlParameter("@var_memanswer", hashedMemInfo));

                SqlDataReader objrs = cmd.ExecuteReader();

                mem_answer.Text = "";

                if (objrs.HasRows)
                {
                    objrs.Read();
                    var_errorCode = objrs["returnValue"].ToString();
                    corpCode = objrs["corp_code"].ToString();
                    corpPin = objrs["corp_pin"].ToString();
                    accRef = objrs["acc_ref"].ToString();

                    if (corpCode.Length > 0) { Session["CORP_CODE"] = corpCode; }
                    if (corpPin.Length > 0) { Session["CORP_PIN"] = corpPin; }
                    if (accRef.Length > 0) { Session["YOUR_ACCOUNT_ID"] = accRef; }

                    Session["MI_ATTEMPT_COUNT"] = (int)Session["MI_ATTEMPT_COUNT"] + 1;
                    if ((int)Session["MI_ATTEMPT_COUNT"] <= 2 && var_errorCode == "0001")
                    {
                        var_errorCode = "<3";
                    }

                }
                else
                {
                    Session["MI_ATTEMPT_COUNT"] = (int)Session["MI_ATTEMPT_COUNT"] + 1;
                    if ((int)Session["MI_ATTEMPT_COUNT"] <= 2)
                    {
                        var_errorCode = "<3";
                    }
                    else
                    {
                        var_errorCode = "0010";
                    }
                }
                objrs.Close();


                if (var_errorCode == "0000")
                {
                    //valid user
                    redirectSuccessfulLogin(txt_username, txt_screenres);
                }
                else
                {
                    if (var_errorCode == "<3")
                    {
                        txt_err_msg.Text = "The memorable answer you provided does not match our database, you have " + (3 - (int)Session["MI_ATTEMPT_COUNT"]) + " tries left.";
                    }
                    else
                    {
                        if ((int)Session["MI_ATTEMPT_COUNT"] == 3)
                        {
                            //log to database
                            Authentication.saveStatus(2, "Mem information invalid", txt_username, txt_screenres);

                            Response.Redirect("frm_lg_entry.aspx?err=0005", true);

                        }
                        else
                        {
                            txt_err_msg.Text = Authentication.returnError(var_errorCode, txt_username, txt_screenres);
                        }
                    }

                    newUser.Visible = false;
                    existingUser.Visible = true;
                    mem_answer.Focus();
                }
            }



    }


    protected void btn_continueNew_Click(object sender, EventArgs e)
    {

        // get values from user 
        // hash the meminfo (512)
        // check credentials against the DB   --- TODO > lengthen the time to generate the hash

        string var_errorCode = "";

        string txt_memquestion = Server.HtmlEncode(mem_question.Value);
        string txt_meminfo1 = Server.HtmlEncode(mem_answer1.Text);
        string txt_meminfo2 = Server.HtmlEncode(mem_answer2.Text);
        string txt_username = Session["YOUR_USERNAME"].ToString();
        string txt_screenres = Server.HtmlEncode(screenres.Value);

        mem_answer1.Text = "";
        mem_answer2.Text = "";

        if (txt_meminfo1 != txt_meminfo2)
        {
            txt_err_msg.Text = "Values entered do not match. Please try again";
        }
        else
        {
            string newSalt = Authentication.setSalt(512);
            string hashedMemInfo = Authentication.setSHA512Hash(txt_meminfo1.ToLower(), newSalt);

            txt_meminfo1 = "";
            txt_meminfo2 = "";


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_setMemInfo", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@varUsername", txt_username));
                cmd.Parameters.Add(new SqlParameter("@varMemQuestion", txt_memquestion));
                cmd.Parameters.Add(new SqlParameter("@varMemAnswer", hashedMemInfo));
                cmd.Parameters.Add(new SqlParameter("@varMemSalt", newSalt));

                SqlDataReader objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    corpCode = objrs["corp_code"].ToString();
                    corpPin = objrs["corp_pin"].ToString();
                    accRef = objrs["acc_ref"].ToString();

                    Session["CORP_CODE"] = corpCode;
                    Session["CORP_PIN"] = corpPin;
                    Session["YOUR_ACCOUNT_ID"] = accRef;

                    redirectSuccessfulLogin(txt_username, txt_screenres);

                }
                else
                {
                    txt_err_msg.Text = Authentication.returnError("0010", txt_username, txt_screenres);
                    mem_answer.Focus();
                }
                objrs.Close();
            }

            

        }

    }


    public string setSessionGUID(string var_corp_code, string var_acc_ref)
    {
        string var_ip_address = GetDefaultGateway();
        string userSessionGUID = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_LOG.dbo.createSession", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", var_acc_ref));
            cmd.Parameters.Add(new SqlParameter("@ip_address", var_ip_address));
            cmd.Parameters.Add(new SqlParameter("@session_timeout", "0"));
            cmd.Parameters.Add(new SqlParameter("@portal_read_only","0"));

            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                userSessionGUID = objrs["sessionGUID"].ToString();

                HttpCookie syncCookie = new HttpCookie("sync");
                syncCookie.Value = userSessionGUID;
		syncCookie.Domain = "assessweb.co.uk";
		syncCookie.Expires = DateTime.Now.AddDays(1);
                Response.Cookies.Add(syncCookie);
            }
            else
            {
                Session.Abandon();
                Response.Redirect("frm_lg_entry.aspx?err=0001", true);
            }
            objrs.Close();
        }

        return userSessionGUID;

    }


    public static string GetDefaultGateway()
    {
        return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
    }



    public void redirectSuccessfulLogin(string var_username, string var_screenres)
    {

        Authentication.saveStatus(1, "Access OK", var_username, var_screenres);
        Session["AUTH"] = "Y";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            //SqlCommand cmd = new SqlCommand("SELECT settings_check FROM Module_HR.dbo.HR_Data_users WHERE corp_code = '" + corpCode + "' AND acc_ref = '" + accRef + "'", dbConn);
            SqlCommand cmd = new SqlCommand("SELECT users.settings_check, ISNULL(prefs.pref_switch, 'N') FROM Module_HR.dbo.HR_Data_users AS users " +
                                            "LEFT JOIN Module_HR.dbo.HR_Data_preferences AS prefs " +
                                            "ON users.corp_code = prefs.corp_code " +
                                            "AND prefs.pref_id = 'USERS_MANAGED_BY_IMPORT' " +
                                            "WHERE users.corp_code = '" + corpCode + "' AND users.acc_ref = '" + accRef + "' ", dbConn);
            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                if (objrs.GetSqlBoolean(0) == true && objrs.GetSqlString(1) != "Y")
                {
                    Response.Redirect("frm_lg_settings.aspx", true);
                }
                else
                {


   		        Response.Redirect("~/core/navigation");

                    string emailRedirectString = "";
                    if (!(Session["EMAIL_SHORTCUT"] == null))
                    {
                        emailRedirectString = "&EA=" + Session["EMAIL_SHORTCUT"].ToString() + "&MC=" + Session["EMAIL_SHORTCUT_MODCODE"].ToString() + "&V1=" + Session["EMAIL_SHORTCUT_VAR1"].ToString() + "&V2=" + Session["EMAIL_SHORTCUT_VAR2"].ToString() + "&V3=" + Session["EMAIL_SHORTCUT_VAR3"].ToString() + "&V4=" + Session["EMAIL_SHORTCUT_VAR4"].ToString() + "&V5=" + Session["EMAIL_SHORTCUT_VAR5"].ToString() + "&V6=" + Session["EMAIL_SHORTCUT_VAR6"].ToString() + "&V7=" + Session["EMAIL_SHORTCUT_VAR7"].ToString() + "&V8=" + Session["EMAIL_SHORTCUT_VAR8"].ToString() + "&V9=" + Session["EMAIL_SHORTCUT_VAR9"].ToString() + "&V10=" + Session["EMAIL_SHORTCUT_VAR10"].ToString();
                    }
                    Response.Redirect("frm_lg_fwswitch.asp?guid=" + setSessionGUID(corpCode, accRef) + emailRedirectString, true);
                }

            }
        }


                

    }

}