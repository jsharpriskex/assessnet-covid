<!-- #include file="../../data/scripts/inc/dbconnections.inc" -->
<%


CALL StartCONNECTIONS()

' disable the timeout option if a global override has been set in the preferences table
if len(session("GLOBAL_TIMEOUT")) > 0 then
    if session("GLOBAL_TIMEOUT") = "0" then
        var_timeout_overridden = false
    else
        var_timeout_overridden = true
        session.Timeout = cint(session("GLOBAL_TIMEOUT"))
    end if
else
    var_timeout_overridden = false
end if


' set the timeout
if var_timeout_overridden = false then
    if len(request.form("frm_timeout")) > 0 then
        objcommand.commandtext = "UPDATE " & Application("DBTABLE_USER_DATA") & " SET acc_timeout = '" & validateInput(request("frm_timeout")) & "' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND Acc_ref = '" & Session("YOUR_ACCOUNT_ID") & "' AND acc_level < 4"
        objcommand.execute

        ' set timeout now its changed
        Session.timeout = validateInput(request.Form("frm_timeout"))
    end if
end if



'removed as nolonger required for .NET login process - session-reset moved to login page
'if request.querystring("redirect") <> "false" then
'    Call endConnections()
'    Response.redirect("../../../../core/system/sessions/sessionReset.aspx?redirectTo=login")
'end if




' ok run a check to see if they have made it all the way through
if len(session("AUTH")) > 0 Then

    'removed as the .NET login process takes over
    'if Session("USER_CHECK_SETTINGS") = True or Session("PASSWORD_EXPIRED") = True then
    '    response.Redirect("frm_lg_settings.asp")
    '    response.End 
    'end if

    CALL StartCONNECTIONS()


    ' Shortcut links
    If Session("EMAIL_SHORTCUT") = "Y" then

    var_redirect_module_code = Session("EMAIL_SHORTCUT_MODCODE")

        Objcommand.commandtext = "SELECT * FROM Module_GLOBAL.dbo.GLOBAL_redirections WHERE module_code = '" & var_redirect_module_code & "'"
        SET Objrs = Objcommand.execute

        if Objrs.eof then
        ' There is no such redirection, ignore and carry on as normal
            else

            ' check to see how many vars are present and replace
            var_redirect_querystring = objrs("querystring")
            if len(var_redirect_querystring) > 0 then
                For X = 1 to 10 
                    var_redirect_querystring = replace(var_redirect_querystring, "VAR" & X, Session("EMAIL_SHORTCUT_VAR" & X))
                    ' Clean up sessions
                    Session.Contents.Remove("EMAIL_SHORTCUT_VAR" & X)
                next
            end if

            Session("EMAIL_REDIRECTION_EMAIL") = objrs("redirectionURL") & "?" & var_redirect_querystring

            ' Kill connection
            CALL EndCONNECTIONS()

            ' redirect to fameset page, session will force main frame to redirect
            var_redirect_url = Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/layout/default.asp"  
            response.redirect (var_redirect_url)

            ' Kill script below
            response.end

            end if


    end if
    

    DIM ary_usertype(4)

    ary_usertype(0) = "Global Administrator"
    ary_usertype(1) = "Local Administrator"
    ary_usertype(2) = "Assessor"
    ary_usertype(4) = "Task Manager User"
    ary_usertype(3) = "Read Only"

    DIM ary_userdesc(4)

    ary_userdesc(0) = "Full permissions on users and records"
    ary_userdesc(1) = "Full permissions on users and records in your location"
    ary_userdesc(2) = "Ability to edit records you own and view others"
    ary_userdesc(4) = "Ability to access Task Manager and view selected records only"
    ary_userdesc(3) = "Ability to view any selected records only"

    var_usertype = ary_usertype(Session("YOUR_ACCESS"))
    var_usertype_desc = ary_userdesc(Session("YOUR_ACCESS"))


    Dim objConn
    Dim objCommand
    Dim objRs



    ' Get the last login date and time
    Objcommand.commandtext = "SELECT TOP 2 datetime FROM " & Application("DBTABLE_LOG_ACCESS") & " WHERE username = '" & Session("YOUR_USERNAME") & "' ORDER BY ID DESC"

    SET Objrs = Objcommand.execute

        IF Objrs.EOF Then
        int_lastlogin = "This is the first time you have used your AssessNET account, welcome..."
        ELSE
		    While NOT Objrs.eof
		    Recordcount = Recordcount + 1
    		
		    IF Recordcount = 2 Then
            varLastLogin = "Y"
            var_logindate = left(objrs("datetime"),10)
            var_logintime = right(objrs("datetime"),8)
		    End IF
    		
		    Objrs.movenext
		    Wend
		    int_lastlogin = var_logindate & " at " & var_logintime 
        END IF
        
        
    ' now check the tasks    
    Objcommand.commandtext =  "SELECT count(id) as task_overdue FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                               "WHERE CORP_CODE = '" & SESSION("CORP_CODE") & "' AND TASK_ACCOUNT LIKE '" & Session("YOUR_ACCOUNT_ID") & "' AND  task_status > '0' AND task_due_date < GETDATE() AND auto_kill IS NULL AND task_accepted = 1"
    SET Objrs = Objcommand.execute  
    int_overdue = objrs("task_overdue")
     

    Objcommand.commandtext =  "SELECT count(id) as task_due FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                               "WHERE CORP_CODE = '" & SESSION("CORP_CODE") & "' AND TASK_ACCOUNT LIKE '" & Session("YOUR_ACCOUNT_ID") & "' AND  task_status > '0' AND task_due_date BETWEEN getdate() AND dateadd(ww,4,getdate()) AND auto_kill IS NULL AND task_accepted = 1"
    SET Objrs = Objcommand.execute 
    int_nextfourweeks = objrs("task_due")

'    'tasks waiting
'    objCommand.commandText = "SELECT COUNT(*) AS num_records FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
'                                                "WHERE TASK_ACCOUNT = '" & Session("YOUR_ACCOUNT_ID") & "' AND task_accepted = 0 AND auto_kill IS NULL"
'    set objRs_count = objCommand.execute
'    int_waiting = objRs_count("num_records")    


    
else

   Session.Abandon
   Response.Redirect "frm_lg_entry.asp"
   Response.End

end if

    Response.Redirect Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/layout/"

%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="css/login.css" media="all" />    

    <script type='text/javascript' src='../../data/scripts/js/global.js'></script>
    <script type="text/javascript" src="../../data/scripts/js/jquery-1.11.0.min.js"></script>
    
    <script type="text/javascript" src="../../data/scripts/js/prototype.js"></script>
    <script type="text/javascript" src="../../data/scripts/js/scriptaculous.js?load=effects,builder"></script>
    <script type="text/javascript" src="../../data/scripts/js/lightbox.js"></script>     

    <style>
        #windowtitle { background: #FFF; }
        .r { text-align: right; }
        .c { text-align: center; }
        .l { text-align: left; }
        
        #blanket {
            background-color:#111;
            opacity: 0.75;
            filter:alpha(opacity=65);
            position:absolute;
            z-index: 9001;
            top:0px;
            left:0px;
            width:100%;
        }

        #popUpDiv {
            position:absolute;
            background-color:#eeeeee;
            width:300px;
            /*height:300px;*/
            z-index: 9002;
        }  
        
        #windowtitle 
        {
           width: 96%;
           margin: 10px 5px 5px 5px;
           padding: 2px 0px 0px 0px;
           background: #940303 url(img/title-bck-rgrad.png) repeat-x left top;
           border-left: #BEBBA9 1px solid !important;
           border-right: #BEBBA9 1px solid !important; 
           border-bottom: #BEBBA9 1px solid !important; 
           text-align: left;
           margin-left: auto;
           margin-right: auto;
        }

        #windowtitle h5
        {
           margin: 3px 6px 5px 6px;
           color: #fff;
           font:bold 13px "Trebuchet MS",Verdana,Arial,Sans-serif;
        }

        #windowdrop
        {
           display: none;
           background: #ECE9D8 url(img/toolbar-bck-ggrad-sm.gif) repeat-x top left;
           padding: 10px 21px 10px 21px;
           font-size:11px;
        }

        #windowmenu 
        {
           padding: 5px 6px 7px 6px; /* padding: 4px 6px 6px 6px; */
           background: #fff url(img/toolbar-bck-ggrad.gif) repeat-x;
           background-position: top left;
        }

        #windowmenu a:link, #windowmenu a:visited {
	        color: #c00;
	        text-decoration: none;
        }

        #windowmenu a:hover {
	        color: #000;
	        text-decoration: none;
        }

        #windowmenu span
        {
            padding: 0px 15px 0px 15px;
            border-right: 1px solid #A39F8F;
        }

        #windowmenu th
        {
            padding: 0px 15px 0px 15px;
            font-weight:normal;
            text-align: left;
            border-right: 1px solid #A39F8F;
        }

        #windowbody
        {
           padding: 10px 0px 10px 0px;
           background: #fff; 
        }

        div.pad
        {
           padding: 10px 6px 10px 6px !important;
        }
              
    </style>

<!-- Knowledge base window -->
<script language='javascript' type="text/javascript">
function openKnowledgebase() {
    window.open('../../../../external/support/', 'window', 'width=750,height=550,screenX=750,screenY=550,left=10,top=10,scrollbars=yes,resizable=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
    }

function openNewKnowledgebase() {
    window.open('../../../../core/help/knowledgeBase.aspx', 'window', 'width=1050,height=550,screenX=1050,screenY=550,left=10,top=10,scrollbars=yes,resizable=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}


</script>
<!-- script end -->

</head>
<body>
<%if session("corp_code") <> "200200" then %>
<!--********START OF NOTIFICATION AREA******-->
<!--
<div id="blanket" style="display:none;"></div>
<div id="popUpDiv" style="display:none; height: 370px; width: 500px" class='c'>
   <div id='windowtitle'>
        <h5>AssessNET Service Notifications</h5>
        <div id='windowdrop'>
        &nbsp;
        </div>
        <div id='windowmenu'>
            <span><a href="#" onclick="popup('popUpDiv')" >Close</a></span>
        </div>

        <div id='windowbody' class='pad'>
            <div class='sectionarea c' >
                <h2>PDF Generation Issue</h2>
                <p class='l'>Some users are currently experiencing an issue downloading PDF`s from AssessNET. We have been investigating the issue in conjunction with our 3rd party supplier of this utility and hope to have a resolution as soon as possible. In the meantime if you urgently require a PDF please contact support quoting the reference number of the report you require and we will endeavour to send a copy via email as soon as possible.</p>
                <p class='l'>Thank you for your patience.</p>
                <p class='r'><span class='info'>Posted 21/06/2011</span><br /></p>
                
                <p class='c'><a href="javascript:popup('popUpDiv')">Close this window</a></p>
            </div>
        </div>
    </div>
</div>
<script> popup("popUpDiv") </script>
-->
<!-- ********END OF NOTIFICATION AREA****** -->

<%end if %>
<div id='tempcontainer'>
<div class='logoAssessNET'>
<p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
</div>

</div>

<br />
<br />
<br />
<br />

<!-- Box -->
<div id='box_top_brn'>
<div id='box_bottom_brn'>
<div id='box_main'>

    <form action='frm_lg_accepted.asp' method='post' id='reg3' >
    
    <%

    IF hour(time) =< 11 Then
	    var_daytime = "Good morning"
    elseIF (hour(time) >= 12) AND (hour(time) =< 17) Then
	    var_daytime = "Good afternoon"
    elseIF hour(time) =< 23 Then
	    var_daytime = "Good evening"
    END IF

	if session("EDITION") = "RN" Then
	    var_info_tagline = "<strong>Reseller Network :</strong> " & Session("YOUR_COMPANY_NAME")
	else
	    if session("YOUR_COMPANY") = "0" then
	        var_info_tagline = "User not binded to a location: Please contact your administrator"
	    else
	        var_info_tagline = Session("YOUR_COMPANY_NAME") & " : " & Session("YOUR_LOCATION_NAME") 
        end if
    end if
    
    
    ' Get the latest news articles

    objCommand.commandText = "SELECT top 2 news.*, users.per_fname, users.per_sname, comp.struct_name FROM " & Application("DBTABLE_GLOBAL_News") & " AS news " & _
                             "LEFT JOIN " & Application("DBTABLE_USER_DATA") & " AS users " & _
                             "  ON news.created_by = users.acc_ref " & _
                             "  AND news.corp_code = users.corp_code " & _
                             "INNER JOIN MODULE_PEP.dbo.Structure_Data_tier1 AS comp " & _
                             "  ON news.corp_code = comp.corp_code " & _
                             "WHERE news.show_from <= GETDATE() " & _
                             "  AND '" & session("CORP_CODE") & "' NOT IN (SELECT corp_code FROM Module_GLOBAL.dbo.GLOBAL_News_Exclude_List WHERE news_id = news.id) "
                             
                             
                             ' & _
                             '"  AND news.show_from > GETDATE()-30"

    if session("CORP_CODE") = "100100" OR session("CORP_CODE") = "222999" then
    else
        objCommand.commandText = objCommand.commandText & " AND (news.corp_code = '100100' OR news.corp_code = '222999' OR news.corp_code = '" & session("CORP_CODE") & "' OR news.global = 1) "
    end if
    if session("YOUR_ACCESS") <> "0" then
        objCommand.commandText = objCommand.commandText & " AND news.GAonly = 0 "
    end if
    if session("CORP_CODE") <> "100100" then
        objCommand.commandText = objCommand.commandText & " AND news.RNonly = 0 "
    end if  
    if session("CORP_CODE") = "200200" then
        objCommand.commandText = objCommand.commandText & " AND news.Demo = 0 "
    end if  
    objCommand.commandText = objCommand.commandText & "ORDER BY news.show_from DESC"





    set objRs_news = objCommand.execute
    if  objRs_news.EOF then
       
            showNews = False
    else
        showNews = True
        var_newsitem = var_newsitem & "<div stlye='float: auto; clear: both;'><div style='float: left; clear: left; padding-top: 15px; color: #664E38;'><strong>Latest News</strong></div><div style='float: right; clear: right; padding-top: 15px;'> "
        if session("YOUR_ACCESS") = "0" then 
            var_newsitem = var_newsitem & "<a href='" & Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/layout/default.asp?cmd=news&action=add' class='news'> [ + Add Your Own ]</a> "
        end if 
        var_newsitem = var_newsitem & "<a href='" & Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/layout/default.asp?cmd=news' class='news'> [ + More News ]</a> </div><div style='clear: both;'></div></div>"
   
        while not objRs_news.eof 
            
            select case objRs_news("Type")
                case "New", "Coming soon"
                    var_span_color = "txt_shade_red"
                    
                case "Upgrade"
                    var_span_color = "txt_shade_grn"
            
                case else
                    var_span_color = "txt_shade_gry"
                    
            end select
                
            var_newsitem = var_newsitem &  "<div style='width: 100%; border-top: 1px solid #CABBA9; padding: 3px; clear: both;'><img src='img/news-iconv1.gif' style='float: left; padding: 0px 15px 0px 0px;'><h5><a href='" & Application("CONFIG_PROVIDER") & "/version" & Application("CONFIG_VERSION") & "/layout/default.asp?cmd=news' class='news'>"
            
            
            if len(objRs_news("headline")) > 80 then
                var_newsitem = var_newsitem & left(objRs_news("headline"),80) & "..."
            else
                var_newsitem = var_newsitem & objRs_news("headline") 
            end if
            
            var_newsitem = var_newsitem & "</a></h5>"
            
            
            if len(objRs_news("body")) > 90 then
                var_newsitem = var_newsitem & left(objRs_news("body"),90) & "..."
            else
                var_newsitem = var_newsitem & objRs_news("body") 
            end if
            
            if objRs_news("corp_code") = "222999" or objRs_news("corp_code") = "100100" then
                var_news_postedby = "AssessNET"
            else
                var_news_postedby = objRs_news("struct_name")
            end if
            
            var_newsitem = var_newsitem & "<span class='postby'>Posted " & left(objRs_news("show_from"),10) & " by " & var_news_postedby & "</span>"
            
            
            var_newsitem = var_newsitem & "</div>"
        objRs_news.movenext
       
       wend
         end if
    set objRs_news = nothing
    
    ' End of news section
    
 
    %>

    
    <div id='reg'>
    <h1><% =var_daytime %>,&nbsp;<% =Session("YOUR_FIRSTNAME") %></h1>
    <h6><% =var_info_tagline %></h6>

    <input type="hidden" name="destination" value="/frm_lg_accepted.asp" />

    
    <% if showNews = True then
        response.write(var_newsitem)
    end if %>
    <table width='100%' class='nostyle' cellpadding='5px' cellspacing='0' style="margin-top: 0px">
    <tr><td colspan='2' align='center'>
    
        <br />
        
        <!-- if session("corp_code") = "010204" or  session("corp_code") = "222999" then response.write Application("CONFIG_PROVIDER_SECURE")   else -->
        
        <a href='<% if session("CORP_CODE") = "100100" then response.write "http://app1.assessweb.co.uk" else response.write Application("CONFIG_PROVIDER") end if %>/version<% =Application("CONFIG_VERSION") %>/layout/<%
                     if session("SPARS") = "Y" then
                        response.write "default.asp?cmd=spars"  
                    elseif ucase(Session("YOUR_USERNAME")) = "TPHELPS.BCC" or ucase(Session("YOUR_USERNAME")) = "MLEGG.BCC" then 
                        response.write "default.asp?cmd=acc"  
                    elseif ucase(Session("YOUR_USERNAME")) = "TJACOBS.BCC" or ucase(Session("YOUR_USERNAME")) = "TANGEL.BCC" or ucase(Session("YOUR_USERNAME")) = "STOWSEY.BCC" or ucase(Session("YOUR_USERNAME")) = "ATAYLOR.BCC" or ucase(Session("YOUR_USERNAME")) = "KJEAKINGS.BCC"  or ucase(Session("YOUR_USERNAME")) = "HCOURSE.BCC"  or ucase(Session("YOUR_USERNAME")) = "LAYLEN.BCC"  or session("corp_code") = "028605" then
                        response.write "default.asp?cmd=acc_home" 
                    elseif ucase(Session("YOUR_USERNAME")) = "ATAYLOR.SAS" then 'gva grimley
                        response.write "default.asp?cmd=compliance" 
                   end if 
                   %>'><img src='img/btn-CWithoutM.png' title='Continue without manual' /></a>
    
        <!-- if session("corp_code") = "010204" or  session("corp_code") = "222999" then response.write Application("CONFIG_PROVIDER_SECURE")   else -->
        
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a href='<% if session("CORP_CODE") = "100100" then response.write "http://app1.assessweb.co.uk" else response.write Application("CONFIG_PROVIDER") end if %>/version<% =Application("CONFIG_VERSION") %>/layout/<%
                     if session("SPARS") = "Y" then
                        response.write "default.asp?cmd=spars"  
                    elseif ucase(Session("YOUR_USERNAME")) = "TPHELPS.BCC" or ucase(Session("YOUR_USERNAME")) = "MLEGG.BCC" then 
                        response.write "default.asp?cmd=acc"  
                    elseif ucase(Session("YOUR_USERNAME")) = "TJACOBS.BCC" or ucase(Session("YOUR_USERNAME")) = "TANGEL.BCC" or ucase(Session("YOUR_USERNAME")) = "STOWSEY.BCC" or ucase(Session("YOUR_USERNAME")) = "ATAYLOR.BCC" or ucase(Session("YOUR_USERNAME")) = "KJEAKINGS.BCC"  or ucase(Session("YOUR_USERNAME")) = "HCOURSE.BCC"  or ucase(Session("YOUR_USERNAME")) = "LAYLEN.BCC"  or session("corp_code") = "028605"  then
                        response.write "default.asp?cmd=acc_home" 
                    elseif ucase(Session("YOUR_USERNAME")) = "ATAYLOR.SAS" then 'gva grimley
                        response.write "default.asp?cmd=compliance" 
                   end if 
                   %>'><img src='img/btn-CWithM.png' title='Continue with manual' <% if session("revert_new_help") = "Y" then response.write "onclick='openKnowledgebase();'" else response.write "onclick='openNewKnowledgebase();'" end if %> />

        </a>

    </td></tr>
    </table>

    <table cellpadding="5" cellspacing="0" border="0">
    <% if int_overdue > 0 then %>
        <tr align="left">
        <!-- <th>Your tasks</th><td><span style='color:#c00c00;'><strong><% =int_overdue %> Overdue</strong></span> | <strong><% =int_nextfourweeks %></strong> within the next four weeks</td></tr> -->
        <th>URGENT tasks</th>
        <td>
            <span style='color:#c00c00;'><strong><% =int_overdue %> Overdue</strong></span> 
            
            
            
           <!-- | <strong><% =int_waiting %></strong> Waiting Acceptance -->
        
        
        </td></tr>
    <% end if %>

    <tr align="left">
    <% if var_timeout_overridden = false then %>
        <th>Logout if idle after</th><td><select name='frm_timeout' onchange='submit()'>
            <option value='15' <% if Session.timeout = "15" then response.write "selected" end if %>>15 mins</option>
            <option value='20' <% if Session.timeout = "20" then response.write "selected" end if %>>20 mins (recommended)</option>
            <option value='30' <% if Session.timeout = "30" then response.write "selected" end if %>>30 mins</option>
            <option value='40' <% if Session.timeout = "40" then response.write "selected" end if %>>40 mins</option>
            <option value='50' <% if Session.timeout = "50" then response.write "selected" end if %>>50 mins</option>
            <% if session("CORP_CODE") = "222999" or session("CORP_CODE") = "039605" or session("CORP_CODE") = "000010" then %>
                <option value='120' <% if Session.timeout = "120" then response.write "selected" end if %>>120 mins</option>
                <option value='180' <% if Session.timeout = "180" then response.write "selected" end if %>>180 mins</option>
            <% end if %>
        </select></td></tr>
    <% else %>
        <th>Logout if idle after</th><td><% =session.Timeout & " minutes" %></td></tr>
    <% end if %>
<!--
    <tr align="left">
    <th>Your account type</th><td><strong><% =var_usertype %></strong> <em>(<% =var_usertype_desc %>)</em></td></tr>
    <tr align="left">
    <th>Last login date / time</th><td><% =int_lastlogin %></td></tr>
-->
    </table>

    </div>
    
    </form>

    
</div>
</div>
</div>
<!-- end box -->

<div id='statement'>
    <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all login attempts are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a></p> 
</div>





<% 

if validateInput(request("a")) = "user" then
    objCommand.commandText = "SELECT acc_name FROM " & Application("DBTABLE_USER_DATA") & " " & _
                             "WHERE corp_code = '" & session("CORP_CODE") & "' " & _
                             "  AND acc_ref = '" & session("YOUR_ACCOUNT_ID") & "'"
    set objRs_a = objCommand.execute
    if not objRs_a.EOF then
        objRs_a.movefirst
        
        response.Write "<script>alert(""WARNING: Your username has been updated to : " & objRs_a("acc_name") & """)</script>"
    end if
    set objRs_a = nothing
end if


CALL EndCONNECTIONS()
%>
</body>
</html>