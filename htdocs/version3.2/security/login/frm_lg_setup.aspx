﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_lg_setup.aspx.cs" Inherits="frm_lg_setup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal id="pageTitle" runat="server" /></title>
     <link rel='stylesheet' href="css/login.css" media="all" />


</head>
<body onload="getinfo()" class='banner-before'>

    <div id='tempcontainer'>
        <div class='logoAssessNET'>
            <p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
        </div>

    </div>

    <br />
    <br />
    <br />
    <br />

    <!-- Box -->
    <div id='box_top_brn'>
    <div id='box_bottom_brn'>
    <div id='box_main'>

        <form id='reg1' onsubmit='return vallogin(this)' runat="server">

        <div id='reg'>

            <div id="prevent" runat="server">
                <h1>Password Updated</h1>
                <p>The account password has been updated.</p>
            </div>


        </div>
  
        </form>
    
    </div>
    </div>
    </div>

    <!-- end box -->

</body>
</html>
