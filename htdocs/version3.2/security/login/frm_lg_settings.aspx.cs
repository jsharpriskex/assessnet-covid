﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using ASNETNSP;
using System.Text;

public partial class frm_lg_settings : System.Web.UI.Page
{

    protected string corpCode = "";
    protected string accRef = "";
    protected string rowId = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        string sName = Context.Application.Get("SoftwareName").ToString();
        string sOwner = Context.Application.Get("SoftwareOwner").ToString();
        string sOwnerSite = Context.Application.Get("SoftwareOwnerSite").ToString();
        string sYear = DateTime.Now.Year.ToString();
        pageTitle.Text = sName + "&nbsp; Copyright & copy; 2000 - " + sYear + "&nbsp;" + sOwner + "&nbsp;" + sOwnerSite;


        corpCode = Session["CORP_CODE"].ToString();
        accRef = Session["YOUR_ACCOUNT_ID"].ToString();


        title.Items.Add("Ms");
        title.Items.Add("Mr");
        title.Items.Add("Miss");
        title.Items.Add("Mrs");

        if (!IsPostBack)
        {
            getDomains();
            getUserDetails();
        }
            

    }

    private void getUserDetails()
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", accRef));

            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                rowId = objrs["id"].ToString();
                title.SelectedValue = objrs["per_title"].ToString();
                forename.Text = objrs["per_fname"].ToString();
                surname.Text = objrs["per_sname"].ToString();
                jobTitle.Text = objrs["per_job_title"].ToString();
                email.Text = objrs["email_name"].ToString();
                emailDomain.SelectedValue = objrs["email_domain"].ToString();
                telephone.Text = objrs["corp_phone"].ToString();

            }
            objrs.Close();
        }
    }

    private void getDomains()
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_getDomains", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            emailDomain.DataTextField = "Name";
            emailDomain.DataValueField = "ID";

            emailDomain.DataSource = dt;
            emailDomain.DataBind();
        }
    }

    protected void btn_continue_Click(object sender, EventArgs e)
    {
        string opt_title = title.SelectedValue;
        string txt_forename = Server.HtmlEncode(forename.Text);
        string txt_surname = Server.HtmlEncode(surname.Text.Replace("'", "`"));
        string txt_jobTitle = Server.HtmlEncode(jobTitle.Text);
        string txt_email = Server.HtmlEncode(email.Text);
        string opt_emailDomain = emailDomain.SelectedValue;
        string txt_telephone = Server.HtmlEncode(telephone.Text);

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HR.dbo.authenticate_setUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", accRef));
            cmd.Parameters.Add(new SqlParameter("@var_title", opt_title));
            cmd.Parameters.Add(new SqlParameter("@var_forename", txt_forename));
            cmd.Parameters.Add(new SqlParameter("@var_surname", txt_surname));
            cmd.Parameters.Add(new SqlParameter("@var_job_title", txt_jobTitle));
            cmd.Parameters.Add(new SqlParameter("@var_email", txt_email + '@' + opt_emailDomain));
            cmd.Parameters.Add(new SqlParameter("@var_telephone", txt_telephone));

            cmd.ExecuteNonQuery();

            SqlCommand cmd1 = new SqlCommand("UPDATE Module_HR.dbo.HR_Data_users SET settings_check = 0 WHERE corp_code = '" + corpCode + "' AND acc_ref = '" + accRef + "'", dbConn);
            cmd1.ExecuteNonQuery();

            Response.Redirect("frm_lg_fwswitch.asp?guid=" + setSessionGUID(corpCode, accRef) + "&h=", true);
        }

    }



    public string setSessionGUID(string var_corp_code, string var_acc_ref)
    {
        string var_ip_address = GetDefaultGateway();
        string userSessionGUID = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_LOG.dbo.createSession", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", var_acc_ref));
            cmd.Parameters.Add(new SqlParameter("@ip_address", var_ip_address));
            cmd.Parameters.Add(new SqlParameter("@session_timeout", "0"));
            cmd.Parameters.Add(new SqlParameter("@portal_read_only", "0"));

            SqlDataReader objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                userSessionGUID = objrs["sessionGUID"].ToString();

                HttpCookie syncCookie = new HttpCookie("sync");
                syncCookie.Value = userSessionGUID;
                Response.Cookies.Add(syncCookie);
            }
            else
            {
                Response.Redirect("frm_lg_entry.aspx?err=0001", true);
            }
            objrs.Close();
        }

        return userSessionGUID;

    }

    public static string GetDefaultGateway()
    {
        return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
    }

}