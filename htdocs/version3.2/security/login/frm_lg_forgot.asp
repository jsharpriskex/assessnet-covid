<!-- #include file="../../data/scripts/inc/dbconnections.inc" -->
<%
Application("DBTABLE_LOG_ACCESS_EMAIL") = Application("CONFIG_DBASE") & "_LOG.dbo.LOG_Access_Email"

dim IP_Address, var_success, var_emailaddress

dim Arry_reason(3)
Arry_reason(1) = "Request Successful"
Arry_reason(2) = "Invalid Email / Not associated to account on the system."
Arry_reason(3) = "Email Address is being used for more than one account."

IP_Address = Request.ServerVariables("remote_addr")
var_success = "2"
var_emailaddress = validateInput(request.Form("email"))

  

if validateInput(request.Form("submit")) = "Cancel" then
 response.Redirect ("frm_lg_entry.aspx")
end if

' get the users details and e-mail, also check to see if there is more than one account, if so fail.
if len(request.Form("email")) > 0 then

    Dim objConn
    Dim objCommand
    Dim objRs
    Dim objRs1
    Dim objRs2

        CALL startConnections() 
  
    ' ok so check them out

    var_emailaddress = Replace(Replace(request.Form("email"),"`","'"),"'", "''")

    Set chEmail = Server.CreateObject("ADODB.Command")
    chEmail.ActiveConnection=objConn
    chEmail.Prepared = true
    chEmail.commandtext="SELECT corp_code, acc_ref FROM " & Application("DBTABLE_USER_DATA") & " WHERE acc_level < 5 and corp_email = '" & var_emailaddress & "' and acc_status <> 'D'"

    set objrs = chEmail.execute

    if objrs.eof then
        var_info_msg = "We are unable to retrieve your details. Please contact your Health and Safety team."
        var_success = "2"
    else
        recordnum = 0
        while not Objrs.eof
            recordnum = recordnum + 1
            objrs.movenext
        wend
        
        if recordnum = 1 then
            set objrs = chEmail.execute

            var_corp_code = objrs("corp_code")
            var_pi_accref = objrs("acc_ref")

            var_success = "1"

            objCommand.commandText = "EXEC Module_HR.dbo.SendActivationEmail '" & var_corp_code & "', '" & var_pi_accref & "'"
            objCommand.execute
           
        
            ' ok get the details and e-mail them
            var_info_msg = "Retrieval successful, your account details have been sent. Please check your e-mail: " & var_emailaddress
            var_disabled = "disabled"
        else
            'more than one record found
            var_info_msg = "We are unable to retrieve your details. Please contact your Health and Safety Administrator."
            var_success = "3"
        end if
    end if
    set objrs = nothing
    

    'Log all user detail requests
    var_reason = Arry_Reason(var_success)

    objCommand.commandText = "INSERT INTO " & Application("DBTABLE_LOG_ACCESS_EMAIL") & " (DATETIME, SUCCESS, IP_ADDRESS, REASON, EMAIL, CORP_CODE, ACC_REF) " & _
                             "VALUES (GETDATE(), '" & var_success & "', '" & IP_Address & "', '" & var_reason & "', '" & Replace(Replace(var_emailaddress,"`","'"),"'", "''")  & "', '" & var_corp_code & "', '" & var_pi_accref & "')"
    objCommand.execute

    CALL endconnections()    
    
end if


 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="css/login.css" media="all" />    
</head>
<body class='banner-before'>

<div id='tempcontainer'>
<div class='logoAssessNET'>
<p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
</div>

</div>

<br />
<br />
<br />
<br />

<!-- Box -->
<div id='box_top_brn'>
<div id='box_bottom_brn'>
<div id='box_main'>

    <form action='frm_lg_forgot.asp' method='post' id='reg-done'>
    
    <div id='reg'>
    <h1>Reset your password</h1>
    <h6>Details will be sent via e-mail to the address that is registered with the account.</h6>
    
    <input type="hidden" name="destination" value="/frm_lg_forgot.asp" />
    <br />
    Before we can reset your password, we need to verify that your account is valid and that you are the authorised recipient. Please enter your full corporate e-mail address in the box provided so we may check your details against the database.
       
    <h4><% =var_info_msg %></h4>
    
    <table cellpadding="5" cellspacing="0" border="0">
    <tr align="left">
    <th><label for="email">Email address</label></th><td><input type="text" name="email" value="" tabindex="1" id="email" class="text"  size='40'  <% =var_disabled %>/></td><td class="desc">&nbsp;</td></tr>
    <tr align="left">
    <th>&nbsp;</th><td><input type="submit"  name='submit' id='submit' class="submitbutton" value="Reset Password" tabindex="2"  <% =var_disabled %> />&nbsp;<input type="submit"  name='submit' id='submit' class="submitbutton" value="Cancel" tabindex="2"   /></td><td class="desc"> &nbsp;</td></tr>
    </table>
    
    <br />
    <p><strong>All retrieval attempts are logged for security purposes</strong> <em>(Your connection details will be stored)</em></p>
    <br />
        
    </div>
    
    </form>

    
</div>
</div>
</div>
<!-- end box -->

<div id='statement'>
    <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all login attempts are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a></p> 
    
    <table cellpadding="0" cellspacing="0" border="0" class='nostyle noborder' id="TABLE1" onclick="return TABLE1_onclick()">
    <tr>
        <td align='right'><img src="img/riskex-logo-2012.png" alt="Riskex" /></td><td align='right'><img src="img/bsi-logos.png" alt="BSI / UKAS Certified ISO Standards" /></td>
    </tr>
    <tr>
        <td align='right' width='50px' style="height: 14px">
            &nbsp;</td><td align='right' width='100px' style="height: 14px">IS 599510&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FS599509&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OHS605516&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
    <tr>
        <td class="isostatement" align="right" colspan="2">Provided by Riskex Ltd under ISO 9001, OHSAS 18001 and ISO 27001 management systems</td>
    </tr>
    </table>
    
</div>

<script language='javascript' type="text/javascript">
    document.getElementById("email").focus();
</script>

</body>
</html>