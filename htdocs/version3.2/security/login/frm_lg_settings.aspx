﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frm_lg_settings.aspx.cs" Inherits="frm_lg_settings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><asp:Literal id="pageTitle" runat="server" /></title>
    <link rel='stylesheet' href="css/login.css" media="all" />
</head>

<body class='banner-before'>

    <div id='tempcontainer'>
        <div class='logoAssessNET'>
            <p>ONLINE HEALTH AND SAFETY MANAGEMENT</p>
        </div>

    </div>

    <br />
    <br />
    <br />
    <br />

    <!-- Box -->
    <div id='box_top_brn'>
    <div id='box_bottom_brn'>
    <div id='box_main'>

        <form id='form1' runat="server">
    
        <div id='reg'>

            <h1>Please check your settings</h1>
            <h6>Please check and confirm your personal details below.</h6>

            <br />

            <div style='clear: both;'><div style='float: left; clear: left; padding-top: 15px; color: #664E38;'><strong>Your Personal Details</strong></div><div style='float: right; clear: right; padding-top: 15px; color: #664E38;'><h4><asp:Literal id="txt_err_msg" runat="server" /></h4></div></div>
            
            <table cellpadding="5" cellspacing="0" border="0">
            <tr aling="left"><th><label for="title">Title</label></th><td><asp:DropDownList runat="server" TabIndex="1" id="title" /></td></tr>
            <tr align="left"><th><label for="forename">Forename</label></th><td><asp:TextBox runat="server" tabindex="2" class="text" id="forename" placeholder="" aria-describedby="forename"></asp:TextBox></td></tr>
            <tr align="left"><th><label for="surname">Surname</label></th><td><asp:TextBox runat="server" tabindex="3" class="text" id="surname" placeholder="" aria-describedby="surname"></asp:TextBox></td></tr>
            <tr align="left"><th><label for="jobTitle">Job Title</label></th><td><asp:TextBox runat="server" tabindex="4" class="text" id="jobTitle" placeholder="" aria-describedby="jobTitle"></asp:TextBox></td></tr>
            <tr align="left"><th><label for="email">Email Address</label></th><td><asp:TextBox runat="server" tabindex="5" class="text" id="email" placeholder="" aria-describedby="email"></asp:TextBox>&nbsp;@&nbsp;<asp:DropDownList runat="server" TabIndex="6" id="emailDomain" /></td></tr>
            <tr align="left"><th><label for="telephone">Phone Number</label></th><td><asp:TextBox runat="server" tabindex="7" class="text" id="telephone" placeholder="" aria-describedby="telephone"></asp:TextBox></td></tr>
            <tr align="left"><th>&nbsp;</th><td><asp:Button id="btn_continue" Text="Continue" tabindex="8" OnClick="btn_continue_Click" runat="server" /></td></tr>
            </table>

        </div>
  
        </form>
    
    </div>
    </div>
    </div>

    <!-- end box -->

    <div id='statement'>
        <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all logins are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a>.  &nbsp;&nbsp;By logging into AssessNET, you agree with our Terms and Conditions at all time.</p> 
    </div>

</body>
</html>
