<!-- #INCLUDE FILE="../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../data/scripts/inc/sys_checkuser.asp" -->
<% 

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' LoginCheckv2.asp                                       ''
'' Last Updated 17/12/04 by James Sharp                	  ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'GLOBAL DIMS
Dim objRs
Dim Objconn
Dim Objcommand
Dim LoginURL

Call StartCONNECTIONS()

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Function ipCheck                                       ''
'' This function requires,                                ''
''                    varIP = Ip address for R-Only    	  ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function ipCheck(varIP)
    Objcommand.Commandtext = "SELECT IPHOST From " & Application("DBTABLE_HR_HOSTS") & " WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND IPHOST = '" & varIP & "' "
    SET objRs = Objcommand.execute
    
    If objRs.EOF then
        ipCheck = "FALSE"
    Else
        ipCheck = "TRUE"
    End if
End Function
    

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                             ''
'' SUB (errCode) instructions of use                           ''
'' This sub requires,                                          ''
''                    0001 - Invalid username or password      ''
''                    0002 - No modules for licence            ''
''                    0003 - Invalid IP for read-only          ''
''                    0004 - Account disabled                  ''
''                    0005 - Mem. info Failed after 3 attempts ''
''                    0006 - License expired                   ''
''                                                             ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub errCode(varERRnumber)
   LoginURL = "../Loginv2/frm_lg_entry.asp?ERR=" & varERRnumber
   
   Session.Abandon
   Call EndConnections()
   
   'Error redirect
   Response.Redirect(LoginURL)
End sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Sub renewalCheck(varCorpCode)                          ''
'' varCorpCode => User's corp code                        ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function renewalCheck(varCorpCode)
    objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_SYSTEMS") & " " & _
                             "WHERE corp_code='" & varCorpCode & "'" & _
                             "   AND next_renewal_date > GETDATE()"
    Set objRs = objCommand.Execute
    If objRs.EOF Then
        renewalCheck = "FALSE"
    Else
        renewalCheck = "TRUE"
    End if
    
    renewalCheck = "TRUE"
    
End function


    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ''                                                        ''
	'' SUB (userLOG) instructions of use                      ''
	'' This sub requires,                                     ''
	''                    status (1 for success, 2 for fail)  ''
	''                    details (Reason for log entry)      ''
	''					  username (Username entered at login ''
	''                                                        ''
	''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	SUB useLOG(varSTATUSlog,varDETAILSlog,varUNAMElog)

	varDATElog = Date() & " " & Time()
	varHTTPlog = Request.ServerVariables("HTTPS")
	varIPlog = Request.ServerVariables("REMOTE_ADDR")

		Objcommand.commandtext = "INSERT INTO " & Application("DBTABLE_LOG_ACCESS") & " " & _
								  "(datetime,https,success,reason,username,ip_address) " & _
								  "VALUES('" & varDATElog & "','" & varHTTPlog & "','" & varSTATUSlog & "','" & varDETAILSlog & "','" & varUNAMElog & "','" & varIPlog & "') "
		Objcommand.execute

	END SUB
	


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Run Login script                                       ''
'' Check Users details                                    ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Run script, check user
Dim varuname
varuname = Server.HTMLencode(Request.Form("username"))
Dim varpword
varpword = Server.HTMLencode(Request.Form("password"))

Call checkUSER(varuname,varpword)
Call EndCONNECTIONS()

' Redirect script once finished.
Response.Redirect(LoginURL)
%>