<!-- #INCLUDE FILE="../../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../../data/scripts/inc/sys_checkuser_v2.asp" -->
<!-- #INCLUDE FILE="../../data/functions/func_random.asp" -->
<% 

'    on error resume next 

if Request.ServerVariables("REMOTE_ADDR") = "193.134.170.35" then
    LoginURL = "../Login/frm_lg_entry.asp?ERR=0001"
   
    Session.Abandon
   
    'Error redirect
    Response.Redirect(LoginURL)
end if


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' LoginCheckv3.asp                                       ''
'' Last Updated 17/07/06 by James Sharp                	  ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'GLOBAL DIMS
'Dim objRs
'Dim Objconn
'Dim Objcommand
Dim LoginURL

'Call StartCONNECTIONS()


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                             ''
'' SUB (errCode) instructions of use                           ''
'' This sub requires,                                          ''
''                    0001 - Invalid username or password      ''
''                    0002 - No modules for licence            ''
''                    0003 - Invalid IP for read-only          ''
''                    0004 - Account disabled                  ''
''                    0005 - Mem. info Failed after 3 attempts ''
''                    0006 - License expired                   ''
''                                                             ''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub errCode(varERRnumber)
   LoginURL = "../Login/frm_lg_entry.asp?ERR=" & varERRnumber
   
   Session.Abandon
   Call EndConnections()
   
   'Error redirect
   Response.Redirect(LoginURL)
End sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Sub renewalCheck(varCorpCode)                          ''
'' varCorpCode => User's corp code                        ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function renewalCheck(varCorpCode)
    objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_SYSTEMS") & " " & _
                             "WHERE corp_code='" & varCorpCode & "'" & _
                             "   AND next_renewal_date > GETDATE()"
    Set objRs = objCommand.Execute
    If objRs.EOF Then
        renewalCheck = "FALSE"
    Else
        renewalCheck = "TRUE"
    End if
    
    renewalCheck = "TRUE"
    
End function

	


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''                                                        ''
'' Run Login script                                       ''
'' Check Users details                                    ''
''                                                        ''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Run script, check user
Dim varuname
varuname = validateInput(Request.Form("username"))
Dim varpword
varpword = validateInput(Request.Form("password"))

'rtechnician redirect
if left(lcase(varuname),11) = "rtechnician" and (Request.ServerVariables("REMOTE_ADDR") <> "92.27.159.64" and Request.ServerVariables("REMOTE_ADDR") <> "195.60.7.88") then
    call errCode("0001")
end if

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
'' SSO Addition to extract details of user based on  
'' Unique identifiers in the database during the SSO process 
''
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Redirect Ocado to SSO (temp measure until Login. comes into force for all)
If lcase(varuname) <> "sysadmin.ocado" and lcase(varuname) <> "rtechnician.ocado" then
    If InStr(lcase(varuname),".ocado") and (Request.ServerVariables("REMOTE_ADDR") <> "92.27.159.64" and Request.ServerVariables("REMOTE_ADDR") <> "195.60.7.88") then
        LoginURL = "https://login.assessweb.co.uk"
   
        Session.Abandon
        'Error redirect
        Response.Redirect(LoginURL)
    End If
End If

' Redirect Ocado to SSO (temp measure until Login. comes into force for all)
If lcase(varuname) <> "sysadmin.uob" and lcase(varuname) <> "rtechnician.uob" then
    If InStr(lcase(varuname),".uob") and (Request.ServerVariables("REMOTE_ADDR") <> "92.27.159.64" and Request.ServerVariables("REMOTE_ADDR") <> "195.60.7.88") then
        LoginURL = "https://uob.assessweb.co.uk"
   
        Session.Abandon
        'Error redirect
        Response.Redirect(LoginURL)
    End If
End If

' Redirect Ocado to SSO (temp measure until Login. comes into force for all)
If lcase(varuname) <> "sysadmin.essex" and lcase(varuname) <> "rtechnician.essex" then
    If InStr(lcase(varuname),".essex") and (Request.ServerVariables("REMOTE_ADDR") <> "92.27.159.64" and Request.ServerVariables("REMOTE_ADDR") <> "195.60.7.88") then
        LoginURL = "https://essex.assessweb.co.uk"
   
        Session.Abandon
        'Error redirect
        Response.Redirect(LoginURL)
    End If
End If


Session("SSO_OK") = "N" 

' GUID length
if len(validateInput(request.querystring("SSOUser"))) = 36 then

    SSOUserID = request.querystring("SSOUser")

    objCommand.CommandText = "SELECT SSOUserIdentity FROM Module_Global.dbo.SSOLoginStore " & _
                             "WHERE SSOUserUniqueID='" & SSOUserID & "'" 
    Set objRs = objCommand.Execute
    If objRs.EOF Then
    Else
        SSOUserIdentity = replace(Objrs("SSOUserIdentity"),"'","''")        
    End if

    ' Now we have the identity (email address) we can find their credentials
    ' At the moment this is default to Ocado rather than pull the IdentityProvider detail, this will change

    objCommand.CommandText = "SELECT Acc_name, Acc_pass FROM Module_HR.dbo.HR_Data_users " & _
                             "WHERE (corp_code = '065912' OR corp_code = '295872' OR corp_code = '295878' OR corp_code = '335372' OR corp_code = '327872' OR corp_code = '039605' OR corp_code = '348872' OR corp_code = '356372' OR corp_code = '000001' OR corp_code = '348872') AND corp_email='" & SSOUserIdentity & "' and Acc_level < 5" 
    Set objRs = objCommand.Execute
    If objRs.EOF Then
    errCode("0011")
    Else
        varuname = objrs("acc_name")
        varpword = objrs("acc_pass")
        Session("SSO_OK") = "Y" 
    End if

    

End if

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


dim varscreenres
varscreenres = validateInput(Request.Form("screenres"))

dim varloginhash
varloginhash = validateInput(Request.Querystring("hash"))

if validateInput(request("type")) = "support" then
   varuname =  validateInput(request("code"))
   Call checkUSERSUPPORT(varuname,varscreenres)  
else
    Call checkUSER(varuname,varpword,varscreenres,varloginhash)
end if

Call EndCONNECTIONS()

' Redirect script once finished.
Response.Redirect(LoginURL)
%>