<!-- #INCLUDE FILE="../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../data/functions/func_random.asp" -->
<% 

dim objrs
dim objconn
dim objcommand


if validateInput(request.querystring("cmd")) = "set" then

    call setRedirection()

elseif validateInput(request.querystring("cmd")) = "get" then

    call getRedirection()

else

    'show the logout page
    response.write "An error occured. Please contact technical support for assistance."

end if




sub setRedirection()

    if session("AUTH") = "Y" Then

        call startConnections()

        var_tab_clicked = request.querystring("tab")
        var_redirect_to = request.querystring("to")
        var_redirect_page = request.querystring("page")
        var_redirect_lang = request.querystring("lang")
        if var_redirect_lang  = "aspx" or var_redirect_lang = "ashx" then  
            var_redirect_lang = "ashx"
        else
            var_redirect_lang = "asp"
        end if
        
        var_session_guid = generateRandom(50)


        For Each Item In Session.Contents 
            if isnull(session(Item)) = false then
                var_item_content = replace(session(Item),"'","`")
            else
                var_item_content = session(Item)
            end if

            objCommand.commandText = "INSERT INTO " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " (corp_code, guid, session_name, session_value) " & _
                                     "VALUES ('" & session("CORP_CODE") & "', '" & var_session_guid & "', '" & Item & "', '" & var_item_content & "')"
            objCommand.execute
        Next 

        objCommand.commandText = "INSERT INTO " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " (corp_code, guid, session_name, session_value) " & _
                                 "VALUES ('" & session("CORP_CODE") & "', '" & var_session_guid & "', 'session_tab_clicked', '" & var_tab_clicked & "')"
        objCommand.execute 

         objCommand.commandText = "INSERT INTO " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " (corp_code, guid, session_name, session_value) " & _
                                 "VALUES ('" & session("CORP_CODE") & "', '" & var_session_guid & "', 'session_page', '" & var_redirect_page & "')"
        objCommand.execute 

       ' objCommand.commandText = "INSERT INTO " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " (corp_code, guid, session_name, session_value) " & _
        '                         "VALUES ('" & session("CORP_CODE") & "', '" & var_session_guid & "', 'session_lang', '" & var_redirect_lang & "')
        'objCommand.execute 
     


        objCommand.commandText = "SELECT domain FROM " & Application("DBTABLE_HR_PLATFORMS") & " WHERE platform = '" & var_redirect_to & "'"
        
        set objRsDirect = objCommand.execute
        if not objRsDirect.EOF then
    
            response.redirect (objRsDirect("domain") & "/version3.2/layout/platform_redirect." & var_redirect_lang & "?cmd=get&guid=" & var_session_guid)

            set objRsDomain = nothing
            call endConnections()

            response.end 

        else
            'send to timeout page

            response.write "An error occured. Please contact technical support."
        end if
    else
            'Not logged in
              response.Redirect "https://www.assessweb.co.uk"
            response.End
            response.write "You must be logged in."
    end if

end sub


sub getRedirection()

    call startConnections()

    objCommand.commandText = "SELECT * FROM " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " WHERE guid='" & validateInput(request.querystring("guid")) & "'"
    set objRsDirect = objCommand.execute
    if not objRsDirect.EOF then

        while not objRsDirect.EOF

            session(objRsDirect("session_name")) = replace(objRsDirect("session_value"),"`","'")

            objRsDirect.movenext
        wend

        set objRsDirect = nothing

        objCommand.commandText = "SELECT platform FROM " & Application("DBTABLE_HR_PLATFORMS") & " WHERE domain = '" & Application("CONFIG_PROVIDER") & "'"
        set objRsPlatform = objCommand.execute
        if not objRsPlatform.EOF then
            session("SESSION_CURRENT_PLATFORM") = objRsPlatform("platform")
        end if

        set objRsPlatform = nothing


        objCommand.commandText = "DELETE FROM " & Application("DBTABLE_HR_PLATFORM_CHANGE") & " WHERE guid = '" & validateInput(request.querystring("guid")) & "'"
        objCommand.execute
        

        For Each Item In Session.Contents 
            if session(Item) = "True" or session(Item) = "False" then
                session(Item) = cbool(session(Item))
            end if
        next

        var_tab_clicked = ucase(session("session_tab_clicked"))

        Session.Contents.Remove("session_tab_clicked")

        if session("revert_new_layout") <> "Y" then
            response.Redirect "../../core/navigation/default.asp?menu=" & var_tab_clicked
            response.End
        end if 

        response.redirect ("default.asp?menu=" & var_tab_clicked)

    else
        'send to timeout page
         response.Redirect "https://www.assessweb.co.uk"
            response.End

        response.write "An error occured. Please contact technical support for assistance."
    end if


    call endConnections()


end sub

%>