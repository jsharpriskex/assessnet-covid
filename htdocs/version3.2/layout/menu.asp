<!-- #INCLUDE FILE="../data/scripts/inc/dbconnections.inc" -->
<%


' kill if no session or login screen
if session("AUTH") = "Y" Then

    Session.contents.remove("MODULE_RR")

if isnull(Session("YOUR_LICENCE")) then Session("YOUR_LICENCE") = ""

    DIM objrs
    DIM Objconn
    DIM Objcommand


    ' ****************************************************
    ' *  This block is duplicated in layout/default.asp  *
    ' *  for the purpose of Application switching        *
    ' ****************************************************

    IF Session("YOUR_ACCESS") = "0" Then
	    var_management = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
    ELSEif Session("YOUR_ACCESS") = "1" and cstr(mid(Session("YOUR_LICENCE"),10,1)) = "1" Then ' added for nbbc
       var_management = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
    ELSEif Session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y" Then ' added primarily for IKEA + global preferences
       var_management = "../modules/management/control/admin_main.asp"
    ELSEIF cstr(mid(Session("YOUR_LICENCE"),24,1)) = "1" or (session("global_usr_perm") <> "Y" and cstr(mid(Session("YOUR_LICENCE"),10,1)) = "1") or (session("global_usr_perm") = "Y" and session("allow_dse_admin")) then ' PTW or DSE ADMIN
       var_management = "../modules/management/control/admin_main.asp"
    ELSEif Session("YOUR_ACCESS") = "1" and session("global_usr_perm") = "Y" Then ' added for permissions
       var_management = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
    else
	    var_management = "javascript:window.alert(""Access Denied - Administrators only"")"
    END IF

    if Session("YOUR_ACCOUNT_ID") = "JARP238075070" or Session("YOUR_ACCOUNT_ID") = "MDS927833822" or session("YOUR_ACCOUNT_ID") = "TION1053526585" or session("YOUR_ACCOUNT_ID") = "FOrNnyjyftf0GgD" or session("YOUR_ACCOUNT_ID") = "XhhB0q5YVH3r3Qg" Then
        var_people_management = "../modules/people/"
    else
        var_people_management = "javascript:window.alert(""Access Denied - Human Resources Only"")"
    end if

    ' ****************************************************
    ' *        End of Application switching block        *
    ' ****************************************************

end if


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
	 <link rel='stylesheet' href="../data/scripts/css/global.css" media="all" />
    <script type='text/javascript' src='../data/scripts/js/global.js'></script>
    
    <script language="JavaScript">

        function accstats() {
			window.open('login_usr_stats.asp', 'window', 'width=400,height=440,screenX=20,screenY=50,left=20,top=50,scrollbars=yes,resizable=no,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
		}
		
		// Status Bar Timer					
		var timerID = null;
		var TimerRunning = false;

		function stop(){
			if(TimerRunning)
			clearTimeout(timerID);
			TimerRunning = false;
		}

		function FB_statusBarDateAndTime() {
					
			var todays_date = new Date();
			var day = todays_date.getDay();
			var date = todays_date.getDate();
			//var date = 2;
			var month = todays_date.getMonth();
			var hours = todays_date.getHours();
			var minutes = todays_date.getMinutes();
			var seconds = todays_date.getSeconds();
					
			var day_name = new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
			var month_name = new Array('January','February','March','April','May','June','July','August','September','October','November','December');
					
			var time_value = "" + ((hours >12) ? hours -12 :hours)
			time_value += ((minutes < 10) ? ":0" : ":") + minutes
			time_value += ((seconds < 10) ? ":0" : ":") + seconds
			time_value += (hours >= 12) ? " PM" : " AM"
						
			if(date==1||date==21||date==31){
				date+="st"
			} else if(date==2||date==22){
				date+="nd"
			} else if(date==3||date==23){
				date+="rd"
			} else {
				date+="th"
			}
			var date_value = day_name[day]+" "+date+" "+month_name[month];
					
			window.status = 'AssessNET (TM)   |   '+date_value+'   |   '+time_value;
			timerID = setTimeout("FB_statusBarDateAndTime()",100);
			TimerRunning = true;
		}

					
		function start() {
			stop();
			FB_statusBarDateAndTime();
		}




        var session_home_platform = "<% =session("SESSION_HOME_PLATFORM") %>"
	    var session_current_platform = "<% =session("SESSION_CURRENT_PLATFORM") %>"

        var current_user = "<% =session("YOUR_ACCOUNT_ID") %>"

        function check_platform(tabClicked) {


//if (current_user == "LbdHyMoiNefoEGe" || current_user == "ezkqttcQpBfHmhR" || current_user == "vxogSrrkv0BGExm") {

                if (tabClicked == "hlp") {
                    if (session_home_platform != "App1") {
                        //call platform redirect
                        parent.location.assign("platform_redirect.asp?cmd=set&tab=" + tabClicked + "&to=App1");
                    }
                } else if (session_home_platform != session_current_platform) {
                    //call platform redirect
                    parent.location.assign("platform_redirect.asp?cmd=set&tab=" + tabClicked + "&to=" + session_home_platform);
                }

//}


            if (tabClicked == "ac") {
                parent.menu.location.href = "menu.asp?menu=AC";
            } else if (tabClicked == "tm") {
                parent.menu.location.href = "menu.asp?menu=TM";
            } else if (tabClicked == "hs") {
                parent.menu.location.href = "menu.asp?menu=HS";
            } else if (tabClicked == "env") {
                parent.menu.location.href = "menu.asp?menu=ENV";
            } else if (tabClicked == "qua") {
                parent.menu.location.href = "menu.asp?menu=QUA";
            } else if (tabClicked == "risk") {
                parent.menu.location.href = "menu.asp?menu=RISK";
            } else if (tabClicked == "pep") {
                parent.menu.location.href = "menu.asp?menu=PEP";
            } else if (tabClicked == "bt") {
                parent.menu.location.href = "menu.asp?menu=BT";
            } else if (tabClicked == "set") {
                parent.menu.location.href = "menu.asp?menu=SET";
            } else if (tabClicked == "hlp") {
                parent.menu.location.href = "menu.asp?menu=HLP";
            } else if (tabClicked == "hr") {
                parent.menu.location.href = "menu.asp?menu=HR";
            }

        }



				//	start();
		
    </script>


<style>

<% 
    var_logo_found = false
    set fs = CreateObject("Scripting.FileSystemObject")
        
    if len(session("demo_company_id")) > 0 then
        if fs.FileExists(server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & session("demo_company_id") & ".jpg")) then
            var_logo_str = "../data/documents/pdf_centre/img/corp_logos/" & session("demo_company_id") & ".jpg"
            var_logo_found = true
        end if
    elseif fs.FileExists(server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & "_3_" & Session("YOUR_LOCATION") & ".jpg")) then
         var_logo_str = "../data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & "_3_" & Session("YOUR_LOCATION") & ".jpg"
         var_logo_found = true
    elseif fs.FileExists(server.MapPath("/version3.2/data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & ".jpg")) then
         var_logo_str = "../data/documents/pdf_centre/img/corp_logos/" & session("corp_code") & ".jpg"
         var_logo_found = true
    end if


if var_logo_found then
%>

BODY {
background-color: #ffffff;
background: #ffffff url(<% =var_logo_str %>) no-repeat top right;
margin: 0px;
}

<% else %>

BODY {
background-color: #ffffff;
background: #ffffff;
margin: 0px;
}

<% end if %>

<% if session("EDITION") = "RN" then  %>
    TD.onbut{border-bottom: 1px solid #2db5e1;}
<% else %>
    TD.onbut{border-bottom: 1px solid #DD2A24;}
<% end if %>

    TD.offbut {border-bottom: 1px solid #888888;}


TD.menu_info {
color: #ffffff ;
}

A.nocss:link { color: #ffffff; font-weight: normal; }
A.nocss:visited { color: #ffffff; font-weight: normal; }
A.nocss:hover { color: #ffffff; font-weight: normal; }



</style>

	</head>
	<body topmargin="0">
		<table width='100%' cellpadding='0' cellspacing='0' >
		<tr>
			<td colspan='<% 
			
			if session("EDITION") = "RN" then 
			    response.write "7" 
			else
                if Len(Session("MODULE_RR")) > 0 then
                    response.write "12"
                else
                    response.write "11" 
                end if
			end if 
			
			%>' height='50' align='left' class='menu_info'>
			
			<%
			
			' kill if no session or login screen
            if session("AUTH") = "Y" Then
			
			 %>
			
			<table width='100%' cellpadding='6' >
			<tr>
				<td nowrap>
				<% if session("EDITION") = "RN" then  %>
				    <img src='mycus-logosml.gif' />
				<% else %>
        			<img src='assessnet-logosml.gif' />
        	    <% end if %>
				</td>
			</tr>
			</table>	

    		<% end if %>
			

				
			</td>
			<td class='offbut menu_info' align='right' valign='middle' nowrap rowspan='2' height='84'> 

            <% if session("EDITION") = "RN" Then %>

            	<% if len(request("ab_frm_srch_company")) > 0 then	%>   
				   
				   <script language='javascript'>
				   parent.main.location.href = "http://www.assessweb.co.uk/version3.2/modules/hresources/modules/address_book/app/addressbook_search.asp?cmd=global&ab_frm_srch_company=<% =request("ab_frm_srch_company") %>"
				   </script>
				    
				<% end if %>				
												
				<form action='menu.asp' method='post'>
				
				<table cellpadding='2' cellspacing='2'>
				<tr><td align='left'>

				<span style='color:#000;'>Search for a contact</span></td></tr>
				<tr><td><input type='text' name='ab_frm_srch_company' class='text' value='' size='30' /> <input type='submit' name='frm_submit' value='Search' />&nbsp;&nbsp;</td></tr>		
				</table>
				
				</form>  


            <% elseif session("corp_code") = "222999" or session("corp_code") = "200200" then %>
        
                &nbsp;
                
                <% else %>


            <% end if %>

			</td>
		</tr>
			<tr>
				<td class='offbut' width='10'>
				
					&nbsp;
				
				</td>
				
				<%
			
			
			    ' kill if no session or login screen
                if session("AUTH") = "Y" Then
                
				
				SELECT CASE UCASE(Request.Querystring("menu"))
					CASE "AC"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_on_mc.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_on.png' onclick='check_platform(""ac"");' /></a></td>"
					  
                        if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if 

                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if					
					    
					
					CASE "TM"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set""); set();' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""sup"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
                    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
                        end if
				        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
					CASE "PEP"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
						Response.write "<td class='onbut' valign='bottom' width='10'><a href='" & var_people_management & "' target='main' ><img src='menu_but/people_on_mc.png' onclick='check_platform(""pep"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if                        
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
			            Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
					
					CASE "HS"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if

					CASE "RISK"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_on.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_on.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_on.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='qua' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='set' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if

					CASE "ENV"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_on.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_on.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_on.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if

                    CASE "QUA"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
					CASE "BT"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_on.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
					    
				
					
					CASE "SET"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_on_mc.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_on.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_on.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_on.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
			
					
					CASE "HLP"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_on_mc.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_on.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_on.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_on.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
					
					CASE "HR"
					
					if session("EDITION") = "RN" Then	
						Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_on_mc.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_off.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and  not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_on.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_on.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
				    
					CASE ELSE
                   
					if session("EDITION") = "RN" Then	
						Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_on_mc.png' onclick='check_platform(""ac"");' /></a></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
					elseif session("YOUR_ACCESS") < 3 Then		
					    Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/acentre/' target='main' ><img src='menu_but/home_on.png' onclick='check_platform(""ac"");' /></a></td>"
					    if session("global_usr_perm") = "Y" and not session("user_tm_active")  then
                        else
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
					    end if
                        Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
                        Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/btools/' target='main' ><img src='menu_but/tools_off.png' onclick='check_platform(""bt"");' /></td>"
                        if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='" & var_management & "' target='main' ><img src='menu_but/manager_off.png' onclick='check_platform(""hr"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 3 Then	
				        if session("DIST_LIST") = "Y" then
                            Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_off.png' onclick='check_platform(""tm"");' /></td>"
                        end if				        
                        Response.write "<td class='onbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_on.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    elseif session("YOUR_ACCESS") = 4 Then	
				        Response.write "<td class='onbut' valign='bottom' width='10'><a href='../modules/management/taskmanager_v2/task_layout.asp' target='main' ><img src='menu_but/tasks_on.png' onclick='check_platform(""tm"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/hsafety/' target='main' ><img src='menu_but/hands_off.png' onclick='check_platform(""hs"");' /></td>"
					    if Len(Session("MODULE_RR")) > 0 then
                            Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/risk/' target='main' ><img src='menu_but/risk_off.png' onclick='check_platform(""risk"");' /></td>"
                        end if
                        if session("menu_enviro_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/enviro/' target='main' ><img src='menu_but/env_off.png' onclick='check_platform(""env"");' /></td>"
                        if session("menu_qual_off") <> "Y" then Response.write "<td class='offbut' valign='bottom'  width='10'><a href='../modules/quality/' target='main' ><img src='menu_but/quality_off.png' onclick='check_platform(""qua"");' /></td>"
					    if Session("disable_settings") <> "Y" then Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/hresources/modules/cpanel/user_manager_version.asp' target='main' ><img src='menu_but/settings_off.png' onclick='check_platform(""set"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom' width='10'><a href='../modules/acentre/acentre_services.asp' target='main' ><img src='menu_but/help_off.png' onclick='check_platform(""hlp"");' /></td>"
					    Response.write "<td class='offbut' valign='bottom'><a href='../data/functions/func_exit.asp' target='_parent' ><img src='menu_but/logoff_off.png' /></td>"
				    end if
                    				
				END SELECT 
				
				else
				' if session is killed or login screen
				
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
				    Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
					Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
                    Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
                    Response.write "<td class='offbut' valign='bottom' width='10'>&nbsp;</td>"
								
				end if
				
				%>
				
				
			</tr>


			<tr>
			<% if session("EDITION") = "RN" then  %>
				<td colspan='10' style='background-color: #2db5e1; border-bottom: 1px solid #CBBB9A;'align='right' valign='middle' height='5'><span style='font-size:3px;'>&nbsp;</span></td>
			<% else %>
                <% if Len(Session("MODULE_RR")) > 0 then %>
                    <td colspan='13' style='background-color: #E1342D; border-bottom: 1px solid #CBBB9A;'align='right' valign='middle' height='5'><span style='font-size:3px;'>&nbsp;</span></td>
                <% else %>
				    <td colspan='12' style='background-color: #E1342D; border-bottom: 1px solid #CBBB9A;'align='right' valign='middle' height='5'><span style='font-size:3px;'>&nbsp;</span></td>
                <% end if %>
			<% end if %>
			</tr>
		</table>
		<table height='200' width='100%' style='background-color: #ffffff;'>
		<tr>
			<td height='200' width='100%'>
			
			&nbsp;
			
			</td>
		</tr>
		</table>
	</body>
</html>
