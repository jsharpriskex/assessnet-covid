<!-- #INCLUDE FILE="../data/scripts/inc/dbconnections.inc" -->

<%

    ' we're on the right server, so open the requested tab as normal
    var_menu_tab = request.querystring("menu")

    select case var_menu_tab
        case "AC"
            var_menu_page = "menu.asp?menu=AC"
            var_main_page = "../modules/acentre/"

        case "TM"
            var_menu_page = "menu.asp?menu=TM"
            var_main_page = "../modules/management/taskmanager_v2/task_layout.asp"

        case "PEP"
            var_menu_page = "menu.asp?menu=PEP"

            ' ** Also found in layout/menu.asp **
            if Session("YOUR_ACCOUNT_ID") = "JARP238075070" or Session("YOUR_ACCOUNT_ID") = "MDS927833822" or session("YOUR_ACCOUNT_ID") = "TION1053526585" or session("YOUR_ACCOUNT_ID") = "FOrNnyjyftf0GgD" or session("YOUR_ACCOUNT_ID") = "XhhB0q5YVH3r3Qg" Then
                var_main_page = "../modules/people/"
            else
                var_main_page = "javascript:window.alert(""Access Denied - Human Resources Only"")"
            end if

        case "HS"
            var_menu_page = "menu.asp?menu=HS"
            var_main_page = "../modules/hsafety/"

        case "RISK"
            var_menu_page = "menu.asp?menu=RISK"
            var_main_page = "../modules/risk/"

        case "ENV"
            var_menu_page = "menu.asp?menu=ENV"
            var_main_page = "../modules/enviro/"

        case "QUA"
            var_menu_page = "menu.asp?menu=QUA"
            var_main_page = "../modules/quality/"

        case "BT"
            var_menu_page = "menu.asp?menu=BT"
            var_main_page = "../modules/btools/"

        case "SET"
            var_menu_page = "menu.asp?menu=SET"
            var_main_page = "../modules/hresources/modules/cpanel/user_manager_v3/user_control.asp?myset=yes&type=Open"

        case "HLP"
            var_menu_page = "menu.asp?menu=HLP"
            var_main_page = "../modules/acentre/acentre_services.asp"

        case "HR"
            var_menu_page = "menu.asp?menu=HR"

            ' ** Also found in layout/menu.asp **
            IF Session("YOUR_ACCESS") = "0" Then
	            var_main_page = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
            ELSEif Session("YOUR_ACCESS") = "1" and cstr(mid(Session("YOUR_LICENCE"),10,1)) = "1" Then ' added for nbbc
               var_main_page = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
            ELSEif Session("YOUR_ACCESS") = "1" and session("LA_USER_CTRL") = "Y" Then ' added primarily for IKEA + global preferences
               var_main_page = "../modules/management/control/admin_main.asp"
            ELSEIF cstr(mid(Session("YOUR_LICENCE"),24,1)) = "1" or (session("global_usr_perm") <> "Y" and cstr(mid(Session("YOUR_LICENCE"),10,1)) = "1") or (session("global_usr_perm") = "Y" and session("allow_dse_admin")) then ' PTW or DSE ADMIN
               var_main_page = "../modules/management/control/admin_main.asp"
            ELSEif Session("YOUR_ACCESS") = "1" and session("global_usr_perm") = "Y" Then ' added for permissions
               var_main_page = "../modules/management/control/admin_main.asp" '"../modules/hresources/modules/cpanel/"
            else
	            var_main_page = "javascript:window.alert(""Access Denied - Administrators only"")"
            END IF

        case else
            var_menu_page = "menu.asp"
            var_main_page = "../modules/acentre/"

    end select

    dim objrs
    dim objconn
    dim objcommand 
    
    dim var_fail_reason                   

    '**************************************************************************************
    '*** Maintenance Block for SSO Users. Normal users are blocked on AssN Login Page
    call startConnections()

    if left(ucase(session("YOUR_USERNAME")),8) <> "SYSADMIN" and left(ucase(session("YOUR_USERNAME")),11) <> "RTECHNICIAN" and left(ucase(session("YOUR_USERNAME")),13) <> "STRUCTUREUSER" then
        objCommand.commandText = "SELECT maintenance FROM Module_HR.dbo.HR_Systems WHERE corp_code = '" & session("CORP_CODE") & "'"
        set objRs = objCommand.execute
        if objRs("maintenance") = true then
            response.redirect("../security/login/asnet_maintenance.asp")
            response.End
        end if
    end if
    call endConnections()
    '**************************************************************************************

    if session("revert_new_layout") <> "Y" then
        response.Redirect("../../core/navigation/default.asp?cmd=" & request.QueryString("cmd") & "&action=" & request.QueryString("action"))
        response.End
    end if

%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-Frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
  <title>AssessNET Online Health and Safety Management</title>
</head>

<frameset rows="90,*">
        
        <%if request.QueryString("cmd") = "news" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=HLP" />
            <% if request.QueryString("action") = "add" then %>
                <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/acentre/services/news/news.asp?action=add" />"   
            <% else %>
                <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/acentre/services/news/news.asp" />"   
            <% end if
        elseif session("YOUR_ACCESS") = "4" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=TM" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/management/taskmanager_v2/tasks.asp" />"
        <%elseif session("YOUR_ACCESS") = "3" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=HS" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hsafety/" />"
        <%elseif request.QueryString("cmd") = "acc" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=HS" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hsafety/accident/app/accident_control.asp" />"
        <%elseif request.QueryString("cmd") = "acc_home" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=HS" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hsafety/accident/" />"
        <%elseif request.QueryString("cmd") = "spars" then %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp?menu=HS" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hsafety/suser_ra/app/suser_rassessment_search.asp" />"
        <%elseif request.QueryString("cmd") = "support" then %>
             <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hresources/modules/dev_manager/default.asp?cat=support" />"
        <%elseif request.QueryString("cmd") = "compliance" then %>
             <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/hsafety/compliance/default.asp" />"        
         <%elseif len(request.QueryString("dest_page")) > 4 then %>
             <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="<%=request.querystring("dest_page") %>" />"  
         <%elseif len(Session("EMAIL_REDIRECTION_EMAIL")) > 4 then %>
             <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/<%=Session("EMAIL_REDIRECTION_EMAIL") %>" />"  
        <%elseif len(var_menu_page) > 0 then %>
             <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="<% =var_menu_page %>" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="<% =var_main_page %>" />"  
        <% else %>
            <frame frameborder="0" name="menu"  scrolling="no" noresize="noresize" src="menu.asp" />
            <frame frameborder="0" name="main" scrolling="yes" noresize="noresize" src="../modules/acentre/" />
        <% end if %>
        
        <%

        ' clear the redirection session
        Session.Contents.Remove("EMAIL_REDIRECTION_EMAIL")
        
        %>

</frameset>

</html>
