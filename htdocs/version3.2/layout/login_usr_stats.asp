<!-- #include file="../data/scripts/inc/dbconnections.inc" -->
<%

'Global Strings
DIM objRs
DIM Objconn
DIM Objcommand
DIM RowNum

IF Len(Request.form("frm_users")) > 0 Then
var_user = Request.Form("frm_users")
	ELSE 
	var_user = Session("YOUR_USERNAME")
END IF

IF Session("YOUR_ACCESS") = "0" Then
var_disable = ""
	ELSE
	var_disable = "DISABLED"
END IF


CALL StartConnections()

SUB Showusers(var_user)

IF Session("YOUR_ACCESS") = "0" Then
Response.Write "<SELECT name='FRM_USERS' onChange='submit()' " & var_disable & ">"
	ELSE
	Response.Write "<SELECT name='FRM_USERS' onChange='submit()' " & var_disable & " onClick='window.alert(""Only admins can view other users."")'>"
END IF

Objcommand.commandtext = "SELECT acc_name, per_fname, per_sname FROM " & Application("DBTABLE_USER_DATA") & " WHERE CORP_CODE = '" & Session("CORP_CODE") & "' " & _
						 "AND Acc_level < 5 AND acc_status NOT LIKE 'D' ORDER BY per_sname ASC"
SET Objrs = Objcommand.execute

IF Objrs.EOF Then
Response.Write "<OPTION value='NA'>- No users found contact support -</OPTION>"
ELSE
	While NOT Objrs.EOF


		IF Objrs("acc_name") = var_user Then
		Response.Write "<OPTION value='" & Objrs("acc_name") & "' SELECTED>" & UCASE(objrs("per_sname")) & ", " & objrs("per_fname") & "</OPTION>"
			ELSE
		Response.Write "<OPTION value='" & Objrs("acc_name") & "'>" & UCASE(objrs("per_sname")) & ", " & objrs("per_fname") & "</OPTION>"
		END IF

	objrs.movenext
	Wend
END IF

Response.Write "</SELECT>"

END SUB



SUB ShowLog(var_user)

Objcommand.commandtext = "SELECT TOP 10 DATETIME,USERNAME,IP_ADDRESS FROM " & Application("DBTABLE_LOG_ACCESS") & " WHERE SUCCESS = '1' AND USERNAME = '" & var_user & "' ORDER BY ID DESC"
SET Objrs = Objcommand.execute

IF Objrs.EOF Then

Response.Write	"<table width='100%' cellpadding='2' >" & _
				"<tr>" & _
				"<td align='center'><strong>There is currently no history of previous logins</strong></td>" & _
				"</tr>" & _
				"</table>"

ELSE
	Response.Write	"<table class='hrows' width='100%' cellpadding='2'> " & _
					"<tr>" & _
					"<th nowrap><strong>Date / Time</strong></th>" & _
					"<th nowrap><strong>User</strong></th>" & _
					"<th nowrap><strong>IP Address</strong></th>" & _
					"</tr>"
	While NOT Objrs.EOF

	RowNum = RowNum + 1
	
	Response.Write  "<tr>" & _
					"<td>" & objrs("DATETIME") & "</td>" & _
					"<td>" & objrs("USERNAME") & "</td>" & _
					"<td>" & Objrs("IP_ADDRESS") & "</td>" & _
					"</tr>"

	objrs.movenext
	Wend
	Response.Write "</table>"
END IF

END SUB

Objcommand.commandtext = "SELECT COUNT(ID) as total_count FROM " & Application("DBTABLE_LOG_ACCESS") & " WHERE YEAR(Datetime) = '" & Year(date) & "' AND SUCCESS = '1' AND USERNAME = '" & var_user & "'"
SET Objrs = Objcommand.execute

var_cnt_total = Objrs("total_count")
username_arry = Split(var_user,".")

Objcommand.commandtext = "SELECT COUNT(ID) as usage_count FROM " & Application("DBTABLE_LOG_ACCESS") & " WHERE YEAR(Datetime) = '" & Year(date) & "' AND SUCCESS = '1' AND USERNAME LIKE '%." & username_arry(1) & "'"
SET Objrs = Objcommand.execute

var_cnt_usage = Round((var_cnt_total / objrs("usage_count")) * 100,1)


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<title>AssessNET Login statistics</title>
		<link rel='stylesheet' href="../data/scripts/css/global.css" media="all" />
		<script type='text/javascript' src='../data/scripts/js/global.js'></script>
		<style>

BODY {

margin: 0px;
}

TD.onbut{border-bottom: 1px solid #E1342D;}
TD.offbut {border-bottom: 1px solid #CBBB94;}

TD.menu_info {
color: #ffffff ;
background-color: #333366;
}

</style>
	</head>
	<body topmargin="0">
		<form action='login_usr_stats.asp' method='post'>
			<table width='100%' cellpadding='0' cellspacing='0'>
				<tr>
					<td height='50' align='left' class='menu_info'>
						&nbsp;<strong>Select user</strong><br />
						&nbsp;<% CALL Showusers(var_user) %>
					</td>
				</tr>
				<tr>
					<td style='background-color: #E1342D; border-bottom: 1px solid #CBBB9A;' height='5'><span style='font-size:3px;'>&nbsp;</span></td>
				</tr>
			</table>
			
			<br />
			&nbsp;&nbsp;<strong>Login Statistics (<% =Year(date) %>)</strong>
			<p>
			&nbsp;&nbsp;<strong>Total : </strong><span class='alt'><% =var_cnt_total %></span> <br />
			&nbsp;&nbsp;<strong>Usage % : </strong><span class='alt'><% =var_cnt_usage %></span>
			</p>		
			
			<table width='100%' cellpadding='5'>
				<tr>
					<td>
						<% 
			
			CALL ShowLog(var_user)
			
			%>
					</td>
				</tr>
			</table>
			
			
			<p align='center'><strong>Currently showing the last <% =RowNum %> logins</strong></p>
			
			
		</form>
	</body>
</html>
<%

CALL EndConnections()

%>
