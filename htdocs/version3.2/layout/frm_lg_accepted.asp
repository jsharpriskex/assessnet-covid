<!-- #include file="../../data/scripts/inc/dbconnections.inc" -->
<%

' ok run a check to see if they have made it all the way through
if len(session("AUTH")) > 0 Then


DIM ary_usertype(3)

ary_usertype(0) = "Global Administrator"
ary_usertype(1) = "Local Administrator"
ary_usertype(2) = "Assessor"
ary_usertype(3) = "Read Only"

DIM ary_userdesc(3)

ary_userdesc(0) = "Full permissions on users and records"
ary_userdesc(1) = "Full permissions on users and records in your location"
ary_userdesc(2) = "Ability to edit records you own and view others"
ary_userdesc(3) = "Ability to view any selected records only"

var_usertype = ary_usertype(Session("YOUR_ACCESS"))
var_usertype_desc = ary_userdesc(Session("YOUR_ACCESS"))


Dim objConn
Dim objCommand
Dim objRs

CALL StartCONNECTIONS()

' Get the last login date and time
Objcommand.commandtext = "SELECT TOP 2 datetime FROM " & Application("DBTABLE_LOG_ACCESS") & " WHERE username = '" & Session("YOUR_USERNAME") & "' ORDER BY ID DESC"

SET Objrs = Objcommand.execute

    IF Objrs.EOF Then
    int_lastlogin = "This is the first time you have used your AssessNET account, welcome..."
    ELSE
		While NOT Objrs.eof
		Recordcount = Recordcount + 1
		
		IF Recordcount = 2 Then
        varLastLogin = "Y"
        var_logindate = left(objrs("datetime"),10)
        var_logintime = right(objrs("datetime"),8)
		End IF
		
		Objrs.movenext
		Wend
		int_lastlogin = var_logindate & " at " & var_logintime 
    END IF
    
    
' now check the tasks    
Objcommand.commandtext =  "SELECT count(id) as task_overdue FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                           "WHERE CORP_CODE = '" & SESSION("CORP_CODE") & "' AND TASK_ACCOUNT LIKE '" & Session("YOUR_ACCOUNT_ID") & "' AND  task_status > '0' AND task_due_date < getdate() "
SET Objrs = Objcommand.execute  
int_overdue = objrs("task_overdue")
 

Objcommand.commandtext =  "SELECT count(id) as task_due FROM " & Application("DBTABLE_TASK_MANAGEMENT") & " " & _
                                           "WHERE CORP_CODE = '" & SESSION("CORP_CODE") & "' AND TASK_ACCOUNT LIKE '" & Session("YOUR_ACCOUNT_ID") & "' AND  task_status > '0' AND task_due_date BETWEEN getdate() AND '" & dateadd("w",4,date()) & "' "
SET Objrs = Objcommand.execute  
int_nextfourweeks = objrs("task_due")


' set the timeout
if len(request.form("frm_timeout")) > 0 then
    objcommand.commandtext = "UPDATE " & Application("DBTABLE_USER_DATA") & " SET acc_timeout = '" & request("frm_timeout") & "' WHERE CORP_CODE = '" & Session("CORP_CODE") & "' AND Acc_ref = '" & Session("YOUR_ACCOUNT_ID") & "' AND acc_level < 4"
    objcommand.execute

    ' set timeout now its changed
    Session.timeout = request.Form("frm_timeout")
end if
    
    
else

   Session.Abandon
   Response.Redirect "frm_lg_entry.asp"
   Response.End

end if

CALL EndCONNECTIONS()

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="css/login.css" media="all" />    

<!-- Knowledge base window -->
<script language='javascript' type="text/javascript">
function openKnowledgebase() {
    window.open('../../../../external/support/', 'window', 'width=750,height=550,screenX=750,screenY=550,left=10,top=10,scrollbars=yes,resizable=yes,toolbar=no,location=no,directories=no,status=no,menubar=no,copyhistory=no');
}


</script>
<!-- script end --->

</head>
<body>

<% if Session("EDITION") = "SME" then %>
<div id='menuclonebrz' align='right'>
    <img src='../../layout/asnet_title_brz.sml.gif' />
    <% ELSE %>
    <div id='menuclone' align='right'>
    <img src='../../layout/asnet_title_wht.gif' /><br />
	Corporate Edition &nbsp;
	<% END IF %>
</div>

<!-- Box -->
<div id='box_top_brn'>
<div id='box_bottom_brn'>
<div id='box_main'>

    <form action='frm_lg_accepted.asp' method='post' id='reg3' >
    
    <div id='reg'>
    <h1>Login <strong>successful</strong></h1>
    <h6>You are about to leave this secure area as encryption is no longer required.</h6>

    <p>Welcome to AssessNET. Below are two options allowing you to either continue directly into AssessNET or to activate the <strong>Knowledge Base</strong> which will open alongside your session, thus allowing you to reference the manuals for guidance <em>(Recommended for first-time users).</em></p>

    <input type="hidden" name="destination" value="/frm_lg_accepted.asp" />
    
    <table width='100%' class='nostyle' cellpadding='5' cellspacing='0'>
    <tr><td>
    
    <div id='box_top_blu_sm'>
    <div id='box_bot_blu_sm'>
    <div id='box_main_blu'>
    
    <p>Continue <strong>without</strong> online manual<br /><br />
    <a href='<% =Application("CONFIG_PROVIDER") %>/version<% =Application("CONFIG_VERSION") %>/layout/<%if ucase(Session("YOUR_USERNAME")) = "TPHELPS.BCC" or ucase(Session("YOUR_USERNAME")) = "MLEGG.BCC"   then response.write "default.asp?cmd=acc"  elseif ucase(Session("YOUR_USERNAME")) = "TANGEL.BCC" or ucase(Session("YOUR_USERNAME")) = "STOWSEY.BCC" or ucase(Session("YOUR_USERNAME")) = "ATAYLOR.BCC" or ucase(Session("YOUR_USERNAME")) = "KJEAKINGS.BCC"  or ucase(Session("YOUR_USERNAME")) = "HCOURSE.BCC"  or ucase(Session("YOUR_USERNAME")) = "LAYLEN.BCC"  or ucase(Session("YOUR_USERNAME")) = "TPARKINSON.BPT"   then response.write "default.asp?cmd=acc_home" %>'><img src='img/but_lrg_continue.gif' title='Continue without manual' /></a></p>
    
    </div>
    </div>
    </div>
    
    </td><td>
    
    <div id='box_top_red_sm'>
    <div id='box_bot_red_sm'>
    <div id='box_main_red'>
    
    <p>Continue <strong>with</strong> online manual<br /><br />
    <a href='<% =Application("CONFIG_PROVIDER") %>/version<% =Application("CONFIG_VERSION") %>/layout/'><img src='img/but_lrg_continue.gif' title='Continue with manual' onclick='openKnowledgebase();'/></a></p>

    </div>
    </div>
    </div>

    </td></tr>
    </table>

    <table cellpadding="5" cellspacing="0" border="0">
    <tr align="left">
    <% if Session("EDITION") <> "SME" then %>
    <th>Your tasks</th><td><strong><% =int_overdue %></strong> Overdue | <strong><% =int_nextfourweeks %></strong> within the next four weeks</td></tr>
    <% else %>
    <th>Version</th><td><strong>Bronze Edition</strong></td></tr>
    <% end if %>
    <tr align="left">
    <th>Logout if idle after</th><td><select name='frm_timeout' onchange='submit()'><option value='15' <% if Session.timeout = "15" then response.write "selected" end if %>>15 mins</option><option value='20' <% if Session.timeout = "20" then response.write "selected" end if %>>20 mins (recommended)</option><option value='30' <% if Session.timeout = "30" then response.write "selected" end if %>>30 mins</option><option value='40' <% if Session.timeout = "40" then response.write "selected" end if %>>40 mins</option><option value='50' <% if Session.timeout = "50" then response.write "selected" end if %>>50 mins</option></select></td></tr>
    <tr align="left">
    <th>Your account type</th><td><strong><% =var_usertype %></strong> <em>(<% =var_usertype_desc %>)</em></td></tr>
    <tr align="left">
    <th>Last login date / time</th><td><% =int_lastlogin %></td></tr>
    </table>

    </div>
    
    </form>

    
</div>
</div>
</div>
<!-- end box -->

<div id='statement'>
    <p>The data held on our system is PRIVATE PROPERTY. Unauthorised entry contravenes the Computer Misuse Act 1990 and may incur criminal penalties and damage, all login attempts are logged and monitored. <a href='asnet_privacy-07-06.asp'>privacy policy</a></p> 
</div>

</body>
</html>