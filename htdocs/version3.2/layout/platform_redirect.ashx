﻿<%@ WebHandler Language="C#" Class="platform_redirect" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using ANET;
using System.Web.SessionState;
using ASNETNSP;

public class platform_redirect : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {


        if (context.Request.QueryString["cmd"] == "get")
        {
            getRedirection(context, TextValidation.ValidateInput(context.Request.QueryString["guid"]));
        }
        else
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Invalid Command");
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public void getRedirection(HttpContext context, String guid)
    {


        using (SqlConnection dbconn = new SqlConnection(ConfigurationManager.ConnectionStrings["MyDbConn"].ConnectionString))
        {
            dbconn.Open();

            using (SqlCommand sqlcomm = new SqlCommand("", dbconn))
            {
                sqlcomm.CommandText = "SELECT * FROM " + context.Application["DBTABLE_HR_PLATFORM_CHANGE"] + " WHERE guid='" + guid + "'";

                using (SqlDataReader objRsDirect = sqlcomm.ExecuteReader())
                {
                    while (objRsDirect.Read())
                    {

                        context.Session[objRsDirect["session_name"].ToString()] = objRsDirect["session_value"].ToString().Replace("`", "'");

                    }

                }


                sqlcomm.CommandText = "SELECT platform FROM " + context.Application["DBTABLE_HR_PLATFORMS"] + " WHERE domain='" + context.Application["CONFIG_PROVIDER"] + "'";
                context.Session["SESSION_CURRENT_PLATFORM"] = sqlcomm.ExecuteScalar();

                sqlcomm.CommandText = "DELETE FROM " + context.Application["DBTABLE_HR_PLATFORM_CHANGE"] + " WHERE guid='" + guid + "'";

                sqlcomm.ExecuteNonQuery();

            }
        }

        //try {
        //int Item;


        //for(Item=0;Item<context.Session.Contents.Count;Item++)
        //{

        //    var keyType = context.Session[Item].GetType();

        //    if (keyType == typeof(string))
        //    {
        //        if (context.Session[Item].ToString() == "True" || context.Session[Item].ToString() == "False")
        //        {
        //            context.Session[context.Session.Keys[Item]] = Convert.ToBoolean(context.Session[Item]);
        //        }
        //    }



        //}
        //}
        //catch (System.Exception e)
        //{

        //    string a = e.Message + "<br /><br /><br /><br />" + e.StackTrace + "<br /><br/><br /><br />" + e.InnerException;
        //    Messages.SendEmail("m.green@riskex.co.uk", "Platform Redirect Error", "Platform Redirect Error", a, "", true);

        //}


        string var_tab_clicked = context.Session["session_tab_clicked"].ToString().ToUpper();
        context.Session.Contents.Remove("session_tab_clicked");



        string var_target_page = context.Session["session_page"].ToString();
        context.Session.Contents.Remove("session_page");


        if (var_tab_clicked == "" || var_tab_clicked.ToUpper() == "NONE") // Just a redirect - No menu available 
        {
            context.Response.Redirect("../" + var_target_page);
        }
        else
        {
            context.Response.Redirect("default.aspx?menu=" + var_tab_clicked);
        }


    }

}