<!-- #INCLUDE FILE="../data/scripts/inc/dbconnections.inc" -->
<!-- #INCLUDE FILE="../data/scripts/inc/auto_logout.inc" -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-Frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<% 

dim objrs
dim objconn
dim objcommand

    var_corp_code = validateInput(request("c"))
    var_demo_code = validateInput(request("d"))
    var_random_string = validateInput(request("f"))
    var_rowid = validateInput(request("i"))
    var_acc_ref = validateInput(request("a"))
    var_corp_string = validateInput(request("r"))
    var_module = validateInput(request("m"))
    var_credentials_validated = validateInput(request("cred"))
    var_acc_accb_reference = validateInput(request("IC"))
    var_acc_record_reference = validateInput(request("ACC"))
    var_acc_ref_tmx = validateInput(request("tmxa"))
    var_acc_ref_self = validateInput(request("selfa"))

    var_lang_hreturn = validateInput(request("l"))


    var_email_login = validateInput(request("u"))


    call startConnections()

    Application("DBTABLE_HR_LANGUAGES") = Application("CONFIG_DBASE") & "_HR.dbo.HR_Data_languages"

    if session("LANDING_MODULE") = "INV" then
        var_redirect = "../../portal_login_sessions.asp?c=" & var_corp_code & "&r=" & var_corp_string & "&f=" & var_random_string & "&i=" & var_rowid & "&a=" & var_acc_ref & "&d=" & var_demo_code & "&l=" & var_lang_hreturn & "&IC=" & var_acc_accb_reference & "&ACC=" & var_acc_record_reference
     elseif session("LANDING_MODULE") = "TMX" then
        var_redirect = "portal_login_sessions.asp?c=" & var_corp_code & "&r=" & var_corp_string & "&f=" & var_random_string & "&i=" & var_rowid & "&a=" & var_acc_ref & "&d=" & var_demo_code & "&tmxa=" & var_acc_ref_tmx
    elseif session("LANDING_MODULE") = "SELF" then
        var_redirect = "portal_login_sessions.asp?c=" & var_corp_code & "&r=" & var_corp_string & "&f=" & var_random_string & "&i=" & var_rowid & "&a=" & var_acc_ref & "&d=" & var_demo_code & "&selfa=" & var_acc_ref_self
    else
        var_redirect = "../../portal_login_sessions.asp?c=" & var_corp_code & "&r=" & var_corp_string & "&f=" & var_random_string & "&i=" & var_rowid & "&a=" & var_acc_ref & "&d=" & var_demo_code & "&l=" & var_lang_hreturn
    end if
    


%>
<head>
    <title><% =Application("SoftwareName") %>&nbsp;Copyright &copy; 2000 - <% =year(now) %>&nbsp;<% =Application("SoftwareOwner") %>&nbsp;<% =Application("SoftwareOwnerSite") %></title>
    <link rel='stylesheet' href="../data/scripts/css/global.css" media="all" />
    <link rel='stylesheet' href="../data/scripts/css/default.css" media="all" />
    <link rel='stylesheet' href="../data/scripts/css/mainmenu.css" media="all" />
    <script type='text/javascript' src='../data/scripts/js/global.js'></script>
    
    <script type='text/javascript'>

        parent.controlUnload = false;

        function pickModule(moduleType) {
            if (moduleType.value != "na") {
                if (moduleType.value == "RA") {
                    window.location.href = "../hsafety/risk_assessment/app/rassessment.asp"
                }

                if (moduleType.value == "FRA") {
                    window.location.href = "../hsafety/risk_assessment/app/rassessment.asp"
                }
            }
        }

    </script>
</head>


        
     
    <body>
    <!-- <div id='section_0' class='optionsection' align='right'>
        <table width='100%'>
        <tr>
            <td class='l title'><% =session("YOUR_GROUP_NAME") & " " & trans("home.title.safetyportal.td") %><br /><span class='info'><%=trans("home.title.quickaccessdesc.td") %></span></td>
        </tr>
        </table>    
    </div>-->
    
    
    <div id='section_1' class='optionsection' align='right'>
    

                <div id='windowtitle'>
                    <h5>Please Select your Language&nbsp;</h5>
                    <div id='windowmenu'>&nbsp;</div>
                    <div id='windowbody' class='pad'>

                       
                        <div class="languages">
                          <!--  <h3><span>Select Language<%=trans("home.changelanguage.h3") %></span></h3>-->
                            <%
                                

                            objcommand.commandtext = "SELECT * FROM " & Application("DBTABLE_HR_LANGUAGES") & " WHERE corp_code = '" & session("CORP_CODE") & "' ORDER BY position"
                            set objrs = objcommand.execute
                            if objrs.eof then
                                'do nothing
                                response.Redirect("../../portal_login_sessions.asp?c=" & var_corp_code & "&r=" & var_corp_string & "&f=" & var_random_string & "&i=" & var_rowid & "&a=" & var_acc_ref & "&d=" & var_demo_code & "&tmxa=" & var_acc_ref_tmx & "&selfa=" & var_acc_ref_self)
                            else
                               
                                while not objrs.eof

                                    var_language = objrs("language")

                                    response.write "<a href='" & var_redirect & "&lng=" & var_language & "'><img src='../images/flags/circle/" & var_language & ".png' width='150px' height='150px' style='padding:20px;' /></a>"
                                              

                                objrs.movenext 
                                wend   
                                
                            end if 
                                
                            %>
                            </div>
                         
                            <div style='clear: both;'></div>

                        </div>

                     
                </div>
            

    </div>    

    <br />
    
</body>


</html>
