﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using System.IO;
using System.Web.UI.HtmlControls;

public partial class sessionReset : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        string redirectType = Request.QueryString["redirectTo"];

        SessionState.clearAllSessions();

        if (redirectType.Length > 0)
        {
            if (redirectType == "timeout") { Response.Redirect("../../../version3.2/security/login/frm_lg_session.asp?redirect=false"); }
            else if (redirectType == "logout") { Response.Redirect("../../../version3.2/data/functions/func_exit.asp?redirect=false"); }
        }

    }


}






