﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.SessionState;
using ASNETNSP;

public class Handler: IHttpHandler {

    public void ProcessRequest(HttpContext context) {

        context.Response.ContentType = "text/plain";

        string userSessionGUID = HttpContext.Current.Request.Cookies.Get("sync").Value.ToString();

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader reader = null;
                SqlCommand cmd = new SqlCommand("Module_LOG.dbo.checkSessionStatus", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@sessionGUID", userSessionGUID));
                reader = cmd.ExecuteReader();

                if (reader.HasRows == true)
                {
                    reader.Read();

                    if (reader["status"].ToString() == "True")
                    {
                        context.Response.Write("");
                    }
                    else
                    {
                        context.Response.Write(SessionState.getRedirectPage());
                    }
                }
                else
                {
                    context.Response.Write(SessionState.getRedirectPage());
                }
                cmd.Dispose();
                reader.Close();

            }
            catch
            {
                context.Response.Write(SessionState.getRedirectPage());
            }
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}
