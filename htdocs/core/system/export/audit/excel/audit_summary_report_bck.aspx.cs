﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative.ExcelLib;
using ASNETNSP;
using System.Linq;

public partial class auditSummaryReport : System.Web.UI.Page
{
    public string auditRef = "2349";
    public string corpCode = "222999";
    public string currentUser = "";
    List<Weighting> weightingList = new List<Weighting>();
    public int weightingList_Size;


    protected void Page_Load(object sender, EventArgs e)
    {
        initiliseWeighting();
        generateSummaryReport();
    }

    private void initiliseWeighting()
    {

        // This method uses a class Weighting to specify the name and value. This is done so it can be referred
        // to during section / question generation.
        // To Do: Error Catching.

        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateWeightingDropDown", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    objrs = cmd.ExecuteReader();

                    while (objrs.Read())
                    {

                        Weighting newWeighting = new Weighting
                        {
                            Name = Convert.ToString(objrs["weight_title"]),
                            Value = Convert.ToString(objrs["weighting"])
                        };

                        weightingList.Add(newWeighting);

                    }

                    Weighting newWeighting_NA = new Weighting
                    {
                        Name = "NA",
                        Value = "x"
                    };

                    weightingList.Add(newWeighting_NA);

                    weightingList_Size = weightingList.Count;

                }
                catch (Exception ex)
                {
                    
                }
            }
        }
        catch (Exception ex)
        {
          
        }
    }

    private void generateSummaryReport()
    {
        // get the Excel workbook format
        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xlsx_2007;

        // create the workbook in the desired format with a single worksheet
        ExcelWorkbook workbook = new ExcelWorkbook(workbookFormat);

        // set the license key before saving the workbook
        workbook.LicenseKey = "ZOr66/j66/r56/jl++v4+uX6+eXy8vLy";

        // set workbook description properties
        workbook.DocumentProperties.Subject = "Audit Summary Report";
        workbook.DocumentProperties.Comments = "Audit Summary Report";

        ExcelCellStyle titleStyle = workbook.Styles.AddStyle("WorksheetTitleStyle");
        // center the text in the title area
        titleStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        titleStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        // set the title area borders
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Top].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Left].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Right].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Medium;
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
        {
            // set the solid fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.SolidFill;
            titleStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(255, 255, 204);
        }
        else
        {
            // set the gradient fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.GradientFill;
            titleStyle.Fill.GradientFillOptions.Color1 = Color.FromArgb(255, 255, 204);
            titleStyle.Fill.GradientFillOptions.Color2 = Color.White;
        }
        // set the title area font 
        titleStyle.Font.Size = 14;
        titleStyle.Font.Bold = true;
        titleStyle.Font.UnderlineType = ExcelCellUnderlineType.Single;



      

        #region Add a style used for accessed cells and ranges

        ExcelCellStyle accessedRangeStyle = workbook.Styles.AddStyle("ΑccessedRangeStyle");
        accessedRangeStyle.Font.Size = 10;
        accessedRangeStyle.Font.Bold = true;
        accessedRangeStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        accessedRangeStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        accessedRangeStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        accessedRangeStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(204, 255, 204);
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        #endregion


        ExcelCellStyle sectionHeaderStyle = workbook.Styles.AddStyle("sectionHeaderStyle");
        sectionHeaderStyle.Font.Size = 14;
        sectionHeaderStyle.Font.Bold = true;
        sectionHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        sectionHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(248, 104, 93);
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;


        ExcelCellStyle sectionColumnHeaderStyle = workbook.Styles.AddStyle("sectionColumnHeaderStyle");
        sectionColumnHeaderStyle.Font.Size = 10;
        sectionColumnHeaderStyle.Font.Bold = true;
        sectionColumnHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionColumnHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionColumnHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionColumnHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(235, 235, 235);
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle sectionContent = workbook.Styles.AddStyle("sectionContent");
        sectionContent.Font.Size = 9;
        sectionContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle sectionWeightingContent = workbook.Styles.AddStyle("sectionWeightingContent");
        sectionWeightingContent.Font.Size = 10;
        sectionColumnHeaderStyle.Font.Bold = true;
        sectionWeightingContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionWeightingContent.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;


        

        // get the first worksheet in the workbook
        ExcelWorksheet worksheet = workbook.Worksheets[0];

        // set the default worksheet name
        worksheet.Name = "Audit Report";

        #region WORKSHEET PAGE SETUP

        // set worksheet paper size and orientation, margins, header and footer
        worksheet.PageSetup.PaperSize = ExcelPagePaperSize.PaperA4;
        worksheet.PageSetup.Orientation = ExcelPageOrientation.Landscape;
        worksheet.PageSetup.LeftMargin = 1;
        worksheet.PageSetup.RightMargin = 1;
        worksheet.PageSetup.TopMargin = 1;
        worksheet.PageSetup.BottomMargin = 1;


        worksheet.PageSetup.RightHeaderFormat = "&A";
        worksheet.PageSetup.CenterFooterFormat = "&P";
        worksheet.PageSetup.LeftFooterFormat = "&F";
        worksheet.PageSetup.RightFooterFormat = "&D";

        #endregion

        #region WRITE THE WORKSHEET TOP TITLE

        worksheet["A2"].Text = "Audit Report";

        #endregion


        #region Start of Content


        const int revertXAxis = 2;
        const int revertYAxis = 12;

        int currentXAxis = revertXAxis;
        int currentYAxis = revertYAxis;

        int maximumXAxis = 0;
        int maximumYAxis = 0;
        int sectionYAxis = 0;


        //worksheet[currentYAxis, currentXAxis].Text = "Test";

        //First generate the sections, then the Questions per.


        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateSectionContent", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    objrs = cmd.ExecuteReader();

                    while (objrs.Read())
                    {

                        
                        string sectionReference = Convert.ToString(objrs["section_reference"]);

                        //Write section information.
                        currentYAxis++;
                        currentYAxis++;
                        currentYAxis++;
                        worksheet[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs["section_title"]));
                        sectionYAxis = currentYAxis;

                        currentYAxis++;
                        worksheet[currentYAxis, currentXAxis].Text = "Audit Point";
                        worksheet[currentYAxis, currentXAxis].ColumnWidthInChars = 70;

                        //Weighting Scheme - Initilised at the top.
                        foreach (Weighting item in weightingList)
                        {
                            currentXAxis++;
                            worksheet[currentYAxis, currentXAxis].Text = item.Name;
                            worksheet.AutofitColumn(currentXAxis);

                        }


                        currentXAxis++;
                        worksheet[currentYAxis, currentXAxis].Text = "Comments";
                        worksheet[currentYAxis, currentXAxis].ColumnWidthInChars = 70;


                        //Clean up
                        maximumYAxis = currentYAxis;
                        maximumXAxis = currentXAxis;

                        currentXAxis = revertXAxis;

                        //Style
                        
                        worksheet[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Merge();
                        worksheet[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Style = sectionHeaderStyle;
                        worksheet[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionColumnHeaderStyle;


                        //Initilise Questions from sections;

                        using (SqlConnection dbConn2 = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {
                                SqlDataReader objrs2 = null;
                                SqlCommand cmd2 = new SqlCommand("Module_SA.dbo.usp_GenerateQuestionContent", dbConn2);
                                cmd2.CommandType = CommandType.StoredProcedure;
                                cmd2.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd2.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                                cmd2.Parameters.Add(new SqlParameter("@section_reference", sectionReference));
                                objrs2 = cmd2.ExecuteReader();


                                while (objrs2.Read())
                                {

                                    //To be used for the weighting scheme.
                                    string currentAnswer = Convert.ToString(objrs2["status"]);
                                    int currentAnswerIndex = weightingList.FindIndex(list => list.Value.Equals(currentAnswer, StringComparison.Ordinal));

                                    currentYAxis++;

                                    worksheet[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs2["question_name"]));
                                    worksheet[currentYAxis, currentXAxis].Style = sectionContent;

                                    //tricky bit - work out the weighting scheme.
                                    int loopCount = 0;

                                    foreach (Weighting item in weightingList)
                                    {
                                        

                                        currentXAxis++;

                                        if (loopCount == currentAnswerIndex)
                                        {
                                            worksheet[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField("&#x2713;");
                                        }

                                        worksheet[currentYAxis, currentXAxis].Style = sectionWeightingContent;
                                        loopCount++;

                                    }

                                    currentXAxis++;

                                    if (objrs2["comments"] == DBNull.Value || Convert.ToString(objrs2["comments"]) == "")
                                    {
                                        worksheet[currentYAxis, currentXAxis].Text = "-";
                                        worksheet[currentYAxis, currentXAxis].Style = sectionContent;
                                        worksheet[currentYAxis, currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                                    }
                                    else
                                    {
                                        worksheet[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs2["comments"]));
                                        worksheet[currentYAxis, currentXAxis].Style = sectionContent;
                                    }

                                    currentXAxis = revertXAxis;
                                    
                                }


                            }
                            catch (Exception ex)
                            {
                                worksheet[1, 1].Text = ex.Message;
                                throw;
                            }
                        }


                    }

                }
                catch (Exception ex)
                {
                    worksheet[1, 1].Text = ex.Message;
                    throw;
                }
            }
        }
        catch (Exception ex)
        {
            worksheet[1, 1].Text = ex.Message;
            throw;
        }

        currentYAxis++;
        currentYAxis++;
        currentXAxis = revertXAxis;

        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateObservations", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    cmd.Parameters.Add(new SqlParameter("@observation_reference", "0"));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {

                        worksheet[currentYAxis, currentXAxis].Text = "Observations";
                        sectionYAxis = currentYAxis;

                        while (objrs.Read())
                        {



                        }

                    }

                }
                catch (Exception ex)
                {
                    worksheet["A1"].Text = ex.Message;
                    throw;
                }
            }
        }
        catch (Exception ex)
        {
            worksheet[1, 1].Text = ex.Message;
            throw;
        }


        #endregion





        // SAVE THE WORKBOOK

        // Save the Excel document in the current HTTP response stream

        string outFileName = workbookFormat == ExcelWorkbookFormat.Xls_2003 ? auditRef + "SA - Audit_Summary_report.xls" : auditRef + "SA - Audit_Summary_report.xlsx";

        HttpResponse httpResponse = HttpContext.Current.Response;

        // Prepare the HTTP response stream for saving the Excel document

        // Clear any data that might have been previously buffered in the output stream  
        httpResponse.Clear();

        // Set output stream content type for Excel 97-2003 (.xls) or Excel 2007(.xlsx)
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
                httpResponse.ContentType = "Application/x-msexcel";
            else
                httpResponse.ContentType = "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            // Add the HTTP header to announce the Excel document either as an attachment or inline
        httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", outFileName));

            // Save the workbook to the current HTTP response output stream
            // and close the workbook after save to release all the allocated resources
        try
            {
                workbook.Save(httpResponse.OutputStream);
            }
            catch (Exception ex)
            {
                // report any error that might occur during save
                Session["ErrorMessage"] = ex.Message;

                Response.Write(ex.Message);
                Response.End();
                //Response.Redirect("ErrorPage.aspx");
            }
            finally
            {
                // close the workbook and release the allocated resources
                workbook.Close();

            }

            // End the response and finish the execution of this page
            httpResponse.End();

        }

}

class Weighting
{
    public string Name { get; set; }
    public string Value { get; set; }
}

