﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative.ExcelLib;
using ASNETNSP;


public partial class auditSummaryReport : System.Web.UI.Page
{
    public string auditRef = "";
    public string corpCode = "";

    public string currentUser = "";
    List<Weighting> weightingList = new List<Weighting>();
    public int weightingList_Size;





    protected void Page_Load(object sender, EventArgs e)
    {

        Server.ScriptTimeout = 300;

        auditRef =  Convert.ToString(Request.QueryString["auditref"]);
        corpCode = Convert.ToString(Request.QueryString["corpcode"]);
        currentUser = Convert.ToString(Request.QueryString["accRef"]);

        SharedComponants.insertHistory(corpCode, auditRef + "SA", currentUser, "DOWNLOADED SUMMARY REPORT ", "SA");



        initiliseWeighting();
        generateSummaryReport();
    }

    private void initiliseWeighting()
    {

        // This method uses a class Weighting to specify the name and value. This is done so it can be referred
        // to during section / question generation.
        // To Do: Error Catching.

        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateWeightingDropDown", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    objrs = cmd.ExecuteReader();

                    while (objrs.Read())
                    {

                        Weighting newWeighting = new Weighting
                        {
                            Name = Formatting.FormatTextbyLength(Convert.ToString(objrs["weight_title"]),8),
                            Value = Convert.ToString(objrs["weighting"]),
                            Colour = Convert.ToString(objrs["weight_color"])
                        };

                        weightingList.Add(newWeighting);

                    }

                    Weighting newWeighting_NA = new Weighting
                    {
                        Name = "NA",
                        Value = "x"
                    };

                    weightingList.Add(newWeighting_NA);

                    weightingList_Size = weightingList.Count;

                }
                catch (Exception ex)
                {
                    
                }
            }
        }
        catch (Exception ex)
        {
          
        }
    }

    private void generateSummaryReport()
    {



        // get the Excel workbook format
        ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xlsx_2007;

        // create the workbook in the desired format with a single worksheet_Report
        ExcelWorkbook workbook = new ExcelWorkbook(workbookFormat);

        // set the license key before saving the workbook
        workbook.LicenseKey = "ZOr66/j66/r56/jl++v4+uX6+eXy8vLy";

        // set workbook description properties
        workbook.DocumentProperties.Subject = "Audit Summary Report";
        workbook.DocumentProperties.Comments = "Audit Summary Report";




        ExcelCellStyle titleStyle = workbook.Styles.AddStyle("WorksheetTitleStyle");
        // center the text in the title area
        titleStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        titleStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        // set the title area borders
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Top].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Left].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Medium;
        titleStyle.Borders[ExcelCellBorderIndex.Right].Color = Color.Green;
        titleStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Medium;
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
        {
            // set the solid fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.SolidFill;
            titleStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(255, 255, 204);
        }
        else
        {
            // set the gradient fill for the title area range with a custom color
            titleStyle.Fill.FillType = ExcelCellFillType.GradientFill;
            titleStyle.Fill.GradientFillOptions.Color1 = Color.FromArgb(255, 255, 204);
            titleStyle.Fill.GradientFillOptions.Color2 = Color.White;
        }
        // set the title area font 
        titleStyle.Font.Size = 14;
        titleStyle.Font.Bold = true;
        titleStyle.Font.UnderlineType = ExcelCellUnderlineType.Single;




        
        ExcelCellStyle accessedRangeStyle = workbook.Styles.AddStyle("ΑccessedRangeStyle");
        accessedRangeStyle.Font.Size = 12;
        accessedRangeStyle.Font.Bold = true;
        accessedRangeStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        accessedRangeStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        accessedRangeStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        accessedRangeStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(204, 255, 204);
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        accessedRangeStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;
        

        ExcelCellStyle sectionHeaderStyle = workbook.Styles.AddStyle("sectionHeaderStyle");
        sectionHeaderStyle.Font.Size = 13;
        sectionHeaderStyle.Font.Bold = true;
        sectionHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        sectionHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(248, 104, 93);
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle headerStyle = workbook.Styles.AddStyle("headerStyle");
        headerStyle.Font.Size = 12;
        headerStyle.Font.Bold = false;
        headerStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        headerStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
        headerStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        headerStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle headerStyleNoBorder = workbook.Styles.AddStyle("headerStyleNoBorder");
        headerStyleNoBorder.Font.Size = 12;
        headerStyleNoBorder.Font.Bold = false;
        headerStyleNoBorder.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        headerStyleNoBorder.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;



        ExcelCellStyle sectionColumnHeaderStyle = workbook.Styles.AddStyle("sectionColumnHeaderStyle");
        sectionColumnHeaderStyle.Font.Size = 12;
        sectionColumnHeaderStyle.Font.Bold = true;
        sectionColumnHeaderStyle.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionColumnHeaderStyle.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionColumnHeaderStyle.Fill.FillType = ExcelCellFillType.SolidFill;
        sectionColumnHeaderStyle.Fill.SolidFillOptions.BackColor = Color.FromArgb(235, 235, 235);
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionColumnHeaderStyle.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;

        ExcelCellStyle sectionContent = workbook.Styles.AddStyle("sectionContent");
        sectionContent.Font.Size = 12;
        sectionContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;
        sectionContent.Alignment.WrapText = true;

        ExcelCellStyle sectionWeightingContent = workbook.Styles.AddStyle("sectionWeightingContent");
        sectionWeightingContent.Font.Size = 12;
        sectionColumnHeaderStyle.Font.Bold = true;
        sectionWeightingContent.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Center;
        sectionWeightingContent.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Left].LineStyle = ExcelCellLineStyle.Thin;
        sectionWeightingContent.Borders[ExcelCellBorderIndex.Right].LineStyle = ExcelCellLineStyle.Thin;



        // get the first worksheet_Report in the workbook
        ExcelWorksheet worksheet_Report = workbook.Worksheets[0];


        // set the default worksheet_Report name
        worksheet_Report.Name = "Audit Report";

        #region worksheet_Report PAGE SETUP


        // set worksheet_Report paper size and orientation, margins, header and footer


       

        

        worksheet_Report.PageSetup.PaperSize = ExcelPagePaperSize.PaperA4;
        worksheet_Report.PageSetup.Orientation = ExcelPageOrientation.Landscape;
        worksheet_Report.PageSetup.LeftMargin = 0.2;
        worksheet_Report.PageSetup.RightMargin = 0.2;
        worksheet_Report.PageSetup.TopMargin = 1;
        worksheet_Report.PageSetup.BottomMargin = 0.2;
        //worksheet_Report.PageSetup.RightHeaderFormat = "&G";



        //if (File.Exists(Server.MapPath("../../../../../version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg")))
        //{
        //    string corpLogo = Server.MapPath("../../../../../version3.2/data/documents/pdf_centre/img/corp_logos/" + corpCode + ".jpg");
        //    System.Drawing.Image logoImg = System.Drawing.Image.FromFile(corpLogo);
        //    worksheet_Report.PageSetup.RightHeaderPicture = logoImg;
        //}
        //else
        //{
        //    string corpLogo = Server.MapPath("../../../../../version3.2/data/documents/pdf_centre/img/corp_logos/222999.jpg");
        //    System.Drawing.Image logoImg = System.Drawing.Image.FromFile(corpLogo);
        //    worksheet_Report.PageSetup.RightHeaderPicture = logoImg;
        //}



        #endregion

        const int revertXAxis = 2;
        const int revertYAxis = 10;

        int currentXAxis = revertXAxis;
        int currentYAxis = revertYAxis;

        int maximumXAxis = 0;
        int maximumYAxis = 0;
        int sectionYAxis = 0;

        //remember score corodinates for styling later
        int scoreXAxis = 0;
        int scoreYAxis = 0;


        int execSummaryYPosition = 0;
        string auditTitle = "";
        string percentageType = "";

        const int defaultRowSize = 28;

        #region WRITE THE worksheet_Report TOP TITLE


        
       
        worksheet_Report["B2"].Style.Font.Size = 20;
        worksheet_Report["B2"].Style.Font.Bold = true;

        worksheet_Report["B4"].Text = "";
        worksheet_Report["B5"].Text = "Location: ";
        worksheet_Report["B6"].Text = "Date: ";
        worksheet_Report["B7"].Text = "Auditor: ";
        #endregion


        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    string sql_text = "SELECT a.audit_title, a.audit_desc, a.audit_datetime, a.int_ref, " +
                                      "b.temp_title, tier2.struct_name as tier2_name,tier3.struct_name as tier3_name, " +
                                      "tier4.struct_name as tier4_name,tier5.struct_name as tier5_name,tier6.struct_name as tier6_name,  " +
                                      "a.rec_bu, a.rec_bl, a.rec_bd, a.tier5_ref, a.tier6_ref, a.locked, a.authorised_date, a.authorised_name,  " +
                                      "a.authorised_jtitle, a.dist_list_sent, owner, b.weighting_scheme, b.weighting_type, b.section_weighting,  " +
                                      "auditor.per_fname, auditor.per_sname, b.scoring_scheme_override, tier3.struct_saudit_scoring_st,  " +
                                      "tier3.struct_saudit_scoring_nc, a.report_recipient_code, d.action_later, b.template_type, a.exec_summary, a.template_id  " +
                                      "FROM " + Application["DBTABLE_SA_AUDIT_ENTRIES"] + " AS a " +
                                      "LEFT JOIN " + Application["DBTABLE_SA_TEMP_INFO"] + " as b " +
                                      "  on a.template_id = b.template_id " +
                                      "LEFT JOIN (SELECT TOP 1 action_later, corp_code, audit_ref FROM " + Application["DBTABLE_SA_AUDIT_DATA"] + " WHERE audit_ref = '" + auditRef + "' AND corp_code = '" + corpCode + "' and action_later = 1) AS d " +
                                      "  on a.corp_code = d.corp_code " +
                                      "  and a.audit_ref = d.audit_ref " +
                                      "LEFT OUTER JOIN " + Application["DBTABLE_STRUCT_TIER2"] + "  AS tier2 " +
                                      "  on a.corp_code = tier2.corp_code " +
                                      "  and a.rec_bu = tier2.tier2_ref " +
                                      "LEFT OUTER JOIN " + Application["DBTABLE_STRUCT_TIER3"] + "  AS tier3 " +
                                      "  on a.corp_code = tier3.corp_code " +
                                      "  and a.rec_bl = tier3.tier3_ref " +
                                      "LEFT OUTER JOIN " + Application["DBTABLE_STRUCT_TIER4"] + "  AS tier4 " +
                                      "  on a.corp_code = tier4.corp_code " +
                                      "  and a.rec_bd = tier4.tier4_ref " +
                                      "LEFT OUTER JOIN " + Application["DBTABLE_STRUCT_TIER5"] + "  AS tier5 " +
                                      "  on a.corp_code = tier5.corp_code " +
                                      "  and a.tier5_ref = tier5.tier5_ref " +
                                      "LEFT OUTER JOIN " + Application["DBTABLE_STRUCT_TIER6"] + "  AS tier6 " +
                                      "  on a.corp_code = tier6.corp_code " +
                                      "  and a.tier6_ref = tier6.tier6_ref " +
                                      "LEFT JOIN " + Application["DBTABLE_USER_DATA"] + " AS auditor " +
                                      "  ON a.corp_code = auditor.corp_code " +
                                      "  AND a.assessor = auditor.acc_ref " +
                                      "where a.audit_ref='" + auditRef + "' and a.corp_code='" + corpCode + "'";

                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    SqlDataReader objrs = sqlComm.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        string structureText;

                        if (Convert.ToString(objrs["tier2_name"]) != "" || objrs["tier2_name"] != DBNull.Value)
                        {
                            structureText = Convert.ToString(objrs["tier2_name"]);

                            if (Convert.ToString(objrs["tier3_name"]) != "" || objrs["tier3_name"] != DBNull.Value)
                            {
                                structureText += " > " + Convert.ToString(objrs["tier3_name"]);

                                if (Convert.ToString(objrs["tier4_name"]) != "" || objrs["tier4_name"] != DBNull.Value)
                                {
                                    structureText += " > " + Convert.ToString(objrs["tier4_name"]);

                                    if (Convert.ToString(objrs["tier5_name"]) != "" ||
                                        objrs["tier5_name"] != DBNull.Value)
                                    {
                                        structureText += " > " + Convert.ToString(objrs["tier5_name"]);

                                        if (Convert.ToString(objrs["tier6_name"]) != "" ||
                                            objrs["tier6_name"] != DBNull.Value)
                                        {
                                            structureText += " > " + Convert.ToString(objrs["tier6_name"]);

                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            structureText = "Error contact support";
                        }

                        auditTitle = Convert.ToString(objrs["audit_title"]);


                        if (corpCode == "061306"){
                            worksheet_Report["B2"].Text =
                           Formatting.FormatTextInputField(Convert.ToString(objrs["audit_title"]));
                        }
                        else
                        {
                            worksheet_Report["B2"].Text =
                           Formatting.FormatTextInputField(Convert.ToString(objrs["audit_title"])) + " - " + auditRef +
                           "SA";
                        }
                       


                       

                        worksheet_Report["B5"].Text += Formatting.FormatTextInputField(structureText);
                        worksheet_Report["B6"].Text += DateTime.Parse(objrs["audit_datetime"].ToString()).ToShortDateString();
                        worksheet_Report["B7"].Text += Convert.ToString(objrs["per_fname"]) + " " + Convert.ToString(objrs["per_sname"]);

                        worksheet_Report["B4"].Style = headerStyleNoBorder;
                        worksheet_Report["B5"].Style = headerStyleNoBorder;
                        worksheet_Report["B6"].Style = headerStyleNoBorder;
                        worksheet_Report["B7"].Style = headerStyleNoBorder;

                        worksheet_Report.Name = Convert.ToString(objrs["temp_title"]);

                        


                    }
                    else
                    {
                        //Error - TODO
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }

        


        #region Start of Content

        int yAxisBeforeWeighting = currentYAxis;


        

        string auditScore = "0";
        string auditColour = "#FFF";

        currentXAxis = revertXAxis;
        currentYAxis = yAxisBeforeWeighting;

        //Score
        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateGeneralScore", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        rdr.Read();
                        //Percentage:
                        //percentageTitle
                        //percentageScore

                        currentYAxis += 0;

                        scoreXAxis = currentXAxis;
                        scoreYAxis = currentYAxis;

                        worksheet_Report[currentYAxis, currentXAxis].Text = "Audit Score";

                        worksheet_Report[currentYAxis, currentXAxis].Style = headerStyle;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Font.Bold = true;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Font.Size = 14;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Fill.SolidFillOptions.BackColor = Color.FromArgb(204, 204, 204);
                        worksheet_Report[currentYAxis, currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;

                        

                        currentYAxis += 1;

                        worksheet_Report[currentYAxis, currentXAxis].Text = rdr["percentage_score"].ToString();

                        if (rdr["template_type"].ToString() == "ST")
                        {
                            worksheet_Report[currentYAxis, currentXAxis].Text += "%";
                            percentageType = "%";
                        }

                        auditScore = rdr["percentage_score"].ToString();

                        worksheet_Report[currentYAxis, currentXAxis].Style = headerStyle;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Font.Bold = true;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Font.Size = 12;
                        worksheet_Report[currentYAxis, currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;



                       

                    }


                }
                catch (Exception)
                {
                    throw;
                }
            }
            
        }
        catch (Exception)
        {
            throw;
        }

       
        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateScoringScheme", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    cmd.Parameters.Add(new SqlParameter("@audit_score", auditScore));
                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        //change text
                        //change colour
                        auditColour = rdr["score_color"].ToString();


                    }

                    currentYAxis++;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }

        if (auditColour == "" || auditColour == "#FFF")
        {
            auditColour = "#000000";
        }
        

        Color col = ColorTranslator.FromHtml(auditColour);
        //worksheet[currentYAxis, currentXAxis].Text = auditColour;
        //worksheet[currentYAxis, currentXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
        worksheet_Report[currentYAxis, currentXAxis].Style.Font.Color = col;


        //Finalise exec summary
        if (worksheet_Report["B2"].Text.Length > 0)
        {

            currentXAxis = revertXAxis;
            currentYAxis++;
            currentYAxis++;
            execSummaryYPosition = currentYAxis;

          

        }




        //First generate the sections, then the Questions per.
        //currentYAxis = yAxisBeforeWeighting;
        currentXAxis = revertXAxis;

        try
        {
            currentYAxis++;

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateSectionContent", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    objrs = cmd.ExecuteReader();

                    while (objrs.Read())
                    {

                        
                        string sectionReference = Convert.ToString(objrs["section_reference"]);
                        int amountOfWeighting = 0;


                        //Write section information.
                        currentYAxis++;
                        currentYAxis++;
                        currentYAxis++;
                        worksheet_Report[currentYAxis, currentXAxis].Text = Convert.ToString(objrs["position"]) + " -  " + Formatting.FormatTextInputField(Convert.ToString(objrs["section_title"]));
                        sectionYAxis = currentYAxis;

                        currentYAxis++;
                        worksheet_Report[currentYAxis, currentXAxis].Text = "Audit Point";
                        worksheet_Report[currentYAxis, currentXAxis].ColumnWidthInChars = 85;

                        //Weighting Scheme - Initilised at the top.
                        foreach (Weighting item in weightingList)
                        {
                            currentXAxis++;
                            amountOfWeighting++;
                            worksheet_Report[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField(item.Name);
                            //worksheet.AutofitColumn(currentXAxis);
                            worksheet_Report[currentYAxis, currentXAxis].ColumnWidthInChars = 5;

                        }


                        currentXAxis++;
                        worksheet_Report[currentYAxis, currentXAxis].Text = "Comments";
                        worksheet_Report[currentYAxis, currentXAxis].ColumnWidthInChars = 85;


                        //Clean up
                        maximumYAxis = currentYAxis;
                        maximumXAxis = currentXAxis;

                        currentXAxis = revertXAxis;

                        //Style
                        
                        worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Merge();
                        worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Style = sectionHeaderStyle;
                        worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionColumnHeaderStyle;

                        worksheet_Report[currentYAxis, currentXAxis + 1, currentYAxis, maximumXAxis - 1].Style.Alignment.Orientation = 45;
                        //Initilise Questions from sections;

                        using (SqlConnection dbConn2 = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {
                                SqlDataReader objrs2 = null;
                                SqlCommand cmd2 = new SqlCommand("Module_SA.dbo.usp_GenerateQuestionContent", dbConn2);
                                cmd2.CommandType = CommandType.StoredProcedure;
                                cmd2.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                                cmd2.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                                cmd2.Parameters.Add(new SqlParameter("@section_reference", sectionReference));
                                objrs2 = cmd2.ExecuteReader();


                                while (objrs2.Read())
                                {

                                    //To be used for the weighting scheme.
                                    string currentAnswer = Convert.ToString(objrs2["status"]);
                                    int currentAnswerIndex = weightingList.FindIndex(list => list.Value.Equals(currentAnswer, StringComparison.Ordinal));

                                    currentYAxis++;

                                    //worksheet_Report[currentYAxis, 1].RowHeightInPoints = defaultRowSize;

                                    worksheet_Report[currentYAxis, currentXAxis].Text = Convert.ToString(objrs["position"]) + "." + Convert.ToString(objrs2["position"]) + " - " + Formatting.FormatTextInputField(Convert.ToString(objrs2["question_name"]));
                                    worksheet_Report[currentYAxis, currentXAxis].Style = sectionContent;

                                    //tricky bit - work out the weighting scheme.
                                    int loopCount = 0;

                                    foreach (Weighting item in weightingList)
                                    {
                                        

                                        currentXAxis++;

                                        worksheet_Report[currentYAxis, currentXAxis].Style = sectionWeightingContent;

                                        if (loopCount == currentAnswerIndex)
                                        {

                                            string currentColour = item.Colour;

                                            if (currentColour == "" || currentColour == "#FFF")
                                            {
                                                currentColour = "#000000";
                                            }

                                            Color colour = ColorTranslator.FromHtml(currentColour);

                                            //worksheet[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField("&#x2713;");
                                            worksheet_Report[currentYAxis, currentXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
                                            worksheet_Report[currentYAxis, currentXAxis].Style.Fill.SolidFillOptions.BackColor = colour;


                                        }

                                       
                                        loopCount++;

                                    }

                                    currentXAxis++;

                                    if (objrs2["comments"] == DBNull.Value || Convert.ToString(objrs2["comments"]) == "")
                                    {
                                        worksheet_Report[currentYAxis, currentXAxis].Text = "-";
                                        worksheet_Report[currentYAxis, currentXAxis].Style = sectionContent;
                                        worksheet_Report[currentYAxis, currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                                    }
                                    else
                                    {
                                        worksheet_Report[currentYAxis, currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs2["comments"]));
                                        worksheet_Report[currentYAxis, currentXAxis].Style = sectionContent;
                                    }

                                    worksheet_Report.AutofitRow(currentYAxis);

                                    currentXAxis = revertXAxis;
                                    
                                }


                            }
                            catch (Exception ex)
                            {
                                worksheet_Report[1, 1].Text = ex.Message;
                                throw;
                            }
                        }


                    }

                }
                catch (Exception ex)
                {
                    worksheet_Report[1, 1].Text = ex.Message;
                    throw;
                }
            }
        }
        catch (Exception ex)
        {
            worksheet_Report[1, 1].Text = ex.Message;
            throw;
        }

       


        currentXAxis = revertXAxis;


        currentYAxis++;
        worksheet_Report.HorizontalPageBreaks.AddPageBreak(worksheet_Report[currentYAxis, currentXAxis, currentYAxis, currentXAxis]);  


       // try
       // {
       //
       //     
       //
       //     
       //
       //
       //     using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
       //     {
       //         try
       //         {
       //             SqlDataReader objrs = null;
       //             SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateObservations", dbConn);
       //             cmd.CommandType = CommandType.StoredProcedure;
       //             cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
       //             cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
       //             cmd.Parameters.Add(new SqlParameter("@observation_reference", "0"));
       //             objrs = cmd.ExecuteReader();
       //
       //             if (objrs.HasRows)
       //             {
       //                 currentYAxis++;
       //                 currentYAxis++;
       //
       //                 worksheet_Report[currentYAxis, currentXAxis].Text = "Observations";
       //                 sectionYAxis = currentYAxis;
       //
       //                 worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Merge();
       //                 worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Style = sectionHeaderStyle;
       //
       //                 while (objrs.Read())
       //                 {
       //                     currentYAxis++;
       //
       //
       //                     worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
       //                     worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionContent;
       //                     worksheet_Report[currentYAxis, currentXAxis].Style.Alignment.WrapText = true;
       //                     worksheet_Report.SetRowHeightInPoints(currentYAxis, 75);
       //                     worksheet_Report[currentYAxis, currentXAxis].Text = "Observation: " + Formatting.FormatTextInputField(Convert.ToString(objrs["observation"])) + 
       //                                                                "\n\nRequirements: " + Formatting.FormatTextInputField(Convert.ToString(objrs["requirements"]));
       //
       //
       //                     string statusText;
       //
       //                     switch (Convert.ToString(objrs["task_status"]))
       //                     {
       //                         case "2":
       //                             statusText = "Pending";
       //                             break;
       //
       //                         case "1":
       //                             statusText = "Active";
       //                             break;
       //
       //                         case "0":
       //                             statusText = "Completed";
       //                             break;
       //
       //                         default:
       //                             statusText = "Unknown";
       //                             break;
       //                     }
       //
       //
       //                     if (Convert.ToString(objrs["actionedto"]) == "nreq")
       //                     {
       //                         currentYAxis++;
       //                         worksheet_Report[currentYAxis, currentXAxis].RowHeightInPoints = 25;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style =
       //                             sectionContent;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style
       //                             .Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.None;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Font.Bold =
       //                             true;
       //                         worksheet_Report[currentYAxis, currentXAxis].Text = "Nobody Required at this time.";
       //                     }
       //                     else
       //                     {
       //                         currentYAxis++;
       //                         worksheet_Report[currentYAxis, currentXAxis].RowHeightInPoints = 25;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionContent;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.None;
       //                         worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Font.Bold = true;
       //                         worksheet_Report[currentYAxis, currentXAxis].Text = "Date: " + Convert.ToDateTime(objrs["task_due_date"]).ToString("dd-MM-yyyy")
       //                                                                      + " - Actionee: " + Convert.ToString(objrs["name"])
       //                                                                      + " - Status: " + statusText;
       //                     }
       //
       //                     
       //
       //
       //                 }
       //
       //             }
       //
       //         }
       //         catch (Exception ex)
       //         {
       //             worksheet_Report["A1"].Text = ex.Message;
       //             throw;
       //         }
       //     }
       // }
       // catch (Exception ex)
       // {
       //     worksheet_Report[1, 1].Text = ex.Message;
       //     throw;
       // }
       //

        currentXAxis = revertXAxis;

        //Remedial Actions

        //try
        //{

        //    currentYAxis++;
        //    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        //    {
        //        try
        //        {
        //            SqlDataReader objrs = null;
        //            SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateFullActions", dbConn);
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
        //            cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
        //            objrs = cmd.ExecuteReader();

        //            if (objrs.HasRows)
        //            {

        //                currentYAxis++;
        //                currentYAxis++;
        //                currentYAxis++;

        //                worksheet_Report[currentYAxis, currentXAxis].Text = "Remedial Actions";
        //                sectionYAxis = currentYAxis;

        //                worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Merge();
        //                worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Style = sectionHeaderStyle;

        //                while (objrs.Read())
        //                {
        //                    currentYAxis++;

        //                    string statusText;

        //                    switch (Convert.ToString(objrs["task_status"]))
        //                    {
        //                        case "2":
        //                            statusText = "Pending";
        //                            break;

        //                        case "1":
        //                            statusText = "Active";
        //                            break;

        //                        case "0":
        //                            statusText = "Completed";
        //                            break;

        //                        default:
        //                            statusText = "Unknown";
        //                            break;
        //                    }


        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionContent;
        //                    worksheet_Report[currentYAxis, currentXAxis].Style.Alignment.WrapText = true;
        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Borders[ExcelCellBorderIndex.Bottom].LineStyle = ExcelCellLineStyle.None;
        //                    worksheet_Report.SetRowHeightInPoints(currentYAxis, 60);

        //                    worksheet_Report[currentYAxis, currentXAxis].Text =
        //                        Convert.ToString(objrs["sectionPosition"]) + "." +
        //                        Convert.ToString(objrs["questionPosition"]) + ": " +
        //                        Convert.ToString(objrs["sectionTitle"]) + " - " + Convert.ToString(objrs["questionTitle"]) +
        //                        Environment.NewLine +
        //                        Formatting.FormatTextInputField(Convert.ToString(objrs["action_text"]));


        //                    currentYAxis++;

        //                    worksheet_Report[currentYAxis, currentXAxis].RowHeightInPoints = 25;


        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionContent;
        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Borders[ExcelCellBorderIndex.Top].LineStyle = ExcelCellLineStyle.None;
        //                    worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Font.Bold = true;
        //                    worksheet_Report[currentYAxis, currentXAxis].Text = "Date: " + Convert.ToDateTime(objrs["task_due_date"]).ToString("dd-MM-yyyy")
        //                                                                 + " - Actionee: " + Convert.ToString(objrs["name"])
        //                                                                 + " - Status: " + statusText;



        //                }

        //            }




        //        }
        //        catch (Exception)
        //        {
        //            throw;
        //        }
        //    }
        //}
        //catch (Exception)
        //{
        //    throw;
        //}

        


        // Root Cause Analysis

        currentXAxis = revertXAxis;

        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateRootCause", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@module", "SA"));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    cmd.Parameters.Add(new SqlParameter("@module_sub_reference", ""));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        currentYAxis++;
                        currentYAxis++;
                        currentYAxis++;

                        worksheet_Report[currentYAxis, currentXAxis].Text = "Root Cause Analysis";
                        sectionYAxis = currentYAxis;

                        worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Merge();
                        worksheet_Report[sectionYAxis, currentXAxis, sectionYAxis, maximumXAxis].Style = sectionHeaderStyle;

                        while (objrs.Read())
                        {
                            currentYAxis++;

                            string fullText = "";

                            fullText = Convert.ToString(objrs["sectionQuestionPosition"]) + " \nRoot Cause: " + Convert.ToString(objrs["root_cause_text"]);

                            if (objrs["secondary_cause_text"] != DBNull.Value)
                            {
                                fullText += " > " + Convert.ToString(objrs["secondary_cause_text"]);
                            }

                            fullText += "\n" + Formatting.FormatTextInputField(Convert.ToString(objrs["comments"]));

                            worksheet_Report[currentYAxis, currentXAxis].Text = fullText;

                            worksheet_Report.SetRowHeightInPoints(currentYAxis, 40);

                            worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Merge();
                            worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style = sectionContent;
                            worksheet_Report[currentYAxis, currentXAxis, currentYAxis, maximumXAxis].Style.Alignment.WrapText = true;
                        }

                    }

                }
                catch (Exception ex)
                {
                    worksheet_Report["A1"].Text = ex.Message;
                    throw;
                }
            }
        }
        catch (Exception ex)
        {
            worksheet_Report[1, 1].Text = ex.Message;
            throw;
        }


        // Weighting Breakdown
        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateWeightingBreakdown", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    objrs = cmd.ExecuteReader();
                    if (objrs.HasRows)
                    {

                        worksheet_Report[scoreYAxis, maximumXAxis].Text = "Answer Breakdown";
                        worksheet_Report[scoreYAxis, maximumXAxis].Style = headerStyle;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Font.Bold = true;
                        

                        scoreYAxis += 1;

                        int i = 1;

                        while (objrs.Read())
                        {

                            if (i == 1)
                            {
                                worksheet_Report[scoreYAxis, maximumXAxis].Text += Formatting.FormatTextInputField(Convert.ToString(objrs["weight_title"])) + ": " +
                                                                            Convert.ToString(objrs["questionCount"]) + "\n";
                            }
                            else
                            {
                                worksheet_Report[scoreYAxis, maximumXAxis].Text += Formatting.FormatTextInputField(Convert.ToString(objrs["weight_title"])) + ": " +
                                                                            Convert.ToString(objrs["questionCount"]) + "\n";
                            }

                           

                            i++;
                        }

                        scoreYAxis -= 1;

                        worksheet_Report[scoreYAxis, maximumXAxis].Style = headerStyle;

                        worksheet_Report[scoreYAxis, maximumXAxis].Style = headerStyle;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Font.Bold = true;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Font.Size = 14;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
                        worksheet_Report[scoreYAxis, maximumXAxis].Style.Fill.SolidFillOptions.BackColor = Color.FromArgb(204, 204, 204);

                        worksheet_Report[scoreYAxis + 1, maximumXAxis].Style = headerStyle;
                        worksheet_Report[scoreYAxis + 1, maximumXAxis].Style.Font.Bold = true;
                        worksheet_Report[scoreYAxis + 1, maximumXAxis].Style.Font.Size = 12;
                        worksheet_Report[scoreYAxis + 1, maximumXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                        worksheet_Report[scoreYAxis + 1, maximumXAxis].Style.Alignment.WrapText = true;
                    }




                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }


        //exec summary

        //Score
        try
        {
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader rdr = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateExecutiveSummary", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@audit_reference", auditRef));
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        rdr.Read();

                        worksheet_Report[execSummaryYPosition, 2].Style = headerStyleNoBorder;
                        worksheet_Report[execSummaryYPosition, 2].Style.Font.Bold = true;
                        worksheet_Report[execSummaryYPosition + 1, 2].Style = headerStyleNoBorder;
                        worksheet_Report[execSummaryYPosition + 1, 2].Style.Alignment.WrapText = true;

                        worksheet_Report[execSummaryYPosition, 2].Text = "Executive Summary";
                        worksheet_Report[execSummaryYPosition + 1, 2].Text = rdr["exec_summary"].ToString();

                        worksheet_Report[execSummaryYPosition, 2, execSummaryYPosition, maximumXAxis].Merge();
                        worksheet_Report[execSummaryYPosition + 1, 2, execSummaryYPosition + 1, maximumXAxis].Merge();

                        worksheet_Report[execSummaryYPosition, 2, execSummaryYPosition, maximumXAxis].Style = sectionHeaderStyle;
                        worksheet_Report[execSummaryYPosition + 1, 2, execSummaryYPosition + 1, maximumXAxis].Style = sectionContent;
                        worksheet_Report[execSummaryYPosition + 1, 2, execSummaryYPosition + 1, maximumXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Left;
                        worksheet_Report[execSummaryYPosition + 1, 2, execSummaryYPosition + 1, maximumXAxis].Style.Alignment.VerticalAlignment = ExcelCellVerticalAlignmentType.Top;

                        worksheet_Report.SetRowHeightInPoints(execSummaryYPosition + 1, 100);
                    }
                    


                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
        catch (Exception)
        {
            throw;
        }





        #endregion
        worksheet_Report[1, 1].ColumnWidthInChars = 2.5;
        //worksheet_Report.PageSetup.LeftHeaderFormat = "&13" + auditTitle + " - Audit Score:" + auditScore + percentageType;
       


        //Actions tab
        #region Actions


        int actions_resetXAxis = 2;
        int actions_resetYAxis = 10;

        int actions_currentXAxis = actions_resetXAxis;
        int actions_currentYAxis = actions_resetYAxis;
        int actions_maxXAxis = actions_currentXAxis;
        int actions_maxYAxis = actions_currentYAxis;



        //Generate the actions
        try
        {

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateFullActions", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        
                        // If we have actions, then make the tab.
                        ExcelWorksheet worksheet_Actions = workbook.Worksheets.AddWorksheet("Remedial Actions");
                        CopyPageSetup(worksheet_Report, worksheet_Actions);


                        worksheet_Actions[1, 1].ColumnWidthInChars = 2.5;
                        worksheet_Actions[2, 2].Text = worksheet_Actions[2, 2].Text + " - Remedial Actions";

                        // Set up the column names with associated widths
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Status";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 15;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "S";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 5;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Q";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 5;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Question";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 50;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Remedial Action";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 85;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Assigned To";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 35;
                        actions_currentXAxis++;

                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = "Due Date";
                        worksheet_Actions[actions_currentYAxis, actions_currentXAxis].ColumnWidthInChars = 20;


                        actions_maxXAxis = actions_currentXAxis;


                        //Format selection here.

                        worksheet_Actions[actions_currentYAxis, actions_resetXAxis, actions_currentYAxis, actions_maxXAxis].Style = sectionColumnHeaderStyle;


                        while (objrs.Read())
                        {
                            actions_currentYAxis++;
                            actions_currentXAxis = actions_resetXAxis;

                            string statusText;
                            string rowColour;
                            Color backColour = new Color();

                            if (objrs["task_status"] != DBNull.Value)
                            {

                                switch (Convert.ToString(objrs["task_status"]))
                                {
                                    case "2":
                                        statusText = "PENDING";
                                        rowColour = "#ff6161";
                                        backColour = Color.DarkRed;
                                        break;

                                    case "1":
                                        statusText = "ACTIVE";
                                        rowColour = "#ffad61";
                                        backColour = Color.OrangeRed;
                                        break;

                                    case "0":
                                        statusText = "COMPLETED";
                                        rowColour = "#4CC98A";
                                        backColour = Color.DarkGreen;
                                        break;

                                    default:
                                        statusText = "UNKNOWN";
                                        rowColour = "#ff6161";
                                        backColour = Color.Black;
                                        break;
                                }
                            }
                            else
                            {
                                statusText = "PROCESSING";
                                rowColour = "#ff6161";
                                backColour = Color.Black;
                            }


                            //Status COLOUR ROW FROM HERE
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = statusText;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Font.Bold = true;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Font.Color = backColour;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            actions_currentXAxis++;

                            //S Pos
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = objrs["sectionPositionNumeric"].ToString();
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            actions_currentXAxis++;

                            //Q Pos
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = objrs["questionPositionNumeric"].ToString();
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            actions_currentXAxis++;

                            //Question Name
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = Formatting.FormatTextInputField(objrs["questionTitle"].ToString());
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;

                            actions_currentXAxis++;

                            //Remedial Action
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs["action_text"]));
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.WrapText = true;
                            actions_currentXAxis++;

                            //Assigned to
                            if (objrs["name"] != DBNull.Value)
                            {
                                worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = objrs["name"].ToString();
                            }
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            actions_currentXAxis++;


                            //Due Date
                            if (objrs["task_due_date"] != DBNull.Value)
                            {
                                worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Text = Convert.ToDateTime(objrs["task_due_date"]).ToString("dd-MM-yyyy");
                            }
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style = sectionContent;
                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;


                            worksheet_Actions[actions_currentYAxis, actions_currentXAxis].RowHeightInPoints = defaultRowSize;

                            
                            //worksheet_Actions[actions_currentYAxis, actions_resetXAxis, actions_currentYAxis, actions_currentXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
                            //worksheet_Actions[actions_currentYAxis, actions_resetXAxis, actions_currentYAxis, actions_currentXAxis].Style.Fill.SolidFillOptions.BackColor = backColour;

                            worksheet_Actions.AutofitRow(actions_currentYAxis);

                            actions_maxYAxis = actions_currentYAxis;


                        }

                        worksheet_Actions.AutofitRows();
                        worksheet_Actions.SetRowHeightInPoints(10, defaultRowSize);

                        //add 5 to each row.
                        // for (int intCounter = actions_resetYAxis; intCounter <= actions_maxYAxis; intCounter++)
                        // {
                        //     worksheet_Actions[intCounter, actions_currentXAxis].RowHeightInPoints = worksheet_Actions[intCounter, actions_currentXAxis].RowHeightInPoints + 5;
                        // }
                        //


                    }




                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }










        #endregion EndActions


        //Actions tab
        #region Actions


        int obs_resetXAxis = 2;
        int obs_resetYAxis = 10;

        int obs_currentXAxis = obs_resetXAxis;
        int obs_currentYAxis = obs_resetYAxis;
        int obs_maxXAxis = obs_currentXAxis;
        int obs_maxYAxis = obs_currentYAxis;



        //Generate the observations
        try
        {

            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.usp_GenerateObservations", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                    cmd.Parameters.Add(new SqlParameter("@record_reference", auditRef));
                    cmd.Parameters.Add(new SqlParameter("@observation_reference", "0"));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {

                        // If we have actions, then make the tab.
                        ExcelWorksheet worksheet_AddObvs = workbook.Worksheets.AddWorksheet("Observations");
                        CopyPageSetup(worksheet_Report, worksheet_AddObvs);


                        worksheet_AddObvs[1, 1].ColumnWidthInChars = 2.5;
                        worksheet_AddObvs[2, 2].Text = worksheet_AddObvs[2, 2].Text + " - Additional Observations";


                        // Set up the column names with associated widths
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Status";
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].ColumnWidthInChars = 15;
                        obs_currentXAxis++;

                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Observation";
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].ColumnWidthInChars = 85;
                        obs_currentXAxis++;

                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Requirements";
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].ColumnWidthInChars = 85;
                        obs_currentXAxis++;

                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Assigned To";
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].ColumnWidthInChars = 35;
                        obs_currentXAxis++;

                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Due Date";
                        worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].ColumnWidthInChars = 20;


                        obs_maxXAxis = obs_currentXAxis;


                        //Format selection here.

                        worksheet_AddObvs[obs_currentYAxis, obs_resetXAxis, obs_currentYAxis, obs_maxXAxis].Style = sectionColumnHeaderStyle;


                        while (objrs.Read())
                        {
                            obs_currentYAxis++;
                            obs_currentXAxis = obs_resetXAxis;

                            string statusText;
                            string rowColour;
                            Color backColour = new Color();

                            switch (Convert.ToString(objrs["task_status"]))
                            {
                                case "2":
                                    statusText = "PENDING";
                                    backColour = Color.DarkRed;
                                    break;

                                case "1":
                                    statusText = "ACTIVE";
                                    backColour = Color.OrangeRed;
                                    break;

                                case "0":
                                    statusText = "COMPLETED";
                                    backColour = Color.DarkGreen;
                                    break;

                                default:
                                    statusText = "";
                                    backColour = Color.Black;
                                    break;
                            }


                            //Status COLOUR ROW FROM HERE
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = statusText;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style = sectionContent;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Font.Bold = true;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Font.Color = backColour;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            obs_currentXAxis++;

                          

                            //Observation
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = Formatting.FormatTextInputField(objrs["observation"].ToString());
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style = sectionContent;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Alignment.WrapText = true;

                            obs_currentXAxis++;

                            //Requirements
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = Formatting.FormatTextInputField(Convert.ToString(objrs["requirements"]));
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style = sectionContent;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Alignment.WrapText = true;
                            obs_currentXAxis++;

                            //Assigned to
                            if (Convert.ToString(objrs["actionedto"]) == "nreq")
                            {
                                worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "Nobody Required at this time.";
                            }
                            else
                            {
                                worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = objrs["name"].ToString();
                            }
                                
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style = sectionContent;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;
                            obs_currentXAxis++;

                            //Due Date

                            if(objrs["task_due_date"] == DBNull.Value)
                            {
                                worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = "";
                            }
                            else
                            {
                                worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Text = Convert.ToDateTime(objrs["task_due_date"]).ToString("dd-MM-yyyy");
                            }
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style = sectionContent;
                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].Style.Alignment.HorizontalAlignment = ExcelCellHorizontalAlignmentType.Center;


                            worksheet_AddObvs[obs_currentYAxis, obs_currentXAxis].RowHeightInPoints = defaultRowSize;


                            //worksheet_AddObvs[obs_currentYAxis, obs_resetXAxis, obs_currentYAxis, obs_currentXAxis].Style.Fill.FillType = ExcelCellFillType.SolidFill;
                            //worksheet_AddObvs[obs_currentYAxis, obs_resetXAxis, obs_currentYAxis, obs_currentXAxis].Style.Fill.SolidFillOptions.BackColor = backColour;

                            worksheet_AddObvs.AutofitRow(obs_currentYAxis);

                            obs_maxYAxis = obs_currentYAxis;


                        }

                        worksheet_AddObvs.AutofitRows();
                        worksheet_AddObvs.SetRowHeightInPoints(10, defaultRowSize);

                        //add 5 to each row.
                       // for (int intCounter = obs_resetYAxis + 1; intCounter <= obs_maxYAxis; intCounter++)
                       // {
                       //     int currentRowSize = (int)worksheet_AddObvs[intCounter, obs_currentXAxis].RowHeightInPoints;
                       //
                       //     //worksheet_AddObvs[intCounter, obs_currentXAxis].RowHeightInPoints = currentRowSize;
                       // }

                      

                    }




                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }










        #endregion EndActions







        // SAVE THE WORKBOOK
        string outFileName = "";

        // Save the Excel document in the current HTTP response stream

        if (corpCode == "061306")
        {
            outFileName = workbookFormat == ExcelWorkbookFormat.Xls_2003 ? "Audit Summary report.xls" : "Audit Summary report.xlsx";
        }
        else
        {
            outFileName = workbookFormat == ExcelWorkbookFormat.Xls_2003 ? auditRef + "SA - Audit Summary report.xls" : auditRef + "SA - Audit Summary report.xlsx";
        }

        HttpResponse httpResponse = HttpContext.Current.Response;

        // Prepare the HTTP response stream for saving the Excel document

        // Clear any data that might have been previously buffered in the output stream  
        httpResponse.Clear();

        // Set output stream content type for Excel 97-2003 (.xls) or Excel 2007(.xlsx)
        if (workbookFormat == ExcelWorkbookFormat.Xls_2003)
                httpResponse.ContentType = "Application/x-msexcel";
            else
                httpResponse.ContentType = "Application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            // Add the HTTP header to announce the Excel document either as an attachment or inline
        httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", outFileName));

            // Save the workbook to the current HTTP response output stream
            // and close the workbook after save to release all the allocated resources
        try
            {
                workbook.Save(httpResponse.OutputStream);
            }
            catch (Exception ex)
            {
                // report any error that might occur during save
                Session["ErrorMessage"] = ex.Message;

                Response.Write(ex.Message);
                Response.End();
                //Response.Redirect("ErrorPage.aspx");
            }
            finally
            {
                // close the workbook and release the allocated resources
                workbook.Close();

            }

            // End the response and finish the execution of this page
            httpResponse.End();

        }


    private void CopyPageSetup(ExcelWorksheet fromWorkSheet, ExcelWorksheet toWorkSheet)
    {

        //This function stops too much code duplication. All it does is copy worksheet based
        // parameters and copies them to another.

        toWorkSheet.PageSetup.PaperSize =    fromWorkSheet.PageSetup.PaperSize;
        toWorkSheet.PageSetup.Orientation =    fromWorkSheet.PageSetup.Orientation;
        toWorkSheet.PageSetup.LeftMargin =    fromWorkSheet.PageSetup.LeftMargin;
        toWorkSheet.PageSetup.RightMargin =    fromWorkSheet.PageSetup.RightMargin;
        toWorkSheet.PageSetup.TopMargin =    fromWorkSheet.PageSetup.TopMargin;
        toWorkSheet.PageSetup.BottomMargin =    fromWorkSheet.PageSetup.BottomMargin;
        //toWorkSheet.PageSetup.RightHeaderFormat = fromWorkSheet.PageSetup.RightHeaderFormat;
        //toWorkSheet.PageSetup.RightHeaderPicture = fromWorkSheet.PageSetup.RightHeaderPicture;
        //toWorkSheet.PageSetup.LeftHeaderFormat = fromWorkSheet.PageSetup.LeftHeaderFormat;

        toWorkSheet["B2"].Text = fromWorkSheet["B2"].Text;
        toWorkSheet["B5"].Text = fromWorkSheet["B5"].Text;
        toWorkSheet["B6"].Text = fromWorkSheet["B6"].Text;
        toWorkSheet["B7"].Text = fromWorkSheet["B7"].Text;

        toWorkSheet["B2"].Style = fromWorkSheet["B2"].Style;
        toWorkSheet["B4"].Style = fromWorkSheet["B4"].Style;
        toWorkSheet["B5"].Style = fromWorkSheet["B5"].Style;
        toWorkSheet["B6"].Style = fromWorkSheet["B6"].Style;
        toWorkSheet["B7"].Style = fromWorkSheet["B7"].Style;

    }


}

class Weighting
{
    public string Name { get; set; }
    public string Value { get; set; }

    public string Colour { get; set; }
}


