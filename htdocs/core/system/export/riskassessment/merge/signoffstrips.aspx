﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="signoffstrips.aspx.cs" Inherits="riskAssessment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><% =ts.GetResource("h1_ra") %></title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <link href="/version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="/core/modules/riskassessment/riskAssessment.css?123" rel="stylesheet" />


</head>
<body runat="server" id="pageBody">

    <form id="pageform" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>



        

        <div class="container">
            <div class="row">


                <div class="panel panel-default" id="headingFourContainer">
                    <div class="panel-heading click" role="tab" id="headingFour" data-toggle="collapse" data-parent="#accordion" data-target="#collapseFour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i><% =ts.GetResource("lbl_assessmentacceptance") %>
                            </a>
                        </h4>
                    </div>
                    <div runat="server" id="collapseFour" class="panel-collapse" role="tabpanel" aria-labelledby="headingFour">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-xs-12">


                                    <div class="col-xs-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">


                                                <asp:Literal ID="signoffTableContent" runat="server" />

                                            </table>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>





            </div>
            <!-- Main Col End -->
        </div>
        <!-- Main Row End -->



    </form>


</body>
</html>
