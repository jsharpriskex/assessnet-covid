﻿using System;
using System.Data.SqlClient;

using ASNETNSP;
using System.Data;
using System.Web.UI.HtmlControls;
using Winnovative.WnvHtmlConvert.PdfDocument;
using Telerik;
using Telerik.Web.UI;


using System.IO;
using System.Windows;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.Export;
using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.ColorSpaces;
using Telerik.Windows.Documents.Fixed.Model.Editing;
using Telerik.Windows.Documents.Fixed.Model.Editing.Flow;
using Telerik.Windows.Documents.Fixed.Model.Fonts;
using Telerik.Windows.Documents.Fixed.Model.Graphics;

using System.Windows.Media;
using Telerik.Windows.Documents.Fixed.Model.Editing.Tables;
using HorizontalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.HorizontalAlignment;
using VerticalAlignment = Telerik.Windows.Documents.Fixed.Model.Editing.Flow.VerticalAlignment;
using System.Collections.Generic;
using System.Web;

public partial class MergePDF : System.Web.UI.Page
{

    public string corpCode = "222999";
    public string recordReference = "3584RA";
    public string currentUser = "O4HdIUqF1obedHn";
    public string idOfJob = "";

    private static readonly double defaultLeftIndent = 25;
    private static readonly double defaultLineHeight = 18;

    private static readonly double defaultTopOffset = 25;

    private static readonly double pageWidth = 1128; //792
    private static readonly double pageHeight = 792; //1128

    //private static readonly double pageWidth = 792; //
    //private static readonly double pageHeight = 1128; //

    private static readonly double maxLeftIndent = pageWidth - defaultLeftIndent;
    private static readonly double maxTopOffset = pageHeight - (defaultTopOffset * 2);

    private static readonly double maxWidth = pageWidth - defaultLeftIndent * 2;


    private double halfPageColumn = (maxWidth / 2) - 25;

    // Variables that dynamically change
    private double currentTopOffset;
    private RadFixedPage currentPage;
    private FixedContentEditor currentEditor;
    private RadFixedDocument currentDocument;


    // Collection that holds the pages
    private List<RadFixedPage> pageList;


    private Size table1Height;
    private Size table2Height;

    private RiskAssessment currentRA;

    protected void Page_PreInit(object sender, EventArgs e)
    {

        corpCode = Request["ccode"] ?? "222999";
        recordReference = Request["ref"] ?? "3584RA";
        idOfJob = Request["guid"] ?? "";

        if (Request["ccode"] != null)
        {
            PdfFormatProvider formatProvider = new PdfFormatProvider();
            formatProvider.ExportSettings.ImageQuality = ImageQuality.High;

            byte[] renderedBytes = null;
            using (MemoryStream ms = new MemoryStream())
            {
                RadFixedDocument document = this.CreateDocument();
                formatProvider.Export(document, ms);
                renderedBytes = ms.ToArray();
            }

            SaveToDisk(renderedBytes);

            //Response.Clear();
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + recordReference + "_" + DateTime.Now.ToString() + ".pdf");
            //Response.ContentType = "application/pdf";
            //Response.BinaryWrite(renderedBytes);
            
        }


       

    }



    private RadFixedDocument CreateDocument()
    {

        currentRA = new RiskAssessment(corpCode, recordReference, currentUser);
        currentRA.GenerateGenericInformation();

        RadFixedDocument document = new RadFixedDocument();
        currentDocument = document;

        pageList = new List<RadFixedPage>();


        CreateNewPage();

        // Add the Document Header
        DrawHeader();

        GenericInformation();

        PersonsAffected();

        Hazards();

        currentTopOffset += defaultLineHeight * 5;
        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);


        currentEditor.Position.Translate(defaultLeftIndent * 4, currentPage.Size.Height - 100);


        //this.DrawText(editor);

        return document;
    }

    private void SetPageProperties()
    {
        currentPage.Size = new Size(pageWidth, pageHeight);
        currentEditor.TextProperties.FontSize = 14;
 

    }

    private void CreateNewPage()
    {
        
        RadFixedPage page = currentDocument.Pages.AddPage();
        pageList.Add(page);

        FixedContentEditor editor = new FixedContentEditor(page);
        currentTopOffset = defaultTopOffset;
        editor.Position.Translate(defaultLeftIndent, defaultTopOffset);

        // Set initial page as the current page, this allows for multi pages.
        currentEditor = editor;
        currentPage = page;

        // Setup the initial page properties, this will be ran when another page gets made.
        SetPageProperties();

        

    }

    private void DrawHeader()
    {


        Table headerTable = new Table();
        TableRow headerRow = headerTable.Rows.AddTableRow();
        TableCell headerLogoCell = headerRow.Cells.AddTableCell();
        
        // Write Image to the header
        Block logoBlock = headerLogoCell.Blocks.AddBlock();
        using (FileStream fs = new FileStream(Server.MapPath("/version3.2/security/login/assessnet-logo-v1.gif"), FileMode.Open, FileAccess.Read))
        {
            logoBlock.InsertImage(fs, 150, 50);
        }

        // Set the logo's cell to be slightly higher than the size of the image.
        headerLogoCell.PreferredWidth = 125;

        // Write Text to the header
        TableCell headerTextCell = headerRow.Cells.AddTableCell();
        headerTextCell.PreferredWidth = maxWidth - 150;

        Block headerTextBlock = headerTextCell.Blocks.AddBlock();
        
        using (headerTextBlock.SaveGraphicProperties())
        {
            PDFStyles.TextType.HeaderTitle(headerTextBlock);
            headerTextBlock.InsertText("Risk Assessment");
        }

        PDFStyles.TextType.HeaderInfo(headerTextBlock);
        headerTextBlock.TextProperties.Font = FontsRepository.Helvetica;
        headerTextBlock.InsertText(" - " + recordReference);

        headerTextBlock.InsertLineBreak();
        PDFStyles.TextType.HeaderSmall(headerTextBlock);
        headerTextBlock.InsertText("Information valid as of " + DateTime.Now.ToString());


        currentEditor.DrawTable(headerTable, new Size(maxWidth, double.PositiveInfinity));

        currentTopOffset = 100;
        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

    }

   
    private void GenericInformation()
    {

        currentTopOffset += defaultLineHeight;

        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);


        //---------------------------------------------------------------------
        // Sign Off

        // REMOVED DUE TO REQUIREMENT.
        //GenerateAlert();

        // End of Sign Off
        //---------------------------------------------------------------------


        //Create Table for left Content
        Table basicInformationParent = new Table();

        TableRow basicInformationParent_Row = basicInformationParent.Rows.AddTableRow();
        TableCell basicInformationParent_Cell = basicInformationParent_Row.Cells.AddTableCell();
        basicInformationParent_Cell.PreferredWidth = halfPageColumn;


        // Inner table for content
        Table basicInformation = basicInformationParent_Cell.Blocks.AddTable();
        PDFStyles.TableType.GenericTable(basicInformation);

        TableRow basicInformation_Row = basicInformation.Rows.AddTableRow();
        TableCell basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();

        //Assessed By - BE CAREFUL MOVING THIS!
      
        basicInformation_Cell.PreferredWidth = halfPageColumn / 2;
        AddTextToCell(basicInformation_Cell, "Assessed By", true);
        

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.PreferredWidth = halfPageColumn / 2;
        AddTextToCell(basicInformation_Cell, currentRA.AssessedByName, false);

        // If there is a internal reference, then..
        if (currentRA.HasInternalReference())
        {
            basicInformation_Row = basicInformation.Rows.AddTableRow();
            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, "Internal Reference", true);

            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, currentRA.InternalRef, false);
        }
       
        //Date Assessed

        basicInformation_Row = basicInformation.Rows.AddTableRow();
        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        AddTextToCell(basicInformation_Cell, "Date Assessed", true);

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        AddTextToCell(basicInformation_Cell, currentRA.AssessmentDate.ToShortDateString(), false);


        if (currentRA.ReviewNotRequired)
        {
            basicInformation_Row = basicInformation.Rows.AddTableRow();
            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, "Review", true);

            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, "Not Required", false);
        }
        else
        {
            //Assigned Reviewer
            basicInformation_Row = basicInformation.Rows.AddTableRow();
            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, "Assigned Reviewer", true);

            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, currentRA.AssignedReviewerName, false);

            // Next review date

            basicInformation_Row = basicInformation.Rows.AddTableRow();
            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, "Next Review Date", true);

            basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
            AddTextToCell(basicInformation_Cell, currentRA.NextReviewDate.ToShortDateString(), false);
        }


        // Title

        basicInformation_Row = basicInformation.Rows.AddTableRow();
        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        AddTextToCell(basicInformation_Cell, "Operation Assessed", true);

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        AddTextToCell(basicInformation_Cell, currentRA.Title, false);


        // if height is more than the page height... new page.
        double sizeOfGeneric = basicInformationParent.Measure().Height;
        CheckForHeightOverflow(sizeOfGeneric);
        currentEditor.DrawBlock(basicInformationParent);

        


        // Table for right content

        currentEditor.Position.Translate((maxWidth / 2) + 50, currentTopOffset);

        string structureText = "";

        structureText += currentRA.Tier2Name;



        if(currentRA.Tier3Name != "")
        {
            structureText += " > " + currentRA.Tier3Name;
        }

        if (currentRA.Tier4Name != "")
        {
            structureText += " > " + currentRA.Tier4Name;
        }

        if (currentRA.Tier5Name != "")
        {
            structureText += " > " + currentRA.Tier5Name;
        }

        if (currentRA.Tier6Name != "")
        {
            structureText += " > " + currentRA.Tier6Name;
        }



        



        // Structure
        Table structureTable = new Table();
        PDFStyles.TableType.GenericTable(structureTable);
        TableRow structureTableRow = structureTable.Rows.AddTableRow();
        TableCell structureCell = structureTableRow.Cells.AddTableCell();
        structureCell.PreferredWidth = halfPageColumn;

        // Header
        structureCell.Background = PDFStyles.Colours.HeaderYellow;
        AddTextToCell(structureCell, "Structure", true);

        // Description Text
        structureTableRow = structureTable.Rows.AddTableRow();
        structureCell = structureTableRow.Cells.AddTableCell();
        AddTextToCell(structureCell, structureText, false);


        double sizeOfStructure = structureTable.Measure().Height;
        CheckForHeightOverflow(sizeOfStructure);
        currentEditor.DrawBlock(structureTable);


        // Set the current offset of the largest size of the content + the default spacing + the current offset.
        currentTopOffset += Math.Max(sizeOfStructure, sizeOfGeneric) + defaultLineHeight;
        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

        // Description
        Table descriptionTable = new Table();
        PDFStyles.TableType.GenericTable(descriptionTable);
        TableRow descriptionTableRow = descriptionTable.Rows.AddTableRow();
        TableCell descriptionCell = descriptionTableRow.Cells.AddTableCell();
        descriptionCell.PreferredWidth = maxWidth;

        // Header
        descriptionCell.Background = PDFStyles.Colours.HeaderYellow;
        AddTextToCell(descriptionCell, "Description of work area and/or activity assessed", true);

        // Description Text
        descriptionTableRow = descriptionTable.Rows.AddTableRow();
        descriptionCell = descriptionTableRow.Cells.AddTableCell();
        AddTextToCell(descriptionCell, currentRA.Description, false);


        double sizeOfDescription = descriptionTable.Measure().Height;
        CheckForHeightOverflow(sizeOfDescription);
        currentEditor.DrawBlock(descriptionTable);



        currentTopOffset += sizeOfDescription + defaultTopOffset;
        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

    }

    private void PersonsAffected()
    {

        

        // Using var in pace of List<RiskAssessment.PersonsAffected.PersonsAffectedType>.
        // This is to be looped through to add the persons affected
        var personAffectedList = currentRA.ReturnPersonsAffectedList();


        // Persons Affected Parent Table
        Table personsAffectedTable = new Table();
        PDFStyles.TableType.GenericTable(personsAffectedTable);
        TableRow personsAffectedRow = personsAffectedTable.Rows.AddTableRow();
        TableCell personsAffectedCell = personsAffectedRow.Cells.AddTableCell();
        personsAffectedCell.PreferredWidth = maxWidth;

        // Header
        personsAffectedCell.Background = PDFStyles.Colours.HeaderYellow;
        AddTextToCell(personsAffectedCell, "Persons Affected", true);

        // Main table cell
        personsAffectedRow = personsAffectedTable.Rows.AddTableRow();
        personsAffectedCell = personsAffectedRow.Cells.AddTableCell();
        //AddTextToCell(personsAffectedCell, currentRA.description, false);


        // Calculate width of the persons affected table, so that we know what we have access to in other tables.
        double workableWidth = personsAffectedTable.Measure().Width;
        double widthOfNumber = 50;
        double widthOfTitle = 165;
        double widthOfElement = widthOfNumber + widthOfTitle;
        double widthOfGap = 0;
        double numberPerRow = 0;


        // Setup the amount of columns per row, so that we can ave more items when the page is in landscape.
        // This also helps with spacing.
        if (workableWidth < 800)
        {
            // Portrait
            numberPerRow = 3;
        }
        else
        {
            // Landsacpe
            numberPerRow = 4;
        }

        widthOfGap = ((workableWidth - (widthOfElement * numberPerRow)) / (numberPerRow - 1)) + 5;


        Table innerPersonsAffectedTable = personsAffectedCell.Blocks.AddTable();
        innerPersonsAffectedTable.Margin = new Thickness(0);
        innerPersonsAffectedTable.DefaultCellProperties.Padding = new Thickness(0);

        TableRow innerPersonsAffectedRow = innerPersonsAffectedTable.Rows.AddTableRow();



        double currentTotalOfColumns = 1;

        // Loop through Persons Affected Options
        foreach (var pa in personAffectedList)
        {

            if (currentTotalOfColumns > numberPerRow)
            {
                // New Row if the amount of rows exceed the amount allowed
                innerPersonsAffectedRow = innerPersonsAffectedTable.Rows.AddTableRow();
                currentTotalOfColumns = 1;
            }

            AddPersonsAffectedTableCell(innerPersonsAffectedRow, widthOfNumber, widthOfTitle, Convert.ToString(pa.personsNumber), pa.personsTitle);
            if(currentTotalOfColumns != numberPerRow)
            {
                AddGapToPersonsAffected(innerPersonsAffectedRow, widthOfGap);
            }
            
            currentTotalOfColumns += 1;

        }
    

        //// Add information
        //AddPersonsAffectedTableCell(innerPersonsAffectedRow, widthOfNumber, widthOfTitle, "123", "Employee");
        //AddGapToPersonsAffected(innerPersonsAffectedRow, widthOfGap);
        //AddPersonsAffectedTableCell(innerPersonsAffectedRow, widthOfNumber, widthOfTitle, "123", "Employee");
        //AddGapToPersonsAffected(innerPersonsAffectedRow, widthOfGap);
        //AddPersonsAffectedTableCell(innerPersonsAffectedRow, widthOfNumber, widthOfTitle, "123", "Employee");




        double sizeOfPersonsAffected = personsAffectedTable.Measure().Height;
        CheckForHeightOverflow(sizeOfPersonsAffected);
        currentEditor.DrawBlock(personsAffectedTable);

        currentTopOffset += sizeOfPersonsAffected + defaultTopOffset;
        currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

    }

    private void Hazards()
    {

        // Return current hazards from the current RA
        var hazardList = currentRA.ReturnIndividualHazardList();


        // Loop through each hazard, and write content to page
        foreach (var hazard in hazardList)
        {

            // Lets gather a list of the current actions.
            // NOTE: This can be done better to use less memory! Currently it puts all actions in memory (collection of actions) per hazard. 
            //       Instead, this could be a method that generates on the fly, rather than when the hazard list is initiated.
            var currentActions = hazard.Actions.ListOfActions;

        

            

            // Create Table that stores hazard information
            Table hazardTable = new Table();
            PDFStyles.TableType.GenericTable(hazardTable);

            // Header of Table

            TableRow hazardRow = hazardTable.Rows.AddTableRow();
            TableCell hazardCell = hazardRow.Cells.AddTableCell();

            hazardCell.PreferredWidth = maxWidth * 0.25;
            AddTextToCell(hazardCell, "Current Rating", true, HorizontalAlignment.Center);

            hazardCell = hazardRow.Cells.AddTableCell();
            hazardCell.PreferredWidth = maxWidth * 0.75;
            AddTextToCell(hazardCell, "Hazard Information", true);

            // Header End

            // Content of Table

            hazardRow = hazardTable.Rows.AddTableRow();

            hazardCell = hazardRow.Cells.AddTableCell();
            AddTextToCell(hazardCell, hazard.CurrentRisk, false, HorizontalAlignment.Center);


            hazardCell = hazardRow.Cells.AddTableCell();
            AddTextToCell(hazardCell, hazard.Type, false);
            AddTextToCell(hazardCell, hazard.Description, false);


            // Content End

            // Write current content to page
            double sizeOfHazardMain = hazardTable.Measure().Height;
            CheckForHeightOverflow(sizeOfHazardMain);
            currentEditor.DrawBlock(hazardTable);


            // Move the current top offset to just below the table
            currentTopOffset += sizeOfHazardMain;
            currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

            // Measures currently in place
            Table measuresInPlace = new Table();
            PDFStyles.TableType.GenericTable(measuresInPlace);

            TableRow measuresRow = measuresInPlace.Rows.AddTableRow();
            TableCell measuresCell = measuresRow.Cells.AddTableCell();

            measuresCell.PreferredWidth = maxWidth;
            AddTextToCell(measuresCell, "Measures Currently in Place", true);

            measuresRow = measuresInPlace.Rows.AddTableRow();

            measuresCell = measuresRow.Cells.AddTableCell();
            AddTextToCell(measuresCell, hazard.MeasuresInPlace, false);

            // Write current content to page
            double sizeOfMeasures = measuresInPlace.Measure().Height;
            CheckForHeightOverflow(sizeOfMeasures);
            currentEditor.DrawBlock(measuresInPlace);


            // Move the current top offset to just below the table
            currentTopOffset += sizeOfMeasures;
            currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

            //Attachments
            Table attachmentsTable = new Table();
            //PDFStyles.TableType.GenericTable(attachmentsTable);

            //TableRow attachmentsRow = attachmentsTable.Rows.AddTableRow();
            //TableCell attachmentsCell = attachmentsRow.Cells.AddTableCell();

            //attachmentsCell.PreferredWidth = maxWidth;
            //AddTextToCell(attachmentsCell, "Attached Photos", true);

            //attachmentsRow = attachmentsTable.Rows.AddTableRow();

            //attachmentsCell = attachmentsRow.Cells.AddTableCell();
            //AddTextToCell(attachmentsCell, "ATTACHMENT", false);
            //AddTextToCell(attachmentsCell, "ATTACHMENT", false);
            //AddTextToCell(attachmentsCell, "ATTACHMENT", false);
            //AddTextToCell(attachmentsCell, "ATTACHMENT", false);
            //AddTextToCell(attachmentsCell, "ATTACHMENT", false);

            // Write current content to page
            double sizeOfAttachments = attachmentsTable.Measure().Height;
            CheckForHeightOverflow(sizeOfAttachments);
            currentEditor.DrawBlock(attachmentsTable);

            // Move the current top offset to just below the table
            currentTopOffset += sizeOfAttachments;
            currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);


            // End of Attachments

            // Potential Information
            // Only show if there are actions!
            //if (currentActions.Count > 0)
            //{

            //    // So, every part of this is going to be a new table, so that the page can dynamically split.
            //    // We need to have the header stored as a table we can copy.

            //    Table currentActionsHeader = GenerateActionsHeader(hazard.PotentialRisk);
            //    double sizeOfActionsHeader = currentActionsHeader.Measure().Height;
            //    CheckForHeightOverflow(sizeOfActionsHeader);
            //    currentEditor.DrawBlock(currentActionsHeader);
            //    // End of header table


            //    double sizeOfPreviousTable = sizeOfActionsHeader;
                
            //        // Loop through the actions, create a new table and insert info!
            //        foreach (var action in currentActions)
            //        {
            //            // Move the current position to jst below current table
            //            currentTopOffset += sizeOfPreviousTable;
            //            currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);

            //            Table currentActionTable = new Table();
            //            PDFStyles.TableType.GenericTable(currentActionTable);
            //            TableRow row = currentActionTable.Rows.AddTableRow();
            //            TableCell cell = row.Cells.AddTableCell();

            //            cell.PreferredWidth = maxWidth * 0.60;
            //            AddTextToCell(cell, action.ActionDetails, false);

            //            cell = row.Cells.AddTableCell();
            //            cell.PreferredWidth = maxWidth * 0.20;
            //            AddTextToCell(cell, action.DueDate.ToShortDateString(), false, HorizontalAlignment.Center);
            //            AddTextToCell(cell, action.AssigneeText, false, HorizontalAlignment.Center);


            //            cell = row.Cells.AddTableCell();
            //            cell.PreferredWidth = maxWidth * 0.20;


            //            cell.Padding = new Thickness(10);

            //            // Insert an iner table that handles the status - Status styling MESSY
            //            Table statusTable = cell.Blocks.AddTable();
            //            Border currentBorder = new Border(3, BorderStyle.Single, action.ActionStatus.BorderColour);

            //            statusTable.DefaultCellProperties.Borders = new TableCellBorders(currentBorder, currentBorder, currentBorder, currentBorder);

            //            TableRow statusRow = statusTable.Rows.AddTableRow();
            //            TableCell statusCell = statusRow.Cells.AddTableCell();
            //            statusCell.PreferredWidth = 1000;
            //            statusCell.Background = action.ActionStatus.BackgroundColour;
            //            statusCell.Padding = new Thickness(5);

            //            Block statusBlock = statusCell.Blocks.AddBlock();
            //            statusBlock.BackgroundColor = action.ActionStatus.BackgroundColour;

            //            PDFStyles.TextType.StatusText(statusBlock);
            //            statusBlock.InsertText(action.ActionStatus.StatusText.ToUpper());

                        

            //            double sizeOfCurrentAction = currentActionTable.Measure().Height;
            //            CheckForHeightOverflow(sizeOfCurrentAction);
            //            currentEditor.DrawBlock(currentActionTable);


            //            //Revert the size of the previous table to the current one. This makes sure the spacing is maintained.
            //            sizeOfPreviousTable = sizeOfCurrentAction;

            //        }

               



            //    // Content End
            //    currentTopOffset += sizeOfPreviousTable + defaultTopOffset;
            //    currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);
            //}
            //else
            //{
                // No further sections so we create space based from the attachments table.

                currentTopOffset += sizeOfAttachments + defaultTopOffset;
                currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);
            //}

            

           
        }



       


    }

    private Table GenerateActionsHeader(string potentialRiskText)
    {
        Table headerOfActions = new Table();
        PDFStyles.TableType.GenericTable(headerOfActions);
        TableRow headerRow = headerOfActions.Rows.AddTableRow();
        TableCell headerCell = headerRow.Cells.AddTableCell();
        headerCell.PreferredWidth = maxWidth * 0.60;

        headerCell.Background = PDFStyles.Colours.HeaderYellow;

        Table controlsWithPotential = headerCell.Blocks.AddTable();
        TableRow row = controlsWithPotential.Rows.AddTableRow();
        TableCell cell = row.Cells.AddTableCell();
        Block textBlock = cell.Blocks.AddBlock();
        textBlock.InsertText("Additional Controls");
        cell.PreferredWidth = maxWidth * 0.45;

        cell = row.Cells.AddTableCell();
        textBlock = cell.Blocks.AddBlock();
        textBlock.HorizontalAlignment = HorizontalAlignment.Right;
        textBlock.InsertText("Potential Risk: " + potentialRiskText);


        headerCell = headerRow.Cells.AddTableCell();
        headerCell.PreferredWidth = maxWidth * 0.20;
        AddTextToCell(headerCell, "Due by / Whom", true, HorizontalAlignment.Center);


        headerCell = headerRow.Cells.AddTableCell();
        headerCell.PreferredWidth = maxWidth * 0.20;
        AddTextToCell(headerCell, "Action Status", true, HorizontalAlignment.Center);

        return headerOfActions;
    }

    private void AddPersonsAffectedTableCell(TableRow personsRow, double widthOfNumber, double widthOfTitle, string numberText, string titleText)
    {
        // Add a cell to the existing table that is passed through
        TableCell innerPersonsAffectedCell = personsRow.Cells.AddTableCell();

        // Make a new table to add content to
        Table parentTable = innerPersonsAffectedCell.Blocks.AddTable();
        PDFStyles.TableType.GenericTable(parentTable);

        TableRow parentTableRow = parentTable.Rows.AddTableRow();

        TableCell parentTableCell = parentTableRow.Cells.AddTableCell();
        parentTableCell.PreferredWidth = widthOfNumber;
        AddTextToCell(parentTableCell, numberText, true);

        parentTableCell.Background = PDFStyles.Colours.LighterGrey;
 
        parentTableCell = parentTableRow.Cells.AddTableCell();
        parentTableCell.PreferredWidth = widthOfTitle;
        AddTextToCell(parentTableCell, titleText, false);

    }

    private void AddGapToPersonsAffected(TableRow personsRow, double widthOfGap)
    {
        TableCell innerPersonsAffectedCell = personsRow.Cells.AddTableCell();
        innerPersonsAffectedCell.PreferredWidth = widthOfGap;
    }

    private void AddTextToCell(TableCell cell, string text, bool isHeader, HorizontalAlignment alignment = HorizontalAlignment.Left)
    {
        Block textBlock = cell.Blocks.AddBlock();
        PDFStyles.TextType.Standard(textBlock);
        textBlock.HorizontalAlignment = alignment;
        if (isHeader)
        {
            //textBlock.TextProperties.Font = FontsRepository.HelveticaBold;
            cell.Background = PDFStyles.Colours.HeaderYellow;
        }
        //textBlock.InsertText(text);

        ProcessCarriageReturnsForBlock(textBlock, text);

        // ON TEXT FROM DB, THIS NEEDS TO SPLIT ALL LINEBREAKS!

    }

    private void ProcessCarriageReturnsForBlock(Block block, string text)
    {

        text = Formatting.FormatTextInputField(text); 

        char[] charToSplit = { (char)13, (char)10 };
        string[] splitText = text.Split(charToSplit);

        foreach (var sText in splitText)
        {

            block.InsertText(sText);
            block.InsertLineBreak();
        }

    }


    private void GenerateAlert()
    {

        currentRA.GenerateBannerInformation();
        if (currentRA.HasAlert)
        {
            Table alertBanner = new Table();
            PDFStyles.TableType.AlertRed_Table(alertBanner);
            TableRow alertBannerTableRow = alertBanner.Rows.AddTableRow();
            TableCell alertBannerCell = alertBannerTableRow.Cells.AddTableCell();
            alertBannerCell.PreferredWidth = maxWidth;
            alertBannerCell.Background = PDFStyles.Colours.AlertBackground_Red;

            Block textBlock = alertBannerCell.Blocks.AddBlock();

            PDFStyles.TextType.AlertTitle(textBlock);


            //Content
            textBlock.InsertText(currentRA.AlertTitle);
            textBlock.InsertLineBreak();

            PDFStyles.TextType.AlertText(textBlock);
            textBlock.InsertText(currentRA.AlertMessage);


            

            double alertBannerHeight = alertBanner.Measure().Height;
            CheckForHeightOverflow(alertBannerHeight);
            currentEditor.DrawBlock(alertBanner);


            currentTopOffset += alertBannerHeight + defaultTopOffset;
            currentEditor.Position.Translate(defaultLeftIndent, currentTopOffset);
        }

        


        
    }


    private void CheckForHeightOverflow(double blockHeight)
    {
        // If the blocks height is greater than that of the space left of the page, then we need to create a new page and resume.
        if(blockHeight > (maxTopOffset - currentTopOffset))
        {
            CreateNewPage();
        }
    }





    // OLD - FOR REFERENCE
    private void DrawText(FixedContentEditor editor)
    {
        





        DrawTable(editor, currentTopOffset);
        

        editor.Position.Translate(defaultLeftIndent, currentTopOffset + 200);



        Table testTable = new Table();
        TableRow testTableRow = testTable.Rows.AddTableRow();
        TableCell testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.PreferredWidth = 1000;
        testTableCell.Blocks.AddBlock().InsertText("Test 1");

        testTableRow = testTable.Rows.AddTableRow();
        testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.Blocks.AddBlock().InsertText("Test 2");

        testTableRow = testTable.Rows.AddTableRow();
        testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.Blocks.AddBlock().InsertText("Test 3");


        testTableRow = testTable.Rows.AddTableRow();
        testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.Blocks.AddBlock().InsertText("Table Current " + testTable.Measure().ToString());

        testTableRow = testTable.Rows.AddTableRow();
        testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.Blocks.AddBlock().InsertText("Table 1 " + table1Height.ToString());

        testTableRow = testTable.Rows.AddTableRow();
        testTableCell = testTableRow.Cells.AddTableCell();
        testTableCell.Blocks.AddBlock().InsertText("Table 2 " + table2Height.ToString());


        editor.DrawTable(testTable, new Size(180, double.PositiveInfinity));


    }

    private void DrawTable(FixedContentEditor editor, double currentTopOffset)
    {

        currentTopOffset += defaultLineHeight;

        editor.Position.Translate(defaultLeftIndent, currentTopOffset);


        //Styles
       

        RgbColor greyHeaderBackground = new RgbColor(220, 220, 220);


        //Create Table for left Content
        Table basicInformationParent = new Table();
        //basicInformationParent.DefaultCellProperties.Borders = new TableCellBorders(blackBorder, blackBorder, blackBorder, blackBorder); ;
        //basicInformationParent.DefaultCellProperties.Padding = new Thickness(1, 1, 1, 1);

        TableRow basicInformationParent_Row = basicInformationParent.Rows.AddTableRow();
        TableCell basicInformationParent_Cell = basicInformationParent_Row.Cells.AddTableCell();
        basicInformationParent_Cell.PreferredWidth = 500;
        //basicInformationParent_Cell.Blocks.AddBlock().InsertText("Parent");




        // Inner table for content
        Table basicInformation = basicInformationParent_Cell.Blocks.AddTable(); ;
        basicInformation.DefaultCellProperties.Borders = new TableCellBorders(PDFStyles.BorderType.ThinBlack, PDFStyles.BorderType.ThinBlack, PDFStyles.BorderType.ThinBlack, PDFStyles.BorderType.ThinBlack);
        basicInformation.DefaultCellProperties.Padding = new Thickness(20, 10, 20, 10);

        TableRow basicInformation_Row = basicInformation.Rows.AddTableRow();
        TableCell basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.PreferredWidth = 200;
        basicInformation_Cell.Blocks.AddBlock().InsertText("Internal Reference");
        basicInformation_Cell.Background = PDFStyles.Colours.HeaderYellow;

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.PreferredWidth = 300;
        basicInformation_Cell.Blocks.AddBlock().InsertText("Not Specified");


        basicInformation_Row = basicInformation.Rows.AddTableRow();
        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.Blocks.AddBlock().InsertText("Date Asssessed");
        basicInformation_Cell.Background = PDFStyles.Colours.HeaderYellow;

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.Blocks.AddBlock().InsertText("19/10/2018");

        basicInformation_Row = basicInformation.Rows.AddTableRow();
        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.Blocks.AddBlock().InsertText("Assessed By");
        basicInformation_Cell.Background = PDFStyles.Colours.HeaderYellow;

        basicInformation_Cell = basicInformation_Row.Cells.AddTableCell();
        basicInformation_Cell.Blocks.AddBlock().InsertText("Adam Prosser");

        editor.DrawBlock(basicInformationParent);


        table1Height = basicInformationParent.Measure();

        // Table for right content

        editor.Position.Translate(defaultLeftIndent + 525, currentTopOffset);

        Table basicStructureParent = new Table();
        //basicStructureParent.DefaultCellProperties.Borders = new TableCellBorders(blackBorder, blackBorder, blackBorder, blackBorder); ;
        //basicStructureParent.DefaultCellProperties.Padding = new Thickness(1, 1, 1, 1);

        TableRow basicStructureParent_Row = basicStructureParent.Rows.AddTableRow();
        TableCell basicStructureParent_Cell = basicStructureParent_Row.Cells.AddTableCell();
        basicStructureParent_Cell.PreferredWidth = 500;
        //basicStructureParent_Cell.Blocks.AddBlock().InsertText("Parent!");


        //secondRow.Cells.AddTableCell();


    }


    protected void generatePDF_Click(object sender, EventArgs e)
    {
        PdfFormatProvider formatProvider = new PdfFormatProvider();
        formatProvider.ExportSettings.ImageQuality = ImageQuality.High;

        byte[] renderedBytes = null;
        using (MemoryStream ms = new MemoryStream())
        {
            RadFixedDocument document = this.CreateDocument();
            formatProvider.Export(document, ms);
            renderedBytes = ms.ToArray();
        }

        SaveToDisk(renderedBytes);

        //Response.Clear();
        //Response.AppendHeader("Content-Disposition", "attachment; filename=" + recordReference + "_" + DateTime.Now.ToString() + ".pdf");
        //Response.ContentType = "application/pdf";
        //Response.BinaryWrite(renderedBytes);
        Response.End();

    }

    private void SaveToDisk(byte[] renderedBytes)
    {
        try
        {
            //DirectoryInfo di = Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/" + corpCode + "/" + currentUser);
            //File.WriteAllBytes(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/" + corpCode + "/" + currentUser + "/" + recordReference + ".pdf", renderedBytes);
            DirectoryInfo di = Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/");
            File.WriteAllBytes(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/" + idOfJob + "-" + recordReference + ".pdf", renderedBytes);

            Response.Write("200");
        }
        catch (Exception e)
        {
            Response.Write("500");
            throw;
        }

       
        Response.End();
    }


}


public class RiskAssessment
{
    public string AssessedBy;
    public string AssessedByName;
    public DateTime AssessmentDate;
    public string AssignedReviewer;
    public string AssignedReviewerName;
    public bool ReviewNotRequired;
    public DateTime NextReviewDate;
    public string InternalRef;

    public bool RequiresSignOff;
    public string SignOffBy;
    public DateTime SignOffDate;

    public string Tier2Name;
    public string Tier3Name;
    public string Tier4Name;
    public string Tier5Name;
    public string Tier6Name;

    public string Title;
    public string Description;

    public string Status;

    public bool HasAlert;
    public string AlertTitle;
    public string AlertMessage;

    private readonly string _corpCode;
    private readonly string _recordReference;
    private readonly string _currentUser;

    protected bool IsInitiated = false;
    private readonly bool _clientHasSignOff = false;

    private PersonsAffected PersonsAffectedInstance;
    private Hazards HazardInstance;


    public RiskAssessment(string corpCode, string recordReference, string currentUser)
    {
        _corpCode = corpCode;
        _recordReference = recordReference;
        _currentUser = currentUser;
        _clientHasSignOff = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ASSESSMENT_SIGNOFF", "pref") == "Y" ? true : false ;

        PersonsAffectedInstance = new PersonsAffected(this);
        HazardInstance = new Hazards(this);
        IsInitiated = true;
    }


    public void GenerateGenericInformation()
    {
        if (IsInitiated)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_RA.dbo.GenerateGeneralDetails", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corpCode", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordReference", _recordReference));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();


                    AssessedBy = Convert.ToString(objrs["owner_id"]);
                    AssessmentDate = Convert.ToDateTime(objrs["recorddate"]);
                    

                    Tier2Name = Convert.ToString(objrs["tier2_name"]);
                    Tier3Name = Convert.ToString(objrs["tier3_name"]);
                    Tier4Name = Convert.ToString(objrs["tier4_name"]);
                    Tier5Name = Convert.ToString(objrs["tier5_name"]);
                    Tier6Name = Convert.ToString(objrs["tier6_name"]);

                    Title = Convert.ToString(objrs["recordequipment"]);
                    Description = Convert.ToString(objrs["recorddescription"]);

                    Status = Convert.ToString(objrs["status"]);
                    InternalRef = Convert.ToString(objrs["Rec_intREF"]);

                    AssessedByName = UserControls.GetUser(AssessedBy, _corpCode);

                    

                    //Check to see if its not required..
                    string reviewTaskCode = Convert.ToString(objrs["review_task_code"]);
                    if(reviewTaskCode == "nreq")
                    {
                        ReviewNotRequired = true;
                    }
                    else
                    {
                        AssignedReviewer = Convert.ToString(objrs["task_account"]);
                        NextReviewDate = Convert.ToDateTime(objrs["task_due_date"]);
                        AssignedReviewerName = UserControls.GetUser(AssignedReviewer, _corpCode);
                    }

                    if (Tier2Name == "")
                    {
                        Tier2Name = "All";
                    }


                    // Check to see if a signoff is really required..
                    if (_clientHasSignOff)
                    {
                        if(Convert.ToString(objrs["SignOff_Type"]) == "SIGNED OFF")
                        {
                            RequiresSignOff = false;
                            SignOffBy = Convert.ToString(objrs["SignOff_Name"]);
                            SignOffDate = Convert.ToDateTime(objrs["SignOff_DateTime"]);
                        }
                        else
                        {
                            RequiresSignOff = true;
                            SignOffBy = "--";
                        }
                    }

                }



                objrs.Close();
            }
        }
        else
        {
            throw new NotSupportedException("The Risk Assessment object must be initialised before using this method.");
        }
    }

    public void GenerateBannerInformation()
    {
        //If it needs to be signed off
        if(Status == "4" && _clientHasSignOff)
        {
            HasAlert = true;
            AlertTitle = "SIGN-OFF REQUIRED";
            AlertMessage = "This assessment requires an electronic sign-off by an Authorised User on AssessNET, in order for it to be deemed complete.";
        }
        else if(NextReviewDate < DateTime.Now && !ReviewNotRequired)
        {

            double overDueBy = Math.Floor((DateTime.Now - NextReviewDate).TotalDays);

            HasAlert = true;
            AlertTitle = "REVIEW REQUIRED";
            AlertMessage = "This assessment has been identified as being OVERDUE for review by " + overDueBy + " day(s).";
        }
    }


    public bool HasInternalReference()
    {
        if (InternalRef == null)
        {
            return false;
        }
        else if(InternalRef.Length == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public List<PersonsAffected.PersonsAffectedType> ReturnPersonsAffectedList()
    {
        return PersonsAffectedInstance.ReturnPersonsAffectedList();
    }


    public List<Hazards.IndividualHazard> ReturnIndividualHazardList()
    {
        return HazardInstance.ReturnIndividualHazardList();
    }


    // Persons affected class stores main information, and can also return a list of PA types for multi use
    public class PersonsAffected
    {

        private RiskAssessment _currentRA;
        private string _corpCode;
        private string _recordReference;

        public PersonsAffected (RiskAssessment currentRA)
        {

            _currentRA = currentRA;
            _corpCode = _currentRA._corpCode;
            _recordReference = currentRA._recordReference;
            
            
        }



        // Equally, this could be made to return a datatable... but for now this works (more efficient)
        public List<PersonsAffectedType> ReturnPersonsAffectedList()
        {

            List<PersonsAffectedType> personAffectedList = new List<PersonsAffectedType>();

            if (_currentRA.IsInitiated)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_RA.dbo.GeneratePersonsAffected", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corpCode", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordReference", _recordReference));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        while (objrs.Read())
                        {
                            string personsTitle = Convert.ToString(objrs["col_persons"]);
                            int personsNumber = Convert.ToInt32(objrs["col_persons_amount"]);
                            ;
                            personAffectedList.Add(new PersonsAffectedType(personsTitle, personsNumber));
                        }

                    }


                    objrs.Close();
                }
            }
            else
            {
                throw new NotSupportedException("The Risk Assessment object must be initialised before using this method.");
            }



            return personAffectedList;
        }


        public class PersonsAffectedType
        {
            public string personsTitle;
            public int personsNumber;            

            public PersonsAffectedType(string title, int number)
            {
                personsTitle = title;
                personsNumber = number;
            }
        }

    }


    public class Hazards
    {
        private RiskAssessment _currentRA;
        private string _corpCode;
        private string _recordReference;

        public Hazards(RiskAssessment currentRA)
        {

            _currentRA = currentRA;
            _corpCode = _currentRA._corpCode;
            _recordReference = currentRA._recordReference;


        }

        // Return a list of all of the hazards.
        public List<IndividualHazard> ReturnIndividualHazardList()
        {

            List<IndividualHazard> HazardList = new List<IndividualHazard>();

            if (_currentRA.IsInitiated)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_RA.dbo.sp_tasks_list", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@ra_reference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@haz_group", ""));
                    cmd.Parameters.Add(new SqlParameter("@group_ref", "0"));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        while (objrs.Read())
                        {

                            string title = Convert.ToString(objrs["hazard_type"]);
                            string description = Convert.ToString(objrs["hazard_desc"]);
                            int hazardID = Convert.ToInt32(objrs["hazard_ref"]);
                            string measures = Convert.ToString(objrs["recordhazcontrols"]);
                            string potentialRisk = Convert.ToString(objrs["icon_value2"]);
                            string currentRisk = Convert.ToString(objrs["icon_value"]);

                            HazardList.Add(new IndividualHazard(this, title, description, hazardID, measures, potentialRisk, currentRisk));
                        }

                    }


                    objrs.Close();
                }
            }
            else
            {
                throw new NotSupportedException("The Risk Assessment object must be initialised before using this method.");
            }



            return HazardList;
        }





        public class IndividualHazard
        {
            private string _corpCode;
            private string _recordReference;


            public string Type;
            public string Description;
            public int HazardID;
            public string MeasuresInPlace;
            public string PotentialRisk;
            public string CurrentRisk;


            public RemedialActions Actions;
            private Hazards _currentHazards;
            // Actions class
            // Attachments Class



            public IndividualHazard(Hazards currentHazards, string type, string description, int hazardID, string measures, string potentialRisk, string currentRisk)
            {
                _currentHazards = currentHazards;
                _corpCode = _currentHazards._corpCode;
                _recordReference = _currentHazards._recordReference;

                Type = type;
                Description = description;
                HazardID = hazardID;
                MeasuresInPlace = measures;
                PotentialRisk = potentialRisk;
                CurrentRisk = currentRisk;

                Actions = new RemedialActions(this);
            }


            // Actions can be manipulated differently for different modules. So RA has its own class that handles its actions, but uses a generic class which holds the details.
            public class RemedialActions
            {
                private string _corpCode;
                private string _recordReference;
                private IndividualHazard _currentHazard;

                public List<Action> ListOfActions;

                // Return all remedial actions
                public RemedialActions(IndividualHazard currentHazard)
                {
                    _currentHazard = currentHazard;
                    _corpCode = _currentHazard._corpCode;
                    _recordReference = _currentHazard._recordReference;

                    ListOfActions = GenerateActionList();
                }


                // Return a list of all the current actions for the hazard
                public List<Action> GenerateActionList()
                {

                    List<Action> ActionList = new List<Action>();

                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {

                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_RA.dbo.GenerateActionsFromHazard", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corpCode", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordReference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@hazardRef", _currentHazard.HazardID));

                        objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {
                            while (objrs.Read())
                            {

                                int status = Convert.ToInt32(objrs["task_status"]);
                                string assignee = Convert.ToString(objrs["task_account"]);
                                DateTime dueDate = Convert.ToDateTime(objrs["task_due_date"]);
                                string actionDetails = Convert.ToString(objrs["action_text"]);


                                ActionList.Add(new Action(status, assignee, dueDate, actionDetails, _corpCode));
                            }

                        }


                        objrs.Close();
                    }

                    return ActionList;
                }
                



            }


        }


    }


   


}


public class Action
{
    // Raw Data
    public string Assignee;
    public DateTime DueDate;


    // "Real" Data
    public string AssigneeText;
    public string ActionDetails;


    // Status Information
    public Status ActionStatus;


    public string CorpCode;




    // Constuct via data already there
    public Action(int status, string assignee, DateTime dueDate, string actionDetails, string corpCode)
    {
        Assignee = assignee;
        DueDate = dueDate;
        ActionDetails = actionDetails;
        CorpCode = corpCode;

        // Status is its own class, which allows us to expose various poperties that are useful.
        ActionStatus = new Status(status);
        AssigneeText = ASNETNSP.UserControls.GetUser(assignee, CorpCode);
    }




    public class Status
    {
        public string StatusText;
        public int StatusValue;
        public RgbColor BorderColour;
        public RgbColor BackgroundColour;
        public RgbColor FontColour;


        public Status(int status)
        {
            StatusValue = status;
            InitiateStatus();
        }


        // Assign the status its complete, active and pending text (with associated colours)
        private void InitiateStatus()
        {

            switch (StatusValue)
            {

                case 0:
                    StatusText = "Complete";
                    BackgroundColour = new RgbColor(126, 183, 43);
                    BorderColour = new RgbColor(97, 150, 30);
                    FontColour = new RgbColor(255, 255, 255);
                    break;
                case 1:
                    StatusText = "Active";
                    BackgroundColour = new RgbColor(245, 161, 82);
                    BorderColour = new RgbColor(222, 72, 6);
                    FontColour = new RgbColor(255, 255, 255);
                    break;
                case 2:
                    StatusText = "Pending";
                    BackgroundColour = new RgbColor(246, 84, 84);
                    BorderColour = new RgbColor(192, 12, 0);
                    FontColour = new RgbColor(255, 255, 255);
                    break;
                default:
                    StatusText = "Unknown";
                    break;
            }
        }

    }

    
}

public class PDFStyles
{
    public class BorderType
    {
        public static Border ThinRed = new Border(1, BorderStyle.Single, new RgbColor(255, 0, 0));
        public static Border ThinBlack = new Border(1, BorderStyle.Single, new RgbColor(0, 0, 0));
        public static Border MediumLightRed = new Border(2, BorderStyle.Single, Colours.AlertBorder_Red);
    }

    public class TextType
    {
        public static void HeaderTitle(Block currentBlock)
        {
            currentBlock.GraphicProperties.FillColor = RgbColors.Black;

            currentBlock.HorizontalAlignment = HorizontalAlignment.Center;
            currentBlock.TextProperties.Font = FontsRepository.Helvetica;
            currentBlock.TextProperties.FontSize = 30;
        }

        public static void HeaderInfo(Block currentBlock)
        {
            currentBlock.GraphicProperties.FillColor = Colours.Grey;

            currentBlock.HorizontalAlignment = HorizontalAlignment.Center;
            currentBlock.TextProperties.Font = FontsRepository.Helvetica;
            currentBlock.TextProperties.FontSize = 20;
        }

        public static void HeaderSmall(Block currentBlock)
        {
            currentBlock.GraphicProperties.FillColor = Colours.Grey;

            currentBlock.HorizontalAlignment = HorizontalAlignment.Center;
            currentBlock.TextProperties.Font = FontsRepository.Helvetica;
            currentBlock.TextProperties.FontSize = 12;
        }

        public static void AlertTitle(Block currentBlock)
        {
            currentBlock.GraphicProperties.FillColor = Colours.AlertFont_Red;

            currentBlock.HorizontalAlignment = HorizontalAlignment.Left;
            currentBlock.TextProperties.Font = FontsRepository.HelveticaBold;
            currentBlock.TextProperties.FontSize = 20;
        }

        public static void AlertText(Block currentBlock)
        {
            currentBlock.GraphicProperties.FillColor = Colours.AlertFont_Red;

            currentBlock.HorizontalAlignment = HorizontalAlignment.Left;
            currentBlock.TextProperties.Font = FontsRepository.Helvetica;
            currentBlock.TextProperties.FontSize = 13;
        }

        public static void Standard(Block currentBlock)
        {
            currentBlock.TextProperties.FontSize = 13;
        }

        public static void StatusText(Block currentBlock)
        {
            currentBlock.TextProperties.FontSize = 18;
            currentBlock.HorizontalAlignment = HorizontalAlignment.Center;
            currentBlock.GraphicProperties.FillColor = RgbColors.White;
        }
    }

    public class TableType
    {
        public static void GenericTable(Table table)
        {
            table.DefaultCellProperties.Borders = new TableCellBorders(BorderType.ThinBlack, BorderType.ThinBlack, BorderType.ThinBlack, BorderType.ThinBlack); ;
            table.DefaultCellProperties.Padding = new Thickness(10);
        }

        public static void AlertRed_Table(Table table)
        {
            table.DefaultCellProperties.Borders = new TableCellBorders(BorderType.MediumLightRed, BorderType.MediumLightRed, BorderType.MediumLightRed, BorderType.MediumLightRed); ;
            table.DefaultCellProperties.Padding = new Thickness(10);
        }
    }

    public class Colours
    {
        public static RgbColor Grey = new RgbColor(128, 128, 128);
        public static RgbColor LightGrey = new RgbColor(230, 230, 230);
        public static RgbColor HeaderYellow = new RgbColor(220, 220, 220);
        public static RgbColor LighterGrey = new RgbColor(250, 250, 250);



        public static RgbColor AlertBackground_Red = new RgbColor(242, 222, 222);
        public static RgbColor AlertFont_Red = new RgbColor(169, 68, 66);
        public static RgbColor AlertBorder_Red = new RgbColor(255, 247, 237);
    }

}
