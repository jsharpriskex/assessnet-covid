﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative;
using ASNETNSP;
using System.Web.UI.HtmlControls;
using System.Text;

public partial class riskAssessment : System.Web.UI.Page
{
    
    public string var_corp_code;
    public string accLang;

    public TranslationServices ts;

    protected void Page_PreInit(object sender, EventArgs e)
    {

        var_corp_code = Request["c"];
        accLang = Request["l"] ?? Context.Session["YOUR_LANGUAGE"].ToString();

        ts = new TranslationServices(var_corp_code, accLang, "riskassessment");


        string saveToDisk = Request["save"] ?? "";

        signoffTableContent.Text = printSignoffTable();

        if (saveToDisk == "Y")
        {
            SaveFileToDisk();
        }

    }


    protected void SaveFileToDisk()
    {



        string idOfJob = Request["guid"];


            //string printOrient = pdfPrintOrient.SelectedValue;
            string signOff = "NA";
            string showPersonsAffected = "N";
            //string viewGallery = pdfViewGallery.Checked.ToString();
            //string viewNotes = pdfViewNotes.Checked.ToString();

            // Create a HTML to PDF converter object with default settings
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();

            // Set license key received after purchase to use the converter in licensed mode
            // Leave it not set to use the converter in demo mode
            htmlToPdfConverter.LicenseKey = Application["PDF_LICENCE_14_4"].ToString(); // "fvDh8eDx4fHg4P/h8eLg/+Dj/+jo6Og=";

            // Install a handler where to change the header and footer in first page
            htmlToPdfConverter.PrepareRenderPdfPageEvent += new PrepareRenderPdfPageDelegate(htmlToPdfConverter_PrepareRenderPdfPageEvent);


            // Set PDF page orientation to Landscape
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = Winnovative.PdfPageOrientation.Landscape;
            htmlToPdfConverter.HtmlViewerWidth = 1400;

            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 10;

            // Stop the listed page elements from breaking inside
            htmlToPdfConverter.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "div" };


            string pdfFileAfter = HttpContext.Current.Server.MapPath(".") +
                                  @"\exportdata\" + idOfJob +
                                  "-alldocuments.pdf";

      

            htmlToPdfConverter.PdfDocumentOptions.AddEndDocument(pdfFileAfter,true, true, true);


            // The buffer to receive the generated PDF document
            byte[] outPdfBuffer = null;

            string url = Application["CONFIG_PROVIDER"] + "/core/system/export/riskassessment/merge/paging.aspx?signoff=20&c=" + var_corp_code + "&l=en-gb&guid=" + idOfJob;

            // Convert the HTML page given by an URL to a PDF document in a memory buffer
            outPdfBuffer = htmlToPdfConverter.ConvertUrl(url);





            DirectoryInfo di = Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/");
            File.WriteAllBytes(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/" + idOfJob + "-PAGING.pdf", outPdfBuffer);

        

       
        Response.End();

    }

    protected void Page_Load(object sender, EventArgs e)
    {
     

        


    }

   


    private static string baseUrl = "";
    private static string htmlInsert = "<head>" +
                                    "<link href='version3.2/layout/upgrade/css/newlayout.css?444' rel='stylesheet' />" +
                                    "<link href='core/modules/riskassessment/riskAssessment.css' rel='stylesheet' />" +
                                    "<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'>" +
                                        "<style>" +
                                        "body { background-color: #ffffff; padding-left:20px; padding-right:20px; width:100%} " +
                                        ".page_title { text-align: center; padding-left: 110px; padding-right: 20px; padding-top: 5px; } " +
                                        ".corporate_logo { width: 250px; float: left; } " +
                                        ".top_remedial { width: 180px; float: right; } " +
                                    "</style>" +
                                "</head>";

    private void htmlToPdfConverter_PrepareRenderPdfPageEvent(PrepareRenderPdfPageParams eventParams)
    {
        if (eventParams.PageNumber == 1)
        {
            // Change the header and footer in first page with an alternative header and footer

            // The PDF page being rendered
            PdfPage pdfPage = eventParams.Page;

            // Add a custom header of 1 points in height to this page
            pdfPage.AddHeaderTemplate(1);
            // Draw the page header elements
            // Add a custom footer of 80 points in height to this page
            // The default document footer will be replaced in this page
            //pdfPage.AddFooterTemplate(80);
            // Draw the page header elements
            //DrawAlternativePageFooter(pdfPage.Footer);
        }
    }

   
    
    public string printSignoffTable()
    {
        string signoffTable;
        int rows;

        signoffTable = "<thead>" +
                       "<th width='30%'>Name</ th>" +
                       "<th width='30%'>Job Title</ th>" +
                       "<th width='30%'>Signature</ th>" +
                       "<th>Date</ th>" +
                       "</ thead>" +
                       "<tbody>";

        if (Request["signoff"] != "NA")
        {
            rows = Convert.ToInt32(Request["signoff"]);

            for (int i = 1; i <= rows; i++)
            {
                signoffTable += "<tr>" +
                           "<td>&nbsp;</ td>" +
                           "<td>&nbsp;</ td>" +
                           "<td>&nbsp;</ td>" +
                           "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</ td>" +
                           "</ tr>";
            }
        }
        signoffTable += "</tbody>";

        return signoffTable;
    }



}




