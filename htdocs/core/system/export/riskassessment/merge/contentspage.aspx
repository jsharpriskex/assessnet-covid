﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contentspage.aspx.cs" Inherits="riskAssessment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><% =ts.GetResource("h1_ra") %></title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="/version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="/core/modules/riskassessment/riskAssessment.css?123" rel="stylesheet" />
    
    <style>
        th {
            background-color: #F0F0F0 !important;
        }
    </style>

</head>
<body runat="server" id="pageBody">

    <form id="pageform" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <div class="container-fluid">
            <div class="row" style="padding-top: 75px">

                <div class="col-xs-12">
                    <div class="row">

                        <div class="col-xs-5">
                            <div class="page_title">
                                <h1>Assessment Merged Report
                                    <br />
                                    <small>Data valid as of <%=DateTime.Now.ToString() %></small>
                                </h1>
                            </div>
                        </div>
                        <div class="col-xs-5 col-xs-offset-2">
                            <div class="alert alert-info" role="alert" style="width:100%">
                                The assessments shown below are contained within this merged assessment report. Please ensure that all risk assessments are kept within this overall report.
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-xs-12">


                           

                            <div class="row">
                                <div class="col-xs-12">

                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center" width="50px">Date
                                                    </th>
                                                    <th width="100px" class="text-center">Reference
                                                    </th>
                                                    <th>Title / Description
                                                    </th>
                                                    <th width="125px" class="text-center">Rating
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <asp:Repeater runat="server" ID="contentsRiskAssessment_Repeater">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="text-center"><%# Eval("recorddate") %></td>
                                                            <td class="text-center"><%# Eval("recordreference") %></td>
                                                            <td><strong><%# Eval("raTitle") %></strong><br />
                                                                <%# Eval("raDesc") %></td>
                                                            <td class="text-center" style="font-size: 15px;"><%# returnOverallRisk(Eval("recordreference")) %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>


                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- Main Col End -->
                </div>
                <!-- Main Row End -->
            </div>
        </div>



    </form>


</body>
</html>
