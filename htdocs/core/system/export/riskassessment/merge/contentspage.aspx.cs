﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Winnovative;
using ASNETNSP;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Data;

public partial class riskAssessment : System.Web.UI.Page
{
    
    public string var_corp_code;
    public string accLang;
    public string idOfJob;

    public TranslationServices ts;

    protected void Page_PreInit(object sender, EventArgs e)
    {

        var_corp_code = Request["c"];
        accLang = Request["l"] ?? Context.Session["YOUR_LANGUAGE"].ToString();

        Context.Session["YOUR_LANGUAGE"] = accLang;

        ts = new TranslationServices(var_corp_code, accLang, "riskassessment");

        idOfJob = Request["guid"];

        GenerateRiskTable();

        string saveToDisk = Request["save"] ?? "";

        if (saveToDisk == "Y")
        {
            SaveFileToDisk();
        }

    }


    protected void SaveFileToDisk()
    {




        try
        {


            //string printOrient = pdfPrintOrient.SelectedValue;
            string signOff = "NA";
            string showPersonsAffected = "N";
            //string viewGallery = pdfViewGallery.Checked.ToString();
            //string viewNotes = pdfViewNotes.Checked.ToString();

            // Create a HTML to PDF converter object with default settings
            HtmlToPdfConverter htmlToPdfConverter = new HtmlToPdfConverter();

            // Set license key received after purchase to use the converter in licensed mode
            // Leave it not set to use the converter in demo mode
            htmlToPdfConverter.LicenseKey = Application["PDF_LICENCE_14_4"].ToString(); // "fvDh8eDx4fHg4P/h8eLg/+Dj/+jo6Og=";

            // Install a handler where to change the header and footer in first page
            htmlToPdfConverter.PrepareRenderPdfPageEvent += new PrepareRenderPdfPageDelegate(htmlToPdfConverter_PrepareRenderPdfPageEvent);


            // Set PDF page orientation to Landscape
            htmlToPdfConverter.PdfDocumentOptions.PdfPageOrientation = Winnovative.PdfPageOrientation.Landscape;
            htmlToPdfConverter.HtmlViewerWidth = 1400;

            htmlToPdfConverter.PdfDocumentOptions.LeftMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.RightMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.TopMargin = 10;
            htmlToPdfConverter.PdfDocumentOptions.BottomMargin = 10;

            // Stop the listed page elements from breaking inside
            htmlToPdfConverter.PdfDocumentOptions.AvoidHtmlElementsBreakSelectors = new string[] { "tr" };

            // The buffer to receive the generated PDF document
            byte[] outPdfBuffer = null;

            string url = Application["CONFIG_PROVIDER"] + "/core/system/export/riskassessment/merge/contentspage.aspx?c=" + var_corp_code + "&l=en-gb&guid=" + idOfJob;

            // Convert the HTML page given by an URL to a PDF document in a memory buffer
            outPdfBuffer = htmlToPdfConverter.ConvertUrl(url);





            DirectoryInfo di = Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/");
            File.WriteAllBytes(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/" + idOfJob + "-contents.pdf", outPdfBuffer);

            Response.Write("200");
            Response.End();
        }
        catch (Exception e)
        {
            Response.Write("500");
            Response.End();

            throw;
        }

       

    }

    protected void Page_Load(object sender, EventArgs e)
    {
     

        


    }

   


    private static string baseUrl = "";
    private static string htmlInsert = "<head>" +
                                    "<link href='version3.2/layout/upgrade/css/newlayout.css?444' rel='stylesheet' />" +
                                    "<link href='core/modules/riskassessment/riskAssessment.css' rel='stylesheet' />" +
                                    "<link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet'>" +
                                        "<style>" +
                                        "body { background-color: #ffffff; padding-left:20px; padding-right:20px; width:100%} " +
                                        ".page_title { text-align: center; padding-left: 110px; padding-right: 20px; padding-top: 5px; } " +
                                        ".corporate_logo { width: 250px; float: left; } " +
                                        ".top_remedial { width: 180px; float: right; } " +
                                    "</style>" +
                                "</head>";

    private void htmlToPdfConverter_PrepareRenderPdfPageEvent(PrepareRenderPdfPageParams eventParams)
    {
        if (eventParams.PageNumber == 1)
        {
            // Change the header and footer in first page with an alternative header and footer

            // The PDF page being rendered
            PdfPage pdfPage = eventParams.Page;

            // Add a custom header of 1 points in height to this page
            pdfPage.AddHeaderTemplate(1);
            // Draw the page header elements
            // Add a custom footer of 80 points in height to this page
            // The default document footer will be replaced in this page
            //pdfPage.AddFooterTemplate(80);
            // Draw the page header elements
            //DrawAlternativePageFooter(pdfPage.Footer);
        }
    }

    public void GenerateRiskTable()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_Global.dbo.Export_RiskAssessmentMerge_ContentsPage", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corpCode", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@exportID", idOfJob));

            contentsRiskAssessment_Repeater.DataSource = cmd.ExecuteReader();
            contentsRiskAssessment_Repeater.DataBind();
        }

    }

    public string returnOverallRisk(object recordRef)
    {

        string overallRiskLevel = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {
                string sql_text = "SELECT TOP 1 matrix.value from " + Application["DBTABLE_RAMOD_HAZARDS"] + " as rahaz " +
                            "left join " + Application["GLOBAL_RISK_MATRIX_OVERALL"] + " as matrix " +
                            "on rahaz.corp_code = matrix.corp_code " +
                            "and rahaz.recordhazseverity = matrix.severity_value " +
                            "and rahaz.recordhazlikelihood = matrix.likelihood_value  " +
                            "and matrix.display_language = '" + accLang + "' " + 
                         "WHERE rahaz.corp_code = '" + var_corp_code + "' and  rahaz.recordreference = '" + recordRef.ToString() + "' order by cast(rahaz.recordhazscore as int) DESC";

                SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();


                if (objrs.HasRows)
                {
                    objrs.Read();
                    overallRiskLevel = objrs["value"].ToString();
                }
                else
                {
                    overallRiskLevel = "";
                    //error coding later
                }

                sqlComm.Dispose();
                objrs.Close();


                //TestLabelHaz.Text = sql_text;

            }
            catch (Exception)
            {
                throw;
            }
        }

        return SharedComponants.RiskLevelVisual(overallRiskLevel, var_corp_code);

    }
   
    



}




