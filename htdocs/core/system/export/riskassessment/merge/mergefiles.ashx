﻿<%@ WebHandler Language="C#" Class="MergeFiles" %>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using ASNETNSP;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf.Streaming;
using Telerik.Windows.Documents.Fixed.Model;
using Telerik.Windows.Documents.Fixed.Model.ColorSpaces;
using Telerik.Windows.Documents.Fixed.Model.Editing;

public class MergeFiles : IHttpHandler, IRequiresSessionState {



    public void ProcessRequest(HttpContext context)
    {


        context.Server.ScriptTimeout = 3600;

        PDFDownloader pdf = new PDFDownloader();
        pdf.InitiatePDFDownloads();
    }


    public bool IsReusable {
        get {
            return false;
        }
    }



}


class PDFDownloader
{
    private List<ExportJob> ExportJobList = new List<ExportJob>();


    public void InitiatePDFDownloads()
    {

        // Check to see whats outstanding and create as individual jobs
        // These come in via the DB. Remember this runs once a minute.

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand(@"Module_GLOBAL.dbo.[Export_ReturnItemsToBeProcessed]", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;

            objrs = cmd.ExecuteReader();

            while (objrs.Read())
            {

                ExportJobList.Add(new ExportJob
                {
                    Id = Guid.Parse(Convert.ToString(objrs["id"])),
                    GeneratedDateTime = Convert.ToDateTime(objrs["GeneratedDateTime"]),
                    CorpCode = Convert.ToString(objrs["corpCode"]),
                    AccRef = Convert.ToString(objrs["accRef"]),
                    EmailAddress = Convert.ToString(objrs["EmailAddress"]),
                    ExportType = Convert.ToString(objrs["ExportType"]),
                    Processed = Convert.ToBoolean(objrs["Processed"])
                });

            }


            objrs.Close();
        }


        foreach (ExportJob job in ExportJobList)
        {
            job.StartProcessing();
        }



    }


}


class ExportJob
{

    public Guid Id;
    public DateTime GeneratedDateTime;
    public string CorpCode;
    public string AccRef;
    public string EmailAddress;
    public string ExportType;
    public bool Processed;
    private List<ExportData> _exportDataList = new List<ExportData>();


    public override string ToString()
    {
        return "ID: " + Id + " | Corp Code: " + CorpCode;
    }


    public void StartProcessing()
    {
        // Initiate the references in use.
        InitiateExportData();


        // We know what data is in use now. 
        // If the type of report is RAMERGE (which all are at the moment), lets process accordingly!

        switch (ExportType)
        {
            case "RAMERGE":
                RiskAssessmentMerge();
                break;

            default:
                Console.WriteLine("No recognised export type used.");
                break;
        }


    }



    private void InitiateExportData()
    {
        // This grabs all data associated to the individual export job and populates into a list for future use.
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand(@"Module_GLOBAL.dbo.[Export_ReturnDataToBeProcessed]", dbConn);
            cmd.Parameters.Add(new SqlParameter("@exportID", Id));

            cmd.CommandType = CommandType.StoredProcedure;

            objrs = cmd.ExecuteReader();

            while (objrs.Read())
            {

                _exportDataList.Add(new ExportData
                {
                    Id = Guid.Parse(Convert.ToString(objrs["id"])),
                    RecordReference = Convert.ToString(objrs["recordreference"]),
                    Processed = Convert.ToBoolean(objrs["Processed"])
                });

            }


            objrs.Close();
        }

    }





    private void RiskAssessmentMerge()
    {



        // Fun Begins.

        // Firstly First lets remove everything incase it failed.
        //Delete Files
        DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/");

        foreach(var documentName in d.GetFiles("*.pdf"))
        {
            // Open each of the files
            if (documentName.Name.Contains(Id.ToString()))
            {
                documentName.Delete();
            }

        }


        //HttpContext.Current.Response.Write(HttpContext.Current.Application["CONFIG_PROVIDER"] + "/core/system/export/riskassessment/merge/contentspage.aspx" +
        //                                   "?l=en-gb" +
        //                                   "&guid=" + Id +
        //                                   "&output=print" +
        //                                   "&a=" + AccRef +
        //                                   "&l=en-gb" +
        //                                   "&save=Y" +
        //                                   "&c=" + CorpCode);
        //HttpContext.Current.Response.End();


        // First we make the contents page
        using (var myClient = new WebClient())
        {

            try
            {

                var httpClient = new HttpClient();


                var myTask = httpClient.GetStringAsync(HttpContext.Current.Application["CONFIG_PROVIDER"] + "/core/system/export/riskassessment/merge/contentspage.aspx" +
                                                       "?guid=" + Id +
                                                       "&output=print" +
                                                       "&a=" + AccRef +
                                                       "&l=en-gb" +
                                                       "&save=Y" +
                                                       "&c=" + CorpCode);
                var myString = myTask.GetAwaiter().GetResult();



                HttpContext.Current.Response.Write(myString);

            }
            catch (Exception e)
            {
                throw;
            }

        }

     

        // We need to save all the files on disk, this is done on a webform page so we need to call it.
        foreach (ExportData exportData in _exportDataList)
        {

            //https://local.assessweb.co.uk/core/modules/riskassessment/ra_summary.aspx?ref=4278RA&c=222999&guid=bdb47e29-d0f6-44f6-8f9d-689d1d4bacf7&output=print&a=edRRGKjWDYclKkW&l=en-gb

            bool errorOccurred = false;
            ProcessIndividualItem(exportData.Id, Id, false, true);


            // Submit request to make the page.
            using (var myClient = new WebClient())
            {

                try
                {
                    //    using (Stream response = myClient.OpenRead(
                    //        "https://local.assessweb.co.uk/core/system/export/riskassessment/merge/merge.aspx" +
                    //        "?ref=" + exportData.RecordReference +
                    //        "&ccode=" + CorpCode))
                    //    {
                    //        StreamReader reader = new StreamReader(response);
                    //        Console.WriteLine(reader.ReadToEnd());
                    //    }

                    var httpClient = new HttpClient();
                    //var myTask = httpClient.GetStringAsync("https://local.assessweb.co.uk/core/system/export/riskassessment/merge/merge.aspx" +
                    //                                       "?ref=" + exportData.RecordReference +
                    //                                       "&ccode=" + CorpCode +
                    //                                       "&guid=" + Id);

                    var myTask = httpClient.GetStringAsync(HttpContext.Current.Application["CONFIG_PROVIDER"] + "/core/modules/riskassessment/ra_summary.aspx" +
                                                           "?ref=" + exportData.RecordReference +
                                                           "&c=" + CorpCode +
                                                           "&guid=" + Id +
                                                           "&output=print" +
                                                           "&a=" + AccRef +
                                                           "&l=en-gb" +
                                                           "&save=Y" +
                                                           "&pa=N" +
                                                           "&signOff=NA" +
                                                           "&ramatrix=N");
                    var myString = myTask.GetAwaiter().GetResult();


                    HttpContext.Current.Response.Write(myString);

                }
                catch (Exception e)
                {

                    throw;

                    HttpContext.Current.Response.Write("Error");

                    errorOccurred = true;
                }

            }



            if (!errorOccurred)
            {
                ProcessIndividualItem(exportData.Id, Id, true, false);
            }


        }




        // Now that we have processed all files, lets check they are all there:
        bool allFilesExist = CheckAllFilesExist();



        // Append Sign Off Page

        using (var myClient = new WebClient())
        {

            try
            {

                var httpClient = new HttpClient();


                var myTask = httpClient.GetStringAsync(HttpContext.Current.Application["CONFIG_PROVIDER"] + "/core/system/export/riskassessment/merge/signoffstrips.aspx" +
                                                       "?signoff=20" +
                                                       "&l=en-gb" +
                                                       "&guid=" + Id +
                                                       "&output=print" +
                                                       "&a=" + AccRef +
                                                       "&l=en-gb" +
                                                       "&save=Y" +
                                                       "&pa=N" +
                                                       "&ramatrix=N");
                var myString = myTask.GetAwaiter().GetResult();

                HttpContext.Current.Response.Write(myString);

            }
            catch (Exception e)
            {
                throw;
            }

        }


        // Lets merge them together.
        if (allFilesExist)
        {





            FileStream documentToMerge = File.Open(HttpContext.Current.Server.MapPath("~") +
                                                        "/core/system/export/riskassessment/merge/exportdata/" + Id +
                                                        "-alldocuments.pdf", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);



            using (PdfStreamWriter fileWriter = new PdfStreamWriter(documentToMerge))
            {
                // Iterate through the files var would like to merge
                foreach(var documentName in d.GetFiles("*.pdf",SearchOption.AllDirectories).OrderBy(t => t.LastWriteTime).ToList())
                {
                    // Open each of the files
                    if (documentName.Name.Contains(Id.ToString()) && !documentName.Name.Contains("alldocuments"))
                    {
                        using (PdfFileSource fileToMerge = new PdfFileSource(File.OpenRead(documentName.FullName)))
                        {
                            // Iterate through the pages of the current document
                            foreach(PdfPageSource pageToMerge in fileToMerge.Pages)
                            {



                                // Append the current page to the fileWriter, which holds the result FileStream
                                fileWriter.WritePage(pageToMerge);
                            }

                        }
                    }

                }


            }


            documentToMerge.Dispose();



            //  Email to client
            EmailClient();





            // Item has been finally processed.
            ProcessFullItem(Id, true, false);


            //Delete Files
            foreach(var documentName in d.GetFiles("*.pdf"))
            {
                // Open each of the files
                if (documentName.Name.Contains(Id.ToString()))
                {
                    documentName.Delete();
                }

            }

        }





    }


    private void EmailClient()
    {



        ASNETEmail mail = new ASNETEmail(CorpCode, "riskAssessmentMerge");

        //Add Attachment here
        mail.AddAttachment("mergedDocument", HttpContext.Current.Server.MapPath("~") +
                                             "/core/system/export/riskassessment/merge/exportdata/" + Id +
                                             "-alldocuments.pdf");


        mail.EmailTo = EmailAddress;
        mail.SendMail();


    }


    private void ProcessIndividualItem(Guid itemID, Guid exportID, bool isProcessed, bool isProcessing)
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand(@"Module_GLOBAL.dbo.[Export_ProcessIndividual]", dbConn);
            cmd.Parameters.Add(new SqlParameter("@itemID", itemID));
            cmd.Parameters.Add(new SqlParameter("@exportID", exportID));
            cmd.Parameters.Add(new SqlParameter("@processed", isProcessed));
            cmd.Parameters.Add(new SqlParameter("@beingProcessed", isProcessing));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }

    private void ProcessFullItem(Guid exportID, bool isProcessed, bool isProcessing)
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand(@"Module_GLOBAL.dbo.[Export_ProcessFull]", dbConn);
            cmd.Parameters.Add(new SqlParameter("@exportID", exportID));
            cmd.Parameters.Add(new SqlParameter("@processed", isProcessed));
            cmd.Parameters.Add(new SqlParameter("@beingProcessed", isProcessing));
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
        }
    }


    private bool CheckAllFilesExist()
    {

        //Count number of expected items
        int expectedCountOfItems = _exportDataList.Count + 1;

        //Grab count of actual number of items
        DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("~") + "/core/system/export/riskassessment/merge/exportdata/");

        int actualCountOfItems = Directory.GetFiles(d.FullName, "*" + Id + "*", SearchOption.TopDirectoryOnly).Length;

        //HttpContext.Current.Response.Write("Exp: " + (expectedCountOfItems) + "<br />");
        //HttpContext.Current.Response.Write("Actual: " + (actualCountOfItems) + "<br />");
        //HttpContext.Current.Response.End();


        return expectedCountOfItems == actualCountOfItems;
    }


    private static RadFixedPage GenerateForegroundTextContent(string text)
    {
        Block block = new Block();
        block.BackgroundColor = new RgbColor(0, 0, 0, 0);
        block.GraphicProperties.FillColor = new RgbColor(255, 0, 0);
        block.TextProperties.FontSize = 120;
        block.InsertText(text);
        System.Windows.Size horizontalTextSize = block.Measure();
        double rotatedTextSquareSize = (horizontalTextSize.Width + horizontalTextSize.Height) / Math.Sqrt(2);

        RadFixedPage foregroundContentOwner = new RadFixedPage();
        foregroundContentOwner.Size = new System.Windows.Size(rotatedTextSquareSize, rotatedTextSquareSize);
        FixedContentEditor foregroundEditor = new FixedContentEditor(foregroundContentOwner);
        foregroundEditor.Position.Translate(horizontalTextSize.Height / Math.Sqrt(2), 0);
        foregroundEditor.Position.Rotate(45);
        foregroundEditor.DrawBlock(block);

        return foregroundContentOwner;
    }


}


class ExportData
{
    public Guid Id;
    public string RecordReference;
    public bool Processed;
}