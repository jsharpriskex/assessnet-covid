﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="merge.cs" Inherits="MergePDF" %>
<%@ Import Namespace="ASNETNSP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Risk Assessment Merge PDF</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="/core/framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="/core/framework/datepicker/js/moment.js"></script>
    <script src="/core/framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>

    <script src="/core/framework/jscookie/jquery.cookie.js"></script>

    <link href="/version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />


</head>
<body>
    <form id="pageform" runat="server">

        <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="false">
        </asp:ScriptManager>


       

    
        <asp:UpdatePanel ID="MainContainer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>


                <asp:Label ID="sqlTextHelperTop" runat="server"></asp:Label>
                <div class="container-fluid">
                <div class="row">

                   <div class="subpageNavListMargin" style="padding-top: 19px;">
                     


                </div>

                    <div class="mainpageNavList">


                        <div class="row">
                            <div class="col-xs-7">
                                <div class="page_title">
                                    <h1>Risk Assessment PDF Test</h1>
                                    <span></span>
                                </div>
                            </div>
                            <div class="col-xs-5">
                                <div class="page_title">
                                    <!-- some menu items to go here -->
                                </div>
                            </div>
                        </div>


                        <!-- Template Selection -->

                        <a href="#templateSelection" id="templateSelectionAnchor" class="templateSelectionAnchor"></a>
                        <div runat="server" id="section_0" class="row">
                            <asp:UpdatePanel ID="templateSelector" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="col-xs-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h3 class="panel-title" style="margin-left: 0 !important;"><i class="fa fa-bars" aria-hidden="true"></i>PDF Generation</h3>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row" id="selectTemplateDiv" runat="server">
                                                    <div class="col-xs-2">
                                                        <div class="row">
                                                            <div class="col-xs-12 text-center">
                                                                <div class="form-group text-center">
                                                                    
             
                                                                        <asp:Button runat="server" ID="generatePDF" OnClick="generatePDF_Click" Text="Generate PDF" CssClass="btn btn-lg btn-success" style="width: 250px;"/>
                                                 

                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                             
                                                    
                                            </div>
                                            <div class="panel-footer">
                                                <div class="row" id="selectTemplateSaveDiv" runat="server">
                                                    <div class="col-xs-12 text-center">
                                                        
                                                    </div>
                                                </div>
                                                <div class="row" id="selectTemplateButtonDiv" runat="server">
                                                    <div class="col-xs-12 text-center">
                                                        

                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <asp:Label ID="Label1" runat="server"></asp:Label>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>




                    </div>
                    <!-- Main Col End -->
                </div>
                <!-- Main Row End -->
            </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>

    <script type="text/javascript">

        function save_section1() {
            $('html, body').animate({
                scrollTop: $(".questionAudit").offset().top
            }, 1500);
        }

        function save_section2() {
            $('html, body').animate({
                scrollTop: $(".additionalObservations").offset().top
            }, 1500);
        }
        function BindControlEvents() {
            $(function () {
                $('#auditDate').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm',
                    sideBySide: true,
                    widgetPositioning: { horizontal: 'right', vertical: 'auto' }
                });
            });

            $(function () {
                $('#due_date').datetimepicker({
                    format: 'DD/MM/YYYY',
                    sideBySide: true,
                    widgetPositioning: { horizontal: 'right', vertical: 'auto' }
                });
            });

            $(function () {
                $('.datepick').each(function () {
                    $(this).datetimepicker({
                        format: 'DD/MM/YYYY',
                        sideBySide: true,
                        widgetPositioning: { horizontal: 'right', vertical: 'auto' }
                    }
                );
             });
            });

            

            $(function () {
                $('#review_date').datetimepicker({
                    format: 'DD/MM/YYYY',
                    sideBySide: true,
                    widgetPositioning: { horizontal: 'right', vertical: 'auto' }
                });
            });
        }
        $(document).ready(function () {
            BindControlEvents();

            $("#accordion").on('shown.bs.collapse', function () {
                var active = $("#accordion .in").attr('id');
                $.cookie('activeAccordionGroup', active);
                //  alert(active);
            });
            $("#accordion").on('hidden.bs.collapse', function () {
                $.removeCookie('activeAccordionGroup');
            });
            var last = $.cookie('activeAccordionGroup');
            if (last != null) {
                //remove default collapse settings
                $("#accordion .panel-collapse").removeClass('in');
                //show the account_last visible group
                $("#" + last).addClass("in");
            }

           

        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


        $(window).load(function () {
            $('#templateModal').modal('show');
        });

    </script>


</body>
</html>
