﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using System.Web.UI.HtmlControls;
using Microsoft.VisualBasic.FileIO;

public partial class importUsers : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {


        try
        {
            //Console.WriteLine(dirName + file);

            //@"C:\apps\AssessNET_UserList_2018-05-18.csv"
            DataTable aData = GetDataTabletFromCSVFile(dirName + file);

            InsertDataIntoSQLServerUsingSQLBulkCopy(aData);

            if (File.Exists(dirName + file))
            {
                File.Delete(dirName + file);
            }
        }
        catch (Exception ex)
        {
            string exMsg = ex.Message.ToString();
            exMsg = exMsg.Replace("'", "`");

            MailMessage mail = new MailMessage("notifications@assessnet.co.uk", "m.green@riskex.co.uk");
            SmtpClient client = new SmtpClient();
            NetworkCredential basicCredential = new NetworkCredential("notifications@riskexltd.onmicrosoft.com", "rX46812@");

            client.Port = 25;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = basicCredential;
            client.Host = "riskex-co-uk.mail.protection.outlook.com";
            mail.Subject = "Ainscough User Import Failed";
            mail.Body = "Importing Ainscough users failed:<br /><br />" + exMsg;
            client.Send(mail);

            client.Dispose();
            mail.Dispose();
        }

    }



    private static DataTable GetDataTabletFromCSVFile(string csv_file_path)
    {
        DataTable csvData = new DataTable();

        using (TextFieldParser csvReader = new TextFieldParser(csv_file_path))
        {
            csvReader.SetDelimiters(new string[] { "," });
            csvReader.HasFieldsEnclosedInQuotes = false;
            string[] colFields = csvReader.ReadFields();
            foreach (string column in colFields)
            {
                DataColumn datecolumn = new DataColumn(column);
                datecolumn.AllowDBNull = true;
                csvData.Columns.Add(datecolumn);
            }
            while (!csvReader.EndOfData)
            {
                string[] fieldData = csvReader.ReadFields();
                //Making empty value as null
                for (int i = 0; i < fieldData.Length; i++)
                {
                    if (fieldData[i] == "")
                    {
                        fieldData[i] = null;
                    }
                }
                csvData.Rows.Add(fieldData);
            }
        }

        return csvData;
    }

    static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData)
    {

        string ConnString = "Data source=192.168.0.50\\ASNETDBSTOR01,1433;Database=MODULE_API;User ID=sqlasnet;Password=DEV10SAS";
        //string ConnString = "Data source=159.253.56.161\\ASNETDBSTOR01,1433;Database=MODULE_API;User ID=sqlasnet;Password=DEV10SAS";
        //string ConnString = "Data source=192.168.0.15\\ASNETDBSTORBUILD;Database=MODULE_API;User ID=sqlasnet;Password=DEV10SAS";

        using (SqlConnection dbConnection = new SqlConnection(ConnString))
        {
            dbConnection.Open();

            string sql = "DELETE FROM Module_API.dbo.ACH_Imported_Users";
            using (SqlCommand cmd = new SqlCommand(sql, dbConnection))
            {
                cmd.ExecuteNonQuery();
            }

            using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
            {
                s.DestinationTableName = "[Module_API].[dbo].[ACH_Imported_Users]";
                foreach (var column in csvFileData.Columns)
                    s.ColumnMappings.Add(column.ToString(), column.ToString());
                s.WriteToServer(csvFileData);
            }

            SqlCommand cmd1 = new SqlCommand("Module_API.dbo.usp_process_ACH_user_import", dbConnection);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.CommandTimeout = 300;
            cmd1.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

}






