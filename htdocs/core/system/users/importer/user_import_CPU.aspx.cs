﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using System.IO;
using System.Web.UI.HtmlControls;
using Winnovative.ExcelLib;

public partial class uploader : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {



    }



    protected void convertExcel()
    {
        // load the uploaded Excel file to a temporary workbook

        // create the Excel stream
        string dataFilePath = System.IO.Path.Combine(Server.MapPath("~"), @"Data\awemployees.xls");
        System.IO.FileStream sourceXlsDataStream = new System.IO.FileStream(dataFilePath, System.IO.FileMode.Open);

        ExcelWorkbook tempWorkbook = new ExcelWorkbook(sourceXlsDataStream);
        ExcelWorksheet tempWorksheet = tempWorkbook.Worksheets[0];

        if (tempWorksheet["A1"].Text != "POSFT ID")
        {
            //delete the first row - it isn't a header
            tempWorksheet.DeleteRow(1);
        }

        if (tempWorksheet["A1"].Text != "POSFT ID" && tempWorksheet["B1"].Text == "Forename" && tempWorksheet["C1"].Text == "Surname" && tempWorksheet["D1"].Text == "Team" && tempWorksheet["E1"].Text == "Posn Title" && tempWorksheet["F1"].Text == "Location Descr" && tempWorksheet["G1"].Text == "Line Manager" && tempWorksheet["H1"].Text == "Email ID")
        {
            //remove the headers, we don't need them
            tempWorksheet.DeleteRow(1);

            //create a datatable to be imported into database table
            DataTable importedData = tempWorksheet.GetDataTable(tempWorksheet.UsedRange, true);

            using (SqlConnection dbConnection = ConnectionManager.GetDatabaseConnection())
            {
                dbConnection.Open();

                string sql = "DELETE FROM Module_API.dbo.CPU_Imported_Users";
                using (SqlCommand cmd = new SqlCommand(sql, dbConnection))
                {
                    cmd.ExecuteNonQuery();
                }

                using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                {
                    s.DestinationTableName = "[Module_API].[dbo].[CPU_Imported_Users]";
                    foreach (var column in importedData.Columns)
                        s.ColumnMappings.Add(column.ToString(), column.ToString());
                    s.WriteToServer(importedData);
                }

                SqlCommand cmd1 = new SqlCommand("Module_API.dbo.usp_process_CPU_user_import", dbConnection);
                cmd1.CommandType = CommandType.StoredProcedure;
                cmd1.CommandTimeout = 300;
                cmd1.ExecuteNonQuery();

                dbConnection.Close();
            }

        }
        else
        {
            //header columns don't match what's expected so throw back to the client
            //remove blank headers
            //make sure headers match pre-arranged format
        }

        //close the temporary workbook
        tempWorkbook.Close();
        //close the data stream
        sourceXlsDataStream.Close();

    }
}







