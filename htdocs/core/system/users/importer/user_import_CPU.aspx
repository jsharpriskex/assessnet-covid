﻿<%@ Page Language="C#" AutoEventWireup="false" CodeFile="user_import_CPU.aspx.cs" Inherits="uploader" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Computershare User File Import</title>
    
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>

        /* Side notes for calling out things
         -------------------------------------------------- */

        /* Base styles (regardless of theme) */
         .bs-callout {
         margin: 20px 0;
         padding: 15px 30px 15px 15px;
         border-left: 3px solid #eee;
         }
         .bs-callout h1,
         .bs-callout h2,
         .bs-callout h3,
         .bs-callout h4,
         .bs-callout h5,
         .bs-callout h6 {
         margin-top: 0;
         }

        .bs-callout-danger h1,
         .bs-callout-danger h2,
         .bs-callout-danger h3,
         .bs-callout-danger h4,
         .bs-callout-danger h5,
         .bs-callout-danger h6 {
         color: #B94A48;
         }

        .bs-callout-warning h1,
         .bs-callout-warning h2,
         .bs-callout-warning h3,
         .bs-callout-warning h4,
         .bs-callout-warning h5,
         .bs-callout-warning h6 {
         color: #C09853;
         }

        .bs-callout-info h1,
         .bs-callout-info h2,
         .bs-callout-info h3,
         .bs-callout-info h4,
         .bs-callout-info h5,
         .bs-callout-info h6 {
         color: #3A87AD;
         }

        .bs-callout-success h1,
         .bs-callout-success h2,
         .bs-callout-success h3,
         .bs-callout-success h4,
         .bs-callout-success h5,
         .bs-callout-success h6 {
         color: #3C763D;
         }

        .bs-callout p:last-child {
         margin-bottom: 0;
         }

        .bs-callout code,
         .bs-callout .highlight {
         background-color: #fff;
         }

        /* Themes for different contexts */
         .bs-callout-danger {
         background-color: #fcf2f2;
         border-color: #B94A48;
         }
         .bs-callout-warning {
         background-color: #fefbed;
         border-color: #C09853;
         }
         .bs-callout-info {
         background-color: #f0f7fd;
         border-color: #3A87AD;
         }
         .bs-callout-success {
         background-color: #dff0d8;
         border-color: #3C763D;
         }

    </style>


</head>
<body>


<div class="container">

    <form id="uploaderPage" runat="server">


        <h2><span class="glyphicon glyphicon glyphicon-list"></span>Computershare Excel Import Control</h2>


    <div class="panel panel-default">
    <div class="panel-heading">
    <h3 class="panel-title">&nbsp;</h3>
    </div>
        <div class="panel-body">

        <div class="row">
        <div class="col-md-6">

            <br />

            <div class="well well-sm">

            <p>Select employee file to be imported</p>
            <p class="text-center"><asp:FileUpload ID="FileDetails" Runat="server" style="width:100%;"/></p>
            <p class="text-center"><asp:Button ID="Upload_but" class="btn btn-primary" Runat="server" Text="Upload Spreadsheet" /></p>
            </div>

            <asp:Label ID="msg" Runat="server" style="display:inline-block; width: 100%;" />

        </div>
        <div class="col-md-6">

        <div class="bs-callout bs-callout-info">
        <h4>Please Note:</h4>
        <p>
            <ul>
                <li>Maximum File Size 10mb.</li>
                <li>Only compatiable with excel files in the agreed format.</li>
                <li>No Offensive or Illegal material allowed.</li>
            </ul>
        </p>
            <p><span class="glyphicon glyphicon-tags"></span> &nbsp;Account details and key identifying information are logged for security reasons.</p>
        </div>

        </div>
        </div>

        </div>


    </div>

        <br />






     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>


    </form>

</div>

<script>
    $("#upload_but").click(function () {
        $("#msg").html("<span class='glyphicon glyphicon-refresh'></span> <strong>Notice</strong> Your file is uploading, please wait....");
        $("#msg").addClass("alert alert-info");
    });
</script>

</body>
</html>
