﻿
Imports System.Web
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Partial Class uploader
    Inherits System.Web.UI.Page


    ' Feb 2014 - Version 1 - Upload Agent for AssessNET
    ' 

    Dim var_str_corp_code As String
    Dim var_str_account_id As String
    Dim FileName As String
    Dim var_maxFileSize As Integer = 10000000
    Dim var_str_filestatus As Integer = 0 ' 0 - No File, 1 - uploaded image, 2 - uploaded file, 3 - too big, 4 - invalid file type, 5 - unknown error

    Dim ImageName As String
    Public var_str_viewtype As String = HttpContext.Current.Request.QueryString("viewtype")
    Public main_reference As String = HttpContext.Current.Request.QueryString("main_reference")
    Public secondary_reference As String = HttpContext.Current.Request.QueryString("secondary_reference")
    Public image_ref As String = HttpContext.Current.Request.QueryString("image_ref")
    Public var_up_frm_cmd As String = HttpContext.Current.Request.QueryString("cmd")
    Public var_id As String = HttpContext.Current.Request.QueryString("id")
    Public var_control As String = HttpContext.Current.Request.QueryString("control")

    ' Need something to list the filetypes in that we can check
    Dim FileTypes As New Dictionary(Of String, Integer) ' 1 as image 2 as file

    ' ON LOAD EVENTS
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        ' Set the vars we need for this page and all the functions
        If Session.Contents.Item("corp_code") = Nothing Then
            Session.Contents.Item("corp_code") = HttpContext.Current.Request.QueryString("ccode")
        End If

        If Session.Contents.Item("account_id") = Nothing Then
            Session.Contents.Item("account_id") = HttpContext.Current.Request.QueryString("acc")
        End If

        var_str_corp_code = Session.Contents.Item("corp_code")
        var_str_account_id = Session.Contents.Item("account_id")

        FileTypes.Add(".jpg", 1)
        FileTypes.Add(".jpeg", 1)
        FileTypes.Add(".gif", 1)
        FileTypes.Add(".png", 1)
        FileTypes.Add(".pdf", 2)
        FileTypes.Add(".dwg", 2)
        FileTypes.Add(".doc", 2)
        FileTypes.Add(".docx", 2)
        FileTypes.Add(".xls", 2)
        FileTypes.Add(".xlsx", 2)
        FileTypes.Add(".ppt", 2)
        FileTypes.Add(".rtf", 2)
        FileTypes.Add(".txt", 2)
        FileTypes.Add(".bmp", 1)
        FileTypes.Add(".tif", 1)

        If var_up_frm_cmd = "detach" Then
            Call delfile(var_id)
        End If

        If var_up_frm_cmd = "cnv" Then
            Call ConvertExcel()
        End If

    End Sub

    ' Object Handlers
    Private Sub UploadFile_click(ByVal Sender As Object, e As EventArgs) Handles Upload_But.Click

        ' Check we even have something to upload
        If FileDetails.HasFile = True Then

            'Try

            Dim var_str_UploadFileName As String = FileDetails.FileName

            ' Reset error to unknown so we pickup something if the file does not go up
            var_str_filestatus = 5

            ' Ok no point going anywhere if the file is too big, lets check 10mb current limit
            If FileDetails.PostedFile.ContentLength <= var_maxFileSize Then

                If CheckFileValid(var_str_UploadFileName) = True Then
                    ' Image or File
                    If CheckFileType(var_str_UploadFileName) = True Then
                        UploadFile()
                    Else
                        UploadImage()
                    End If

                    AddtoDB()

                Else

                    ' Invalid Filetype
                    var_str_filestatus = 4

                End If

            Else
                var_str_filestatus = 3 ' too big
            End If

            'Catch ex As Exception

            'Response.Write(ex)

            ' Any other errors, we need to know
            'var_str_filestatus = 5

            'End Try

        End If

        ' output Error Messages
        Select Case var_str_filestatus

            Case 0
                msg.Text = "<span class='glyphicon glyphicon-floppy-remove'></span> <strong>Notice</strong> You have not selected a file to upload"
                msg.CssClass = "alert alert-info"
            Case 1
                msg.Text = "<span class='glyphicon glyphicon-floppy-saved'></span> <strong>Success</strong> Your image was uploaded"
                msg.CssClass = "alert alert-success"
            Case 2
                msg.Text = "<span class='glyphicon glyphicon-floppy-saved'></span> <strong>Success</strong> Your file was uploaded"
                msg.CssClass = "alert alert-success"
            Case 3
                msg.Text = "<span class='glyphicon glyphicon-floppy-disk'></span> <strong>Notice</strong> Your file exceeds our 10mb maximum"
                msg.CssClass = "alert alert-info"
            Case 4
                msg.Text = "<span class='glyphicon glyphicon-floppy-remove'></span> <strong>Error</strong> We do not accept this file type"
                msg.CssClass = "alert alert-danger"
            Case 5
                msg.Text = "<span class='glyphicon glyphicon-floppy-remove'></span> <strong>Error</strong> An unknown error has occured, contact support"
                msg.CssClass = "alert alert-danger"

        End Select

    End Sub


    ' Functions
    Function CheckFileType(FileName As String) As Boolean

        Dim ext As String = Path.GetExtension(FileName)

        ' Check if its part of the image group or not
        If FileTypes.Item(LCase(ext)) = 2 Then
            Return True
        Else
            Return False
        End If

    End Function

    Function CheckFileValid(FileName As String) As Boolean

        Dim ext As String = Path.GetExtension(FileName)

        If FileTypes.ContainsKey(LCase(ext)) = True Then
            Return True
        Else
            Return False
        End If

    End Function


    ' Subs

    Public Sub AddtoDB()

        Dim ConnStr As String = ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString
        Dim sqlConn As New SqlConnection(ConnStr)
        Dim sqlcomm As New SqlCommand
        Dim objrs As SqlDataReader
        sqlConn.Open()

        Dim var_datetime As DateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")
        Dim var_fl_size As Integer = FileDetails.PostedFile.ContentLength
        Dim var_fl_ext As String = Path.GetExtension(FileDetails.FileName)
        Dim var_hostip As String = HttpContext.Current.Request.ServerVariables("REMOTE_ADDR")
        Dim var_vpath As String = var_str_corp_code & "/" & var_str_account_id & "/"
        Dim var_path As String = Application("DOC_UPLOAD_PATH") & var_str_corp_code & "\excel-imports\" & var_str_account_id & "\"
        Dim var_fl_name As String = Replace(FileName, Path.GetExtension(FileDetails.FileName), "")
        Dim var_fl_originalname As String = FileDetails.FileName
        Dim var_fl_title As String = FileTitle.Text

        Dim sql_query As String
        sql_query = "INSERT INTO " & Application("DBTABLE_GLOBAL_FILES") & _
                    " (UP_STOR_MOD,UP_STOR_EXT,UP_STOR_path,UP_STOR_vpath,UP_STOR_NAME,accountid,corp_code,hostname,up_stor_datetime,up_stor_size,up_stor_chmod,up_stor_dirid,up_stor_title,up_stor_master) " & _
                    "values('XLS-IMPORT','" & var_fl_ext & "','" & var_path & "','" & var_vpath & "','" & var_fl_name & "','" & var_str_account_id & "','" & var_str_corp_code & "','" & var_hostip & "','" & var_datetime & "','" & var_fl_size & "','','','" & var_fl_title & "','" & var_fl_originalname & "') "

        sqlcomm = New SqlCommand(sql_query, sqlConn)
        sqlComm.ExecuteNonQuery()

        sql_query = "SELECT id FROM " & Application("DBTABLE_GLOBAL_FILES") & " " & _
                    "WHERE up_stor_name='" & var_fl_name & "' and corp_code='" & var_str_corp_code & "' and accountid='" & var_str_account_id & "'"

        sqlcomm = New SqlCommand(sql_query, sqlConn)
        objrs = sqlcomm.ExecuteReader()

        If objrs.HasRows Then
            objrs.Read()
            msg.Text = "test"
            sql_query = "INSERT INTO " & Application("DBTABLE_GLOBAL_ATTACH") & " " & _
                        "(corp_code,recordreference,recordreference2,up_file_id,up_attach_by,up_attach_datetime) " & _
                        "values('" & var_str_corp_code & "','" & main_reference & "','" & secondary_reference & "','" & objrs("id") & "','" & var_str_account_id & "','" & var_datetime & "')"

            Dim sqlComm2 As New SqlCommand(sql_query, sqlConn)
            sqlComm2.ExecuteNonQuery()
            sqlComm2 = Nothing

        Else

        End If

        sqlConn.Close()
        sqlComm = Nothing


    End Sub

    Public Sub delfile(ByVal id As String)

        Dim ConnStr As String = ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString
        Dim sqlConn As New SqlConnection(ConnStr)
        Dim sqlComm As New SqlCommand

        sqlConn.Open()

        Dim sql_query As String

        sql_query = "DELETE FROM " & Application("DBTABLE_GLOBAL_FILES") & " WHERE corp_code='" & var_str_corp_code & "' and id='" & id & "'"


        sqlComm = New SqlCommand(sql_query, sqlConn)
        sqlComm.ExecuteNonQuery()



        sqlConn.Close()
        sqlComm = Nothing


    End Sub

    Public Sub ShowFiles()


        Dim ConnStr As String = ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString
        Dim sqlConn As New SqlConnection(ConnStr)
        sqlConn.Open()

        Dim sql_query As String
        sql_query = "SELECT files.id, files.up_stor_vpath, files.up_stor_mod, files.up_stor_name, files.up_stor_datetime, files.up_stor_desc, files.up_stor_size, files.up_stor_ext, files.UP_STOR_TITLE, files.up_stor_master, audit.audit_ref, audit.template_id, audit.complete " & _
                                             "FROM " & Application("DBTABLE_GLOBAL_FILES") & " as files " & _
                                             "LEFT JOIN " & Application("DBTABLE_SA_AUDIT_ENTRIES") & " as audit " & _
                                             "ON files.corp_code = audit.corp_code " & _
                                             "AND replace(files.UP_STOR_NAME,'-','_') = audit.excel_import_ref " & _
                                             "WHERE files.corp_code = '" & var_str_corp_code & "' " & _
                                             "AND files.ACCOUNTID = '" & var_str_account_id & "' " & _
                                             "AND (files.UP_STOR_MOD = 'XLS-IMPORT' " & _
                                             "OR files.UP_STOR_MOD = 'XLS-IMPORT-DONE') " & _
                                             "ORDER BY files.up_stor_datetime DESC"

        Dim sqlComm As New SqlCommand(sql_query, sqlConn)
        Dim objrs As SqlDataReader = sqlComm.ExecuteReader()

        If objrs.HasRows Then

            Response.Write("<table class='table table-bordered'>" & _
                             "<tr>" & _
                             "<th class='active'>Date</th>" & _
                             "<th class='active'>Title</th>" & _
                             "<th class='active'>Status</th>" & _
                             "<th class='active' width='150px'>&nbsp;</th>" & _
                             "</tr>")

            While objrs.Read()

                Dim var_db_col_id As Integer = objrs("id")
                Dim var_db_col_ext As String = objrs("UP_STOR_EXT")
                Dim var_db_col_path As String = objrs("UP_STOR_VPATH")
                Dim var_db_col_name As String = objrs("UP_STOR_NAME")
                Dim var_db_col_title As String = objrs("UP_STOR_TITLE")
                Dim var_db_col_orgfilename As String = objrs("UP_STOR_MASTER")
                Dim var_db_col_date As DateTime = objrs("UP_STOR_DATETIME")
                Dim var_db_col_complete As Boolean = False
                Dim var_db_col_auditref As String = 0
                Dim var_db_col_templateid As String = 0

                If IsDBNull(objrs("audit_ref")) Then

                Else
                    var_db_col_auditref = objrs("audit_ref")
                    var_db_col_templateid = objrs("template_id")
                    var_db_col_complete = objrs("Complete")
                End If

                Dim var_status As String = "Uploaded"
                Dim var_css As String = "warning"
                Dim show_option As Boolean = True
                If IsDBNull(var_db_col_title) Or Len(var_db_col_title) = 0 Then var_db_col_title = var_db_col_name


                If objrs("up_stor_mod") = "XLS-IMPORT-DONE" Then

                    var_css = "success"
                    show_option = False

                    If var_db_col_complete = True Then
                        var_status = "Audit Completed"
                    Else
                        var_status = "Converted to Audit"
                    End If

                End If


                Response.Write("<tr class='" & var_css & "'>" & _
                "<td>" & var_db_col_date & "</td>" & _
                "<td class='text-uppercase'>" & var_db_col_title & "<br/><span class='label label-warning'>" & var_db_col_orgfilename & "</span><br /><span class='label label-info'>" & var_db_col_name & "</span></td>" & _
                "<td class='text-center'>" & var_status & "</td>" & _
                "<td>")

                If show_option = True Then

                    Response.Write("<div class='btn-group'>" &
                      "<button type='button' class='btn btn-danger'>Options</button>" &
                      "<button type='button' class='btn btn-danger dropdown-toggle' data-toggle='dropdown'>" &
                        "<span class='caret'></span>" &
                        "<span class='sr-only'>Toggle Dropdown</span>" &
                      "</button>" &
                      "<ul class='dropdown-menu' role='menu'>" &
                        "<li><a href='Excel_Import.aspx?cmd=cnv&id=" & var_db_col_id & "&ccode=" & var_str_corp_code & "&acc_ref=" & var_str_account_id & "'>Convert to Audit</a></li>" &
                        "<li class='divider'></li>" &
                        "<li><a href='Excel_Import.aspx?cmd=detach&id=" & var_db_col_id & "&ccode=" & var_str_corp_code & "&acc_ref=" & var_str_account_id & "'>Delete File</a></li>" &
                      "</ul>" &
                    "</div>")

                Else

                    If var_db_col_complete = True Then
                        Response.Write("<a href='saudit_report.asp?reference=" & var_db_col_auditref & "&temp_ref=" & var_db_col_templateid & "' class='btn btn-primary btn active' role='button'>Audit Report</a>")

                    Else
                        Response.Write("<a href='saudit_commands.asp?cmd=edit&ref=" & var_db_col_auditref & "&temp_ref=" & var_db_col_templateid & "' class='btn btn-success btn active' role='button'>Finalise Audit</a>")
                    End If

                End If

                Response.Write("</td>" & _
                "</tr>")


            End While

            Response.Write("</table>")

        Else

            Response.Write("<div class='alert alert-info' role='alert'><strong>Notice</strong> There are currently no uploaded excel files that require to be processed.</div>")


        End If

        sqlConn.Close()

    End Sub

    Public Sub CreateNewAudit(DBTableRef As String)


        ' Ok so we have the file converted into a table within SA, now we need to make an audit file and populate with the information as an incomplete.
        ' Will capture these in seperate trys so we can isolate an issue easier.
        Try

            Dim dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
            Dim str_audit_ref As String = 0
            Dim str_template_ref As String = 0
            Dim str_weighting_scheme As String = 0
            Dim str_ExcelDBTableName As String = "Module_sa.dbo.EXL_" & var_str_corp_code & "_" & DBTableRef
            Dim tableExists As Boolean = False

            Using dbConn
                dbConn.Open()

                ' Grab a new reference
                Dim sql_text As String = "SELECT max(audit_ref) as audit_ref from " & Application("DBTABLE_SA_AUDIT_ENTRIES")
                Dim sqlComm As New SqlCommand(sql_text, dbConn)
                Dim objrs As SqlDataReader = sqlComm.ExecuteReader()

                If objrs.HasRows Then
                    objrs.Read()
                    str_audit_ref = CInt(objrs("audit_ref")) + 1
                End If

                ' Ok we need the template Ref which we can get from the imported data
                sql_text = "SELECT distinct templateref from " & str_ExcelDBTableName & " where templateref is not null"
                Dim sqlComm3 As New SqlCommand(sql_text, dbConn)
                Dim objrs3 As SqlDataReader = sqlComm3.ExecuteReader()

                If objrs3.HasRows Then
                    objrs3.Read()
                    str_template_ref = objrs3("templateref")
                    tableExists = True
                Else

                End If


                If tableExists = True Then


                    ' Now run an update on the awnser set so we convert them to what the system identifies with
                    ' Check the template and see if we need to worry about custom scoring system
                    sql_text = "SELECT weighting_scheme FROM " & Application("DBTABLE_SA_TEMP_INFO") & " where corp_code = '" & var_str_corp_code & "' and template_id = '" & str_template_ref & "'"
                    Dim sqlComm7 As New SqlCommand(sql_text, dbConn)
                    Dim objrs7 As SqlDataReader = sqlComm7.ExecuteReader()

                    If objrs7.HasRows Then
                        objrs7.Read()
                        str_weighting_scheme = objrs7("weighting_scheme")
                    End If

                    If Len(str_weighting_scheme) > 1 Then ' we know its a custom weighting 

                        Dim sqlComm8 As New SqlCommand
                        sqlComm8.Connection = dbConn

                        Dim sqlComm10 As New SqlCommand
                        sqlComm10.Connection = dbConn

                        sqlComm8.CommandText = "select * from " & Application("DBTABLE_SA_V2_TEMPLATE_WEIGHTING") & " where corp_code = '" & var_str_corp_code & "' and weight_reference = '" & str_weighting_scheme & "'"
                        Dim objrs8 As SqlDataReader = sqlComm8.ExecuteReader()

                        If objrs8.HasRows Then
                            While objrs8.Read()

                                sqlComm10.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='" & objrs8("weighting") & "', score=" & objrs8("weighting") & " WHERE question='" & objrs8("weight_title") & "' and len(templateRef) > 0 "
                                sqlComm10.ExecuteNonQuery()


                            End While
                        End If

                        Dim sqlComm11 As New SqlCommand
                        sqlComm11.Connection = dbConn

                        sqlComm11.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='n', score=0 WHERE question is null and len(templateRef) > 0 "
                        sqlComm11.ExecuteNonQuery()

                        sqlComm11.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='x', score=0 WHERE question='Not Applicable' and len(templateRef) > 0 "
                        sqlComm11.ExecuteNonQuery()


                    Else ' We go by the default set, so update accordingly.

                        Dim sqlComm8 As New SqlCommand
                        sqlComm8.Connection = dbConn

                        sqlComm8.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='0', score=0 WHERE question='Does not meet requirements' and len(templateRef) > 0 "
                        sqlComm8.ExecuteNonQuery()

                        sqlComm8.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='1', score=1 WHERE question='Attention Required' and len(templateRef) > 0 "
                        sqlComm8.ExecuteNonQuery()

                        sqlComm8.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='2', score=2 WHERE question='Meets Requirements' and len(templateRef) > 0 "
                        sqlComm8.ExecuteNonQuery()

                        sqlComm8.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='n', score=0 WHERE question is null and len(templateRef) > 0 "
                        sqlComm8.ExecuteNonQuery()

                        sqlComm8.CommandText = "UPDATE " & str_ExcelDBTableName & " SET question='x', score=0 WHERE question='Not Applicable' and len(templateRef) > 0 "
                        sqlComm8.ExecuteNonQuery()

                    End If

                    ' Now run a final clean up before we move on
                    Dim sqlComm9 As New SqlCommand
                    sqlComm9.Connection = dbConn

                    'sqlComm9.CommandText = "DELETE " & str_ExcelDBTableName & " WHERE templateRef is null "
                    'sqlComm9.ExecuteNonQuery()





                    '
                    ' At this point the excel is imported, restructured and manipluated to be compatiable with a straight forward injection into the tables.
                    '


                    ' Add in a new record so we have something to work with
                    sql_text = "INSERT INTO " & Application("DBTABLE_SA_AUDIT_ENTRIES") &
                               "(excel_import_ref,excel_import_datetime,gendatetime, audit_ref, owner, revision, corp_code, template_id, audit_datetime, last_modified_by, Last_modified,rec_bu,rec_bl,rec_bd,complete,count_notapp,count_meet_req,count_doesnt_meet,count_action_req,comments,audit_title,audit_desc,int_ref,version,dist_list_sent) " &
                               "VALUES('" & DBTableRef & "','" & DateTime.Now & "','" & DateTime.Now & "','" & str_audit_ref & "','" & var_str_account_id & "','0','" & var_str_corp_code & "','" & str_template_ref & "','" & DateTime.Now & "','" & Session("account_id") & "','" & DateTime.Now & "','-1','0','0','0','0','0','0','0','','Imported Excel Audit - TITLE','This audit was imported on " & DateTime.Now & " and is awaiting completion','','2','0')"
                    Dim sqlComm2 As New SqlCommand(sql_text, dbConn)
                    sqlComm2.ExecuteNonQuery()


                    'Now we need to add in the audit data from the excel
                    sql_text = "INSERT INTO " & Application("DBTABLE_SA_AUDIT_DATA") &
                               "(gendatetime,audit_ref,corp_code,question_id,status,comments,ref,section_reference,action_later,score) " &
                               "SELECT '" & DateTime.Now & "','" & str_audit_ref & "','" & var_str_corp_code & "',questionRef,question,comment,questionRef + '_" & str_audit_ref & "',SectionRef,'0',score FROM " & str_ExcelDBTableName & " Where TemplateRef is not null"
                    sqlComm2 = New SqlCommand(sql_text, dbConn)
                    sqlComm2.ExecuteNonQuery()


                    sqlComm9.CommandText = "DROP TABLE " & str_ExcelDBTableName & " "
                    sqlComm9.ExecuteNonQuery()

                End If





            End Using

        Catch ex As Exception
            ' There is an issue and its with the creation of the audit entry
            Response.Write(ex)

        End Try

        Response.Redirect("Excel_Import.aspx?&ccode=" & var_str_corp_code & "&acc_ref=" & var_str_account_id, True)

    End Sub

    Public Sub ConvertExcel()

        Try

            Dim var_import_table As String
            Dim var_excel_file As String

            Dim dbConn As New SqlConnection(ConfigurationManager.ConnectionStrings("MyDbConn").ConnectionString)
            Dim str_audit_ref As String = 0


            Using dbConn
                dbConn.Open()

                Dim sql_query As String
                sql_query = "SELECT id, up_stor_path, up_stor_name,up_stor_datetime,up_stor_desc,up_stor_size,up_stor_ext, UP_STOR_TITLE, up_stor_master " &
                                                     "FROM " & Application("DBTABLE_GLOBAL_FILES") & " " &
                                                     "WHERE corp_code = '" & var_str_corp_code & "' " &
                                                     "AND ACCOUNTID = '" & var_str_account_id & "' " &
                                                     "AND UP_STOR_MOD = 'XLS-IMPORT' " &
                                                     "AND ID = " & var_id & " " &
                                                     "ORDER BY up_stor_datetime ASC"



                Dim sqlComm As New SqlCommand(sql_query, dbConn)
                Dim objrs As SqlDataReader = sqlComm.ExecuteReader()

                If objrs.HasRows Then

                    While objrs.Read()

                        var_import_table = Replace(Replace(Replace(objrs("UP_STOR_NAME"), ".xls", ""), ".xlsx", ""), "-", "_")
                        var_excel_file = objrs("up_stor_path") & objrs("UP_STOR_NAME") & objrs("up_stor_ext")

                    End While
                Else

                End If


                sql_query = "SELECT * INTO Module_sa.dbo.EXL_" & var_str_corp_code & "_" & var_import_table & " FROM OPENROWSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=" & var_excel_file & "','select * from [Excel Export from AssessNET$] where SectionRef is not null')"

                Response.Write(sql_query)

                Dim sqlComm2 As New SqlCommand(sql_query, dbConn)
                sqlComm2.ExecuteNonQuery()

                sql_query = "UPDATE " & Application("DBTABLE_GLOBAL_FILES") & " SET UP_STOR_MOD = 'XLS-IMPORT-DONE' " & _
                                                     "WHERE corp_code = '" & var_str_corp_code & "' " & _
                                                     "AND ACCOUNTID = '" & var_str_account_id & "' " & _
                                                     "AND UP_STOR_MOD = 'XLS-IMPORT' " & _
                                                     "AND ID = " & var_id & " "

                Dim sqlComm3 As New SqlCommand(sql_query, dbConn)
                sqlComm3.ExecuteNonQuery()

                ' Clean up the imported data into a much easier structure to deal with later (Move into store proc at some point)
                sql_query = "ALTER TABLE Module_sa.dbo.EXL_" & var_str_corp_code & "_" & var_import_table & " ADD comment varchar(1000)"
                Dim sqlComm4 As New SqlCommand(sql_query, dbConn)
                sqlComm4.ExecuteNonQuery()


                ' Find questions that have comments and re-format into the same row
                sql_query = "SELECT questionref, question from Module_sa.dbo.EXL_" & var_str_corp_code & "_" & var_import_table & " WHERE templateref is null" ' and len(question) > 0 "
                Dim sqlComm5 As New SqlCommand(sql_query, dbConn)
                Dim objrs5 As SqlDataReader = sqlComm5.ExecuteReader()

                Dim sqlComm6 As New SqlCommand
                sqlComm6.Connection = dbConn

                If objrs5.HasRows Then
                    While objrs5.Read()

                        Dim currentQuestion As String

                        If Not IsDBNull(objrs5("question")) Then
                            currentQuestion = objrs5("question")
                        Else
                            currentQuestion = ""
                        End If

                        If currentQuestion <> "" Then
                            sqlComm6.CommandText = "UPDATE Module_sa.dbo.EXL_" & var_str_corp_code & "_" & var_import_table & " SET comment='" & objrs5("question") & "' WHERE questionref='" & objrs5("questionref") & "' and templateref is not null "
                            sqlComm6.ExecuteNonQuery()
                        End If


                    End While
                End If

                ' Bug Fix CG - add score.

                sqlComm6.CommandText = "ALTER TABLE Module_sa.dbo.EXL_" & var_str_corp_code & "_" & var_import_table & " ADD score numeric(18, 1)"
                sqlComm6.ExecuteNonQuery()




            End Using

            ' Now conver that excel import to an audit
            CreateNewAudit(var_import_table)

        Catch ex As Exception
            Response.Write(ex)
        End Try




    End Sub

    Public Sub UploadImage()

        CreateFolder(var_str_account_id & "/thumbnails/")
        CreateFolder(var_str_account_id & "/originals/")

        Dim gu As New Guid
        gu = Guid.NewGuid

        Dim ext As String = Path.GetExtension(FileDetails.FileName)
        ImageName = gu.ToString & ext

        'Save original
        FileDetails.SaveAs(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/originals/" & ImageName)

        'Make slimline edition for general viewing
        Using fl = File.OpenRead(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/originals/" & ImageName)
            Using slimFile = File.Create(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/" & ImageName)
                ResizeImage(0, 0.1, fl, slimFile)
            End Using
        End Using

        'Make thumbnail
        Using fl = File.OpenRead(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/" & ImageName)
            Using thumbFile = File.Create(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/thumbnails/" & ImageName)
                ResizeImage(1, 0.1, fl, thumbFile)
            End Using
        End Using

        var_str_filestatus = 1


    End Sub

    Public Sub UploadFile()

        ' Create Directories or ignore if exists
        CreateFolder(var_str_account_id)


        Dim gu As New Guid
        gu = Guid.NewGuid

        Dim ext As String = Path.GetExtension(FileDetails.FileName)

        FileName = gu.ToString & ext
        'FileDetails.SaveAs(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/" & var_str_account_id & "/" & FileName)
        FileDetails.SaveAs(Application("DOC_UPLOAD_PATH") & var_str_corp_code & "/excel-imports/" & var_str_account_id & "/" & FileName)
        var_str_filestatus = 2


    End Sub

    Private Sub CreateFolder(ByVal DirName As String)

        Dim Path As String = Application("DOC_UPLOAD_PATH")
        Path = Path & var_str_corp_code & "\excel-imports\" & DirName

        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If

    End Sub


    Private Sub ResizeImage(ByVal sizetype As Integer, ByVal scaleFactor As Double, ByVal fromStream As Stream, ByVal toStream As Stream)
        Using image__1 = Image.FromStream(fromStream)

            Dim newWidth As Integer
            Dim newHeight As Integer

            If sizetype = "1" Then
                newWidth = 150
                newHeight = 150
            Else
                newWidth = 500
                newHeight = 500
            End If

            Using thumbnailBitmap = New Bitmap(newWidth, newHeight)
                Using thumbnailGraph = Graphics.FromImage(thumbnailBitmap)
                    thumbnailGraph.CompositingQuality = CompositingQuality.HighQuality
                    thumbnailGraph.SmoothingMode = SmoothingMode.HighQuality
                    thumbnailGraph.InterpolationMode = InterpolationMode.HighQualityBicubic

                    Dim imageRectangle = New Rectangle(0, 0, newWidth, newHeight)
                    thumbnailGraph.DrawImage(image__1, imageRectangle)

                    thumbnailBitmap.Save(toStream, image__1.RawFormat)
                End Using
            End Using
        End Using
    End Sub



End Class
