﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="accessDenied.aspx.cs" Inherits="accessDenied" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8">

    <title>AccessDenied</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <meta http-equiv="refresh" content="300"/>

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
        
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?555" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />

    <style>



        .hero {
            font-size: 150px;
        }

        .page-padding {
            padding-top: 40px;
        }

    </style>

</head>
<body>

    <form runat="server">

        <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <div class="container">

            <div class="row">

                <div class="col-xs-12 text-center page-padding">

                    <div class="red-colour">
                        <i class="fa fa-ban hero"></i>
                        <h2>Oops! No Entry</h2>
                        &nbsp;
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>You do not have the required permissions to access the selected page. Please contact a Global Administrator.</p>
                            <!-- <p>Your designated user manager is <strong>&lt;NAME IN HERE FROM DB&gt;</strong></p> -->
                        </div>
                        <div class="panel-footer">
                            <a class="btn btn-success" href='javascript:history.go(-1)'>Return to Previous Page</a> <a href="../navigation/homepage.asp" class="btn btn-warning">Return to Homepage</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </form>

</body>
</html>
