﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="IncidentCentreDropDown.ascx.cs" Inherits="IncidentCentreDropDown_UserControl" %>



<div class="row">
    <div class="col-xs-12">
        <div class="form-group">

            <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="Structure_UpdatePanel">
                <ContentTemplate>

                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="incidentCentreSelect_UpdatePanel">
                        <ContentTemplate>
                            
                            <label for="IncidentCentreDropDown"><asp:Literal runat="server" ID="incidentDropDownText_Literal"></asp:Literal></label>
                            
                            <telerik:RadComboBox ID="incidentCentreSelect_RadComboBox" runat="server"
                                Width="100%"
                                EnableVirtualScrolling="true"
                                DropDownWidth="900px"
                                DropDownHeight="300px"
                                Skin="Bootstrap"
                                HighlightTemplatedItems="true"
                                Filter="Contains"
                                EnableLoadOnDemand="True"
                                AppendDataBoundItems="True"
                                AllowCustomText="false"
                                Height="300"
                                ShowMoreResultsBox="true"
                                OnItemsRequested="incident_Blank_ItemsRequested"
                                OnSelectedIndexChanged="incidentCentreSelect_OnSelectedIndexChanged"
                                AutoPostBack="True">

                                <ItemTemplate>
                                    <%# Container.DataItem != null ? DataBinder.Eval(Container.DataItem, "ACCB_CODE") : DataBinder.Eval(Container, "Text") %>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:HiddenField runat="server" ID="tier2Reference_HiddenField"/>
                    <asp:HiddenField runat="server" ID="tier3Reference_HiddenField"/>
                    <asp:HiddenField runat="server" ID="tier4Reference_HiddenField"/>
                    <asp:HiddenField runat="server" ID="tier5Reference_HiddenField"/>
                    <asp:HiddenField runat="server" ID="tier6Reference_HiddenField"/>
                    <asp:HiddenField runat="server" ID="accbCode_HiddenField"/>
                    <asp:HiddenField runat="server" ID="isStructure_HiddenField"/>


                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier2_UpdatePanel">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="structureTier2_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier2_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier3_UpdatePanel">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="structureTier3_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier3_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier4_UpdatePanel">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="structureTier4_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier4_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier5_UpdatePanel">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="structureTier5_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier5_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier6_UpdatePanel">
                        <ContentTemplate>
                            <asp:DropDownList runat="server" ID="structureTier6_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier6_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </ContentTemplate>
            </asp:UpdatePanel>








        </div>
    </div>
</div>
<%--OnSelectedIndexChanged="structureTier1_SelectedIndexChanged"--%>

<!-- alert -->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Invalid Data Entry</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        Please enter valid information within the fields that have been highlighted in <span style="color: #c00c00; font-weight: bold;">red</span>.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->
