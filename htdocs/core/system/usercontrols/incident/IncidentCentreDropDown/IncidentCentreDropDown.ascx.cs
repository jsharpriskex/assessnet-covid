﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;
using ListItem = System.Web.UI.WebControls.ListItem;

public partial class IncidentCentreDropDown_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;

    public bool IsAdmin;

    public bool IsStructure;

    public Page ReferenceToCurrentPage;

    public IncidentCentreDropDown CurrentIncidentCentreDropDown;
    //public PersonDetails.Type UserType;
    public IncidentCentreDropDown.IncidentType IncidentType;
    
    public DateTime IncidentDateTime;

    public TranslationServices ts;

    public bool IsCreate = false;

    public string Tier2ID { get; private set; }
    public string Tier3ID { get; private set; }
    public string Tier4ID { get; private set; }
    public string Tier5ID { get; private set; }
    public string Tier6ID { get; private set; }


    public string Tier2Name{ get; private set; }
    public string Tier3Name{ get; private set; }
    public string Tier4Name{ get; private set; }
    public string Tier5Name{ get; private set; }
    public string Tier6Name{ get; private set; }

    public string Tier2Label{ get; private set; }
    public string Tier3Label{ get; private set; }
    public string Tier4Label{ get; private set; }
    public string Tier5Label{ get; private set; }
    public string Tier6Label{ get; private set; }


    public string IncidentStructureStartsOn;


    private string t2ID = "";
    private string t3ID = "";
    private string t4ID = "";
    private string t5ID = "";
    private string t6ID = "";

    protected void Page_Init(object sender, EventArgs e)
    {
        
        //if (ts == null)
        //{
        //    ts = new TranslationServices(_corpCode, "en-gb", "incident");
        //}
        
        //***************************
        // CHECK THE SESSION STATUS
        
        Tier2Label = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "corp_tier2", "pref") ?? "Company";
        Tier3Label = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "corp_tier3", "pref") ?? "Location";
        Tier4Label = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "corp_tier4", "pref") ?? "Department";
        Tier5Label = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "corp_tier5", "pref") ?? "Err";
        Tier6Label = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "corp_tier6", "pref") ?? "Err";
        //END OF SESSION STATUS CHECK
        //***************************


        if (!Page.IsPostBack)
        {
            accbCode_HiddenField.Value = IncidentCentre;



            structureTier2_DropDownList.Visible = false;;
            structureTier3_DropDownList.Visible = false;;
            structureTier4_DropDownList.Visible = false;;
            structureTier5_DropDownList.Visible = false;;
            structureTier6_DropDownList.Visible = false;;
            isStructure_HiddenField.Value = IsStructure.ToString();

           

        }


        incidentCentreSelect_RadComboBox.EmptyMessage = ts.GetResource("ph_incidentDropDown");

    }

    protected void Page_Load(object sender, EventArgs e)
    {



        Tier2ID = tier2Reference_HiddenField.Value;
        Tier3ID = tier3Reference_HiddenField.Value;
        Tier4ID = tier4Reference_HiddenField.Value;
        Tier5ID = tier5Reference_HiddenField.Value;
        Tier6ID = tier6Reference_HiddenField.Value;

        if (isStructure_HiddenField.Value == "True")
        {
            IncidentCentre = "";
        }
        else
        {
            if (incidentCentreSelect_RadComboBox.SelectedValue == "0")
            {
                accbCode_HiddenField.Value = "0";
                IncidentCentre = accbCode_HiddenField.Value;
            }
            else
            {
                IncidentCentre = accbCode_HiddenField.Value;
            }

            
        }

        

        if (!Page.IsPostBack)
        {

            isStructure_HiddenField.Value = IsStructure.ToString();

            if (IsStructure)
            {
                DisplayBlankStructure();
            }

        }
        
       
        
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        incidentCentreSelect_RadComboBox.Visible = isStructure_HiddenField.Value != "True";
        incidentDropDownText_Literal.Text = isStructure_HiddenField.Value == "True"
            ? ts.GetResource("lbl_incidentDropDown_associatedArea")
            : ts.GetResource("lbl_incidentDropDown_incidentCentre");

    }

    // Methods for displaying structure, rather than ACCB info
    public void DisplayBlankStructure()
    {
        isStructure_HiddenField.Value = "True";
        structureTier2_DropDownList.SelectedValue = null;
        structureTier3_DropDownList.SelectedValue = null;
        structureTier4_DropDownList.SelectedValue = null;
        structureTier5_DropDownList.SelectedValue = null;
        structureTier6_DropDownList.SelectedValue = null;
        tier2Reference_HiddenField.Value = "";
        tier3Reference_HiddenField.Value = "";
        tier4Reference_HiddenField.Value = "";
        tier5Reference_HiddenField.Value = "";
        tier6Reference_HiddenField.Value = "";
        incidentCentreSelect_RadComboBox.SelectedValue = null;
        incidentCentreSelect_RadComboBox.Text = null;
        ReturnTierLevel("2", "");

    }

    public void DisplayStructure(string t2, string t3, string t4, string t5, string t6)
    {
        IsStructure = true;
        isStructure_HiddenField.Value = "True";

        if (t2 == "")
        {
            t2 = "-1";
        }
        if (t3 == "")
        {
            t3 = "-1";
        }
        if (t4 == "")
        {
            t4 = "-1";
        }
        if (t5 == "")
        {
            t5 = "-1";
        }
        if (t6 == "")
        {
            t6 = "-1";
        }

       
        ReturnDropDownTierData("2", "", t2);
        structureTier2_DropDownList.SelectedValue = t2 != "-1" ? t2 : null;


        ReturnDropDownTierData("3", t2,t3);
        structureTier3_DropDownList.SelectedValue  = t3 != "-1" ? t3 : null;

        ReturnDropDownTierData("4", t3, t4);
        structureTier4_DropDownList.SelectedValue = t4 != "-1" ? t4 : null;


        ReturnDropDownTierData("5", t4, t5);
        structureTier5_DropDownList.SelectedValue = t5 != "-1" ? t5 : null;


        ReturnDropDownTierData("6", t5, t6);
        structureTier6_DropDownList.SelectedValue = t6 != "-1" ? t6 : null;
        


        tier2Reference_HiddenField.Value = t2;
        tier3Reference_HiddenField.Value = t3;
        tier4Reference_HiddenField.Value = t4;
        tier5Reference_HiddenField.Value = t5;
        tier6Reference_HiddenField.Value = t6;

        Structure_UpdatePanel.Update();

    }

    public void ReturnAccbNameAndTierInformation()
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[IncidentCentre_ReturnFurtherLevelsFromACCB]", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", IncidentCentre));
             


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    IncidentStructureStartsOn = Convert.ToString(objrs["nextLevel"]);
                }
                else
                {
                    
                }

                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[IncidentCentre_ReturnTierNameInformation]", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", IncidentCentre));
                cmd.Parameters.Add(new SqlParameter("@t2", tier2Reference_HiddenField.Value));
                cmd.Parameters.Add(new SqlParameter("@t3", tier3Reference_HiddenField.Value));
                cmd.Parameters.Add(new SqlParameter("@t4", tier4Reference_HiddenField.Value));
                cmd.Parameters.Add(new SqlParameter("@t5", tier5Reference_HiddenField.Value));
                cmd.Parameters.Add(new SqlParameter("@t6", tier6Reference_HiddenField.Value));
             


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    Tier2Name = Convert.ToString(objrs["tier2_name"]);
                    Tier3Name = Convert.ToString(objrs["tier3_name"]);
                    Tier4Name = Convert.ToString(objrs["tier4_name"]);
                    Tier5Name = Convert.ToString(objrs["tier5_name"]);
                    Tier6Name = Convert.ToString(objrs["tier6_name"]);
                }
                else
                {
                    
                }

                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }



    }


    // These functions are for when a incident centre is selected.

    private void ReturnNextStructure_FromACCB(string accbCode, bool isSelected)
    {

        accbCode_HiddenField.Value = accbCode;

        string nextLevel = "";
       

        tier2Reference_HiddenField.Value = "";
        tier3Reference_HiddenField.Value = "";
        tier4Reference_HiddenField.Value = "";
        tier5Reference_HiddenField.Value = "";
        tier6Reference_HiddenField.Value = "";

        string previousLevelID = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[IncidentCentre_ReturnFurtherLevelsFromACCB]", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", accbCode));
             


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    nextLevel = Convert.ToString(objrs["nextLevel"]);
                    t2ID = Convert.ToString(objrs["REC_BU"]);
                    t3ID = Convert.ToString(objrs["REC_BL"]);
                    t4ID = Convert.ToString(objrs["tier4_ref"]);
                    t5ID = Convert.ToString(objrs["tier5_ref"]);
                    t6ID = Convert.ToString(objrs["tier6_ref"]);
                }
                else
                {
                    
                }

                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }


        switch (nextLevel)
        {
            
            case "2":
                previousLevelID = "";
                break;

            case "3":
                previousLevelID = t2ID;
                break;

            case "4":
                previousLevelID = t3ID;
                break;

            case "5":
                previousLevelID = t4ID;
                break;

            case "6":
                previousLevelID = t5ID;
                break;

        }

        tier2Reference_HiddenField.Value = t2ID;
        tier3Reference_HiddenField.Value = t3ID;
        tier4Reference_HiddenField.Value = t4ID;
        tier5Reference_HiddenField.Value = t5ID;
        tier6Reference_HiddenField.Value = t6ID;

       
        DisplayTierLevel_FromACCB(nextLevel, previousLevelID, isSelected);
        
        

      
    }


    private void DisplayTierLevel_FromACCB(string level, string previousLevelID, bool isSelected)
    {

       structureTier2_DropDownList.Visible = false;
       structureTier3_DropDownList.Visible = false;
       structureTier4_DropDownList.Visible = false;
       structureTier5_DropDownList.Visible = false;
       structureTier6_DropDownList.Visible = false;

        switch (level)
        {


            case "2":
                structureTier2_DropDownList.Visible = true;
                
                break;

            case "3":
                structureTier3_DropDownList.Visible = true;
                break;

            case "4":
                structureTier4_DropDownList.Visible = true;
                break;

            case "5":
                structureTier5_DropDownList.Visible = true;
                break;

            case "6":
                structureTier6_DropDownList.Visible = true;
                break;

        }

        if (!string.IsNullOrEmpty(level) && isSelected)
        {
            ReturnDropDownTierData(level, previousLevelID);
            Structure_UpdatePanel.Update();
        }
        

       

    }



    // Functions for generic return of DropDown (from events and so forth, not from a blank ACCB!)
    private void ReturnTierLevel(string level, string previousLevelID)
    {

        
        
        

        switch (level)
        {
            case "2":
                

                structureTier2_DropDownList.Visible = true;
                structureTier3_DropDownList.Visible = false;
                structureTier4_DropDownList.Visible = false;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier2Reference_HiddenField.Value = "";
                tier3Reference_HiddenField.Value = "";
                tier4Reference_HiddenField.Value = "";
                tier5Reference_HiddenField.Value = "";
                tier6Reference_HiddenField.Value = "";
                
                break;

            case "3":
                tier2Reference_HiddenField.Value = previousLevelID;
                

                structureTier3_DropDownList.Visible = true;
                structureTier4_DropDownList.Visible = false;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier3Reference_HiddenField.Value = "";
                tier4Reference_HiddenField.Value = "";
                tier5Reference_HiddenField.Value = "";
                tier6Reference_HiddenField.Value = "";
                break;

            case "4":
                tier3Reference_HiddenField.Value = previousLevelID;

                structureTier4_DropDownList.Visible = true;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier4Reference_HiddenField.Value = "";
                tier5Reference_HiddenField.Value = "";
                tier6Reference_HiddenField.Value = "";
                break;

            case "5":
                tier4Reference_HiddenField.Value = previousLevelID;

                structureTier5_DropDownList.Visible = true;
                structureTier6_DropDownList.Visible = false;
                tier5Reference_HiddenField.Value = "";
                tier6Reference_HiddenField.Value = "";
                break;

            case "6":
                tier5Reference_HiddenField.Value = previousLevelID;

                structureTier6_DropDownList.Visible = true;
                tier6Reference_HiddenField.Value = "";
                break;

        }

        if (!string.IsNullOrEmpty(level))
        {
            ReturnDropDownTierData(level, previousLevelID);
        }


        Structure_UpdatePanel.Update();
    }


    //Put the data in the dropdowns

    private void ReturnDropDownTierData(string level, string previousLevelID, string currentItem = null)
    {
        string currentLabel = "";

        bool showDisabled = IncidentType == IncidentCentreDropDown.IncidentType.Search;

        using (DataTable sDT = StructureDropDown_DataTable(level, previousLevelID, showDisabled, currentItem))
        {

                DropDownList currentDropDown = null;

                switch (level)
                {
                    case "2":
                        currentDropDown = structureTier2_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier2Label;

                        
                        break;

                    case "3":
                        currentDropDown = structureTier3_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier3Label;

                        
                        break;

                    case "4":
                        currentDropDown = structureTier4_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier4Label;

                        break;

                    case "5":
                        currentDropDown = structureTier5_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier5Label;

                        
                        break;

                    case "6":
                        currentDropDown = structureTier6_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier6Label;
                        break;
                }

                currentDropDown.DataSource = null;
                currentDropDown.DataBind();

                if (currentDropDown != null)
                {
                    currentDropDown.DataSource = sDT;
                    currentDropDown.DataTextField = "struct_name";
                    currentDropDown.DataValueField = "tierRef";
                    currentDropDown.DataBind();

                    currentDropDown.Visible = currentDropDown.Items.Count > 0;

                    currentDropDown.Items.Insert(0, new ListItem(ts.Translate("Please select a") + " " + currentLabel + "...", "0"));
                    if (showDisabled)
                    {
                        currentDropDown.Items.Insert(1, new ListItem(ts.Translate("All") + " " + currentLabel, "-1"));
                    }

                }

        }

    }

    private DataTable StructureDropDown_DataTable(string level, string previousLevelID, bool showDisabled,
        string selectedItem)
    {

        DataTable sDT = new DataTable();


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_PEP.dbo.GetAllStructureTier" + level, dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corpCode", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@previousTierID", previousLevelID));
                cmd.Parameters.Add(new SqlParameter("@showDisabled", showDisabled));
                if (selectedItem == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@selectedItem", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@selectedItem", selectedItem));
                }
                


                objrs = cmd.ExecuteReader();
                sDT.Load(objrs);
              

                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }
        

        return sDT;


    }


    // Blank retrieval
    public void DisplayBlankCentre()
    {
        isStructure_HiddenField.Value = "False";
        ReturnTierLevel("2", "");
        structureTier2_DropDownList.Visible = false;
        structureTier3_DropDownList.Visible = false;
        structureTier4_DropDownList.Visible = false;
        structureTier5_DropDownList.Visible = false;
        structureTier6_DropDownList.Visible = false;
        tier2Reference_HiddenField.Value = "";
        tier3Reference_HiddenField.Value = "";
        tier4Reference_HiddenField.Value = "";
        tier5Reference_HiddenField.Value = "";
        tier6Reference_HiddenField.Value = "";
        structureTier2_DropDownList.SelectedValue = null;
        structureTier3_DropDownList.SelectedValue = null;
        structureTier4_DropDownList.SelectedValue = null;
        structureTier5_DropDownList.SelectedValue = null;
        structureTier6_DropDownList.SelectedValue = null;
        incidentCentreSelect_RadComboBox.SelectedValue = null;
        incidentCentreSelect_RadComboBox.Text = null;
    }
    

    //Initiate incident centre from physical code and structure already selected
    public void ReturnIncidentCentreFromExternal(string accbCode, string t2, string t3, string t4, string t5, string t6)
    {


        

        ReturnNextStructure_FromACCB(accbCode, false);

        bool needT2 = structureTier2_DropDownList.Visible;
        bool needT3 = structureTier3_DropDownList.Visible;
        bool needT4 = structureTier4_DropDownList.Visible;
        bool needT5 = structureTier5_DropDownList.Visible;
        bool needT6 = structureTier6_DropDownList.Visible;


        if (needT2)
        {
            needT3 = true;
            needT4 = true;
            needT5 = true;
            needT6 = true;
        }
        if (needT3)
        {
            needT4 = true;
            needT5 = true;
            needT6 = true;
        }
        if (needT4)
        {
            needT5 = true;
            needT6 = true;
        }
        if (needT5)
        {
            needT6 = true;
        }


        if (t2 == "")
        {
            t2 = t2ID;
        }
        if (t3 == "")
        {
            t3 = t3ID;
        }
        if (t4 == "")
        {
            t4 = t4ID;
        }
        if (t5 == "")
        {
            t5 = t5ID;
        }
        if (t6 == "")
        {
            t6 = t6ID;
        }


        if (needT2)
        {
            ReturnDropDownTierData("2", "");
            structureTier2_DropDownList.SelectedValue = t2 != "-1" && t2 != "0" ? t2 : null;
        }
        if (needT3)
        {
            ReturnDropDownTierData("3", t2);
            structureTier3_DropDownList.SelectedValue  = t3 != "-1" && t3 != "0" ? t3  : null;
        }
        if (needT4)
        {
            ReturnDropDownTierData("4", t3);
            structureTier4_DropDownList.SelectedValue = t4 != "-1" && t4 != "0" ? t4 : null;
        }
        if (needT5)
        {
            ReturnDropDownTierData("5", t4);
            structureTier5_DropDownList.SelectedValue = t5 != "-1" && t5 != "0" ? t5 : null;
        }
        if (needT6)
        {
            ReturnDropDownTierData("6", t5);
            structureTier6_DropDownList.SelectedValue = t6 != "-1" && t6 != "0" ? t6 : null;
        }


        tier2Reference_HiddenField.Value = t2;
        tier3Reference_HiddenField.Value = t3;
        tier4Reference_HiddenField.Value = t4;
        tier5Reference_HiddenField.Value = t5;
        tier6Reference_HiddenField.Value = t6;


        
        Incident.IncidentGeneric.returnIncidentCentresDropDown_Initilise(incidentCentreSelect_RadComboBox, _currentUser,
            "any", _corpCode, "", accbCode, IsCreate);


        Structure_UpdatePanel.Update();

    }

    // Events
    public void generic_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "none", _corpCode);
    }


    public void generic_UserDropDown_Validation(object source, ServerValidateEventArgs args)
    {

        CustomValidator currentValidator = (CustomValidator)source;
        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);
        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, false);

    }

    protected void incident_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        Incident.IncidentGeneric.returnIncidentCentresDropDown(currentDropDown, e, "none", _corpCode, _currentUser, "", IsCreate);
    }


    protected void incidentCentreSelect_OnSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        
        // Run a check to see if we need to display any further tiers
        string selectedACCB = e.Value;
        accbCode_HiddenField.Value = selectedACCB;
        ReturnNextStructure_FromACCB(selectedACCB, true);

        Structure_UpdatePanel.Update();


    }
    protected void structureTier2_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("3", selectedOption);
    }

    protected void structureTier3_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("4", selectedOption);
    }

    protected void structureTier4_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("5", selectedOption);
    }

    protected void structureTier5_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("6", selectedOption);
    }

    protected void structureTier6_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        tier6Reference_HiddenField.Value = selectedOption;
        Structure_UpdatePanel.Update();

    }
   
}


public class IncidentCentreDropDown
{



    private IncidentType _IncidentType;
    private Status _IncidentStatus;


    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5,
        Main = 6,
        Investigation = 7,
        Defined = 8,
        Creation = 9,
        Search = 10,
        SearchStructureOnly = 11

    }

    public enum Status
    {
        Edit = 0,
        Report = 1,

    }

}