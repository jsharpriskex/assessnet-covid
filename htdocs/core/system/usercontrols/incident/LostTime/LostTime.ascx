﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LostTime.ascx.cs" Inherits="LostTime_UserControl" %>


<script>
    

    

</script>

<div id="editContent" runat="server" visible="false">

    <asp:CustomValidator ID="lostTimeExternalValidator" runat="server" ValidationGroup="LostTimeExternal" ClientIDMode="Static" ClientValidationFunction="ValidateLostTimeExternal"></asp:CustomValidator>

    <asp:UpdatePanel ID="lostTimeList_UpdatePanel" UpdateMode="Conditional" runat="server" ChildrenAsTriggers="False">
        <ContentTemplate>
            
            <div class="row" id="buttonMain_Div" runat="server">
                <div class="col-xs-12 text-right" style="padding-bottom: 10px;">
                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="addLostTime_LinkButton" runat="server" CssClass="btn btn-success" OnClick="addLostTime_Button_Click" OnClientClick="RemoveErrorLTA()"><i class="fa fa-plus"></i> <%=ts.GetResource("button_lostTime_noLostTime") %></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <asp:Repeater ID="lostTimeList_Repeater" runat="server" OnItemCommand="lostTimeList_Repeater_ItemCommand" OnItemDataBound="lostTimeList_Repeater_OnItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-condensed table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="30%"><%#ts.GetResource("lbl_lostTime_personAffected") %></th>
                                        <th width="15%"><%#ts.GetResource("lbl_lostTime_type") %></th>
                                        <th width="10%" class="text-center"><%#ts.GetResource("lbl_lostTime_startDate") %></th>
                                        <th width="10%" class="text-center"><%#ts.GetResource("lbl_lostTime_endDate") %></th>
                                        <th width="15%" class="text-center"><%#ts.GetResource("lbl_timeLost") %></th>
                                        <th width="20%" class="text-center"><%#ts.Translate("Options") %></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="lta_row">
                                <td><asp:Literal runat="server" id="itemLanguageBadge"></asp:Literal><%#Eval("per_name") %></td>
                                <td><%#LostTimeType(Eval("lta_type")) %></td>
                                <td class="text-center"><%# ASNETNSP.Formatting.FormatDateTime(Eval("start_date").ToString(), 1) %></td>
                                <td class="text-center"><%# LostTimeEnd(Eval("ed_type"), Eval("start_date"), Eval("end_date")) %></td>
                                <td class="text-center"><%#LostTimeTotal(Eval("ed_type"), Eval("hours"), Eval("days")) %></td>
                                <td class="text-center">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="lostTimeButton_UpdatePanel">
                                        <ContentTemplate>
                                            <asp:LinkButton runat="server" CssClass="btn btn-sm btn-warning" CommandArgument='<%#Eval("id") %>' CommandName="EditLTA" ID="editLTA_LinkButton"><i class="fa fa-pencil"> </i></asp:LinkButton>
                                            <asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" CommandArgument='<%#Eval("id") %>' CommandName="RemoveLTA" ID="removeLTA_LinkButton"><i class="fa fa-trash"> </i></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:LinkButton runat="server" CssClass="btn btn-sm btn-primary disabled" CommandArgument='<%#Eval("id") %>'  ID="disabled_LinkButton" visible="False">N/A</asp:LinkButton>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="lostTimeAlert_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row" id="lostTimeAlert_Div" runat="server">
                <div class="col-xs-12">
                    <div class="alert alert-danger alertForLostTime">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><%=ts.GetResource("h4_lostTime_noneFound") %></h4>
                                        <p runat="server" id="mainAlertText_Div"><%=ts.GetResource("lbl_lostTime_noLostTime") %></p>
                                        <p runat="server" id="typeAlertText_Div"><%=ts.GetResource("lbl_lostTime_requiredLostTime") %></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="row" id="buttonType_Div" runat="server">
        <div class="col-xs-12 text-center">
            <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="addLostTime_Button" runat="server" CssClass="btn btn-success" OnClick="addLostTime_Button_Click" OnClientClick="RemoveErrorLTA()"><i class="fa fa-plus"></i> <%=ts.GetResource("button_lostTime_noLostTime") %></asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>




    <!-- Lost Time Modal -->
    <div class="modal fade" id="lostTimeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="">Lost Time</h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel ID="lostTimeModal_UpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>

                            <asp:HiddenField ID="lostTimeID_HiddenField" Value="" runat="server" />
                            <asp:HiddenField ID="lostTimeSubRef_HiddenField" Value="" runat="server" />
                            <asp:Label ID="lostTimeID_Label" value="" runat="server"></asp:Label>
                            
                            <div id="fullLTAForm_Div" runat="server">

                                <div class="row" runat="server" id="fullName_Div" visible="False">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="fullName_TextBox"><%=ts.GetResource("lbl_lostTime_fullName") %></label>
                                            <asp:TextBox runat="server" ID="fullName_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                   
                                </div>

                                <div class="row" runat="server">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="lostTimeType_DropDownList"><%=ts.GetResource("lbl_lostTime_type") %></label>
                                            <asp:DropDownList runat="server" class="form-control" ID="lostTimeType_DropDownList" aria-describedby="title" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="commencedDate_RadDatePicker"><%=ts.GetResource("lbl_lostTime_commencedDate") %></label>
                                            <telerik:RadDatePicker runat="server" Skin="Bootstrap" ID="commencedDate_RadDatePicker" Width="100%" PopupDirection="BottomLeft" EnableScreenBoundaryDetection="false">
                                                <ClientEvents OnDateSelected="commencedDate_RadDatePicker_OnDateSelected" />
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="number_TextBox"><%=ts.GetResource("lbl_lostTime_returnType") %></label>
                                            <asp:DropDownList runat="server" class="form-control" ID="returnType_DropDownList" aria-describedby="title" ClientIDMode="Static" onchange="ToggleReturnInformation();">
                                                <asp:ListItem Value="na"></asp:ListItem>
                                                <asp:ListItem Value="1"></asp:ListItem>
                                                <asp:ListItem Value="2"></asp:ListItem>
                                                <asp:ListItem Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" id="returnDate_Div">
                                        <div class="form-group">
                                            <label for="returnDate_RadDatePicker"><%=ts.GetResource("lbl_lostTime_returnDate") %></label>
                                            <telerik:RadDatePicker runat="server" Skin="Bootstrap" ID="returnDate_RadDatePicker" Width="100%" PopupDirection="BottomLeft" EnableScreenBoundaryDetection="false">
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" id="returnHours_Div">
                                        <div class="form-group">
                                            <label for="hoursLostTime_DropDownList"><%=ts.GetResource("lbl_lostTime_hoursOfLostTime") %></label>
                                            <asp:DropDownList ID="hoursLostTime_DropDownList" runat="server" CssClass="form-control" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="description_TextBox"><%=ts.GetResource("lbl_lostTime_description") %></label>
                                            <asp:TextBox runat="server" ID="description_TextBox" CssClass="form-control" TextMode="MultiLine" Rows="4" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="returnedDateOnly_Div" runat="server">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="alert alert-danger">
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <%=ts.GetResource("lbl_lostTime_linkedRIDDOR") %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label for="returnDate_RadDatePicker"><%=ts.GetResource("lbl_lostTime_returnDate") %></label>
                                            <telerik:RadDatePicker runat="server" Skin="Bootstrap" ID="returnDate2_RadDatePicker" Width="100%" PopupDirection="BottomLeft" EnableScreenBoundaryDetection="false">
                                            </telerik:RadDatePicker>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <asp:CustomValidator ID="lostTime_CustomValidator" runat="server" ValidationGroup="LostTime" ClientIDMode="Static" ClientValidationFunction="ValidateLostTime"></asp:CustomValidator>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>
                            <asp:LinkButton runat="server" CssClass="btn btn-success" ID="saveLostTime_Button" OnClick="saveLostTime_Button_Click" ValidationGroup="LostTime"><%=ts.GetResource("button_lostTime_save") %></asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of LTA Modal -->

    <!-- alert -->
    <div class="modal fade" id="LostTimeAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id=""><%=ts.GetResource("h4_alert_title") %></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <%=ts.GetResource("lbl_alert_text") %>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>                         
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of alert Modal -->

</div>

<div id="reportContent" runat="server" visible="False">
    
    <div class="row">
        <div class="col-xs-12">
            <asp:Repeater ID="lostTimeListReport_Repeater" runat="server">
                <HeaderTemplate>
                    <table class="table table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="30%"><%#ts.GetResource("lbl_lostTime_personAffected") %></th>
                                <th width="15%"><%#ts.GetResource("lbl_lostTime_type") %></th>
                                <th width="10%" class="text-center"><%#ts.GetResource("lbl_lostTime_startDate") %></th>
                                <th width="10%" class="text-center"><%#ts.GetResource("lbl_lostTime_endDate") %></th>
                                <th width="15%" class="text-center"><%#ts.GetResource("lbl_timeLost") %></th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="lta_row">
                        <td><%#Eval("per_name") %></td>
                        <td><%#LostTimeType(Eval("lta_type")) %></td>
                        <td class="text-center"><%# ASNETNSP.Formatting.FormatDateTime(Eval("start_date").ToString(), 1) %></td>
                        <td class="text-center"><%# LostTimeEnd(Eval("ed_type"), Eval("start_date"), Eval("end_date")) %></td>
                        <td class="text-center"><%#LostTimeTotal(Eval("ed_type"), Eval("hours"), Eval("days")) %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>

</div>