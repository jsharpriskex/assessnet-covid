﻿

  

    function ToggleFurtherWitnessTypes() {

        var witnessWholeIncidentY = $("[id*='wholeIncident_Y_RadioButton']").prop('checked');
        var witnessWholeIncidentN = $("[id*='wholeIncident_N_RadioButton']").prop('checked');

        if (witnessWholeIncidentY) {
            $('#furtherWitnessTypes').hide();
        } else if(witnessWholeIncidentN) {
            $('#furtherWitnessTypes').show();
        }


    }

    function ShowWitnessDetails() {
        $("#collapseFirstSection").show();
        $("#collapseThirdSection").hide();
        $("#collapseSecondSection").hide();

        ToggleFurtherWitnessTypes();
    }

    function ShowInterviewDetails() {
        $("#collapseFirstSection").hide();
        $("#collapseThirdSection").hide();
        $("#collapseSecondSection").show();
    }

    function ShowAttachmentDetails() {
        $("#collapseFirstSection").hide();
        $("#collapseSecondSection").hide();
        $("#collapseThirdSection").show();
        InitiateDropZone();

    }

    function ChangeSection(position, validate) {

        var isPageValid = true;

        if (validate) {
            
            isPageValid= ValidateForm();
        }

        if (isPageValid) {
            if (position === "1") {
                ShowWitnessDetails();
            }
            else if (position === "2") {
                ShowInterviewDetails();
            }
            else if (position === "3") {
                ShowAttachmentDetails();
            }
        }
        
    }

    function ValidateForm() {


        var isValid = true;
        

        $("#collapseFirstSection, #collapseSecondSection").find("input[type=text]:visible,textarea:visible").each(function() {

            var optionReference = $(this);

            if ($(this).attr('id') !== "witIDNumber_TextBox") {
                if (optionReference.val() === "") {
                    optionReference.addClass("inputError");
                    isValid = false;
                } else {
                    optionReference.removeClass("inputError");
                }
            }

           

        });

        $("#collapseFirstSection").find("select").each(function () {
            

            var optionReference = $(this);

            if (optionReference.val() == "na") {
                isValid = false;
                optionReference.addClass("inputError");
            }
            else {
                optionReference.removeClass("inputError");
            }

        });

        
       
        $('.inputRow:visible').each(function (i, obj) {
            var $this = $(this);
            var foundCheckboxes = false;
            var foundCheckedbox = false;


            // Radio Buttons
            // Loop through any found radios
            $this.find("input:radio, input:checkbox").each(function () {

                foundCheckboxes = true;

                // Ok we know there are checkboxes, so we have to see if one has been selected
                if ($(this).is(':checked')) {
                    foundCheckedbox = true;
                }


            });
            if (foundCheckboxes == true && foundCheckedbox == false) {
                isValid = false;
                $this.addClass("inputError");
            }
            else if (foundCheckboxes == true && foundCheckedbox == true) {
                $this.removeClass("inputError");
            }


        });


           
        if ($("#interviewDeclaration").is(":visible")) {


            if ($("#interviewDeclaration_CheckBox").is(':checked')) {
                $("#interviewDeclaration").removeClass("inputError");
            } else {
                $("#interviewDeclaration").addClass("inputError");
                isValid = false;
            }
        }



        if (!isValid) {
            $("#witnessAlert").modal('show');
        }

        return isValid;

    }
