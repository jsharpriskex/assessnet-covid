﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CostAnalysis.ascx.cs" Inherits="CostAnalysis_UserControl" %>


<div class="row">
    <div class="col-xs-12 text-right">
        <asp:UpdatePanel UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="addCostAnalysis_Button" runat="server" CssClass="btn btn-success" OnClick="addCostAnalysis_Button_Click"><i class="fa fa-plus"></i> <%=ts.GetResource("button_cost_add")%></asp:LinkButton>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<br/>

<asp:UpdatePanel ID="costAnalysisList_UpdatePanel" UpdateMode="Conditional" runat="server">
    <ContentTemplate>

        <div class="row">
            <div class="col-xs-12">

                <asp:Repeater ID="costAnalysisList_Repeater" runat="server" OnItemCommand="costAnalysisList_Repeater_ItemCommand" OnItemDataBound="costAnalysisList_Repeater_ItemDataBound">
                    <HeaderTemplate>
                        <table class="table table-condensed table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="10%"><%#ts.GetResource("lbl_cost_type")%></th>
                                    <th width="10%">Sub <%#ts.GetResource("lbl_cost_type")%></th>
                                    <th width="40%"><%=ts.GetResource("lbl_cost_description")%></th>
                                    <th width="15%" class="text-center"><%#ts.GetResource("lbl_cost_date")%></th>
                                    <th width="10%" class="text-center"><%#ts.GetResource("lbl_cost")%></th>
                                    <th width="20%" class="text-center"><%#ts.Translate("Options")%></th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                                <tr>
                                    <td><%#Eval("group_title") %></td>

                                    <td><%#Eval("item_title") %></td>
                                    
                                    <td><asp:Literal runat="server" id="itemLanguageBadge"></asp:Literal> <asp:Literal runat="server" id="description_Literal"></asp:Literal></td>
                                    <td class="text-center"><%# ASNETNSP.Formatting.FormatDateTime(Eval("cost_date"), 1) %></td>
                                    <td class="text-center">£<%#Eval("cost_amount") %></td>
                                    <td class="text-center">
                                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:LinkButton runat="server" CssClass="btn btn-sm btn-warning" CommandArgument='<%#Eval("costreference") %>' CommandName="EditCost"><i class="fa fa-pencil"> </i></asp:LinkButton>
                                                <asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" CommandArgument='<%#Eval("costreference") %>' ID="removeCost_LinkButton" OnCommand="removeCost_LinkButton_OnCommand"><i class="fa fa-trash"> </i></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                                <tr>
                                    <td style="border:none;"></td>
                                    <td style="border:none;"></td>
                                    <td style="border:none;"></td>
                                    <td style="border:none;"></td>
                                    <td class="text-center"><strong>£<%#ReturnTotal() %></strong></td>
                                    <td style="border:none;"></td>
                                </tr>
                            </tbody>
                        </table>
                    </FooterTemplate>
                    
                </asp:Repeater>

            </div>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>




<asp:UpdatePanel ID="costAnalysisAlert_UpdatePanel" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="row" id="costAnalysisAlert_Div" runat="server">
            <div class="col-xs-12">
                <div class="alert alert-warning">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><%=ts.GetResource("h4_cost_noCosts")%></h4>
                                    <p><%=ts.GetResource("h4_cost_noCosts_text")%></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>





<!-- CA Modal -->
<div class="modal fade" id="costAnalysisModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id=""><%=ts.GetResource("h4_cost_costAnalysis")%></h4>
                
            </div>
            <div class="modal-body">
                <asp:UpdatePanel ID="costAnalysisModal_UpdatePanel" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>

                        <asp:HiddenField ID="costAnalysisID_HiddenField" Value="" runat="server" />
                        <asp:Label ID="lostTimeID_Label" value="" runat="server"></asp:Label>

                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="costAnalysisType_DropDownList"><%=ts.GetResource("lbl_cost_costIncurred")%></label>
                                    <asp:DropDownList runat="server" class="form-control" ID="costAnalysisType_DropDownList" aria-describedby="title" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="commencedDate_RadDatePicker"><%=ts.GetResource("lbl_cost_date")%></label>
                                    <telerik:RadDatePicker runat="server" Skin="Bootstrap" ID="dateRecorded_RadDatePicker" Width="100%"></telerik:RadDatePicker>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="cost_RadNumericTextBox"><%=ts.GetResource("lbl_cost")%> (£)</label>
                                    <telerik:RadNumericTextBox runat="server" Skin="Bootstrap" Width="100%" ID="cost_RadNumericTextBox" ClientIDMode="Static" ShowSpinButtons="true"></telerik:RadNumericTextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="costDescription_TextBox"><%=ts.GetResource("lbl_cost_description")%></label>
                                    <asp:TextBox runat="server" ID="costDescription_TextBox" CssClass="form-control" TextMode="MultiLine" Rows="4" ClientIDMode="Static" />
                                </div>
                            </div>
                        </div>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <asp:CustomValidator ID="costAnalysis_CustomValidator" runat="server" ValidationGroup="CostAnalysis" ClientIDMode="Static" ClientValidationFunction="ValidateCostAnalysis"></asp:CustomValidator>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>
                        <asp:LinkButton runat="server" CssClass="btn btn-success" ID="saveCostAnalysis_Button"  OnClick="saveCostAnalysis_Button_Click" ValidationGroup="CostAnalysis"><%=ts.GetResource("button_cost_save")%></asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of CA Modal -->

<!-- alert -->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id=""><%=ts.GetResource("h4_alert_title") %></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <%=ts.GetResource("lbl_alert_text") %>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>                         
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->

<script type="text/javascript">



</script>
