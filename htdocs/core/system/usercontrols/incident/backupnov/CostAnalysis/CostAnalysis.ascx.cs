﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class CostAnalysis_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public CostAnalysisList CurrentCostAnalysisList;
    public CostAnalysisList.IncidentType IncidentType;

    private bool ReturnData;
  

    public event EventHandler SaveOrEditButton_Click;

    private bool FormDataRequired = false;

    public string PersonStatusText;
    public string AdvancedContactDetails;
    public string AdvancedContactDetails_StaffID;
    public string AdvancedContactDetails_StaffID_Description;
    public string PersonDetailsUnknown;
    public string PersonDetailsAddressExternal;

    public bool ShowData;

    public double totalCost;


    public DateTime IncidentDateTime;

    public TranslationServices ts;

    public event EventHandler RefreshClick;

    protected void Page_Load(object sender, EventArgs e)
    {
        

        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************

        
        ////REMOVE
        //ts = new TranslationServices(_corpCode, "en-gb", "incidentCG");

        CurrentCostAnalysisList = new CostAnalysisList(this);
        totalCost = 0;

        if (ReturnData)
        {
            _ReturnCostAnalysisList();
        }

    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        //ts = new TranslationServices(_corpCode, "en-gb", "incidentCG");
    }


    public void ReturnCostAnalysisList()
    {
        ReturnData = true;

    }


    private void _ReturnCostAnalysisList()
    {
        totalCost = 0;
        CurrentCostAnalysisList.ReturnCostAnalysisList();
        ReturnData = false;

    }


    protected void addCostAnalysis_Button_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "$('#costAnalysisModal').modal('show');", true);
        CurrentCostAnalysisList.ReturnIndividualCostAnalysis("");
    }

    protected void saveCostAnalysis_Button_Click(object sender, EventArgs e)
    {
        CurrentCostAnalysisList.currentCostAnalysis.SaveCostAnalysisData();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "saveinfo", "HideCostAnalysisModal();", true);


        _ReturnCostAnalysisList();
        //lostTimeList_UpdatePanel.Update();

        if (RefreshClick != null)
        {
            RefreshClick(this, e); 

        }
    }

    protected void costAnalysisList_Repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string currentID = e.CommandArgument.ToString();

        switch (e.CommandName)
        {
            case "EditCost":
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "$('#costAnalysisModal').modal('show');", true);
                CurrentCostAnalysisList.ReturnIndividualCostAnalysis(currentID);
                break;

            case "RemoveCost":
                CurrentCostAnalysisList.RemoveIndividualCost(currentID);
                _ReturnCostAnalysisList();
                costAnalysisList_UpdatePanel.Update();

                if (RefreshClick != null)
                {
                    RefreshClick(this, e); 

                }

                break;
        }


    }


    protected void costAnalysisList_Repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            //Literal totalCost_Literal = (Literal)e.Item.FindControl("totalCost");
            double currentCost = Convert.ToDouble(DataBinder.Eval(e.Item.DataItem, "cost_amount"));

            totalCost += currentCost;

            string witLanguage = DataBinder.Eval(e.Item.DataItem, "display_language").ToString() ?? "";
            string description = DataBinder.Eval(e.Item.DataItem, "cost_description").ToString() ?? "";

            description = ts.Translate(description, witLanguage, IncidentCentre, IncidentReference,IncidentSubReference, "cost_description") + " ";

            ((Literal)e.Item.FindControl("itemLanguageBadge")).Text = ts.GetRecordMarker(witLanguage, ts.LanguageCode()) + " ";
            ((Literal) e.Item.FindControl("description_Literal")).Text = description;



        }
    }

    protected string ReturnTotal()
    {

        double truncatedTotalCost = Math.Truncate(totalCost * 100) / 100;

        return string.Format("{0:N2}", truncatedTotalCost);
    }

    protected void removeCost_LinkButton_OnCommand(object sender, CommandEventArgs e)
    {
        CurrentCostAnalysisList.RemoveIndividualCost(e.CommandArgument.ToString());
        _ReturnCostAnalysisList();
        costAnalysisList_UpdatePanel.Update();

        if (RefreshClick != null)
        {
            RefreshClick(this, e); 

        }

    }
}


public class CostAnalysisList
{

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _subIncidentReference;
    private bool _isAdmin;

    private CostAnalysis_UserControl _currentUserControl;

    private IncidentType _IncidentType;

    private bool _hasData;

    private Repeater _costAnalysisList_Repeater;
    private HtmlGenericControl _costAnalysisAlert_Div;

    private UpdatePanel _costAnalysisList_UpdatePanel;
    private UpdatePanel _costAnalysisAlert_UpdatePanel;

    public CostAnalysis currentCostAnalysis;

    private DateTime _incidentDateTime;

    public CostAnalysisList(CostAnalysis_UserControl currentUserControl)
    {

        _currentUserControl = currentUserControl;

        _corpCode = _currentUserControl._corpCode;
        _currentUser = _currentUserControl._currentUser;
        _incidentCentre = _currentUserControl.IncidentCentre;
        _incidentReference = _currentUserControl.IncidentReference;
        _subIncidentReference = _currentUserControl.IncidentSubReference;
        _isAdmin = _currentUserControl.IsAdmin;
        _IncidentType = _currentUserControl.IncidentType;

        _costAnalysisList_Repeater = (Repeater)_currentUserControl.FindControl("costAnalysisList_Repeater");
        _costAnalysisAlert_Div = (HtmlGenericControl)_currentUserControl.FindControl("costAnalysisAlert_Div");
        _costAnalysisList_UpdatePanel = (UpdatePanel)_currentUserControl.FindControl("costAnalysisList_UpdatePanel");
        _costAnalysisAlert_UpdatePanel = (UpdatePanel)_currentUserControl.FindControl("costAnalysisAlert_UpdatePanel");

        _incidentDateTime = _currentUserControl.IncidentDateTime;

        currentCostAnalysis = new CostAnalysis(this);

    }
    

    public void ReturnCostAnalysisList()
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.CostAnalysis_Display ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@module", ReturnModuleCodeFromType(_IncidentType)));
                cmd.Parameters.Add(new SqlParameter("@language", _currentUserControl.ts.LanguageCode()));
                cmd.Parameters.Add(new SqlParameter("@costReference", DBNull.Value));


                objrs = cmd.ExecuteReader();

                _costAnalysisList_Repeater.DataSource = objrs;
                _costAnalysisList_Repeater.DataBind();

                objrs.Dispose();
                cmd.Dispose();


                if (_costAnalysisList_Repeater.Items.Count == 0)
                {
                    _costAnalysisAlert_Div.Visible = true;
                    _costAnalysisList_Repeater.Visible = false;
                }
                else
                {
                    _costAnalysisAlert_Div.Visible = false;
                    _costAnalysisList_Repeater.Visible = true;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }


        _costAnalysisList_UpdatePanel.Update();
        _costAnalysisAlert_UpdatePanel.Update();
    }


    
    public void RemoveIndividualCost(string currentId)
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.CostAnalysis_Remove ", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@costReference", currentId));



                cmd.ExecuteNonQuery();


            }
            catch (Exception)
            {
                throw;
            }

        }
    }
  
    private string ReturnModuleCodeFromType(IncidentType type)
    {

        string returnValue = "";
        
        switch (type)
        {
            case IncidentType.Injury:
                returnValue = "inj";
                break;
            case IncidentType.NearMiss:
                break;
            case IncidentType.PropertyDamage:
                break;
            case IncidentType.Illness:
                returnValue = "id";
                break;
            case IncidentType.Violence:
                break;
            case IncidentType.DangerousOccurrence:
                break;
            case IncidentType.Main:
                // this gets null'd
                returnValue = "acc";
                break;

            default:
                break;
        }


        return returnValue;
    }


    public void ReturnIndividualCostAnalysis(string costReference)
    {
        currentCostAnalysis.ReturnCostAnalysisData(costReference);
    }




    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5,
        Main = 6

    }

    public enum Status
    {
        Edit = 0,
        Report = 1
    }


    public class CostAnalysis
    {

        private CostAnalysis_UserControl _costAnalysis_UserControl;


        private string _corpCode;
        private string _currentUser;
        private string _yourAccessLevel;

        private DateTime _incidentDateTime;

        private string _incidentCentre;
        private string _incidentReference;
        private string _subIncidentReference;
        private bool _isAdmin;

        private CostAnalysis_UserControl _currentUserControl;
        private CostAnalysisList _costAnalysisList;

        private IncidentType _IncidentType;

        private bool _hasData;


        // Form Elements
        private UpdatePanel _costAnalysisModal_UpdatePanel;
        private HiddenField _costAnalysisID_HiddenField;
        private DropDownList _costAnalysisType_DropDownList;
        private RadDatePicker _dateRecorded_RadDatePicker;
        private RadNumericTextBox _cost_RadNumericTextBox;
        private TextBox _costDescription_TextBox;

        private string _costType;
        private DateTime? _commencedDate;
        private int _cost;
        private string _description;


        public TranslationServices _ts;


        public CostAnalysis(CostAnalysisList costAnalysisList)
        {
            _costAnalysis_UserControl = costAnalysisList._currentUserControl;

            _costAnalysisList = costAnalysisList;
            _currentUserControl = _costAnalysisList._currentUserControl;

            _ts = _costAnalysis_UserControl.ts;

            _corpCode = _costAnalysisList._corpCode;
            _currentUser = _costAnalysisList._currentUser;
            _isAdmin = _costAnalysisList._isAdmin;
            _incidentCentre = _costAnalysisList._incidentCentre;
            _incidentReference = _costAnalysisList._incidentReference;
            _subIncidentReference = _costAnalysisList._subIncidentReference;
            _IncidentType = _costAnalysisList._IncidentType;

            // Hook up form elements
            _costAnalysisID_HiddenField = (HiddenField)_currentUserControl.FindControl("costAnalysisID_HiddenField");
            _costAnalysisModal_UpdatePanel = (UpdatePanel)_currentUserControl.FindControl("costAnalysisModal_UpdatePanel");
            _costAnalysisType_DropDownList = (DropDownList)_currentUserControl.FindControl("costAnalysisType_DropDownList");
            _dateRecorded_RadDatePicker = (RadDatePicker)_currentUserControl.FindControl("dateRecorded_RadDatePicker");
            _cost_RadNumericTextBox = (RadNumericTextBox)_currentUserControl.FindControl("cost_RadNumericTextBox");
            _costDescription_TextBox = (TextBox)_currentUserControl.FindControl("costDescription_TextBox");

            _incidentDateTime = _currentUserControl.IncidentDateTime;
        }


        private void PopulateInternalVariablesFromForm()
        {

            _costType = _costAnalysisType_DropDownList.SelectedValue;
            _commencedDate = _dateRecorded_RadDatePicker.SelectedDate;
            _cost = Convert.ToInt32(_cost_RadNumericTextBox.Value);
            _description = Formatting.FormatTextInput(_costDescription_TextBox.Text);

        }

        private void PopulateInternalVariablesFromDB(string costReference)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.CostAnalysis_Display ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@module", _costAnalysisList.ReturnModuleCodeFromType(_IncidentType)));
                    cmd.Parameters.Add(new SqlParameter("@language", _currentUserControl.ts.LanguageCode()));
                    cmd.Parameters.Add(new SqlParameter("@costReference", costReference));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _costType = Convert.ToString(objrs["cost_type"]);
                        _commencedDate = Convert.ToDateTime(objrs["cost_date"]);
                        _cost = Convert.ToInt32(objrs["cost_amount"]);
                        _description = Convert.ToString(objrs["cost_description"]);

                    }

                    objrs.Dispose();
                    cmd.Dispose();


                }
                catch (Exception)
                {
                    throw;
                }

            }
        }

        private void PopulateFormFromInternalVariables()
        {

            

            _costAnalysisType_DropDownList.SelectedValue = _costType;
            _dateRecorded_RadDatePicker.SelectedDate = _commencedDate;
            _cost_RadNumericTextBox.Value = _cost;
            _costDescription_TextBox.Text = _description;

        }

        private void BlankAllFormInputs()
        {
            _costAnalysisID_HiddenField.Value = null;
            _dateRecorded_RadDatePicker.SelectedDate = null;
            _costDescription_TextBox.Text = null;
            _costAnalysisType_DropDownList.SelectedValue = null;
            _cost_RadNumericTextBox.Value = null;

            _dateRecorded_RadDatePicker.MinDate = _incidentDateTime;
            _dateRecorded_RadDatePicker.MaxDate = DateTime.Now;
        }

        public void ReturnCostAnalysisData(string costReference)
        {

            PopulateDropDowns();

            _costAnalysisID_HiddenField.Value = costReference;

            if (costReference != "")
            {
                PopulateInternalVariablesFromDB(costReference);
                PopulateFormFromInternalVariables();
            }
            else
            {
                BlankAllFormInputs();

            }

            _costAnalysisModal_UpdatePanel.Update();

        }

        private void PopulateDropDowns()
        {

            // Only one dropdown to initiate....

            _costAnalysisType_DropDownList.Items.Clear();
            using (DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "inc_cost", false, _ts.LanguageCode(), "0"))
            {

                _costAnalysisType_DropDownList.DataSource = accOptions;
                _costAnalysisType_DropDownList.DataTextField = "opt_value";
                _costAnalysisType_DropDownList.DataValueField = "opt_id";


                _costAnalysisType_DropDownList.DataBind();

                _costAnalysisType_DropDownList.Items.Insert(0, new ListItem(_ts.Translate("Please select") + "...", "na"));
            } 

        }



        public void SaveCostAnalysisData()
        {
            PopulateInternalVariablesFromForm();

            string costReference = _costAnalysisID_HiddenField.Value;


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.CostAnalysis_Save ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@module", _costAnalysisList.ReturnModuleCodeFromType(_IncidentType)));
                    cmd.Parameters.Add(new SqlParameter("@costReference", costReference));
                    cmd.Parameters.Add(new SqlParameter("@costType", _costType));
                    cmd.Parameters.Add(new SqlParameter("@costAmount", _cost));
                    cmd.Parameters.Add(new SqlParameter("@costDate", _commencedDate));
                    cmd.Parameters.Add(new SqlParameter("@costDescription", _description));
                    cmd.Parameters.Add(new SqlParameter("@accRef", _currentUser));
                    cmd.Parameters.Add(new SqlParameter("@accLang", _ts.LanguageCode()));



                    objrs = cmd.ExecuteReader();



                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }


            if (costReference == "")
            {
                Incident.IncidentGeneric.InsertIncidentHistory(_corpCode, _incidentCentre, _incidentReference, _incidentReference, _currentUser, "ACC", "Added Cost");
            }
            else
            {
                Incident.IncidentGeneric.InsertIncidentHistory(_corpCode, _incidentCentre, _incidentReference, _incidentReference, _currentUser, "ACC", "Edited Cost ( " + costReference + ")");
            }

            // CLEAR
            BlankAllFormInputs();
            _costAnalysisModal_UpdatePanel.Update();
        }


    }


}