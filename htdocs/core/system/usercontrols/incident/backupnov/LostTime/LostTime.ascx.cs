﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class LostTime_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public LostTimeList CurrentLostTimeList;
    //public PersonDetails.Type UserType;
    public LostTimeList.IncidentType IncidentType;
    public LostTimeList.Status Status;

    private bool ReturnData;
  

    public event EventHandler RefreshClick;

    private bool FormDataRequired = false;

    public string PersonStatusText;
    public string AdvancedContactDetails;
    public string AdvancedContactDetails_StaffID;
    public string AdvancedContactDetails_StaffID_Description;
    public string PersonDetailsUnknown;
    public string PersonDetailsAddressExternal;

    public bool ShowData;


    public DateTime IncidentDateTime;

    public TranslationServices ts;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (ts == null)
        {
            ts = new TranslationServices(_corpCode, "en-gb", "incident");
        }
       

        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************

        CurrentLostTimeList = new LostTimeList(this);
        IncidentDateTime = Incident.IncidentGeneric.ReturnIncidentDateTime(_corpCode, IncidentCentre, IncidentReference);
        InitiatePrecautions();

        

        if (ReturnData)
        {
            _ReturnLostTimeList();
        }

        if (IncidentType == LostTimeList.IncidentType.Main)
        {
            buttonMain_Div.Visible = true;
            buttonType_Div.Visible = false;
            mainAlertText_Div.Visible = true;
            typeAlertText_Div.Visible = false;
        }
        else
        {
            buttonMain_Div.Visible = false;
            buttonType_Div.Visible = true;
            mainAlertText_Div.Visible = false;
            typeAlertText_Div.Visible = true;
        }

        TranslateOptions();


    }
     
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (ts == null)
        {
            ts = new TranslationServices(_corpCode, "en-gb", "incident");
        }
    }

    private void TranslateOptions()
    {

        if (returnType_DropDownList.Items.Count > 0)
        {

            if (returnType_DropDownList.Items.FindByValue("na") != null)
            {
                returnType_DropDownList.Items.FindByValue("na").Text = ts.GetResource("li_lostTime_returnType_pleaseSelect");
            }

            if (returnType_DropDownList.Items.FindByValue("1") != null)
            {
                returnType_DropDownList.Items.FindByValue("1").Text = ts.GetResource("li_lostTime_returnType_later");
            }

            if (returnType_DropDownList.Items.FindByValue("2") != null)
            {
                returnType_DropDownList.Items.FindByValue("2").Text = ts.GetResource("li_lostTime_returnType_returned");
            }

            if (returnType_DropDownList.Items.FindByValue("3") != null)
            {
                returnType_DropDownList.Items.FindByValue("3").Text = ts.GetResource("li_lostTime_returnType_notReturned");
            }
            
            
            
            
        }
        


    }
    
    protected void Page_PreRender(object sender, EventArgs e)
    {

        CurrentLostTimeList = new LostTimeList(this);

        if (ShowData)
        {
            _ReturnLostTimeList();
        }

    }

    protected void InitiatePrecautions()
    {
        commencedDate_RadDatePicker.MinDate = IncidentDateTime;
        returnDate_RadDatePicker.MinDate =  IncidentDateTime;
        returnDate2_RadDatePicker.MinDate = IncidentDateTime;

        commencedDate_RadDatePicker.MaxDate = DateTime.Today;
        returnDate_RadDatePicker.MaxDate = DateTime.Today;
        returnDate2_RadDatePicker.MaxDate = DateTime.Today;
    }

    public void ReturnLostTimeList()
    {
        ReturnData = true;
 
    }

    private void _ReturnLostTimeList()
    {
        CurrentLostTimeList.ReturnLostTimeList();
        ReturnData = false;

    }



    public string LostTimeType(object ltaType)
    {

        string currentLTA = ltaType.ToString();

        switch (currentLTA)
        {
            case "inj_afw":
                return ts.GetResource("lbl_lostTime_awayFromWork");
            case "inj_ld":
                return ts.GetResource("lbl_lostTime_lightDuties");
            case "id_afw":
                return ts.GetResource("lbl_lostTime_awayFromWork");
            case "id_ld":
                return ts.GetResource("lbl_lostTime_lightDuties");
            default:
                return ASNETNSP.Incident.IncidentGeneric.ReturnAccOptByValue(_corpCode, "lta_type", ts.LanguageCode(), currentLTA);;
        }


    }

    public string LostTimeTotal(object edType, object hours_Value, object days_value)
    {

        string currentType = edType.ToString();
        string hours = hours_Value.ToString();
        string days = days_value.ToString();

        switch (currentType)
        {

            case "1":
                return hours + " " + ts.Translate("Hours");
            case "2":
                return days + " " + ts.Translate("Days");
            case "3":
                return "Not returned to date";
            default:
                return ts.Translate("ERROR");
        }

    }

    public string LostTimeEnd(object edType, object startDate_Value, object endDate_Value)
    {


        switch (edType.ToString())
        {

            case "1":
                return "Same Day";
            case "2":
                return ((DateTime)endDate_Value).ToShortDateString();
            case "3":
                return "--";
            default:
                return "ERROR";
        }

            
 

        

    }



    //Button Handlers



    protected void addLostTime_Button_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "ToggleReturnInformation(); $('#lostTimeModal').modal('show');", true);
        CurrentLostTimeList.ReturnIndividualLostTime("");
    }

    protected void saveLostTime_Button_Click(object sender, EventArgs e)
    {
        CurrentLostTimeList.currentLostTime.SaveLostTimeData();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "saveinfo", "HideLostTimeModal();", true);


        _ReturnLostTimeList();
        lostTimeList_UpdatePanel.Update();

        if (RefreshClick != null)
        {
            RefreshClick(this, e); 

        }
    }

    protected void lostTimeList_Repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string currentID = e.CommandArgument.ToString();

        switch (e.CommandName)
        {
            case "EditLTA":
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "ToggleReturnInformation(); $('#lostTimeModal').modal('show');", true);
                CurrentLostTimeList.ReturnIndividualLostTime(currentID);
                lostTimeList_UpdatePanel.Update();
                break;

            case "RemoveLTA":
                CurrentLostTimeList.RemoveIndividualLostTime(currentID);
                _ReturnLostTimeList();
                lostTimeList_UpdatePanel.Update();

                if (RefreshClick != null)
                {
                    RefreshClick(this, e); 

                }

                break;
        }


    }

    protected void lostTimeList_Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
      
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string injuryRIDDOR = DataBinder.Eval(e.Item.DataItem, "injuryRIDDOR").ToString() ?? "";
            string ltaLanguage = DataBinder.Eval(e.Item.DataItem, "display_language").ToString() ?? "";

            LinkButton editLTA_LinkButton = (LinkButton) e.Item.FindControl("editLTA_LinkButton");
            LinkButton removeLTA_LinkButton = (LinkButton) e.Item.FindControl("removeLTA_LinkButton");
            ((Literal)e.Item.FindControl("itemLanguageBadge")).Text = ts.GetRecordMarker(ltaLanguage, ts.LanguageCode()) + " ";


            if (!string.IsNullOrEmpty(injuryRIDDOR))
            {
                editLTA_LinkButton.Visible = false;
                editLTA_LinkButton.Visible = false;
            }

        }
    }
}


public class LostTimeList
{

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _subIncidentReference;
    private bool _isAdmin;

    private IncidentType _IncidentType;
    private Status _incidentStatus;

    private bool _hasData;

    private LostTime_UserControl _lostTime_UserControl;

    private Repeater _lostTimeList_Repeater;
    private HtmlGenericControl _lostTimeAlert_Div;

    private UpdatePanel _lostTimeList_UpdatePanel;
    private UpdatePanel _lostTimeAlert_UpdatePanel;

    private HtmlGenericControl _editContent;
    private HtmlGenericControl _reportContent;

    public LostTime currentLostTime;

    public LostTimeList(LostTime_UserControl lostTime_UserControl)
    {
        _lostTime_UserControl = lostTime_UserControl;


        _corpCode = _lostTime_UserControl._corpCode;
        _currentUser = _lostTime_UserControl._currentUser;
        _isAdmin = _lostTime_UserControl.IsAdmin;
        _incidentCentre = _lostTime_UserControl.IncidentCentre;
        _incidentReference = _lostTime_UserControl.IncidentReference;
        _subIncidentReference = _lostTime_UserControl.IncidentSubReference;
        _IncidentType = _lostTime_UserControl.IncidentType;
        
        _lostTimeAlert_Div = (HtmlGenericControl)_lostTime_UserControl.FindControl("lostTimeAlert_Div");
        _incidentStatus = _lostTime_UserControl.Status;
        _editContent = (HtmlGenericControl)_lostTime_UserControl.FindControl("editContent");
        _reportContent = (HtmlGenericControl)_lostTime_UserControl.FindControl("reportContent");

        _lostTimeList_UpdatePanel = (UpdatePanel)_lostTime_UserControl.FindControl("lostTimeList_UpdatePanel");
        _lostTimeAlert_UpdatePanel = (UpdatePanel)_lostTime_UserControl.FindControl("lostTimeAlert_UpdatePanel");




        if (_incidentStatus == Status.Edit)
        {
            _editContent.Visible = true;
            _lostTimeList_Repeater = (Repeater)_lostTime_UserControl.FindControl("lostTimeList_Repeater");
        }
        else if (_incidentStatus == Status.Report)
        {
            _reportContent.Visible = true;
            _lostTimeList_Repeater = (Repeater)_lostTime_UserControl.FindControl("lostTimeListReport_Repeater");
        }

        currentLostTime = new LostTime(this);

    }


    // Return current list of list time options

    public void ReturnLostTimeList()
    {

    


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.sp_return_lta_list ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@list_type", ReturnModuleCodeFromType(_IncidentType)));
                cmd.Parameters.Add(new SqlParameter("@module_version", "10"));

                objrs = cmd.ExecuteReader();

                _lostTimeList_Repeater.DataSource = objrs;
                _lostTimeList_Repeater.DataBind();

                objrs.Dispose();
                cmd.Dispose();


                if(_lostTimeList_Repeater.Items.Count == 0)
                {
                    _lostTimeAlert_Div.Visible = true;
                    _lostTimeList_Repeater.Visible = false;
                }
                else
                {
                    _lostTimeAlert_Div.Visible = false;
                    _lostTimeList_Repeater.Visible = true;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }


        _lostTimeList_UpdatePanel.Update();
        _lostTimeAlert_UpdatePanel.Update();

    }

    private string ReturnModuleCodeFromType(IncidentType type)
    {

        string returnValue = "";
        
        switch (type)
        {
            case IncidentType.Injury:
                returnValue = "inj";
                break;
            case IncidentType.NearMiss:
                break;
            case IncidentType.PropertyDamage:
                break;
            case IncidentType.Illness:
                returnValue = "id";
                break;
            case IncidentType.Violence:
                break;
            case IncidentType.DangerousOccurrence:
                break;
            case IncidentType.Main:
                returnValue = "";
                break;
            default:
                break;
        }


        return returnValue;
    }



    public void ReturnIndividualLostTime(string ltaID)
    {
        currentLostTime.ReturnLostTimeData(ltaID);
    }


    public void RemoveIndividualLostTime(string ltaID)
    {
        currentLostTime.RemoveLTA(ltaID);
    }

    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5,
        Main = 6,
        Investigation = 7,
        Defined = 8
    }

    public enum Status
    {
        Edit = 0,
        Report = 1
    }


    public class LostTime
    {

        private LostTime_UserControl _lostTime_UserControl;
        private LostTimeList _LostTimeList;

        private string _corpCode;
        private string _currentUser;
        private string _yourAccessLevel;

        private string _incidentCentre;
        private string _incidentReference;
        private string _subIncidentReference;
        private bool _isAdmin;

        private HiddenField _lostTimeID_HiddenField;
        private HiddenField _lostTimeSubRef_HiddenField;
        private Label _lostTimeID_Label;
        private UpdatePanel _lostTimeModal_UpdatePanel;


        private DropDownList _lostTimeType_DropDownList;
        private RadDatePicker _commencedDate_RadDatePicker;
        private DropDownList _returnType_DropDownList;
        private RadDatePicker _returnDate_RadDatePicker;
        private RadDatePicker _returnDate2_RadDatePicker;
        private TextBox _description_TextBox;
        private DropDownList _hoursLostTime_DropDownList;

        private TextBox _fullName_TextBox;
        private HtmlGenericControl _fullName_Div;

        private string _type;
        private DateTime? _startDate;
        private string _returnType;
        private DateTime? _returnDate;
        private string _description;
        private string _hours;
        private string _days;
        private string _additionalName;

        private bool _isInjuryRIDDORAndNotReturnedToDate;
        private HtmlGenericControl _fullLTAForm_Div;
        private HtmlGenericControl _returnedDateOnly_Div;

        private IncidentType _IncidentType;
        private IncidentType _FormIncidentType;

        public LostTime(LostTimeList lostTimeList)
        {
            _LostTimeList = lostTimeList;
            _lostTime_UserControl = lostTimeList._lostTime_UserControl;


            _corpCode = _LostTimeList._corpCode;
            _currentUser = _LostTimeList._currentUser;
            _isAdmin = _LostTimeList._isAdmin;
            _incidentCentre = _LostTimeList._incidentCentre;
            _incidentReference = _LostTimeList._incidentReference;
            _subIncidentReference = _LostTimeList._subIncidentReference;
            //_IncidentType = _LostTimeList._IncidentType;


            if(_subIncidentReference.Contains("INJ"))
            {
                _IncidentType = IncidentType.Injury;
            }
            else if(_subIncidentReference.Contains("ID"))
            {
                _IncidentType = IncidentType.Illness;
            }
            else
            {
                _IncidentType = IncidentType.Main;
            }

            _FormIncidentType = _LostTimeList._IncidentType;
            


            _lostTimeID_HiddenField = (HiddenField)_lostTime_UserControl.FindControl("lostTimeID_HiddenField");
            _lostTimeID_Label = (Label)_lostTime_UserControl.FindControl("lostTimeID_Label");
            _lostTimeModal_UpdatePanel = (UpdatePanel)_lostTime_UserControl.FindControl("lostTimeModal_UpdatePanel");

            _lostTimeType_DropDownList = (DropDownList)_lostTime_UserControl.FindControl("lostTimeType_DropDownList");
            _commencedDate_RadDatePicker = (RadDatePicker)_lostTime_UserControl.FindControl("commencedDate_RadDatePicker");
            _returnType_DropDownList = (DropDownList)_lostTime_UserControl.FindControl("returnType_DropDownList");
            _returnDate_RadDatePicker = (RadDatePicker)_lostTime_UserControl.FindControl("returnDate_RadDatePicker");
            _returnDate2_RadDatePicker = (RadDatePicker)_lostTime_UserControl.FindControl("returnDate2_RadDatePicker");
            _description_TextBox = (TextBox)_lostTime_UserControl.FindControl("description_TextBox");
            _hoursLostTime_DropDownList = (DropDownList)_lostTime_UserControl.FindControl("hoursLostTime_DropDownList");
            _fullName_TextBox = (TextBox)_lostTime_UserControl.FindControl("fullName_TextBox");
            _fullName_Div = (HtmlGenericControl)_lostTime_UserControl.FindControl("fullName_Div");
            _lostTimeSubRef_HiddenField = (HiddenField)_lostTime_UserControl.FindControl("lostTimeSubRef_HiddenField");

            _fullLTAForm_Div = (HtmlGenericControl)_lostTime_UserControl.FindControl("fullLTAForm_Div");
            _returnedDateOnly_Div = (HtmlGenericControl)_lostTime_UserControl.FindControl("returnedDateOnly_Div");

        }



        private void PopulateInternalVariablesFromDB(string ltaID)
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.sp_return_lta ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@lta_id", ltaID));


                    objrs = cmd.ExecuteReader();


                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _type = Convert.ToString(objrs["lta_type"]);
                        _startDate = Convert.ToDateTime(objrs["start_Date"]);
                        _returnType = Convert.ToString(objrs["ed_type"]);
                        _returnDate = objrs["end_date"] != DBNull.Value ? Convert.ToDateTime(objrs["end_date"]) : DateTime.MinValue;
                        _description = Convert.ToString(objrs["description"]);
                        _hours = Convert.ToString(objrs["hours"]);
                        _additionalName = Convert.ToString(objrs["additional_name"]);

                        string currentSubReference = Convert.ToString(objrs["sub_ref"]);
                        _lostTimeSubRef_HiddenField.Value = currentSubReference;


                        string injuryRIDDOR = Convert.ToString(objrs["injuryRIDDOR"]);
                        if (!string.IsNullOrEmpty(injuryRIDDOR) && _returnType == "3")
                        {
                            _isInjuryRIDDORAndNotReturnedToDate = true;
                        }
                        


                        if(currentSubReference.Contains("INJ"))
                        {
                            _IncidentType = IncidentType.Injury;
                        }
                        else if(currentSubReference.Contains("ID"))
                        {
                            _IncidentType = IncidentType.Illness;
                        }
                        else
                        {
                            _IncidentType = IncidentType.Main;
                        }

                    }

                    objrs.Dispose();
                    cmd.Dispose();

                    

                }
                catch (Exception)
                {
                    throw;
                }

            }

        }

        private void PopulateInternalVariablesFromForm()
        {

            _type = _lostTimeType_DropDownList.SelectedValue;
            _startDate = _commencedDate_RadDatePicker.SelectedDate;
            _returnType = _returnType_DropDownList.SelectedValue;
            _returnDate = _returnDate_RadDatePicker.SelectedDate;
            _description = Formatting.FormatTextInput(_description_TextBox.Text);
            _hours = _hoursLostTime_DropDownList.SelectedValue;
            _additionalName = Formatting.FormatTextInput(_fullName_TextBox.Text);

           

            


            if (_returnType == "1")
            {
                _returnDate = _startDate;
                _days = "0";
            }
            else if (_returnType == "2")
            {
                _hours = "0";
                _days = Math.Floor(((DateTime)_returnDate - (DateTime)_startDate).TotalDays).ToString();
            }
            else if (_returnType == "3")
            {
                _returnDate = null;
                _hours = "0";
                _days = Math.Floor((DateTime.Now - (DateTime)_startDate).TotalDays).ToString();
            }


            if (_returnDate2_RadDatePicker.SelectedDate != null)
            {
                _returnDate = _returnDate2_RadDatePicker.SelectedDate;
                _returnType = "2";
                _days = Math.Floor(((DateTime)_returnDate - (DateTime)_startDate).TotalDays).ToString();
            }

        }

        private void PopulateFormFromInternalVariables()
        {
            _lostTimeType_DropDownList.SelectedValue = _type;
            _commencedDate_RadDatePicker.SelectedDate = _startDate;
            _returnType_DropDownList.SelectedValue = _returnType;
            if (_returnDate != DateTime.MinValue)
            {
                _returnDate_RadDatePicker.SelectedDate = _returnDate;
            }
            
            _description_TextBox.Text = Formatting.FormatTextInputField(_description);
            
            _fullName_TextBox.Text = Formatting.FormatTextInputField(_additionalName);


            if (_hoursLostTime_DropDownList.Items.FindByValue(_hours) != null)
            {
                _hoursLostTime_DropDownList.SelectedValue = _hours;
            }

            if (_isInjuryRIDDORAndNotReturnedToDate)
            {
                _fullLTAForm_Div.Visible = false;
                _returnedDateOnly_Div.Visible = true;
            }
            else
            {
                _fullLTAForm_Div.Visible = true;
                _returnedDateOnly_Div.Visible = false;
            }

        }

        private void BlankAllFormInputs()
        {
            _returnedDateOnly_Div.Visible = false;
            _fullLTAForm_Div.Visible = true;
            _lostTimeType_DropDownList.SelectedValue = null;
            _commencedDate_RadDatePicker.SelectedDate = null;
            _returnType_DropDownList.SelectedValue = null;
            _returnDate_RadDatePicker.SelectedDate = null;
            _description_TextBox.Text = null;
            _hoursLostTime_DropDownList.SelectedValue = null;
            _lostTimeID_HiddenField.Value = "";
            _fullName_TextBox.Text = null;
            _lostTimeSubRef_HiddenField.Value = null;
            _returnDate2_RadDatePicker.SelectedDate = null;
        }

        public void ReturnLostTimeData(string ltaID)
        {

           

            _lostTimeID_HiddenField.Value = ltaID;
            
            if(ltaID != "")
            {
                PopulateInternalVariablesFromDB(ltaID);
                PopulateDropDowns(_IncidentType);
                PopulateFormFromInternalVariables();
                _returnDate_RadDatePicker.MinDate =  _commencedDate_RadDatePicker.SelectedDate ?? DateTime.Today;

            }
            else
            {
                PopulateDropDowns(_IncidentType);
                BlankAllFormInputs();
                _commencedDate_RadDatePicker.SelectedDate = _commencedDate_RadDatePicker.MinDate;

            }

            if (_IncidentType == IncidentType.Main)
            {
                _fullName_Div.Visible = true;
            }
            else
            {
                _fullName_Div.Visible = false;
            }


            //_commencedDate_RadDatePicker.MaxDate = DateTime.Now;
            //_returnDate_RadDatePicker.MaxDate = DateTime.Now;



            _lostTimeModal_UpdatePanel.Update();

        }

        private void PopulateDropDowns(IncidentType incidentType)
        {

            // Incident Type needs to be found when clicking the button against the ID

            // Lost Time Type - this changes if its injury, illness or generic.
            // Generic pulls through from the global list of categories, the others dont as they use a static value to determine if its RIDDOR or not.

            _lostTimeType_DropDownList.Items.Clear();

            bool useGeneric = false;

            switch (incidentType)
            {
                case IncidentType.Injury:
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.Translate("Please select") + "...", "na"));
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.GetResource("lbl_lostTime_awayFromWork"), "inj_afw"));
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.GetResource("lbl_lostTime_lightDuties"), "inj_ld"));
                    break;
                case IncidentType.Illness:
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.Translate("Please select") + "...", "na"));
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.GetResource("lbl_lostTime_awayFromWork"), "id_afw"));
                    _lostTimeType_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.GetResource("lbl_lostTime_lightDuties"), "id_ld"));
                    break;
                default:
                    useGeneric = true;
                    break;
            }

            if (useGeneric)
            {

                using (DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "lta_type", false, _lostTime_UserControl.ts.LanguageCode(), "0"))
                {

                    _lostTimeType_DropDownList.DataSource = accOptions;
                    _lostTimeType_DropDownList.DataTextField = "opt_value";
                    _lostTimeType_DropDownList.DataValueField = "opt_id";


                    _lostTimeType_DropDownList.DataBind();

                    _lostTimeType_DropDownList.Items.Insert(0, new ListItem(_lostTime_UserControl.ts.Translate("Please select") + "...", "na"));


                    //if (_personStatus != "")
                    //{
                    //    if (_personStatus_DropDownList.Items.FindByValue(_personStatus) != null)
                    //    {
                    //        _personStatus_DropDownList.SelectedValue = _personStatus;
                    //    }
                    //}


                }

            }



            // populate hours..
            _hoursLostTime_DropDownList.Items.Add(new ListItem(_lostTime_UserControl.ts.Translate("Please select") + "...", "na"));

            for (int i = 1; i < 24; i++)
            {
                _hoursLostTime_DropDownList.Items.Add(new ListItem(i.ToString(), i.ToString()));

            }


        }

        public void SaveLostTimeData()
        {

            PopulateInternalVariablesFromForm();

            string ltaID = _lostTimeID_HiddenField.Value;
            string forcedSubReference = "";


            if (_FormIncidentType == IncidentType.Main)
            {
                _subIncidentReference = _lostTimeSubRef_HiddenField.Value;
            }
     

            // Remove any existing lost time

            if(ltaID != "")
            {
                RemoveLTA(ltaID);
            }


            



            // Add Lost Time
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.sp_insert_lta ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@start_date", ((DateTime)_startDate).ToShortDateString()));
                    cmd.Parameters.Add(new SqlParameter("@end_date_opt", _returnType));
                    if(_returnDate == null)
                    {
                        cmd.Parameters.Add(new SqlParameter("@end_date", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@end_date", ((DateTime)_returnDate).ToShortDateString()));
                    }
                    
                    cmd.Parameters.Add(new SqlParameter("@description", _description));
                    cmd.Parameters.Add(new SqlParameter("@lta_type", _type));
                    cmd.Parameters.Add(new SqlParameter("@hours", _hours));
                    cmd.Parameters.Add(new SqlParameter("@days", _days));
                    cmd.Parameters.Add(new SqlParameter("@additional_name", _additionalName));
                    cmd.Parameters.Add(new SqlParameter("@your_acc_ref", _currentUser));
                    cmd.Parameters.Add(new SqlParameter("@accLang", _lostTime_UserControl.ts.LanguageCode()));



                    objrs = cmd.ExecuteReader();



                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }


            Incident.IncidentGeneric.InsertIncidentHistory(_corpCode, _incidentCentre, _incidentReference, _incidentReference, _currentUser, "ACC", "Added Lost Time");


            // CLEAR
            BlankAllFormInputs();
            _lostTimeModal_UpdatePanel.Update();
        }

        public void RemoveLTA(string ltaID)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.sp_remove_lta ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@id", ltaID));
                    cmd.Parameters.Add(new SqlParameter("@your_acc_ref", _currentUser));

                    objrs = cmd.ExecuteReader();
                                       
                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }


            Incident.IncidentGeneric.InsertIncidentHistory(_corpCode, _incidentCentre, _incidentReference, _incidentReference, _currentUser, "ACC", "Removed Lost Time");

        }
        
    }


}