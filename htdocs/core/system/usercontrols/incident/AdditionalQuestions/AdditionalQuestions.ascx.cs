﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class AdditionalQuestions_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public AdditionalQuestionsList CurrentAdditionalQuestionList;
    //public PersonDetails.Type UserType;
    public AdditionalQuestionsList.IncidentType IncidentType;
    public AdditionalQuestionsList.Status Status;

    public bool ReturnData;
  

    public event EventHandler SaveOrEditButton_Click;

    private bool FormDataRequired = false;

    public string PersonStatusText;
    public string AdvancedContactDetails;
    public string AdvancedContactDetails_StaffID;
    public string AdvancedContactDetails_StaffID_Description;
    public string PersonDetailsUnknown;
    public string PersonDetailsAddressExternal;

    public bool ShowData;

    public TranslationServices ts;
    public string accLang;


    public DateTime IncidentDateTime;

    public DateTime StartDate; //Ocado Use Only

    public bool IsSignedOff;


    protected void Page_Init(object sender, EventArgs e)
    {
        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();

        //END OF SESSION STATUS CHECK
        //***************************

        if (ts == null)
        {
            ts = new TranslationServices(_corpCode, accLang, "incident");
        }

        if (IncidentType != AdditionalQuestionsList.IncidentType.Main)
        {
            additionalQuestions_Validator.Enabled = false;
        }


        CurrentAdditionalQuestionList = new AdditionalQuestionsList(this);
        currentIDOfAdditionalQuestion.Value = this.ID;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        
        
    }


    public void SaveData()
    {
        CurrentAdditionalQuestionList.SaveData();
    }


    public void generic_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "none", _corpCode);
    }


    public void generic_UserDropDown_Validation(object source, ServerValidateEventArgs args)
    {
        CustomValidator currentValidator = (CustomValidator)source;


        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);

        //if (currentRadBox.ID == "recordReviewUser" && oneoffAssessmentResetSwitch.Checked == true)
        //{

        //}
        //else
        //{
        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, false);
        //}


    }

}


public class AdditionalQuestionsList
{

    private DataTable _controlData = new DataTable();

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _subIncidentReference;
    private bool _isAdmin;

    private bool _returnData;

    private IncidentType _IncidentType;
    private Status _IncidentStatus;

    private DateTime _startDate;

    private bool _hasData;

    private bool _isSignedOff;

    AdditionalQuestions_UserControl _additionalQuestions_UserControl;

    PlaceHolder _additionalQuestions_PlaceHolder;

    public AdditionalQuestionsList(AdditionalQuestions_UserControl currentControl)
    {
        _additionalQuestions_UserControl = currentControl;

        _corpCode = _additionalQuestions_UserControl._corpCode;
        _currentUser = _additionalQuestions_UserControl._currentUser;
        _isAdmin = _additionalQuestions_UserControl.IsAdmin;
        _incidentCentre = _additionalQuestions_UserControl.IncidentCentre;
        _incidentReference = _additionalQuestions_UserControl.IncidentReference;
        _subIncidentReference = _additionalQuestions_UserControl.IncidentSubReference;
        _IncidentType = _additionalQuestions_UserControl.IncidentType;
        _IncidentStatus = _additionalQuestions_UserControl.Status;
        _startDate = _additionalQuestions_UserControl.StartDate;
        _isSignedOff = _additionalQuestions_UserControl.IsSignedOff;

        _returnData = _additionalQuestions_UserControl.ReturnData;

        _additionalQuestions_PlaceHolder = (PlaceHolder)_additionalQuestions_UserControl.FindControl("additionalQuestions_PlaceHolder");

        if (_returnData)
        {
            InitiateData();

            if(_IncidentStatus == Status.Edit)
            {
                InitiateControls();
            }
            else if (_IncidentStatus == Status.Report)
            {
                InitiateReport();
            }
            
        }

    }




    private void InitiateData()
    {

        _controlData.Clear();
        if (_controlData.Columns.Count == 0)
        {
            _controlData.Columns.Add("question_ref", typeof(string));
            _controlData.Columns.Add("question_title", typeof(string));
            _controlData.Columns.Add("question_description", typeof(string));
            _controlData.Columns.Add("question_type", typeof(string));
            _controlData.Columns.Add("trigger_actions", typeof(bool));
            _controlData.Columns.Add("question_answer", typeof(string));
            _controlData.Columns.Add("mandatory", typeof(string));
            _controlData.Columns.Add("parent_ref", typeof(string));
            _controlData.Columns.Add("action_text", typeof(string));
            _controlData.Columns.Add("action_status", typeof(string));
            _controlData.Columns.Add("action_assigned_to", typeof(string));
            _controlData.Columns.Add("action_due_date", typeof(DateTime));
            _controlData.Columns.Add("triggerAnswer", typeof(string));

        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.AdditionalQuestions_Display", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@module", ReturnModuleCodeFromType(_IncidentType)));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    while (objrs.Read())
                    {
                        string question_ref = objrs["question_ref"].ToString();
                        string question_title = objrs["question_title"].ToString();
                        string question_description = objrs["question_description"].ToString();
                        string question_type = objrs["question_type"].ToString();
                        bool trigger_actions = Convert.ToBoolean(objrs["trigger_actions"]);
                        string question_answer = Formatting.FormatTextInputField(objrs["question_answer"].ToString());
                        string mandatory = objrs["mandatory"].ToString();
                        string parent_ref = objrs["parent_ref"].ToString();

                        string action_text = objrs["action_text"].ToString();
                        string action_status = objrs["task_status"].ToString();
                        string action_assigned_to = objrs["original_action_to"].ToString();

                        string triggerAnswer = objrs["triggerAnswer"].ToString();

                        DateTime action_due_date;

                        if (objrs["task_due_date"] == DBNull.Value)
                        {
                            action_due_date = DateTime.MinValue;
                        }
                        else
                        {
                            action_due_date = Convert.ToDateTime(objrs["task_due_date"]);
                        }
                         
                        


                        _controlData.Rows.Add(question_ref, question_title, question_description, question_type
                                , trigger_actions, question_answer, mandatory, parent_ref, action_text, action_status, action_assigned_to, action_due_date, triggerAnswer);

                        
                    }

                }
                else
                {
                    //Error - TODO
                }

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        // If all the values are null, then this returns false.
        _hasData = _controlData.AsEnumerable().Count(r => r["question_answer"] == DBNull.Value || r["question_answer"] == "") != _controlData.Rows.Count;
        

    }

    // This needs to load each time there is a postback
    private void InitiateControls() {

        foreach (DataRow row in _controlData.Rows)
        {
            string questionReference = row.Field<string>(0);
            string questionTitle = row.Field<string>(1);
            string questionDescription = row.Field<string>(2);
            string questionType = row.Field<string>(3);
            bool triggerActions = row.Field<bool>(4);
            string questionAnswer = row.Field<string>(5);
            string mandatory = row.Field<string>(6);
            string parentReference = row.Field<string>(7);

            string actionText = row.Field<string>(8);
            string actionStatus = row.Field<string>(9);
            string action_assigned_to = row.Field<string>(10);
            DateTime action_due_date = row.Field<DateTime>(11);

            string triggerAnswer = row.Field<string>(12);


            bool isDropDown = questionType.Contains("DD_") == true ? true : false;
            bool isTextItem = questionType.Contains("Section") || questionType.Contains("Title") == true ? true : false;

            LiteralControl tableTop_Html = new LiteralControl();
            LiteralControl tableBottom_Html = new LiteralControl();




           




            string formGroup = "form-group";
            if(questionType == "YN" || questionType == "YNNA" || questionType == "YNU")
            {
                formGroup = "";
            }

            if (isTextItem)
            {
                if (questionType.Contains("Section"))
                {
                    tableTop_Html.Text = "<h2 style='margin-bottom:15px; margin-top:15px;'>" + questionTitle + "</h2>";
                }
                else if (questionType.Contains("Title"))
                {
                    tableTop_Html.Text = "<h3 style='margin-bottom:10px; margin-top:10px;'>" + questionTitle + "</h2>";
                }



                tableBottom_Html.Text = "";

                _additionalQuestions_PlaceHolder.Controls.Add(tableTop_Html);
            }
            else
            {

                string parentClass = parentReference.Length > 0 ? "question_" + parentReference : "";
                string parentHide = parentReference.Length > 0 && questionAnswer.Length == 0 ? "style='display:none;'" : "";

                // If its ocado, we need to skip question 8!
                if (_corpCode == "065912" && questionReference == "8" && _IncidentType == IncidentType.Injury && _IncidentStatus == Status.Edit)
                {
                    parentHide = "style='display:none;'";
                }

                if (questionDescription != "")
                {
                    questionTitle = questionTitle + " <br> <small class='info'>" + questionDescription + "</small>";
                }

                tableTop_Html.Text = " <table class='table table-condensed table-bordered table-addQ " + parentClass + "' " + parentHide  + "> " +
                "<tr>" +
                    " <th width='35%'>" + questionTitle + "</th >" +
                          "<td class='inputRow'>" +
                             " <div class='" + formGroup + "'>";

                tableBottom_Html.Text = " </div> " +
                           " </td>" +
                       " </tr>" +
                   " </table>";

                _additionalQuestions_PlaceHolder.Controls.Add(tableTop_Html);

                switch (questionType)
                {

                    case "Free Text":
                        TextBox control_TextBox = new TextBox();
                        control_TextBox.ID = "control_" + questionReference;
                        control_TextBox.CssClass = "form-control";
                        control_TextBox.Text = questionAnswer;



                        _additionalQuestions_PlaceHolder.Controls.Add(control_TextBox);

                        break;

                    case "Large Text":
                        TextBox control_MultiTextBox = new TextBox();
                        control_MultiTextBox.ID = "control_" + questionReference;
                        control_MultiTextBox.CssClass = "form-control";
                        control_MultiTextBox.TextMode = TextBoxMode.MultiLine;
                        control_MultiTextBox.Rows = 4;
                        control_MultiTextBox.Text = questionAnswer;

                        _additionalQuestions_PlaceHolder.Controls.Add(control_MultiTextBox);

                        break;

                    case "Numeric":
                        RadNumericTextBox control_Numeric = new RadNumericTextBox();
                        control_Numeric.ID = "control_" + questionReference;
                        control_Numeric.Skin = "Bootstrap";
                        control_Numeric.NumberFormat.DecimalDigits = 0;
                        control_Numeric.ShowSpinButtons = true;
                        control_Numeric.Width = new Unit("50%");
                        control_Numeric.Text = questionAnswer;

                        _additionalQuestions_PlaceHolder.Controls.Add(control_Numeric);

                        break;

                    case "Date":
                        RadDatePicker control_Date = new RadDatePicker();
                        control_Date.ID = "control_" + questionReference;
                        control_Date.Skin = "Bootstrap";
                        control_Date.Width = new Unit("50%");


                        if (string.IsNullOrEmpty(questionAnswer))
                        {
                            if (_corpCode == "065912" && questionReference == "7" && _IncidentType == IncidentType.Injury && _IncidentStatus == Status.Edit)
                            {
                                if(HttpContext.Current.Session["Ocado_UserStartDate"] != null)
                                {
                                    questionAnswer = HttpContext.Current.Session["Ocado_UserStartDate"].ToString();
                                }
                            }
                        }
                       

                        
                        control_Date.SelectedDate = string.IsNullOrEmpty(questionAnswer) ? (DateTime?)null : Convert.ToDateTime(questionAnswer);
                        
                        

                        _additionalQuestions_PlaceHolder.Controls.Add(control_Date);

                        break;

                    case "Time":
                        RadTimePicker control_Time = new RadTimePicker();
                        control_Time.ID = "control_" + questionReference;
                        control_Time.Skin = "Bootstrap";
                        control_Time.Width = new Unit("50%");
                        control_Time.SelectedDate = questionAnswer == "" ? (DateTime?)null : Convert.ToDateTime(questionAnswer);

                        _additionalQuestions_PlaceHolder.Controls.Add(control_Time);

                        break;

                    case "YN": case "YNNA":
                    case "YNU":

                        string parentRadioGroup_HTML = "<div class='parentRadioGroup'>";
                        string parentRadioGroup_bottom_HTML = "</div>";


                        string radioItem_top_HTML = "<div class='radioGroup'>" +
                                                        "<label class='green white'>";

                        string radioItem_Bottom_HTML = "</label></div>";

                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(parentRadioGroup_HTML));




                        RadioButton yes_RadioButton = new RadioButton();
                        yes_RadioButton.ID = "control_" + questionReference + "_Y";
                        yes_RadioButton.GroupName = "control_" + questionReference;
                        yes_RadioButton.Attributes.Add("onclick", "TriggerIfChildQuestionNeeded('" + questionReference + "', 'Y', '"+  triggerAnswer + "')");
                        yes_RadioButton.Checked = questionAnswer == "Y" ? true : false;

                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_top_HTML));
                        _additionalQuestions_PlaceHolder.Controls.Add(yes_RadioButton);
                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl("<span>Yes</span>"));
                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_Bottom_HTML));


                        RadioButton no_RadioButton = new RadioButton();
                        no_RadioButton.ID = "control_" + questionReference + "_N";
                        no_RadioButton.GroupName = "control_" + questionReference;
                        no_RadioButton.Attributes.Add("onclick", "TriggerIfChildQuestionNeeded('" + questionReference + "', 'N', '"+  triggerAnswer + "')");
                        no_RadioButton.Checked = questionAnswer == "N" ? true : false;

                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_top_HTML));
                        _additionalQuestions_PlaceHolder.Controls.Add(no_RadioButton);
                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl("<span>No</span>"));
                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_Bottom_HTML));


                        if(questionType == "YNNA")
                        {
                            RadioButton na_RadioButton = new RadioButton();
                            na_RadioButton.ID = "control_" + questionReference + "_NA";
                            na_RadioButton.GroupName = "control_" + questionReference;
                            na_RadioButton.Attributes.Add("onclick", "TriggerIfChildQuestionNeeded('" + questionReference + "', 'NA', '"+  triggerAnswer + "')");
                            na_RadioButton.Checked = questionAnswer == "NA" ? true : false;

                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_top_HTML));
                            _additionalQuestions_PlaceHolder.Controls.Add(na_RadioButton);
                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl("<span>Not Applicable</span>"));
                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_Bottom_HTML));
                        }
                        else if (questionType == "YNU")
                        {
                            RadioButton u_RadioButton = new RadioButton();
                            u_RadioButton.ID = "control_" + questionReference + "_U";
                            u_RadioButton.GroupName = "control_" + questionReference;
                            u_RadioButton.Attributes.Add("onclick", "TriggerIfChildQuestionNeeded('" + questionReference + "', 'U', '"+  triggerAnswer + "')");
                            u_RadioButton.Checked = questionAnswer == "U" ? true : false;

                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_top_HTML));
                            _additionalQuestions_PlaceHolder.Controls.Add(u_RadioButton);
                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl("<span>Unknown</span>"));
                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(radioItem_Bottom_HTML));
                        }


                            _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(parentRadioGroup_bottom_HTML));
                        break;



                    default:


                        if (isDropDown)
                        {

                            //ReturnDropDownItems
                            string dropDownReference = questionType.Replace("DD_", "");

                            DropDownList control_DropDownList = new DropDownList();
                            control_DropDownList.ID = "control_" + questionReference;
                            control_DropDownList.CssClass = "form-control";

                            control_DropDownList.DataSource = ReturnDropDownItems(dropDownReference);
                            control_DropDownList.DataTextField = "opt_value";
                            control_DropDownList.DataValueField = "opt_id";
                            control_DropDownList.DataBind();


                            if (control_DropDownList.Items.FindByValue(questionAnswer) != null)
                            {
                                control_DropDownList.SelectedValue = questionAnswer;
                            }
                           

                            control_DropDownList.Items.Insert(0, new ListItem("Please Select...", "na"));

                            _additionalQuestions_PlaceHolder.Controls.Add(control_DropDownList);
                        }

                        break;
                }




            }



            _additionalQuestions_PlaceHolder.Controls.Add(tableBottom_Html);




            if(triggerActions)
            {

                if(actionText.Length == 0)
                {

                    // Requires an action

                    // Action Text
                    string html1 = "<table class='table table-condensed table-bordered' style='margin-top: -6px;'>" +
                                        "<tr>" +
                                            "<td class='inputRow'>" +
                                                "<div class='col-xs-10 col-xs-offset-1'>" +
                                                    "<div class='row'>" +
                                                        "<div class='col-xs-8'>" +
                                                            "<div class='form-group'>" +
                                                                "<label for='remedial_action'>" +
                                        "                           Remedial Action" +
                                                                "</label>";

                    string html2 = "</div>" +
                                                        "</div>" +
                                                        "<div class='col-xs-4'>" +
                                                            "<div class='row'>" +
                                                                "<div class='col-xs-12'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='taskDateStart'>" +
                                                                            "Person to action works" +
                                                                        "</label>";

                    string html3 = " </div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                             "<div class='row'>" +
                                                                 "<div class='col-xs-12'>" +
                                                                    "<div class='form-group'>" +
                                                                        "<label for='taskDateStart'>" +
                                                                            "Due date" +
                                                                        " </label>";

                    string html4 = "</div>" +
                                                                "</div>" +
                                                            "</div>" +
                                                        "</div>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</td>" +
                                        "</tr>" +
                                    "</table>";


                    TextBox actionText_TextBox = new TextBox();
                    actionText_TextBox.CssClass = "form-control";
                    actionText_TextBox.TextMode = TextBoxMode.MultiLine;
                    actionText_TextBox.ID = "action_textbox_" + questionReference;
                    actionText_TextBox.Rows = 5;

                    RadComboBox actionUser_RadComboBox = new RadComboBox();
                    actionUser_RadComboBox.Skin = "Bootstrap";
                    actionUser_RadComboBox.ID = "action_radcombobox_" + questionReference;
                    actionUser_RadComboBox.Width = new Unit("100%");
                    actionUser_RadComboBox.EnableVirtualScrolling = true;
                    actionUser_RadComboBox.HighlightTemplatedItems = true;
                    actionUser_RadComboBox.Filter = RadComboBoxFilter.Contains;
                    actionUser_RadComboBox.EmptyMessage = "Please type a name or select a user";
                    actionUser_RadComboBox.EnableLoadOnDemand = true;
                    actionUser_RadComboBox.AppendDataBoundItems = true;
                    actionUser_RadComboBox.AllowCustomText = false;
                    actionUser_RadComboBox.Height = new Unit("300");
                    actionUser_RadComboBox.ShowMoreResultsBox = true;
                    actionUser_RadComboBox.ItemsRequested += new RadComboBoxItemsRequestedEventHandler(_additionalQuestions_UserControl.generic_Blank_ItemsRequested);
                    actionUser_RadComboBox.DropDownWidth = new Unit("300");
                    actionUser_RadComboBox.ClientIDMode = ClientIDMode.Predictable;


                    RadDatePicker actionDate_RadDatePicker = new RadDatePicker();
                    actionDate_RadDatePicker.Skin = "Bootstrap";
                    actionDate_RadDatePicker.Width = new Unit("100%");
                    actionDate_RadDatePicker.ID = "action_raddatepicker_" + questionReference;
                    actionDate_RadDatePicker.MinDate = DateTime.Now;


                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html1));
                    _additionalQuestions_PlaceHolder.Controls.Add(actionText_TextBox);
                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html2));
                    _additionalQuestions_PlaceHolder.Controls.Add(actionUser_RadComboBox);
                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html3));
                    _additionalQuestions_PlaceHolder.Controls.Add(actionDate_RadDatePicker);
                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html4));
                }
                else
                {

                    // Already has an action

                    string html1 = "<table class='table table-condensed table-bordered table-addQ' style='margin-top: -6px;'>" +
                                        "<tr>" +
                                            "<th width='70%'>Action <h5 style='float: right; display:inline-block; font-size: 16px;'><small class='info'>To edit this action, please use the \"Action\" tab within the main incident report</small></h5></th>" +
                                            "<th width='15%' class=' text-center'>Current Status</th>" +
                                            "<th width='15%;' class=' text-center'>Due Date / By Whom</th>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td>" + actionText  + "" +
                                            "<br />" +
                                            "<br />" +
                                            "</td>" +
                                            "<td align='center'>" +
                                                "<div class='taskStatGroup'>" +
                                                    "<div class='taskStat" + ASNETNSP.SharedComponants.returnTaskStatus(actionStatus, "") + " NoWidth'>" + ASNETNSP.SharedComponants.returnTaskStatus(actionStatus, "") + "</div>" +
                                                "</div>" +
                                            "</td>" +
                                            "<td align='center'><strong>" + ASNETNSP.Formatting.FormatDateTime(action_due_date, 1) + "</strong>" +
                                                "<br>" +
                                                ASNETNSP.UserControls.GetUser(action_assigned_to, _corpCode) + "</td>" +
                                        "</tr>" +
                                "</table>";

                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html1));
                }



            }


        }



    }


    private void InitiateReport()
    {

        if (_hasData)
        {
                
            if (_IncidentType == IncidentType.Main && !_isSignedOff)
            {

                string html = "<div class='row'>" +
                              "<div class='col-xs-12 text-right'>" +
                              "<a href='additionaloptions/addopts_create.aspx?accb=" + _incidentCentre + "&recordreference=" + _incidentReference + "' class='btn btn-success' style='margin-bottom:10px;'><i class='fa fa-plus'></i> Edit Additional Questions</a><br />" +
                              "</div>" +                                
                              "</div>";

                _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html));
            }

            foreach (DataRow row in _controlData.Rows)
            {
                string questionReference = row.Field<string>(0);
                string questionTitle = row.Field<string>(1);
                string questionDescription = row.Field<string>(2);
                string questionType = row.Field<string>(3);
                bool triggerActions = row.Field<bool>(4);
                string questionAnswer = row.Field<string>(5);
                string mandatory = row.Field<string>(6);
                string parentReference = row.Field<string>(7);

                string actionText = row.Field<string>(8);
                string actionStatus = row.Field<string>(9);
                string action_assigned_to = row.Field<string>(10);
                DateTime action_due_date = row.Field<DateTime>(11);

                bool isDropDown = questionType.Contains("DD_") == true ? true : false;
                bool isTextItem = questionType.Contains("Section") || questionType.Contains("Title") == true ? true : false;

                LiteralControl tableTop_Html = new LiteralControl();
                LiteralControl tableBottom_Html = new LiteralControl();


                string formGroup = "form-group";
                if (questionType == "YN" || questionType == "YNNA" || questionType == "YNU")
                {
                    formGroup = "";
                }

                if (isTextItem)
                {
                    if (questionType.Contains("Section"))
                    {
                        tableTop_Html.Text = "<h2 style='margin-bottom:15px; margin-top:15px;'>" + questionTitle + "</h2>";
                    }
                    else if (questionType.Contains("Title"))
                    {
                        tableTop_Html.Text = "<h3 style='margin-bottom:10px; margin-top:10px;'>" + questionTitle + "</h2>";
                    }



                    tableBottom_Html.Text = "";

                    _additionalQuestions_PlaceHolder.Controls.Add(tableTop_Html);
                }
                else
                {

                    string parentClass = parentReference.Length > 0 ? "question_" + parentReference : "";
                    string parentHide = parentReference.Length > 0 && questionAnswer.Length == 0 ? "style='display:none;'" : "";

                    if (questionDescription != "")
                    {
                        questionTitle = questionTitle + " <br> <small class='info'>" + questionDescription + "</small>";
                    }

                    tableTop_Html.Text = " <table class='table table-condensed table-bordered table-addQ " + parentClass + "' " + parentHide + "> " +
                    "<tr>" +
                        " <th width='35%'>" + questionTitle + "</th >" +
                              "<td class='inputRow'>" +
                                 " <div class='" + formGroup + "'>";

                    tableBottom_Html.Text = " </div> " +
                               " </td>" +
                           " </tr>" +
                       " </table>";

                    _additionalQuestions_PlaceHolder.Controls.Add(tableTop_Html);

                    string answer = "";

                    switch (questionType)
                    {

                        case "Free Text":

                            answer = Formatting.FormatTextReplaceReturn(questionAnswer);

                            break;

                        case "Large Text":
                            answer = Formatting.FormatTextReplaceReturn(questionAnswer);

                            break;

                        case "Numeric":
                            answer = Formatting.FormatTextReplaceReturn(questionAnswer);

                            break;

                        case "Date":
                            answer = Formatting.FormatDateTime(questionAnswer, 1);

                            break;

                        case "Time":
                            
                            DateTime DateTimeValue = Convert.ToDateTime(questionAnswer);

                            answer = DateTimeValue.ToShortTimeString();

                            break;

                        case "YN":
                        case "YNNA":
                        case "YNU":

                            switch (questionAnswer)
                            {
                                case "Y":
                                    answer = "Yes";
                                    break;
                                case "N":
                                    answer = "No";
                                    break;
                                case "NA":
                                    answer = "Not Applicable";
                                    break;
                                case "U":
                                    answer = "Unknown";
                                    break;
                                default:
                                    answer = "Not Specified";
                                    break;
                            }


                            break;



                        default:


                            if (isDropDown)
                            {

                                //ReturnDropDownItems
                                string dropDownReference = questionType.Replace("DD_", "");

                                answer = ReturnSelectedDropDownItem(dropDownReference, questionAnswer);


                            }

                            break;
                    }



                    _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(answer));

                }



                _additionalQuestions_PlaceHolder.Controls.Add(tableBottom_Html);




                if (triggerActions)
                {

     

                    string html1 = "<table class='table table-condensed table-bordered table-addQ' style='margin-top: -6px;'>" +
                                        "<tr>" +
                                            "<th width='70%'>Action <h5 style='float: right; display:inline-block; font-size: 16px;'><small class='info'>To edit this action, please use the \"Action\" tab within the main incident report</small></h5></th>" +
                                            "<th width='15%' class=' text-center'>Current Status</th>" +
                                            "<th width='15%;' class=' text-center'>Due Date / By Whom</th>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td>" + actionText + "" +
                                            "<br />" +
                                            "<br />" +
                                            "</td>" +
                                            "<td align='center'>" +
                                                "<div class='taskStatGroup'>" +
                                                    "<div class='taskStat" + ASNETNSP.SharedComponants.returnTaskStatus(actionStatus, "") + " NoWidth'>" + ASNETNSP.SharedComponants.returnTaskStatus(actionStatus, "") + "</div>" +
                                                "</div>" +
                                            "</td>" +
                                            "<td align='center'><strong>" + ASNETNSP.Formatting.FormatDateTime(action_due_date, 1) + "</strong>" +
                                                "<br>" +
                                                ASNETNSP.UserControls.GetUser(action_assigned_to, _corpCode) + "</td>" +
                                        "</tr>" +
                                "</table>";

                        _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html1));
      



                }


            }

        }
        else
        {

            if (_IncidentType == IncidentType.Main)
            {

                string html = "<div class='row'>" +
                              "<div class='col-xs-12 text-right'>" +
                              "<a href='additionaloptions/addopts_create.aspx?accb=" + _incidentCentre + "&recordreference=" + _incidentReference + "' class='btn btn-success' style='margin-bottom:10px;'><i class='fa fa-plus'></i> Add Additional Questions</a><br />" +
                              "</div>" +     
                              "<div class='col-xs-12'>" +
                              "<div class='alert alert-danger'>" +
                              "<div class='row'>" +
                              "<div class='col-xs-12'>" +
                              "<p><strong>Warning: This section needs to be completed.</strong></p>" +
                              "<p>This \"Additional Questions\" section needs to be completed. Please select the \"Add\" button to answer any additional questions.</p>" +
                              "</div>" +
                              "</div>" +
                              "</div>" +
                              "</div>" +
                              "</div>";

                _additionalQuestions_PlaceHolder.Controls.Add(new LiteralControl(html));
            }

        }


    }

    private string ReturnSelectedDropDownItem(string dropDownReference, string optID)
    {

        string returnValue = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.AdditionalQuestions_DropDown", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@module", ReturnModuleCodeFromType_CustomLists(_IncidentType)));
                cmd.Parameters.Add(new SqlParameter("@dropdownReference", dropDownReference));
                cmd.Parameters.Add(new SqlParameter("@opt_value", optID));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    returnValue = Convert.ToString(objrs["opt_value"]);
                }
                else
                {
                    returnValue = "ERR";
                }

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnValue;
    }

    private DataTable ReturnDropDownItems(string dropDownReference)
    {

        DataTable returnTable = new DataTable();

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.AdditionalQuestions_DropDown", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@module", ReturnModuleCodeFromType_CustomLists(_IncidentType)));
                cmd.Parameters.Add(new SqlParameter("@dropdownReference", dropDownReference));

                objrs = cmd.ExecuteReader();

                returnTable.Load(objrs);

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnTable;

    }





    public void SaveData()
    {

        

        
            //TextBoxes
            var textBox_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(TextBox));

            //Get information of the text box and add it to the datatable.
            foreach (var control in textBox_List)
            {
                TextBox currentControl = (TextBox)control;

                if (currentControl.ID.Contains("action_")){
                    string currentID = currentControl.ID.Replace("action_textbox_", "");
                    string currentValue = currentControl.Text;

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["action_text"] = currentValue;
                }
                else
                {
                    string currentID = currentControl.ID.Replace("control_", "");
                    string currentValue = currentControl.Text;

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["question_answer"] = currentValue;
                }

               

            }


            //Radio Boxes
            var Radio_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(RadioButton));

            foreach (var control in Radio_List)
            {
                RadioButton currentControl = (RadioButton)control;

                if (currentControl.Checked)
                {
                    string currentID = currentControl.ID.Replace("control_", "");
                    currentID = currentID.Substring(0, currentID.LastIndexOf("_") + 0);

                    string currentValue = currentControl.ID.Replace("control_" + currentID + "_", "");



                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["question_answer"] = currentValue;


                }

            }

            //Dropdown Lists
            var DropDown_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(DropDownList));
            foreach (var control in DropDown_List)
            {
                DropDownList currentControl = (DropDownList)control;

                string currentID = currentControl.ID.Replace("control_", "");
                string currentValue = currentControl.SelectedValue;

                // update datatable
                DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                row["question_answer"] = currentValue;

            }


            // Numeric
            var NumericTextBox_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(RadNumericTextBox));
            foreach (var control in NumericTextBox_List)
            {
                RadNumericTextBox currentControl = (RadNumericTextBox)control;

                string currentID = currentControl.ID.Replace("control_", "");
                string currentValue = currentControl.Text;

                // update datatable
                DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                row["question_answer"] = currentValue;

            }


            // Time
            var TimePicker_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(RadTimePicker));
            foreach (var control in TimePicker_List)
            {
                RadTimePicker currentControl = (RadTimePicker)control;

                string currentID = currentControl.ID.Replace("control_", "");
                string currentValue = currentControl.SelectedDate.ToString();

                // update datatable
                DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                row["question_answer"] = currentValue;

            }


            // Date
            var DatePicker_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(RadDatePicker));
            foreach (var control in DatePicker_List)
            {
                RadDatePicker currentControl = (RadDatePicker)control;

                if (currentControl.ID.Contains("action_"))
                {
                    string currentID = currentControl.ID.Replace("action_raddatepicker_", "");
                    string currentValue = currentControl.SelectedDate.ToString();

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["action_due_date"] = currentValue;
                }
                else
                {
                    string currentID = currentControl.ID.Replace("control_", "");
                    string currentValue = currentControl.SelectedDate.ToString();

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["question_answer"] = currentValue;
                }


            }


            //RadComboBox
            var RadComboBox_List = GetAllControls(_additionalQuestions_PlaceHolder, typeof(RadComboBox));
            foreach (var control in RadComboBox_List)
            {
                RadComboBox currentControl = (RadComboBox)control;

                if (currentControl.ID.Contains("action_"))
                {
                    string currentID = currentControl.ID.Replace("action_radcombobox_", "");
                    string currentValue = currentControl.SelectedValue;

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["action_assigned_to"] = currentValue;
                }
                else
                {
                    string currentID = currentControl.ID.Replace("control_", "");
                    string currentValue = currentControl.SelectedValue;

                    // update datatable
                    DataRow row = _controlData.Select("question_ref='" + currentID + "'").FirstOrDefault();
                    row["question_answer"] = currentValue;
                }


            }


            // Update Ocado's Question
            if (_corpCode == "065912" && _IncidentType == IncidentType.Injury && _IncidentStatus == Status.Edit)
            {
                DataRow monthRow = _controlData.Select("question_ref='8'").FirstOrDefault();
                DataRow dateRow = _controlData.Select("question_ref='7'").FirstOrDefault();


                var startDate = Convert.ToDateTime(dateRow["question_answer"]);
                var differenceInMonths =
                    ((DateTime.Now.Year - startDate.Year) * 12) + DateTime.Now.Month - startDate.Month;


                monthRow["question_answer"] = differenceInMonths;
            }

       

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {
             

                foreach (DataRow row in _controlData.Rows)
                {


                    // Assign variables based on the DataTable column position. This may change.
                    string currentQuestionReference = row.Field<string>(0);
                    string currentValue = row.Field<string>(5);

                    string actionText = row.Field<string>(8);
                    DateTime actionDueDate = row.Field<DateTime>(11);
                    string actionAssignedUser = row.Field<string>(10);



                    // First we need to wipe any data that is linked to an item that has a parent ref, where its parent is not "N"
                    //DataRow[] itemsWithParent = _controlData.Select("parent_ref = '" + currentQuestionReference + "'");
                    //foreach (DataRow rowParent in itemsWithParent)
                    //{
                    //    rowParent["question_answer"] = "";

                    //}

                    //DataRow rowContainsParent = _controlData.Select("parent_ref = '" + currentQuestionReference + "'").FirstOrDefault();
                    //if(rowContainsParent != null)
                    //{
                    //    rowContainsParent["question_answer"] = "";
                    //}


                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.AdditionalQuestions_Update", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                    cmd.Parameters.Add(new SqlParameter("@module", ReturnModuleCodeFromType(_IncidentType)));
                    cmd.Parameters.Add(new SqlParameter("@question_reference", currentQuestionReference));
                    cmd.Parameters.Add(new SqlParameter("@question_answer", Formatting.FormatTextInput(currentValue)));
                    cmd.Parameters.Add(new SqlParameter("@current_user", _currentUser));
                    cmd.Parameters.Add(new SqlParameter("@action_text", actionText));
                    
                    cmd.Parameters.Add(new SqlParameter("@action_assigned_to", actionAssignedUser));

                    if(actionDueDate == DateTime.MinValue)
                    {
                        cmd.Parameters.Add(new SqlParameter("@action_due_date", DBNull.Value));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@action_due_date", actionDueDate.ToUniversalTime()));
                    }


                    objrs = cmd.ExecuteReader();

                    cmd.Dispose();
                    objrs.Dispose();

                }
            }
            catch
            {
                throw;
            }


        }


    }



    private string ReturnModuleCodeFromType(IncidentType type)
    {

        string returnValue = "";

        switch (type)
        {
            case IncidentType.Injury:
                returnValue = "INJ";
                break;
            case IncidentType.NearMiss:
                returnValue = "NM";
                break;
            case IncidentType.PropertyDamage:
                returnValue = "EQUIP";
                break;
            case IncidentType.Illness:
                returnValue = "ID";
                break;
            case IncidentType.Violence:
                returnValue = "VI";
                break;
            case IncidentType.DangerousOccurrence:
                returnValue = "DO";
                break;
            case IncidentType.Main:
                returnValue = "ACC";
                break;
            case IncidentType.Investigation:
                returnValue = "INV";
                break;
            case IncidentType.Defined:
                returnValue = "DEF";
                break;
            default:
                break;
        }


        return returnValue;
    }

    private string ReturnModuleCodeFromType_CustomLists(IncidentType type)
    {

        string returnValue = "";

        switch (type)
        {
            case IncidentType.Injury:
                returnValue = "injury";
                break;
            case IncidentType.NearMiss:
                returnValue = "nearmiss";
                break;
            case IncidentType.PropertyDamage:
                returnValue = "equipment";
                break;
            case IncidentType.Illness:
                returnValue = "illness";
                break;
            case IncidentType.Violence:
                returnValue = "violence";
                break;
            case IncidentType.DangerousOccurrence:
                    returnValue = "dangerousoccurrence";
                break;
            case IncidentType.Main:
                returnValue = "acc";
                break;
            case IncidentType.Investigation:
                returnValue = "investigation";
                break;
            case IncidentType.Defined:
                returnValue = "defined";
                break;
            default:
                break;
        }

        return returnValue;
    }



    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5,
        Main = 6,
        Investigation = 7,
        Defined = 8

    }

    public enum Status
    {
        Edit = 0,
        Report = 1
    }

    public IEnumerable<Control> GetAllControls(Control control, Type type)
    {
        var controls = control.Controls.Cast<Control>();

        return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                  .Concat(controls)
                                  .Where(c => c.GetType() == type);
    }

}