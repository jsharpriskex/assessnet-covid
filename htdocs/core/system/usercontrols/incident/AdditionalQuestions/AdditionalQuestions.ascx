﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AdditionalQuestions.ascx.cs" Inherits="AdditionalQuestions_UserControl" %>


<asp:HiddenField ID="currentIDOfAdditionalQuestion" runat="server" />

<div class="row">
    <div class="col-xs-12">
            <asp:CustomValidator runat="server" ID="additionalQuestions_Validator" ValidationGroup="additionalQuestions" ClientValidationFunction="ValidateAdditionalControl"></asp:CustomValidator>
            <asp:PlaceHolder ID="additionalQuestions_PlaceHolder" runat="server">

            </asp:PlaceHolder>
    </div>
</div>

<%--<div class="row">
    <div class="col-xs-12">
        <div class="row">
            
            <h2 style="margin-bottom:15px;">Employee Details</h2>
            <h4 style="font-weight:100; margin-bottom:10px;">This is a title for employee details</h4>

            <table class='table table-condensed table-bordered table-addQ'>
                <tr>
                    <th width='35%'>What Happened After Treatment?</th>
                    <td>
                        <div class='form-group input-sm'>
                             <asp:DropDownList CssClass='form-control' runat='server' Width='50%'></asp:DropDownList>
                        </div>     
                    </td>
                </tr>
            </table>

            <table class="table table-condensed table-bordered table-addQ">
                <tr>
                    <th width="35%">Employee Start Date</th>
                    <td>
                        <div class="form-group input-sm">
                             <telerik:RadDatePicker runat="server" Skin="Bootstrap" Width="50%"></telerik:RadDatePicker>
                        </div>     
                    </td>
                </tr>
            </table>

             <table class="table table-condensed table-bordered table-addQ">
                <tr>
                    <th width="35%">Length of Service</th>
                    <td>
                        <div class="form-group input-sm">
                             <telerik:RadNumericTextBox runat="server" Skin="Bootstrap" Width="50%" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                        </div>     
                    </td>
                </tr>
            </table>

            <table class="table table-condensed table-bordered table-addQ">
                <tr>
                    <th width="35%">What is the staff ID?</th>
                    <td>
                        <div class="form-group input-sm">
                             <asp:TextBox runat="server" CssClass="form-control">
                             </asp:TextBox>
                        </div>     
                    </td>
                </tr>
            </table>

             <table class="table table-condensed table-bordered table-addQ">
                <tr>
                    <th width="35%">Briefly describe the employees current views on brexit.</th>
                    <td>
                        <div class="form-group col-xs-12">
                             <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Rows="4">
                             </asp:TextBox>
                        </div>     
                    </td>
                </tr>
            </table>

            <table class="table table-condensed table-bordered table-addQ" >
                <tr>
                    <th width="35%">What time does the person usually have breakfast?</th>
                    <td>
                        <div class="form-group input-sm">
                           <telerik:RadTimePicker runat="server" Skin="Bootstrap"  ></telerik:RadTimePicker>
                        </div>     
                    </td>
                </tr>
            </table>

            <table class="table table-condensed table-bordered table-addQ" >
                <tr>
                    <th width="35%">Do you know what I mean?</th>
                    <td>
                        <div class="parentRadioGroup">
                            <div class='radioGroup'>
                                <label class='green white' />
                                    <asp:RadioButton runat='server' ID='Yes_RB' GroupName='1'/>
                                    <span>Yes</span>
                            </div>
                            <div class="radioGroup">
                                <label class="green white" />
                                    <asp:RadioButton runat="server" ID="RadioButton1" GroupName='1'/>
                                    <span>No</span>
                            </div>
                            <div class="radioGroup">
                                <label class="green white" />
                                    <asp:RadioButton runat="server" ID="RadioButton2" GroupName='1'/>
                                    <span>Not Applicable</span>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <table class='table table-condensed table-bordered table-addQ' style='margin-top: -6px;'>

                    <tr>
                        <th width='70%'>Action</th>
                        <th width='15%' class=' text-center'>Current Status</th>
                        <th width='15%;' class=' text-center'>Due Date / By Whom</th>
                    </tr>

                    <tr>
                        <td>asdasd</td>
                        <td align='center'>
                            <div class='taskStatGroup'>
                                <div class='taskStatPending NoWidth'>Pending</div>
                            </div>
                        </td>
                        <td align='center'><strong>20/06/2018 </strong>
                            <br>
                            K.Wright</td>
                    </tr>

            </table>


            <table class='table table-condensed table-bordered' style='margin-top: -6px;'>
                <tr>
                    <td>
                        <div class='col-xs-10 col-xs-offset-1'>

                        
                        <div class='row'>
                            <div class='col-xs-8'>
                                <div class='form-group'>
                                    <label for='remedial_action'>
                                        Remedial Action
                                    </label>
                                    <textarea name='actionDetails' rows='6' cols='20' id='actionDetails' class='form-control' placeholder=''></textarea>
                                </div>
                            </div>
                            <div class='col-xs-4'>
                               <div class='row'>
                                    <div class='col-xs-12'>
                                        <div class='form-group'>
                                            <label for='taskDateStart'>
                                            Person to action works
                                                </label>
                                            <telerik:RadComboBox runat='server' Skin='Bootstrap' Width='100%'></telerik:RadComboBox>
                                        </div>
                                    </div>
                                </div>
                                <div class='row'>
                                    <div class='col-xs-12'>
                                        <div class='form-group'>
                                            <label for='taskDateStart'>
                                            Due date
                                                </label>
                                            <telerik:RadDatePicker runat='server' Skin='Bootstrap' Width='100%'></telerik:RadDatePicker>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </div>
                    </td>
                </tr>
            </table>

        </div>
    </div>
</div>--%>



<!-- alert -->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Invalid Data Entry</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        Please enter valid information within the fields that have been highlighted in <span style="color: #c00c00; font-weight: bold;">red</span>.
                    </div>
                </div>
                <div class="row" id="alertExtraText_Row" style="display: none;">
                    <div class="col-xs-12">
                        <br/>
                        <br/>
                        <p><strong>Additional Information:</strong></p>
                        <p id="alertExtraText"></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->

<script type="text/javascript">

   




</script>
