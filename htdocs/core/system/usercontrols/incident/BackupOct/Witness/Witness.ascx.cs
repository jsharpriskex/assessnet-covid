﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class Witness_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public WitnessList CurrentWitnessList;
    //public PersonDetails.Type UserType;
    public WitnessList.IncidentType IncidentType;
    public WitnessList.Status Status;

    private bool ReturnData;
  

    public event EventHandler SaveOrEditButton_Click;

    private bool FormDataRequired = false;

    public string PersonStatusText;
    public string AdvancedContactDetails;
    public string AdvancedContactDetails_StaffID;
    public string AdvancedContactDetails_StaffID_Description;
    public string PersonDetailsUnknown;
    public string PersonDetailsAddressExternal;

    public bool ShowData;

    public TranslationServices ts;

    public DateTime IncidentDateTime;

    public event EventHandler UpdateAllTabs;

    protected void Page_Load(object sender, EventArgs e)
    {


        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************

        
        //REMOVE
        //ts = new TranslationServices(_corpCode, "en-gb", "incident");

        CurrentWitnessList = new WitnessList(this);
        
        if (ReturnData)
        {
            _ReturnWitnessList();
        }

        if (IncidentType == WitnessList.IncidentType.Main)
        {
            buttonMain_Div.Visible = true;
            buttonType_Div.Visible = false;
            mainAlertText_Div.Visible = true;
        }
        else
        {
            buttonMain_Div.Visible = false;
            buttonType_Div.Visible = true;
            mainAlertText_Div.Visible = false;
        }
        

    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        CurrentWitnessList = new WitnessList(this);

        if (ShowData)
        {
            _ReturnWitnessList();
        }

    }
    

    public void ReturnWitnessList()
    {
        ReturnData = true;
 
    }

    private void _ReturnWitnessList()
    {
        CurrentWitnessList.ReturnWitnessList();
        ReturnData = false;

    }



    

    //Button Handlers



    protected void addLostTime_Button_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "ShowWitnessDetails(); $('#WitnessModal').modal('show');", true);
        CurrentWitnessList.ReturnIndividualWitness("", true);
    }

    protected void saveWitness_Button_Click(object sender, EventArgs e)
    {
        CurrentWitnessList.currentLostTime.SaveWitnessData();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "saveinfo", "$('#WitnessModal').modal('hide');", true);


        _ReturnWitnessList();
        witnessList_UpdatePanel.Update();
    }

    protected void lostTimeList_Repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string currentID = e.CommandArgument.ToString();

        switch (e.CommandName)
        {
            case "EditLTA":
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "toggleinfo", "ShowWitnessDetails(); $('#WitnessModal').modal('show');", true);
                CurrentWitnessList.ReturnIndividualWitness(currentID, true);
                witnessList_UpdatePanel.Update();
                break;

            case "RemoveLTA":
                CurrentWitnessList.RemoveIndividualWitness(currentID);
                _ReturnWitnessList();
                witnessList_UpdatePanel.Update();
                break;

            case "ViewWitness" :
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "witnessReportModal", "$('#witnessReportModal').modal('show');", true);
                CurrentWitnessList.ReturnIndividualWitness(currentID, false);
                break;
        }


    }

    protected void lostTimeList_Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            LinkButton editLTA_LinkButton = (LinkButton) e.Item.FindControl("editLTA_LinkButton");
            LinkButton removeLTA_LinkButton = (LinkButton) e.Item.FindControl("removeLTA_LinkButton");
            string witLanguage = DataBinder.Eval(e.Item.DataItem, "display_language").ToString() ?? "";

            HtmlGenericControl repeaterInfo_div = (HtmlGenericControl) e.Item.FindControl("repeaterInfo_div");
            HtmlGenericControl repeaterInfoHashed_Div = (HtmlGenericControl) e.Item.FindControl("repeaterInfoHashed_Div");
            ((Literal)e.Item.FindControl("itemLanguageBadge")).Text = ts.GetRecordMarker(witLanguage, ts.LanguageCode()) + " ";

            HtmlButton noOptions_Button = (HtmlButton) e.Item.FindControl("noOptions_Button");

            if (!IsAdmin)
            {
                editLTA_LinkButton.Visible = false;
                removeLTA_LinkButton.Visible = false;
                noOptions_Button.Visible = true;
                repeaterInfoHashed_Div.Visible = true;
            }
            else
            {
                noOptions_Button.Visible = false;
                repeaterInfo_div.Visible = true;
            }
        }

        


    }
}


public class WitnessList
{

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _subIncidentReference;
    private bool _isAdmin;

    private IncidentType _IncidentType;
    private Status _incidentStatus;

    private bool _hasData;

    private Witness_UserControl _witness_UserControl;

    private Repeater _witnessList_Repeater;
    private HtmlGenericControl _witnessAlert_Div;

    private UpdatePanel _witnessList_UpdatePanel;
    private UpdatePanel _witnessAlert_UpdatePanel;

    private HtmlGenericControl _editContent;
    private HtmlGenericControl _reportContent;

    public Witness currentLostTime;

    public WitnessList(Witness_UserControl lostTime_UserControl)
    {
        _witness_UserControl = lostTime_UserControl;


        _corpCode = _witness_UserControl._corpCode;
        _currentUser = _witness_UserControl._currentUser;
        _isAdmin = _witness_UserControl.IsAdmin;
        _incidentCentre = _witness_UserControl.IncidentCentre;
        _incidentReference = _witness_UserControl.IncidentReference;
        _subIncidentReference = _witness_UserControl.IncidentSubReference;
        _IncidentType = _witness_UserControl.IncidentType;
        
        _witnessAlert_Div = (HtmlGenericControl)_witness_UserControl.FindControl("witnessAlert_Div");
        _incidentStatus = _witness_UserControl.Status;
        _editContent = (HtmlGenericControl)_witness_UserControl.FindControl("editContent");
        _reportContent = (HtmlGenericControl)_witness_UserControl.FindControl("reportContent");

        _witnessList_UpdatePanel = (UpdatePanel)_witness_UserControl.FindControl("witnessList_UpdatePanel");
        _witnessAlert_UpdatePanel = (UpdatePanel)_witness_UserControl.FindControl("witnessAlert_UpdatePanel");




        if (_incidentStatus == Status.Edit)
        {
            _editContent.Visible = true;
            _witnessList_Repeater = (Repeater)_witness_UserControl.FindControl("lostTimeList_Repeater");
        }
        else if (_incidentStatus == Status.Report)
        {
            _reportContent.Visible = true;
            _witnessList_Repeater = (Repeater)_witness_UserControl.FindControl("lostTimeListReport_Repeater");
        }

        currentLostTime = new Witness(this);

    }


    // Return current list of list time options

    public void ReturnWitnessList()
    {

    


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Witness_Display_All ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));


                objrs = cmd.ExecuteReader();

                _witnessList_Repeater.DataSource = objrs;
                _witnessList_Repeater.DataBind();

                objrs.Dispose();
                cmd.Dispose();


                if(_witnessList_Repeater.Items.Count == 0)
                {
                    _witnessAlert_Div.Visible = true;
                    _witnessList_Repeater.Visible = false;
                }
                else
                {
                    _witnessAlert_Div.Visible = false;
                    _witnessList_Repeater.Visible = true;
                }


            }
            catch (Exception)
            {
                throw;
            }

        }


        _witnessList_UpdatePanel.Update();
        _witnessAlert_UpdatePanel.Update();

    }

  

    public void ReturnIndividualWitness(string witID, bool isEdit)
    {
        currentLostTime.ReturnWitnessData(witID, isEdit);
    }


    public void RemoveIndividualWitness(string witID)
    {
        currentLostTime.RemoveWitness(witID);
    }

    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5,
        Main = 6,
        Investigation = 7,
        Defined = 8
    }

    public enum Status
    {
        Edit = 0,
        Report = 1
    }


    public class Witness
    {

        private Witness_UserControl _witness_UserControl;
        private WitnessList _witnessList;

        private string _corpCode;
        private string _currentUser;
        private string _yourAccessLevel;

        private string _incidentCentre;
        private string _incidentReference;
        private string _subIncidentReference;
        private bool _isAdmin;

        private bool _hasData;

        private HiddenField _witnessID_HiddenField;
        private HiddenField _lostTimeSubRef_HiddenField;
        private Label _lostTimeID_Label;
        private UpdatePanel _witnessModal_UpdatePanel;
        private UpdatePanel _witnessReportModal_UpdatePanel;

        private TranslationServices _ts; 


        private string _witID;

        // Witness Details
        private string _witFirstName;
        private string _witSurname;
        private string _witContactNumber;
        private string _witContactEmail;
        private string _witStatus;
        private string _witIDNumber;
        private string _witOccupation;
        private bool _witWholeIncident;
        private string _witWitnessed;


        //Interview Details
        private string _intFirstName;
        private string _intSurname;
        private DateTime _intDateTime;
        private string _intStatement;


        private bool _incidentType_LeadUp;
        private bool _incidentType_Injury;
        private bool _incidentType_Illness;
        private bool _incidentType_DO;
        private bool _incidentType_Property;
        private bool _incidentType_NearMiss;
        private bool _incidentType_Violence;

        private string _recordLanguage;


        // Items on Page

        private TextBox _witFirstName_TextBox;
        private TextBox _witSurname_TextBox;
        private TextBox _witContactNumber_TextBox;
        private TextBox _witEmailAddress_TextBox;
        private DropDownList _witStatus_DropDownList;
        private TextBox _witIDNumber_TextBox;
        private TextBox _witOccupation_TextBox;

        private TextBox _intFirstName_TextBox;
        private TextBox _intSurname_TextBox;

        private TextBox _intStatement_TextBox;

        private RadDateTimePicker _intDateTime_RadDateTimePicker;

        private RadioButton _wholeIncident_Y_RadioButton;
        private RadioButton _wholeIncident_N_RadioButton;


        private CheckBox _incidentType_LeadUp_CheckBox;
        private CheckBox _incidentType_Injury_CheckBox;
        private CheckBox _incidentType_Illness_CheckBox;
        private CheckBox _incidentType_DO_CheckBox;
        private CheckBox _incidentType_Property_CheckBox;
        private CheckBox _incidentType_NearMiss_CheckBox;
        private CheckBox _incidentType_Violence_CheckBox;


        private HtmlGenericControl _incidentType_LeadUp_Label;
        private HtmlGenericControl _incidentType_Injury_Label;
        private HtmlGenericControl _incidentType_Illness_Label;
        private HtmlGenericControl _incidentType_DO_Label;
        private HtmlGenericControl _incidentType_Property_Label;
        private HtmlGenericControl _incidentType_NearMiss_Label;
        private HtmlGenericControl _incidentType_Violence_Label;

        private HtmlGenericControl _attachmentReport_Div;

        private Literal _translationArea;

        private Literal _witnessRelatedFilesList_Literal;


        private Literal _witName_literal;
        private Literal _witStatus_Literal;
        private Literal _witOccupation_Literal;
        private Literal _witContactNumber_Literal;
        private Literal _witContactEmail_Literal;
        private Literal _witWholeIncident_Literal;
        private Literal _witSpecified_Literal;
        private Literal _intName_Literal;
        private Literal _intDateTime_Literal;
        private Literal _intStatement_Literal;
        private Literal _witIDNumber_Literal;

        private Repeater _witnessReportFiles_Repeater;


        private IncidentType _IncidentType;
        private IncidentType _FormIncidentType;

        public Witness(WitnessList witnessList)
        {
            _witnessList = witnessList;
            _witness_UserControl = witnessList._witness_UserControl;
            _witID = "";
            _ts = _witness_UserControl.ts;

            _isAdmin = witnessList._isAdmin;

            _corpCode = _witnessList._corpCode;
            _currentUser = _witnessList._currentUser;
            _isAdmin = _witnessList._isAdmin;
            _incidentCentre = _witnessList._incidentCentre;
            _incidentReference = _witnessList._incidentReference;
            _subIncidentReference = _witnessList._subIncidentReference;
            //_IncidentType = _witnessList._IncidentType;


            if(_subIncidentReference.Contains("INJ"))
            {
                _IncidentType = IncidentType.Injury;
            }
            else if(_subIncidentReference.Contains("ID"))
            {
                _IncidentType = IncidentType.Illness;
            }
            else
            {
                _IncidentType = IncidentType.Main;
            }

            _FormIncidentType = _witnessList._IncidentType;

            _witnessReportFiles_Repeater = (Repeater) _witness_UserControl.FindControl("witnessReportFiles_Repeater");

            _witnessID_HiddenField = (HiddenField) _witness_UserControl.FindControl("witnessID_HiddenField");

            _incidentType_LeadUp_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_LeadUp_CheckBox");
            _incidentType_Injury_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_Injury_CheckBox");
            _incidentType_Illness_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_Illness_CheckBox");
            _incidentType_DO_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_DO_CheckBox");
            _incidentType_Property_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_Property_CheckBox");
            _incidentType_NearMiss_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_NearMiss_CheckBox");
            _incidentType_Violence_CheckBox = (CheckBox) _witness_UserControl.FindControl("incidentType_Violence_CheckBox");

            _incidentType_LeadUp_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_LeadUp_Label");
            _incidentType_Injury_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_Injury_Label");
            _incidentType_Illness_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_Illness_Label");
            _incidentType_DO_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_DO_Label");
            _incidentType_Property_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_Property_Label");
            _incidentType_NearMiss_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_NearMiss_Label");
            _incidentType_Violence_Label = (HtmlGenericControl) _witness_UserControl.FindControl("incidentType_Violence_Label");

            _witnessModal_UpdatePanel = (UpdatePanel) _witness_UserControl.FindControl("witnessModal_UpdatePanel");
            _witnessReportModal_UpdatePanel = (UpdatePanel) _witness_UserControl.FindControl("witnessReportModal_UpdatePanel");


            _witFirstName_TextBox = (TextBox) _witness_UserControl.FindControl("witFirstName_TextBox");
            _witSurname_TextBox = (TextBox) _witness_UserControl.FindControl("witSurname_TextBox");
            _witContactNumber_TextBox = (TextBox) _witness_UserControl.FindControl("witContactNumber_TextBox");
            _witEmailAddress_TextBox = (TextBox) _witness_UserControl.FindControl("witEmailAddress_TextBox");
            _witStatus_DropDownList = (DropDownList) _witness_UserControl.FindControl("witStatus_DropDownList");
            _witIDNumber_TextBox = (TextBox) _witness_UserControl.FindControl("witIDNumber_TextBox");
            _witOccupation_TextBox = (TextBox) _witness_UserControl.FindControl("witOccupation_TextBox");
            _intFirstName_TextBox = (TextBox) _witness_UserControl.FindControl("intFirstName_TextBox");
            _intSurname_TextBox = (TextBox) _witness_UserControl.FindControl("intSurname_TextBox");
            _intStatement_TextBox = (TextBox) _witness_UserControl.FindControl("intStatement_TextBox");
            
            _intDateTime_RadDateTimePicker = (RadDateTimePicker) _witness_UserControl.FindControl("intDateTime_RadDateTimePicker");
            
            
            _wholeIncident_Y_RadioButton = (RadioButton) _witness_UserControl.FindControl("wholeIncident_Y_RadioButton");
            _wholeIncident_N_RadioButton = (RadioButton) _witness_UserControl.FindControl("wholeIncident_N_RadioButton");

            _witnessRelatedFilesList_Literal = (Literal) _witness_UserControl.FindControl("witnessRelatedFilesList_Literal");
            
            _witName_literal = (Literal) _witness_UserControl.FindControl("witName_literal");
            _witStatus_Literal = (Literal) _witness_UserControl.FindControl("witStatus_Literal");
            _witOccupation_Literal = (Literal) _witness_UserControl.FindControl("witOccupation_Literal");
            _witContactNumber_Literal = (Literal) _witness_UserControl.FindControl("witContactNumber_Literal");
            _witContactEmail_Literal = (Literal) _witness_UserControl.FindControl("witContactEmail_Literal");
            _witIDNumber_Literal = (Literal) _witness_UserControl.FindControl("witIDNumber_Literal");
            _witWholeIncident_Literal = (Literal) _witness_UserControl.FindControl("witWholeIncident_Literal");
            _witSpecified_Literal = (Literal) _witness_UserControl.FindControl("witSpecified_Literal");
            _intName_Literal = (Literal) _witness_UserControl.FindControl("intName_Literal");
            _intDateTime_Literal = (Literal) _witness_UserControl.FindControl("intDateTime_Literal");
            _intStatement_Literal = (Literal) _witness_UserControl.FindControl("intStatement_Literal");
            _translationArea = (Literal) _witness_UserControl.FindControl("translationArea");

            _attachmentReport_Div = (HtmlGenericControl) _witness_UserControl.FindControl("attachmentReport_Div");
        }

        private void DisplayWhatWasWitnessed()
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Witness_ReturnIncidentTypes", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@witID", _witID));


                    objrs = cmd.ExecuteReader();


                    if (objrs.HasRows)
                    {
                        objrs.Read();


                        bool hasINJ = Convert.ToString(objrs["INJ_VALUE"]) == "Y";
                        bool hasID = Convert.ToString(objrs["ID_VALUE"]) == "Y";
                        bool hasNM = Convert.ToString(objrs["NM_VALUE"]) == "Y";
                        bool hasDO = Convert.ToString(objrs["DO_VALUE"]) == "Y";
                        bool hasDAM = Convert.ToString(objrs["DAM_VALUE"]) == "Y";
                        bool hasVI = Convert.ToString(objrs["VI_VALUE"]) == "Y";

                        _incidentType_Injury_Label.Visible = hasINJ;
                        _incidentType_Illness_Label.Visible = hasID;
                        _incidentType_DO_Label.Visible = hasDO;
                        _incidentType_Property_Label.Visible  = hasDAM;
                        _incidentType_NearMiss_Label.Visible  = hasNM;
                        _incidentType_Violence_Label.Visible  = hasVI;


                    }

                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }

        }

        private void PopulateInternalVariablesFromDB(string witID)
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[Witness_Display]", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@witID", witID));


                    objrs = cmd.ExecuteReader();


                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _witFirstName = Convert.ToString(objrs["first_name"]);
                        _witSurname = Convert.ToString(objrs["surname"]);
                        _witContactNumber = Convert.ToString(objrs["contact_num"]);
                        _witContactEmail = Convert.ToString(objrs["contact_email"]);
                        _witStatus = Convert.ToString(objrs["wit_status"]);
                        _witIDNumber = Convert.ToString(objrs["extra_info"]);
                        _witOccupation = Convert.ToString(objrs["occupation"]);
                        _witWitnessed = Convert.ToString(objrs["rel_to"]);

                        _witWholeIncident = _witWitnessed.Substring(0, 1) == "1";

                        _intFirstName = Convert.ToString(objrs["interviewer_fname"]);
                        _intSurname = Convert.ToString(objrs["interviewer_sname"]);
                        _intDateTime = Convert.ToDateTime(objrs["interview_date"]);
                        _intStatement = Convert.ToString(objrs["statement_text"]);
                        
                        _recordLanguage = Convert.ToString(objrs["display_language"]);

                        
                        _incidentType_Injury = _witWitnessed.Substring(1, 1) == "1";
                        _incidentType_Illness = _witWitnessed.Substring(2, 1) == "1";
                        _incidentType_DO = _witWitnessed.Substring(3,1) == "1";
                        _incidentType_Property = _witWitnessed.Substring(4, 1) == "1";
                        _incidentType_NearMiss = _witWitnessed.Substring(5, 1) == "1";
                        _incidentType_Violence = _witWitnessed.Substring(6, 1) == "1";
                        _incidentType_LeadUp = _witWitnessed.Substring(7, 1) == "1";
                    }

                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }

        }

        private void PopulateInternalVariablesFromForm()
        {

            _witFirstName = Formatting.FormatTextInput(_witFirstName_TextBox.Text);
            _witSurname = Formatting.FormatTextInput(_witSurname_TextBox.Text);
            _witContactNumber = Formatting.FormatTextInput(_witContactNumber_TextBox.Text);
            _witContactEmail = Formatting.FormatTextInput(_witEmailAddress_TextBox.Text);
            _witStatus = _witStatus_DropDownList.SelectedValue;
            _witIDNumber = Formatting.FormatTextInput(_witIDNumber_TextBox.Text);
            _witOccupation = Formatting.FormatTextInput(_witOccupation_TextBox.Text);
            
            //_witWitnessed = Formatting.FormatTextInput(_witFirstName_TextBox.Text);
            

            _intFirstName = Formatting.FormatTextInput(_intFirstName_TextBox.Text);
            _intSurname = Formatting.FormatTextInput(_intSurname_TextBox.Text);
            _intDateTime = _intDateTime_RadDateTimePicker.SelectedDate ?? DateTime.Now;
            _intStatement = Formatting.FormatTextInput(_intStatement_TextBox.Text);

           
                        

            _witWitnessed = "";
            _witWitnessed += _wholeIncident_Y_RadioButton.Checked ? "1" : "0";
            _witWitnessed += _incidentType_Injury_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_Illness_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_DO_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_Property_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_NearMiss_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_Violence_CheckBox.Checked ? "1" : "0";
            _witWitnessed += _incidentType_LeadUp_CheckBox.Checked ? "1" : "0";

            
            _witWholeIncident = _wholeIncident_Y_RadioButton.Checked;
            _incidentType_LeadUp = _incidentType_LeadUp_CheckBox.Checked;
            _incidentType_Injury = _incidentType_Injury_CheckBox.Checked;
            _incidentType_Illness = _incidentType_Illness_CheckBox.Checked;
            _incidentType_DO = _incidentType_DO_CheckBox.Checked;
            _incidentType_Property = _incidentType_Property_CheckBox.Checked;
            _incidentType_NearMiss = _incidentType_NearMiss_CheckBox.Checked;
            _incidentType_Violence = _incidentType_Violence_CheckBox.Checked;


        }

        private void PopulateFormFromInternalVariables()
        {
            _witFirstName_TextBox.Text = Formatting.FormatTextInputField(_witFirstName);
            _witSurname_TextBox.Text = Formatting.FormatTextInputField(_witSurname);
            _witContactNumber_TextBox.Text = Formatting.FormatTextInputField(_witContactNumber);
            _witEmailAddress_TextBox.Text = Formatting.FormatTextInputField(_witContactEmail);
      
            _witIDNumber_TextBox.Text =Formatting.FormatTextInputField(_witIDNumber) ;
            _witOccupation_TextBox.Text = Formatting.FormatTextInputField(_witOccupation);
            _intFirstName_TextBox.Text = Formatting.FormatTextInputField(_intFirstName);
            _intSurname_TextBox.Text = Formatting.FormatTextInputField(_intSurname);
            _intStatement_TextBox.Text = Formatting.FormatTextInputField(_intStatement);

            _intDateTime_RadDateTimePicker.SelectedDate = _intDateTime;

            DateTime incidentDate =
                Incident.IncidentGeneric.ReturnIncidentDateTime(_corpCode, _incidentCentre, _incidentReference);


            _intDateTime_RadDateTimePicker.MinDate =  new DateTime(incidentDate.Year, incidentDate.Month, incidentDate.Day);
            _intDateTime_RadDateTimePicker.MaxDate = DateTime.Now;
            
            _wholeIncident_Y_RadioButton.Checked = _witWholeIncident;
            _wholeIncident_N_RadioButton.Checked = !_witWholeIncident;
            _incidentType_LeadUp_CheckBox.Checked = _incidentType_LeadUp;
            _incidentType_Injury_CheckBox.Checked = _incidentType_Injury;
            _incidentType_Illness_CheckBox.Checked = _incidentType_Illness;
            _incidentType_DO_CheckBox.Checked = _incidentType_DO;
            _incidentType_Property_CheckBox.Checked = _incidentType_Property;
            _incidentType_NearMiss_CheckBox.Checked = _incidentType_NearMiss;
            _incidentType_Violence_CheckBox.Checked = _incidentType_Violence;

           

        }

        private void PopulateReportFromInternalVariables()
        {


            // If they arent an admin, do not show sensitive info
            if (!_isAdmin)
            {
                _witContactNumber = "*********";
                _witContactEmail = "*********";
                _witStatus = "*********";
                _witOccupation = "*********";
                _witIDNumber = "*********";
            }

            _translationArea.Text = _translationArea.Text = _ts.GetPageMarker(_recordLanguage);


            _witName_literal.Text = _witFirstName + " " + _witSurname;
            _witStatus_Literal.Text = Incident.IncidentGeneric.ReturnAccOptByValue(_corpCode, "acc_status", _witness_UserControl.ts.LanguageCode(), _witStatus);
            _witOccupation_Literal.Text = _witOccupation;
            _witContactNumber_Literal.Text = _witContactNumber;
            _witContactEmail_Literal.Text = _witContactEmail;
            _intName_Literal.Text = _intFirstName + " " + _intSurname;
            _intDateTime_Literal.Text = _intDateTime.ToShortDateString() + " " + _intDateTime.ToShortTimeString();
            _intStatement_Literal.Text = Formatting.FormatTextReplaceReturn(_ts.Translate(_intStatement, _recordLanguage, _incidentCentre, _incidentReference,_subIncidentReference, "wit_statement") );
            _witIDNumber_Literal.Text = _witIDNumber;

            if (_witWholeIncident)
            {
                _witSpecified_Literal.Text = _witness_UserControl.ts.GetResource("lbl_witness_witnessType_whole");
            }
            else
            {
                string partsOfIncident = "";

                partsOfIncident += _incidentType_Injury ? _witness_UserControl.ts.GetResource("lbl_witness_witnessType_injury") : "";
                partsOfIncident += _incidentType_Illness ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_id") + "," : "";
                partsOfIncident += _incidentType_DO ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_do") : "";
                partsOfIncident += _incidentType_Property ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_pdam") : "";
                partsOfIncident += _incidentType_NearMiss ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_nm") : "";
                partsOfIncident += _incidentType_Violence ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_vi") : "";
                partsOfIncident += _incidentType_LeadUp ? ", " + _witness_UserControl.ts.GetResource("lbl_witness_witnessType_LeadUp") : "";

                if (partsOfIncident.Substring(0, 1) == ",")
                {
                    partsOfIncident = partsOfIncident.Substring(1);

                }

                _witSpecified_Literal.Text = partsOfIncident;

               
            }

            SharedComponants.getFilesForOutput(_witnessReportFiles_Repeater, _corpCode, _incidentCentre, _incidentReference, _witID, "");
            _witnessReportFiles_Repeater.DataBind();


            _attachmentReport_Div.Visible =_witnessReportFiles_Repeater.Items.Count > 0;


        }


        private void BlankAllFormInputs()
        {
            _witFirstName_TextBox.Text = null;
            _witSurname_TextBox.Text = null;
            _witContactNumber_TextBox.Text = null;
            _witEmailAddress_TextBox.Text = null;
      
            _witIDNumber_TextBox.Text = null;
            _witOccupation_TextBox.Text = null;
            _intFirstName_TextBox.Text = null;
            _intSurname_TextBox.Text = null;
            _intStatement_TextBox.Text = null;

            _intDateTime_RadDateTimePicker.SelectedDate = null;


            _wholeIncident_Y_RadioButton.Checked = false;

            _wholeIncident_N_RadioButton.Checked = false;
            

            _incidentType_LeadUp_CheckBox.Checked = false;
            _incidentType_Injury_CheckBox.Checked = false;
            _incidentType_Illness_CheckBox.Checked = false;
            _incidentType_DO_CheckBox.Checked = false;
            _incidentType_Property_CheckBox.Checked = false;
            _incidentType_NearMiss_CheckBox.Checked = false;
            _incidentType_Violence_CheckBox.Checked = false;

            _witStatus_DropDownList.SelectedValue = null;
            DateTime incidentDate =
                Incident.IncidentGeneric.ReturnIncidentDateTime(_corpCode, _incidentCentre, _incidentReference);


            _intDateTime_RadDateTimePicker.MinDate =  new DateTime(incidentDate.Year, incidentDate.Month, incidentDate.Day);
            _intDateTime_RadDateTimePicker.MaxDate = DateTime.Now;
        }

        public void ReturnWitnessData(string witID, bool isEdit)
        {
            _witID = witID;

            if (isEdit)
            {
                _witnessID_HiddenField.Value = witID;
                
            
                if(witID != "")
                {
                    PopulateInternalVariablesFromDB(witID);
                    PopulateControls();
                    PopulateFormFromInternalVariables();
                }
                else
                {
                    CreateTempID();

                    PopulateControls();
                    BlankAllFormInputs();
                }

                PopulateDropZone();

            
                DisplayWhatWasWitnessed();

                _witnessModal_UpdatePanel.Update();
            }
            else
            {

                // Is report
                PopulateInternalVariablesFromDB(witID);
                PopulateReportFromInternalVariables();
                _witnessReportModal_UpdatePanel.Update();
            }
            
        }
        public void PopulateControls()
        {

            //Populate Person Affected
            using(DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "acc_status", false, _witness_UserControl.ts.LanguageCode(), _witStatus))
            {

                _witStatus_DropDownList.DataSource = accOptions;
                _witStatus_DropDownList.DataTextField = "opt_value";
                _witStatus_DropDownList.DataValueField = "opt_id";
            

                _witStatus_DropDownList.DataBind();

                _witStatus_DropDownList.Items.Insert(0, new ListItem(_witness_UserControl.ts.Translate("Please Select") + "...", "na"));


                if (_witStatus != "")
                {
                    if (_witStatus_DropDownList.Items.FindByValue(_witStatus) != null)
                    {
                        _witStatus_DropDownList.SelectedValue = _witStatus;
                    }
                }
            

            }

        }

        public void SaveWitnessData()
        {


            _witID = _witnessID_HiddenField.Value;
            PopulateInternalVariablesFromForm();








            // Add Lost Time
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[Witness_Save]", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@witID", _witID));
                    cmd.Parameters.Add(new SqlParameter("@witFirstName", _witFirstName));
                    cmd.Parameters.Add(new SqlParameter("@witSurname", _witSurname));
                    cmd.Parameters.Add(new SqlParameter("@witContactEmail", _witContactEmail));
                    cmd.Parameters.Add(new SqlParameter("@witContactNumber", _witContactNumber));
                    cmd.Parameters.Add(new SqlParameter("@witStatus", _witStatus));
                    cmd.Parameters.Add(new SqlParameter("@witIDNumber", _witIDNumber));
                    cmd.Parameters.Add(new SqlParameter("@witOccupation", _witOccupation));
                    cmd.Parameters.Add(new SqlParameter("@witWitnessed", _witWitnessed));
                    cmd.Parameters.Add(new SqlParameter("@intFirstname", _intFirstName));
                    cmd.Parameters.Add(new SqlParameter("@intSurname", _intSurname));
                    cmd.Parameters.Add(new SqlParameter("@intDateTime", _intDateTime));
                    cmd.Parameters.Add(new SqlParameter("@intStatement", _intStatement));
                    cmd.Parameters.Add(new SqlParameter("@accref", _currentUser));
                    cmd.Parameters.Add(new SqlParameter("@accLang", _ts.LanguageCode()));


                    cmd.ExecuteNonQuery();

                    
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }




            // CLEAR
            BlankAllFormInputs();
            //_lostTimeModal_UpdatePanel.Update();
        }

        public void RemoveWitness(string witID)
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.[Witness_Delete]", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@witID", witID));
                    cmd.Parameters.Add(new SqlParameter("@accRef", _currentUser));

                    objrs = cmd.ExecuteReader();

                    objrs.Dispose();
                    cmd.Dispose();



                }
                catch (Exception)
                {
                    throw;
                }

            }
            
        }


        private void CreateTempID()
        {
            _witID = Guid.NewGuid().ToString("n").Substring(0, 8);
            _witnessID_HiddenField.Value = _witID;
        }

        private void PopulateDropZone()
        {

            _witnessRelatedFilesList_Literal.Text = SharedComponants.getFilesForDropzone(_corpCode, _incidentCentre, _incidentReference, _witID, "all");

        }

    }


}