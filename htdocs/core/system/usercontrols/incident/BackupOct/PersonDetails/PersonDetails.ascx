﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersonDetails.ascx.cs" Inherits="PersonDetails_UserControl" %>


<div id="Parent" style="display:none;" runat="server" ClientIDMode="Static">


<div class="row" id="Unavailable_Parent" runat="server" visible="false" ClientIDMode="Static">
    <div class="col-xs-12">
        <div class="alert alert-warning unavailable_class  alert-sm">
            <div class="row">
                <div class="col-xs-10">
                    <div class="media">
                        <div class="media-left media-middle">
                            <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                        </div>
                        <div class="media-body">
                            <h4 class="media-heading"><%=ts.GetResource("h4_personDetails_detailsUnavailable") %></h4>
                            <p id="NotSelectedText"><%=ts.GetResource("lbl_personDetails_areDetailsUnavailable") %></p>
                            <p id="SelectedText" style="display:none;"><%=ts.GetResource("lbl_personDetails_detailsAreUnavailable") %></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-2 text-right" style="padding-top: 15px;" id="detailsUnavailable_Label" runat="server" ClientIDMode="Static">
                    <label class="switch-sm">
                        <asp:CheckBox runat="server" ID="detailsUnavailable_Checkbox" ClientIDMode="Static" />
                        <span class="slider-sm round" onclick=" Unavailable_ToggleCheckbox();"></span>
                    </label>

                </div>
            </div>
        </div>
    </div>
    
</div>



    <div class="row" id="ContentRow">
<%--        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <div class="panel panel-default panel-subpanel" id="headingOneContainer" runat="server" clientidmode="Static">
                <div class="panel-heading click" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" onclick="headingOne_clicked();">
                    <h4 class="panel-title">
                        <a role="button" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-bars" aria-hidden="true"></i>Person Details

                        </a>
                    </h4>
                </div>--%>
                <div runat="server" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" clientidmode="Static">
                    <div class="panel-body">

                        <div class="row" id="AssociatedAccount_Parent_Div" runat="server" clientidmode="Static">
                            <div class="col-xs-12">
                                <div class="row" id="AssociatedAccount_Parent">
                                    <div class="col-xs-12">
                                        <div class="alert alert-warning AssociatedAccount alert-sm">
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <div class="media">
                                                        <div class="media-left media-middle">
                                                            <i class="fa fa-user fa-3x" aria-hidden="true"></i>

                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading"><%=ts.GetResource("h4_personDetails_useAssociatedAccount") %></h4>
                                                            <p id="AssociatedAccount_NotSelectedText"><%=ts.GetResource("lbl_personDetails_useAssociatedAccount") %></p>
                                                            <p id="AssociatedAccount_SelectedText" style="display: none;"><%=ts.GetResource("lbl_personDetails_useAssociatedAccountInUse") %></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-2 text-right" style="padding-top: 15px;" id="associatedAccount_Label" runat="server" clientidmode="Static">
                                                    <label class="switch-sm">
                                                        <asp:CheckBox runat="server" ID="associatedAccount_Checkbox" ClientIDMode="Static" />
                                                        <span class="slider-sm round" onclick="AssociatedAccount_ToggleCheckbox();"></span>
                                                    </label>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="AssociatedUserParent">
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3">
                                    <div class="form-group">
                                        <label for="reportedBy">
                                            
                                            <%=ts.GetResource("lbl_personDetails_associatedAccount") %>
                                        </label>

                                        <telerik:RadComboBox ID="associatedUser_RadComboBox" runat="server"
                                            Width="100%"
                                            EnableVirtualScrolling="true"
                                            DropDownWidth="500px"
                                            DropDownHeight="300px"
                                            Skin="Bootstrap"
                                            HighlightTemplatedItems="true"
                                            Filter="Contains"
                                            EmptyMessage="Please type a name or select a user"
                                            EnableLoadOnDemand="True"
                                            AppendDataBoundItems="True"
                                            AllowCustomText="false"
                                            Height="300"
                                            ShowMoreResultsBox="true"
                                            OnItemsRequested="generic_Blank_ItemsRequested"
                                            >
                                            <ItemTemplate>

                                                <%# Container.DataItem != null ? DataBinder.Eval(Container.DataItem, "user_details") : DataBinder.Eval(Container, "Text") %>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="BasicInfo">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-6" id="forename_Div" runat="server">

                                            <div class="form-group">
                                                <label for="forename_TextBox"><%=ts.GetResource("lbl_personDetails_forename") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="forename_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="col-xs-6" id="surname_Div" runat="server">

                                            <div class="form-group">
                                                <label for="surname_TextBox"><%=ts.GetResource("lbl_personDetails_surname") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="surname_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-xs-6" id="personStatus_Div" runat="server">

                                            <div class="form-group">
                                                <label for="personStatus_DropDownList"><%=PersonStatusText%></label>
                                                <asp:DropDownList ID="personStatus_DropDownList" runat="server" CssClass="form-control" EnableViewState="true" ClientIDMode="Static"></asp:DropDownList>
                                            </div>

                                        </div>

                                        <div class="col-xs-6" id="occupation_Div" runat="server">

                                            <div class="form-group">
                                                <label for="occupation_TextBox"><%=ts.GetResource("lbl_personDetails_jobTitle") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="occupation_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>


                                        

                                    </div>



                                    <div class="row">

                                       
                                        <div class="col-xs-6" id="gender_Div" runat="server">

                                            <div class="form-group">
                                                <label for="gender_DropDownList"><%=ts.GetResource("lbl_personDetails_gender") %></label>
                                                <asp:DropDownList ID="gender_DropDownList" runat="server" CssClass="form-control" EnableViewState="false" ClientIDMode="Static">
                                                    <asp:ListItem Value="na"></asp:ListItem>
                                                    <asp:ListItem Value="0"></asp:ListItem>
                                                    <asp:ListItem Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                       

                        <div id="addressFull_Div" runat="server">

                            <hr />

                            <div class="row" id="External_Parent" runat="server" visible="false" clientidmode="Static">
                                <div class="col-xs-12">
                                    <div class="alert alert-warning External alert-sm">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <div class="media">
                                                    <div class="media-left media-middle">
                                                        <i class="fa fa-hdd-o fa-3x" aria-hidden="true"></i>

                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><%=ts.GetResource("h4_personDetails_addressIsExternal") %></h4>
                                                        <p id="External_NotSelectedText"><%=ts.GetResource("lbl_personDetails_isAddressExternal") %></p>
                                                        <p id="External_SelectedText" style="display: none;"><%=ts.GetResource("lbl_personDetails_addressIsExternal") %></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 text-right" style="padding-top: 15px;" id="addressExternal_Label" runat="server" clientidmode="Static">
                                                <label class="switch-sm">
                                                    <asp:CheckBox runat="server" ID="addressExternal_CheckBox" ClientIDMode="Static" />
                                                    <span class="slider-sm round" onclick="External_ToggleCheckbox();"></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div id="Address" runat="server">
                                <div class="row">
                                    <div class="col-xs-6" id="street_Div" runat="server">
                                        <div class="form-group">
                                            <label for="street_TextBox"><%=ts.GetResource("lbl_personDetails_street") %></label>
                                            <asp:TextBox runat="server" class="form-control" ID="street_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-xs-6" id="town_Div" runat="server">
                                        <div class="form-group">
                                            <label for="town_TextBox"><%=ts.GetResource("lbl_personDetails_town") %></label>
                                            <asp:TextBox runat="server" class="form-control" ID="town_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-6" id="county_Div" runat="server">
                                        <div class="form-group">
                                            <label for="county_TextBox"><%=ts.GetResource("lbl_personDetails_county") %></label>
                                            <asp:TextBox runat="server" class="form-control" ID="county_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-xs-6" id="postcode_Div" runat="server">
                                        <div class="form-group">
                                            <label for="postcode_TextBox"><%=ts.GetResource("lbl_personDetails_postcode") %></label>
                                            <asp:TextBox runat="server" class="form-control" ID="postcode_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
 

                        </div>





                        <div id="contactInfoAndAge" runat="server">

                            <hr />

                            <div class="row" id="contactDetails_Div" runat="server" visible="false" clientidmode="Static">
                                <div class="col-xs-12">

                                    <div class="row">
                                        <div class="col-xs-6" id="number_Div" runat="server">

                                            <div class="form-group">
                                                <label for="number_TextBox"><%=ts.GetResource("lbl_personDetails_contactNumber") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="number_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>
                                        <div class="col-xs-6" id="email_Div" runat="server">

                                            <div class="form-group">
                                                <label for="email_TextBox"><%=ts.GetResource("lbl_personDetails_contactEmail") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="email_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row" runat="server" id="contactDetails_StaffID_Div" visible="false" clientidmode="Static">
                                        <div class="col-xs-6">

                                            <div class="form-group">
                                                <label for="staffID_TextBox"><%=ts.GetResource("lbl_personDetails_idNumbers") %></label>
                                                <asp:TextBox runat="server" class="form-control" ID="staffID_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row" id="age_Div" runat="server" clientidmode="Static">
                                <div class="col-xs-12">

                                    <div class="row" id="ageInput_Div" runat="server">
                                        <div class="col-xs-6">

                                            <div class="form-group" id="ageUser_Div">
                                                <label for="age_RadNumericTextBox"><%=ts.GetResource("lbl_personDetails_age") %></label>
                                                <telerik:RadNumericTextBox ID="age_RadNumericTextBox" MinValue="0" MaxValue="120" runat="server" Skin="Bootstrap" Width="100%" ShowSpinButtons="true" NumberFormat-DecimalDigits="0" ClientIDMode="Static"></telerik:RadNumericTextBox>
                                            </div>

                                            <div class="form-group" id="dateOfBirth_Div" style="display: none;">
                                                <label for="dateOfBirth_RadDatePicker"><%=ts.GetResource("lbl_personDetails_dateOfBirth") %></label>
                                                <telerik:RadDatePicker ID="dateOfBirth_RadDatePicker" runat="server" Skin="Bootstrap" Width="100%" ClientIDMode="Static">
                                                    <Calendar ShowRowHeaders="false"></Calendar> 
                                                </telerik:RadDatePicker>
                                            </div>

                                        </div>
                                        <div class="col-xs-3">

                                            <div class="alert alert-warning alert-sm" style="height: 65px;" id="dateOfBirth_Alert">
                                                <div class="row">
                                                    <div class="col-xs-8" style="padding-top: 10px;">
                                                        <p><%=ts.GetResource("lbl_personDetails_useDateOfBirth") %></p>
                                                    </div>

                                                    <div class="col-xs-4 text-right">
                                                        <label class="switch-sm" style="margin-top: 10px;">
                                                            <asp:CheckBox runat="server" ID="toggleDateOfBirth_CheckBox" ClientIDMode="Static" />
                                                            <span class="slider-sm round" onclick="ToggleDateOfBirthCheckBox();"></span>
                                                        </label>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>


                        <asp:CustomValidator ID="personDetails_CustomValidator" runat="server" ValidationGroup="PersonDetails" ClientIDMode="Static"></asp:CustomValidator>

                    </div>
                </div>
            </div>



        <%--</div>
    </div>--%>
</div>



<div id="Report" runat="server" ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">

            <div class="alert alert-warning alert-sm" id="personDetailsUnavailable_Report" runat="server" visible="false">
                <div class="row">
                    <div class="col-xs-10">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><%=ts.GetResource("h4_personDetails_detailsUnavailable") %></h4>
                                <p><%=ts.GetResource("lbl_personDetails_detailsAreUnavailable") %></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group" id="associatedUser_Report" runat="server" visible="false">
                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr id="associatedUserDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_associatedAccount") %></th>
                            <td><asp:Literal ID="associatedUser_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="form-group" id="mainData_Report" runat="server">

                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr id="nameDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_name") %></th>
                            <td><asp:Literal ID="name_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="statusDiv" runat="server">
                            <th style="width: 180px;"><%=PersonStatusText%></th>
                            <td><asp:Literal ID="status_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="genderDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_gender") %></th>
                            <td><asp:Literal ID="gender_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="occupationDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_jobTitle") %></th>
                            <td><asp:Literal ID="occupation_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>


                <div class="alert alert-warning alert-sm" id="addressIsStoredExternally_Report" runat="server" visible="false">
                    <div class="row">
                        <div class="col-xs-10">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading"><%=ts.GetResource("h4_personDetails_addressIsExternal") %></h4>
                                    <p><%=ts.GetResource("lbl_personDetails_addressIsExternal") %></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <table class="table table-bordered table-searchstyle01" id="addressTable" runat="server">
                    <tbody>
                        <tr id="streetDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_street") %></th>
                            <td><asp:Literal ID="street_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="townDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_town") %></th>
                            <td><asp:Literal ID="town_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="countyDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_county") %></th>
                            <td><asp:Literal ID="county_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="postCodeDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_postcode") %></th>
                            <td><asp:Literal ID="postCode_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-searchstyle01" id="contactAndAge_Div" runat="server">
                    <tbody>
                        <tr id="numberDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_contactNumber") %></th>
                            <td><asp:Literal ID="number_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="emailDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_contactEmail") %></th>
                            <td><asp:Literal ID="email_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="idNumbersDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_idNumbers") %></th>
                            <td><asp:Literal ID="idNumbers_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="ageReportDiv" runat="server">
                            <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_age") %></th>
                            <td><asp:Literal ID="ageReport_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


<div runat="server" id="editScript">

 <!-- alert -->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id=""><%=ts.GetResource("h4_alert_title") %></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <%=ts.GetResource("lbl_alert_text") %>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>                         
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->

    
<script type="text/javascript">

    
<%--    // ------ VALIDATION
    function <%=this.ID%>_ValidatePersonDetails(sender, args){

        var IsValid = true;

        // Start with associated user, this comes from JSON data.
        if ($("#<%=this.ID%>_associatedUser_RadComboBox").is(":visible")) {
            var associatedUserJSON = $("#<%=this.ID%>_associatedUser_RadComboBox_ClientState").val();

            var IsValidUserDropDown = true;

            if (associatedUserJSON != "" && associatedUserJSON != undefined) {
                var parsedJSON = $.parseJSON(associatedUserJSON);
                console.log(parsedJSON);
                if (parsedJSON.value == undefined) {
                    IsValidUserDropDown = false;
                }
            }
            else {
                IsValidUserDropDown = false
            }

            if (IsValidUserDropDown == false) {
                IsValid = false;
                $("#<%=this.ID%>_associatedUser_RadComboBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_associatedUser_RadComboBox").removeClass("inputError");
            }
            
        }

        // First Name
        if ($("#<%=this.ID%>_forename_TextBox").is(":visible")) {
            var firstName = $("#<%=this.ID%>_forename_TextBox").val();
            if (firstName == "") {
                IsValid = false;
                $("#<%=this.ID%>_forename_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_forename_TextBox").removeClass("inputError");
            }
        }

        // Surname
        if ($("#<%=this.ID%>_surname_TextBox").is(":visible")) {
            var surname = $("#<%=this.ID%>_surname_TextBox").val();
            if (surname == "") {
                IsValid = false;
                $("#<%=this.ID%>_surname_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_surname_TextBox").removeClass("inputError");
            }
        }

        // Occupation
        if ($("#<%=this.ID%>_surname_TextBox").is(":visible")) {
            var occupation = $("#<%=this.ID%>_occupation_TextBox").val();
            if (occupation == "") {
                IsValid = false;
                $("#<%=this.ID%>_occupation_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_occupation_TextBox").removeClass("inputError");
            }
        }

        // ID
        if ($("#<%=this.ID%>_staffID_TextBox").is(":visible")) {
            var staffID = $("#<%=this.ID%>_staffID_TextBox").val();
            if (staffID == "") {
                IsValid = false;
                $("#<%=this.ID%>_staffID_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_staffID_TextBox").removeClass("inputError");
            }
        }

        // Person Status
        if ($("#<%=this.ID%>_personStatus_DropDownList").is(":visible")) {
            var status = $("#<%=this.ID%>_personStatus_DropDownList").val();
            if (status == "na") {
                IsValid = false;
                $("#<%=this.ID%>_personStatus_DropDownList").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_personStatus_DropDownList").removeClass("inputError");
            }
        }

        // Gender
        if ($("#<%=this.ID%>_gender_DropDownList").is(":visible")) {
            var gender = $("#<%=this.ID%>_gender_DropDownList").val();
            if (gender == "na") {
                IsValid = false;
                $("#<%=this.ID%>_gender_DropDownList").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_gender_DropDownList").removeClass("inputError");
            }
        }

        // Street
        if ($("#<%=this.ID%>_street_TextBox").is(":visible")) {
            var street = $("#<%=this.ID%>_street_TextBox").val();
            if (street == "") {
                IsValid = false;
                $("#<%=this.ID%>_street_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_street_TextBox").removeClass("inputError");
            }
        }

         // Town
        if ($("#<%=this.ID%>_town_TextBox").is(":visible")) {
            var town = $("#<%=this.ID%>_town_TextBox").val();
            if (town == "") {
                IsValid = false;
                $("#<%=this.ID%>_town_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_town_TextBox").removeClass("inputError");
            }
        }

         // County
        if ($("#<%=this.ID%>_county_TextBox").is(":visible")) {
            var county = $("#<%=this.ID%>_county_TextBox").val();
            if (county == "") {
                IsValid = false;
                $("#<%=this.ID%>_county_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_county_TextBox").removeClass("inputError");
            }
        }

        // postCode
        if ($("#<%=this.ID%>_postcode_TextBox").is(":visible")) {
            var postCode = $("#<%=this.ID%>_postcode_TextBox").val();
            if (postCode == "") {
                IsValid = false;
                $("#<%=this.ID%>_postcode_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_postcode_TextBox").removeClass("inputError");
            }
        }

        // contact number
        if ($("#<%=this.ID%>_number_TextBox").is(":visible")) {
            var number = $("#<%=this.ID%>_number_TextBox").val();
            if (number == "") {
                IsValid = false;
                $("#<%=this.ID%>_number_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_number_TextBox").removeClass("inputError");
            }
        }

        // contact email
        if ($("#<%=this.ID%>_email_TextBox").is(":visible")) {
            var number = $("#<%=this.ID%>_email_TextBox").val();
            if (number == "") {
                IsValid = false;
                $("#<%=this.ID%>_email_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_email_TextBox").removeClass("inputError");
            }
        }

         // age
        if ($("#<%=this.ID%>_age_Div").is(":visible")) {
            var age = $("#<%=this.ID%>_age_RadNumericTextBox").val();
            if (age == "0") {
                IsValid = false;
                $("#<%=this.ID%>_age_RadNumericTextBox_wrapper").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_age_RadNumericTextBox_wrapper").removeClass("inputError");
            }
        }

         // DOB
        if ($("#<%=this.ID%>_dateOfBirth_Div").is(":visible")) {
            var dateOfBirthJSON = $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_ClientState").val();

            var IsValidDateOfBirth = true;

            if (dateOfBirthJSON != "" && dateOfBirthJSON != undefined) {
                var parsedDOBJSON = $.parseJSON(dateOfBirthJSON);

                if (parsedDOBJSON.valueAsString == undefined || parsedDOBJSON.valueAsString == "") {
                    IsValidDateOfBirth = false;
                }
            }
            else {
                IsValidDateOfBirth = false
            }


            if (IsValidDateOfBirth == false) {
                IsValid = false;
                $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_wrapper").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_wrapper").removeClass("inputError");
            }
            
        }


        if (IsValid == false) {
            $("#<%=this.ID%>_alert").modal('show');
        }

        args.IsValid = IsValid;

    }











    // ---- UNAVAILABLE CHECKBOX -----

    $('#<%=this.ID%>_detailsUnavailable_Checkbox').change(function() {
        Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>();
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    });


    function Unavailable_ToggleCheckbox_<%=CurrentPerson.CurrentType%>() {

        var isChecked = $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>() {


        var isChecked = $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").is(':checked');


        if (!isChecked) {
            $("#<%=CurrentPerson.CurrentType%>_NotSelectedText").show();
            $("#<%=CurrentPerson.CurrentType%>_SelectedText").hide();

            $(".<%=CurrentPerson.CurrentType%>_class").removeClass("alert-danger");
            $(".<%=CurrentPerson.CurrentType%>_class").addClass("alert-warning");

            $("#<%=this.ID%>_ContentRow").show();

            $("#<%=this.ID%>_External_Parent").show()

            
        }
        else {
            $("#<%=CurrentPerson.CurrentType%>_NotSelectedText").hide();
            $("#<%=CurrentPerson.CurrentType%>_SelectedText").show();

            $(".<%=CurrentPerson.CurrentType%>_class").addClass("alert-danger");
            $(".<%=CurrentPerson.CurrentType%>_class").removeClass("alert-warning");

            $("#<%=this.ID%>_ContentRow").hide();

            $("#<%=this.ID%>_External_Parent").hide()

            
        }


    }


    //  ---- ASSOCIATED ACCOUNT CHECKBOX -----
    $('#<%=this.ID%>_associatedAccount_Checkbox').change(function() {
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    });

     function AssociatedAccount_ToggleCheckbox_<%=this.ID%>() {

        var isChecked = $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function AssociatedAccount_ToggleDetails_<%=this.ID%>() {


        var isChecked = $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").is(':checked');


        if (!isChecked) {
            $("#<%=this.ID%>_AssociatedAccount_NotSelectedText").show();
            $("#<%=this.ID%>_AssociatedAccount_SelectedText").hide();

            $(".<%=this.ID%>_AssociatedAccount").removeClass("alert-danger");
            $(".<%=this.ID%>_AssociatedAccount").addClass("alert-warning");

            $("#<%=this.ID%>_BasicInfo").show();
            $("#<%=this.ID%>_External_Parent").show();
            $("#<%=this.ID%>_Address").show();
            $("#<%=this.ID%>_contactInfoAndAge").show();

            $("#<%=this.ID%>_AssociatedUserParent").hide();

            External_ToggleDetails_<%=this.ID%>();
            
        }
        else {
            $("#<%=this.ID%>_AssociatedAccount_NotSelectedText").hide();
            $("#<%=this.ID%>_AssociatedAccount_SelectedText").show();

            $(".<%=this.ID%>_AssociatedAccount").addClass("alert-danger");
            $(".<%=this.ID%>_AssociatedAccount").removeClass("alert-warning");

            $("#<%=this.ID%>_BasicInfo").hide();
            $("#<%=this.ID%>_External_Parent").hide();
            $("#<%=this.ID%>_Address").hide();
            $("#<%=this.ID%>_contactInfoAndAge").hide();

            $("#<%=this.ID%>_AssociatedUserParent").show();
            
        }


    }


    // ---- EXTERNAL CHECKBOX -----


     $('#<%=this.ID%>_addressExternal_CheckBox').change(function() {
         External_ToggleDetails_<%=this.ID%>();
    });



    function External_ToggleCheckbox_<%=this.ID%>() {

        var isChecked = $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").removeAttr('Checked','Checked');
        }
        

    }

    function External_ToggleDetails_<%=this.ID%>() {

        

        var isChecked = $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").is(':checked');

        if (!isChecked) {
            $("#<%=this.ID%>_External_NotSelectedText").show();
            $("#<%=this.ID%>_External_SelectedText").hide();

            $(".<%=this.ID%>_External").removeClass("alert-danger");
            $(".<%=this.ID%>_External").addClass("alert-warning");

            $("#<%=this.ID%>_Address").show();

            $("#<%=this.ID%>_Unavailable_Parent").show()
        }
        else {
            $("#<%=this.ID%>_External_NotSelectedText").hide();
            $("#<%=this.ID%>_External_SelectedText").show();

            $(".<%=this.ID%>_External").addClass("alert-danger");
            $(".<%=this.ID%>_External").removeClass("alert-warning");

            $("#<%=this.ID%>_Address").hide();

            $("#<%=this.ID%>_Unavailable_Parent").hide()

        }


    }


    // ---- DATE OF BIRTH CHECKBOX -----

    $('#<%=this.ID%>_toggleDateOfBirth_CheckBox').change(function() {
         ShowDateOfBirth_<%=this.ID %>();
    });

    function ToggleDateOfBirthCheckBox_<%=this.ID %>() {
        var isChecked = $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function ShowDateOfBirth_<%=this.ID %>() {
        var isChecked = $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").is(':checked');

        if (!isChecked) {
            $("#<%=this.ID %>_dateOfBirth_Div").hide();
            $("#<%=this.ID %>_ageUser_Div").show();

            $("#<%=this.ID %>_dateOfBirth_Alert").removeClass("alert-danger");
            $("#<%=this.ID %>_dateOfBirth_Alert").addClass("alert-warning");

        }
        else {
            $("#<%=this.ID %>_dateOfBirth_Div").show();
            $("#<%=this.ID %>_ageUser_Div").hide();

            $("#<%=this.ID %>_dateOfBirth_Alert").addClass("alert-danger");
            $("#<%=this.ID %>_dateOfBirth_Alert").removeClass("alert-warning");


        }

    }


    function RunAllChecks_<%=this.ID%>() {      
        External_ToggleDetails_<%=this.ID%>();
        Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>();
        ShowDateOfBirth_<%=this.ID%>();
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    }

    $( document ).ready(function() {
        BindControlEvents_<%=this.ID%>();
    });

    var prm_<%=this.ID%> = Sys.WebForms.PageRequestManager.getInstance();

    prm_<%=this.ID%>.add_endRequest(function () {
        BindControlEvents_<%=this.ID%>();
    });

    function BindControlEvents_<%=this.ID%>() {
        $("#<%=this.ID %>_Parent").show();
        RunAllChecks_<%=this.ID%>();
    }
    $("#<%=this.ID %>_Parent").show();
    RunAllChecks_<%=this.ID%>();--%>

</script>

</div>