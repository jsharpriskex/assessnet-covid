﻿// ------ VALIDATION

function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}

function ValidatePersonDetails(sender, args){

    var IsValid = true;

    //associatedUser_RadComboBox_Input

    // Start with associated user, this comes from JSON data.
    if ($("[id*='associatedUser_RadComboBox']").is(":visible")) {


        var associatedUserJSON = $("[id*='associatedUser_RadComboBox_ClientState']").val();

        var IsValidUserDropDown = true;

        if (associatedUserJSON != "" && associatedUserJSON != undefined) {
            var parsedJSON = $.parseJSON(associatedUserJSON);

            if (parsedJSON.value == undefined) {
                IsValidUserDropDown = false;
            }
            else {
                if (parsedJSON.value == "0") {
                    IsValidUserDropDown = false;
                }
            }
            
        }
        else {
            IsValidUserDropDown = false;
        }

        var hasUserSelected = $("[id*='associatedUser_RadComboBox_Input']").val();
        if (hasUserSelected.length > 0 && hasUserSelected !== "Please type a name or select a user") {
            IsValidUserDropDown = true;
        }

        if (IsValidUserDropDown === false ) {
            IsValid = false;
            $("div[id*='associatedUser_RadComboBox']").addClass("inputError");
        }
        else {
            $("div[id*='associatedUser_RadComboBox']").removeClass("inputError");
        }



    }

    // First Name
    if ($("#forename_TextBox").is(":visible")) {
        var firstName = $("#forename_TextBox").val();
        if (firstName == "") {
            IsValid = false;
            $("#forename_TextBox").addClass("inputError");
        }
        else {
            $("#forename_TextBox").removeClass("inputError");
        }
    }

    // Surname
    if ($("#surname_TextBox").is(":visible")) {
        var surname = $("#surname_TextBox").val();
        if (surname == "") {
            IsValid = false;
            $("#surname_TextBox").addClass("inputError");
        }
        else {
            $("#surname_TextBox").removeClass("inputError");
        }
    }

    // Occupation
    if ($("#surname_TextBox").is(":visible")) {
        var occupation = $("#occupation_TextBox").val();
        if (occupation == "") {
            IsValid = false;
            $("#occupation_TextBox").addClass("inputError");
        }
        else {
            $("#occupation_TextBox").removeClass("inputError");
        }
    }

    // ID
    //if ($("#staffID_TextBox").is(":visible")) { 
    //    var staffID = $("#staffID_TextBox").val();
    //    if (staffID == "") {
    //        IsValid = false;
    //        $("#staffID_TextBox").addClass("inputError");
    //    }
    //    else {
    //        $("#staffID_TextBox").removeClass("inputError");
    //    }
    //}

    // Person Status
    if ($("#personStatus_DropDownList").is(":visible")) {
        var status = $("#personStatus_DropDownList").val();
        if (status == "na") {
            IsValid = false;
            $("#personStatus_DropDownList").addClass("inputError");
        }
        else {
            $("#personStatus_DropDownList").removeClass("inputError");
        }
    }

    // Gender
    if ($("#gender_DropDownList").is(":visible")) {
        var gender = $("#gender_DropDownList").val();
        if (gender == "na") {
            IsValid = false;
            $("#gender_DropDownList").addClass("inputError");
        }
        else {
            $("#gender_DropDownList").removeClass("inputError");
        }
    }

    // Street
    if ($("#street_TextBox").is(":visible")) {
        var street = $("#street_TextBox").val();
        if (street == "") {
            IsValid = false;
            $("#street_TextBox").addClass("inputError");
        }
        else {
            $("#street_TextBox").removeClass("inputError");
        }
    }

    // Town
    if ($("#town_TextBox").is(":visible")) {
        var town = $("#town_TextBox").val();
        if (town == "") {
            IsValid = false;
            $("#town_TextBox").addClass("inputError");
        }
        else {
            $("#town_TextBox").removeClass("inputError");
        }
    }

    // County
    if ($("#county_TextBox").is(":visible")) {
        var county = $("#county_TextBox").val();
        if (county == "") {
            IsValid = false;
            $("#county_TextBox").addClass("inputError");
        }
        else {
            $("#county_TextBox").removeClass("inputError");
        }
    }

    // postCode
    if ($("#postcode_TextBox").is(":visible")) {
        var postCode = $("#postcode_TextBox").val();
        if (postCode == "") {
            IsValid = false;
            $("#postcode_TextBox").addClass("inputError");
        }
        else {
            $("#postcode_TextBox").removeClass("inputError");
        }
    }

    // contact number
    if ($("#number_TextBox").is(":visible")) {

        var number = $("#number_TextBox").val();
        var email = $("#email_TextBox").val();

        if (number !== "****") {
	        if ((email == "") && number == "") {
		        IsValid = false;
		        $("#number_TextBox").addClass("inputError");
		        $("#email_TextBox").addClass("inputError");
	        } else {
		        $("#number_TextBox").removeClass("inputError");
		        $("#email_TextBox").removeClass("inputError");

		        if ((!validateEmail(email) || email == "") && number == "") {
			        IsValid = false;
			        $("#email_TextBox").addClass("inputError");
			        $("#number_TextBox").removeClass("inputError");
		        }
		        else {
			        $("#email_TextBox").removeClass("inputError");
			        $("#number_TextBox").removeClass("inputError");
		        }
 
	        }



	        if (email != "" && !validateEmail(email)) {
		        IsValid = false;
		        $("#email_TextBox").addClass("inputError");
	        }


        }

       
        
    }

    // age
    if ($("#ageUser_Div").is(":visible")) {
        var age = $("#age_RadNumericTextBox").val();
        if (age == "0") {
            IsValid = false;
            $("#age_RadNumericTextBox_wrapper").addClass("inputError");
        }
        else {
            $("#age_RadNumericTextBox_wrapper").removeClass("inputError");
        }
    }

    // DOB
    if ($("#dateOfBirth_Div").is(":visible")) {
        var dateOfBirthJSON = $("[id$='dateOfBirth_RadDatePicker_dateInput_ClientState']").val();

        console.log(dateOfBirthJSON)

        var IsValidDateOfBirth = true;

        if (dateOfBirthJSON != "" && dateOfBirthJSON != undefined) {
            var parsedDOBJSON = $.parseJSON(dateOfBirthJSON);

            if (parsedDOBJSON.valueAsString == undefined || parsedDOBJSON.valueAsString == "") {
                IsValidDateOfBirth = false;
            }
        }
        else {
            IsValidDateOfBirth = false;
        }


        if (IsValidDateOfBirth == false) {
            IsValid = false;
            $("[id$='dateOfBirth_RadDatePicker_dateInput_wrapper']").addClass("inputError");
        }
        else {
            $("[id$='dateOfBirth_RadDatePicker_dateInput_wrapper']").removeClass("inputError");
        }

    }


    if (IsValid == false) {
        $("#alert").modal('show');
    }

    args.IsValid = IsValid;

}











// ---- UNAVAILABLE CHECKBOX -----


$(document).on('change', '#detailsUnavailable_Checkbox', function () {
    Unavailable_ToggleDetails();
    AssociatedAccount_ToggleDetails();
});

function Unavailable_ToggleCheckbox() {

    var isChecked = $("#detailsUnavailable_Checkbox").is(':checked');

    if (isChecked || isChecked == null) {
        $("#detailsUnavailable").attr('Checked', 'Checked');
    }
    else {
        $("#detailsUnavailable_Checkbox").removeAttr('Checked', 'Checked');
    }


}

function Unavailable_ToggleDetails() {


    var isChecked = $("#detailsUnavailable_Checkbox").is(':checked');


    if (!isChecked) {
        $("#NotSelectedText").show();
        $("#SelectedText").hide();

        $(".unavailable_class").removeClass("alert-success");
        $(".unavailable_class").addClass("alert-warning");

        $("#ContentRow").show();

        $("#External_Parent").show()


    }
    else {
        $("#NotSelectedText").hide();
        $("#SelectedText").show();

        $(".unavailable_class").addClass("alert-success");
        $(".unavailable_class").removeClass("alert-warning");

        $("#ContentRow").hide();

        $("#External_Parent").hide()


    }


}


//  ---- ASSOCIATED ACCOUNT CHECKBOX -----
$(document).on('change', '#associatedAccount_Checkbox', function () {
    AssociatedAccount_ToggleDetails();
});

function AssociatedAccount_ToggleCheckbox() {

    console.log("here")

    var isChecked = $("#associatedAccount_Checkbox").is(':checked');

    if (isChecked || isChecked == null) {
        $("#associatedAccount_Checkbox").attr('Checked', 'Checked');
    }
    else {
        $("#associatedAccount_Checkbox").removeAttr('Checked', 'Checked');
    }


}

function AssociatedAccount_ToggleDetails() {


    var isChecked = $("#associatedAccount_Checkbox").is(':checked');


    if (!isChecked) {
        $("#AssociatedAccount_NotSelectedText").show();
        $("#AssociatedAccount_SelectedText").hide();

        $(".AssociatedAccount").removeClass("alert-success");
        $(".AssociatedAccount").addClass("alert-warning");

        $("#BasicInfo").show();
        $("#External_Parent").show();
        $("#Address").show();
        $("#contactInfoAndAge").show();

        $("#AssociatedUserParent").hide();

        External_ToggleDetails();

    }
    else {
        $("#AssociatedAccount_NotSelectedText").hide();
        $("#AssociatedAccount_SelectedText").show();

        $(".AssociatedAccount").addClass("alert-success");
        $(".AssociatedAccount").removeClass("alert-warning");

        $("#BasicInfo").hide();
        $("#External_Parent").hide();
        $("#Address").hide();
        $("#contactInfoAndAge").hide();

        $("#AssociatedUserParent").show();

    }


}


// ---- EXTERNAL CHECKBOX -----
$(document).on('change', '#addressExternal_CheckBox', function () {
    External_ToggleDetails();
});

function External_ToggleCheckbox() {

    var isChecked = $("#addressExternal_CheckBox").is(':checked');

    if (isChecked || isChecked == null) {
        $("#addressExternal_CheckBox").attr('Checked', 'Checked');
    }
    else {
        $("#addressExternal_CheckBox").removeAttr('Checked', 'Checked');
    }


}

function External_ToggleDetails() {



    var isChecked = $("#addressExternal_CheckBox").is(':checked');

    if (!isChecked) {
        $("#External_NotSelectedText").show();
        $("#External_SelectedText").hide();

        $(".External").removeClass("alert-success");
        $(".External").addClass("alert-warning");

        $("#Address").show();

        $("#Unavailable_Parent").show()
    }
    else {
        $("#External_NotSelectedText").hide();
        $("#External_SelectedText").show();

        $(".External").addClass("alert-success");
        $(".External").removeClass("alert-warning");

        $("#Address").hide();

        $("#Unavailable_Parent").hide()

    }


}


// ---- DATE OF BIRTH CHECKBOX -----
$(document).on('change', '#toggleDateOfBirth_CheckBox', function () {
    ShowDateOfBirth();
});


function ToggleDateOfBirthCheckBox() {
    var isChecked = $("#toggleDateOfBirth_CheckBox").is(':checked');

    if (isChecked || isChecked == null) {
        $("#toggleDateOfBirth_CheckBox").attr('Checked', 'Checked');
    }
    else {
        $("#toggleDateOfBirth_CheckBox").removeAttr('Checked', 'Checked');
    }


}

function ShowDateOfBirth() {
    var isChecked = $("#toggleDateOfBirth_CheckBox").is(':checked');

    if (!isChecked) {
        $("#dateOfBirth_Div").hide();
        $("#ageUser_Div").show();

        $("#dateOfBirth_Alert").removeClass("alert-success");
        $("#dateOfBirth_Alert").addClass("alert-warning");

    }
    else {
        $("#dateOfBirth_Div").show();
        $("#ageUser_Div").hide();

        $("#dateOfBirth_Alert").addClass("alert-success");
        $("#dateOfBirth_Alert").removeClass("alert-warning");


    }

}


function RunAllChecks() {
    $("#Parent").show();
    External_ToggleDetails();
    Unavailable_ToggleDetails();
    ShowDateOfBirth();
    AssociatedAccount_ToggleDetails();
}

$(document).ready(function () {
    BindControlEvents();
});

//var prm = Sys.WebForms.PageRequestManager.getInstance();

//prm.add_endRequest(function () {
//    BindControlEvents();
//});

function BindControlEvents() {
    
    RunAllChecks();
}
