﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class PersonDetails_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public PersonDetails CurrentPerson;
    public PersonDetails.Type UserType;
    public PersonDetails.IncidentType IncidentType;
    public PersonDetails.Status Status;


  

    public event EventHandler SaveOrEditButton_Click;

    private bool FormDataRequired = false;

    public string PersonStatusText;
    public string AdvancedContactDetails;
    public string AdvancedContactDetails_StaffID;
    public string AdvancedContactDetails_StaffID_Description;
    public string PersonDetailsUnknown;
    public string PersonDetailsAddressExternal;

    public bool ShowData;

    protected void Page_Load(object sender, EventArgs e)
    {

        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************


        ChangeFormFromSessions();


        CurrentPerson = new PersonDetails(this, Status);






    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        CurrentPerson = new PersonDetails(this, Status);

        if (ShowData)
        {
            if (Status == PersonDetails.Status.Edit)
            {
                Report.Visible = false;
                _ReturnFormData();
            }
            else if (Status == PersonDetails.Status.Report)
            {
                editScript.Visible = false;
                Parent.Visible = false;
                CurrentPerson.ReturnReportData();
            }
        }

    }


    private void ChangeFormFromSessions()
    {
        // MOVE THIS INTO CLASS
        PersonStatusText = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_STATUS_COL_TITLE", "pref");
        AdvancedContactDetails = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_ADV_CONTACT_DETAILS", "pref");
        AdvancedContactDetails_StaffID = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_ADV_CONTACT_DETAILS_SHOW_ID", "pref");
        AdvancedContactDetails_StaffID_Description = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_ADV_CONTACT_DETAILS_ID_TEXT", "pref");
        PersonDetailsUnknown = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_IPDETAILS_UNKNOWN", "pref");
        PersonDetailsAddressExternal = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "ACC_SKIP_INJADDRESS_VALIDATION", "pref");

        if (Status == PersonDetails.Status.Edit)
        {
            if (PersonStatusText == "")
            {
                PersonStatusText = "Person Status";
            }

            if (AdvancedContactDetails == "Y")
            {
                contactDetails_Div.Visible = true;
            }

            if (AdvancedContactDetails_StaffID == "Y")
            {
                contactDetails_StaffID_Div.Visible = true;
                staffID_TextBox.Attributes.Add("placeholder", AdvancedContactDetails_StaffID_Description);
            }

            if (PersonDetailsUnknown == "Y")
            {
                Unavailable_Parent.Visible = true;
            }

            if (PersonDetailsAddressExternal == "Y")
            {
                External_Parent.Visible = true;
            }
        }

        

        // END OF MOVE INTO CLASS
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        personDetails_CustomValidator.ClientValidationFunction = this.ID + "_ValidatePersonDetails";
    }

    public void SaveData()
    {
        CurrentPerson.SaveData();
    }

    
    // The public method triggers that we need to show some form data, and initates it during page load
    public void ReturnFormData()
    {
        FormDataRequired = true;
    }
    // This is called during page load
    private void _ReturnFormData()
    {
        CurrentPerson.PopulateControls();
        CurrentPerson.ReturnFormData();
        FormDataRequired = false;
        //ScriptManager.RegisterStartupScript(ReferenceToCurrentPage.Page, ReferenceToCurrentPage.GetType(), Guid.NewGuid().ToString(), "RunAllChecks_" + this.ID + "();", true);
    }

    public void PushJavascriptToPage(string script)
    {
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "", script, true);
    }

    //User Dropdown
    protected void generic_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "none", _corpCode);
    }


    protected void generic_UserDropDown_Validation(object source, ServerValidateEventArgs args)
    {
        CustomValidator currentValidator = (CustomValidator)source;


        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);

        //if (currentRadBox.ID == "recordReviewUser" && oneoffAssessmentResetSwitch.Checked == true)
        //{

        //}
        //else
        //{
        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, false);
        //}


    }

}


public class PersonDetails
{

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _subIncidentReference;
    private bool _isAdmin = true;

    private bool _hasData;

    private string _associatedUser;

    private string _firstName;
    private string _middleName;
    private string _surnameName;
    private string _occupation;
    private string _personStatus;

    private string _contactNumber;
    private string _contactEmail;

    private string _staffID;

    private string _addressStreet;
    private string _addressTown;
    private string _addressCounty;
    private string _addressPostCode;

    private string _addressSingle; // This will be removed.

    private string _gender;
    private string _age;
    private DateTime? _dateOfBirth;


    // This controls what is shown.
    public bool Address_IsStoredExternally;
    public bool Address_UseSameDetails;
    public bool Address_UsemyDetails;
    public bool Address_IsManual;

    public bool InformationUnvailable;


    public PersonDetails.Type CurrentType;
    public readonly PersonDetails.IncidentType _incidentType;

    //Current control lets us find the elements within it.
    private PersonDetails_UserControl _currentControl;




    // Controls on page - FORMS
    private TextBox _forename_TextBox;
    private TextBox _surname_TextBox;
    private TextBox _occupation_TextBox;
    private TextBox _staffID_TextBox;
    private DropDownList _personStatus_DropDownList;
    private TextBox _street_TextBox;
    private TextBox _town_TextBox;
    private TextBox _county_TextBox;
    private TextBox _postcode_TextBox;
    private TextBox _number_TextBox;
    private TextBox _email_TextBox;
    private RadNumericTextBox _age_RadNumericTextBox;
    private RadDatePicker _dateOfBirth_RadDatePicker;
    private DropDownList _gender_DropDownList;


    private CheckBox _addressExternal_CheckBox;
    private HtmlGenericControl _addressExternal_Label;


    private CheckBox _detailsUnavailable_Checkbox;
    private HtmlGenericControl _detailsUnavailable_Label;


    private CheckBox _associatedAccount_Checkbox;
    private HtmlGenericControl _associatedAccount_Label;



    private RadComboBox _associatedUser_RadComboBox;

    private HtmlGenericControl _age_Div;
    private HtmlGenericControl _external_Div;
    private HtmlGenericControl _Unavailable_Parent;
    private HtmlGenericControl _AssociatedAccount_Parent_Div;


    // Controls on page - REPORT

    private HtmlGenericControl _personDetailsUnavailable_Report;
    private HtmlGenericControl _mainData_Report;
    private HtmlGenericControl _associatedUser_Report;

    private HtmlTableRow _associatedUserDiv;
    private Literal _associatedUser_Literal;



    private HtmlTableRow _nameDiv;
    private Literal _name_Literal;

    private HtmlTableRow _statusDiv;
    private Literal _status_Literal;

    private HtmlTableRow _genderDiv;
    private Literal _gender_Literal;

    private HtmlTableRow _occupationDiv;
    private Literal _occupation_Literal;

    private HtmlGenericControl _addressIsStoredExternally_Report;
    private HtmlTable _addressTable;


    private HtmlTableRow _streetDiv;
    private Literal _street_Literal;

    private HtmlTableRow _townDiv;
    private Literal _town_Literal;

    private HtmlTableRow _countyDiv;
    private Literal _county_Literal;

    private HtmlTableRow _postCodeDiv;
    private Literal _postCode_Literal;


    private HtmlTableRow _numberDiv;
    private Literal _number_Literal;

    private HtmlTableRow _emailDiv;
    private Literal _email_Literal;

    private HtmlTableRow _idNumbersDiv;
    private Literal _idNumbers_Literal;

    private HtmlTableRow _ageReportDiv;
    private Literal _ageReport_Literal;




    // Basic Initiating Method
    // DEFUNCT
    public PersonDetails(string corpCode, string incidentCentre, string incidentReference, string subIncidentReference, Type currentType, string currentUser, bool isAdmin, IncidentType incidentType, PersonDetails_UserControl currentControl)
    {
        _corpCode = corpCode;
        _incidentCentre = incidentCentre;
        _incidentReference = incidentReference;
        _subIncidentReference = subIncidentReference;
        CurrentType = currentType;
        _currentUser = currentUser;
        _isAdmin = isAdmin;
        _incidentType = incidentType;
        _currentControl = currentControl;

    }

    // BETTER
    public PersonDetails(PersonDetails_UserControl currentControl, Status currentStatus)
    {
        _corpCode = currentControl._corpCode;
        _incidentCentre = currentControl.IncidentCentre;
        _incidentReference = currentControl.IncidentReference;
        _subIncidentReference = currentControl.IncidentSubReference;
        CurrentType = currentControl.UserType;
        _currentUser = currentControl._currentUser;
        _isAdmin = currentControl.IsAdmin;
        _incidentType = currentControl.IncidentType;
        _currentControl = currentControl;


        // Create / Edit Controls

        _forename_TextBox = (TextBox)_currentControl.FindControl("forename_TextBox");
        _surname_TextBox = (TextBox)_currentControl.FindControl("surname_TextBox");
        _occupation_TextBox = (TextBox)_currentControl.FindControl("occupation_TextBox");
        _staffID_TextBox = (TextBox)_currentControl.FindControl("staffID_TextBox");
        _personStatus_DropDownList = (DropDownList)_currentControl.FindControl("personStatus_DropDownList");
        _street_TextBox = (TextBox)_currentControl.FindControl("street_TextBox");
        _town_TextBox = (TextBox)_currentControl.FindControl("town_TextBox");
        _county_TextBox = (TextBox)_currentControl.FindControl("county_TextBox");
        _postcode_TextBox = (TextBox)_currentControl.FindControl("postcode_TextBox");
        _number_TextBox = (TextBox)_currentControl.FindControl("number_TextBox");
        _email_TextBox = (TextBox)_currentControl.FindControl("email_TextBox");
        _age_RadNumericTextBox = (RadNumericTextBox)_currentControl.FindControl("age_RadNumericTextBox");
        _dateOfBirth_RadDatePicker = (RadDatePicker)_currentControl.FindControl("dateOfBirth_RadDatePicker");
        _gender_DropDownList = (DropDownList)_currentControl.FindControl("gender_DropDownList");
        _associatedUser_RadComboBox = (RadComboBox)_currentControl.FindControl("associatedUser_RadComboBox");

        _age_Div = (HtmlGenericControl)_currentControl.FindControl("age_Div");
        _external_Div = (HtmlGenericControl)_currentControl.FindControl("External_Parent");
        _Unavailable_Parent = (HtmlGenericControl)_currentControl.FindControl("Unavailable_Parent");
        _AssociatedAccount_Parent_Div = (HtmlGenericControl)_currentControl.FindControl("AssociatedAccount_Parent_Div");

        _addressExternal_CheckBox = (CheckBox)_currentControl.FindControl("addressExternal_CheckBox");
        _detailsUnavailable_Checkbox = (CheckBox)_currentControl.FindControl("detailsUnavailable_Checkbox");
        _associatedAccount_Checkbox = (CheckBox)_currentControl.FindControl("associatedAccount_Checkbox");

        _detailsUnavailable_Label = (HtmlGenericControl)_currentControl.FindControl("detailsUnavailable_Label");
        _addressExternal_Label = (HtmlGenericControl)_currentControl.FindControl("addressExternal_Label");
        _associatedAccount_Label = (HtmlGenericControl)_currentControl.FindControl("associatedAccount_Label");


        _dateOfBirth_RadDatePicker.MinDate = DateTime.Parse("1900-01-01");
        _dateOfBirth_RadDatePicker.MaxDate = DateTime.Now;


        // Report Controls

        _personDetailsUnavailable_Report = (HtmlGenericControl)_currentControl.FindControl("personDetailsUnavailable_Report");
        _mainData_Report = (HtmlGenericControl)_currentControl.FindControl("mainData_Report");

        _associatedUser_Report = (HtmlGenericControl)_currentControl.FindControl("associatedUser_Report");
        _associatedUserDiv = (HtmlTableRow)_currentControl.FindControl("associatedUserDiv");
        _associatedUser_Literal = (Literal)_currentControl.FindControl("associatedUser_Literal");

        _nameDiv = (HtmlTableRow)_currentControl.FindControl("nameDiv");
        _name_Literal = (Literal)_currentControl.FindControl("name_Literal");

        _statusDiv = (HtmlTableRow)_currentControl.FindControl("statusDiv");
        _status_Literal = (Literal)_currentControl.FindControl("status_Literal");

        _genderDiv = (HtmlTableRow)_currentControl.FindControl("genderDiv");
        _gender_Literal = (Literal)_currentControl.FindControl("gender_Literal");

        _occupationDiv = (HtmlTableRow)_currentControl.FindControl("occupationDiv");
        _occupation_Literal = (Literal)_currentControl.FindControl("occupation_Literal");

        _addressIsStoredExternally_Report = (HtmlGenericControl)_currentControl.FindControl("addressIsStoredExternally_Report");
        _addressTable = (HtmlTable)_currentControl.FindControl("addressTable");


        _streetDiv = (HtmlTableRow)_currentControl.FindControl("streetDiv");
        _street_Literal = (Literal)_currentControl.FindControl("street_Literal");

        _townDiv = (HtmlTableRow)_currentControl.FindControl("townDiv");
        _town_Literal = (Literal)_currentControl.FindControl("town_Literal");

        _countyDiv = (HtmlTableRow)_currentControl.FindControl("countyDiv");
        _county_Literal = (Literal)_currentControl.FindControl("county_Literal");

        _postCodeDiv = (HtmlTableRow)_currentControl.FindControl("postCodeDiv");
        _postCode_Literal = (Literal)_currentControl.FindControl("postCode_Literal");


        _numberDiv = (HtmlTableRow)_currentControl.FindControl("numberDiv");
        _number_Literal = (Literal)_currentControl.FindControl("number_Literal");

        _emailDiv = (HtmlTableRow)_currentControl.FindControl("emailDiv");
        _email_Literal = (Literal)_currentControl.FindControl("email_Literal");

        _idNumbersDiv = (HtmlTableRow)_currentControl.FindControl("idNumbersDiv");
        _idNumbers_Literal = (Literal)_currentControl.FindControl("idNumbers_Literal");

        _ageReportDiv = (HtmlTableRow)_currentControl.FindControl("ageReportDiv");
        _ageReport_Literal = (Literal)_currentControl.FindControl("ageReport_Literal");

    }

    // Methods to retrieve data

    // This method populates dropdowns etc with their data, rather than
    public void PopulateControls()
    {

        //Populate Person Affected
        using(DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "acc_status", false, "en-gb", _personStatus))
        {

            _personStatus_DropDownList.DataSource = accOptions;
            _personStatus_DropDownList.DataTextField = "opt_value";
            _personStatus_DropDownList.DataValueField = "opt_id";
            

            _personStatus_DropDownList.DataBind();

            _personStatus_DropDownList.Items.Insert(0, new ListItem("Please Select...", "na"));


            if (_personStatus != "")
            {
                if (_personStatus_DropDownList.Items.FindByValue(_personStatus) != null)
                {
                    _personStatus_DropDownList.SelectedValue = _personStatus;
                }
            }
            

        }

    }


    private void ReturnFormFromInternalVariables()
    {
        _forename_TextBox.Text = _firstName;
        //MIDDLE NAME
        _surname_TextBox.Text = _surnameName;
        //ASSOCIATED USER
        _occupation_TextBox.Text = _occupation;
        _staffID_TextBox.Text = _staffID;

        if(_personStatus != "")
        {
            if(_personStatus_DropDownList.Items.FindByValue(_personStatus) != null)
            {
                _personStatus_DropDownList.SelectedValue = _personStatus;
            }
            
        }
        
        _street_TextBox.Text = _addressStreet;
        _town_TextBox.Text = _addressTown;
        _county_TextBox.Text = _addressCounty;
        _postcode_TextBox.Text = _addressPostCode;
        _number_TextBox.Text = _contactNumber;
        _email_TextBox.Text = _contactEmail;
        _age_RadNumericTextBox.Text = _age;
        _dateOfBirth_RadDatePicker.SelectedDate = _dateOfBirth;
        _gender_DropDownList.SelectedValue = _gender;
        _addressExternal_CheckBox.Checked = Address_IsStoredExternally;
        _detailsUnavailable_Checkbox.Checked = InformationUnvailable;

   

        if(_associatedUser != "" && _associatedUser != null)
        {
            UserControls.returnUserDropDown_Initilise(_associatedUser_RadComboBox, _associatedUser, "none", _corpCode);
            _associatedAccount_Checkbox.Checked = true;
        }

        // If the user isn't an admin, they arent able to change various things, as well as see information.

        if (!_isAdmin && _hasData)
        {


            if (InformationUnvailable)
            {
                _detailsUnavailable_Label.Attributes.Add("style", "display:none");
            }
            else
            {
                _Unavailable_Parent.Visible = false;
            }

            if (Address_IsStoredExternally)
            {
                _addressExternal_Label.Attributes.Add("style", "display:none");
                _Unavailable_Parent.Visible = false;
                _AssociatedAccount_Parent_Div.Visible = false;
            }
            else
            {
                _external_Div.Visible = false;
            }

            if(_associatedUser.Length > 0)
            {
                _associatedAccount_Label.Attributes.Add("style", "display:none");
                _Unavailable_Parent.Visible = false;
                _external_Div.Visible = false;
            }
            else
            {
                _AssociatedAccount_Parent_Div.Visible = false;
            }

            _street_TextBox.Attributes.Add("disabled", "true");
            _town_TextBox.Attributes.Add("disabled", "true");
            _county_TextBox.Attributes.Add("disabled", "true");
            _postcode_TextBox.Attributes.Add("disabled", "true");
            _number_TextBox.Attributes.Add("disabled", "true");
            _email_TextBox.Attributes.Add("disabled", "true");
            _age_RadNumericTextBox.Enabled = false;


            _occupation_TextBox.Attributes.Add("disabled", "true");
            _staffID_TextBox.Attributes.Add("disabled", "true");

            _dateOfBirth_RadDatePicker.Enabled = false;

            _gender_DropDownList.Items.Clear();
            _gender_DropDownList.Items.Add(new ListItem("****"));
            _gender_DropDownList.Attributes.Add("disabled", "true");

            _age_Div.Visible = false;



        }

    }


    private void ReturnReportFromInternalVariables()
    {


        if (InformationUnvailable)
        {
            _personDetailsUnavailable_Report.Visible = true;
            _mainData_Report.Visible = false;
        }
        else if(_associatedUser.Length > 1)
        {
            _associatedUser_Report.Visible = true;
            _mainData_Report.Visible = false;
            _associatedUser_Literal.Text = UserControls.GetUser(_associatedUser, _corpCode);
        }
        else
        {
            _name_Literal.Text = _firstName + " " + _middleName + " " + _surnameName;
            _status_Literal.Text = _personStatus;

            _gender = ReturnGender(_gender);

            if (!_isAdmin)
            {
                _gender = "****";
            }

            _gender_Literal.Text = _gender;
            _occupation_Literal.Text = _occupation;

            if (Address_IsStoredExternally)
            {
                _addressIsStoredExternally_Report.Visible = true;
            }
            else
            {
                _street_Literal.Text = _addressStreet;
                _town_Literal.Text = _addressTown;
                _county_Literal.Text = _addressCounty;
                _postCode_Literal.Text = _addressPostCode;
            }

            _number_Literal.Text = _contactNumber;
            _email_Literal.Text = _contactEmail;
            _idNumbers_Literal.Text = _staffID;

            // Another check
            if (!_isAdmin)
            {
                _age = "****";
            }

            _ageReport_Literal.Text = _age;
        }


        

        

      


    }

    // This method populates the fields with data thats already there, as if its in edit mode.
    public void ReturnFormData()
    {
        SaveInternalVariablesFromDB();
        ReturnFormFromInternalVariables();
        //PushJavascriptToMain("RunAllChecks_" + _currentControl.ID + "();");
    }

    public void ReturnReportData()
    {
        SaveInternalVariablesFromDB();
        ReturnReportFromInternalVariables();
    }



    // Methods to save data
    private void SaveInternalVariablesFromForm()
    {

        // MISSING
        //   - Middle Name
        //   - Associated User

        _firstName = _forename_TextBox.Text;
        _middleName = "";
        _surnameName = _surname_TextBox.Text;
        _associatedUser = _associatedUser_RadComboBox.SelectedValue;
        _occupation = _occupation_TextBox.Text;
        _staffID = _staffID_TextBox.Text;
        _personStatus = _personStatus_DropDownList.SelectedValue;
        _addressStreet = _street_TextBox.Text;
        _addressTown = _town_TextBox.Text;
        _addressCounty = _county_TextBox.Text;
        _addressPostCode = _postcode_TextBox.Text;
        _contactNumber = _number_TextBox.Text;
        _contactEmail = _email_TextBox.Text;
        _age = _age_RadNumericTextBox.Text;
        _gender = _gender_DropDownList.SelectedValue;
        

        Address_IsStoredExternally = _addressExternal_CheckBox.Checked;
        InformationUnvailable = _detailsUnavailable_Checkbox.Checked;

        // If no date of birth, then we set to null
        _dateOfBirth = _dateOfBirth_RadDatePicker.SelectedDate == null ? null : _dateOfBirth_RadDatePicker.SelectedDate;


       
    }


    private void SaveInternalVariablesFromDB()
    {
        // This grabs the data from the DB, also checks to see if any data is there!
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {



                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.PersonDetails_ReturnData", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@recordreference", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_recordreference", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@type_of_person", (int)CurrentType));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    _associatedUser = Convert.ToString(objrs["linked_account"]);

                    _firstName = Convert.ToString(objrs["first_name"]);
                    _middleName = Convert.ToString(objrs["middle_name"]);
                    _surnameName = Convert.ToString(objrs["surname"]);
                    _occupation = Convert.ToString(objrs["occupation"]);
                    _personStatus = Convert.ToString(objrs["person_status"]);

                    _contactNumber = Convert.ToString(objrs["contact_number"]);
                    _contactEmail = Convert.ToString(objrs["contact_email"]);

                    _staffID = Convert.ToString(objrs["staff_id"]);

                    _addressStreet = Convert.ToString(objrs["address_street"]);
                    _addressTown = Convert.ToString(objrs["address_town"]);
                    _addressCounty = Convert.ToString(objrs["address_county"]);
                    _addressPostCode = Convert.ToString(objrs["address_postcode"]);

                    //_addressSingle = Convert.ToString(objrs[""]);

                    _gender = Convert.ToString(objrs["gender"]);

                    _age = Convert.ToString(objrs["age"]);

                    if(objrs["date_of_birth"] == DBNull.Value)
                    {
                        _dateOfBirth = null;
                    }
                    else
                    {
                        _dateOfBirth = Convert.ToDateTime(objrs["date_of_birth"]);
                    }
                    

                    Address_IsStoredExternally = Convert.ToBoolean(objrs["addressExternal"]);
                    InformationUnvailable = Convert.ToBoolean(objrs["informationUnavailable"]);

                    _hasData = true;
                }
                else
                {
                    // No data, lets indicate this to the class for some error prevention / record creation... woooo \o/
                    _hasData = false;
                }


                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }

        // Now that we have the data from the database, we really need to star it out if they are not an admin!
        // This is done at this stage in case this method is called elsewhere.

        if (!_isAdmin && _hasData)
        {
            _contactNumber = "****";
            _contactEmail = "****";
            _staffID = "****";
            _addressStreet = "****";
            _addressTown = "****";
            _addressCounty = "****";
            _addressPostCode = "****";
            _addressSingle = "****";
            _gender = "****";
            _age = "0";
            _dateOfBirth = null;
        }

    }


    public void SaveData()
    {

        SaveInternalVariablesFromForm();

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                // If the information is unavailable, then we need to fully wipe it out.
                if (InformationUnvailable)
                {
                    _firstName = "";
                    _middleName = "";
                    _surnameName = "";
                    _associatedUser = "";
                    _occupation = "";
                    _personStatus = "";
                    _contactNumber = "";
                    _contactEmail = "";
                    _staffID = "";
                    _addressStreet = "";
                    _addressTown = "";
                    _addressCounty = "";
                    _addressPostCode = "";
                    _gender = "";
                    _age = "";
                    _dateOfBirth = null;
                    Address_IsStoredExternally = false;
                }
                // If the person comes from an associated user, then we need to wipe details.
                else if (_associatedUser.Length > 0 && _associatedAccount_Checkbox.Checked == true)
                {
                    _firstName = "";
                    _middleName = "";
                    _surnameName = "";
                    _occupation = "";
                    _personStatus = "";
                    _contactNumber = "";
                    _contactEmail = "";
                    _staffID = "";
                    _addressStreet = "";
                    _addressTown = "";
                    _addressCounty = "";
                    _addressPostCode = "";
                    _gender = "";
                    _age = "";
                    _dateOfBirth = null;
                    Address_IsStoredExternally = false;
                    InformationUnvailable = false;
                }
                // If the address is external, then we need to wipe the address.
                else if (Address_IsStoredExternally)
                {
                    _addressStreet = "";
                    _addressTown = "";
                    _addressCounty = "";
                    _addressPostCode = "";
                    InformationUnvailable = false;
                    _associatedUser = "";
                }
                else
                {
                    _associatedUser = "";
                }






                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.PersonDetails_Save", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@recordreference", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_recordreference", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@isAdmin", _isAdmin));
                cmd.Parameters.Add(new SqlParameter("@type_of_person", (int)CurrentType));
                cmd.Parameters.Add(new SqlParameter("@first_name", _firstName));
                cmd.Parameters.Add(new SqlParameter("@middle_name", _middleName));
                cmd.Parameters.Add(new SqlParameter("@surname", _surnameName));
                cmd.Parameters.Add(new SqlParameter("@linked_account", _associatedUser));
                cmd.Parameters.Add(new SqlParameter("@occupation", _occupation));
                cmd.Parameters.Add(new SqlParameter("@person_status", _personStatus));
                cmd.Parameters.Add(new SqlParameter("@contact_number", _contactNumber));
                cmd.Parameters.Add(new SqlParameter("@contact_email", _contactEmail));
                cmd.Parameters.Add(new SqlParameter("@staff_id", _staffID));
                cmd.Parameters.Add(new SqlParameter("@address_street", _addressStreet));
                cmd.Parameters.Add(new SqlParameter("@address_town", _addressTown));
                cmd.Parameters.Add(new SqlParameter("@address_county", _addressCounty));
                cmd.Parameters.Add(new SqlParameter("@address_postcode", _addressPostCode));
                cmd.Parameters.Add(new SqlParameter("@gender", _gender));
                cmd.Parameters.Add(new SqlParameter("@age", _age));
                cmd.Parameters.Add(new SqlParameter("@date_of_birth", _dateOfBirth));
                cmd.Parameters.Add(new SqlParameter("@address_external", Address_IsStoredExternally));
                cmd.Parameters.Add(new SqlParameter("@information_unavailable", InformationUnvailable));

                cmd.ExecuteNonQuery();


                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }
    }


    private void PushJavascriptToMain(string script)
    {
        _currentControl.PushJavascriptToPage(script);
    }

    private string ReturnGender(string gender)
    {

        if(gender == "0")
        {
            gender = "Male";
        }
        else if(gender == "1")
        {
            gender = "Female";
        }
        else
        {
            gender = "ERR";
        }

        return gender;
    }

    public enum Type {
        PersonAffected = 0,
        Reporter = 1
    }

    public enum IncidentType
    {
        Injury = 0,
        NearMiss = 1,
        PropertyDamage = 2,
        Illness = 3,
        Violence = 4,
        DangerousOccurrence = 5
    }

    public enum Status
    {
        Edit = 0,
        Report = 1
    }


}