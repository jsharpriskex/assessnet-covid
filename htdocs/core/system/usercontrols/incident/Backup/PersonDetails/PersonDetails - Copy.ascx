﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PersonDetails - Copy.ascx.cs" Inherits="PersonDetails_UserControl" %>


<div id="Parent" style="display:none;" runat="server">


<div class="row" id="Unavailable_Parent" runat="server" visible="false">
    <div class="alert alert-warning <%=CurrentPerson.CurrentType%>_class unavailable_class">
        <div class="row">
            <div class="col-xs-10">
                <div class="media">
                    <div class="media-left media-middle">
                        <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">DETAILS UNAVAILABLE</h4>
                        <p id="<%=CurrentPerson.CurrentType%>_NotSelectedText">Are the details for the person currently unavailable? Please click the slider to the right if the details are unavailable.</p>
                        <p id="<%=CurrentPerson.CurrentType%>_SelectedText" style="display:none;">This persons details are currently unavailable.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-2 text-right" style="padding-top: 15px;" id="detailsUnavailable_Label" runat="server">
                <label class="switch">
                    <asp:CheckBox runat="server" ID="detailsUnavailable_Checkbox" />
                    <span class="slider round" onclick=" Unavailable_ToggleCheckbox_<%=CurrentPerson.CurrentType%>();"></span>
                </label>

            </div>
        </div>
    </div>
</div>



<div class="row" id="<%=this.ID%>_ContentRow">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

        <div class="panel panel-default panel-subpanel" id="headingOneContainer" runat="server">
            <div class="panel-heading click" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" onclick="headingOne_clicked();">
                <h4 class="panel-title">
                    <a role="button" aria-expanded="true" aria-controls="collapseOne">
                        <i class="fa fa-bars" aria-hidden="true"></i>Person Details

                    </a>
                </h4>
            </div>
            <div runat="server" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">

                    <div class="row" id="AssociatedAccount_Parent_Div" runat="server">
                        <div class="col-xs-12">
                            <div class="row" id="<%=this.ID%>_AssociatedAccount_Parent">
                                <div class="col-xs-12">
                                    <div class="alert alert-warning <%=this.ID%>_AssociatedAccount">
                                        <div class="row">
                                            <div class="col-xs-10">
                                                <div class="media">
                                                    <div class="media-left media-middle">
                                                        <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading">USE DETAILS FROM ACCOUNT</h4>
                                                        <p id="<%=this.ID%>_AssociatedAccount_NotSelectedText">Would you like to link this record to an associated AssessNET account? If so, please click on the slider to the right.</p>
                                                        <p id="<%=this.ID%>_AssociatedAccount_SelectedText" style="display:none;">This persons details are currently linked to an AssessNET account.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-2 text-right" style="padding-top: 15px;" id="associatedAccount_Label" runat="server">
                                                <label class="switch">
                                                    <asp:CheckBox runat="server" ID="associatedAccount_Checkbox" />
                                                    <span class="slider round" onclick="AssociatedAccount_ToggleCheckbox_<%=this.ID%>();"></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="<%=this.ID%>_AssociatedUserParent">
                        <div class="row">
                            <div class="col-xs-6 col-xs-offset-3">
                                <div class="form-group">
                                    <label for="reportedBy"> Reported by
                                    </label>
                                            
                                    <telerik:RadComboBox  ID="associatedUser_RadComboBox" runat="server"
                                                            Width="100%" 
                                                            EnableVirtualScrolling="true"
                                                            DropDownWidth="500px" 
                                                            DropDownHeight="300px"   
                                                            Skin="Bootstrap" 
                                                            HighlightTemplatedItems="true" 
                                                            filter="Contains" 
                                                            EmptyMessage="Please type a name or select a user"
                                                            EnableLoadOnDemand="True"  
                                                            AppendDataBoundItems="True" 
                                                            AllowCustomText="false" 
                                                            Height="300" 
                                                            showmoreresultsbox="true" 
                                                            OnItemsRequested="generic_Blank_ItemsRequested"> 
                                        <ItemTemplate>
                                                                        
                                            <%# Container.DataItem != null ? DataBinder.Eval(Container.DataItem, "user_details") : DataBinder.Eval(Container, "Text") %>
                                                                                            
                                        </ItemTemplate>
                                    </telerik:RadComboBox>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="<%=this.ID%>_BasicInfo">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="forename_TextBox">Forename</label>
                                            <asp:TextBox runat="server" class="form-control" ID="forename_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="surname_TextBox">Surname</label>
                                            <asp:TextBox runat="server" class="form-control" ID="surname_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="personStatus_DropDownList"><%=PersonStatusText%></label>
                                            <asp:DropDownList ID="personStatus_DropDownList" runat="server" CssClass="form-control" EnableViewState="true"></asp:DropDownList>
                                        </div>

                                    </div>

                                    

                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="gender_DropDownList">Gender</label>
                                            <asp:DropDownList ID="gender_DropDownList" runat="server" CssClass="form-control" EnableViewState="false">
                                                <asp:ListItem Value="na" Text="Please Select..."></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Male"></asp:ListItem>
                                                <asp:ListItem Value="1" Text="Female"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    
                                </div>



                                <div class="row">
                                    
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="occupation_TextBox">Occupation / Job Title</label>
                                            <asp:TextBox runat="server" class="form-control" ID="occupation_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                    
                                </div>

                            </div>
                        </div>

                    </div>
                    
                     <hr />

                    <div>
                       


                            <div class="row" id="External_Parent" runat="server" visible="false">
                                <div class="col-xs-12">
                                    <div class="alert alert-warning <%=this.ID%>_External">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <div class="media">
                                                <div class="media-left media-middle">
                                                    <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                                                </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">ADDRESS INFORMATION IS EXTERNAL</h4>
                                                    <p id="<%=this.ID%>_External_NotSelectedText">Are the details for the person held externally of AssessNET. Please click the slider to the right if so.</p>
                                                    <p id="<%=this.ID%>_External_SelectedText" style="display:none;">This persons address details are currently held externally.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 text-right" style="padding-top: 15px;" id="addressExternal_Label" runat="server">
                                            <label class="switch">
                                                <asp:CheckBox runat="server" ID="addressExternal_CheckBox" />
                                                <span class="slider round" onclick="External_ToggleCheckbox_<%=this.ID%>();"></span>
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                </div>
                                

                        </div>

                        <div id="<%=this.ID%>_Address">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="street_TextBox">Street</label>
                                        <asp:TextBox runat="server" class="form-control" ID="street_TextBox" aria-describedby="title"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="town_TextBox">Town</label>
                                        <asp:TextBox runat="server" class="form-control" ID="town_TextBox" aria-describedby="title"></asp:TextBox>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="county_TextBox">County</label>
                                        <asp:TextBox runat="server" class="form-control" ID="county_TextBox" aria-describedby="title"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="postcode_TextBox">Postcode</label>
                                        <asp:TextBox runat="server" class="form-control" ID="postcode_TextBox" aria-describedby="title"></asp:TextBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    

                    
                    <div id="<%=this.ID%>_contactInfoAndAge">

                  <hr />

                        <div class="row" id="contactDetails_Div" runat="server" visible="false">
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="number_TextBox">Contact Number</label>
                                            <asp:TextBox runat="server" class="form-control" ID="number_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="email_TextBox">Contact Email</label>
                                            <asp:TextBox runat="server" class="form-control" ID="email_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>

                                <div class="row" runat="server" id="contactDetails_StaffID_Div" visible="false">
                                    <div class="col-xs-6">

                                        <div class="form-group">
                                            <label for="staffID_TextBox">Any Identification Numbers</label>
                                            <asp:TextBox runat="server" class="form-control" ID="staffID_TextBox" aria-describedby="title"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="row" id="age_Div" runat="server" >
                            <div class="col-xs-12">

                                <div class="row">
                                    <div class="col-xs-6">

                                        <div class="form-group" id="<%=this.ID %>_ageUser_Div">
                                            <label for="age_RadNumericTextBox">Age</label>
                                            <telerik:RadNumericTextBox ID="age_RadNumericTextBox" MinValue="0" MaxValue="120" runat="server" Skin="Bootstrap" Width="100%" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                        </div>

                                        <div class="form-group" id="<%=this.ID %>_dateOfBirth_Div" style="display:none;">
                                            <label for="dateOfBirth_RadDatePicker">Date of Birth</label>
                                            <telerik:RadDatePicker ID="dateOfBirth_RadDatePicker" runat="server" Skin="Bootstrap" Width="100%"></telerik:RadDatePicker>
                                        </div>

                                    </div>
                                    <div class="col-xs-6">

                                        <div class="alert alert-warning" style="height: 65px;" id="<%=this.ID %>_dateOfBirth_Alert">
                                            <div class="row">
                                                <div class="col-xs-5" style="padding-top:5px;">
                                                    <p>Use Date of Birth?</p>
                                                </div>
                                        
                                                <div class="col-xs-7 text-right">
                                                    <label class="switch">
                                                        <asp:CheckBox runat="server" ID="toggleDateOfBirth_CheckBox" />
                                                        <span class="slider round" onclick="ToggleDateOfBirthCheckBox_<%=this.ID %>();"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                       
                    </div>


                    <asp:CustomValidator ID="personDetails_CustomValidator" runat="server" ValidationGroup="PersonDetails" ></asp:CustomValidator>

                </div>
            </div>
        </div>



    </div>
</div>
</div>



<div id="Report" runat="server">
    <div class="row">
        <div class="col-xs-12">

            <div class="alert alert-warning" id="personDetailsUnavailable_Report" runat="server" visible="false">
                <div class="row">
                    <div class="col-xs-10">
                        <div class="media">
                            <div class="media-left media-middle">
                                <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">DETAILS UNAVAILABLE</h4>
                                <p>This persons details are currently unavailable.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group" id="associatedUser_Report" runat="server" visible="false">
                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr id="associatedUserDiv" runat="server">
                            <th style="width: 180px;">Associated User</th>
                            <td><asp:Literal ID="associatedUser_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="form-group" id="mainData_Report" runat="server">

                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr id="nameDiv" runat="server">
                            <th style="width: 180px;">Name</th>
                            <td><asp:Literal ID="name_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="statusDiv" runat="server">
                            <th style="width: 180px;">Status</th>
                            <td><asp:Literal ID="status_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="genderDiv" runat="server">
                            <th style="width: 180px;">Gender</th>
                            <td><asp:Literal ID="gender_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="occupationDiv" runat="server">
                            <th style="width: 180px;">Occupation / Job Title</th>
                            <td><asp:Literal ID="occupation_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>


                    <div class="alert alert-warning" id="addressIsStoredExternally_Report" runat="server" visible="false">
                        <div class="row">
                            <div class="col-xs-10">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <i class="fa fa-eye fa-3x" aria-hidden="true"></i>

                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">ADDRESS INFORMATION IS EXTERNAL</h4>
                                        <p>This persons address details are currently held externally.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                <table class="table table-bordered table-searchstyle01" id="addressTable" runat="server">
                    <tbody>
                        <tr id="streetDiv" runat="server">
                            <th style="width: 180px;">Street</th>
                            <td><asp:Literal ID="street_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="townDiv" runat="server">
                            <th style="width: 180px;">Town</th>
                            <td><asp:Literal ID="town_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="countyDiv" runat="server">
                            <th style="width: 180px;">County</th>
                            <td><asp:Literal ID="county_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="postCodeDiv" runat="server">
                            <th style="width: 180px;">Postcode</th>
                            <td><asp:Literal ID="postCode_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr id="numberDiv" runat="server">
                            <th style="width: 180px;">Contact Number</th>
                            <td><asp:Literal ID="number_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="emailDiv" runat="server">
                            <th style="width: 180px;">Contact Email</th>
                            <td><asp:Literal ID="email_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="idNumbersDiv" runat="server">
                            <th style="width: 180px;">Identification Numbers</th>
                            <td><asp:Literal ID="idNumbers_Literal" runat="server"></asp:Literal></td>
                        </tr>
                        <tr id="ageReportDiv" runat="server">
                            <th style="width: 180px;">Age</th>
                            <td><asp:Literal ID="ageReport_Literal" runat="server"></asp:Literal></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>


<div runat="server" id="editScript">

 <!-- alert -->
<div class="modal fade" id="<%=this.ID%>_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Invalid Data Entry</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        Please enter valid information within the fields that have been highlighted in <span style="color: #c00c00; font-weight: bold;">red</span>.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                         
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->

    
<script type="text/javascript">

    
    // ------ VALIDATION
    function <%=this.ID%>_ValidatePersonDetails(sender, args){

        var IsValid = true;

        // Start with associated user, this comes from JSON data.
        if ($("#<%=this.ID%>_associatedUser_RadComboBox").is(":visible")) {
            var associatedUserJSON = $("#<%=this.ID%>_associatedUser_RadComboBox_ClientState").val();

            var IsValidUserDropDown = true;

            if (associatedUserJSON != "" && associatedUserJSON != undefined) {
                var parsedJSON = $.parseJSON(associatedUserJSON);
                console.log(parsedJSON);
                if (parsedJSON.value == undefined) {
                    IsValidUserDropDown = false;
                }
            }
            else {
                IsValidUserDropDown = false
            }

            if (IsValidUserDropDown == false) {
                IsValid = false;
                $("#<%=this.ID%>_associatedUser_RadComboBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_associatedUser_RadComboBox").removeClass("inputError");
            }
            
        }

        // First Name
        if ($("#<%=this.ID%>_forename_TextBox").is(":visible")) {
            var firstName = $("#<%=this.ID%>_forename_TextBox").val();
            if (firstName == "") {
                IsValid = false;
                $("#<%=this.ID%>_forename_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_forename_TextBox").removeClass("inputError");
            }
        }

        // Surname
        if ($("#<%=this.ID%>_surname_TextBox").is(":visible")) {
            var surname = $("#<%=this.ID%>_surname_TextBox").val();
            if (surname == "") {
                IsValid = false;
                $("#<%=this.ID%>_surname_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_surname_TextBox").removeClass("inputError");
            }
        }

        // Occupation
        if ($("#<%=this.ID%>_surname_TextBox").is(":visible")) {
            var occupation = $("#<%=this.ID%>_occupation_TextBox").val();
            if (occupation == "") {
                IsValid = false;
                $("#<%=this.ID%>_occupation_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_occupation_TextBox").removeClass("inputError");
            }
        }

        // ID
        if ($("#<%=this.ID%>_staffID_TextBox").is(":visible")) {
            var staffID = $("#<%=this.ID%>_staffID_TextBox").val();
            if (staffID == "") {
                IsValid = false;
                $("#<%=this.ID%>_staffID_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_staffID_TextBox").removeClass("inputError");
            }
        }

        // Person Status
        if ($("#<%=this.ID%>_personStatus_DropDownList").is(":visible")) {
            var status = $("#<%=this.ID%>_personStatus_DropDownList").val();
            if (status == "na") {
                IsValid = false;
                $("#<%=this.ID%>_personStatus_DropDownList").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_personStatus_DropDownList").removeClass("inputError");
            }
        }

        // Gender
        if ($("#<%=this.ID%>_gender_DropDownList").is(":visible")) {
            var gender = $("#<%=this.ID%>_gender_DropDownList").val();
            if (gender == "na") {
                IsValid = false;
                $("#<%=this.ID%>_gender_DropDownList").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_gender_DropDownList").removeClass("inputError");
            }
        }

        // Street
        if ($("#<%=this.ID%>_street_TextBox").is(":visible")) {
            var street = $("#<%=this.ID%>_street_TextBox").val();
            if (street == "") {
                IsValid = false;
                $("#<%=this.ID%>_street_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_street_TextBox").removeClass("inputError");
            }
        }

         // Town
        if ($("#<%=this.ID%>_town_TextBox").is(":visible")) {
            var town = $("#<%=this.ID%>_town_TextBox").val();
            if (town == "") {
                IsValid = false;
                $("#<%=this.ID%>_town_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_town_TextBox").removeClass("inputError");
            }
        }

         // County
        if ($("#<%=this.ID%>_county_TextBox").is(":visible")) {
            var county = $("#<%=this.ID%>_county_TextBox").val();
            if (county == "") {
                IsValid = false;
                $("#<%=this.ID%>_county_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_county_TextBox").removeClass("inputError");
            }
        }

        // postCode
        if ($("#<%=this.ID%>_postcode_TextBox").is(":visible")) {
            var postCode = $("#<%=this.ID%>_postcode_TextBox").val();
            if (postCode == "") {
                IsValid = false;
                $("#<%=this.ID%>_postcode_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_postcode_TextBox").removeClass("inputError");
            }
        }

        // contact number
        if ($("#<%=this.ID%>_number_TextBox").is(":visible")) {
            var number = $("#<%=this.ID%>_number_TextBox").val();
            if (number == "") {
                IsValid = false;
                $("#<%=this.ID%>_number_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_number_TextBox").removeClass("inputError");
            }
        }

        // contact email
        if ($("#<%=this.ID%>_email_TextBox").is(":visible")) {
            var number = $("#<%=this.ID%>_email_TextBox").val();
            if (number == "") {
                IsValid = false;
                $("#<%=this.ID%>_email_TextBox").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_email_TextBox").removeClass("inputError");
            }
        }

         // age
        if ($("#<%=this.ID%>_age_Div").is(":visible")) {
            var age = $("#<%=this.ID%>_age_RadNumericTextBox").val();
            if (age == "0") {
                IsValid = false;
                $("#<%=this.ID%>_age_RadNumericTextBox_wrapper").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_age_RadNumericTextBox_wrapper").removeClass("inputError");
            }
        }

         // DOB
        if ($("#<%=this.ID%>_dateOfBirth_Div").is(":visible")) {
            var dateOfBirthJSON = $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_ClientState").val();

            var IsValidDateOfBirth = true;

            if (dateOfBirthJSON != "" && dateOfBirthJSON != undefined) {
                var parsedDOBJSON = $.parseJSON(dateOfBirthJSON);

                if (parsedDOBJSON.valueAsString == undefined || parsedDOBJSON.valueAsString == "") {
                    IsValidDateOfBirth = false;
                }
            }
            else {
                IsValidDateOfBirth = false
            }


            if (IsValidDateOfBirth == false) {
                IsValid = false;
                $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_wrapper").addClass("inputError");
            }
            else {
                $("#<%=this.ID%>_dateOfBirth_RadDatePicker_dateInput_wrapper").removeClass("inputError");
            }
            
        }


        if (IsValid == false) {
            $("#<%=this.ID%>_alert").modal('show');
        }

        args.IsValid = IsValid;

    }











    // ---- UNAVAILABLE CHECKBOX -----

    $('#<%=this.ID%>_detailsUnavailable_Checkbox').change(function() {
        Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>();
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    });


    function Unavailable_ToggleCheckbox_<%=CurrentPerson.CurrentType%>() {

        var isChecked = $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>() {


        var isChecked = $("#<%=this.ID %>_<%=detailsUnavailable_Checkbox.ID %>").is(':checked');


        if (!isChecked) {
            $("#<%=CurrentPerson.CurrentType%>_NotSelectedText").show();
            $("#<%=CurrentPerson.CurrentType%>_SelectedText").hide();

            $(".<%=CurrentPerson.CurrentType%>_class").removeClass("alert-danger");
            $(".<%=CurrentPerson.CurrentType%>_class").addClass("alert-warning");

            $("#<%=this.ID%>_ContentRow").show();

            $("#<%=this.ID%>_External_Parent").show()

            
        }
        else {
            $("#<%=CurrentPerson.CurrentType%>_NotSelectedText").hide();
            $("#<%=CurrentPerson.CurrentType%>_SelectedText").show();

            $(".<%=CurrentPerson.CurrentType%>_class").addClass("alert-danger");
            $(".<%=CurrentPerson.CurrentType%>_class").removeClass("alert-warning");

            $("#<%=this.ID%>_ContentRow").hide();

            $("#<%=this.ID%>_External_Parent").hide()

            
        }


    }


    //  ---- ASSOCIATED ACCOUNT CHECKBOX -----
    $('#<%=this.ID%>_associatedAccount_Checkbox').change(function() {
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    });

     function AssociatedAccount_ToggleCheckbox_<%=this.ID%>() {

        var isChecked = $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function AssociatedAccount_ToggleDetails_<%=this.ID%>() {


        var isChecked = $("#<%=this.ID %>_<%=associatedAccount_Checkbox.ID %>").is(':checked');


        if (!isChecked) {
            $("#<%=this.ID%>_AssociatedAccount_NotSelectedText").show();
            $("#<%=this.ID%>_AssociatedAccount_SelectedText").hide();

            $(".<%=this.ID%>_AssociatedAccount").removeClass("alert-danger");
            $(".<%=this.ID%>_AssociatedAccount").addClass("alert-warning");

            $("#<%=this.ID%>_BasicInfo").show();
            $("#<%=this.ID%>_External_Parent").show();
            $("#<%=this.ID%>_Address").show();
            $("#<%=this.ID%>_contactInfoAndAge").show();

            $("#<%=this.ID%>_AssociatedUserParent").hide();

            External_ToggleDetails_<%=this.ID%>();
            
        }
        else {
            $("#<%=this.ID%>_AssociatedAccount_NotSelectedText").hide();
            $("#<%=this.ID%>_AssociatedAccount_SelectedText").show();

            $(".<%=this.ID%>_AssociatedAccount").addClass("alert-danger");
            $(".<%=this.ID%>_AssociatedAccount").removeClass("alert-warning");

            $("#<%=this.ID%>_BasicInfo").hide();
            $("#<%=this.ID%>_External_Parent").hide();
            $("#<%=this.ID%>_Address").hide();
            $("#<%=this.ID%>_contactInfoAndAge").hide();

            $("#<%=this.ID%>_AssociatedUserParent").show();
            
        }


    }


    // ---- EXTERNAL CHECKBOX -----


     $('#<%=this.ID%>_addressExternal_CheckBox').change(function() {
         External_ToggleDetails_<%=this.ID%>();
    });



    function External_ToggleCheckbox_<%=this.ID%>() {

        var isChecked = $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").removeAttr('Checked','Checked');
        }
        

    }

    function External_ToggleDetails_<%=this.ID%>() {

        

        var isChecked = $("#<%=this.ID %>_<%=addressExternal_CheckBox.ID %>").is(':checked');

        if (!isChecked) {
            $("#<%=this.ID%>_External_NotSelectedText").show();
            $("#<%=this.ID%>_External_SelectedText").hide();

            $(".<%=this.ID%>_External").removeClass("alert-danger");
            $(".<%=this.ID%>_External").addClass("alert-warning");

            $("#<%=this.ID%>_Address").show();

            $("#<%=this.ID%>_Unavailable_Parent").show()
        }
        else {
            $("#<%=this.ID%>_External_NotSelectedText").hide();
            $("#<%=this.ID%>_External_SelectedText").show();

            $(".<%=this.ID%>_External").addClass("alert-danger");
            $(".<%=this.ID%>_External").removeClass("alert-warning");

            $("#<%=this.ID%>_Address").hide();

            $("#<%=this.ID%>_Unavailable_Parent").hide()

        }


    }


    // ---- DATE OF BIRTH CHECKBOX -----

    $('#<%=this.ID%>_toggleDateOfBirth_CheckBox').change(function() {
         ShowDateOfBirth_<%=this.ID %>();
    });

    function ToggleDateOfBirthCheckBox_<%=this.ID %>() {
        var isChecked = $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").is(':checked');

        if (isChecked || isChecked == null) {
            $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").attr('Checked','Checked');
        }
        else {
            $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").removeAttr('Checked','Checked');
        }

         
    }

    function ShowDateOfBirth_<%=this.ID %>() {
        var isChecked = $("#<%=this.ID %>_<%=toggleDateOfBirth_CheckBox.ID %>").is(':checked');

        if (!isChecked) {
            $("#<%=this.ID %>_dateOfBirth_Div").hide();
            $("#<%=this.ID %>_ageUser_Div").show();

            $("#<%=this.ID %>_dateOfBirth_Alert").removeClass("alert-danger");
            $("#<%=this.ID %>_dateOfBirth_Alert").addClass("alert-warning");

        }
        else {
            $("#<%=this.ID %>_dateOfBirth_Div").show();
            $("#<%=this.ID %>_ageUser_Div").hide();

            $("#<%=this.ID %>_dateOfBirth_Alert").addClass("alert-danger");
            $("#<%=this.ID %>_dateOfBirth_Alert").removeClass("alert-warning");


        }

    }


    function RunAllChecks_<%=this.ID%>() {      
        External_ToggleDetails_<%=this.ID%>();
        Unavailable_ToggleDetails_<%=CurrentPerson.CurrentType%>();
        ShowDateOfBirth_<%=this.ID%>();
        AssociatedAccount_ToggleDetails_<%=this.ID%>();
    }

    $( document ).ready(function() {
        BindControlEvents_<%=this.ID%>();
    });

    var prm_<%=this.ID%> = Sys.WebForms.PageRequestManager.getInstance();

    prm_<%=this.ID%>.add_endRequest(function () {
        BindControlEvents_<%=this.ID%>();
    });

    function BindControlEvents_<%=this.ID%>() {
        $("#<%=this.ID %>_Parent").show();
        RunAllChecks_<%=this.ID%>();
    }
    $("#<%=this.ID %>_Parent").show();
    RunAllChecks_<%=this.ID%>();

</script>

</div>