﻿function commencedDate_RadDatePicker_OnDateSelected(sender, e) {
 
    ChangeEndDateFromCommencedDate();
}


function ChangeEndDateFromCommencedDate() {


    var commencedDate_Box = $find("commencedDate_RadDatePicker");
    var endDate_Box = $find("CurrentLostTime_returnDate_RadDatePicker");

    var endDate = endDate_Box.get_selectedDate();

    var commencedDate = new Date();
    commencedDate = commencedDate_Box.get_selectedDate();
    commencedDate.setDate(commencedDate.getDate() + 1);


    endDate_Box.set_minDate(commencedDate);

}


function ToggleReturnInformation() {
    var currentSelection = $("#returnType_DropDownList").val();

    if (currentSelection == "na" || currentSelection == "3") {
        $("#returnDate_Div").hide();
        $("#returnHours_Div").hide();
    }
    else if (currentSelection == "1") {
        $("#returnDate_Div").hide();
        $("#returnHours_Div").show();
    }
    else if (currentSelection == "2") {
        $("#returnDate_Div").show();
        $("#returnHours_Div").hide();
    }
}

function HideLostTimeModal() {
    $('#lostTimeModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}


function ValidateLostTime(sender, args) {

    var IsValid = true;


    // description
    if ($("#description_TextBox").is(":visible")) {
        var description = $("#description_TextBox").val();
        if (description == "") {
            IsValid = false;
            $("#description_TextBox").addClass("inputError");
        }
        else {
            $("#description_TextBox").removeClass("inputError");
        }
    }


    // Lost Time Type
    if ($("#lostTimeType_DropDownList").is(":visible")) {
        var lostTimeType = $("#lostTimeType_DropDownList").val();
        if (lostTimeType == "na") {
            IsValid = false;
            $("#lostTimeType_DropDownList").addClass("inputError");
        }
        else {
            $("#lostTimeType_DropDownList").removeClass("inputError");
        }
    }


    // Date Lost Time Commenced
    if ($("#commencedDate_RadDatePicker_wrapper").is(":visible")) {
        var ltaCommencedJSON = $("#CurrentLostTime_commencedDate_RadDatePicker_dateInput_ClientState").val();

        var IsValidCommencedDate = true;

        if (ltaCommencedJSON != "" && ltaCommencedJSON != undefined) {
            var parsedltaCommencedJSON = $.parseJSON(ltaCommencedJSON);

            if (parsedltaCommencedJSON.valueAsString == undefined || parsedltaCommencedJSON.valueAsString == "") {
                IsValidCommencedDate = false;
            }
        }
        else {
            IsValidCommencedDate = false
        }


        if (IsValidCommencedDate == false) {
            IsValid = false;
            $("#CurrentLostTime_commencedDate_RadDatePicker_dateInput_wrapper").addClass("inputError");
        }
        else {
            $("#CurrentLostTime_commencedDate_RadDatePicker_dateInput_wrapper").removeClass("inputError");
        }

    }

    // Lost Time Return Type
    if ($("#returnType_DropDownList").is(":visible")) {
        var lostTimeReturnType = $("#returnType_DropDownList").val();
        if (lostTimeReturnType == "na") {
            IsValid = false;
            $("#returnType_DropDownList").addClass("inputError");
        }
        else {
            $("#returnType_DropDownList").removeClass("inputError");
        }
    }

    // Lost Time Hours
    if ($("#hoursLostTime_DropDownList").is(":visible")) {
        var lostTimeHours = $("#hoursLostTime_DropDownList").val();
        if (lostTimeHours == "na") {
            IsValid = false;
            $("#hoursLostTime_DropDownList").addClass("inputError");
        }
        else {
            $("#hoursLostTime_DropDownList").removeClass("inputError");
        }
    }

    // Return Date
    if ($("#CurrentLostTime_returnDate_RadDatePicker_dateInput_wrapper").is(":visible")) {
        var ltaReturnJSON = $("#CurrentLostTime_returnDate_RadDatePicker_dateInput_ClientState").val();

        var IsValidReturnDate = true;

        if (ltaReturnJSON != "" && ltaReturnJSON != undefined) {
            var parsedltaReturnJSON = $.parseJSON(ltaReturnJSON);

            if (parsedltaReturnJSON.valueAsString == undefined || parsedltaReturnJSON.valueAsString == "") {
                IsValidReturnDate = false;
            }
        }
        else {
            IsValidReturnDate = false
        }


        if (IsValidReturnDate == false) {
            IsValid = false;
            $("#CurrentLostTime_returnDate_RadDatePicker_dateInput_wrapper").addClass("inputError");
        }
        else {
            $("#CurrentLostTime_returnDate_RadDatePicker_dateInput_wrapper").removeClass("inputError");
        }

    }


    if (IsValid == false) {
        $("#alert").modal('show');
    }

    args.IsValid = IsValid;

}



function ValidateLostTimeExternal(sender, args) {

    var IsValid = true;

    var countOfLTA = $(".lta_row").length;

    if (countOfLTA === 0) {
        IsValid = false;
        $(".alertForLostTime").addClass("inputError");
    }
    else {
        $(".alertForLostTime").removeClass("inputError");
    }


    if (IsValid == false) {
        $("#alert").modal('show');
    }

    args.IsValid = IsValid;

}

function RemoveErrorLTA() {
    $(".alertForLostTime").removeClass("inputError");
}


function ShowDatePopup(sender) {
    console.log(sender.id);
}