﻿


$(document).ready(function () {
    BindControlEvents();
});

//var prm = Sys.WebForms.PageRequestManager.getInstance();

//prm.add_endRequest(function () {
//    BindControlEvents();
//});

function BindControlEvents() {
    
    RunAllChecks();
}


// Modifications
$(document).on('change', '#CurrentEquipment_modificationsY_RadioButton', function () {
    ToggleModificationTextBox();
});
$(document).on('change', '#CurrentEquipment_modificationsN_RadioButton', function () {
    ToggleModificationTextBox();
});


function ToggleModificationTextBox() {
        
    if ($('#CurrentEquipment_modificationsY_RadioButton').is(':checked')) {
        $("#modificationsObserved").show();
    } else {
        $("#modificationsObserved").hide();
    }

}





 function ValidateEquipment(sender, args) {


            var isValid = ValidateEquipmentForm();

            if (isValid === false) {
                // alert("Invalid Data Entry. Please enter valid information for the sections highlighted above in red.");
                $("#alert").modal('show');
            }

            args.IsValid = isValid;

        }

        function ValidateEquipmentForm() {

            var isValid = true;
            var selectedValue = "";
            var currentItem;

            // Type
            currentItem = $("[id*='type_DropDownList']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "na") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }


            // Owner
            currentItem = $("[id*='owner_DropDownList']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "na") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }

            // Manufacturer
            currentItem = $("[id*='manufacturer_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }

            // Model
            currentItem = $("[id*='model_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }

            // Mileage
            currentItem = $("[id*='mileage_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }

            // Registration
            currentItem = $("[id*='registration_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }


            // Vehicle Number
            currentItem = $("[id*='number_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }


            // Year of Manufacture
            currentItem = $("[id*='yearOfManufacture_DropDownList']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "na") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }

            // Modifications
            currentItem = $("[id*='modificationsObserved_TextBox']");
            if (currentItem.is(":visible")) {

                selectedValue = currentItem.val();

                if (selectedValue === "") {
                    isValid = false;
                    currentItem.addClass("inputError");
                } else {
                    currentItem.removeClass("inputError");
                }

            }


            // Loop through each row
            $('.inputRow:visible').each(function (i, obj) {
                var $this = $(this);
                var foundCheckboxes = false;
                var foundCheckedbox = false;


                // Radio Buttons
                // Loop through any found radios
                $this.find("input:radio").each(function () {

                    foundCheckboxes = true;

                    // Ok we know there are checkboxes, so we have to see if one has been selected
                    if ($(this).is(':checked')) {
                        foundCheckedbox = true;
                    }


                });
                if (foundCheckboxes == true && foundCheckedbox == false) {
                    isValid = false;
                    $this.addClass("inputError");
                }
                else if (foundCheckboxes == true && foundCheckedbox == true) {
                    $this.removeClass("inputError");
                }

            });

            return isValid;

        }