﻿
function HideCostAnalysisModal() {
    $('#costAnalysisModal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}


function ValidateCostAnalysis(sender, args) {

    var IsValid = true;


    // description
    if ($("#costDescription_TextBox").is(":visible")) {
        var description = $("#costDescription_TextBox").val();
        if (description == "") {
            IsValid = false;
            $("#costDescription_TextBox").addClass("inputError");
        }
        else {
            $("#costDescription_TextBox").removeClass("inputError");
        }
    }


    // Cost Type
    if ($("#costAnalysisType_DropDownList").is(":visible")) {
        var costAnalysisType = $("#costAnalysisType_DropDownList").val();
        if (costAnalysisType == "na") {
            IsValid = false;
            $("#costAnalysisType_DropDownList").addClass("inputError");
        }
        else {
            $("#costAnalysisType_DropDownList").removeClass("inputError");
        }
    }


    //cost
    // Date of cost
    if ($("#cost_RadNumericTextBox_wrapper").is(":visible")) {
        var costJSON = $("#cost_RadNumericTextBox_ClientState").val();

        var IsValidCost = true;

        if (costJSON != "" && costJSON != undefined) {
            var parsedCostJSON = $.parseJSON(costJSON);

            if (parsedCostJSON.valueAsString == undefined || parsedCostJSON.valueAsString == "") {
                IsValidCost = false;
            }
        }
        else {
            IsValidCost = false
        }


        if (IsValidCost == false) {
            IsValid = false;
            $("#cost_RadNumericTextBox_wrapper").addClass("inputError");
        }
        else {
            $("#cost_RadNumericTextBox_wrapper").removeClass("inputError");
        }

    }

    // Date of cost
    if ($("#CurrentCostAnalysis_dateRecorded_RadDatePicker_wrapper").is(":visible")) {
        var ltaCommencedJSON = $("#CurrentCostAnalysis_dateRecorded_RadDatePicker_dateInput_ClientState").val();

        var IsValidCommencedDate = true;

        if (ltaCommencedJSON != "" && ltaCommencedJSON != undefined) {
            var parsedltaCommencedJSON = $.parseJSON(ltaCommencedJSON);

            if (parsedltaCommencedJSON.valueAsString == undefined || parsedltaCommencedJSON.valueAsString == "") {
                IsValidCommencedDate = false;
            }
        }
        else {
            IsValidCommencedDate = false
        }


        if (IsValidCommencedDate == false) {
            IsValid = false;
            $("#CurrentCostAnalysis_dateRecorded_RadDatePicker_dateInput_wrapper").addClass("inputError");
        }
        else {
            $("#CurrentCostAnalysis_dateRecorded_RadDatePicker_dateInput_wrapper").removeClass("inputError");
        }

    }
    
    if (IsValid == false) {
        $("#alert").modal('show');
    }

    args.IsValid = IsValid;

}
