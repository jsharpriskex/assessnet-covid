﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Witness.ascx.cs" Inherits="Witness_UserControl" %>

<script>

    function InitiateDropZone() {

        var incidentReference = "<%=IncidentReference%>";
        var incidentCentre = "<%=IncidentCentre%>";
        var witnessID = $("#witnessID_HiddenField").val();


        createDropzone_3Reference("WIT", "ACC", incidentCentre, incidentReference, witnessID, false);

    }

</script>

<div id="editContent" runat="server" visible="false">

    <asp:UpdatePanel ID="witnessList_UpdatePanel" UpdateMode="Conditional" runat="server" ChildrenAsTriggers="False">
        <ContentTemplate>
            
            <div class="row" id="buttonMain_Div" runat="server">
                <div class="col-xs-12 text-right" style="padding-bottom: 10px;">
                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnClick="addLostTime_Button_Click" OnClientClick="RemoveErrorLTA()"><i class="fa fa-plus"></i> <%=ts.GetResource("button_witness_add")%></asp:LinkButton>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <asp:Repeater ID="lostTimeList_Repeater" runat="server" OnItemCommand="lostTimeList_Repeater_ItemCommand" OnItemDataBound="lostTimeList_Repeater_OnItemDataBound">
                        <HeaderTemplate>
                            <table class="table table-condensed table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="30%"><%#ts.GetResource("lbl_witness")%></th>
                                        <th width="50%"><%#ts.GetResource("lbl_witness_contact")%></th>
                                        <th width="10%" class="text-center"><%#ts.GetResource("lbl_witness_statement")%></th>
                                        <th width="10%" class="text-center"><%#ts.Translate("Options") %></th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="lta_row">
                                <td><asp:Literal runat="server" id="itemLanguageBadge"></asp:Literal> <%#Eval("first_name") %> <%#Eval("surname") %></td>
                                <td><div runat="server" id="repeaterInfo_div" visible="false"><%#Eval("contact_num") %> / <%#Eval("contact_email") %></div><div runat="server" visible="False" id="repeaterInfoHashed_Div">********************* / *********************</div></td>
                                <td class="text-center">
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton runat="server" CssClass="btn btn-sm btn-primary" CommandArgument='<%#Eval("id") %>' CommandName="ViewWitness" ID="LinkButton2"><%=ts.Translate("View") %></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </td>
                                <td class="text-center">
                                    <button type="button" disabled class="btn btn-sm sm-primary" id="noOptions_Button" runat="server">No Options Available</button>
                                    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:LinkButton runat="server" CssClass="btn btn-sm btn-warning" CommandArgument='<%#Eval("id") %>' CommandName="EditLTA" ID="editLTA_LinkButton"><i class="fa fa-pencil"> </i></asp:LinkButton>
                                            <asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" CommandArgument='<%#Eval("id") %>' CommandName="RemoveLTA" ID="removeLTA_LinkButton"><i class="fa fa-trash"> </i></asp:LinkButton>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody>
                        </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>


    <asp:UpdatePanel ID="witnessAlert_UpdatePanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="row" id="witnessAlert_Div" runat="server">
                <div class="col-xs-12">
                    <div class="alert alert-warning alertForLostTime">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <i class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></i>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><%=ts.GetResource("lbl_witness_noneFound") %></h4>
                                        <p runat="server" id="mainAlertText_Div"><%=ts.GetResource("lbl_witness_noneFound_text") %></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="row" id="buttonType_Div" runat="server">
        <div class="col-xs-12 text-center">
            <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <asp:LinkButton ID="addLostTime_Button" runat="server" CssClass="btn btn-success" OnClick="addLostTime_Button_Click" OnClientClick="RemoveErrorLTA()"><i class="fa fa-plus"></i> <%=ts.GetResource("button_witness_add")%></asp:LinkButton>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>




    <!-- Lost Time Modal -->
    <div class="modal fade" id="WitnessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            
            <!-- alert -->
            <div class="modal fade" id="witnessAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id=""><%=ts.GetResource("h4_alert_title") %></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <%=ts.GetResource("lbl_alert_text") %>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-danger" onclick="$('#witnessAlert').modal('hide')"><%=ts.Translate("Close") %></button>                         
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End of alert Modal -->

                <div class="modal-header">
                    <h4 class="modal-title" id=""><%=ts.GetResource("h4_witness_information") %></h4>
                </div>
                <div class="modal-body modalScroll">
                

                    <asp:UpdatePanel ID="witnessModal_UpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>

                            <asp:HiddenField ID="witnessID_HiddenField" Value="" runat="server" ClientIDMode="Static" />




                            <div class="panel panel-default panel-subpanel">
                                <div class="panel-heading" role="tab" id="firstSection">
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i> <%=ts.GetResource("lbl_witness_witnessDetails") %>
                                    </h4>
                                </div>
                                <div id="collapseFirstSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="firstSection">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witFirstName_TextBox"><%=ts.GetResource("lbl_personDetails_forename") %></label>
                                                    <asp:TextBox runat="server" ID="witFirstName_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witSurname_TextBox"><%=ts.GetResource("lbl_personDetails_surname") %></label>
                                                    <asp:TextBox runat="server" ID="witSurname_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witContactNumber_TextBox"><%=ts.GetResource("lbl_personDetails_contactNumber") %></label>
                                                    <asp:TextBox runat="server" ID="witContactNumber_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witEmailAddress_TextBox"><%=ts.GetResource("lbl_personDetails_contactEmail") %></label>
                                                    <asp:TextBox runat="server" ID="witEmailAddress_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witStatus_DropDownList"><%=ts.GetResource("lbl_personDetails_personStatus") %></label>
                                                    <asp:DropDownList runat="server" CssClass="form-control" id="witStatus_DropDownList"/>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witIDNumber_TextBox"><%=ts.GetResource("lbl_personDetails_idNumbers") %></label>
                                                    <asp:TextBox runat="server" ID="witIDNumber_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="witOccupation_TextBox"><%=ts.GetResource("lbl_personDetails_jobTitle") %></label>
                                                    <asp:TextBox runat="server" ID="witOccupation_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-condensed table-bordered table-addQ ">
                                                    <tbody>
                                                        <tr>
                                                            <th width="35%"><%=ts.GetResource("lbl_witness_witnessWholeIncident") %></th>
                                                            <td class="inputRow">
                                                                <div class="">
                                                                    <div class="parentRadioGroup">
                                                                        <div class="radioGroup" onclick="ToggleFurtherWitnessTypes()">
                                                                            <label class="green white">
                                                                                <asp:RadioButton runat="server" ID="wholeIncident_Y_RadioButton" GroupName="WholeIncident"/>
                                                                                <span><%=ts.Translate("Yes") %></span>
                                                                            </label>
                                                                        </div>
                                                                        <div class="radioGroup" onclick="ToggleFurtherWitnessTypes()">
                                                                            <label class="green white">
                                                                                <asp:RadioButton runat="server" ID="wholeIncident_N_RadioButton" GroupName="WholeIncident"/>
                                                                                <span><%=ts.Translate("No") %></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="row" id="furtherWitnessTypes" style="display: none;">
                                            <div class="col-xs-12">
                                                <table class="table table-condensed table-bordered table-addQ ">
                                                    <tbody>
                                                        <tr>
                                                            <th width="35%"><%=ts.GetResource("lbl_witness_witnessSpecify") %></th>
                                                            <td class="inputRow">
                                                                <div class="">
                                                                    <div class="parentRadioGroup" onclick="toggleRootCauseDropDown()">
                                                                         <div class="radioGroup">
                                                                            <label class="green white" runat="server" id="incidentType_LeadUp_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_LeadUp_CheckBox"/>
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_LeadUp") %></span>
                                                                            </label>
                                                                        
                                                                      
                                                                            <label class="green white" runat="server" id="incidentType_Injury_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_Injury_CheckBox"/>
                                                                               <span><%=ts.GetResource("lbl_witness_witnessType_injury") %></span>
                                                                            </label>
                                                                        
                                                                      
                                                                            <label class="green white" runat="server" id="incidentType_Illness_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_Illness_CheckBox" />
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_id") %></span>
                                                                            </label>
                                                                       
                                                                    
                                                                            <label class="green white" runat="server" id="incidentType_DO_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_DO_CheckBox" />
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_do") %></span>
                                                                            </label>
                                                                        
                                                                    
                                                                            <label class="green white" runat="server" id="incidentType_Property_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_Property_CheckBox" />
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_pdam") %></span>
                                                                            </label>
                                                                     
                                                                            <label class="green white" runat="server" id="incidentType_NearMiss_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_NearMiss_CheckBox" />
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_nm") %></span>
                                                                            </label>
                                                                       
                                                                            <label class="green white" runat="server" id="incidentType_Violence_Label">
                                                                                <asp:CheckBox runat="server" id="incidentType_Violence_CheckBox" />
                                                                                <span><%=ts.GetResource("lbl_witness_witnessType_vi") %></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <button type="button" class="btn btn-primary" onclick="ChangeSection('2', true)"><%=ts.GetResource("button_nextSection") %></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-subpanel">
                                <div class="panel-heading" role="tab" id="secondSection">
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("h4_witness_interview_details") %>
                                    </h4>
                                </div>
                                <div id="collapseSecondSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="secondSection">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="intFirstName_TextBox"><%=ts.GetResource("lbl_witness_interviewer_firstName") %></label>
                                                    <asp:TextBox runat="server" ID="intFirstName_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>
                                            
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="intSurname_TextBox"><%=ts.GetResource("lbl_witness_interviewer_surname") %></label>
                                                    <asp:TextBox runat="server" ID="intSurname_TextBox" CssClass="form-control" ClientIDMode="Static" />
                                                </div>
                                            </div>

                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="intFirstName_TextBox"><%=ts.GetResource("lbl_witness_interviewer_datetime") %></label>
                                                    <telerik:RadDateTimePicker runat="server" skin="Bootstrap" Width="100%" ID="intDateTime_RadDateTimePicker"></telerik:RadDateTimePicker>
                                                </div>
                                            </div>
                                            
                                          

                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="intFirstName_TextBox"><%=ts.GetResource("lbl_witness_interviewer_statement") %></label>
                                                    <asp:TextBox runat="server" ID="intStatement_TextBox" CssClass="form-control" ClientIDMode="Static" TextMode="MultiLine" rows="5" />
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-xs-12">

                                                <div class="alert alert-warning alert-sm" id="interviewDeclaration">
                                                    <div class="row">
                                                        <div class="col-xs-10">
                                                            <div class="media">

                                                                <div class="media-body">
                                                                    <p id="NotSelectedText"><%=ts.GetResource("lbl_witness_interviewer_statementconfirmation") %></p>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2 text-right" style="padding-top: 15px;" id="detailsUnavailable_Label" runat="server" clientidmode="Static">
                                                            <label class="switch-sm">
                                                                <asp:CheckBox runat="server" ID="interviewDeclaration_CheckBox" ClientIDMode="Static" />
                                                                <span class="slider-sm round"></span>
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>


                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <button type="button" class="btn btn-primary" onclick="ChangeSection('1', false)"><%=ts.GetResource("button_previousSection") %></button>
                                                <button type="button" class="btn btn-primary" onclick="ChangeSection('3', true)"><%=ts.GetResource("button_nextSection") %></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default panel-subpanel">
                                <div class="panel-heading" role="tab" id="thirdSection">
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("h4_witness_interviewer_attachments") %>
                                    </h4><div id="file_list" ></div>
                                </div>
                                <div id="collapseThirdSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="thirdsection">
                                    <div class="panel-body">
                                        <div id="dZUploadContainer" runat="server">
                                            <div id="file_listWIT"><asp:Literal id="witnessRelatedFilesList_Literal" runat="server"></asp:Literal></div>

                                            <div id="dZActions" class="row" runat="server">
                                                <div class="col-lg-9">
                                                    <div id="dZUploadMessageSmallWIT" class="dZUploadMessageSmallArea"><i class="fa fa-cloud-upload purple-colour" aria-hidden="true"></i> <%= ts.GetResource("lbl_attachments_dropFiles") %></div>
                                                    <span class="fileupload-process">
                                                        <div id="total-progressWIT" class="totalProgressArea progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                                            <div class="progress-bar progress-bar-success" style="width: 0%;" data-dz-uploadprogress></div>
                                                        </div>
                                                    </span>
                                                </div>
                                                <div class="col-lg-3" style="text-align: right;">
                                                    <span class="btn btn-success fileinput-buttonWIT">
                                                        <i class="glyphicon glyphicon-plus"></i>
                                                        <span><%= ts.GetResource("lbl_attachments_addFilesManually") %></span>
                                                    </span>
                                                </div>
                                            </div>

                                            <div id="dZUploadWIT" class="dZUploadArea">
                                                <div id="dZUploadMessageLargeWIT" class="dZUploadMessageLargeArea"><i class="fa fa-cloud-upload purple-colour" aria-hidden="true"></i> <%= ts.GetResource("lbl_attachments_dropFiles") %></div>
                                                <div class="table table-striped previewsArea" class="files" id="previewsWIT">
                                                    <div id="dZtemplateWIT" class="file-row">
                                                        <!-- This is used as the file preview template -->
                                                        <div>
                                                            <span data-dz-preview class="preview">
                                                                <img data-dz-thumbnail /></span>
                                                        </div>
                                                        <div>
                                                            <p class="name" data-dz-name></p>
                                                            <strong class="error text-danger" data-dz-errormessage></strong>
                                                        </div>
                                                        <div>
                                                            <p class="size" data-dz-size></p>
                                                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"></div>
                                                            <div class="progress-bar progress-bar-success" style="width: 0%;" data-dz-uploadprogress></div>
                                                        </div>
                                                        <div id="dZcommands">
                                                            <button data-dz-remove class="btn btn-warning cancel" title="cancel">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                                <span><%= ts.Translate("Cancel") %></span>
                                                            </button>
                                                            <button data-dz-remove class="btn btn-danger delete dZfloated" title="remove" id="dZRemove" runat="server">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            <a data-dz-download class="btn btn-warning download dZfloated" target="_blank" title="download">
                                                                <i class='glyphicon glyphicon-download-alt'></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                       

                                    </div>
                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-xs-12 text-center">
                                                <button type="button" class="btn btn-primary" onclick="ChangeSection('2', false)"><%=ts.GetResource("button_previousSection") %></button>
                                                <asp:LinkButton runat="server" CssClass="btn btn-success" ID="saveWitness_Button" OnClick="saveWitness_Button_Click"><%=ts.GetResource("button_saveWitness") %></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><%= ts.Translate("Close") %></button>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of LTA Modal -->
    



 <!-- Lost Time Modal -->
    <div class="modal fade" id="witnessReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
            


                <div class="modal-header">
                    <h4 class="modal-title" id=""><%=ts.GetResource("h4_witness_information")%></h4>
                    
                </div>
                <div class="modal-body modalScroll">
                    

                

                    <asp:UpdatePanel ID="witnessReportModal_UpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            
                            <div class="row">
                                <div class="col-xs-12">
                                                
                                    <asp:Literal id="translationArea" runat="server"></asp:Literal>

                                </div>
                            </div>


                            <div class="panel panel-default panel-subpanel">
                                <div class="panel-heading" role="tab" >
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("lbl_witness_witnessDetails") %>
                                    </h4>
                                </div>
                                <div id="collapseFirstSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="firstSection">
                                    <div class="panel-body">
                                        

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">

                                                    <table class="table table-bordered table-searchstyle01">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_lostTime_fullName") %></th>
                                                                <td><asp:Literal id="witName_literal" runat="server"></asp:Literal></td>
                                                            </tr>
                                                           <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_personStatus") %></th>
                                                                <td><asp:Literal id="witStatus_Literal" runat="server"></asp:Literal></td>
                                                            </tr>

                                                            <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_jobTitle") %></th>
                                                                <td><asp:Literal id="witOccupation_Literal" runat="server"></asp:Literal></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group">

                                                    <table class="table table-bordered table-searchstyle01">
                                                        <tbody>
                                                            <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_contactNumber") %></th>
                                                                <td><asp:Literal id="witContactNumber_Literal" runat="server"></asp:Literal></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_contactEmail") %></th>
                                                                <td><asp:Literal id="witContactEmail_Literal" runat="server"></asp:Literal></td>
                                                            </tr>
                                                            <tr>
                                                                <th style="width: 180px;"><%=ts.GetResource("lbl_personDetails_idNumbers") %></th>
                                                                <td><asp:Literal id="witIDNumber_Literal" runat="server"></asp:Literal></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    

                                                </div>
                                            </div>
                                            

                                        </div>

                                        
                                        
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-condensed table-bordered table-addQ ">
                                                    <tbody>
                                                        <tr>
                                                            <th width="35%"><%=ts.GetResource("lbl_witness_witnessed") %></th>
                                                            <td class="inputRow">
                                                                <div class="">
                                                                    <asp:Literal id="witSpecified_Literal" runat="server"></asp:Literal>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                       

                                    </div>
                                    <div class="panel-footer">
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-subpanel">
                                <div class="panel-heading" role="tab" >
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("h4_witness_interview_details") %>
                                    </h4>
                                </div>
                                <div id="collapseSecondSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="secondSection">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <table class="table table-bordered table-searchstyle01">
                                                    <tbody>
                                                    <tr>
                                                        <th style="width: 180px;"><%=ts.GetResource("lbl_witness_fullname") %></th>
                                                        <td><asp:Literal id="intName_Literal" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                            <div class="col-xs-6">
                                                <table class="table table-bordered table-searchstyle01">
                                                    <tbody>
                                                    <tr>
                                                        <th style="width: 180px;"><%=ts.GetResource("lbl_witness_interviewer_datetime") %></th>
                                                        <td><asp:Literal id="intDateTime_Literal" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                        
                                        
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-striped table-bordered table-searchstyle01">
                                                    <tbody>
                                                    <tr>
                                                        <th><%=ts.GetResource("lbl_witness_interviewer_statement") %></th>
                                                    </tr>
                                                    <tr>
                                                        <td><asp:Literal id="intStatement_Literal" runat="server"></asp:Literal></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                        
                                       

                                    </div>
                                    <div class="panel-footer">
                                       </div>
                                </div>
                            </div>

                            <div class="panel panel-default panel-subpanel" id="attachmentReport_Div" runat="server">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title" style="font-weight: 100">
                                        <i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("h4_attachments")%>
                                    </h4>
                                </div>
                                <div id="collapseThirdSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="thirdsection">
                                    <div class="panel-body">
                                      
                                        <table>
                                             <asp:Repeater ID="witnessReportFiles_Repeater" runat="server">
                                                 <HeaderTemplate>
                                                     
                                                     <tr>
                                                         <td colspan="3" style="text-align: left;">
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                     <div style="float: left;">
                                                         <%#ASNETNSP.SharedComponants.returnAttachmentItem(Eval("filepath").ToString(), Eval("filename").ToString(), Eval("friendlyname").ToString(), Eval("filetype").ToString(),"medium", "/uploads") %>
                                                         <%--<img src="<%#relatedFilesRoot + Eval("filepath") + Eval("filename").ToString() %>" class="imagePrint" style="cursor: pointer" lightbox-name="<%#Eval("friendlyname") %>">--%>
                                                     </div>
                                                 </ItemTemplate>
                                                 <FooterTemplate>
                                                         </td>
                                                </tr>
                                                 </FooterTemplate>
                                             </asp:Repeater>
                                        </table>

                                    </div>
                                    <div class="panel-footer">
                                       
                                    </div>
                                </div>
                            </div>


                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of LTA Modal -->
    
</div>

<%--<div id="reportContent" runat="server" visible="False">
    
    <div class="row">
        <div class="col-xs-12">
            <asp:Repeater ID="lostTimeListReport_Repeater" runat="server">
                <HeaderTemplate>
                    <table class="table table-condensed table-bordered table-striped">
                        <thead>
                            <tr>
                                <th width="30%"><%#ts.GetResource("lbl_lostTime_personAffected") %></th>
                                <th width="15%">Type<%#ts.GetResource("lbl_lostTime_personAffected") %></th>
                                <th width="10%" class="text-center">Start Date<%#ts.GetResource("lbl_lostTime_personAffected") %></th>
                                <th width="10%" class="text-center">End Date</th>
                                <th width="15%" class="text-center">Time Lost</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="lta_row">
                        <td>Current IP</td>
                        <td><%#LostTimeType(Eval("lta_type")) %></td>
                        <td class="text-center"><%# ASNETNSP.Formatting.FormatDateTime(Eval("start_date").ToString(), 1) %></td>
                        <td class="text-center"><%# LostTimeEnd(Eval("ed_type"), Eval("start_date"), Eval("end_date")) %></td>
                        <td class="text-center"><%#LostTimeTotal(Eval("ed_type"), Eval("hours"), Eval("days")) %></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody>
                </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
    </div>

</div>--%>