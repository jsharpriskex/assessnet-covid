﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class PersonDetails_UserControl : UserControl
{


    public string _corpCode;
    public string _currentUser;
    private string _yourAccessLevel;

    public string IncidentReference;
    public string IncidentCentre;
    public string IncidentSubReference;
    public string IncidentPropertyDamageReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public Equipment CurrentEquipment;
    public Equipment.Status Status;


    public bool useFullForm;

    public string NewEquipmentRecord;

    public event EventHandler SaveOrEditButton_Click;

    private bool DataRequired = false;

    public TranslationServices ts;

    public bool ShowData;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (ts == null)
        {
            ts = new TranslationServices(_corpCode, "en-gb", "incident");
        }

        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************


        useFullForm = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "acc_equipment_fulldetail", "pref") == "Y";

        ChangeFormFromSessions();


        CurrentEquipment = new Equipment(this, Status);


    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

        //CurrentPerson = new PersonDetails(this, Status);

        if (ShowData)
        {
            _ReturnData();
        }

    }


    private void ChangeFormFromSessions()
    {

        if (useFullForm)
        {
            typeLabel_Literal.Text = ts.GetResource("lbl_equipment_type1");
            manufacturerLabel_Literal.Text = ts.GetResource("lbl_equipment_manufacturer1");
            modelLabel_Literal.Text = ts.GetResource("lbl_equipment_model1");
            registrationLabel_Literal.Text = ts.GetResource("lbl_equipment_reg1");
            numberLabel_Literal.Text = _corpCode == "065912" ? ts.GetResource("lbl_equipment_vehicleNumber_ocado") : ts.GetResource("lbl_equipment_vehicleNumber");
            fullAdditionalQuestions.Visible = true;

            typeReportLabel_Literal.Text = ts.GetResource("lbl_equipment_type1");
            manufacturerReportLabel_Literal.Text = ts.GetResource("lbl_equipment_manufacturer1");
            modelReportLabel_Literal.Text = ts.GetResource("lbl_equipment_model1");
            registrationReportLabel_Literal.Text = ts.GetResource("lbl_equipment_reg1");
            numberReportLabel_Literal.Text = _corpCode == "065912" ? ts.GetResource("lbl_equipment_vehicleNumber_ocado") : ts.GetResource("lbl_equipment_vehicleNumber");
            fullAdditionalQuestionsReport.Visible = true;

        }
        else
        {
            typeLabel_Literal.Text = ts.GetResource("lbl_equipment_type2");
            manufacturerLabel_Literal.Text = ts.GetResource("lbl_equipment_manufacturer2");
            modelLabel_Literal.Text = ts.GetResource("lbl_equipment_model2");
            registrationLabel_Literal.Text = ts.GetResource("lbl_equipment_reg2");
            numberLabel_Literal.Text = "";

            typeReportLabel_Literal.Text = ts.GetResource("lbl_equipment_type2");
            manufacturerReportLabel_Literal.Text = ts.GetResource("lbl_equipment_manufacturer2");
            modelReportLabel_Literal.Text = ts.GetResource("lbl_equipment_model2");
            registrationReportLabel_Literal.Text = ts.GetResource("lbl_equipment_reg2");
            numberReportLabel_Literal.Text = "";
        }
       
    }


    public void SaveData()
    {
        CurrentEquipment.SaveData();
    }

    public string RecordLanguage()
    {
        CurrentEquipment.ReturnEquipmentData();
        return CurrentEquipment.recordLanguage;
    }

    
    // The public method triggers that we need to show some form data, and initates it during page load
    public void ReturnData()
    {
        DataRequired = true;
    }
    // This is called during page load
    private void _ReturnData()
    {
        CurrentEquipment.ReturnEquipmentData();
        DataRequired = false;
        ScriptManager.RegisterStartupScript(ReferenceToCurrentPage.Page, ReferenceToCurrentPage.GetType(), Guid.NewGuid().ToString(), "ToggleModificationTextBox();", true);
    }


    public string CreateEquipmentRecord(string propertyDamageReference)
    {

        string returnReference = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Equipment_Create ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", IncidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", IncidentReference));
                cmd.Parameters.Add(new SqlParameter("@accRef", _currentUser));
                cmd.Parameters.Add(new SqlParameter("@propertyDamage_Ref", propertyDamageReference));
                cmd.Parameters.Add(new SqlParameter("@accLang", ts.LanguageCode()));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    returnReference = Convert.ToString(objrs["EQReference"]);
                }
               
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }

        return returnReference;
    }

}


public class Equipment
{

    private string _corpCode;
    private string _currentUser;
    private string _yourAccessLevel;

    private string _incidentCentre;
    private string _incidentReference;
    private string _propertyDamageReference;
    private string _subIncidentReference;
    private bool _isAdmin = true;

    private bool _hasData;

    private string _associatedUser;

    private Status _currentStatus;

    // internal variables
    private string _type;
    private string _owner;
    private string _make;
    private string _model;
    private string _milage;
    private string _registration;
    private string _vehicleNumber;
    private DateTime? _yearOfManufacture;

    private bool _modifications;
    private string _modificationText;
    private bool _licenceHeld;
    private bool _shiftChecks;

    public string recordLanguage;


    // items on page
    private DropDownList _type_DropDownList;
    private DropDownList _owner_DropDownList;
    private TextBox _manufacturer_TextBox;
    private TextBox _model_TextBox;
    private TextBox _mileage_TextBox;
    private TextBox _registration_TextBox;
    private TextBox _number_TextBox;
    private DropDownList _yearOfManufacture_DropDownList;

    private RadioButton _modificationsY_RadioButton;
    private RadioButton _modificationsN_RadioButton;

    private TextBox _modificationsObserved_TextBox;

    private RadioButton _licenceY_RadioButton;
    private RadioButton _licenceN_RadioButton;

    private RadioButton _shiftsY_RadioButton;
    private RadioButton _shiftsN_RadioButton;


    //Report items
    private Literal _type_Literal;
    private Literal _owner_Literal;
    private Literal _manufacturer_Literal;
    private Literal _model_Literal;
    private Literal _registration_Literal;
    private Literal _number_Literal;
    private Literal _yearOfManufacture_Literal;
    private Literal _modifications_Literal;
    private Literal _modificationsObserved_Literal;
    private Literal _licence_Literal;
    private Literal _shifts_Literal;
    private Literal _mileage_Literal;
    private HtmlGenericControl _modificationsObservedReport_Div;

    // Content Divs
    private HtmlGenericControl _EditEquipment;
    private HtmlGenericControl _ReportEquipment;

    private PersonDetails_UserControl _currentControl;


    public Equipment(PersonDetails_UserControl currentControl, Status currentStatus)
    {
        _currentStatus = currentStatus;
        _currentControl = currentControl;
        _corpCode = currentControl._corpCode;
        _incidentCentre = currentControl.IncidentCentre;
        _incidentReference = currentControl.IncidentReference;
        _subIncidentReference = currentControl.IncidentSubReference;
        _currentUser = currentControl._currentUser;
        _isAdmin = currentControl.IsAdmin;

        _propertyDamageReference = currentControl.IncidentPropertyDamageReference;

        _EditEquipment = (HtmlGenericControl)currentControl.FindControl("EditEquipment");
        _ReportEquipment = (HtmlGenericControl)currentControl.FindControl("ReportEquipment");

        if (_currentStatus == Status.Edit)
        {
            _type_DropDownList = (DropDownList) currentControl.FindControl("type_DropDownList");
            _owner_DropDownList = (DropDownList) currentControl.FindControl("owner_DropDownList");
            _yearOfManufacture_DropDownList = (DropDownList) currentControl.FindControl("yearOfManufacture_DropDownList");

            _manufacturer_TextBox = (TextBox)currentControl.FindControl("manufacturer_TextBox");
            _model_TextBox = (TextBox)currentControl.FindControl("model_TextBox");
            _mileage_TextBox = (TextBox)currentControl.FindControl("mileage_TextBox");
            _registration_TextBox = (TextBox)currentControl.FindControl("registration_TextBox");
            _number_TextBox = (TextBox)currentControl.FindControl("number_TextBox");
            _modificationsObserved_TextBox = (TextBox)currentControl.FindControl("modificationsObserved_TextBox");

            _modificationsY_RadioButton = (RadioButton)currentControl.FindControl("modificationsY_RadioButton");
            _modificationsN_RadioButton = (RadioButton)currentControl.FindControl("modificationsN_RadioButton");

            _licenceY_RadioButton = (RadioButton)currentControl.FindControl("licenceY_RadioButton");
            _licenceN_RadioButton = (RadioButton)currentControl.FindControl("licenceN_RadioButton");

            _shiftsY_RadioButton = (RadioButton)currentControl.FindControl("shiftsY_RadioButton");
            _shiftsN_RadioButton = (RadioButton)currentControl.FindControl("shiftsN_RadioButton");

        }

        if (_currentStatus == Status.Report)
        {
            // Report
            _type_Literal = (Literal)currentControl.FindControl("type_Literal");
            _owner_Literal = (Literal)currentControl.FindControl("owner_Literal");
            _manufacturer_Literal = (Literal)currentControl.FindControl("manufacturer_Literal");
            _model_Literal = (Literal)currentControl.FindControl("model_Literal");
            _registration_Literal = (Literal)currentControl.FindControl("registration_Literal");
            _number_Literal = (Literal)currentControl.FindControl("number_Literal");
            _yearOfManufacture_Literal = (Literal)currentControl.FindControl("yearOfManufacture_Literal");
            _modifications_Literal = (Literal)currentControl.FindControl("modifications_Literal");
            _modificationsObserved_Literal = (Literal)currentControl.FindControl("modificationsObserved_Literal");
            _licence_Literal = (Literal)currentControl.FindControl("licence_Literal");
            _shifts_Literal = (Literal)currentControl.FindControl("shifts_Literal");
            _mileage_Literal = (Literal)currentControl.FindControl("mileage_Literal");
            _modificationsObservedReport_Div = (HtmlGenericControl)currentControl.FindControl("modificationsObservedReport_Div");


        }


       

    }


    public void ReturnEquipmentData()
    {
        PopulateInternalVariablesFromDB();

        switch (_currentStatus)
        {
            case Status.Edit:
                PopulateDropDownLists();
                _EditEquipment.Visible = true;

                PopulateFormFromInternalVariables();


                
                break;
            case Status.Report:
                _ReportEquipment.Visible = true;
                PopulateReportFromInternalVariables();
                break;
            default:
                throw new Exception("No Equipment Status Found");
                break;
        }
    }


    // Populate Dropdown lists
    private void PopulateDropDownLists()
    {

        // Owner
        using(DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "pdam_vehicle_owner", false, _currentControl.ts.LanguageCode(), _owner))
        {

            _owner_DropDownList.DataSource = accOptions;
            _owner_DropDownList.DataTextField = "opt_value";
            _owner_DropDownList.DataValueField = "opt_id";
            

            _owner_DropDownList.DataBind();

            _owner_DropDownList.Items.Insert(0, new ListItem(_currentControl.ts.Translate("Please select") + "...", "na"));


            if (_owner != "")
            {
                if (_owner_DropDownList.Items.FindByValue(_owner) != null)
                {
                    _owner_DropDownList.SelectedValue = _owner;
                }
            }
            

        }

        // Type
        using(DataTable accOptions = Incident.IncidentGeneric.ReturnAccOptsByType_DataTable(_corpCode, "pdam_vehicle_type", false, _currentControl.ts.LanguageCode(), _type))
        {

            _type_DropDownList.DataSource = accOptions;
            _type_DropDownList.DataTextField = "opt_value";
            _type_DropDownList.DataValueField = "opt_id";
            

            _type_DropDownList.DataBind();

            _type_DropDownList.Items.Insert(0, new ListItem(_currentControl.ts.Translate("Please select") + "...", "na"));


            if (_type != "")
            {
                if (_type_DropDownList.Items.FindByValue(_type) != null)
                {
                    _type_DropDownList.SelectedValue = _type;
                }
            }
            

        }

        // Year
        // Dont ask why it starts at 90s, or why we add 2 years.. this comes from classic

        if (_yearOfManufacture_DropDownList.Items.Count == 0)
        {

            _yearOfManufacture_DropDownList.Items.Insert(0, new ListItem(_currentControl.ts.Translate("Please select") + "...", "na"));

            for (int i = 1990; i < DateTime.Now.Year + 3; i++)
            {
                _yearOfManufacture_DropDownList.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
       

    }


    private void ResetForm()
    {

    }

    // Initiate Form from internal variables
    private void PopulateFormFromInternalVariables()
    {

        //_type_DropDownList - Covered already
        //_owner_DropDownList - Covered already
        _yearOfManufacture_DropDownList.SelectedValue = _yearOfManufacture == null ? null : Convert.ToDateTime(_yearOfManufacture).Year.ToString();
        _manufacturer_TextBox.Text = _make;
        _model_TextBox.Text = _model;
        _mileage_TextBox.Text = _milage;
        _registration_TextBox.Text = _registration;
        _number_TextBox.Text = _vehicleNumber;

        if (_modifications)
        {
            _modificationsY_RadioButton.Checked = true;
            _modificationsObserved_TextBox.Text = _modificationText;
        }
        else
        {
            _modificationsN_RadioButton.Checked = true;
        }

        if (_licenceHeld)
        {
            _licenceY_RadioButton.Checked = true;
        }
        else
        {
            _licenceN_RadioButton.Checked = true;
        }

        if (_shiftChecks)
        {
            _shiftsY_RadioButton.Checked = true;
        }
        else
        {
            _shiftsN_RadioButton.Checked = true;
        }

    }

    private void PopulateReportFromInternalVariables()
    {
        _type_Literal.Text = Incident.IncidentGeneric.ReturnAccOptByValue(_corpCode, "pdam_vehicle_type", _currentControl.ts.LanguageCode(), _type);
        _owner_Literal.Text = Incident.IncidentGeneric.ReturnAccOptByValue(_corpCode, "pdam_vehicle_owner", _currentControl.ts.LanguageCode(), _owner);
        _manufacturer_Literal.Text = _make;
        _model_Literal.Text = _model;
        _registration_Literal.Text = _registration;
        _mileage_Literal.Text = _milage;
        _number_Literal.Text = _milage;
        _yearOfManufacture_Literal.Text = _yearOfManufacture == null ? "ERROR" : Convert.ToDateTime(_yearOfManufacture).Year.ToString();
        _modifications_Literal.Text = _modifications ? "Yes" : "No";
        _modificationsObservedReport_Div.Visible = _modifications;
        _modificationsObserved_Literal.Text =  _currentControl.ts.Translate(_modificationText, recordLanguage, _incidentCentre, _incidentReference,_subIncidentReference, "equip_modificationText");
        _licence_Literal.Text = _licenceHeld  ? "Yes" : "No";
        _shifts_Literal.Text = _shiftChecks ? "Yes" : "No";
    }


    // Initate Internal Variables from DataSource (either DB or from the form items)
    private void PopulateInternalVariablesFromDB()
    {
        if (_subIncidentReference != null)
        {
             using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Equipment_Display ", dbConn);
                    SqlDataReader objrs = null;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                    cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                    cmd.Parameters.Add(new SqlParameter("@sub_inc_ref", _subIncidentReference));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _type = Convert.ToString(objrs["equip_type"]);
                        _owner = Convert.ToString(objrs["equip_owner"]);
                        _make = Convert.ToString(objrs["equip_make"]);
                        _model = Convert.ToString(objrs["equip_model"]);
                        _milage = Convert.ToString(objrs["equip_vehicle_mileage"]);
                        _registration = Convert.ToString(objrs["equip_reg_serial"]);
                        _vehicleNumber = Convert.ToString(objrs["equip_vehicle_id"]);

                        _yearOfManufacture = objrs["equip_date_manufacture"] != DBNull.Value ? Convert.ToDateTime(objrs["equip_date_manufacture"]) : DateTime.Now;
                        _modificationText = Convert.ToString(objrs["equip_mod_details"]);
                        _modifications = Convert.ToBoolean(objrs["equip_mods"]);
                        _licenceHeld = Convert.ToBoolean(objrs["valid_licence_held"]);
                        _shiftChecks = Convert.ToBoolean(objrs["checks_complete"]);
                        recordLanguage  = Convert.ToString(objrs["display_language"]);
                        _hasData = true;



                    }
                    else
                    {
                        // We dont have one, so we need to create on save.
                        _hasData = false;
                    }

                    objrs.Dispose();
                    cmd.Dispose();


                }
                catch (Exception)
                {
                    throw;
                }

            }
        }
        else
        {
            _hasData = false;
        }
       

    }

    private void PopulateInternalVariablesFromForm()
    {

        if(_yearOfManufacture_DropDownList.SelectedValue != "na")
        {
            int selectedYear = Convert.ToInt32(_yearOfManufacture_DropDownList.SelectedValue);
            DateTime newYear = new DateTime(selectedYear, 1, 1);
            _yearOfManufacture = newYear;
        }
        else
        {
            _yearOfManufacture = DateTime.Now;
        }



        _type = _type_DropDownList.SelectedValue;
        _owner = _owner_DropDownList.SelectedValue;
        _make = _manufacturer_TextBox.Text;
        _model = _model_TextBox.Text;
        _milage = _mileage_TextBox.Text;
        _registration = _registration_TextBox.Text;
        _vehicleNumber = _number_TextBox.Text;
        
        _modificationText = _modificationsObserved_TextBox.Text;
        _modifications = _modificationsY_RadioButton.Checked;
        _licenceHeld = _licenceY_RadioButton.Checked;
        _shiftChecks = _shiftsY_RadioButton.Checked;
    }


    // Save the form
    public void SaveData()
    {
        PopulateInternalVariablesFromForm();
        SaveDataInternal();
    }

    private void SaveDataInternal()
    {

        if (_subIncidentReference == null || _subIncidentReference == "")
        {
            CreateEquipmentRecord();
        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Equipment_Save ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@sub_ref", _subIncidentReference));
                cmd.Parameters.Add(new SqlParameter("@type", _type));
                cmd.Parameters.Add(new SqlParameter("@owner", _owner));
                cmd.Parameters.Add(new SqlParameter("@make", _make));
                cmd.Parameters.Add(new SqlParameter("@model", _model));
                cmd.Parameters.Add(new SqlParameter("@mileage", _milage));
                cmd.Parameters.Add(new SqlParameter("@registration", _registration));
                cmd.Parameters.Add(new SqlParameter("@vehiclenumber", _vehicleNumber));
                cmd.Parameters.Add(new SqlParameter("@yearOfManufacture", _yearOfManufacture));
                cmd.Parameters.Add(new SqlParameter("@modifications", _modifications));
                cmd.Parameters.Add(new SqlParameter("@modificationsText", _modificationText));               
                cmd.Parameters.Add(new SqlParameter("@licenceHeld", _licenceHeld));               
                cmd.Parameters.Add(new SqlParameter("@shiftChecks", _shiftChecks));
                cmd.Parameters.Add(new SqlParameter("@accRef", _currentUser));

                cmd.ExecuteNonQuery();

                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }
    }

    
    
    private void CreateEquipmentRecord()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_ACC.dbo.Equipment_Create ", dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@accb_code", _incidentCentre));
                cmd.Parameters.Add(new SqlParameter("@inc_ref", _incidentReference));
                cmd.Parameters.Add(new SqlParameter("@accRef", _currentUser));
                cmd.Parameters.Add(new SqlParameter("@propertyDamage_Ref", _propertyDamageReference));
                cmd.Parameters.Add(new SqlParameter("@accLang", _currentControl.ts.LanguageCode()));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    _subIncidentReference = Convert.ToString(objrs["EQReference"]);
                }
               
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }

    }


    public enum Status
    {
        Edit = 0,
        Report = 1
    }


}