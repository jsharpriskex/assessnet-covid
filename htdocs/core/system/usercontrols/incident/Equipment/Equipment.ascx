﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Equipment.ascx.cs" Inherits="PersonDetails_UserControl" %>


<script>



</script>

<div id="EditEquipment" runat="server" clientidmode="Static" Visible="False">




    <div class="row" id="ContentRow">
        <div runat="server" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" clientidmode="Static">
            <div class="panel-body">


                <div id="BasicInfo">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label for="type_DropDownList">
                                            <asp:Literal runat="server" ID="typeLabel_Literal"></asp:Literal></label>
                                            <asp:DropDownList runat="server" CssClass="form-control" Width="100%" id="type_DropDownList" />
                                    </div>

                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="owner_DropDownList"><%=ts.GetResource("lbl_equipment_owner")%></label>
                                        <asp:DropDownList runat="server" CssClass="form-control" Width="100%"  id="owner_DropDownList" />
                                    </div>

                                </div>
                            </div>

                            <div class="row">

                                <div class="col-xs-6">



                                    <div class="form-group">
                                        <label for="manufacturer_TextBox">
                                            <asp:Literal runat="server" ID="manufacturerLabel_Literal"></asp:Literal></label>
                                            <asp:TextBox runat="server" class="form-control" ID="manufacturer_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="col-xs-6">



                                    <div class="form-group">
                                        <label for="model_TextBox">
                                            <asp:Literal runat="server" ID="modelLabel_Literal"></asp:Literal></label>
                                             <asp:TextBox runat="server" class="form-control"  id="model_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label for="mileage_TextBox"><%=ts.GetResource("lbl_equipment_vehicleMileage")%></label>
                                        <asp:TextBox runat="server" class="form-control"  id="mileage_TextBox"  aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label for="registration_TextBox">
                                            <asp:Literal runat="server" ID="registrationLabel_Literal"></asp:Literal></label>
                                            <asp:TextBox runat="server" class="form-control" ID="registration_TextBox" aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                    </div>

                                </div>

                            </div>

                            <div class="row" id="fullAdditionalQuestions" runat="server" visible="False">

                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label for="number_TextBox">
                                            <asp:Literal runat="server" ID="numberLabel_Literal"></asp:Literal></label>
                                        <asp:TextBox runat="server" class="form-control"  id="number_TextBox"  aria-describedby="title" ClientIDMode="Static"></asp:TextBox>
                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <label for="yearOfManufacture_DropDownList"><%=ts.GetResource("lbl_equipment_yearManufacture")%></label>
                                        <asp:DropDownList runat="server" CssClass="form-control" Width="100%" id="yearOfManufacture_DropDownList"  />
                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_modifications")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                        <div class="parentRadioGroup">
                                                            <div class="radioGroup">
                                                                <label class="green white">
                                                                    <asp:RadioButton runat="server" ID="modificationsY_RadioButton" GroupName="ModificationsRadio" />
                                                                    <span><%=ts.Translate("Yes")%></span>
                                                                </label>
                                                            </div>
                                                            <div class="radioGroup">
                                                                <label class="green white">
                                                                    <asp:RadioButton runat="server" ID="modificationsN_RadioButton" GroupName="ModificationsRadio" />
                                                                    <span><%=ts.Translate("No")%></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-12" id="modificationsObserved" style="display:none;">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_modifications_observed")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                       <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" rows="5" id="modificationsObserved_TextBox"></asp:TextBox>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_validLicense")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                        <div class="parentRadioGroup">
                                                            <div class="radioGroup">
                                                                <label class="green white">
                                                                    <asp:RadioButton runat="server" ID="licenceY_RadioButton" GroupName="LicenceRadio" />
                                                                    <span><%=ts.Translate("Yes")%></span>
                                                                </label>
                                                            </div>
                                                            <div class="radioGroup">
                                                                <label class="green white">
                                                                    <asp:RadioButton runat="server" ID="licenceN_RadioButton" GroupName="LicenceRadio" />
                                                                    <span><%=ts.Translate("No")%></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                 <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_preShiftChecks")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="parentRadioGroup">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                                <asp:RadioButton runat="server" ID="shiftsY_RadioButton" GroupName="ShiftsRadio" />
                                                                <span><%=ts.Translate("Yes")%></span>
                                                            </label>
                                                        </div>
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                                <asp:RadioButton runat="server" ID="shiftsN_RadioButton" GroupName="ShiftsRadio" />
                                                                <span><%=ts.Translate("No")%></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>




                    </div>
                </div>

            </div>


            <asp:CustomValidator ID="equipment_CustomValidator" runat="server" ValidationGroup="Equipment" ClientIDMode="Static" ClientValidationFunction="ValidateEquipment"></asp:CustomValidator>

        </div>
    </div>
</div>



<div id="ReportEquipment" runat="server" Visible="False">
    
     <div class="row" id="ContentRow">
        <div runat="server" id="Div1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" clientidmode="Static">
            <div class="panel-body">


                <div id="BasicInfo">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><asp:Literal runat="server" ID="typeReportLabel_Literal"></asp:Literal></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="type_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><%=ts.GetResource("lbl_equipment_owner")%></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="owner_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>

                            <div class="row">

                                <div class="col-xs-6">



                                    <div class="form-group">

                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><asp:Literal runat="server" ID="manufacturerReportLabel_Literal"></asp:Literal></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="manufacturer_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        
                                    </div>

                                </div>
                                <div class="col-xs-6">



                                    <div class="form-group">
                                        
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><asp:Literal runat="server" ID="modelReportLabel_Literal"></asp:Literal></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="model_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>    
                                        
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-xs-6">

                                    <div class="form-group">
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><%=ts.GetResource("lbl_equipment_vehicleMileage")%></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="mileage_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>    

                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">
                                        
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><asp:Literal runat="server" ID="registrationReportLabel_Literal"></asp:Literal></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="registration_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>    
                                    </div>

                                </div>

                            </div>

                            <div class="row" id="fullAdditionalQuestionsReport" runat="server" visible="False">

                                <div class="col-xs-6">

                                    <div class="form-group">
                                    
                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><asp:Literal runat="server" ID="numberReportLabel_Literal"></asp:Literal></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="number_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>    
                                        
                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="form-group">

                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                            <tr>
                                                <th style="width: 180px;"><%=ts.GetResource("lbl_equipment_yearManufacture")%></th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Literal runat="server" ID="yearOfManufacture_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>    

                                    </div>

                                </div>

                                <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_modifications")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                        <asp:Literal runat="server" ID="modifications_Literal"></asp:Literal>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-12" id="modificationsObservedReport_Div" runat="server" Visible="False">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="25%"><%=ts.GetResource("lbl_equipment_modifications_observed")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                        <asp:Literal runat="server" ID="modificationsObserved_Literal"></asp:Literal>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_validLicense")%>
                                                </th>
                                                <td class="inputRow">
                                                    <div class="">
                                                        <asp:Literal runat="server" ID="licence_Literal"></asp:Literal>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                                 <div class="col-xs-12">
                                    <table class="table table-condensed table-bordered table-addQ">
                                        <tbody>
                                            <tr>
                                                <th width="35%"><%=ts.GetResource("lbl_equipment_preShiftChecks")%>
                                                </th>
                                                <td class="inputRow">
                                                    <asp:Literal runat="server" ID="shifts_Literal"></asp:Literal>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>




                    </div>
                </div>

            </div>


            <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="Equipment" ClientIDMode="Static" ClientValidationFunction="ValidateEquipment"></asp:CustomValidator>

        </div>
    </div>
    
    

</div>


<div runat="server" id="editScript">

    <!-- alert -->
    <div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id=""><%=ts.GetResource("h4_alert_title") %></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <%=ts.GetResource("lbl_alert_text") %>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><%=ts.Translate("Close") %></button>                         
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- End of alert Modal -->
    
    <script>
        

       

    </script>


</div>
