﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class AttachedAssessment_UserControl : UserControl
{


    public string CorpCode;
    public string CurrentUser;
    private string _yourAccessLevel;

    public string RefererReference1;
    public string RefererReference2;

    public bool IsAdmin;


    public ModuleType Module;
    public ModuleType RefererModule;

    public string Module_Code; 

    
    public string RefererModule_Code;

    private bool ReturnData;

    private string accLang;
  

    private bool FormDataRequired = false;

    private string sessionGUID;
    private string uniqueSearchCode;
    public TranslationServices ts;

    public string Tier2ID { get; private set; }
    public string Tier3ID { get; private set; }
    public string Tier4ID { get; private set; }
    public string Tier5ID { get; private set; }
    public string Tier6ID { get; private set; }


    public string Tier2Name { get; private set; }
    public string Tier3Name { get; private set; }
    public string Tier4Name { get; private set; }
    public string Tier5Name { get; private set; }
    public string Tier6Name { get; private set; }

    public string Tier2Label { get; private set; }
    public string Tier3Label { get; private set; }
    public string Tier4Label { get; private set; }
    public string Tier5Label { get; private set; }
    public string Tier6Label { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        

        //***************************
        // CHECK THE SESSION STATUS
        CorpCode = Context.Session["CORP_CODE"].ToString();
        CurrentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "YOUR_ACCESS", "user");
        sessionGUID = Context.Session["sessionGUID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        //***************************
        // INITIATE AND SET TRANSLATIONS
        //ts = new TranslationServices(CorpCode, accLang, "usercontrol");
        // END TRANSLATIONS
        //***************************

        Tier2Label = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "corp_tier2", "pref") ?? "Company";
        Tier3Label = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "corp_tier3", "pref") ?? "Location";
        Tier4Label = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "corp_tier4", "pref") ?? "Department";
        Tier5Label = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "corp_tier5", "pref") ?? "Err";
        Tier6Label = SharedComponants.getPreferenceValue(CorpCode, CurrentUser, "corp_tier6", "pref") ?? "Err";

        uniqueSearchCode = "ATTTACHED" + CorpCode + sessionGUID;

        Module_Code = ReturnModuleCode(Module);
        RefererModule_Code = ReturnModuleCode(RefererModule);

        if (!IsPostBack)
        {
            DisplayBlankStructure();
            ReturnCurrentAttachedAssessments();
            PopulateDropDowns();
        }
        

    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        
    }


    private void PopulateDropDowns()
    {
        if (srch_sortOrder_DropDownList.Items.Count == 0)
        {

            srch_sortOrder_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_reference"), "id"));
            srch_sortOrder_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_personAffected"), "recordtotalaffected"));
            srch_sortOrder_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_riskRating"), "max_icon_rating, myriskscore"));
            srch_sortOrder_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_assessmentTitle"), "recordequipment"));

        }

        if (srch_sortBy_DropDownList.Items.Count == 0)
        {
            srch_sortBy_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_descending"), "desc"));
            srch_sortBy_DropDownList.Items.Add(new ListItem(ts.GetResource("uc_attach_lbl_ascending"), "asc"));

        }
    }



    // --- Initiate Search Results
    protected void InitiateSearch()
    {

        // Capture User Inputs
        string reference = Formatting.FormatTextInput(srch_Reference_TextBox.Text);
        string keyword = Formatting.FormatTextInput(srch_Keyword_TextBox.Text);
        string sortOrder = Formatting.FormatTextInput(srch_sortOrder_DropDownList.SelectedValue);
        string sortBy = Formatting.FormatTextInput(srch_sortBy_DropDownList.SelectedValue);


        string tier2Reference = tier2Reference_HiddenField.Value;
        string tier3Reference = tier3Reference_HiddenField.Value;
        string tier4Reference = tier4Reference_HiddenField.Value;
        string tier5Reference = tier5Reference_HiddenField.Value;
        string tier6Reference = tier6Reference_HiddenField.Value;




        DataTable searchDT = new DataTable();

        // Run Stored Procedure and Bind to a DataTable that returned the results
        // Extra logic to rename column headings to a generic name, which can be utilised for all Assessments.
        if (Module == ModuleType.RiskAssessment)
        {
            searchDT = ReturnRiskAssessmentResults(reference, keyword, sortOrder, sortBy, tier2Reference, tier3Reference, tier4Reference, tier5Reference, tier6Reference);


            searchDT.Columns["recordreference"].ColumnName = "reference";
            searchDT.Columns["recordequipment"].ColumnName = "title";
            searchDT.Columns["recorddescription"].ColumnName = "description";
            searchDT.Columns["recorddate"].ColumnName = "date";
        }




       


        // Only show 500 rows
        if(searchDT.Rows.Count > 500)
        {
            tooManyRows_Div.Visible = true;
        }
        else
        {
            tooManyRows_Div.Visible = false;
        }

        // Show Results - Limit to 500
        if(searchDT.Rows.Count > 0)
        {
            searchResults_Repeater.DataSource = searchDT.AsEnumerable().Take(250).CopyToDataTable();
            searchResults_Repeater.DataBind();

        }
        else
        {
            //Error prevention, this is just an empty datatable
            searchResults_Repeater.DataSource = searchDT;
            searchResults_Repeater.DataBind();
        }
       


        if(searchResults_Repeater.Items.Count == 0)
        {
            noRows_Div.Visible = true;
            searchResults_Repeater.Visible = false;
        }
        else
        {
            noRows_Div.Visible = false;
            searchResults_Repeater.Visible = true;
        }

        searchResultsMainContainer.Visible = true;
        searchResultsMain_UpdatePanel.Update();


        searchDT.Dispose();

    }


    private DataTable ReturnRiskAssessmentResults(string reference, string keyword, string sortOrder, string sortBy, string tier2Reference, string tier3Reference, string tier4Reference, string tier5Reference, string tier6Reference)
    {

        DataTable rDT = new DataTable();

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            //sqlText = "execute Module_RA.dbo.sp_RA_Search_dotNET '" + Tempvar_corp_code + "', '" + raReference + "', '" + intRef + "', '" + titleOrDescription + "', '" + hazardTypeSelection + "', '" + assessedBySelection + "',	'" + reviewedBySelection + "', '" + actionedToSelection + "', '" + tier2Selection + "', '" + tier3Selection + "', '" + tier4Selection + "', '" + tier5Selection + "', '" + tier6Selection + "', '" + groupSelection + "', '" + filterBy + "', '" + advancedSearchActive + "', '" + hazardRiskSelection + "', '" + riskMatrix + "', '" + overallRiskSelection + "', '" + personsAffectedSelection + "', '" + reviewDateOption + "', '" + reviewDateSelection2 + "', '" + reviewDateSelection + "', '" + createdDateOption + "', '" + createdDateSelection2 + "', '" + createdDateSelection + "', '" + nhsFrequency + "', '" + recordsYouManageCheckbox + "', '" + showOptionSelection + "', '" + sortResultsBySelection + "', '" + sortResultsByOrder + "', '" + yourAccessLevel + "', '" + yourAccountID + "', '" + yourCompany + "', '" + yourLocation + "', '" + yourDepartment + "', '" + pageNumber + "', '" + pageSize + "', '" + uniqueSearchCode + "', '" + groupBy + "', '" + riskMatrixType + "', '" + userPermsActive + "', '" + globalAdminPrivateOvRide + "', '" + accLang + "'";
            string sqlText = "execute Module_RA.dbo.sp_RA_Search_dotNET '" + CorpCode + "', '" + reference +"', '', '" + keyword + "', 'any', '0',	'', '', '" + tier2Reference + "', '" + tier3Reference + "', '" + tier4Reference + "', '" + tier5Reference + "', '" + tier6Reference + "', '0', 'Company', 'A', 'any', '', 'any', '0', '-1', 'NONE', 'NONE', '-1', 'NONE', 'NONE', '', '', 'Complete', '" + sortOrder + "', '" + sortBy + "', '0', '" + CurrentUser + "', '0', '0', '0', '10', '0', '" + uniqueSearchCode + "', 'overall', 'c', '1', 'Y', '" + ts.LanguageCode() + "'";

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            rDT.Load(objrs);

            sqlComm.Dispose();
            objrs.Close();

        }



        return rDT;

    }



    // --- End of Search Results




    // --- Attach Assessments



    private void ReturnCurrentAttachedAssessments()
    {

        string recordReference = "";

        if(RefererModule == ModuleType.Incident)
        {
            recordReference = RefererReference1 +  "/" + RefererReference2;
        }
        else
        {
            recordReference = RefererReference1;
        }
 
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            try
            {
                SqlCommand cmd = new SqlCommand("Module_Global.dbo.AttachAssessmentDisplay", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", CorpCode));
                cmd.Parameters.Add(new SqlParameter("@accRef", CurrentUser));
                cmd.Parameters.Add(new SqlParameter("@recordReference", recordReference));


                SqlDataReader objrs = cmd.ExecuteReader();
                currentAttachedAssessments_Repeater.DataSource = objrs;
                currentAttachedAssessments_Repeater.DataBind();


                objrs.Dispose();
                cmd.Dispose();


                if(currentAttachedAssessments_Repeater.Items.Count == 0)
                {
                    noAttachedAssessments_Div.Visible = true;
                    currentAttachedAssessments_Repeater.Visible = false;
                }
                else
                {
                    noAttachedAssessments_Div.Visible = false;
                    currentAttachedAssessments_Repeater.Visible = true;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

        }

        attachedAssessments_UpdatePanel.Update();

    }

    private void AttachAssessments()
    {

        // Iterate over the search results to find what has been selected
        foreach(RepeaterItem row in searchResults_Repeater.Items)
        {

            CheckBox attachAssessment_CheckBox = (CheckBox)row.FindControl("attachAssessment_CheckBox");
            HiddenField reference_HiddenField = (HiddenField)row.FindControl("reference_HiddenField");
            string reference = reference_HiddenField.Value;

            // We've found an attached item.
            if (attachAssessment_CheckBox.Checked)
            {

                AttachIndividualAssessment(reference, "");
               
            }


        }


        // Attachments will have been sorted, so lets take the user to the top of the page and show whats been attached.
        searchResultsMainContainer.Visible = false;
        searchResultsMain_UpdatePanel.Update();

        
        successAttachedAssessments_Div.Visible = true;
        ReturnCurrentAttachedAssessments();

        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scripta", "console.log(\"" + test + "\");", true);

    }


    private void AttachIndividualAssessment(string reference1, string reference2)
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            try
            {
                SqlCommand cmd = new SqlCommand("Module_Global.dbo.[AttachAssessment]", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", CorpCode));
                cmd.Parameters.Add(new SqlParameter("@accRef", CurrentUser));
                cmd.Parameters.Add(new SqlParameter("@module", Module_Code));
                cmd.Parameters.Add(new SqlParameter("@refererModule", RefererModule_Code));
                cmd.Parameters.Add(new SqlParameter("@reference1", reference1));
                cmd.Parameters.Add(new SqlParameter("@reference2", reference2));
                cmd.Parameters.Add(new SqlParameter("@refererReference1", RefererReference1));
                cmd.Parameters.Add(new SqlParameter("@refererReference2", RefererReference2));

                cmd.ExecuteNonQuery();

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }

        }



    }


    private void RemoveAssessment(string recordID)
    {

        string recordReference = "";

        if (RefererModule == ModuleType.Incident)
        {
            recordReference = RefererReference1 + "/" + RefererReference2;
        }
        else
        {
            recordReference = RefererReference1;
        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            try
            {
                SqlCommand cmd = new SqlCommand("Module_Global.dbo.[AttachAssessmentRemove]", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", CorpCode));
                cmd.Parameters.Add(new SqlParameter("@accRef", CurrentUser));
                cmd.Parameters.Add(new SqlParameter("@recordReference", recordReference));
                cmd.Parameters.Add(new SqlParameter("@recordID", recordID));


                cmd.ExecuteNonQuery();

                cmd.Dispose();
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        
        successAttachedAssessments_Div.Visible = false;
        ReturnCurrentAttachedAssessments();

    }


    // --- End of Attach Assessments




    // ---- These functions relate to Structure



    //Put the data in the dropdowns
    private void DisplayBlankStructure()
    {
        structureTier2_DropDownList.SelectedValue = null;
        structureTier3_DropDownList.SelectedValue = null;
        structureTier4_DropDownList.SelectedValue = null;
        structureTier5_DropDownList.SelectedValue = null;
        structureTier6_DropDownList.SelectedValue = null;
        tier2Reference_HiddenField.Value = "0";
        tier3Reference_HiddenField.Value = "0";
        tier4Reference_HiddenField.Value = "0";
        tier5Reference_HiddenField.Value = "0";
        tier6Reference_HiddenField.Value = "0";
        ReturnTierLevel("2", "");

    }

    private void ReturnTierLevel(string level, string previousLevelID)
    {





        switch (level)
        {
            case "2":


                structureTier2_DropDownList.Visible = true;
                structureTier3_DropDownList.Visible = false;
                structureTier4_DropDownList.Visible = false;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier2Reference_HiddenField.Value = "0";
                tier3Reference_HiddenField.Value = "0";
                tier4Reference_HiddenField.Value = "0";
                tier5Reference_HiddenField.Value = "0";
                tier6Reference_HiddenField.Value = "0";

                break;

            case "3":
                tier2Reference_HiddenField.Value = previousLevelID;


                structureTier3_DropDownList.Visible = true;
                structureTier4_DropDownList.Visible = false;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier3Reference_HiddenField.Value = "0";
                tier4Reference_HiddenField.Value = "0";
                tier5Reference_HiddenField.Value = "0";
                tier6Reference_HiddenField.Value = "0";
                break;

            case "4":
                tier3Reference_HiddenField.Value = previousLevelID;

                structureTier4_DropDownList.Visible = true;
                structureTier5_DropDownList.Visible = false;
                structureTier6_DropDownList.Visible = false;

                tier4Reference_HiddenField.Value = "0";
                tier5Reference_HiddenField.Value = "0";
                tier6Reference_HiddenField.Value = "0";
                break;

            case "5":
                tier4Reference_HiddenField.Value = previousLevelID;

                structureTier5_DropDownList.Visible = true;
                structureTier6_DropDownList.Visible = false;
                tier5Reference_HiddenField.Value = "0";
                tier6Reference_HiddenField.Value = "0";
                break;

            case "6":
                tier5Reference_HiddenField.Value = previousLevelID;

                structureTier6_DropDownList.Visible = true;
                tier6Reference_HiddenField.Value = "0";
                break;

        }

        if (!string.IsNullOrEmpty(level))
        {
            ReturnDropDownTierData(level, previousLevelID);
        }


        Structure_UpdatePanel.Update();
    }

    private void ReturnDropDownTierData(string level, string previousLevelID, string currentItem = null)
    {
        string currentLabel = "";

        bool showDisabled = true;

        using (DataTable sDT = Structure.ReturnStructure(CorpCode, CurrentUser, previousLevelID, currentItem, "SEARCH", _yourAccessLevel, "", level, ""))
        {
            try
            {

                DropDownList currentDropDown = null;

                switch (level)
                {
                    case "2":
                        currentDropDown = structureTier2_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier2Label;


                        break;

                    case "3":
                        currentDropDown = structureTier3_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier3Label;


                        break;

                    case "4":
                        currentDropDown = structureTier4_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier4Label;

                        break;

                    case "5":
                        currentDropDown = structureTier5_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier5Label;


                        break;

                    case "6":
                        currentDropDown = structureTier6_DropDownList;
                        currentDropDown.SelectedValue = null;
                        currentLabel = Tier6Label;
                        break;
                }

                currentDropDown.DataSource = null;
                currentDropDown.DataBind();
                
                if (currentDropDown != null)
                {
                    currentDropDown.DataSource = sDT;
                    currentDropDown.DataTextField = "struct_name";
                    currentDropDown.DataValueField = "tierRef";
                    currentDropDown.DataBind();
                
                    currentDropDown.Visible = currentDropDown.Items.Count > 0;
                
                    currentDropDown.Items.Insert(0, new ListItem(ts.Translate("Please select a") + " " + currentLabel + "...", "0"));
                    //if (showDisabled)
                    //{
                    //    currentDropDown.Items.Insert(1, new ListItem(ts.Translate("All") + " " + currentLabel, "-1"));
                    //}
                
                }



            }
            catch (Exception)
            {
                throw;
            }
        }

    }

    private DataTable StructureDropDown_DataTable(string level, string previousLevelID, bool showDisabled,
        string selectedItem)
    {

        DataTable sDT = new DataTable();


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlCommand cmd = new SqlCommand("Module_PEP.dbo.GetStructureTier" + level, dbConn);
                SqlDataReader objrs = null;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corpCode", CorpCode));
                cmd.Parameters.Add(new SqlParameter("@tierRef", previousLevelID));
                cmd.Parameters.Add(new SqlParameter("@showDisabled", showDisabled));
                cmd.Parameters.Add(new SqlParameter("@accRef", CurrentUser));
                if (selectedItem == null)
                {
                    cmd.Parameters.Add(new SqlParameter("@selectedItem", DBNull.Value));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@selectedItem", selectedItem));
                }



                objrs = cmd.ExecuteReader();
                sDT.Load(objrs);


                objrs.Dispose();
                cmd.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

        }


        return sDT;


    }


    protected void structureTier2_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("3", selectedOption);
    }

    protected void structureTier3_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("4", selectedOption);
    }

    protected void structureTier4_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("5", selectedOption);
    }

    protected void structureTier5_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        ReturnTierLevel("6", selectedOption);
    }

    protected void structureTier6_DropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList structDD = (DropDownList)sender;
        string selectedOption = structDD.SelectedValue;

        tier6Reference_HiddenField.Value = selectedOption;
        Structure_UpdatePanel.Update();

    }

    // ---- End of Structure DropDowns


    // This will ultimately change at some point, so TODO.
    public enum ModuleType
    {
        RiskAssessment,
        MethodStatement,
        Incident,
        FireRiskAssessment,
        ManualHandling,
        SafetyAudit
    }

    public string ReturnModuleCode(ModuleType module)
    {

        switch (module)
        {
            case ModuleType.RiskAssessment:
                return "RA";
            case ModuleType.MethodStatement:
                return "MS";
            case ModuleType.Incident:
                return "ACC";
            case ModuleType.FireRiskAssessment:
                return "FRA";
            case ModuleType.ManualHandling:
                return "MH";
            case ModuleType.SafetyAudit:
                return "SA";
            default:
                return "ERROR";
        }

    }


    protected void searchAssessment_LinkButton_Click(object sender, EventArgs e)
    {
        InitiateSearch();
    }

    protected void attachAssessments_LinkButton_Click(object sender, EventArgs e)
    {
        AttachAssessments();
    }

    protected void currentAttachedAssessments_Repeater_ItemCommand(object source, RepeaterCommandEventArgs e)
    {

        string command = e.CommandName;
        string reference = e.CommandArgument.ToString();


        switch (command)
        {
            case "Remove":
                RemoveAssessment(reference);
                break;
            default:
                break;
        }

    }

    protected void currentAttachedAssessments_Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item )
        {
            string reference = DataBinder.Eval(e.Item.DataItem, "attach_ref").ToString();
            string recLanguage = DataBinder.Eval(e.Item.DataItem, "display_language").ToString();
            string description = DataBinder.Eval(e.Item.DataItem, "attach_desc").ToString();
            string title = DataBinder.Eval(e.Item.DataItem, "attach_title").ToString();

            ((Literal)e.Item.FindControl("itemLanguageBadge")).Text = ts.GetRecordMarker(recLanguage, ts.LanguageCode()) + " ";


            string titleTrans = ts.Translate(title, recLanguage, reference, "", "", "attach_title");
            string descriptionTrans = ts.Translate(description, recLanguage, "", "", reference, "attach_description");
            ((Literal) e.Item.FindControl("attachDescription_Literal")).Text = "<strong>" + titleTrans + "</strong><br />" + descriptionTrans;




            
        }
    }

    protected void searchResults_Repeater_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item )
        {
            string reference = DataBinder.Eval(e.Item.DataItem, "reference").ToString();
            string recLanguage = DataBinder.Eval(e.Item.DataItem, "display_language").ToString();
            string description = DataBinder.Eval(e.Item.DataItem, "description").ToString();
            string title = DataBinder.Eval(e.Item.DataItem, "title").ToString();

            ((Literal)e.Item.FindControl("itemLanguageBadge")).Text = ts.GetRecordMarker(recLanguage, ts.LanguageCode()) + " ";


            string titleTrans = ts.Translate(title, recLanguage, reference, "", "", "attach_title");
            string descriptionTrans = ts.Translate(description, recLanguage, "", "", reference, "attach_description");
            ((Literal) e.Item.FindControl("attachDescription_Literal")).Text = "<h3>" + titleTrans + "</h3><br />" + descriptionTrans;




            
        }
    }
}


