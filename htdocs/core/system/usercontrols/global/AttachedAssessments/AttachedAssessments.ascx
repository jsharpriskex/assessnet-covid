﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AttachedAssessments.ascx.cs" Inherits="AttachedAssessment_UserControl" %>



<div class="col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("uc_attach_h3_riskAssessmentSearch")%></h3>
        </div>
        <div class="panel-body">

            <div id="UpdatePanel1">


                <div class="well">
                    <div class="row">
                        <div class="col-xs-6">


                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">


                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                                <tr>
                                                    <th class="text-left" style="width: 180px;"><%=ts.GetResource("uc_attach_lbl_reference")%></th>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" ID="srch_Reference_TextBox" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width: 180px;"><%=ts.GetResource("uc_attach_lbl_titleOrDesc")%></th>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" ID="srch_Keyword_TextBox" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <th class="text-left" style="width: 180px;"><%=ts.GetResource("uc_attach_lbl_sortResultsBy")%></th>
                                                    <td>
                                                        <asp:DropDownList ID="srch_sortOrder_DropDownList" runat="server" CssClass="form-control">
                                                           
                                                        </asp:DropDownList>

                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="srch_sortBy_DropDownList" runat="server" CssClass="form-control">
                                                           
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>

                                        </table>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <div class="col-xs-6">

                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="Structure_UpdatePanel">
                                            <ContentTemplate>
                                                <div class="form-group">

                                                    <table class="table table-bordered table-searchstyle01">
                                                        <tbody>
                                                            <tr>
                                                                <th><%=ts.GetResource("uc_attach_lbl_filterArea")%></th>
                                                            </tr>
                                                            <tr>
                                                                <td>

                                                                    <asp:HiddenField runat="server" ID="tier2Reference_HiddenField" />
                                                                    <asp:HiddenField runat="server" ID="tier3Reference_HiddenField" />
                                                                    <asp:HiddenField runat="server" ID="tier4Reference_HiddenField" />
                                                                    <asp:HiddenField runat="server" ID="tier5Reference_HiddenField" />
                                                                    <asp:HiddenField runat="server" ID="tier6Reference_HiddenField" />

                                                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier2_UpdatePanel">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList runat="server" ID="structureTier2_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier2_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier3_UpdatePanel">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList runat="server" ID="structureTier3_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier3_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier4_UpdatePanel">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList runat="server" ID="structureTier4_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier4_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier5_UpdatePanel">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList runat="server" ID="structureTier5_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier5_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                    <asp:UpdatePanel runat="server" RenderMode="Inline" UpdateMode="Conditional" ID="structureTier6_UpdatePanel">
                                                                        <ContentTemplate>
                                                                            <asp:DropDownList runat="server" ID="structureTier6_DropDownList" class="form-control" Style="margin-top: 3px !important; display: inline;" AutoPostBack="true" OnSelectedIndexChanged="structureTier6_DropDownList_OnSelectedIndexChanged"></asp:DropDownList>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>





                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>



            <div class="row">
                <div class="col-xs-12">
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default panel-subpanel">
                            <div class="panel-heading click" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" data-target="#collapseTwo" aria-expanded="true">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" aria-expanded="false" aria-controls="collapseTwo">
                                        <i class="fa fa-save" aria-hidden="true"></i><%=ts.GetResource("h3_attachedAssessments")%>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true" style="">
                                <div class="panel-body">

                                    <asp:UpdatePanel runat="server" ID="attachedAssessments_UpdatePanel" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="row" id="noAttachedAssessments_Div" runat="server">
                                                <div class="col-xs-12">
                                                    <div class="alert alert-warning alert-dismissible">
                                                        <strong><%=ts.GetResource("uc_attach_lbl_warning")%></strong> <%=ts.GetResource("uc_attach_lbl_useSearchFacility")%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="successAttachedAssessments_Div" runat="server" visible="false">
                                                <div class="col-xs-12">
                                                    <div class="alert alert-success alert-dismissible">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <strong><%=ts.GetResource("uc_attach_lbl_success")%></strong> <%=ts.GetResource("uc_attach_lbl_successfulAttached")%>
                                                    </div>
                                                </div>
                                            </div>
                                           

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <div id="tableSavedSearches_UpdatePanel">

                                                        <asp:Repeater runat="server" ID="currentAttachedAssessments_Repeater" OnItemCommand="currentAttachedAssessments_Repeater_ItemCommand" OnItemDataBound="currentAttachedAssessments_Repeater_OnItemDataBound">
                                                            <HeaderTemplate>
                                                                <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                                                    <thead>
                                                                        <tr>
                                                                            <th width="75px"><%#ts.Translate("Reference")%></th>          
                                                                            <th class="text-left"><%#ts.Translate("Description")%></th>
                                                                            <th width="75px"><%#ts.Translate("Options")%></th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td class="text-center"><%#Eval("attach_ref") %></td>
                                                                    <td>
                                                                        <asp:Literal runat="server" id="attachDescription_Literal"></asp:Literal>
                                                                        <asp:Literal id="itemLanguageBadge" runat="server"></asp:Literal>

                                                                    </td>
                                                                    <td class="text-center">
                                                                        <asp:LinkButton ID="removeAttachment_LinkButton" runat="server" CssClass="btn btn-danger" CommandArgument='<%#Eval("record_id") %>' CommandName="Remove"><i class="fa fa-trash"></i></asp:LinkButton>
                                                                    </td>
                                                                </tr>

                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </tbody>
                                                    </table>

                                                            </FooterTemplate>
                                                        </asp:Repeater>



                                                    </div>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <!-- End of Panel Body -->

        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <div id="ctl18">
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <%--<input type="submit" name="clearSearch" value="Clear" id="clearSearch" class="btn btn-danger">--%>
                                <asp:LinkButton runat="server" ID="searchAssessment_LinkButton" class="btn btn-success" OnClick="searchAssessment_LinkButton_Click"><%=ts.GetResource("uc_attach_btn_searchForAssessment")%></asp:LinkButton>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>


<asp:UpdatePanel runat="server" ID="searchResultsMain_UpdatePanel" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="col-xs-12" id="searchResultsMainContainer" runat="server" visible="false">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i><%=ts.GetResource("uc_attach_btn_searchForAssessment")%>
                    </h3>
                </div>


                <!-- Search results panel begins -->
                <div id="section2Panel" class="panel-body">



                    <div id="UpdateSearchResults">
                        <div id="searchResultsArea" class="searchResultsArea">
                            <div class="row " id="attachAssessments_Div" style="display: none; position:fixed; left: 50px; bottom:0; z-index:1000;">
                                <div class="col-xs-12">
                                    <div class="alert alert-warning  text-right">
                                        <asp:LinkButton runat="server" ID="attachAssessments_LinkButton" CssClass="btn btn-success" OnClick="attachAssessments_LinkButton_Click"><%=ts.GetResource("uc_attach_btn_attachAssessments")%> <span class="badge" id="countOfAssessments"></span></asp:LinkButton>
                                    </div>
                                </div>

                            </div>

                            <div class="row" id="tooManyRows_Div" runat="server" visible="false">
                                <div class="col-xs-12">
                                    <div class="alert alert-warning alert-dismissible" role="alert">
                                        <strong><%=ts.GetResource("uc_attach_lbl_warning")%></strong> <%=ts.GetResource("uc_attach_lbl_250rows")%>
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="noRows_Div" runat="server" visible="false">
                                <div class="col-xs-12">
                                    <div class="alert alert-danger" role="alert">
                                        <strong><%=ts.GetResource("uc_attach_lbl_warning")%></strong> <%=ts.GetResource("uc_attach_lbl_noResultsFound")%>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-12">
                                    <asp:UpdatePanel runat="server" ID="searchResults_UpdatePanel">
                                        <ContentTemplate>
                                            <div id="DisplayResults" class="DisplayResults">
                                                <div class="table">


                                                    <asp:Repeater runat="server" ID="searchResults_Repeater" OnItemDataBound="searchResults_Repeater_OnItemDataBound">
                                                        <HeaderTemplate>
                                                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                                                <thead>
                                                                    <tr>
                                                                        <th style="width: 10px;"><%#ts.GetResource("uc_attach_lbl_attach")%></th>
                                                                        <th style="width: 10px;"><%#ts.GetResource("uc_attach_lbl_date")%></th>
                                                                        <th style="width: 70px; text-align: left;"><%#ts.GetResource("uc_attach_lbl_reference")%></th>
                                                                        <th style="text-align: left;"><%#ts.GetResource("uc_attach_lbl_titleDescriptionPreview")%></th>
                                                                        <th style="width: 50px;"><%#ts.Translate("Options") %></th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td class="text-center">
                                                                    <asp:CheckBox ID="attachAssessment_CheckBox" runat="server" onclick="ToggleAttachmentWindow();" />
                                                                    <asp:HiddenField ID="reference_HiddenField" runat="server" Value='<%#Eval("reference") %>' />
                                                                </td>
                                                                <td class="text-center"><%# ASNETNSP.Formatting.FormatDateTime(Eval("date"),1) %></td>
                                                                <td><%#Eval("reference") %></td>
                                                                <td class="searchDescriptionCell">
                                                                    <i class="fa fa-file-text fa-2x green-colour" aria-hidden="true"></i>
                                                                    <asp:Literal runat="server" id="attachDescription_Literal"></asp:Literal>
                                                                    <asp:Literal id="itemLanguageBadge" runat="server"></asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <!-- Complete options -->
                                                                    <a class="btn btn-primary" href="javascript:OpenRAReport('<%#Eval("reference") %>');"><%#ts.Translate("Report") %></a>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody>
                                                        </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>




                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <!-- display results end -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of search results panel body -->

                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6 text-left">
                            <!-- Total Assessments -->
                        </div>


                    </div>
                </div>

            </div>
            <!-- end of search results panel -->
        </div>
    </ContentTemplate>
</asp:UpdatePanel>



<script>


    function ToggleAttachmentWindow() {
        var bulktoolschecked = $("input[id*='attachAssessment_CheckBox']:checked").length;

        if (bulktoolschecked == 0) {
            $("#attachAssessments_Div").hide();
            $("#countOfAssessments").html(bulktoolschecked);
        }
        else {
            $("#attachAssessments_Div").show();
            $("#countOfAssessments").html(bulktoolschecked);
        }

    }

    function OpenRAReport(ref) {
        window.open("/version3.2/modules/hsafety/risk_assessment/app/rassessment_report.asp?cmd=preview&ref=" + ref + "&referer=attach", "preview", "width=700,height=500,scrollbars=yes")
    }

</script>
