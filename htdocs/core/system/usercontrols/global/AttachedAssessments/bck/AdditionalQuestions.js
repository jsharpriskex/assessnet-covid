﻿function checkedDebug(test) {
    var value = $(test).is(":checked");
    console.log(value);
}


function TriggerIfChildQuestionNeeded(questionReference, answer, trigger) {

    //This function will only be called if the question has anything underneath it.
    // We need to check if "No" has been selected, if so we show another question.
    if (answer === trigger) {
        $(".question_" + questionReference).show();
    }
    else {
        $(".question_" + questionReference).hide();
    }


}


function ValidateAdditionalControl(sender, args) {

    var isValid = true;



    // Loop through each row
    $('.inputRow:visible').each(function (i, obj) {
        var $this = $(this);
        var foundCheckboxes = false;
        var foundCheckedbox = false;


        // Radio Buttons
        // Loop through any found radios
        $this.find("input:radio").each(function () {

            foundCheckboxes = true;

            // Ok we know there are checkboxes, so we have to see if one has been selected
            if ($(this).is(':checked')) {
                foundCheckedbox = true;
            }


        });
        if (foundCheckboxes == true && foundCheckedbox == false) {
            isValid = false;
            $this.addClass("inputError");
        }
        else if (foundCheckboxes == true && foundCheckedbox == true) {
            $this.removeClass("inputError");
        }



        //Short Text
        //Large Text
        //Numeric
        //Date
        //Time
        $this.find("input[type=text],textarea").each(function () {
            var currentText = $(this).val();

            if (currentText == "" || currentText == "Please type a name or select a user") {
                isValid = false;
                $this.addClass("inputError");
            }
            else {
                $this.removeClass("inputError");
            }

        });


        //DropDown
        $this.find("select").each(function () {
            var currentText = $(this).val();

            if (currentText == "na") {
                isValid = false;
                $this.addClass("inputError");
            }
            else {
                $this.removeClass("inputError");
            }

        });




    });

    if (isValid == false) {
        // alert("Invalid Data Entry. Please enter valid information for the sections highlighted above in red.");
        $("#alert").modal('show');
    }


    args.IsValid = isValid;

}