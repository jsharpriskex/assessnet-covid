﻿using System;
using System.Data.SqlClient;
using ASNETNSP;

public partial class LicenceRenewalCore :  System.Web.UI.UserControl
{
    public string corpCode;
    public string yourAccRef;
    public string accLang;
    public string yourAccessLevel;
    public string renewalDate;
    public int renewalCountdown;
    public bool renewalOverdue;

    public TranslationServices tsUC;

    protected void Page_Load(object sender, EventArgs e)
    {

        corpCode = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();

        //***************************
        // INITIATE AND SET TRANSLATIONS
        tsUC = new TranslationServices(corpCode, accLang, "usercontrol");
        // END TRANSLATIONS
        //***************************

        yourAccessLevel = SharedComponants.getPreferenceValue(corpCode, yourAccRef, "YOUR_ACCESS", "user");

        RenewalWrapper.Visible = false;

        if (yourAccessLevel == "0")
        {
            getRenewalDetails();
        }

        

    }


    public void getRenewalDetails()
    {
        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                string sqlText = "execute Module_HR.dbo.GetLicenceRenewalDate '" + corpCode + "' ";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    renewalDate = objrs["next_renewal_date"].ToString();
                    renewalCountdown = Int32.Parse(objrs["countdown"].ToString());
                    renewalOverdue = Convert.ToBoolean(objrs["overdue"].ToString());

                    RenewalWrapper.Visible = true;
                    lbl_licence_date.Text = tsUC.GetResource("uc_renewal_dueon") + ": " + Formatting.FormatDateTime(renewalDate, 1);



                    if (renewalOverdue == true)
                    {

                        div_licence_alert.Attributes.Add("class", "alert alert-danger");
                        i_licence_icon.Attributes.Add("class", "fa fa-3x fa-hourglass-end");
                        lbl_licence_header.Text = tsUC.GetResource("uc_renewal_licenceoverdue_title") + ": " + renewalCountdown + " " + tsUC.GetResource("uc_renewal_days");
                        lbl_licence_body.Text = tsUC.GetResource("uc_renewal_ensurepaidurgently");

                    }
                    else if (renewalCountdown <= 60)
                    {

                        lbl_licence_header.Text = tsUC.GetResource("uc_renewal_licencedue_title") + ": " + renewalCountdown + " " + tsUC.GetResource("uc_renewal_days");
                        lbl_licence_body.Text = tsUC.GetResource("uc_renewal_ensurepaidbeforeduedate");
                    }



                }


                objrs.Close();

            }

        }
        catch (Exception)
        {
            throw;
        }
    }


}