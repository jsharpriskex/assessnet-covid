﻿using System;
using System.Data.SqlClient;
using ASNETNSP;

public partial class EmailBlacklistCore : System.Web.UI.UserControl
{
    public string corpCode;
    public string yourAccRef;
    public string accLang;

    public TranslationServices tsUC;

    protected void Page_Load(object sender, EventArgs e)
    {
        corpCode = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();

        //***************************
        // INITIATE AND SET TRANSLATIONS
        tsUC = new TranslationServices(corpCode, accLang, "usercontrol");
        // END TRANSLATIONS
        //***************************

        BlacklistWrapper.Visible = false;

        GetBlacklistDetails();

    }


    public void GetBlacklistDetails()
    {
        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                string sqlText = "execute Module_SD.dbo.CheckBlacklistStatus '" + corpCode + "', '" + yourAccRef + "' ";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

                if (objrs.HasRows)
                {

                    objrs.Read();

                    DateTime accountDeactivation = Convert.ToDateTime(objrs[0]).AddDays(15);
                    DateTime current = DateTime.Now;

                    int timeRemaining = (int)(accountDeactivation - current).TotalDays;

                    BlacklistWrapper.Visible = true;

                    lbl_blacklist_header.Text = tsUC.GetResource("uc_blacklist_header");
                    lbl_blacklist_body.Text = "<p>" + tsUC.GetResource("uc_blacklist_body") + "</p>" +
                                              "<p><strong>" + tsUC.GetResource("uc_blacklist_duedate") + " " + timeRemaining.ToString() + " " + tsUC.GetResource("uc_renewal_days") + "</strong></p>";
                    
                }

                objrs.Close();
                dbConn.Dispose();
            }

        }
        catch (Exception)
        {
            throw;
        }
    }

}