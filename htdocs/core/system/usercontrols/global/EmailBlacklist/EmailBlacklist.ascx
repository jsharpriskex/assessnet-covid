﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="EmailBlacklist.ascx.cs" Inherits="EmailBlacklistCore" %>

<div class="row" runat="server" id="BlacklistWrapper">
    <div class="col-md-12">

        <div class="alert alert-warning" role="alert" runat="server" id="div_blacklist_alert" style="margin-left:20px;margin-right:20px;padding-right:5px;">
                    
        <div class="media">
                <div class="media-left">
                    <i class="fa fa-3x fa-hourglass-half" runat="server" id="i_blacklist_icon" aria-hidden="true"></i>
                </div>
                <div class="media-body">
                <h4 class="media-heading" style="margin-bottom:0px;margin-top:2px;"><asp:Literal id="lbl_blacklist_header" runat="server"></asp:Literal></h4>
                <asp:Literal id="lbl_blacklist_body" runat="server"></asp:Literal>
                </div>
            </div>
        </div>

    </div>
</div>
