﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="RecurringTask.ascx.cs" Inherits="RecurringTask_UserControl" %>


<asp:UpdatePanel ID="testText_UP" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Label runat="server" ID="testText"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>




<!-- Task Modal -->
<div class="modal fade" id="openPlanningTaskModal" tabindex="-1" role="dialog" aria-labelledby="openPlanningTaskModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <!-- Alert -->
            <div class="modal fade in" id="recurring_Alert" tabindex="-1" role="dialog" data-backdrop="static">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id=""><% =ts.GetResource("uc_recurring_invaliddataentry") %></h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <% =ts.GetResource("uc_recurring_entervalidinfo") %> <span style="color: #c00c00; font-weight: bold;"><% =ts.GetResource("uc_recurring_red") %></span>.

                                       
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                    <button type="button" class="btn btn-danger" id="closeRecurringAlert" onclick="$('#recurring_Alert').modal('hide');"><% =ts.GetResource("uc_recurring_close") %></button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:UpdatePanel id="label_UpdatePanel" updatemode="conditional" runat="server">
                    <ContentTemplate>
                        <h4 class="modal-title" id="openPlanningTaskModalLabel"><asp:Label runat="server" id="taskModalHeader_Label"></asp:Label></h4>
                    </ContentTemplate>
                </asp:UpdatePanel>
                
            </div>
            <div class="modal-body modalScroll" style="height: 500px; overflow-y: scroll;" id="ModalBody">

                <div class="panel-body">







                    <asp:UpdatePanel ID="planningTask_UpdatePanel" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="recurringReference_HiddenField" runat="server" />
                            <asp:HiddenField ID="recurringType_HiddenField" runat="server" />
                            <asp:HiddenField ID="recurringCommand_HiddenField" runat="server" />
                            <asp:HiddenField ID="recurringModuleReference_HiddenField" runat="server" />
                            <asp:HiddenField ID="recurringModuleActionReference_HiddenField" runat="server" />



                            <asp:Label id="debugText" runat="server" />


                            <div class="panel-group" id="recurringAccordion" role="tablist" aria-multiselectable="false">

                                <!-- First Section -->
                                <div class="panel panel-default panel-subpanel">
                                    <div class="panel-heading" role="tab" id="firstSection">
                                        <h4 class="panel-title" style="font-weight: 100">
                                            <i class="fa fa-bars" aria-hidden="true"></i><% =ts.GetResource("uc_recurring_step1") %>
                                        </h4>
                                    </div>
                                    <div id="collapseFirstSection" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="firstSection">
                                        <div class="panel-body">
                                            <div class="row form-group">
                                                <div class="col-xs-6">
                                                    <div class="row" id="auditTemplate_Div" runat="server" visible="false">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="creation_Template_DropDownList"><% =ts.GetResource("uc_recurring_audittemplate") %></label>
                                                                <asp:DropDownList runat="server" CssClass="form-control" ID="creation_Template_DropDownList"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="creation_Task_For">
                                                                    <asp:Literal ID="EventFor_Literal" runat="server"></asp:Literal></label>
                                                                <telerik:RadComboBox runat="server" ID="creation_Task_For"
                                                                    EnableViewState="false"
                                                                    Width="100%"
                                                                    EnableVirtualScrolling="true"
                                                                    DropDownWidth="500px"
                                                                    DropDownHeight="300px"
                                                                    Skin="Bootstrap"
                                                                    HighlightTemplatedItems="true"
                                                                    Filter="Contains"
                                                                    EnableLoadOnDemand="True"
                                                                    AppendDataBoundItems="True"
                                                                    AllowCustomText="false"
                                                                    Height="300"
                                                                    ShowMoreResultsBox="true"
                                                                    OnItemsRequested="generic_Blank_ItemsRequested">
                                                                    <ItemTemplate>

                                                                        <%# Container.DataItem != null ? DataBinder.Eval(Container.DataItem, "user_details") : DataBinder.Eval(Container, "Text") %>
                                                                    </ItemTemplate>
                                                                </telerik:RadComboBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="row" id="eventSubject_Div" runat="server" visible="false">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="creation_Subject_TextBox"><% =ts.GetResource("uc_recurring_eventsubject") %></label>
                                                                <asp:TextBox runat="server" id="creation_Subject_TextBox" CssClass="form-control"  />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <asp:UpdatePanel ID="creationStructure_UpdatePanel" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>

                                                            <asp:Label runat="server" ID="Label1" />

                                                            <div class="form-group">

                                                                <table class="table table-bordered table-searchstyle01">
                                                                    <tr>
                                                                        <th style="background-color: #FFF4E6 !important;"><% =ts.GetResource("uc_recurring_associatetospecificarea") %></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <asp:DropDownList runat="server" ID="structureTier1" class="form-control" OnSelectedIndexChanged="structureTier1_SelectedIndexChanged_Create" AutoPostBack="true" Style="display: inline;"></asp:DropDownList>
                                                                            <asp:DropDownList runat="server" ID="structureTier2" class="form-control" Style="margin-top: 3px !important; display: inline;" OnSelectedIndexChanged="structureTier2_SelectedIndexChanged_Create" AutoPostBack="true"></asp:DropDownList>
                                                                            <asp:DropDownList runat="server" ID="structureTier3" class="form-control" Style="margin-top: 3px !important; display: inline;" OnSelectedIndexChanged="structureTier3_SelectedIndexChanged_Create" AutoPostBack="true"></asp:DropDownList>
                                                                            <asp:DropDownList runat="server" ID="structureTier4" class="form-control" Style="margin-top: 3px !important; display: inline;" OnSelectedIndexChanged="structureTier4_SelectedIndexChanged_Create" AutoPostBack="true"></asp:DropDownList>
                                                                            <asp:DropDownList runat="server" ID="structureTier5" class="form-control" Style="margin-top: 3px !important; display: inline;"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </table>

                                                                <asp:Label ID="Label2" runat="server"></asp:Label>

                                                            </div>




                                                        </ContentTemplate>

                                                    </asp:UpdatePanel>

                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col-xs-12">
                                                    <label for="creation_Comments_TextBox">
                                                        <asp:Literal ID="EventDetails_Literal" runat="server"></asp:Literal></label>
                                                    <asp:TextBox TextMode="MultiLine" runat="server" CssClass="form-control" ID="creation_Comments_TextBox"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <button type="button" class="btn btn-primary" onclick="SaveGeneralInformation()"><% =ts.GetResource("uc_recurring_nextsection") %></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of First Section -->


                                <!-- Second Section -->
                                <div class="panel panel-default panel-subpanel">
                                    <div class="panel-heading" role="tab" id="secondSection">
                                        <h4 class="panel-title" style="font-weight: 100">
                                            <i class="fa fa-bars" aria-hidden="true"></i><% =ts.GetResource("uc_recurring_step2") %>
                                        </h4>
                                    </div>
                                    <div id="collapseSecondSection" class="panel-collapse collapse" role="tabpanel" aria-labelledby="secondSection" style="overflow-y:scroll; max-height:350px;">
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_schedule") %></label>
                                                                <asp:DropDownList runat="server" CssClass="form-control" ID="recurringType_DropDownList" onchange="RecurringTask_ShowDivs()" ClientIDMode="Static">
                                                                    <asp:ListItem id="recurringType_DropDownListOnce" runat="server" Value="O" Text="Once"></asp:ListItem>
                                                                    <asp:ListItem id="recurringType_DropDownListDaily" runat="server" Value="D" Text="Daily" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem id="recurringType_DropDownListWeekly" runat="server" Value="W" Text="Weekly"></asp:ListItem>
                                                                    <asp:ListItem id="recurringType_DropDownListMonthly" runat="server" Value="M" Text="Monthly"></asp:ListItem>
                                                                    <asp:ListItem id="recurringType_DropDownListAnnually" runat="server" Value="A" Text="Annually"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>




                                            </div>

                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <!-- Once Criteria -->
                                                    <div class="row form-group" id="onceCriteria" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_date") %></label>
                                                                <div style="border: 1px solid #ddd; padding: 10px;">
                                                                    <div class="row">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                            <div class="col-xs-6">
                                                                                <telerik:RadDatePicker Skin="Bootstrap" runat="server" Width="100%" ID="once_RadDatePicker"></telerik:RadDatePicker>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Daily Criteria -->


                                                    <!-- Daily Criteria -->
                                                    <div class="row form-group" id="dailyCriteria" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_frequency") %></label>
                                                                <div style="border: 1px solid #ddd; padding: 10px;" id="frequencyDailyOptions">
                                                                    <div class="row">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                            <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                <div style="margin: 0 auto;">
                                                                                    <asp:RadioButton runat="server" GroupName="DailyRecurrence" ID="Daily_Days" />
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-xs-1" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_every") %>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <telerik:RadNumericTextBox Skin="Bootstrap" Width="100px" runat="server" ID="Daily_XDays" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                            </div>
                                                                            <div class="col-xs-1 " style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_days") %>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                            <div class="col-xs-1 text-right" style="padding-top: 0px;">
                                                                                <div style="margin: 0 auto;">
                                                                                    <asp:RadioButton runat="server" GroupName="DailyRecurrence" ID="Daily_WeekDays" />
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-xs-9" style="padding-top: 0px;">
                                                                                <% =ts.GetResource("uc_recurring_weekday") %>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Daily Criteria -->


                                                    <!-- Weekly Criteria -->
                                                    <div class="row" id="weeklyCriteria" style="display: none; margin-bottom: 15px;">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <div class="form-group" style="margin-bottom: 0px;">
                                                                    <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_frequency") %></label>
                                                                </div>
                                                                <div style="border: 1px solid #ddd; padding: 10px;" id="frequencyWeeklyOptions">
                                                                    <div class="row">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                            <div class="col-xs-1" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_every") %>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <telerik:RadNumericTextBox Skin="Bootstrap" Width="100px" runat="server" ID="WeeklyXWeeks" Value="0" MinValue="0" MaxValue="52" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                            </div>
                                                                            <div class="col-xs-2" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_weekson") %>
                                                                            </div>
                                                                        </div>


                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="margin: 10px auto;">
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Monday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_mon") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Tuesday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_tue") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Wednesday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_wed") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Thursday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_thu") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>

                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Friday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_fri") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" ID="weekly_Saturday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_sat") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>
                                                                                <div style="width: 50px; display: inline-block;" class="text-center">
                                                                                    <label>
                                                                                        <asp:CheckBox runat="server" CssClass="" ID="weekly_Sunday" />
                                                                                        <div style="display: inline-block; width: 30px; font-weight: 100;">
                                                                                            <% =ts.GetResource("uc_recurring_sun") %>
                                                                                        </div>
                                                                                    </label>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Weekly Criteria -->

                                                    <!-- Monthly Criteria -->
                                                    <div class="row form-group" id="monthlyCriteria" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_recurevery") %></label>
                                                                <div style="border: 1px solid #ddd; padding: 10px;" id="frequencyMonthlyOptions">
                                                                    <!-- Day xx every xx months -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="MonthlyRecurrence" ID="Monthly_DayEveryMonth" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-1" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_day") %>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="Monthly_1_Period" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                                </div>
                                                                                <div class="col-xs-1 text-center" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_every") %>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="Monthly_1_Months" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                                </div>
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_months") %>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <!-- the xx xx of every x months -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="MonthlyRecurrence" ID="MonthlyPositionDayMonth" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-1" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_the") %>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Monthly_2_Interval">
                                                                                        <asp:ListItem id="Monthly_2_IntervalFirst" runat="server" Text="First" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_IntervalSecond" runat="server" Text="Second" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_IntervalThird" runat="server" Text="Third" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_IntervalFourth" runat="server" Text="Fourth" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_IntervalLast" runat="server" Text="Last" Value="5"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Monthly_2_Day">
                                                                                        <asp:ListItem id="Monthly_2_DayMonday" runat="server" Text="Monday" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DayTuesday" runat="server" Text="Tuesday" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DayWednesday" runat="server" Text="Wednesday" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DayThursday" runat="server" Text="Thursday" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DayFriday" runat="server" Text="Friday" Value="5"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DaySaturday" runat="server" Text="Saturday" Value="6"></asp:ListItem>
                                                                                        <asp:ListItem id="Monthly_2_DaySunday" runat="server" Text="Sunday" Value="7"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="col-xs-1 text-center" style="padding-top: 7px;">
                                                                                     <% =ts.GetResource("uc_recurring_every") %>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="Monthly_2_Period" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                                </div>
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_months") %>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Month Criteria -->


                                                    <!-- Annual Criteria -->
                                                    <div class="row form-group" id="annualCriteria" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_recurevery") %></label>
                                                                <div style="border: 1px solid #ddd; padding: 10px;" id="frequencyAnnualOptions">
                                                                    <!-- Day every month 1st -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="AnnualRecurrence" ID="Annual_EveryMonthDay" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-1" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_every") %>
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Annually_1_Month">
                                                                                        <asp:ListItem id="Annually_1_MonthJanuary" runat="server" Text="January" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthFebruary" runat="server" Text="February" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthMarch" runat="server" Text="March" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthApril" runat="server" Text="April" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthMay" runat="server" Text="May" Value="5"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthJune" runat="server" Text="June" Value="6"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthJuly" runat="server" Text="July" Value="7"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthAugust" runat="server" Text="August" Value="8"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthSeptember" runat="server" Text="September" Value="9"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthOctober" runat="server" Text="October" Value="10"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthNovember" runat="server" Text="November" Value="11"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_1_MonthDecember" runat="server" Text="December" Value="12"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="Annually_1_Month_Day" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <!-- the xx xx of every x months -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="AnnualRecurrence" ID="Annual_XDayMonth" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-1" style="padding-top: 7px;">
                                                                                    The
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Annually_2_Interval">
                                                                                        <asp:ListItem id="Annually_2_IntervalFirst" runat="server" Text="First" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_IntervalSecond" runat="server" Text="Second" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_IntervalThird" runat="server" Text="Third" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_IntervalFourth" runat="server" Text="Fourth" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_IntervalLast" runat="server" Text="Last" Value="5"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Annually_2_Day">
                                                                                        <asp:ListItem id="Annually_2_DayMonday" runat="server" Text="Monday" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DayTuesday" runat="server" Text="Tuesday" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DayWednesday" runat="server" Text="Wednesday" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DayThursday" runat="server" Text="Thursday" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DayFriday" runat="server" Text="Friday" Value="5"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DaySaturday" runat="server" Text="Saturday" Value="6"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_DaySunday" runat="server" Text="Sunday" Value="7"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="col-xs-1 text-center" style="padding-top: 7px;">
                                                                                    Of
                                                                                </div>
                                                                                <div class="col-xs-3">
                                                                                    <asp:DropDownList CssClass="form-control" runat="server" ID="Annually_2_Month">
                                                                                        <asp:ListItem id="Annually_2_MonthJanuary" runat="server" Text="January" Value="1"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthFebruary" runat="server" Text="February" Value="2"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthMarch" runat="server" Text="March" Value="3"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthApril" runat="server" Text="April" Value="4"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthMay" runat="server" Text="May" Value="5"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthJune" runat="server" Text="June" Value="6"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthJuly" runat="server" Text="July" Value="7"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthAugust" runat="server" Text="August" Value="8"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthSeptember" runat="server" Text="September" Value="9"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthOctober" runat="server" Text="October" Value="10"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthNovember" runat="server" Text="November" Value="11"></asp:ListItem>
                                                                                        <asp:ListItem id="Annually_2_MonthDecember" runat="server" Text="December" Value="12"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Month Criteria -->


                                                    <!-- Recur Criteria -->
                                                    <div class="row form-group" id="frequency">
                                                        <div class="col-xs-12">
                                                            <div class="">
                                                                <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_endcriteria") %></label>
                                                                <div style="border: 1px solid #ddd; padding: 10px;" id="endCriteria">
                                                                    <!-- No End Date -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="Frequency" ID="Frequency_NoEndDate" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-2" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_noenddate") %>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <!-- end after -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="Frequency" ID="Frequency_EndAfter" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-2" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_endafter") %>
                                                                                </div>
                                                                                <div class="col-xs-2">
                                                                                    <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="Frequency_Occurrence" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- end after -->
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                                <div class="col-xs-1 text-right" style="padding-top: 7px;">
                                                                                    <div>
                                                                                        <asp:RadioButton runat="server" GroupName="Frequency" ID="Frequency_EndBy" />
                                                                                    </div>

                                                                                </div>
                                                                                <div class="col-xs-2" style="padding-top: 7px;">
                                                                                    <% =ts.GetResource("uc_recurring_endby") %>
                                                                                </div>
                                                                                <div class="col-xs-6">
                                                                                    <telerik:RadDatePicker Skin="Bootstrap" runat="server" Width="100%" ID="Frequency_EndDate"></telerik:RadDatePicker>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End of Recur Criteria -->
                                                </div>
                                            </div>


                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <button type="button" class="btn btn-primary" onclick="PreviousSection('1')"><% =ts.GetResource("uc_recurring_previoussection") %></button>
                                                    <button type="button" class="btn btn-primary" onclick="SaveRecurringInformation()" id="nextSectionToTaskManager"><% =ts.GetResource("uc_recurring_nextsection") %></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Second Section -->


                                <!-- Third Section -->
                                <div class="panel panel-default panel-subpanel" id="taskManagerSettings">
                                    <div class="panel-heading" role="tab" id="thirdSection">
                                        <h4 class="panel-title" style="font-weight: 100">
                                            <i class="fa fa-bars" aria-hidden="true"></i><% =ts.GetResource("uc_recurring_step3") %>
                                        </h4>
                                    </div>
                                    <div id="collapseThirdSection" class="panel-collapse collapse" role="tabpanel" aria-labelledby="thirdSection">
                                        <div class="panel-body">
                                            <div class="col-xs-12">

                                                <!-- Add to Task Manager Criteria -->
                                                <div class="row form-group" id="taskmanager">
                                                    <div class="col-xs-12">
                                                        <div class="">
                                                            <label for="recurringType_DropDownList"><% =ts.GetResource("uc_recurring_taskmanager") %></label>
                                                            <div style="border: 1px solid #ddd; padding: 10px;">
                                                                <!-- Add to -->
                                                                <div class="row" id="taskmanager_DisplayBefore">
                                                                    <div class="col-xs-12">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">

                                                                            <div class="col-xs-3" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_addtotaskmanager") %>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <telerik:RadNumericTextBox Skin="Bootstrap" Width="75px" runat="server" ID="TaskManager_DisplayBefore" Value="15" MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
                                                                            </div>
                                                                            <div class="col-xs-4" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_daysbeforetaskisdue") %>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <!-- Assign tasks from -->
                                                                <div class="row" id="taskmanager_AssignTasksFrom">
                                                                    <div class="col-xs-12">
                                                                        <div style="padding: 5px; display: inline-block; width: 100%;">
                                                                            <div class="col-xs-3" style="padding-top: 7px;">
                                                                                <% =ts.GetResource("uc_recurring_assigntasksfrom") %>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <telerik:RadDatePicker Skin="Bootstrap" runat="server" Width="100%" ID="TaskManager_StartAfter"></telerik:RadDatePicker>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End of Task Manager Criteria -->


                                            </div>
                                        </div>
                                        <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">
                                                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                                                        <ContentTemplate>
                                                            <button type="button" class="btn btn-primary" onclick="PreviousSection('2')"><% =ts.GetResource("uc_recurring_previoussection") %></button>
                                                            
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End of Third Section -->


                            </div>


                           
                        </ContentTemplate>
                    </asp:UpdatePanel>


                </div>

            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <asp:UpdatePanel ID="modalButton_UpdatePanel" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <button type="button" class="btn btn-danger" data-dismiss="modal"><% =ts.GetResource("uc_recurring_cancel") %></button>
                                <button type="button" class="btn btn-default" id="processPlan_Button_Disabled" data-toggle="tooltip" title="<%=ts.GetResource("uc_recurring_optiondisabled") %>"><asp:Label runat="server" id="processPlan_Button_Delete" /></button>
                                <asp:Button runat="server" CssClass="btn btn-success" Text="" ID="processPlan_Button" OnClick="processPlan_Button_Click" ValidationGroup="RecurringTask" style="display:none;" ClientIDMode="static" />
                                <asp:CustomValidator runat="server" ID="recurringTask_CustomValidator" ValidationGroup="RecurringTask" Display="Dynamic" ClientValidationFunction="ValidateRecurringTask"></asp:CustomValidator>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End of Task Modal -->


<!-- Remove Modal -->
<div class="modal fade" id="remModal_Recurring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <asp:UpdatePanel ID="UpdateRunRemoveModal" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <asp:HiddenField runat="server" ID="removeRecurringID" />
                    <asp:HiddenField runat="server" ID="removeRecurringTaskType" />

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="runRemoveModalLabel"><% =ts.GetResource("uc_recurring_remove") %> <asp:Label runat="server" ID="removalTextLabel"><% =ts.GetResource("uc_recurring_recurringtask") %></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                       
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <% =ts.GetResource("uc_recurring_areyousure") %> <asp:Label runat="server" ID="removalTextLabel2"><% =ts.GetResource("uc_recurring_recurringtask") %></asp:label> <% =ts.GetResource("uc_recurring_entry") %>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <asp:Button ID="removeAssessment" runat="server" class="btn btn-success" Text="Confirm" OnClick="removeAssessment_Click" OnClientClick=" $('#remModal').modal('hide');" />&nbsp;
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><% =ts.GetResource("uc_recurring_cancel") %></button>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
</div>
<!-- End of Remove Modal -->

<script src="/core/system/usercontrols/global/RecurringTask/RecurringTask.js?d2"></script>
