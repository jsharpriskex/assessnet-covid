﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;

public partial class RecurringTask_UserControl : System.Web.UI.UserControl
{

    

    public event EventHandler UpdateSearchResults_EventHandler;

    private string _corpCode;
    private string _currentUser;
    public string _accLang;


    private string _tier2Ref;
    private string _tier3Ref;
    private string _tier4Ref;
    private string _tier5Ref;
    private string _tier6Ref;

    private string _taskModRef;

    private string _typeOfTask;

    private string _yourAccessLevel;

    private string _recurrenceID;
    private string _recurrenceType;

    private string _moduleReference;
    private string _moduleActionReference;

    private bool _isRecurringTask;

    public string AssociatedRecurringTask;

    public TranslationServices ts;

    protected void Page_Load(object sender, EventArgs e)
    {

        //***************************
        // CHECK THE SESSION STATUS
        _corpCode = Context.Session["CORP_CODE"].ToString();
        _currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        _yourAccessLevel = SharedComponants.getPreferenceValue(_corpCode, _currentUser, "YOUR_ACCESS", "user");
        _accLang = Context.Session["YOUR_LANGUAGE"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        //***************************
        // INITIATE AND SET TRANSLATIONS
        ts = new TranslationServices(_corpCode, _accLang, "taskmanager");
        // END TRANSLATIONS
        //***************************

        //string combinedReference = recurringReference_HiddenField.Value;

        //_recurrenceID = combinedReference.Split('_')[1];
        //_recurrenceType = combinedReference.Split('_')[0];

        //References set when initiating the control

        _moduleReference = recurringModuleReference_HiddenField.Value;
        _moduleActionReference = recurringModuleActionReference_HiddenField.Value;

        creation_Task_For.EmptyMessage = ts.Translate("Please type a name or select a user");

        recurringType_DropDownListOnce.Text = ts.Translate("Once");
        recurringType_DropDownListDaily.Text = ts.Translate("Daily");
        recurringType_DropDownListWeekly.Text = ts.Translate("Weekly");
        recurringType_DropDownListMonthly.Text = ts.Translate("Monthly");
        recurringType_DropDownListAnnually.Text = ts.Translate("Annually");


        Monthly_2_IntervalFirst.Text = ts.Translate("First");
        Monthly_2_IntervalSecond.Text = ts.Translate("Second");
        Monthly_2_IntervalThird.Text = ts.Translate("Third");
        Monthly_2_IntervalFourth.Text = ts.Translate("Fourth");
        Monthly_2_IntervalLast.Text = ts.Translate("Last");


        Monthly_2_DayMonday.Text = ts.Translate("Monday");
        Monthly_2_DayTuesday.Text = ts.Translate("Tuesday");
        Monthly_2_DayWednesday.Text = ts.Translate("Wednesday");
        Monthly_2_DayThursday.Text = ts.Translate("Thursday");
        Monthly_2_DayFriday.Text = ts.Translate("Friday");
        Monthly_2_DaySaturday.Text = ts.Translate("Saturday");
        Monthly_2_DaySunday.Text = ts.Translate("Sunday");

        Annually_1_MonthJanuary.Text = ts.Translate("January");
        Annually_1_MonthFebruary.Text = ts.Translate("February");
        Annually_1_MonthMarch.Text = ts.Translate("March");
        Annually_1_MonthApril.Text = ts.Translate("April");
        Annually_1_MonthMay.Text = ts.Translate("May");
        Annually_1_MonthJune.Text = ts.Translate("June");
        Annually_1_MonthJuly.Text = ts.Translate("July");
        Annually_1_MonthAugust.Text = ts.Translate("August");
        Annually_1_MonthSeptember.Text = ts.Translate("September");
        Annually_1_MonthOctober.Text = ts.Translate("October");
        Annually_1_MonthNovember.Text = ts.Translate("November");
        Annually_1_MonthDecember.Text = ts.Translate("December");


        Annually_2_IntervalFirst.Text = ts.Translate("First");
        Annually_2_IntervalSecond.Text = ts.Translate("Second");
        Annually_2_IntervalThird.Text = ts.Translate("Third");
        Annually_2_IntervalFourth.Text = ts.Translate("Fourth");
        Annually_2_IntervalLast.Text = ts.Translate("Last");

        Annually_2_DayMonday.Text = ts.Translate("Monday");
        Annually_2_DayTuesday.Text = ts.Translate("Tuesday");
        Annually_2_DayWednesday.Text = ts.Translate("Wednesday");
        Annually_2_DayThursday.Text = ts.Translate("Thursday");
        Annually_2_DayFriday.Text = ts.Translate("Friday");
        Annually_2_DaySaturday.Text = ts.Translate("Saturday");
        Annually_2_DaySunday.Text = ts.Translate("Sunday");

        Annually_2_MonthJanuary.Text = ts.Translate("January");
        Annually_2_MonthFebruary.Text = ts.Translate("February");
        Annually_2_MonthMarch.Text = ts.Translate("March");
        Annually_2_MonthApril.Text = ts.Translate("April");
        Annually_2_MonthMay.Text = ts.Translate("May");
        Annually_2_MonthJune.Text = ts.Translate("June");
        Annually_2_MonthJuly.Text = ts.Translate("July");
        Annually_2_MonthAugust.Text = ts.Translate("August");
        Annually_2_MonthSeptember.Text = ts.Translate("September");
        Annually_2_MonthOctober.Text = ts.Translate("October");
        Annually_2_MonthNovember.Text = ts.Translate("November");
        Annually_2_MonthDecember.Text = ts.Translate("December");

        processPlan_Button.Text = ts.GetResource("uc_recurring_createplan");
        

        if (!IsPostBack)
        {
            InitiateBlankStructure();
            EventFor_Literal.Text = ts.GetResource("uc_recurring_eventfor");
            EventDetails_Literal.Text = ts.GetResource("uc_recurring_eventdetails");
        }

    }

    public override void Dispose()
    {
        if (ts != null)
        {
            ts.Dispose();
        }
        base.Dispose();
    }


    // Initiating Functions
    public void IntiateControl(string recurringID, bool isCopy)
    {

        //Populate the hiddenfield for the control with a variable thats called from page.
        recurringReference_HiddenField.Value = recurringID;
        recurringType_HiddenField.Value = "";

        if (!isCopy)
        {
            recurringCommand_HiddenField.Value = "update";
        }
        else
        {
            recurringCommand_HiddenField.Value = "add";
        }
        

        
        //Generate references
        string combinedReference = recurringReference_HiddenField.Value;
        _recurrenceID = combinedReference.Split('_')[1];
        _recurrenceType = combinedReference.Split('_')[0];


        // If the type is "Once", then its not a recurring task.
        _isRecurringTask = _recurrenceType == "Once" ? false : true;

        // Populate Recurring Task Information Here
        CreateFromDB();

        planningTask_UpdatePanel.Update();

        
        modalButton_UpdatePanel.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script123123123", "RecurringTask_ShowDivs();$('#openPlanningTaskModal').modal('show');", true);
    }


    public void InitiateRemoveRecurrence(string recurringID, string taskType)
    {

        //Generate references
        string combinedReference = recurringID;
        _recurrenceID = combinedReference.Split('_')[1];
        _recurrenceType = combinedReference.Split('_')[0];

        // If the type is "Once", then its not a recurring task.
        _isRecurringTask = _recurrenceType == "Once" ? false : true;

        removeRecurringID.Value = _recurrenceID;
        removeRecurringTaskType.Value = taskType;

        if (taskType == "APS")
        {
            removalTextLabel.Text = ts.GetResource("uc_recurring_auditplan"); ;
            removalTextLabel2.Text = ts.GetResource("uc_recurring_auditplan");
            
        }

        UpdateRunRemoveModal.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "showRecurringRemoveModal", "$('#remModal_Recurring').modal('show');", true);

    }

    protected void removeAssessment_Click(object sender, EventArgs e)
    {
        RemoveRecurrence();
    }

    private void RemoveRecurrence()
    {
        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_Global.dbo.RecurrenceRemove_BYID", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@recurring_id", removeRecurringID.Value));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", _currentUser));



                objrs = cmd.ExecuteReader();
                objrs.Close();
            }
        }
        catch
        {
            throw;
        }

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hideRecurringRemoveModal", "$('#remModal_Recurring').modal('hide'); ", true);
        UpdateSearchResults_EventHandler(this, EventArgs.Empty);
    }


    public void CreateNew(string taskType)
    {
        recurringReference_HiddenField.Value = "";
        recurringType_HiddenField.Value = taskType;
        recurringCommand_HiddenField.Value = "add";

        _taskModRef = "";

        ResetForm();

        if(taskType == "APS")
        {
            taskModalHeader_Label.Text = ts.GetResource("uc_recurring_auditplaninfo");

            processPlan_Button.Text = ts.GetResource("uc_recurring_createplan");
            processPlan_Button_Delete.Text = ts.GetResource("uc_recurring_createplan");
            auditTemplate_Div.Visible = true;
            eventSubject_Div.Visible = false;
            InitiateAuditSpecifics();
            creation_Template_DropDownList.SelectedIndex = 0;
        }
        else
        {

            processPlan_Button.Text = ts.GetResource("uc_recurring_createtask");
            processPlan_Button_Delete.Text = ts.GetResource("uc_recurring_createtask");
            auditTemplate_Div.Visible = false;
            eventSubject_Div.Visible = true;
            creationStructure_UpdatePanel.Visible = false;
        }

        label_UpdatePanel.Update();
        modalButton_UpdatePanel.Update();
        planningTask_UpdatePanel.Update();

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script123123123", "RecurringTask_ShowDivs();$('#openPlanningTaskModal').modal('show');", true);

    }


    private void ResetForm()
    {
        recurringType_DropDownList.SelectedValue = "O";
        once_RadDatePicker.SelectedDate = null;
        Daily_XDays.Text = "0";
        Daily_Days.Checked = false;
        Daily_WeekDays.Checked = false;
        WeeklyXWeeks.Text = "0";
        Monthly_DayEveryMonth.Checked = false;
        Monthly_1_Period.Text = "0";
        Monthly_1_Months.Text = "0";
        MonthlyPositionDayMonth.Checked = false;
        Monthly_2_Interval.SelectedValue = null;
        Monthly_2_Day.SelectedValue = null;
        Monthly_2_Period.Text = "0";
        Annual_EveryMonthDay.Checked = false;
        Annually_1_Month.SelectedValue = null;
        Annually_1_Month_Day.Text = "0";
        Annual_XDayMonth.Checked = false;
        Annually_2_Interval.SelectedValue = null;
        Annually_2_Day.SelectedValue = null;
        Annually_2_Month.SelectedValue = null;
        Frequency_NoEndDate.Checked = false;
        Frequency_EndAfter.Checked = false;
        Frequency_Occurrence.Text = "0";
        Frequency_EndBy.Checked = false;
        Frequency_EndDate.SelectedDate = null;
        Frequency_EndDate.MinDate = DateTime.Today;

        TaskManager_DisplayBefore.Text = "15";
        TaskManager_StartAfter.SelectedDate = DateTime.Today;

        EventFor_Literal.Text = ts.GetResource("uc_recurring_eventfor");
        EventDetails_Literal.Text = ts.GetResource("uc_recurring_eventdetails");


        if(creation_Template_DropDownList.Items.Count > 0)
        {
            creation_Template_DropDownList.SelectedIndex = 0;
        }

        taskModalHeader_Label.Text = ts.GetResource("uc_recurring_taskinformation");
        label_UpdatePanel.Update();


        auditTemplate_Div.Visible = false;
        eventSubject_Div.Visible = true;

        structureTier2.Visible = false;
        structureTier3.Visible = false;
        structureTier4.Visible = false;
        structureTier5.Visible = false;

        InitiateBlankStructure();

        creation_Subject_TextBox.Text = "";

        creation_Comments_TextBox.Text = "";

        weekly_Monday.Checked = false;
        weekly_Tuesday.Checked = false;
        weekly_Wednesday.Checked = false;
        weekly_Thursday.Checked = false;
        weekly_Friday.Checked = false;
        weekly_Saturday.Checked = false;
        weekly_Sunday.Checked = false;

        weekly_Monday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Tuesday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Wednesday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Thursday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Friday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Saturday.InputAttributes["class"] = "checkbox checkbox-override-inline";
        weekly_Sunday.InputAttributes["class"] = "checkbox checkbox-override-inline";



        UserControls.returnUserDropDown(creation_Task_For, new RadComboBoxItemsRequestedEventArgs(), "", _corpCode);

        creation_Task_For.SelectedIndex = -1;
        creation_Task_For.Text = null;

        recurringModuleReference_HiddenField.Value = "";
        recurringModuleActionReference_HiddenField.Value = "";
    }

    private void CreateFromDB()
    {

        ResetForm();

        string taskDescription;
        string taskFurtherInformation;

        string recurringType;
        string onceDate;

        string recurDaily; // 1 is every x days, 2 is every weekday
        string recurDaily_opt1; // How many days

        string recurWeekly; //Only one option
        string recurWeekly_opt1_period; // How many weeks
        string recurWeekly_opt1_days; // Binary string for days, 7 long. 1000000 is Monday on, other days off.

        string recurMonthly; // 1 is day x of every x months. 2 is the xx xx of every xx months
        string recurMonthly_opt1_period; // day xx
        string recurMonthly_opt1_months; // every xx months
        string recurMonthly_opt2_week_interval; // First, Second, etc
        string recurMonthly_opt2_week_day; // Day, Monday Tuesday etc
        string recurMonthly_opt2_week_period; // every xx months

        string recurAnnually; //1 is every xx xx (every may 5th), 2 is the xx xx of xx (First Monday of January)
        string recurAnnually_opt1_month; //Month selection
        string recurAnnually_opt1_month_day; // Numerical value symbolising the day in month (0-31)
        string recurAnnually_opt2_interval; // First, second, etc
        string recurAnnually_opt2_day; // Monday, Tuesday etc
        string recurAnnually_opt2_month; // Jan, Feb, etc.

        string frequencyType; // 1 is no End Date, 2 is after x occurences, 3 is end by a date.
        string frequencyOccurrence;// Numeric value
        string frequencyEndDate; // date tasks should end by, this might need to be a date.

        string taskDisplayBefore; // when should the task be displayed in the calendar
        string taskStartAfter; // when the recurring task should kick in.
        

        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_Global.dbo.RecurrenceDisplay", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@rowID", _recurrenceID));
                cmd.Parameters.Add(new SqlParameter("@taskType", _recurrenceType));
                
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    _typeOfTask = Convert.ToString(objrs["Task_module"]);
                    _tier2Ref = Convert.ToString(objrs["task_company"]);
                    _tier3Ref = Convert.ToString(objrs["task_location"]);
                    _tier4Ref = Convert.ToString(objrs["task_department"]);
                    _tier5Ref = Convert.ToString(objrs["task_tier5"]);
                    _tier6Ref = Convert.ToString(objrs["task_tier6"]);

                    _taskModRef = Convert.ToString(objrs["task_mod_ref"]);

                    recurringType_HiddenField.Value = _typeOfTask;
                    recurringModuleReference_HiddenField.Value = _taskModRef;
                    recurringModuleActionReference_HiddenField.Value = Convert.ToString(objrs["task_mod_action_ref"]);


                    //Task Specific Settings
                    if (_typeOfTask == "APS")
                    {
                        taskModalHeader_Label.Text = ts.GetResource("uc_recurring_auditplaninfo");
                        //_taskModRef is the template reference
                        auditTemplate_Div.Visible = true;
                        eventSubject_Div.Visible = false;

                        creationStructure_UpdatePanel.Visible = true;
                        InitiateStructure();
                        InitiateAuditSpecifics();
                        processPlan_Button.Text = ts.GetResource("uc_recurring_updateplan");
                        processPlan_Button_Delete.Text = ts.GetResource("uc_recurring_updateplan");
                    }
                    else
                    {
                        eventSubject_Div.Visible = true;
                        taskFurtherInformation = Convert.ToString(objrs["task_furtherinfo"]);
                        creation_Subject_TextBox.Text = taskFurtherInformation;
                        creationStructure_UpdatePanel.Visible = false;
                        processPlan_Button.Text = ts.GetResource("uc_recurring_updateinformation");
                        processPlan_Button_Delete.Text = ts.GetResource("uc_recurring_updateinformation");
                    }

                    label_UpdatePanel.Update();


                    // This will only be relevant for Once type tasks
                    string valueForAssignedAccount = Convert.ToString(objrs["Task_account"]);

                    if(valueForAssignedAccount.Length == 0 || valueForAssignedAccount == "")
                    {
                        valueForAssignedAccount = Convert.ToString(objrs["Task_by"]);
                    }


                    InitiateTaskFor(valueForAssignedAccount, _typeOfTask);


                    //Comments / Details
                    taskDescription = Convert.ToString(objrs["task_description"]);

                    creation_Comments_TextBox.Text = taskDescription;

                    //Recurrence

                    recurringType = Convert.ToString(objrs["Recur_type"]);

                    switch (recurringType)
                    {
                        case "O":
                            onceDate = Convert.ToString(objrs["Recur_o_date"]);

                            recurringType_DropDownList.SelectedValue = "O";
                            once_RadDatePicker.SelectedDate = Convert.ToDateTime(onceDate);

                            break;

                        case "D":
                            recurDaily = Convert.ToString(objrs["recur_d"]);
                            recurDaily_opt1 = Convert.ToString(objrs["Recur_d_opt1_period"]);


                            //Trigger radio buttons and inputs
                            recurringType_DropDownList.SelectedValue = "D";

                            if (recurDaily == "1")
                            {
                                Daily_XDays.Text = recurDaily_opt1;
                                Daily_Days.Checked = true;
                            }
                            else
                            {
                                Daily_WeekDays.Checked = true;
                            }


                            break;

                        case "W":
                            recurWeekly = Convert.ToString(objrs["recur_w"]);
                            recurWeekly_opt1_period = Convert.ToString(objrs["recur_w_opt1_period"]);
                            recurWeekly_opt1_days = Convert.ToString(objrs["recur_w_opt1_days"]);


                            //Trigger radio buttons and inputs - STILL NEED TO DO DAYS!
                            recurringType_DropDownList.SelectedValue = "W";
                            WeeklyXWeeks.Text = recurWeekly_opt1_period;



                            // Days are a binary string, 7 long. each bit is relevant to a day, i.e. 1100000 is Monday Tuesday on, rest off.
                            int i = 1;
                            foreach (char dayBit in recurWeekly_opt1_days)
                            {

                                if (dayBit == '1')
                                {

                                    switch (i)
                                    {
                                        case 1:
                                            weekly_Monday.Checked = true;
                                            break;

                                        case 2:
                                            weekly_Tuesday.Checked = true;
                                            break;

                                        case 3:
                                            weekly_Wednesday.Checked = true;
                                            break;

                                        case 4:
                                            weekly_Thursday.Checked = true;
                                            break;

                                        case 5:
                                            weekly_Friday.Checked = true;
                                            break;

                                        case 6:
                                            weekly_Saturday.Checked = true;
                                            break;
                                        case 7:
                                            weekly_Sunday.Checked = true;
                                            break;

                                        default:
                                            break;
                                    }


                                }

                                i++;
                            }




                            break;

                        case "M":
                            recurMonthly = Convert.ToString(objrs["recur_m"]);
                            recurMonthly_opt1_period = Convert.ToString(objrs["recur_m_opt1_period"]);
                            recurMonthly_opt1_months = Convert.ToString(objrs["recur_m_opt1_period_months"]);
                            recurMonthly_opt2_week_interval = Convert.ToString(objrs["recur_m_opt2_week_interval"]);
                            recurMonthly_opt2_week_day = Convert.ToString(objrs["recur_m_opt2_week_day"]);
                            recurMonthly_opt2_week_period = Convert.ToString(objrs["recur_m_opt2_week_period"]);

                            //Trigger radio buttons and inputs
                            recurringType_DropDownList.SelectedValue = "M";

                            if (recurMonthly == "1")
                            {
                                Monthly_DayEveryMonth.Checked = true;
                                Monthly_1_Period.Text = recurMonthly_opt1_period;
                                Monthly_1_Months.Text = recurMonthly_opt1_months;
                            }
                            else if (recurMonthly == "2")
                            {
                                MonthlyPositionDayMonth.Checked = true;
                                Monthly_2_Interval.SelectedValue = recurMonthly_opt2_week_interval;
                                Monthly_2_Day.SelectedValue = recurMonthly_opt2_week_day;
                                Monthly_2_Period.Text = recurMonthly_opt2_week_period;
                            }



                            break;

                        case "A":
                            recurAnnually = Convert.ToString(objrs["recur_a"]);
                            recurAnnually_opt1_month = Convert.ToString(objrs["recur_a_opt1_month"]);
                            recurAnnually_opt1_month_day = Convert.ToString(objrs["recur_a_opt1_month_day"]);
                            recurAnnually_opt2_interval = Convert.ToString(objrs["recur_a_opt2_interval"]);
                            recurAnnually_opt2_day = Convert.ToString(objrs["recur_a_opt2_day"]);
                            recurAnnually_opt2_month = Convert.ToString(objrs["recur_a_opt2_month"]);


                            //Trigger radio buttons and inputs
                            recurringType_DropDownList.SelectedValue = "A";

                            if (recurAnnually == "1")
                            {
                                Annual_EveryMonthDay.Checked = true;
                                Annually_1_Month.SelectedValue = recurAnnually_opt1_month;
                                Annually_1_Month_Day.Text = recurAnnually_opt1_month_day;
                            }
                            else if (recurAnnually == "2")
                            {
                                Annual_XDayMonth.Checked = true;
                                Annually_2_Interval.SelectedValue = recurAnnually_opt2_interval;
                                Annually_2_Day.SelectedValue = recurAnnually_opt2_day;
                                Annually_2_Month.SelectedValue = recurAnnually_opt2_month;
                            }


                            break;

                        default:
                            break;
                    }

                    //Only show frequency for recurring type tasks
                    if (_isRecurringTask)
                    {
                        frequencyType = Convert.ToString(objrs["freq_type"]);

                        switch (frequencyType)
                        {
                            case "1":
                                Frequency_NoEndDate.Checked = true;
                                break;
                            case "2":
                                frequencyOccurrence = Convert.ToString(objrs["freq_occurences"]);

                                Frequency_EndAfter.Checked = true;
                                Frequency_Occurrence.Text = frequencyOccurrence;
                                break;
                            case "3":
                                frequencyEndDate = Convert.ToString(objrs["freq_enddate"]);

                                Frequency_EndBy.Checked = true;

                                if (Convert.ToDateTime(frequencyEndDate) < DateTime.Today)
                                {
                                    Frequency_EndDate.MinDate = Convert.ToDateTime(frequencyEndDate);
                                }

                                Frequency_EndDate.SelectedDate = Convert.ToDateTime(frequencyEndDate);
                                break;
                            default:
                                break;
                        }

                        taskDisplayBefore = Convert.ToString(objrs["task_display_before"]);
                        taskStartAfter = Convert.ToString(objrs["Task_start_after"]);

                        TaskManager_DisplayBefore.Text = taskDisplayBefore;
                        TaskManager_StartAfter.SelectedDate = Convert.ToDateTime(taskStartAfter);
                    }




                }

                objrs.Close();
            }
        }
        catch
        {
            throw;
        }

    }

    

    private void InitiateStructure()
    {

        if(_tier2Ref != "0" && _tier2Ref != null)
        {
            structureTier2.Visible = true;
            structureTier1.SelectedValue = _tier2Ref;
            getTier2StructureInformation_Create();
        }

        if (_tier3Ref != "0" && _tier3Ref != null)
        {
            structureTier3.Visible = true;
            structureTier2.SelectedValue = _tier3Ref;
            getTier3StructureInformation_Create();
        }

        if (_tier4Ref != "0" && _tier4Ref != null)
        {
            structureTier4.Visible = true;
            structureTier3.SelectedValue = _tier4Ref;
            getTier4StructureInformation_Create();
        }

        if (_tier5Ref != "0" && _tier5Ref != null)
        {
            structureTier5.Visible = true;
            structureTier4.SelectedValue = _tier5Ref;
            getTier5StructureInformation_Create();
        }

        if (_tier6Ref != "0" && _tier6Ref != null)
        {
            structureTier5.SelectedValue = _tier6Ref;
        }

    }

    private void InitiateTaskFor(string dbValueForAccount, string typeOfTask)
    {



        if (_isRecurringTask && typeOfTask != "PER")
        {
            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.get_Recurring_Task_dist_list", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recurr_id", _recurrenceID));

                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        string taskAccount = Convert.ToString(objrs["task_account"]);

                        UserControls.returnUserDropDown_Initilise(creation_Task_For, taskAccount, "", _corpCode);


                    }

                    objrs.Close();
                }
            }
            catch
            {
                throw;
            }
        }
        else
        {
            UserControls.returnUserDropDown_Initilise(creation_Task_For, dbValueForAccount, "", _corpCode);        
        }

        
    }

    private void InitiateAuditSpecifics()
    {

        //Template
        // If a user has access to audit templates, run one set of code.
        // If they don't run another. Such fun.

        bool HasTemplateAccess = Permissions.moduleAccessAllowed(_corpCode, _currentUser, "SA_T", "permission");

        

        if (HasTemplateAccess)
        {
            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.sp_return_templates_for_user_highlevel", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));

                    objrs = cmd.ExecuteReader();

                    creation_Template_DropDownList.DataSource = objrs;
                    creation_Template_DropDownList.DataTextField = "temp_title";
                    creation_Template_DropDownList.DataValueField = "template_id";
                    creation_Template_DropDownList.DataBind();


                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script123123145dfgdfgdfg23", "console.log('" + HasTemplateAccess + "')", true);

                    objrs.Close();
                }
            }
            catch
            {
                throw;
            }
        }
        else
        {
            try
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_SA.dbo.sp_return_templates_for_user", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", _currentUser));
                    cmd.Parameters.Add(new SqlParameter("@access", _yourAccessLevel));
                    cmd.Parameters.Add(new SqlParameter("@perms_active", "Y"));
                    cmd.Parameters.Add(new SqlParameter("@filter", "C"));

                    objrs = cmd.ExecuteReader();

                    creation_Template_DropDownList.DataSource = objrs;
                    creation_Template_DropDownList.DataTextField = "temp_title";
                    creation_Template_DropDownList.DataValueField = "template_id";
                    creation_Template_DropDownList.DataBind();

                   

                    objrs.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        creation_Template_DropDownList.Items.Insert(0, new ListItem(ts.GetResource("uc_recurring_pleaseselect"), "0"));


        //If the selected value does not exist, lets find the template and put it in. This is a failsafe.



        if (_taskModRef.Length > 0 && _taskModRef != "0")
        {
            if(creation_Template_DropDownList.Items.FindByValue(_taskModRef) == null)
            {
                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {
                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_SA.dbo.sp_return_templates_for_user_Missing", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@template_id", _taskModRef));

                        objrs = cmd.ExecuteReader();

                        while (objrs.Read())
                        {
                            string temp_title = Convert.ToString(objrs["temp_title"]) + " (INACTIVE)";
                            string template_id = Convert.ToString(objrs["template_id"]);

                            creation_Template_DropDownList.Items.Add(new ListItem(temp_title, template_id));
                        }

                        objrs.Close();
                    }

                    catch
                    {
                        throw;
                    }
                }
            }

            creation_Template_DropDownList.SelectedValue = _taskModRef;

            
        }





        // Set Labels
        EventFor_Literal.Text = ts.GetResource("uc_recurring_auditor");
        EventDetails_Literal.Text = ts.GetResource("uc_recurring_planningcomments");

    }

    private void InitiateBlankStructure()
    {
        structureTier2.Visible = false;
        structureTier3.Visible = false;
        structureTier4.Visible = false;
        structureTier5.Visible = false;
        structureTier1_populate_Create(0);
    }


    // Update and Create Functions
    private void ProcessRecurrence()
    {
        // Set up variables to be processed.

        string taskDescription = creation_Comments_TextBox.Text;
        string selectedUser = creation_Task_For.SelectedValue;
        string taskFurtherInformation = creation_Subject_TextBox.Text;

        string recurringType = recurringType_DropDownList.SelectedValue;
        DateTime onceDate = DateTime.MaxValue;


        string recurDaily = "";
        string recurDaily_opt1 = "";

        string recurWeekly = "";
        string recurWeekly_opt1_period = "";
        string recurWeekly_opt1_days = "";

        string recurMonthly = ""; 
        string recurMonthly_opt1_period = "";
        string recurMonthly_opt1_months = "";
        string recurMonthly_opt2_week_interval = ""; 
        string recurMonthly_opt2_week_day = "";
        string recurMonthly_opt2_week_period = "";

        string recurAnnually = ""; 
        string recurAnnually_opt1_month = "";
        string recurAnnually_opt1_month_day = "";
        string recurAnnually_opt2_interval = "";
        string recurAnnually_opt2_day = "";
        string recurAnnually_opt2_month = "";

        string frequencyType = ""; 
        string frequencyOccurrence = "";
        DateTime frequencyEndDate = DateTime.MaxValue;

        string taskDisplayBefore = "";
        DateTime taskStartAfter = DateTime.MaxValue;


        string tier2;
        string tier3;
        string tier4;
        string tier5;
        string tier6;

        string recurringTaskType = recurringType_HiddenField.Value;
        string recurringCommand = recurringCommand_HiddenField.Value;
        string recurringModuleReference = recurringModuleReference_HiddenField.Value;
        string recurringModuleActionReference = recurringModuleActionReference_HiddenField.Value;


        if (recurringTaskType == "APS")
        {
            recurringModuleReference = creation_Template_DropDownList.SelectedValue;
            
        }

    

        string combinedReference = recurringReference_HiddenField.Value;
        int reccurenceIDNumeric;
        if (combinedReference.Length > 0)
        {
            _recurrenceID = combinedReference.Split('_')[1];
            _recurrenceType = combinedReference.Split('_')[0];

            reccurenceIDNumeric = Convert.ToInt32(_recurrenceID);
        }
        else
        {
            _recurrenceID = null;
            _recurrenceType = "";
            reccurenceIDNumeric = 0;
        }

        

        //Assign Variables

        taskDescription = creation_Comments_TextBox.Text;
        tier2 = structureTier1.SelectedValue;
        tier3 = structureTier2.SelectedValue;
        tier4 = structureTier3.SelectedValue;
        tier5 = structureTier4.SelectedValue;
        tier6 = structureTier5.SelectedValue;

        if (structureTier2.Visible == false)
        {
            tier3 = "0";
            tier4 = "0";
            tier5 = "0";
            tier6 = "0";
        }
        else if (structureTier3.Visible == false)
        {
            tier4 = "0";
            tier5 = "0";
            tier6 = "0";
        }
        else if (structureTier4.Visible == false)
        {
            tier5 = "0";
            tier6 = "0";
        }
        else if (structureTier5.Visible == false)
        {
            tier6 = "0";
        }

        recurringType = recurringType_DropDownList.SelectedValue;


        //Once - TODO - DOESNT GO IN RECURRING TABLE!
        if(recurringType == "O")
        {
            onceDate = (DateTime)once_RadDatePicker.SelectedDate;
        }
        

        
        if(recurringType == "D")
        {
            //Daily 
            if (Daily_Days.Checked)
            {
                recurDaily = "1";
                recurDaily_opt1 = Daily_XDays.Text;
            }
            else
            {
                recurDaily = "2";
                recurDaily_opt1 = "";
            }
            
            
        }
        else if (recurringType == "W")
        {
            //Weekly
            recurWeekly = "1";
            recurWeekly_opt1_period = WeeklyXWeeks.Text;


            recurWeekly_opt1_days = weekly_Monday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Tuesday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Wednesday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Thursday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Friday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Saturday.Checked == true ? "1" : "0";
            recurWeekly_opt1_days += weekly_Sunday.Checked == true ? "1" : "0";


        }
        else if (recurringType == "M")
        {
            //Monthly

            if (Monthly_DayEveryMonth.Checked)
            {
                recurMonthly = "1";
                recurMonthly_opt1_period = Monthly_1_Period.Text;
                recurMonthly_opt1_months = Monthly_1_Months.Text;


            }
            else if (MonthlyPositionDayMonth.Checked)
            {
                recurMonthly = "2";
                recurMonthly_opt2_week_interval = Monthly_2_Interval.Text;
                recurMonthly_opt2_week_day = Monthly_2_Day.Text;
                recurMonthly_opt2_week_period = Monthly_2_Period.Text;


            }
           

        }
        else if(recurringType == "A")
        {
            //Annually

            if (Annual_EveryMonthDay.Checked)
            {
                recurAnnually = "1";
                recurAnnually_opt1_month = Annually_1_Month.SelectedValue;
                recurAnnually_opt1_month_day = Annually_1_Month_Day.Text;

            }
            else if (Annual_XDayMonth.Checked)
            {
                recurAnnually = "2";
                recurAnnually_opt2_interval = Annually_2_Interval.SelectedValue;
                recurAnnually_opt2_day = Annually_2_Day.SelectedValue;
                recurAnnually_opt2_month = Annually_2_Month.SelectedValue;
            }
        }


        if(recurringType != "O")
        {
            if (Frequency_NoEndDate.Checked)
            {
                frequencyType = "1";
            }
            else if (Frequency_EndAfter.Checked)
            {
                frequencyType = "2";
                frequencyOccurrence = Frequency_Occurrence.Text;
            }
            else if (Frequency_EndBy.Checked)
            {
                frequencyType = "3";
                frequencyEndDate = (DateTime)Frequency_EndDate.SelectedDate;
            }

            taskStartAfter = (DateTime)TaskManager_StartAfter.SelectedDate;

        }

        taskDisplayBefore = TaskManager_DisplayBefore.Text;



        //testText.Text = frequencyEndDate.ToString();
        //testText_UP.Update();


        try
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_GLOBAL.dbo.RecurrenceProcess", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@created_by", _currentUser));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", selectedUser));
                cmd.Parameters.Add(new SqlParameter("@var_task_call_type", recurringCommand));
                cmd.Parameters.Add(new SqlParameter("@var_task_module", recurringTaskType)); 
                cmd.Parameters.Add(new SqlParameter("@var_task_module_ref", recurringModuleReference));
                cmd.Parameters.Add(new SqlParameter("@var_task_module_action_ref", recurringModuleActionReference));
                cmd.Parameters.Add(new SqlParameter("@var_recur_company", tier2));
                cmd.Parameters.Add(new SqlParameter("@var_recur_location", tier3));
                cmd.Parameters.Add(new SqlParameter("@var_recur_dept", tier4));
                cmd.Parameters.Add(new SqlParameter("@var_recur_description", taskDescription));
                cmd.Parameters.Add(new SqlParameter("@var_recur_additional_info", taskFurtherInformation));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_main", recurringType));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_once_date", onceDate));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_once_show_before", "")); // To do
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_day", recurDaily));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_daily", recurDaily_opt1));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_weekly", recurWeekly_opt1_period));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_days", recurWeekly_opt1_days));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_monthly", recurMonthly));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_monthly_day", recurMonthly_opt1_period));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_monthly_month_numeric", recurMonthly_opt1_months));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_month_interval", recurMonthly_opt2_week_interval));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_month_interval_weekday", recurMonthly_opt2_week_day));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_monthly_month", recurMonthly_opt2_week_period));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_year", recurAnnually));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_yearly_month", recurAnnually_opt1_month));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_yearly_day", recurAnnually_opt1_month_day));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_yearly_interval", recurAnnually_opt2_interval));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_yearly_interval_weekday", recurAnnually_opt2_day));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_yearly_interval_month", recurAnnually_opt2_month));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq", frequencyType));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_limit", frequencyOccurrence));
                cmd.Parameters.Add(new SqlParameter("@var_recur_freq_end", frequencyEndDate));
                cmd.Parameters.Add(new SqlParameter("@var_recur_show_before", taskDisplayBefore));
                cmd.Parameters.Add(new SqlParameter("@var_recur_start_after", taskStartAfter));
                cmd.Parameters.Add(new SqlParameter("@var_recur_frm_dist_list", selectedUser));
                cmd.Parameters.Add(new SqlParameter("@var_task_tier5", tier5));
                cmd.Parameters.Add(new SqlParameter("@var_task_tier6", tier6));
                
                if(reccurenceIDNumeric > 0)
                {
                    cmd.Parameters.Add(new SqlParameter("@var_recurring_id", _recurrenceID));
                }
                else
                {
                    cmd.Parameters.Add(new SqlParameter("@var_recurring_id", DBNull.Value));
                }
                
                
                //testText.Text = SqlCommandDumper.GetCommandText(cmd);
                
                //testText.Text = recurWeekly_opt1_days;
                //testText_UP.Update();
                objrs = cmd.ExecuteReader();
                
                objrs.Close();
            }
        }
        catch
        {
            throw;
        }
        UpdateSearchResults_EventHandler(this, EventArgs.Empty);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "hideModal", "$('#openPlanningTaskModal').modal('hide');", true);
    }




    // Structure Elements 

    protected void structureTier1_populate_Create(Int32 refID)
    {

        structureTier1.Items.Clear();



        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(_corpCode, _currentUser, "", _tier2Ref, "CREATE", _yourAccessLevel, "", "2", ""))
            {
                structureTier1.DataSource = returnedStructure;
                structureTier1.DataValueField = "tierRef";
                structureTier1.DataTextField = "struct_name";
                structureTier1.DataBind();

                if (structureTier1.Items.Count == 0)
                {
                    structureTier1.Visible = false;
                    structureTier1.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    protected void structureTier1_SelectedIndexChanged_Create(object sender, EventArgs e)
    {

        structureTier2.Visible = true;
        structureTier3.Visible = false;
        structureTier4.Visible = false;
        structureTier5.Visible = false;
        structureTier2.Items.Clear();
        structureTier3.Items.Clear();
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier2StructureInformation_Create();

    }


    protected void structureTier2_SelectedIndexChanged_Create(object sender, EventArgs e)
    {

        structureTier3.Visible = true;
        structureTier4.Visible = false;
        structureTier5.Visible = false;
        structureTier3.Items.Clear();
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier3StructureInformation_Create();

    }


    protected void structureTier3_SelectedIndexChanged_Create(object sender, EventArgs e)
    {

        structureTier4.Visible = true;
        structureTier5.Visible = false;
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier4StructureInformation_Create();

    }


    protected void structureTier4_SelectedIndexChanged_Create(object sender, EventArgs e)
    {

        structureTier5.Visible = true;
        structureTier5.Items.Clear();

        getTier5StructureInformation_Create();

    }

    protected void getTier2StructureInformation_Create()
    {
        try
        {



            string currentSelection = "0";

            if (structureTier1.SelectedValue != "")
            {
                currentSelection = structureTier1.SelectedValue;
            }

            //structureDebug.Text = tier3Ref;

            using (DataTable returnedStructure = Structure.ReturnStructure(_corpCode, _currentUser, currentSelection, _tier3Ref, "CREATE", _yourAccessLevel, "", "3", ""))
            {
                structureTier2.DataSource = returnedStructure;
                structureTier2.DataValueField = "tierRef";
                structureTier2.DataTextField = "struct_name";
                structureTier2.DataBind();

                if (structureTier2.Items.Count == 0)
                {
                    structureTier2.Visible = false;
                    structureTier2.Items.Clear();
                }
            }


        }

        catch (Exception ex)
        {
            Response.Write(ex);
        }

        creationStructure_UpdatePanel.Update();
    }

    protected void getTier3StructureInformation_Create()
    {

        try
        {
            string currentSelection = "0";

            if (structureTier2.SelectedValue != "")
            {
                currentSelection = structureTier2.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(_corpCode, _currentUser, currentSelection, _tier4Ref, "CREATE", _yourAccessLevel, "", "4", ""))
            {
                structureTier3.DataSource = returnedStructure;
                structureTier3.DataValueField = "tierRef";
                structureTier3.DataTextField = "struct_name";
                structureTier3.DataBind();

                if (structureTier3.Items.Count == 0)
                {
                    structureTier3.Visible = false;
                    structureTier3.Items.Clear();
                }
            }



        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

        creationStructure_UpdatePanel.Update();

    }

    protected void getTier4StructureInformation_Create()
    {


        string currentSelection = "0";

        if (structureTier3.SelectedValue != "")
        {
            currentSelection = structureTier3.SelectedValue;
        }


        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(_corpCode, _currentUser, currentSelection, _tier5Ref, "CREATE", _yourAccessLevel, "", "5", ""))
            {
                structureTier4.DataSource = returnedStructure;
                structureTier4.DataValueField = "tierRef";
                structureTier4.DataTextField = "struct_name";
                structureTier4.DataBind();

                if (structureTier4.Items.Count == 0)
                {
                    structureTier4.Visible = false;
                    structureTier4.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

        creationStructure_UpdatePanel.Update();

    }

    protected void getTier5StructureInformation_Create()
    {

        try
        {

            string currentSelection = "0";

            if (structureTier4.SelectedValue != "")
            {
                currentSelection = structureTier4.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(_corpCode, _currentUser, currentSelection, _tier6Ref, "CREATE", _yourAccessLevel, "", "6", ""))
            {
                structureTier5.DataSource = returnedStructure;
                structureTier5.DataValueField = "tierRef";
                structureTier5.DataTextField = "struct_name";
                structureTier5.DataBind();

                if (structureTier5.Items.Count == 0)
                {
                    structureTier5.Visible = false;
                    structureTier5.Items.Clear();
                }
            }


        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

        creationStructure_UpdatePanel.Update();
                
    }


    // Structure Elements End

        

    //User dropdown initilise.
    protected void generic_Any_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "any", _corpCode);
    }

    protected void generic_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "none", _corpCode);
    }


    protected void generic_UserDropDown_Validation(object source, ServerValidateEventArgs args)
    {
        CustomValidator currentValidator = (CustomValidator)source;
        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);
        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, false);
    }

    protected void generic_UserDropDown_Blank_Validation(object source, ServerValidateEventArgs args)
    {
        CustomValidator currentValidator = (CustomValidator)source;
        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);
        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, true);
    }

    protected void processPlan_Button_Click(object sender, EventArgs e)
    {
        ProcessRecurrence();
    }
}