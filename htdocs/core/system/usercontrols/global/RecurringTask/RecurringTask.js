﻿function RecurringTask_ShowDivs() {

    // Recurrence Drop Down
    var recurrenceSelection = $('#recurringType_DropDownList').val();

    //console.log(recurrenceSelection)

    $('#onceCriteria').hide();
    $('#dailyCriteria').hide();
    $('#weeklyCriteria').hide();
    $('#monthlyCriteria').hide();
    $('#annualCriteria').hide();

    $('#frequency').hide();
    $('#taskmanager').hide();
    $('#taskmanager_AssignTasksFrom').show();

    $('#taskManagerSettings').show();
    $('#nextSectionToTaskManager').show();

    $('#processPlan_Button').hide();
    $('#processPlan_Button_Disabled').show();

    switch (recurrenceSelection) {
        case "O":
            //Once
            $('#onceCriteria').show();

            $('#taskManagerSettings').hide();

            $('#taskmanager_AssignTasksFrom').hide();
            $('#taskmanager').show();
            $('#nextSectionToTaskManager').hide();


           

            if (!$('#recurringType_DropDownList').is(":hidden")) {
                $('#processPlan_Button').show();
                $('#processPlan_Button_Disabled').hide();
            }


            break;
        case "D":
            //Daily
            $('#dailyCriteria').show();

            $('#frequency').show();
            $('#taskmanager').show();
            break;
        case "W":
            //Weekly
            $('#weeklyCriteria').show();

            $('#frequency').show();
            $('#taskmanager').show();
            break;
        case "M":
            $('#monthlyCriteria').show();

            $('#frequency').show();
            $('#taskmanager').show();
            break;
        case "A":
            $('#annualCriteria').show();

            $('#frequency').show();
            $('#taskmanager').show();
            break;
        default:

            break;
    }


}



function SaveGeneralInformation() {

    var continueToNextSection = ValidateGeneralInformation();

    if (continueToNextSection === true) {
        $("#collapseFirstSection").collapse('hide');
        $('#collapseSecondSection').collapse('show');
        $('#ModalBody').scrollTop(0);



        if ($('#recurringType_DropDownList').val() === "O") {
            $('#processPlan_Button').show();
            $('#processPlan_Button_Disabled').hide();
        }
       

    }
    else {
        $('#processPlan_Button').hide();
        $('#processPlan_Button_Disabled').show();
    }

}

function SaveRecurringInformation() {

    var continueToNextSection = ValidateRecurringInformation();

    if (continueToNextSection === true) {
        $("#collapseSecondSection").collapse('hide');
        $('#collapseThirdSection').collapse('show');
        $('#ModalBody').scrollTop(0);
        $('#processPlan_Button').show();
        $('#processPlan_Button_Disabled').hide();
    }
    else {
        $('#processPlan_Button').hide();
        $('#processPlan_Button_Disabled').show();
    }


}

function PreviousSection(position) {
    if (position === "1") {
        $("#collapseFirstSection").collapse('show');
        $('#collapseSecondSection').collapse('hide');
    }
    else if (position === "2") {
        $("#collapseSecondSection").collapse('show');
        $('#collapseThirdSection').collapse('hide');
    }
    $('#processPlan_Button').hide();
    $('#processPlan_Button_Disabled').show();
    $('#ModalBody').scrollTop(0);
}

function ValidateGeneralInformation() {

    var isValid = true;

    // If audit template is there, see if its been selected.
    if ($('#RecurringTask_creation_Template_DropDownList').length) {

        var selectedAuditTemplate = $('#RecurringTask_creation_Template_DropDownList').val();

        if (selectedAuditTemplate === "0") {
            isValid = false;
            $('#RecurringTask_creation_Template_DropDownList').addClass("form-control-invalid");
        }
        else {
            $('#RecurringTask_creation_Template_DropDownList').removeClass("form-control-invalid");
        }

    }

    // Person task is assigned to
    if ($('#RecurringTask_creation_Task_For_Input').length) {

        var taskFor = $('#RecurringTask_creation_Task_For_Input').val();

        if (taskFor === "Please type a name or select a user") {
            isValid = false;
            $('#RecurringTask_creation_Task_For').addClass("form-control-invalid");
        }
        else {
            $('#RecurringTask_creation_Task_For').removeClass("form-control-invalid");
        }

    }

    // Structure
    if ($('#RecurringTask_structureTier1').length) {
        var tier2 = $('#RecurringTask_structureTier1').val();
        if (tier2 === "0") {
            isValid = false;
            $('#RecurringTask_structureTier1').addClass("form-control-invalid");
        }
        else {
            $('#RecurringTask_structureTier1').removeClass("form-control-invalid");
        }

        if ($('#RecurringTask_structureTier2').length) {
            var tier3 = $('#RecurringTask_structureTier2').val();
            if (tier3 === "0") {
                isValid = false;
                $('#RecurringTask_structureTier2').addClass("form-control-invalid");
            }
            else {
                $('#RecurringTask_structureTier2').removeClass("form-control-invalid");
            }

            if ($('#RecurringTask_structureTier3').length) {
                var tier4 = $('#RecurringTask_structureTier3').val();
                if (tier4 === "0") {
                    isValid = false;
                    $('#RecurringTask_structureTier3').addClass("form-control-invalid");
                }
                else {
                    $('#RecurringTask_structureTier3').removeClass("form-control-invalid");
                }


                if ($('#RecurringTask_structureTier4').length) {
                    var tier5 = $('#RecurringTask_structureTier4').val();
                    if (tier5 === "0") {
                        isValid = false;
                        $('#RecurringTask_structureTier4').addClass("form-control-invalid");
                    }
                    else {
                        $('#RecurringTask_structureTier4').removeClass("form-control-invalid");
                    }

                    if ($('#RecurringTask_structureTier5').length) {
                        var tier6 = $('#RecurringTask_structureTier5').val();
                        if (tier6 === "0") {
                            isValid = false;
                            $('#RecurringTask_structureTier5').addClass("form-control-invalid");
                        }
                        else {
                            $('#RecurringTask_structureTier5').removeClass("form-control-invalid");
                        }
                    }
                }
            }
        }
    }


    // Comments
    if ($('#RecurringTask_creation_Comments_TextBox').length) {
        var comment = $('#RecurringTask_creation_Comments_TextBox').val();

        if (comment === "") {
            isValid = false;
            $('#RecurringTask_creation_Comments_TextBox').addClass("form-control-invalid");
        }
        else {
            $('#RecurringTask_creation_Comments_TextBox').removeClass("form-control-invalid");
        }
    }

    // Comments
    if ($('#RecurringTask_creation_Subject_TextBox').length) {

        

        var furtherInfo = $('#RecurringTask_creation_Subject_TextBox').val();


        if (furtherInfo === "") {
            isValid = false;
            $('#RecurringTask_creation_Subject_TextBox').addClass("form-control-invalid");

            console.log(furtherInfo);
        }
        else {
            $('#RecurringTask_creation_Subject_TextBox').removeClass("form-control-invalid");
        }
    }

    if (isValid === false) {
        ShowValidationModal();
    }

    return isValid;

}

function ValidateRecurringInformation() {

    var isValid = true;

    var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}$/;

    // Recurring Task Part
    var selectedRecurrenceType = $('#recurringType_DropDownList').val();

    // For recurrence, we have the following options:
    // - Once, Daily, Weekly, Monthly, Annually

    if (selectedRecurrenceType === "O") {
        // Once
        $('#RecurringTask_once_RadDatePicker_wrapper').removeClass("form-control-invalid");
        var once_SelectedDate = $('#RecurringTask_once_RadDatePicker_dateInput').val();

        if (!once_SelectedDate.match(dateReg)) {
            isValid = false;
            $('#RecurringTask_once_RadDatePicker_wrapper').addClass("form-control-invalid");
        }


    }
    else if (selectedRecurrenceType === "D") {

        $('#frequencyDailyOptions').removeClass("form-control-invalid");
        $('#RecurringTask_Daily_XDays_wrapper').removeClass("form-control-invalid");


        // First check to see if an item has been selected
        if (!$("input[name='RecurringTask$DailyRecurrence']:checked").val()) {
            isValid = false;
            $('#frequencyDailyOptions').addClass("form-control-invalid");
        }
        else {

            // Something has been selected, lets check to see if anything else needs validating.
            // Only every x days needs validating here
            if ($('#RecurringTask_Daily_Days').is(':checked')) {

                var daily_XDays = $("#RecurringTask_Daily_XDays").val();
                if (daily_XDays === "0") {
                    isValid = false;
                    $('#RecurringTask_Daily_XDays_wrapper').addClass("form-control-invalid");
                }

            }

        }


    }
    else if (selectedRecurrenceType === "W") {

        $('#frequencyWeeklyOptions').removeClass("form-control-invalid");
        $('#RecurringTask_WeeklyXWeeks_wrapper').removeClass("form-control-invalid");

        // Weekly - uses tick boxes.
        var weekly_Mon = $("#RecurringTask_weekly_Monday").is(':checked');
        var weekly_Tue = $("#RecurringTask_weekly_Tuesday").is(':checked');
        var weekly_Wed = $("#RecurringTask_weekly_Wednesday").is(':checked');
        var weekly_Thu = $("#RecurringTask_weekly_Thursday").is(':checked');
        var weekly_Fri = $("#RecurringTask_weekly_Friday").is(':checked');
        var weekly_Sat = $("#RecurringTask_weekly_Saturday").is(':checked');
        var weekly_Sun = $("#RecurringTask_weekly_Sunday").is(':checked');

        if (!weekly_Mon &&
            !weekly_Tue &&
            !weekly_Wed &&
            !weekly_Thu &&
            !weekly_Fri &&
            !weekly_Sat &&
            !weekly_Sun) {
            isValid = false;
            $('#frequencyWeeklyOptions').addClass("form-control-invalid");
        }


        //Every X Weeks
        var weekly_XWeeks = $("#RecurringTask_WeeklyXWeeks").val();
        if (weekly_XWeeks === "0") {
            isValid = false;
            $('#RecurringTask_WeeklyXWeeks_wrapper').addClass("form-control-invalid");
        }



    }
    else if (selectedRecurrenceType === "M") {

        $('#frequencyMonthlyOptions').removeClass("form-control-invalid");
        $('#RecurringTask_Monthly_1_Period_wrapper').removeClass("form-control-invalid");
        $('#RecurringTask_Monthly_1_Months_wrapper').removeClass("form-control-invalid");
        $('#RecurringTask_Monthly_2_Period_wrapper').removeClass("form-control-invalid");

        //First see if something has been selected
        if (!$("input[name='RecurringTask$MonthlyRecurrence']:checked").val()) {
            isValid = false;
            $('#frequencyMonthlyOptions').addClass("form-control-invalid");
        }
        else {

            if ($('#RecurringTask_Monthly_DayEveryMonth').is(':checked')) {



                //Flush
                $('#RecurringTask_Monthly_2_Period_wrapper').removeClass("form-control-invalid");

                var monthly_XDays = $("#RecurringTask_Monthly_1_Period").val();
                var monthly_xMonths_1 = $("#RecurringTask_Monthly_1_Months").val();

                if (monthly_XDays === "0") {
                    isValid = false;
                    $('#RecurringTask_Monthly_1_Period_wrapper').addClass("form-control-invalid");
                }


                if (monthly_xMonths_1 === "0") {
                    isValid = false;
                    $('#RecurringTask_Monthly_1_Months_wrapper').addClass("form-control-invalid");
                }


            }
            else if ($('#RecurringTask_MonthlyPositionDayMonth').is(':checked')) {

                var monthly_XMonths_2 = $("#RecurringTask_Monthly_2_Period").val();
                if (monthly_XMonths_2 === "0") {
                    isValid = false;
                    $('#RecurringTask_Monthly_2_Period_wrapper').addClass("form-control-invalid");
                }


            }

        }

    }
    else if (selectedRecurrenceType === "A") {

        $('#frequencyAnnualOptions').removeClass("form-control-invalid");
        $('#RecurringTask_Annually_1_Month_Day_wrapper').removeClass("form-control-invalid");

        if (!$("input[name='RecurringTask$AnnualRecurrence']:checked").val()) {
            isValid = false;
            $('#frequencyAnnualOptions').addClass("form-control-invalid");
        }
        else {


            if ($('#RecurringTask_Annual_EveryMonthDay').is(':checked')) {

                var annual_MonthDay = $("#RecurringTask_Annually_1_Month_Day").val();

                if (annual_MonthDay === "0") {
                    isValid = false;
                    $('#RecurringTask_Annually_1_Month_Day_wrapper').addClass("form-control-invalid");
                }

            }


        }
    }


    if (selectedRecurrenceType !== "O") {

        $('#RecurringTask_Frequency_Occurrence_wrapper').removeClass("form-control-invalid");
        $('#endCriteria').removeClass("form-control-invalid");
        $('#RecurringTask_Frequency_EndDate_wrapper').removeClass("form-control-invalid");
        $("#RecurringTask_TaskManager_DisplayBefore_wrapper").removeClass("form-control-invalid");
        $("#RecurringTask_TaskManager_StartAfter_wrapper").removeClass("form-control-invalid");


        if (!$("input[name='RecurringTask$Frequency']:checked").val()) {
            isValid = false;
            $('#endCriteria').addClass("form-control-invalid");
        }
        else {

            if ($('#RecurringTask_Frequency_EndAfter').is(':checked')) {

                var endAfterXDays = $("#RecurringTask_Frequency_Occurrence").val();

                if (endAfterXDays === "0") {
                    $('#RecurringTask_Frequency_Occurrence_wrapper').addClass("form-control-invalid");
                    isValid = false;
                }

            }
            else if ($('#RecurringTask_Frequency_EndBy').is(':checked')) {
               

                var endByDate = $("#RecurringTask_Frequency_EndDate_dateInput").val();

                if (!endByDate.match(dateReg)) {
                    $('#RecurringTask_Frequency_EndDate_wrapper').addClass("form-control-invalid");
                    isValid = false;
                }

            }

        }


    }



    if (isValid === false) {
        ShowValidationModal();
    }

    return isValid;

}

function ValidateTaskInformation() {

    var isValid = true;
    var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
    var selectedRecurrenceType = $('#recurringType_DropDownList').val();

    //When should the task end?
    if (selectedRecurrenceType !== "O") {
        $("#RecurringTask_TaskManager_DisplayBefore_wrapper").removeClass("form-control-invalid");
        $("#RecurringTask_TaskManager_StartAfter_wrapper").removeClass("form-control-invalid");


        // Task Manager

        var addToTaskManagerXDays = $("#RecurringTask_TaskManager_DisplayBefore").val();
        var assignTasksFromDate = $("#RecurringTask_TaskManager_StartAfter_dateInput").val();

        if (addToTaskManagerXDays === "0") {
            $("#RecurringTask_TaskManager_DisplayBefore_wrapper").addClass("form-control-invalid");
            isValid = false;
        }

        if (!assignTasksFromDate.match(dateReg)) {
            $("#RecurringTask_TaskManager_StartAfter_wrapper").addClass("form-control-invalid");
            isValid = false;
        }

    }
    else if (selectedRecurrenceType === "O") {
        addToTaskManagerXDays = $("#RecurringTask_TaskManager_DisplayBefore").val();
        if (addToTaskManagerXDays === "0") {
            $("#RecurringTask_TaskManager_DisplayBefore_wrapper").addClass("form-control-invalid");
            isValid = false;
        }
    }



    if (isValid === false) {
        ShowValidationModal();
    }

    return isValid;

}


function ShowValidationModal() {
    $("#recurring_Alert").modal('show');
}


function ValidateRecurringTask(sender, args) {

    var SaveSection = ValidateTaskInformation();

    args.IsValid = SaveSection;
}