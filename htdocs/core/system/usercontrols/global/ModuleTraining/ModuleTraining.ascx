﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ModuleTraining.ascx.cs" Inherits="ModuleTrainingCore" %>


   <asp:UpdatePanel id="UpdatePanelTrainingUC" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
   <ContentTemplate>



    <div class="modal fade" id="TrainingConfirmModal" tabindex="-1" role="dialog" aria-labelledby="TrainingConfirmModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
                    

    <div id="TrainingPanel" runat="server">

    <div class="modal-header">
        <h4 class="modal-title" id="TrainingConfirmModalLabel"><%= tsUC.GetResource("uc_elearning_modal_title") %></h4>
    </div>
    <div class="modal-body" align="center" >
        
        <p></p><P></p>
        <p><%= tsUC.GetResource("uc_elearning_P1_modal_body") %></p><p><%= tsUC.GetResource("uc_elearning_P2_modal_body") %></p>

   </div>
    <div class="modal-footer">
        <div class="row">
            <div class="col-xs-12 text-center">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><%= tsUC.GetResource("uc_elearning_btn_modal_cancel") %></button>
                <ASP:Button runat="server"  class="btn btn-success" id="trainingOpen" OnClick="trainingOpen_Click" Text="" />

            </div>
        </div>
    </div>
                                     
    </div>    



    </div>
    </div>
    </div>

    </ContentTemplate>
    </asp:UpdatePanel>



            <div class="row" runat="server" id="ELearningWrapper" >
                <div class="col-md-12">

                    <div class="alert alert-success" role="alert" style="margin-left:20px;margin-right:20px;padding-right:5px;">
                    
                    <div class="media">
                          <div class="media-left">
                              <i class="fa fa-3x fa-trophy" aria-hidden="true"></i>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading" style="margin-bottom:0px;margin-top:2px;;"><%= tsUC.GetResource("uc_elearning_alert_title") %></h4>
                            <%= tsUC.GetResource("uc_elearning_alert_body") %>
                          </div>
                          <div class="media-left">
                            <a href="#" class="btn btn-success btn btn-block" onclick="$('#TrainingConfirmModal').modal('show');"><%= tsUC.GetResource("uc_elearning_btn_modal_runcourse") %></a>
                          </div>
                        </div>
                    </div>

                </div>
            </div>