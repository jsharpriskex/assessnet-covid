﻿using System;
using System.Data.SqlClient;
using ASNETNSP;

public partial class ModuleTrainingCore :  System.Web.UI.UserControl
{
    public string var_corp_code;
    public string yourAccRef;
    public string accLang;
    public string trainingLocation;
    public string trainingLocation2;
    public string ra_elearning_disabled;

    private string currentUser;
    private string yourAccessLevel;

    public string mod { get; set; }

    public TranslationServices tsUC;

    protected void Page_Load(object sender, EventArgs e)
    {

        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();
        currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();

        //***************************
        // INITIATE AND SET TRANSLATIONS
        tsUC = new TranslationServices(var_corp_code, accLang, "usercontrol");
        // END TRANSLATIONS
        //***************************





        trainingOpen.Text = tsUC.GetResource("uc_elearning_btn_modal_runcourse");

        trainingLocation = System.Web.VirtualPathUtility.ToAbsolute("~/core/training/" + mod + "/" + accLang + "/");

        trainingOpen.OnClientClick = "$('#TrainingConfirmModal').modal('hide'); window.open('" + trainingLocation + "', '_blank');";


        switch (mod)
        {
            case "ACC":

                break;
            case "HAZ":

                break;
            case "INSP":

                break;
            case "RA":

                break;
            case "SA":

                break;
            case "TM":

                break;
            case "MH":

                break;
            case "TMX":

                break;
            default:
                ELearningWrapper.Visible = false;
                break;

        }

        // If there is a preference ETraining_Disabled_(MOD) = Y, it will disabled the training
        checkNotDisabled(mod);



    }

    protected void checkNotDisabled(string mod)
    {
        if (SharedComponants.getPreferenceValue(var_corp_code, currentUser, "ETraining_Disabled_" + mod, "pref").ToString() == "Y")
        {
            ELearningWrapper.Visible = false;

        }

    }


    protected void trainingOpen_Click(object sender, EventArgs e)
    {
                logTraining();
    }

    protected void logTraining()
    {

        string sqlText = "";
        try
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "execute Module_LOG.dbo.LogElearning '" + var_corp_code + "', '" + yourAccRef + "','" + mod + "'";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

            }


        }
        catch (Exception)
        {
            throw;
        }


    }



}