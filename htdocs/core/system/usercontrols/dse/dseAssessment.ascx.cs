﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ASNETNSP;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web;

public partial class dseAssessment_UserControl : UserControl
{


    public string corpCode;
    public string currentUser;
    public string templateId;
    public string recordRef;
    private string yourAccessLevel;

    public string currentSectionReference;

    public bool IsAdmin;

    public Page ReferenceToCurrentPage;

    public ControlsCreate CurrentControlsCreate;
    public ControlsCreate.Status Status;
    public AttachmentPopUp currentAttachments;
    public CommentPopUp currentComments;

    public bool ReturnData;
  

    public event EventHandler SaveOrEditButton_Click;

    private bool FormDataRequired = false;

 



    protected void Page_Init(object sender, EventArgs e)
    {
        //***************************
        // CHECK THE SESSION STATUS
        corpCode = Context.Session["CORP_CODE"].ToString();
        currentUser = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        yourAccessLevel = SharedComponants.getPreferenceValue(corpCode, currentUser, "YOUR_ACCESS", "user");
        //END OF SESSION STATUS CHECK
        //***************************

        currentSectionReference = sectionReference_HiddenField_v2.Value;
        sectionReference_Label_v2.Text = currentSectionReference;

        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script12ddfgfdgdfdgdfggffdfgfd3", "alert('" + currentSectionReference + "');", true);


        //CurrentControlsCreate = new ControlsCreate(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        currentSectionReference = sectionReference_HiddenField_v2.Value;
        sectionReference_Label_v2.Text = currentSectionReference;

        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script12ddfgfdgdfdgdfggffdfgfd233", "alert('" + currentSectionReference + "_hello');", true);

        CurrentControlsCreate = new ControlsCreate(this);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {

       if (Status == ControlsCreate.Status.Report)
        {
            CurrentControlsCreate = new ControlsCreate(this);
        }


        UpdatePanel attachmentsModal_UpdatePanel = (UpdatePanel)this.Page.FindControl("attachmentsModal_UpdatePanel");
        currentAttachments = new AttachmentPopUp(attachmentsModal_UpdatePanel, corpCode, recordRef);

        UpdatePanel commentsModal_UpdatePanel = (UpdatePanel)this.Page.FindControl("commentsModal_UpdatePanel");
        currentComments = new CommentPopUp(commentsModal_UpdatePanel, corpCode, recordRef);

    }

    public void generic_Blank_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        RadComboBox currentDropDown = (RadComboBox)sender;
        UserControls.returnUserDropDown(currentDropDown, e, "none", corpCode);
    }


    public void generic_UserDropDown_Validation(object source, ServerValidateEventArgs args)
    {
        CustomValidator currentValidator = (CustomValidator)source;


        RadComboBox currentRadBox = (RadComboBox)currentValidator.FindControl(currentValidator.ControlToValidate);

        UserControls.validate_UserDropDown(currentValidator, currentRadBox, args, false);
        
    }


    public string ReturnCurrentSectionTitle()
    {
        return CurrentControlsCreate.ReturnCurrentSectionTitle();
    }



}



public class ControlsCreate
{

    //Please note: This is a proof of concept. The alternative is binding to a repeater/listview and controlling the added controls then.
    // The downside of another control is extra work when incorporating this logic elsewhere.

    private DataTable _controlData = new DataTable();
    private DataTable _controlSections = new DataTable();


    private PlaceHolder _controlPlaceHolder;
    private UpdatePanel _controlUpdatePanel;
    private Label _debugText;
    private Literal _pageTitle;
    private Literal _pageTitleDescription;
    private HiddenField _assessmentStatus;

    public bool DebugMode = false;

    private string _corpCode;
    private string _sectionReference;
    private string _recordReference;
    private string _templateID;
    private string _currentUser;

    private string _templateTitle;
    private string _templateDescription;

    private string _status;

    private bool _returnData;

    dseAssessment_UserControl _dseAssessment_UserControl;

    private string _idOfControl;

    private Status _AssessmentStatus;

    //PlaceHolder _dseAssessment_PlaceHolder;
    //UpdatePanel _dseAssessment_UpdatePanel;

    //Initiate the Class


    public ControlsCreate(dseAssessment_UserControl currentControl)
    {
        _dseAssessment_UserControl = currentControl;
        _idOfControl = _dseAssessment_UserControl.ID;

        _pageTitle = (Literal)_dseAssessment_UserControl.Page.FindControl("pageTitle");
        _pageTitleDescription = (Literal)_dseAssessment_UserControl.Page.FindControl("pageTitleDescription");

        // Create the object and set the private variables up. They are private so cannot be accessed outside of the interal object.

        _sectionReference = "";
        _controlPlaceHolder = (PlaceHolder)_dseAssessment_UserControl.FindControl("dseAssessment_PlaceHolder");
        _controlUpdatePanel = (UpdatePanel)_dseAssessment_UserControl.FindControl("dseAssessment_UpdatePanel");

        _assessmentStatus = (HiddenField)_dseAssessment_UserControl.Page.FindControl("assessmentStatus");


        _corpCode = _dseAssessment_UserControl.corpCode;
        _sectionReference = _dseAssessment_UserControl.currentSectionReference;
        //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script12dfdfgfd3", "alert('" + _sectionReference + "')", true);
        _templateID = _dseAssessment_UserControl.templateId;
        //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script12dfdfgfdfdsfsd3", "alert('" + _templateID + "')", true);

        _recordReference = _dseAssessment_UserControl.recordRef;

        _debugText = (Label)_controlPlaceHolder.FindControl("textText");

        _currentUser = _dseAssessment_UserControl.currentUser;

        if (_sectionReference == "")
        {
            _sectionReference = ReturnFirstSectionReference();
        }

        _AssessmentStatus = _dseAssessment_UserControl.Status;

        _returnData = _dseAssessment_UserControl.ReturnData;

        if (_returnData)
        {
            //InitiateData();

            if (_AssessmentStatus == Status.Edit)
            {
                InitiateControls();
            }
            else if (_AssessmentStatus == Status.Report)
            {
                InitiateReportPage();
            }

        }

        if (_pageTitleDescription.Text == "")
        {
            //Return title and description
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@sectionReference", ""));
                    cmd.Parameters.Add(new SqlParameter("@customReference", ""));
                    cmd.Parameters.Add(new SqlParameter("@questionReference", ""));
                    cmd.Parameters.Add(new SqlParameter("@type", "GENERALINFO"));


                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _pageTitleDescription.Text = ASNETNSP.Formatting.FormatTextReplaceReturn(objrs["template_description"].ToString());
                        _pageTitle.Text = objrs["template_title"].ToString();
                        _assessmentStatus.Value = objrs["status"].ToString();
                    }
                    else
                    {
                        //Error - TODO
                    }


                    //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        _status = _assessmentStatus.Value;



    }



    //Initiate various things on screen
    private void InitiateDataTable()
    {

        _controlData.Clear();
        if (_controlData.Columns.Count == 0)
        {
            _controlData.Columns.Add("questionReference", typeof(string));
            _controlData.Columns.Add("sectionReference", typeof(string));
            _controlData.Columns.Add("type", typeof(string));
            _controlData.Columns.Add("title", typeof(string));
            _controlData.Columns.Add("placeholder", typeof(string));
            _controlData.Columns.Add("currentValue", typeof(string));
            _controlData.Columns.Add("description", typeof(string));
            _controlData.Columns.Add("custom_type", typeof(string));
            _controlData.Columns.Add("custom_refrence", typeof(string));
            _controlData.Columns.Add("commentRequired", typeof(string));
            _controlData.Columns.Add("comment", typeof(string));
            _controlData.Columns.Add("negativeAnswer", typeof(string));
            _controlData.Columns.Add("attach_required", typeof(string));
            _controlData.Columns.Add("action_required", typeof(string));
            //_controlSections.Columns.Add("sectionReference", typeof(string));
            //_controlSections.Columns.Add("sectionTitle", typeof(string));

            //_controlSections.Rows.Add("1_abc", "Section 1 - Concept");
            //_controlSections.Rows.Add("2_xyz", "Section 2 - Concept");
            //_controlSections.Rows.Add("3_xyz", "Section 3 - Concept");


        }



        // Load datatable with data
        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_ReturnData", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", _sectionReference));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {

                    while (objrs.Read())
                    {

                        string questionReference = objrs["question_reference"].ToString();
                        string sectionReferencee = objrs["section_reference"].ToString();
                        string questionType = objrs["question_type"].ToString();
                        string questionTitle = objrs["question_title"].ToString();
                        string placeholderText = objrs["question_placeholder"].ToString();
                        string currentValue = Formatting.FormatTextInputField(objrs["answer"].ToString());
                        string description = ASNETNSP.Formatting.FormatTextReplaceReturn(objrs["question_description"].ToString());
                        string customReference = objrs["custom_reference"].ToString();
                        string customType = objrs["customType"].ToString();
                        string commentRequired = objrs["force_comment"].ToString();
                        string comment = objrs["comment"].ToString();
                        string negativeAnswer = objrs["negative_answer"].ToString();
                        string attachRequired = objrs["attach_required"].ToString();
                        string action_required = objrs["action_required"].ToString();

                        _controlData.Rows.Add(questionReference, sectionReferencee, questionType, questionTitle
                            , placeholderText, currentValue, description, customType, customReference, commentRequired, comment
                            , negativeAnswer, attachRequired, action_required);

                    }

                }
                else
                {
                    //Error - TODO
                }


                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }






    }

    private void InitiateReportTable()
    {

        _controlData.Clear();
        if (_controlData.Columns.Count == 0)
        {
            _controlData.Columns.Add("questionReference", typeof(string));
            _controlData.Columns.Add("sectionReference", typeof(string));
            _controlData.Columns.Add("type", typeof(string));
            _controlData.Columns.Add("title", typeof(string));
            _controlData.Columns.Add("placeholder", typeof(string));
            _controlData.Columns.Add("currentValue", typeof(string));
            _controlData.Columns.Add("description", typeof(string));
            _controlData.Columns.Add("custom_type", typeof(string));
            _controlData.Columns.Add("custom_refrence", typeof(string));
            _controlData.Columns.Add("commentRequired", typeof(string));
            _controlData.Columns.Add("comment", typeof(string));
            _controlData.Columns.Add("negativeAnswer", typeof(string));
            _controlData.Columns.Add("attach_required", typeof(string));
            _controlData.Columns.Add("action_required", typeof(string));
            _controlData.Columns.Add("section_title", typeof(string));
            _controlData.Columns.Add("section_description", typeof(string));
            _controlData.Columns.Add("pending_count", typeof(string));
            _controlData.Columns.Add("active_count", typeof(string));
            _controlData.Columns.Add("complete_count", typeof(string));
            _controlData.Columns.Add("attach_count", typeof(string));
            _controlData.Columns.Add("concernRemoved", typeof(bool));
            _controlData.Columns.Add("concernRemovedBy", typeof(string));
            _controlData.Columns.Add("concernRemovedOn", typeof(string));
            _controlData.Columns.Add("concernComment", typeof(string));
            _controlData.Columns.Add("concernOngoing", typeof(bool));
            _controlData.Columns.Add("concernOngoingBy", typeof(string));
            _controlData.Columns.Add("concernOngoingOn", typeof(string));
        }

        int i = 0;

        // Load datatable with data
        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_ReturnData", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", DBNull.Value));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {

                    while (objrs.Read())
                    {

                        string questionReference = objrs["question_reference"].ToString();
                        string sectionReference = objrs["section_reference"].ToString();
                        string questionType = objrs["question_type"].ToString();
                        string questionTitle = objrs["question_title"].ToString();
                        string placeholderText = objrs["question_placeholder"].ToString();
                        string currentValue = Formatting.FormatTextInputField(objrs["answer"].ToString());
                        string description = objrs["question_description"].ToString();
                        string customReference = objrs["custom_reference"].ToString();
                        string customType = objrs["customType"].ToString();
                        string commentRequired = objrs["force_comment"].ToString();
                        string comment = objrs["comment"].ToString();
                        string negativeAnswer = objrs["negative_answer"].ToString();
                        string attachRequired = objrs["attach_required"].ToString();
                        string action_required = objrs["action_required"].ToString();
                        string sectionTitle = objrs["section_title"].ToString();
                        string sectionDescription = objrs["section_description"].ToString();
                        string pendingCount = objrs["pending_Count"].ToString();
                        string activeCount = objrs["active_Count"].ToString();
                        string completeCount = objrs["complete_Count"].ToString();
                        string attachCount = objrs["attachCount"].ToString();
                        bool concernRemoved = Convert.ToBoolean(objrs["concern_removed"]);
                        string concernRemovedBy = objrs["concern_removed_by"].ToString();
                        string concernRemovedOn = objrs["concern_removed_on"].ToString();
                        string concernComment = objrs["concern_comment"].ToString();
                        bool concernOngoing = Convert.ToBoolean(objrs["concern_ongoing"]);
                        string concernOngoingBy = objrs["concern_ongoing_by"].ToString();
                        string concernOngoingOn = objrs["concern_ongoing_on"].ToString();

                        _controlData.Rows.Add(questionReference, sectionReference, questionType, questionTitle
                            , placeholderText, currentValue, description, customType, customReference, commentRequired, comment
                            , negativeAnswer, attachRequired, action_required, sectionTitle, sectionDescription, pendingCount, activeCount, completeCount, attachCount
                            , concernRemoved, concernRemovedBy, concernRemovedOn, concernComment, concernOngoing, concernOngoingBy, concernOngoingOn);

                        //ScriptManager.RegisterStartupScript(_currentPlaceHolder.Page, this.GetType(), "sdfsdfdf" , "", true);

                        i++;

                    }

                }
                else
                {
                    //Error - TODO
                }




                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }






    }


    public void InitiateControls()
    {
        InitiateDataTable();
        InitiateControls(_sectionReference);
    }

    private void InitiateControls(string sectionReference)
    {
        _controlPlaceHolder.Controls.Clear();

        LiteralControl startDiv = new LiteralControl();

        startDiv.Text = "<div class='col-xs-12' style='margin-bottom:0px;margin-top: 0px;'> " +
                                "<div class='table'>" +
                                    "  <table class='table table-bordered table-assessmentstyle'>" +
                                    "    <tbody>";

        _controlPlaceHolder.Controls.Add(startDiv);

        // Create the controls on the page.
        foreach (DataRow row in _controlData.Select("sectionReference='" + sectionReference + "'"))
        {


            // Assign variables based on the DataTable column position. This may change.
            string currentControlID = row.Field<string>(0);
            string currentSectionReference = row.Field<string>(1);
            string currentControlType = row.Field<string>(2);
            string currentControlTitle = row.Field<string>(3);
            string currentControlPlaceholder = row.Field<string>(4);
            string currentValue = row.Field<string>(5);
            string description = row.Field<string>(6);
            string customType = row.Field<string>(7);
            string customReference = row.Field<string>(8);
            string commentRequired = row.Field<string>(9);
            string currentComment = row.Field<string>(10);
            string negativeAnswer = row.Field<string>(11);
            string attachRequired = row.Field<string>(12);
            string actionRequired = row.Field<string>(13);

            string[] negativeAnswer_Array = StringToArray(negativeAnswer, ',');
            string[] currentValue_Array = StringToArray(currentValue, ',');

            // If the answer is the templates negative one, then we need to declare it.
            bool currentAnswerIsNegative = false;

            // If its checkboxes multiple can be selected. We check to see if the two arrays share a value.
            bool hasCommonElements = currentValue_Array.Intersect(negativeAnswer_Array).Count() > 0;

            if ((negativeAnswer_Array.Length > 0) && (currentValue != "") && (negativeAnswer_Array.Contains(currentValue) || hasCommonElements))
            {
                currentAnswerIsNegative = true;
            }


            // Extra logic for options with a description
            if (description.Length > 0)
            {
                description = " <br /> <span class='small'>" + description + "</span>";
            }

            // Create a HTML element for the top of the control.
            LiteralControl startRow = new LiteralControl();
            //startRow.Text = "<div class='row'><div class='col-xs-3'>" + currentControlTitle + "</div><div class='col-xs-9'><div class='form-group'>";
            startRow.Text = "<tr class='controlRow'>" +
                                "<th>" +
                                "  <p style='margin: 0px !important;'>" + currentControlTitle + description + "</p>" +
                                "</th>";


            _controlPlaceHolder.Controls.Add(startRow);


            UpdatePanel new_UpdatePanel = new UpdatePanel();
            new_UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
            new_UpdatePanel.ID = "button_UpdatePanel_" + currentControlID;

            UpdatePanel new_UpdatePanel2 = new UpdatePanel();
            new_UpdatePanel2.UpdateMode = UpdatePanelUpdateMode.Conditional;
            new_UpdatePanel2.ID = "button_UpdatePanel2_" + currentControlID;

            //if (attachRequired == "1" || attachRequired == "2")
            //{

            LiteralControl startAttachCol = new LiteralControl();
            startAttachCol.Text = "<th width='10px'>";

            _controlPlaceHolder.Controls.Add(startAttachCol);

            LinkButton attachmentButton = new LinkButton();
                attachmentButton.ID = "attach_" + currentControlID;
            
                attachmentButton.Text = "<i class='fa fa-paperclip' style='text-align:left;'></i>";

                attachmentButton.Click += new EventHandler(attachmentButton_Click);


                attachmentButton.CssClass = ReturnCSSClassForAttachment(_corpCode, _recordReference, _sectionReference, currentControlID);



                new_UpdatePanel.ContentTemplateContainer.Controls.Add(attachmentButton);

            _controlPlaceHolder.Controls.Add(new_UpdatePanel);

            LiteralControl endAttachCol = new LiteralControl();
            endAttachCol.Text = "</th>";

            _controlPlaceHolder.Controls.Add(endAttachCol);
            
            //}

            //if (actionRequired == "1" || actionRequired == "2")
            //{
            //    LinkButton actionsButton = new LinkButton();
            //    actionsButton.ID = "actions_" + currentControlID;
            //    actionsButton.Text = "<i class='fa fa-calendar-check-o' style='text-align:left;'></i> Remedial Actions";

            //    actionsButton.Click += new EventHandler(actionsButton_Click);


            //    actionsButton.CssClass = ReturnCSSClassForActions(_corpCode, _recordReference, _sectionReference, currentControlID);


            //    new_UpdatePanel.ContentTemplateContainer.Controls.Add(actionsButton);


            //}

            //if (commentRequired == "3")
            //{

            LiteralControl startAttachCol2 = new LiteralControl();
            startAttachCol2.Text = "<th width='10px'>";

            _controlPlaceHolder.Controls.Add(startAttachCol2);

            LinkButton commentsButton = new LinkButton();
            commentsButton.ID = "comment_" + currentControlID;
            commentsButton.Text = "<i class='fa fa-comments' style='text-align:left;'></i>";

            commentsButton.Click += new EventHandler(commentButton_Click);


            commentsButton.CssClass = ReturnCSSClassForComment(_corpCode, _recordReference, _sectionReference, currentControlID);



            new_UpdatePanel2.ContentTemplateContainer.Controls.Add(commentsButton);

            _controlPlaceHolder.Controls.Add(new_UpdatePanel2);

            LiteralControl endAttachCol2 = new LiteralControl();
            endAttachCol2.Text = "</th>";

            _controlPlaceHolder.Controls.Add(endAttachCol2);

            //Literal commentsButton = new Literal();

            //commentsButton.Text = "<td width='10px'><button type='button' class='btn btn-primary pullright' id='actions_" + currentControlID + "' onclick='ShowComments(this);'><i class='fa fa-comments' style='text-align:left;'></i></button>";
                
            ////_controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

            //commentsButton.Text = commentsButton.Text + "</td>";

            //new_UpdatePanel.ContentTemplateContainer.Controls.Add(commentsButton);


            //}

            
            // _controlPlaceHolder.Controls.Add(afterButtonRow);

            LiteralControl startAnswer = new LiteralControl();
            startAnswer.Text = "<td width='255px'>";
            _controlPlaceHolder.Controls.Add(startAnswer);

            //This converts the string of controlType to an Enum.
            QuestionType currentControl = (QuestionType)Enum.Parse(typeof(QuestionType), currentControlType, true);

            switch (currentControl)
            {

                // TextBox - Single
                case QuestionType.TextBox_Single:
                    TextBox control_1 = new TextBox();
                    control_1.ID = "control_" + currentControlID;
                    control_1.Attributes.Add("placeholder", currentControlPlaceholder);
                    control_1.CssClass = "form-control";
                    control_1.Text = currentValue;
                    _controlPlaceHolder.Controls.Add(control_1);

                    _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                    break;

                // Textbox - Multi
                case QuestionType.TextBox_Multi:
                    TextBox control_2 = new TextBox();
                    control_2.ID = "control_" + currentControlID;
                    control_2.Attributes.Add("placeholder", currentControlPlaceholder);
                    control_2.CssClass = "form-control";
                    control_2.TextMode = TextBoxMode.MultiLine;
                    control_2.Rows = 5;
                    control_2.Text = currentValue;
                    _controlPlaceHolder.Controls.Add(control_2);

                    _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                    break;

                // Checkbox
                case QuestionType.CheckBox:
                    CheckBox control_3 = new CheckBox();
                    control_3.ID = "control_" + currentControlID;
                    control_3.CssClass = "";
                    _controlPlaceHolder.Controls.Add(control_3);

                    break;

                // Radio Buttons Y/N/NA
                case QuestionType.Radio_YNNA:
                case QuestionType.Radio_YN:
                    //Radio buttons are styled differently to be buttons.
                    LiteralControl startButtonY = new LiteralControl();
                    LiteralControl endButtonY = new LiteralControl();

                    LiteralControl startButtonN = new LiteralControl();
                    LiteralControl endButtonN = new LiteralControl();

                    LiteralControl startButtonNA = new LiteralControl();
                    LiteralControl endButtonNA = new LiteralControl();

                    LiteralControl startHTML = new LiteralControl();
                    LiteralControl endHTML = new LiteralControl();

                    RadioButton RadioButtonY = new RadioButton();
                    RadioButtonY.ID = "control_" + currentControlID + "_Y";
                    RadioButtonY.GroupName = currentControlID.ToString();
                    RadioButtonY.Checked = (currentValue == "Y") ? true : false;

                    RadioButton RadioButtonN = new RadioButton();
                    RadioButtonN.ID = "control_" + currentControlID + "_N";
                    RadioButtonN.GroupName = currentControlID.ToString();
                    RadioButtonN.Checked = (currentValue == "N") ? true : false;

                    RadioButton RadioButtonNA = new RadioButton();
                    RadioButtonNA.ID = "control_" + currentControlID + "_NA";
                    RadioButtonNA.GroupName = currentControlID.ToString();
                    RadioButtonNA.Checked = (currentValue == "NA") ? true : false;


                    startHTML.Text = "<div class='parentRadioGroup text-left' style='margin-top:10px !important;'><div class='radioGroup'>";
                    endHTML.Text = "</div></div>";

                    startButtonY.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"Y\", \"" + commentRequired + "\")' for='" + _idOfControl + "_control_" + currentControlID + "_Y'>"; 
                    endButtonY.Text = "<span>Yes</span></label>&nbsp;";

                    startButtonNA.Text = "&nbsp;<label class='blue'  onclick='triggerComments(this, \"" + negativeAnswer + "\", \"NA\", \"" + commentRequired + "\")' for='" + _idOfControl + "_control_" + currentControlID + "_NA'>";
                    endButtonNA.Text = "<span>N/A</span></label>";

                    startButtonN.Text = "<label class='blue'  onclick='triggerComments(this, \"" + negativeAnswer + "\", \"N\", \"" + commentRequired + "\")' for='" + _idOfControl + "_control_" + currentControlID + "_N'>";
                    endButtonN.Text = "<span>No</span></label>";

                    _controlPlaceHolder.Controls.Add(startHTML);

                    _controlPlaceHolder.Controls.Add(startButtonY);
                    _controlPlaceHolder.Controls.Add(RadioButtonY);
                    _controlPlaceHolder.Controls.Add(endButtonY);

                    _controlPlaceHolder.Controls.Add(startButtonN);
                    _controlPlaceHolder.Controls.Add(RadioButtonN);
                    _controlPlaceHolder.Controls.Add(endButtonN);

                    if (currentControl == QuestionType.Radio_YNNA)
                    {
                        _controlPlaceHolder.Controls.Add(startButtonNA);
                        _controlPlaceHolder.Controls.Add(RadioButtonNA);
                        _controlPlaceHolder.Controls.Add(endButtonNA);
                    }



                    _controlPlaceHolder.Controls.Add(endHTML);


                    //ToDo
                    _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));

                    break;

                case QuestionType.Custom:
                    // This is for customised dropdowns / radiobuttons
                    // The custom_reference is used to populate control.
                    // customType, customReference have been populated.
                    DataTable temp_QuestionHolder = new DataTable();

                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                            cmd.Parameters.Add(new SqlParameter("@type", "RETURNOPTIONS"));
                            cmd.Parameters.Add(new SqlParameter("@sectionReference", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("@questionReference", DBNull.Value));

                            objrs = cmd.ExecuteReader();

                            temp_QuestionHolder.Load(objrs);

                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }


                    //Now that we have the data in a temp datatable, we need to assign it to either a dropdownlist or radio selection.
                    CustomQuestionType currentCustomType = (CustomQuestionType)Enum.Parse(typeof(CustomQuestionType), customType, true);
                    switch (currentCustomType)
                    {
                        //Dropdown
                        case CustomQuestionType.DropDown:
                            DropDownList control_DropDownList = new DropDownList();
                            control_DropDownList.ID = "control_" + currentControlID;
                            control_DropDownList.CssClass = "form-control";
                            control_DropDownList.DataSource = temp_QuestionHolder;
                            control_DropDownList.DataValueField = "opt_id";
                            control_DropDownList.DataTextField = "title";

                            control_DropDownList.DataBind();

                            control_DropDownList.Items.Insert(0, new ListItem("Please Select...", ""));

                            if (control_DropDownList.Items.FindByValue(currentValue) != null)
                            {
                                control_DropDownList.SelectedValue = currentValue;
                            }

                            control_DropDownList.EnableViewState = false;

                            control_DropDownList.Attributes["onChange"] = "triggerComments(this, \"" + negativeAnswer + "\", this.options[this.selectedIndex].value, \"" + commentRequired + "\");";

                            _controlPlaceHolder.Controls.Add(control_DropDownList);

                            _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));


                            break;

                        //Radio Buttons
                        case CustomQuestionType.Radio:


                            LiteralControl cus_startHTML = new LiteralControl();
                            LiteralControl cus_endHTML = new LiteralControl();

                            cus_startHTML.Text = "<div class='parentRadioGroup text-center'><div class='radioGroup'>";
                            cus_endHTML.Text = "</div></div>";

                            _controlPlaceHolder.Controls.Add(cus_startHTML);

                            foreach (DataRow customRow in temp_QuestionHolder.Select())
                            {

                                RadioButton custom_RadioButton = new RadioButton();
                                LiteralControl custom_Start = new LiteralControl();
                                LiteralControl custom_End = new LiteralControl();
                                decimal current_OptId = customRow.Field<decimal>(3);
                                string current_OptName = customRow.Field<string>(4);

                                custom_Start.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"" + current_OptId + "\", \"" + commentRequired + "\")'>";
                                custom_End.Text = "<span>" + current_OptName + "</span></label>";

                                custom_RadioButton.ID = "control_" + currentControlID + "_" + current_OptId.ToString();
                                custom_RadioButton.GroupName = currentControlID.ToString();
                                custom_RadioButton.Checked = (currentValue == current_OptId.ToString()) ? true : false;

                                _controlPlaceHolder.Controls.Add(custom_Start);
                                _controlPlaceHolder.Controls.Add(custom_RadioButton);
                                _controlPlaceHolder.Controls.Add(custom_End);


                            }

                            _controlPlaceHolder.Controls.Add(cus_endHTML);

                            _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));



                            break;

                        // Checkboxes
                        case CustomQuestionType.Checkbox:
                            LiteralControl cuscheck_startHTML = new LiteralControl();
                            LiteralControl cuscheck_endHTML = new LiteralControl();

                            cuscheck_startHTML.Text = "<div class='parentRadioGroup text-center'><div class='radioGroup'>";
                            cuscheck_endHTML.Text = "</div></div>";

                            _controlPlaceHolder.Controls.Add(cuscheck_startHTML);

                            foreach (DataRow customRow in temp_QuestionHolder.Select())
                            {

                                CheckBox custom_CheckBox = new CheckBox();
                                LiteralControl custom_Start = new LiteralControl();
                                LiteralControl custom_End = new LiteralControl();
                                decimal current_OptId = customRow.Field<decimal>(3);
                                string current_OptName = customRow.Field<string>(4);
                                string[] valueArray = currentValue.Split(',');

                                custom_Start.Text = "<label class='blue' onclick='triggerComments(this, \"" + negativeAnswer + "\", \"" + current_OptId + "\", \"" + commentRequired + "\")'>";
                                custom_End.Text = "<span>" + current_OptName + "</span></label>";

                                custom_CheckBox.ID = "control_" + currentControlID + "_" + current_OptId.ToString();
                                custom_CheckBox.Checked = (valueArray.Contains(current_OptId.ToString())) ? true : false;

                                _controlPlaceHolder.Controls.Add(custom_Start);
                                _controlPlaceHolder.Controls.Add(custom_CheckBox);
                                _controlPlaceHolder.Controls.Add(custom_End);

                            }

                            _controlPlaceHolder.Controls.Add(cuscheck_endHTML);


                            // needs further jquery
                            _controlPlaceHolder.Controls.Add(ReturnRequiredComment(typeof(RadioButton), currentControlID, commentRequired, currentComment, currentAnswerIsNegative));


                            break;

                        default:
                            break;
                    }








                    temp_QuestionHolder.Dispose();

                    break;

                default:
                    break;
            }

            LiteralControl endAnswer = new LiteralControl();
            endAnswer.Text = "</td>";

            _controlPlaceHolder.Controls.Add(endAnswer);

            // Create a HTML element for the bottom of the control.
            LiteralControl endRow = new LiteralControl();
            endRow.Text = " </tr>";
            _controlPlaceHolder.Controls.Add(endRow);

        }

        LiteralControl endDiv = new LiteralControl();
        endDiv.Text = "    </tbody>" +
                            " </table>" +
                        "  </div>" +
                    "  </div> ";

        _controlPlaceHolder.Controls.Add(endDiv);

        InitiateBottomButtons();
        //InitiateFinalButton();


        if (DebugMode)
        {
            _debugText.Text = this.ToString();
        }

    }

    public void InitiateReportPage()
    {
        InitiateReportTable();
        InitiateReport();

        //InitiateReportV2();
    }

    private void InitiateReport()
    {

        string sectionReference = "";
        _controlPlaceHolder.Controls.Clear();

        //Grab the distinct rows so we can loop through them.
        var distinctRows = _controlData.AsEnumerable()
                   .Select(s => new {
                       id = s.Field<string>("sectionReference"),
                       title = s.Field<string>("section_title"),
                       description = s.Field<string>("section_description"),
                   })
                   .Distinct().ToList();

        foreach (var sectionRow in distinctRows)
        {
            string currentSectionReference = sectionRow.id;


            //Create Panel
            string topSection_HTML = "<div class='panel panel-default sectionClass " + currentSectionReference + "' id='section_" + currentSectionReference + "' style='page-break-before: always;'> " +
                                            "<div class='panel-heading'>" +
                                                "<h3 class='panel-title' style='margin-left: 0 !important;'><i class='fa fa-bars' aria-hidden='true'></i> Section - " + sectionRow.title + "</h3>" +
                                            "</div>" +
                                            "<div class='panel-body'>" +
                                               " <div class='row'>" +
                                                    "<div class='col-xs-12'>";

            string bottomSection_HTML = "</div>" +
                                    " </div>" +
                                " </div>" +
                           " </div> ";


            Literal topSection_Literal = new Literal();
            topSection_Literal.Text = topSection_HTML;

            Literal bottomSection_Literal = new Literal();
            bottomSection_Literal.Text = bottomSection_HTML;

            _controlPlaceHolder.Controls.Add(topSection_Literal);


            // Create the controls on the page.
            foreach (DataRow row in _controlData.Select("sectionReference='" + currentSectionReference + "'"))
            {


                // Assign variables based on the DataTable column position. This may change.
                string currentControlID = row.Field<string>(0);
                //string currentSectionReference = row.Field<string>(1);
                string currentControlType = row.Field<string>(2);
                string currentControlTitle = row.Field<string>(3);
                string currentControlPlaceholder = row.Field<string>(4);
                string currentValue = row.Field<string>(5);
                string description = row.Field<string>(6);
                string customType = row.Field<string>(7);
                string customReference = row.Field<string>(8);
                string commentRequired = row.Field<string>(9);
                string currentComment = row.Field<string>(10);
                string negativeAnswer = row.Field<string>(11);
                string attachRequired = row.Field<string>(12);
                string actionRequired = row.Field<string>(13);
                string pendingCount = row.Field<string>(16);
                string activeCount = row.Field<string>(17);
                string completeCount = row.Field<string>(18);
                string attachCount = row.Field<string>(19);
                bool concernRemoved = row.Field<bool>(20);
                string concernRemovedBy = row.Field<string>(21);
                string concernRemovedOn = row.Field<string>(22);
                string concernComment = row.Field<string>(23);
                bool concernOngoing = row.Field<bool>(24);
                string concernOngoingBy = row.Field<string>(25);
                string concernOngoingOn = row.Field<string>(26);

                string[] negativeAnswer_Array = StringToArray(negativeAnswer, ',');
                string[] currentValue_Array = StringToArray(currentValue, ',');

                bool hasActions = false;
                if (pendingCount != "0" || activeCount != "0" || completeCount != "0") { hasActions = true; }

                string actionTop = "";
                string actionBottom = "";
                string actionColSpan = "1";

                if (hasActions)
                {
                    actionTop = "<th width = '7%' class='text-center'>Task Status</th>";
                    actionColSpan = "2";
                    actionBottom = " <td> " +
                                        "<div class='taskStatGroup'> " +
                                        "  <div class='taskStatPending'>" + pendingCount + "</div> " +
                                        "  <div class='taskStatActive'>" + activeCount + "</div> " +
                                        "  <div class='taskStatComplete'>" + completeCount + "</div> " +
                                    " </div> " +
                                "  </td> ";
                }


                string valueCustomStringName = "";

                //Enum of current type
                QuestionType currentControl = (QuestionType)Enum.Parse(typeof(QuestionType), currentControlType, true);

                // Convert numerical answers to their counterparts
                switch (currentControl)
                {

                    // TextBox - Single
                    case QuestionType.TextBox_Single:
                        valueCustomStringName = Formatting.FormatTextReplaceReturn(currentValue);
                        break;

                    // Textbox - Multi
                    case QuestionType.TextBox_Multi:
                        valueCustomStringName = Formatting.FormatTextReplaceReturn(currentValue);
                        break;

                    // Checkbox
                    case QuestionType.CheckBox:

                        break;

                    // Radio Buttons Y/N/NA
                    case QuestionType.Radio_YNNA:
                    case QuestionType.Radio_YN:

                        if (currentValue == "Y")
                        {
                            valueCustomStringName = "Yes";
                        }
                        else if (currentValue == "N")
                        {
                            valueCustomStringName = "No";
                        }
                        else if (currentValue == "NA")
                        {
                            valueCustomStringName = "Not Applicable";
                        }


                        break;

                    case QuestionType.Custom:
                        // This is for customised dropdowns / radiobuttons


                        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {

                                SqlDataReader objrs = null;
                                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                                cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                                cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                                cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                                cmd.Parameters.Add(new SqlParameter("@type", "RETURNSELECTEDOPTIONS_CUSTOM"));



                                objrs = cmd.ExecuteReader();

                                if (objrs.HasRows)
                                {
                                    objrs.Read();
                                    string[] currentSelectedOptions = StringToArray(objrs["deliminatedOutput"].ToString(), ',');

                                    int itemCount = 0;

                                    foreach (var item in currentSelectedOptions)
                                    {


                                        if (itemCount == 0)
                                        {
                                            valueCustomStringName = item;
                                        }
                                        else
                                        {
                                            if (item != "")
                                            {
                                                valueCustomStringName += " - " + item;
                                            }

                                        }

                                        itemCount++;

                                    }

                                }
                                else
                                {
                                    //Error - TODO
                                }




                                cmd.Dispose();
                                objrs.Dispose();

                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }



                        break;

                    default:
                        break;
                }


                // Work out if its negative, if so, we colour the border in red.
                bool isNegative = false;
                string rowColour = "";
                string negativeClass = "";



                foreach (var item in StringToArray(negativeAnswer, ','))
                {

                    if (currentControl == QuestionType.Custom)
                    {
                        foreach (var custItem in StringToArray(currentValue, ','))
                        {

                            if (item == custItem && item != "")
                            {
                                isNegative = true;
                            }

                        }
                    }
                    else
                    {
                        if (item == currentValue)
                        {
                            isNegative = true;
                        }
                    }

                }

                if (isNegative && concernRemoved != true && concernOngoing != true)
                {
                    rowColour = "#e1726e";
                    negativeClass = "negativeAnswer";
                }
                else if (concernRemoved == true)
                {
                    rowColour = "#4E9258";
                    negativeClass = "removedConcern";
                }
                else if (concernOngoing == true)
                {
                    rowColour = "#FFD801";
                    negativeClass = "ongoingConcern";
                }
                else
                {
                    rowColour = "#FAFAFA";
                    negativeClass = "nonnegativeAnswer";
                }


                if (concernRemoved == true)
                {
                    valueCustomStringName += "<br /><br />Concern removed by " + UserControls.GetUser(concernRemovedBy, _corpCode) + "  on " + Formatting.FormatDateTime(concernRemovedOn, 1) + "";

                    if (concernComment.Length > 0)
                    {
                        valueCustomStringName += " with the reason: <br /> " + concernComment;
                    }

                }

                if (concernOngoing == true)
                {
                    valueCustomStringName += "<br /><br />Flagged as ongoing by " + UserControls.GetUser(concernOngoingBy, _corpCode) + "  on " + Formatting.FormatDateTime(concernOngoingOn, 1) + "";

                    if (concernComment.Length > 0)
                    {
                        valueCustomStringName += " with the reason: <br /> " + concernComment;
                    }

                }




                Literal addThis = new Literal();

                addThis.Text = "<br />" + currentSectionReference + " - " + currentControlID;

                ///_currentPlaceHolder.Controls.Add(addThis);

                UpdatePanel sectionUpdatePanel = new UpdatePanel();
                sectionUpdatePanel.ID = "sectionUpdatePanel_" + currentSectionReference + "_" + currentControlID;
                sectionUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;


                string mainContainerTop_Html_1 = "<div class='row questionClass " + negativeClass + "'>" +
                                                            "<div class='col-xs-12'>" +
                                                                "<div class='actionViewContainer ' style='border-color: " + rowColour + " !important; border-width: 5px; margin-top: 0px;'>" +
                                                                   " <div class='row'>" +
                                                                       " <div class='col-xs-12'>" +
                                                                          "  <div class='row'>" +
                                                                               " <div class='col-xs-12'>" +
                                                                               "<table class='table table-bordered table-marginless table-assessmentstyle'> " +
                                                                                       " <thead> " +
                                                                                          "  <tr class='warning'> " +
                                                                                           "     <th width = '50%' style='border-right: none;'>" + currentControlTitle + "</th> " +
                                                                                            "    <th width = '50%' style='border-left: none; text-align:right;'>";

                string mainContainerTop_Html_2 = "   </th> " +
                                                                                              actionTop +
                                                                                            " </tr> " +
                                                                                       "  </thead> " +
                                                                                        " <tbody>" +
                                                                                        "<tr> " +
                                                                                         "<td colspan = '2'>" + valueCustomStringName +
                                                                                                     " <br /> " +
                                                                                                 " </td> " +
                                                                                                 actionBottom +
                                                                                             "</tr> " +
                                                                                         "</tbody>";


                Literal mainContainerTop_Literal_1 = new Literal();
                Literal mainContainerTop_Literal_2 = new Literal();
                mainContainerTop_Literal_1.Text = mainContainerTop_Html_1;
                mainContainerTop_Literal_2.Text = mainContainerTop_Html_2;

                UpdatePanel buttonUpdatePanel = new UpdatePanel();
                buttonUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
                buttonUpdatePanel.ID = "buttonUpdatePanel_" + currentSectionReference + "_" + currentControlID;


                LinkButton remedialActions = new LinkButton();
                remedialActions.CssClass = "btn btn-default actionButton";
                remedialActions.Text = "Manage Actions";
                remedialActions.Click += new EventHandler(actionsButton_Click);
                remedialActions.ID = "remedialActions_" + currentSectionReference + "_" + currentControlID;

                LinkButton removeConcern = new LinkButton();
                removeConcern.CssClass = "btn btn-default concernButton";
                removeConcern.Text = "Remove Concern";
                removeConcern.Click += new EventHandler(removeConcernButton_Click);
                removeConcern.ID = "removeConcern_" + currentSectionReference + "_" + currentControlID;
                removeConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton ongoingConcern = new LinkButton();
                ongoingConcern.CssClass = "btn btn-default concernButton";
                ongoingConcern.Text = "Ongoing Concern";
                ongoingConcern.Click += new EventHandler(ongoingConcernButton_Click);
                ongoingConcern.ID = "ongoingConcern_" + currentSectionReference + "_" + currentControlID;
                ongoingConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton restoreConcern = new LinkButton(); 
                restoreConcern.CssClass = "btn btn-default concernButton";
                restoreConcern.Text = "Restore Concern";
                restoreConcern.Click += new EventHandler(restoreConcernButton_Click);
                restoreConcern.ID = "restoreConcern_" + currentSectionReference + "_" + currentControlID;
                restoreConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton manageComments = new LinkButton();
                manageComments.CssClass = "btn btn-default concernButton";
                manageComments.Text = "Add Comment";
                manageComments.Click += new EventHandler(addCommentsButton_Click);
                manageComments.ID = "manageComments_" + currentSectionReference + "_" + currentControlID;
                manageComments.Attributes.Add("style", "margin-right:10px;");


                //if ((_currentAccess == "edit" || _currentAccess == "supervisor" || _currentAccess == "authoriseduser"))
                //{

                //    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(manageComments);

                //    //if (_isPortal == "N")
                //    //{
                if (isNegative && concernRemoved != true && concernOngoing != true)
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(removeConcern);
                }
                if (isNegative && concernOngoing != true && concernRemoved != true)
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(ongoingConcern);
                }
                if (isNegative && (concernRemoved == true || concernOngoing == true))
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(restoreConcern);
                }

                //    //    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(remedialActions);
                //    //}

                //}





                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerTop_Literal_1);
                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(buttonUpdatePanel);
                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerTop_Literal_2);

                //_currentPlaceHolder.Controls.Add(buttonUpdatePanel);
                //_currentPlaceHolder.Controls.Add(mainContainerTop_Literal_2);


                string mainContainerBottom_Html = "</table></div>" +
                                                    "</div>";



                // CG - COMMENTS HERE
                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                        cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                        cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                        cmd.Parameters.Add(new SqlParameter("@type", "RETURNCOMMENTS"));



                        objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {

                            mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''> " +
                                                       "<div class='col-xs-12'> " +
                                                          " <table class='table table-bordered table-marginless table-assessmentstyle'> " +
                                                               "<thead> " +
                                                                   "<tr class='warning'> " +
                                                                       "<th width = '70%'> Comments </th> " +
                                                                       "<th width = '15%' class='text-center' style='text-align:center !important;'> Added On </ th > " +
                                                                       "<th width = '15%' class='text-center' style='text-align:center !important;'> Added By </ th > " +
                                                                   "</tr> " +
                                                               "</thead> " +
                                                               "<tbody>";


                            while (objrs.Read())
                            {


                                string comment = Formatting.FormatTextInputField(Formatting.FormatTextReplaceReturn(Convert.ToString(objrs["comment"])));
                                string addedBy = UserControls.GetUser(Convert.ToString(objrs["addedBy"]), _corpCode);


                                string addedOnText;

                                if (objrs["addedOn"] == DBNull.Value)
                                {
                                    addedOnText = "Added During Assessment";
                                }
                                else
                                {
                                    addedOnText = Convert.ToDateTime(objrs["addedOn"]).ToShortDateString();
                                }


                                mainContainerBottom_Html += "<tr> " +
                                                                "<td>" + comment + "</td> " +
                                                                "<td class='text-center'>" + addedOnText + "</td> " +
                                                                "<td class='text-center'>" + addedBy + "</td> " +
                                                            "</tr> ";
                            }


                            mainContainerBottom_Html += "</tbody> " +
                                                           "</table> " +
                                                       "</div> " +
                                                   "</div>";

                        }
                        else
                        {
                            //Error - TODO
                        }




                        cmd.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }



                if (currentComment.Length > 0)
                {

                }


                if (attachCount != "0" && attachCount.Length > 0)
                {

                    //Grab data!
                    DataTable currentAttachments = ASNETNSP.SharedComponants.returnFileData(_corpCode, _recordReference, currentSectionReference + "_" + currentControlID, "");
                    StringBuilder attachments = new StringBuilder();

                    foreach (DataRow item in currentAttachments.Rows)
                    {
                        attachments.AppendLine("<div style='float: left; '>" + ASNETNSP.SharedComponants.returnAttachmentItem(item.Field<string>("filepath"), item.Field<string>("filename"), item.Field<string>("friendlyname"), item.Field<string>("filetype"), "medium", "../../../uploads") + "</div>");
                    }

                    mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''> " +
                                                        "<div class='col-xs-12'> " +
                                                           " <table class='table table-bordered  table-condensed  table-marginless'> " +
                                                                "<thead> " +
                                                                    "<tr class='warning'> " +
                                                                        "<th width = '100%'> Attachments </th > " +
                                                                    "</tr> " +
                                                                "</thead> " +
                                                                "<tbody> " +
                                                                    "<tr> " +
                                                                        "<td>" + attachments.ToString() + "</td> " +
                                                                    "</tr> " +
                                                                "</tbody> " +
                                                            "</table> " +
                                                        "</div> " +
                                                    "</div>";

                    currentAttachments.Dispose();
                }


                if (hasActions)
                {


                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_ActionCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                            cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                            cmd.Parameters.Add(new SqlParameter("@actionreference", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTION"));


                            objrs = cmd.ExecuteReader();

                            if (objrs.HasRows)
                            {

                                StringBuilder currentActions = new StringBuilder();


                                while (objrs.Read())
                                {
                                    currentActions.AppendLine("<tr>" +
                                                                "<td>" + objrs["task_description"].ToString() + "</td>" +
                                                                "<td align = 'center' >" +
                                                                        "<div class='taskStatGroup'>" +
                                                                        "<div class='taskStat" + ASNETNSP.SharedComponants.returnTaskStatus(objrs["task_status"].ToString(), "") + " NoWidth'>" +
                                                                            ASNETNSP.SharedComponants.returnTaskStatus(objrs["task_status"].ToString(), "") +
                                                                        "</div>" +
                                                                    "</div>         " +
                                                                "</td>" +
                                                                "<td align = 'center' ><strong > " + ASNETNSP.Formatting.FormatDateTime(objrs["action_due_date"], 1) + " </strong ><br /> " + ASNETNSP.Formatting.FormatFullNameDotSurname(objrs["Per_fname"], objrs["Per_sname"]) + "</td>" +
                                                            " </tr>");
                                }



                                mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''>" +
                                                                                "<div class='col-xs-12'>" +
                                                                                    "<table class='table table-bordered table-marginless'>" +
                                                                                        "<thead>" +
                                                                                            "<tr class='active'>" +
                                                                                                "<th width = '70%' class='warning'>Action" +
                                                                                                "</th>" +
                                                                                                "<th width = '15%' class='warning text-center'>Current Status</th>" +
                                                                                                "<th width = '15%;' class='warning text-center'>Due Date / By Whom</th>" +
                                                                                            "</tr>" +
                                                                                        "</thead>" +
                                                                                        "<tbody>" +
                                                                                            currentActions.ToString() +
                                                                                        "</tbody>" +
                                                                                    "</table>" +
                                                                                "</div>" +
                                                                           " </div>";




                            }


                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }




                mainContainerBottom_Html += "</div>" +
                                                    "</div>" +
                                                    "</div>" +
                                                    "</div>" +
                                                    "</div>";

                Literal mainContainerBottom_Literal = new Literal();
                mainContainerBottom_Literal.Text = mainContainerBottom_Html;


                //_currentPlaceHolder.Controls.Add(mainContainerBottom_Literal);



                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerBottom_Literal);


                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(bottomSection_Literal);

                _controlPlaceHolder.Controls.Add(sectionUpdatePanel);
                //_currentPlaceHolder.Controls.Add(bottomSection_Literal);

            }





            // End Panel.


        }


    }


    private void InitiateReportV2()
    {

        string sectionReference = "";
        _controlPlaceHolder.Controls.Clear();

        //Grab the distinct rows so we can loop through them.
        var distinctRows = _controlData.AsEnumerable()
                   .Select(s => new {
                       id = s.Field<string>("sectionReference"),
                       title = s.Field<string>("section_title"),
                       description = s.Field<string>("section_description"),
                   })
                   .Distinct().ToList();

        foreach (var sectionRow in distinctRows)
        {
            string currentSectionReference = sectionRow.id;


            //Create Panel
            string topSection_HTML = "<div class='panel panel-default sectionClass " + currentSectionReference + "' id='section_" + currentSectionReference + "' style='page-break-before: always;'> " +
                                            "<div class='panel-heading'>" +
                                                "<h3 class='panel-title' style='margin-left: 0 !important;'><i class='fa fa-bars' aria-hidden='true'></i> Section - " + sectionRow.title + "</h3>" +
                                            "</div>" +
                                            "<div class='panel-body'>" +
                                               " <div class='row'>" +
                                                    "<div class='col-xs-12'>" +
                                                        "<table class='table table-bordered table-marginless table-assessmentstyle'>" +
                                                        "<thead>" +
                                                        "  <tr class='warning'> " +
                                                        "     <th width = '50%' style='border-right: none;'>Question</th> " +
                                                        "     <th width = '50%' style='border-right: none;'>Answer</th> " +
                                                        "     <th width = '50%' style='border-right: none;'>Options</th> " +
                                                        "  </tr> " +
                                                        "</thead>" +
                                                        "<tbody>";
                                                        

            string bottomSection_HTML = "</tbody>" +
                                        "</table>" +
                                        "</div>" +
                                    " </div>" +
                                " </div>" +
                           " </div> ";


            Literal topSection_Literal = new Literal();
            topSection_Literal.Text = topSection_HTML;

            Literal bottomSection_Literal = new Literal();
            bottomSection_Literal.Text = bottomSection_HTML;

            _controlPlaceHolder.Controls.Add(topSection_Literal);


            // Create the controls on the page.
            foreach (DataRow row in _controlData.Select("sectionReference='" + currentSectionReference + "'"))
            {


                // Assign variables based on the DataTable column position. This may change.
                string currentControlID = row.Field<string>(0);
                //string currentSectionReference = row.Field<string>(1);
                string currentControlType = row.Field<string>(2);
                string currentControlTitle = row.Field<string>(3);
                string currentControlPlaceholder = row.Field<string>(4);
                string currentValue = row.Field<string>(5);
                string description = row.Field<string>(6);
                string customType = row.Field<string>(7);
                string customReference = row.Field<string>(8);
                string commentRequired = row.Field<string>(9);
                string currentComment = row.Field<string>(10);
                string negativeAnswer = row.Field<string>(11);
                string attachRequired = row.Field<string>(12);
                string actionRequired = row.Field<string>(13);
                string pendingCount = row.Field<string>(16);
                string activeCount = row.Field<string>(17);
                string completeCount = row.Field<string>(18);
                string attachCount = row.Field<string>(19);
                bool concernRemoved = row.Field<bool>(20);
                string concernRemovedBy = row.Field<string>(21);
                string concernRemovedOn = row.Field<string>(22);
                string concernComment = row.Field<string>(23);
                bool concernOngoing = row.Field<bool>(24);
                string concernOngoingBy = row.Field<string>(25);
                string concernOngoingOn = row.Field<string>(26);

                string[] negativeAnswer_Array = StringToArray(negativeAnswer, ',');
                string[] currentValue_Array = StringToArray(currentValue, ',');

                bool hasActions = false;
                if (pendingCount != "0" || activeCount != "0" || completeCount != "0") { hasActions = true; }

                string actionTop = "";
                string actionBottom = "";
                string actionColSpan = "1";

                if (hasActions)
                {
                    actionTop = "<th width = '7%' class='text-center'>Task Status</th>";
                    actionColSpan = "2";
                    actionBottom = " <td> " +
                                        "<div class='taskStatGroup'> " +
                                        "  <div class='taskStatPending'>" + pendingCount + "</div> " +
                                        "  <div class='taskStatActive'>" + activeCount + "</div> " +
                                        "  <div class='taskStatComplete'>" + completeCount + "</div> " +
                                    " </div> " +
                                "  </td> ";
                }


                string valueCustomStringName = "";

                //Enum of current type
                QuestionType currentControl = (QuestionType)Enum.Parse(typeof(QuestionType), currentControlType, true);

                // Convert numerical answers to their counterparts
                switch (currentControl)
                {

                    // TextBox - Single
                    case QuestionType.TextBox_Single:
                        valueCustomStringName = Formatting.FormatTextReplaceReturn(currentValue);
                        break;

                    // Textbox - Multi
                    case QuestionType.TextBox_Multi:
                        valueCustomStringName = Formatting.FormatTextReplaceReturn(currentValue);
                        break;

                    // Checkbox
                    case QuestionType.CheckBox:

                        break;

                    // Radio Buttons Y/N/NA
                    case QuestionType.Radio_YNNA:
                    case QuestionType.Radio_YN:

                        if (currentValue == "Y")
                        {
                            valueCustomStringName = "Yes";
                        }
                        else if (currentValue == "N")
                        {
                            valueCustomStringName = "No";
                        }
                        else if (currentValue == "NA")
                        {
                            valueCustomStringName = "Not Applicable";
                        }


                        break;

                    case QuestionType.Custom:
                        // This is for customised dropdowns / radiobuttons


                        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                        {
                            try
                            {

                                SqlDataReader objrs = null;
                                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                                cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                                cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                                cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                                cmd.Parameters.Add(new SqlParameter("@type", "RETURNSELECTEDOPTIONS_CUSTOM"));



                                objrs = cmd.ExecuteReader();

                                if (objrs.HasRows)
                                {
                                    objrs.Read();
                                    string[] currentSelectedOptions = StringToArray(objrs["deliminatedOutput"].ToString(), ',');

                                    int itemCount = 0;

                                    foreach (var item in currentSelectedOptions)
                                    {


                                        if (itemCount == 0)
                                        {
                                            valueCustomStringName = item;
                                        }
                                        else
                                        {
                                            if (item != "")
                                            {
                                                valueCustomStringName += " - " + item;
                                            }

                                        }

                                        itemCount++;

                                    }

                                }
                                else
                                {
                                    //Error - TODO
                                }




                                cmd.Dispose();
                                objrs.Dispose();

                            }
                            catch (Exception)
                            {
                                throw;
                            }
                        }



                        break;

                    default:
                        break;
                }


                // Work out if its negative, if so, we colour the border in red.
                bool isNegative = false;
                string rowColour = "";
                string negativeClass = "";



                foreach (var item in StringToArray(negativeAnswer, ','))
                {

                    if (currentControl == QuestionType.Custom)
                    {
                        foreach (var custItem in StringToArray(currentValue, ','))
                        {

                            if (item == custItem && item != "")
                            {
                                isNegative = true;
                            }

                        }
                    }
                    else
                    {
                        if (item == currentValue)
                        {
                            isNegative = true;
                        }
                    }

                }

                if (isNegative && concernRemoved != true && concernOngoing != true)
                {
                    rowColour = "#e1726e";
                    negativeClass = "negativeAnswer";
                }
                else if (concernRemoved == true)
                {
                    rowColour = "#4E9258";
                    negativeClass = "removedConcern";
                }
                else if (concernOngoing == true)
                {
                    rowColour = "#FFD801";
                    negativeClass = "ongoingConcern";
                }
                else
                {
                    rowColour = "#FAFAFA";
                    negativeClass = "nonnegativeAnswer";
                }


                if (concernRemoved == true)
                {
                    currentControlTitle += "<br /><br />Concern removed by " + UserControls.GetUser(concernRemovedBy, _corpCode) + "  on " + Formatting.FormatDateTime(concernRemovedOn, 1) + "";

                    if (concernComment.Length > 0)
                    {
                        currentControlTitle += " with the reason: <br /> " + concernComment;
                    }

                }

                if (concernOngoing == true)
                {
                    currentControlTitle += "<br /><br />Flagged as ongoing by " + UserControls.GetUser(concernOngoingBy, _corpCode) + "  on " + Formatting.FormatDateTime(concernOngoingOn, 1) + "";

                    if (concernComment.Length > 0)
                    {
                        currentControlTitle += " with the reason: <br /> " + concernComment;
                    }

                }




                Literal addThis = new Literal();

                addThis.Text = "<br />" + currentSectionReference + " - " + currentControlID;

                ///_currentPlaceHolder.Controls.Add(addThis);

                UpdatePanel sectionUpdatePanel = new UpdatePanel();
                sectionUpdatePanel.ID = "sectionUpdatePanel_" + currentSectionReference + "_" + currentControlID;
                sectionUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;


                string mainContainerTop_Html_1 = "  <tr style='background-color: " + rowColour + ";'> " +
                                                 "     <td width = '50%' style='border-right: none;'>" + currentControlTitle + "</td> " +
                                                 "      <td>" + valueCustomStringName +
                                                 "      </td> " +
                                                 "     <td width = '50%' style='border-left: none; text-align:right;'>";

                string mainContainerTop_Html_2 = "   </td> " +
                                                 "</tr>" +
                                                 "<tr>" +
                                                     actionTop +
                                                 " </tr> " +
                                                 "<tr> " +                             
                                                    actionBottom +
                                                "</tr> ";


                Literal mainContainerTop_Literal_1 = new Literal();
                Literal mainContainerTop_Literal_2 = new Literal();
                mainContainerTop_Literal_1.Text = mainContainerTop_Html_1;
                mainContainerTop_Literal_2.Text = mainContainerTop_Html_2;

                UpdatePanel buttonUpdatePanel = new UpdatePanel();
                buttonUpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
                buttonUpdatePanel.ID = "buttonUpdatePanel_" + currentSectionReference + "_" + currentControlID;


                LinkButton remedialActions = new LinkButton();
                remedialActions.CssClass = "btn btn-default actionButton";
                remedialActions.Text = "Manage Actions";
                remedialActions.Click += new EventHandler(actionsButton_Click);
                remedialActions.ID = "remedialActions_" + currentSectionReference + "_" + currentControlID;

                LinkButton removeConcern = new LinkButton();
                removeConcern.CssClass = "btn btn-default concernButton";
                removeConcern.Text = "Remove Concern";
                removeConcern.Click += new EventHandler(removeConcernButton_Click);
                removeConcern.ID = "removeConcern_" + currentSectionReference + "_" + currentControlID;
                removeConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton ongoingConcern = new LinkButton();
                ongoingConcern.CssClass = "btn btn-default concernButton";
                ongoingConcern.Text = "Ongoing Concern";
                ongoingConcern.Click += new EventHandler(ongoingConcernButton_Click);
                ongoingConcern.ID = "ongoingConcern_" + currentSectionReference + "_" + currentControlID;
                ongoingConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton restoreConcern = new LinkButton();
                restoreConcern.CssClass = "btn btn-default concernButton";
                restoreConcern.Text = "Restore Concern";
                restoreConcern.Click += new EventHandler(restoreConcernButton_Click);
                restoreConcern.ID = "restoreConcern_" + currentSectionReference + "_" + currentControlID;
                restoreConcern.Attributes.Add("style", "margin-right:10px;");

                LinkButton manageComments = new LinkButton();
                manageComments.CssClass = "btn btn-default concernButton";
                manageComments.Text = "Add Comment";
                manageComments.Click += new EventHandler(addCommentsButton_Click);
                manageComments.ID = "manageComments_" + currentSectionReference + "_" + currentControlID;
                manageComments.Attributes.Add("style", "margin-right:10px;");


                //if ((_currentAccess == "edit" || _currentAccess == "supervisor" || _currentAccess == "authoriseduser"))
                //{

                //    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(manageComments);

                //    //if (_isPortal == "N")
                //    //{
                if (isNegative && concernRemoved != true && concernOngoing != true)
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(removeConcern);
                }
                if (isNegative && concernOngoing != true && concernRemoved != true)
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(ongoingConcern);
                }
                if (isNegative && (concernRemoved == true || concernOngoing == true))
                {
                    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(restoreConcern);
                }

                //    //    buttonUpdatePanel.ContentTemplateContainer.Controls.Add(remedialActions);
                //    //}

                //}





                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerTop_Literal_1);
                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(buttonUpdatePanel);
                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerTop_Literal_2);

                //_currentPlaceHolder.Controls.Add(buttonUpdatePanel);
                //_currentPlaceHolder.Controls.Add(mainContainerTop_Literal_2);


                //string mainContainerBottom_Html = "</table></div>" +
                //                                   "</div>";

                string mainContainerBottom_Html = "";


                // CG - COMMENTS HERE
                using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                {
                    try
                    {

                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@customReference", customReference));
                        cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                        cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                        cmd.Parameters.Add(new SqlParameter("@type", "RETURNCOMMENTS"));



                        objrs = cmd.ExecuteReader();

                        if (objrs.HasRows)
                        {

                            mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''> " +
                                                       "<div class='col-xs-12'> " +
                                                          " <table class='table table-bordered table-marginless table-assessmentstyle'> " +
                                                               "<thead> " +
                                                                   "<tr class='warning'> " +
                                                                       "<th width = '70%'> Comments </th> " +
                                                                       "<th width = '15%' class='text-center' style='text-align:center !important;'> Added On </ th > " +
                                                                       "<th width = '15%' class='text-center' style='text-align:center !important;'> Added By </ th > " +
                                                                   "</tr> " +
                                                               "</thead> " +
                                                               "<tbody>";


                            while (objrs.Read())
                            {


                                string comment = Formatting.FormatTextInputField(Formatting.FormatTextReplaceReturn(Convert.ToString(objrs["comment"])));
                                string addedBy = UserControls.GetUser(Convert.ToString(objrs["addedBy"]), _corpCode);


                                string addedOnText;

                                if (objrs["addedOn"] == DBNull.Value)
                                {
                                    addedOnText = "Added During Assessment";
                                }
                                else
                                {
                                    addedOnText = Convert.ToDateTime(objrs["addedOn"]).ToShortDateString();
                                }


                                mainContainerBottom_Html += "<tr> " +
                                                                "<td>" + comment + "</td> " +
                                                                "<td class='text-center'>" + addedOnText + "</td> " +
                                                                "<td class='text-center'>" + addedBy + "</td> " +
                                                            "</tr> ";
                            }


                            mainContainerBottom_Html += "</tbody> " +
                                                           "</table> " +
                                                       "</div> " +
                                                   "</div>";

                        }
                        else
                        {
                            //Error - TODO
                        }




                        cmd.Dispose();
                        objrs.Dispose();

                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }



                if (currentComment.Length > 0)
                {

                }


                if (attachCount != "0" && attachCount.Length > 0)
                {

                    //Grab data!
                    DataTable currentAttachments = ASNETNSP.SharedComponants.returnFileData(_corpCode, _recordReference, currentSectionReference + "_" + currentControlID, "");
                    StringBuilder attachments = new StringBuilder();

                    foreach (DataRow item in currentAttachments.Rows)
                    {
                        attachments.AppendLine("<div style='float: left; '>" + ASNETNSP.SharedComponants.returnAttachmentItem(item.Field<string>("filepath"), item.Field<string>("filename"), item.Field<string>("friendlyname"), item.Field<string>("filetype"), "medium", "../../../uploads") + "</div>");
                    }

                    mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''> " +
                                                        "<div class='col-xs-12'> " +
                                                           " <table class='table table-bordered  table-condensed  table-marginless'> " +
                                                                "<thead> " +
                                                                    "<tr class='warning'> " +
                                                                        "<th width = '100%'> Attachments </th > " +
                                                                    "</tr> " +
                                                                "</thead> " +
                                                                "<tbody> " +
                                                                    "<tr> " +
                                                                        "<td>" + attachments.ToString() + "</td> " +
                                                                    "</tr> " +
                                                                "</tbody> " +
                                                            "</table> " +
                                                        "</div> " +
                                                    "</div>";

                    currentAttachments.Dispose();
                }


                if (hasActions)
                {


                    using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {

                            SqlDataReader objrs = null;
                            SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_ActionCommands", dbConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                            cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                            cmd.Parameters.Add(new SqlParameter("@sectionReference", currentSectionReference));
                            cmd.Parameters.Add(new SqlParameter("@questionReference", currentControlID));
                            cmd.Parameters.Add(new SqlParameter("@actionreference", DBNull.Value));
                            cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTION"));


                            objrs = cmd.ExecuteReader();

                            if (objrs.HasRows)
                            {

                                StringBuilder currentActions = new StringBuilder();


                                while (objrs.Read())
                                {
                                    currentActions.AppendLine("<tr>" +
                                                                "<td>" + objrs["task_description"].ToString() + "</td>" +
                                                                "<td align = 'center' >" +
                                                                        "<div class='taskStatGroup'>" +
                                                                        "<div class='taskStat" + ASNETNSP.SharedComponants.returnTaskStatus(objrs["task_status"].ToString(), "") + " NoWidth'>" +
                                                                            ASNETNSP.SharedComponants.returnTaskStatus(objrs["task_status"].ToString(), "") +
                                                                        "</div>" +
                                                                    "</div>         " +
                                                                "</td>" +
                                                                "<td align = 'center' ><strong > " + ASNETNSP.Formatting.FormatDateTime(objrs["action_due_date"], 1) + " </strong ><br /> " + ASNETNSP.Formatting.FormatFullNameDotSurname(objrs["Per_fname"], objrs["Per_sname"]) + "</td>" +
                                                            " </tr>");
                                }



                                mainContainerBottom_Html += "<div class='row otherInformationAudit' style=''>" +
                                                                                "<div class='col-xs-12'>" +
                                                                                    "<table class='table table-bordered table-marginless'>" +
                                                                                        "<thead>" +
                                                                                            "<tr class='active'>" +
                                                                                                "<th width = '70%' class='warning'>Action" +
                                                                                                "</th>" +
                                                                                                "<th width = '15%' class='warning text-center'>Current Status</th>" +
                                                                                                "<th width = '15%;' class='warning text-center'>Due Date / By Whom</th>" +
                                                                                            "</tr>" +
                                                                                        "</thead>" +
                                                                                        "<tbody>" +
                                                                                            currentActions.ToString() +
                                                                                        "</tbody>" +
                                                                                    "</table>" +
                                                                                "</div>" +
                                                                           " </div>";




                            }


                            cmd.Dispose();
                            objrs.Dispose();

                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }




                mainContainerBottom_Html += "</div>" +
                                                    "</div>" +
                                                    "</div>" +
                                                    "</div>" +
                                                    "</div>";

                //Mark Debuging
                mainContainerBottom_Html = "";

                Literal mainContainerBottom_Literal = new Literal();
                mainContainerBottom_Literal.Text = mainContainerBottom_Html;


                //_currentPlaceHolder.Controls.Add(mainContainerBottom_Literal);



                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(mainContainerBottom_Literal);


                sectionUpdatePanel.ContentTemplateContainer.Controls.Add(bottomSection_Literal);

                _controlPlaceHolder.Controls.Add(sectionUpdatePanel);
                //_currentPlaceHolder.Controls.Add(bottomSection_Literal);

            }





            // End Panel.


        }


    }


    private void InitiateBottomButtons()
    {

        PlaceHolder button_PlaceHolder = (PlaceHolder)_controlPlaceHolder.Page.FindControl("button_PlaceHolder");
        button_PlaceHolder.Controls.Clear();

        // We create an updatepanel that encapsulates the buttons as a safeguard for postbacks (which was an issue - might not be anymore!)
        UpdatePanel new_UpdatePanel = new UpdatePanel();
        new_UpdatePanel.UpdateMode = UpdatePanelUpdateMode.Conditional;
        new_UpdatePanel.ID = "new_UpdatePanel";

        LiteralControl startRow = new LiteralControl();
        startRow.Text = "<div class='col-xs-12'> " +
                            "<div class='row text-center'>" + "<div class='col-xs-12' >";


        // Page Validation handler
        CustomValidator new_CustomValidator = new CustomValidator();
        new_CustomValidator.ID = "pageValidator";
        new_CustomValidator.ValidationGroup = "page";
        new_CustomValidator.Display = ValidatorDisplay.Dynamic;
        new_CustomValidator.ClientValidationFunction = "ValidatePage";

        new_UpdatePanel.ContentTemplateContainer.Controls.Add(startRow);
        new_UpdatePanel.ContentTemplateContainer.Controls.Add(new_CustomValidator);


        // Create a HTML element for the bottom of the control.
        LiteralControl endRow = new LiteralControl();
        endRow.Text = " </div></div>" +
                    "  </div> ";




        // Run sproc that returns which buttons should be shown.
        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_SectionCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                cmd.Parameters.Add(new SqlParameter("@type", "BUTTONS"));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    string showNextButton = "0";
                    string showPreviousButton = "0";
                    string showFinalButton = "0";

                    showNextButton = objrs["showNext"].ToString();
                    showPreviousButton = objrs["showPrevious"].ToString();
                    showFinalButton = objrs["showFinal"].ToString();


                    if (showPreviousButton == "1")
                    {
                        Button previousButton = new Button();
                        previousButton.ID = "previousButton";
                        previousButton.CssClass = "btn btn-primary btn-extramargin";
                        previousButton.Text = "Previous Section";
                        previousButton.Click += new EventHandler(previous_Click); 
                        previousButton.UseSubmitBehavior = false;
                        //previousButton.ValidationGroup = "page";

                        new_UpdatePanel.ContentTemplateContainer.Controls.Add(previousButton);
                    }

                    if (showNextButton == "1")
                    {
                        Button nextButton = new Button();
                        nextButton.ID = "nextButton";
                        nextButton.CssClass = "btn btn-primary btn-extramargin";
                        nextButton.Text = "Next Section";
                        nextButton.Click += new EventHandler(next_Click); 
                        nextButton.UseSubmitBehavior = false;
                        nextButton.ValidationGroup = "page";

                        new_UpdatePanel.ContentTemplateContainer.Controls.Add(nextButton);
                    }

                    if (showFinalButton == "1")
                    {
                        Button finishButton = new Button();
                        finishButton.ID = "finishButton";
                        finishButton.CssClass = "btn btn-primary btn-extramargin";
                        finishButton.Text = "Finish Assessment";
                        finishButton.Click += new EventHandler(finish_Click);
                        finishButton.ValidationGroup = "page";

                        new_UpdatePanel.ContentTemplateContainer.Controls.Add(finishButton);
                    }


                }
                else
                {
                    //Error - TODO
                }


                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + _recordReference + "')", true);

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }



        new_UpdatePanel.ContentTemplateContainer.Controls.Add(endRow);

        button_PlaceHolder.Controls.Add(new_UpdatePanel);

    }

    //private void InitiateFinalButton()
    //{
    //    Button completeAssessment_Button = (Button)_controlPlaceHolder.Page.FindControl("completeAssessment_Button");
    //    completeAssessment_Button.Click += new EventHandler(completeAssessment_Click);
    //}



    //Process the data (Internal)
    private void NextSection()
    {
        //HiddenField sectionReference_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("sectionReference_HiddenField");

        Label sectionReference_Label_v2 = (Label)_dseAssessment_UserControl.FindControl("sectionReference_Label_v2");
        HiddenField sectionReference_HiddenField_v2 = (HiddenField)_dseAssessment_UserControl.FindControl("sectionReference_HiddenField_v2");
        UpdateView();

        //Save the Data!
        SaveData();

        //Clear the current items.
        ClearControls();

        //Grab the next reference and initiate
        string sectionReference = "";


        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_SectionCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                cmd.Parameters.Add(new SqlParameter("@type", "NEXT"));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    sectionReference = objrs["section_reference"].ToString();
                    _sectionReference = sectionReference;
                }
                else
                {
                    //Error - TODO
                }

                sectionReference_Label_v2.Text = sectionReference;
                sectionReference_HiddenField_v2.Value = sectionReference;

                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true); 

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }


        InitiateControls();

        //Update
        UpdateView();

        StringBuilder script = new StringBuilder();
        script.AppendLine("$('html, body').animate({ scrollTop: 0 }, 100);");

        ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", script.ToString(), true);

    }

    private void PreviousSection()
    {
        Label sectionReference_Label_v2 = (Label)_dseAssessment_UserControl.FindControl("sectionReference_Label_v2");
        HiddenField sectionReference_HiddenField_v2 = (HiddenField)_dseAssessment_UserControl.FindControl("sectionReference_HiddenField_v2");

        string sectionReference = "";

        //Save the Data!
        SaveData();

        //Clear the current items.
        ClearControls();

        //Grab the previous reference and initiate
        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_SectionCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                cmd.Parameters.Add(new SqlParameter("@type", "PREVIOUS"));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    sectionReference = objrs["section_reference"].ToString();
                    _sectionReference = sectionReference;
                }
                else
                {
                    //Error - TODO
                }

                sectionReference_Label_v2.Text = sectionReference;
                sectionReference_HiddenField_v2.Value = sectionReference;

                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true);

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }




        InitiateControls();

        //Update
        UpdateView();
    }

    private void FinishAssessment() 
    {
        //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script12hfggfhfghfghg3", "alert('We made it to the end');", true); 
        //Save the Data!
        SaveData();

        if (_status != "2")
        {
            //Initilise the finilise assessment modal
            //ReturnFinalAssessment();
            HttpContext.Current.Response.Redirect("dseSummary.aspx?templateref=" + _templateID + "&ref=" + _recordReference);
        }
        else
        {
            //if its signoff, then we take them back to the report (they would have edited)
            HttpContext.Current.Response.Redirect("dseSummary.aspx?templateref=" + _templateID + "&ref=" + _recordReference);
        }



    }

    private void ClearControls()
    {
        _controlPlaceHolder.Controls.Clear();
    }


    private void ReturnFinalAssessment() 
    {
        UpdatePanel finalModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("finalModal_UpdatePanel");
        HiddenField superviseremails = (HiddenField)_controlPlaceHolder.Page.FindControl("superviseremails");
        HtmlGenericControl showEmailSection = (HtmlGenericControl)_controlPlaceHolder.Page.FindControl("showEmailSection");

        // We will also need to work out if we are sending their supervisers an email,
        //    or if we let the user select their own email address for a review.

        // work out if the user has any supervisers with an email.


        string ListOfSupervisers = UserControls.ReturnUserSuperviserEmails(_currentUser, _corpCode);
        bool requiredSignOff = ReturnIfSignOffRequired("RequiresSignOff");

        if (ListOfSupervisers == "" && requiredSignOff)
        {
            // No Supervisers
        }
        else
        {
            //Has supervisers
            superviseremails.Value = ListOfSupervisers;
            showEmailSection.Visible = false;

        }


        finalModal_UpdatePanel.Update();

        ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script", "$('#finalModal').modal('show');", true);
    }

    private void FinishAndRedirect()
    {

        TextBox peopleEmail = (TextBox)_controlPlaceHolder.Page.FindControl("peopleEmail");
        DropDownList domainList = (DropDownList)_controlPlaceHolder.Page.FindControl("domainList");
        HtmlGenericControl alertArea = (HtmlGenericControl)_controlPlaceHolder.Page.FindControl("alertArea");
        Literal alertAreaMsg = (Literal)_controlPlaceHolder.Page.FindControl("alertAreaMsg");
        HiddenField superviseremails = (HiddenField)_controlPlaceHolder.Page.FindControl("superviseremails");

        bool requiresSignOff = ReturnIfSignOffRequired("RequiresSignOff");

        string emailprefix = "";
        string emailsuffix = "";
        string emailaddress = "";


        string supervisorList = superviseremails.Value;

        if (supervisorList != "")
        {

        }
        else
        {
            emailprefix = peopleEmail.Text;
            emailsuffix = domainList.SelectedValue;
            emailaddress = emailprefix + "@" + domainList.SelectedValue;
            alertArea.Visible = false;
            alertArea.Attributes.Add("class", "alert alert-danger");
        }




        if (Messages.IsValidEmail(emailaddress) || supervisorList != "" || !requiresSignOff)
        {

            //Save comments
            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    TextBox finalComments_TextBox = (TextBox)_controlPlaceHolder.Page.FindControl("finalComments");
                    string finalComments = Formatting.FormatTextInput(finalComments_TextBox.Text);

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_Finish", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                    cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                    cmd.Parameters.Add(new SqlParameter("@comment", finalComments));
                    cmd.Parameters.Add(new SqlParameter("@acc_ref", _currentUser));

                    if (supervisorList != "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@email", supervisorList));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@email", emailaddress));
                    }


                    objrs = cmd.ExecuteReader();

                    cmd.Dispose();
                    objrs.Dispose();

                }
                catch (Exception)
                {
                    throw;
                }
            }

            // Redirect

            HttpContext.Current.Response.Redirect("dseReport.aspx?templateref=" + _templateID + "&ref=" + _recordReference);
        }
        else
        {
            alertArea.Visible = true;
            alertAreaMsg.Text = "The email address you provided is not valid, please check and try again.";
        }
    }


    private void removeConcernButton_Click(object sender, EventArgs e)
    {

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string[] actionReference = StringToArray(currentButton.ID.Replace("removeConcern_", ""), '_');

        string questionReference = actionReference[1];
        string sectionReference = actionReference[0];

        // Generate current actions
        ReturnRemoveConcern(questionReference, sectionReference);
    }

    private void ongoingConcernButton_Click(object sender, EventArgs e)
    {
        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string[] actionReference = StringToArray(currentButton.ID.Replace("ongoingConcern_", ""), '_');

        string questionReference = actionReference[1];
        string sectionReference = actionReference[0];

        // Generate current actions
        ReturnOngoingConcern(questionReference, sectionReference);
    }

    private void restoreConcernButton_Click(object sender, EventArgs e) 
    {

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string[] actionReference = StringToArray(currentButton.ID.Replace("restoreConcern_", ""), '_');

        string questionReference = actionReference[1];
        string sectionReference = actionReference[0];

        // Generate current actions
        ReturnRestoreConcern(questionReference, sectionReference);
    }

    private void addCommentsButton_Click(object sender, EventArgs e)
    {

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string[] actionReference = StringToArray(currentButton.ID.Replace("manageComments_", ""), '_');

        string questionReference = actionReference[1];
        string sectionReference = actionReference[0];

        // Generate current actions
        ReturnAddComment(questionReference, sectionReference);
    }

    private void ReturnRemoveConcern(string questionReference, string sectionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField concern_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("concern_SectionReference_HiddenField");
        HiddenField concern_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("concern_QuestionReference_HiddenField");

        UpdatePanel RemoveConcernModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("RemoveConcernModal_UpdatePanel");


        concern_QuestionReference_HiddenField.Value = questionReference;
        concern_SectionReference_HiddenField.Value = sectionReference;


        RemoveConcernModal_UpdatePanel.Update();


        ScriptManager.RegisterStartupScript(concern_SectionReference_HiddenField.Page, this.GetType(), "script", "$('#removeConcernModal').modal('show');", true);

    }

    private void ReturnOngoingConcern(string questionReference, string sectionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField ongoing_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("ongoing_SectionReference_HiddenField");
        HiddenField ongoing_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("ongoing_QuestionReference_HiddenField");

        UpdatePanel OngoingConcernModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("OngoingConcernModal_UpdatePanel");


        ongoing_QuestionReference_HiddenField.Value = questionReference;
        ongoing_SectionReference_HiddenField.Value = sectionReference;


        OngoingConcernModal_UpdatePanel.Update();


        ScriptManager.RegisterStartupScript(ongoing_SectionReference_HiddenField.Page, this.GetType(), "script", "$('#ongoingConcernModal').modal('show');", true);

    }

    private void ReturnRestoreConcern(string questionReference, string sectionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField restore_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("restore_SectionReference_HiddenField");
        HiddenField restore_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("restore_QuestionReference_HiddenField");

        UpdatePanel RestoreConcernModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("RestoreConcernModal_UpdatePanel");


        restore_QuestionReference_HiddenField.Value = questionReference;
        restore_SectionReference_HiddenField.Value = sectionReference;


        RestoreConcernModal_UpdatePanel.Update();


        ScriptManager.RegisterStartupScript(restore_SectionReference_HiddenField.Page, this.GetType(), "scriptasdasdasd", "$('#restoreConcernModal').modal('show');", true);

    }

    private void ReturnAddComment(string questionReference, string sectionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField comment_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_SectionReference_HiddenField");
        HiddenField comment_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_QuestionReference_HiddenField");

        UpdatePanel AddCommentModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("AddCommentModal_UpdatePanel");


        comment_SectionReference_HiddenField.Value = questionReference;
        comment_QuestionReference_HiddenField.Value = sectionReference;


        AddCommentModal_UpdatePanel.Update();


        ScriptManager.RegisterStartupScript(comment_SectionReference_HiddenField.Page, this.GetType(), "script", "$('#addCommentModal').modal('show');", true);

    }


    //Public Functionality

    public void SaveData()
    {


        try
        {
            //TextBoxes
            var textBox_List = GetAllControls(_controlPlaceHolder, typeof(TextBox));

            //Get information of the text box and add it to the datatable.
            foreach (var control in textBox_List)
            {
                TextBox currentControl = (TextBox)control;

                string currentID = currentControl.ID.Replace("control_", "");
                string currentValue = currentControl.Text;

                if (currentID.Contains("comment"))
                {

                    currentID = currentID.Replace("_comment", "");
                    DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                    row["comment"] = currentValue;

                }
                else
                {
                    // update datatable
                    DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                    row["currentValue"] = currentValue;
                }



            }


            //Checkboxes
            try
            {
                var CheckBox_List = GetAllControls(_controlPlaceHolder, typeof(CheckBox));
                foreach (var control in CheckBox_List)
                {

                    CheckBox currentControl = (CheckBox)control;

                    string currentID = currentControl.ID.Replace("control_", "");
                    currentID = currentID.Substring(0, currentID.LastIndexOf("_") + 0);

                    string currentValue = "";

                    if (currentControl.Checked)
                    {
                        currentValue = currentControl.ID.Replace("control_" + currentID + "_", "") + ",";
                    }

                    // update datatable
                    DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();

                    //Logic to ensure delimation doesn't duplicate.
                    if (row["currentValue"].ToString().Contains("C"))
                    {

                    }
                    else
                    {
                        row["currentValue"] = "C";
                    }

                    row["currentValue"] = row["currentValue"].ToString() + currentValue;

                }
            }
            catch
            {
                throw;
            }

            // Ok so we have done the checkboxes, lets remove any instace of C from them.
            // This selects custom type that are 3 (checkbox) or those that are just checkbox.
            var checkbox_Rows = _controlData.Select("(type=5 and custom_type=3) or (type=3)");
            foreach (var row in checkbox_Rows)
            {
                row["currentValue"] = row["currentValue"].ToString().Remove(0, 1);
            }



            //Get information of the text box and add it to the datatable.



            //Radio Boxes
            var Radio_List = GetAllControls(_controlPlaceHolder, typeof(RadioButton));

            foreach (var control in Radio_List)
            {
                RadioButton currentControl = (RadioButton)control;

                if (currentControl.Checked)
                {
                    string currentID = currentControl.ID.Replace("control_", "");
                    currentID = currentID.Substring(0, currentID.LastIndexOf("_") + 0);

                    string currentValue = currentControl.ID.Replace("control_" + currentID + "_", "");


                    // update datatable
                    DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                    row["currentValue"] = currentValue;


                }


                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), Guid.NewGuid().ToString(), "console.log(\"" + currentControl.ID + "\");", true);

            }

            //Dropdown Lists
            var DropDown_List = GetAllControls(_controlPlaceHolder, typeof(DropDownList));
            foreach (var control in DropDown_List)
            {
                DropDownList currentControl = (DropDownList)control;

                string currentID = currentControl.ID.Replace("control_", "");
                string currentValue = currentControl.SelectedValue;

                // update datatable
                DataRow row = _controlData.Select("questionReference='" + currentID + "'").FirstOrDefault();
                row["currentValue"] = currentValue;

            }




            // Now that the datatable has been saved, we need to iterate through
            // the rows and save them. The results will be flushed at a later stage
            // so it doesn't need to do it here.


            using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
            {
                try
                {


                    foreach (DataRow row in _controlData.Select("sectionReference='" + _sectionReference + "'"))
                    {


                        // Assign variables based on the DataTable column position. This may change.
                        string currentQuestionReference = row.Field<string>(0);
                        string currentSectionReference = row.Field<string>(1);
                        string currentValue = row.Field<string>(5);
                        string currentComment = row.Field<string>(10);



                        SqlDataReader objrs = null;
                        SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_UpdateQuestion", dbConn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                        cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                        cmd.Parameters.Add(new SqlParameter("@currentSection", currentSectionReference));
                        cmd.Parameters.Add(new SqlParameter("@currentQuestion", currentQuestionReference));
                        cmd.Parameters.Add(new SqlParameter("@answer", Formatting.FormatTextInput(currentValue)));
                        cmd.Parameters.Add(new SqlParameter("@comment", Formatting.FormatTextInput(currentComment)));

                        objrs = cmd.ExecuteReader();

                        cmd.Dispose();
                        objrs.Dispose();

                    }
                }
                catch
                {
                    throw;
                }


            }
        }
        catch
        {
            throw;
        }




    }

    override public string ToString()
    {
        StringBuilder returnValue = new StringBuilder();

        foreach (DataRow row in _controlData.Rows)
        {
            returnValue.AppendLine(String.Format("<br />ID: {0}, Current Value: {1} <br />", row.Field<string>(0).ToString(), row.Field<string>(5).ToString()));
        }

        return returnValue.ToString();
    }

    public void UpdateView()
    {
        _controlUpdatePanel.Update();
    }

    public string ReturnFirstSectionReference()
    {

        string sectionReference = "";

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_SectionCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                cmd.Parameters.Add(new SqlParameter("@currentSection", ""));
                cmd.Parameters.Add(new SqlParameter("@type", "FIRST"));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    sectionReference = objrs["section_reference"].ToString();
                }
                else
                {
                    //Error - TODO
                }

                Label sectionReference_Label_v2 = (Label)_dseAssessment_UserControl.FindControl("sectionReference_Label_v2"); 
                HiddenField sectionReference_HiddenField_v2 = (HiddenField)_dseAssessment_UserControl.FindControl("sectionReference_HiddenField_v2");

                sectionReference_Label_v2.Text = sectionReference;
                sectionReference_HiddenField_v2.Value = sectionReference;

                //ScriptManager.RegisterStartupScript(_controlPlaceHolder.Page, this.GetType(), "script123", "alert('" + sectionReference + "')", true);

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return sectionReference;

    }

    public string ReturnCurrentSectionTitle()
    {

        string sectionTitle = "";

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_SectionCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@template_id", _templateID));
                cmd.Parameters.Add(new SqlParameter("@currentSection", _sectionReference));
                cmd.Parameters.Add(new SqlParameter("@type", "TITLE"));


                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    sectionTitle = objrs["section_title"].ToString();
                }
                else
                {
                    //Error - TODO
                }


                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return sectionTitle;
    }




    // Return string for CSS for actions and Attachments
    public static string ReturnCSSClassForActions(string corpCode, string recordReference, string sectionReference, string questionReference)
    {

        int[] actionCountArray = ReturnActionCount(corpCode, recordReference, sectionReference, questionReference);

        int actionCount = actionCountArray[0];
        int actionRequired = actionCountArray[1];

        string cssClass;

        //Action Required, 0 means do not show, 1 is forced, 2 is optional

        switch (actionRequired)
        {
            case 0:
                cssClass = "btn btn-primary pull-right";

                break;

            case 1:

                if (actionCount == 0)
                {
                    cssClass = "btn btn-danger pull-right";
                }
                else
                {
                    cssClass = "btn btn-success pull-right";
                }

                break;

            case 2:
                cssClass = "btn btn-primary pull-right";
                break;


            default:
                cssClass = "btn btn-primary pull-right";
                break;
        }



        return cssClass;
    }

    public static string ReturnCSSClassForAttachment(string corpCode, string recordReference, string sectionReference, string questionReference) 
    {
        int[] attachCountArray = ReturnAttachmentCount(corpCode, recordReference, sectionReference, questionReference);

        int attachCount = attachCountArray[0];
        int attachRequired = attachCountArray[1];

        string cssClass;

        //Action Required, 0 means do not show, 1 is forced, 2 is optional

        switch (attachRequired)
        {
            case 0:
                cssClass = "btn btn-default pull-right";

                break;

            case 2:

                if (attachCount == 0)
                {
                    cssClass = "btn btn-danger pull-right";
                }
                else
                {
                    cssClass = "btn btn-success pull-right";
                }

                break;

            case 1:
                cssClass = "btn btn-default pull-right";
                break;


            default:
                cssClass = "btn btn-default pull-right";
                break;
        }

        if(attachCount != 0)
        {
            cssClass = "btn btn-success pull-right";
        }

        return cssClass;
    }
    public static string ReturnCSSClassForComment(string corpCode, string recordReference, string sectionReference, string questionReference)
    {
        int[] commentCountArray = ReturnCommentCount(corpCode, recordReference, sectionReference, questionReference);

        int commentCount = commentCountArray[0];
        int commentRequired = commentCountArray[1];

        string cssClass;

        //Action Required, 0 means do not show, 1 is forced, 2 is optional

        switch (commentRequired)
        {
            case 0:
                cssClass = "btn btn-default pull-right";
                break;

            

            case 1:
                cssClass = commentCount == 0 ? "btn btn-danger pull-right" : "btn btn-success pull-right";
                break;

            case 2:
                cssClass = "btn btn-default pull-right";
                break;


            default:
                cssClass = "btn btn-default pull-right";
                break;
        }

        if(commentCount != 0)
        {
            cssClass = "btn btn-success pull-right";
        }

        cssClass += " commentButton";

        return cssClass;
    }

    public static int[] ReturnActionCount(string corpCode, string recordReference, string sectionReference, string questionReference)
    {

        int[] returnValues = new int[2];

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@type", "RETURNACTIONCOUNT"));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", sectionReference));
                cmd.Parameters.Add(new SqlParameter("@questionReference", questionReference));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {

                    objrs.Read();

                    returnValues[0] = Convert.ToInt32(objrs["actionCount"]);
                    returnValues[1] = Convert.ToInt32(objrs["action_required"]);
                }

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnValues;

    }
    public static int[] ReturnAttachmentCount(string corpCode, string recordReference, string sectionReference, string questionReference)
    {

        int[] returnValues = new int[2];

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@type", "RETURNFILECOUNT"));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", sectionReference));
                cmd.Parameters.Add(new SqlParameter("@questionReference", questionReference));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {

                    objrs.Read();

                    returnValues[0] = Convert.ToInt32(objrs["fileCount"]);
                    returnValues[1] = Convert.ToInt32(objrs["attachRequired"]);
                }

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnValues;

    }

    public static int[] ReturnCommentCount(string corpCode, string recordReference, string sectionReference, string questionReference)
    {

        int[] returnValues = new int[2];

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", recordReference));
                cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@type", "RETURNCOMMENTCOUNT"));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", sectionReference));
                cmd.Parameters.Add(new SqlParameter("@questionReference", questionReference));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {

                    objrs.Read();

                    returnValues[0] = Convert.ToInt32(objrs["commentCount"]);
                    returnValues[1] = Convert.ToInt32(objrs["commentRequired"]);
                }

                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnValues;

    }

    public static void RecalculateActionCount(string corpCode, string recordReference, string sectionReference, string questionReference, Control onPage)
    {
        string cssClass = ReturnCSSClassForActions(corpCode, recordReference, sectionReference, questionReference);
        ScriptManager.RegisterStartupScript(onPage.Page, onPage.GetType(), "recalucateActionCount", "RecalculateAction('" + questionReference + "', '" + cssClass + "');", true);
    }


    //Button Handlers
    private void next_Click(object sender, EventArgs e)
    {
        NextSection();
    }

    private void previous_Click(object sender, EventArgs e)
    {
        PreviousSection();
    }

    private void finish_Click(object sender, EventArgs e)
    {
        FinishAssessment();
    }

    private void actionsButton_Click(object sender, EventArgs e)
    {

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string actionQuestionReference = currentButton.ID.Replace("actions_", "");

        //SaveData();

        // Generate current actions
        ReturnAssociatedActions(actionQuestionReference);
    }
    private void attachmentButton_Click(object sender, EventArgs e)
    {

        //SaveData();

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string attachQuestionReference = currentButton.ID.Replace("attach_", "");

        // Generate current actions
        ReturnAssociatedAttachments(attachQuestionReference);
    }

    private void commentButton_Click(object sender, EventArgs e)
    {

        // Workout the question reference based on our selection
        LinkButton currentButton = (LinkButton)sender;
        string commentQuestionReference = currentButton.ID.Replace("comment_", "");

        // Generate current actions
        ReturnAssociatedComments(commentQuestionReference);
    }

    private void completeAssessment_Click(object sender, EventArgs e)
    {

        FinishAndRedirect();

    }



    //Methods relating to actions
    private void ReturnAssociatedActions(string questionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField action_QuestionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("action_QuestionReference_HiddenField");
        HiddenField action_SectionReference_HiddenField = (HiddenField)_controlPlaceHolder.Page.FindControl("action_SectionReference_HiddenField");
        HiddenField action_CurrentProcess_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("action_CurrentProcess_HiddenField");
        HiddenField action_ActionCount_HiddenField = (HiddenField)_controlPlaceHolder.FindControl("action_ActionCount_HiddenField");

        Label action_QuestionReference_Label = (Label)_controlPlaceHolder.Page.FindControl("action_QuestionReference_Label");
        Label action_SectionReference_Label = (Label)_controlPlaceHolder.Page.FindControl("action_SectionReference_Label");
        Label action_ActionCount_Label = (Label)_controlPlaceHolder.Page.FindControl("action_ActionCount_Label");

        UpdatePanel taskInformation_Modal = (UpdatePanel)_controlPlaceHolder.Page.FindControl("taskInformation_Modal");


        action_QuestionReference_HiddenField.Value = questionReference;
        action_SectionReference_HiddenField.Value = _sectionReference;

        action_CurrentProcess_HiddenField.Value = "0";

        action_ActionCount_HiddenField.Value = TaskPopUp.ReturnActionCount(_corpCode, _recordReference, _sectionReference, questionReference).ToString();
        //action_ActionCount_Label.Text = action_ActionCount_HiddenField.Value;


        //action_QuestionReference_Label.Text = questionReference;
        //action_SectionReference_Label.Text = _sectionReference;

        taskInformation_Modal.Update();


        ScriptManager.RegisterStartupScript(action_CurrentProcess_HiddenField.Page, this.GetType(), "script", "$('#newTaskModal').modal('show');", true);

    }

    private void ReturnAssociatedAttachments(string questionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField attachment_SectionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_SectionReference");
        HiddenField attachment_QuestionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_QuestionReference");
        HiddenField attachment_Type = (HiddenField)_controlPlaceHolder.Page.FindControl("attachment_Type");

        UpdatePanel attachmentsModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("attachmentsModal_UpdatePanel");


        var filteredQuestion = _controlData.Select(@"questionReference = '" + questionReference + "'").FirstOrDefault();
        attachment_Type.Value = filteredQuestion.Field<string>(12);


        attachment_SectionReference.Value = _sectionReference;
        attachment_QuestionReference.Value = questionReference;

        attachmentsModal_UpdatePanel.Update();


        ScriptManager.RegisterStartupScript(attachment_SectionReference.Page, this.GetType(), "script", "$('#attachmentsModal').modal('show');", true);

    }

    private void ReturnAssociatedComments(string questionReference)
    {

        // the tasks pop up initlises every postback, so we just have to change the hiddenfield values so the object works correctly.
        HiddenField comment_SectionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_SectionReference");
        HiddenField comment_QuestionReference = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_QuestionReference");
        HiddenField comment_Type = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_Type");
        HiddenField comment_CurrentStatus = (HiddenField)_controlPlaceHolder.Page.FindControl("comment_CurrentStatus");

        UpdatePanel commentsModal_UpdatePanel = (UpdatePanel)_controlPlaceHolder.Page.FindControl("commentsModal_UpdatePanel");


        var filteredQuestion = _controlData.Select(@"questionReference = '" + questionReference + "'").FirstOrDefault();
        comment_Type.Value = filteredQuestion.Field<string>(12);


        comment_SectionReference.Value = _sectionReference;
        comment_QuestionReference.Value = questionReference;
        comment_CurrentStatus.Value = "1";

        commentsModal_UpdatePanel.Update();


        // Trigger comments for what has already been selected so no css classes are lost.



        ScriptManager.RegisterStartupScript(comment_SectionReference.Page, this.GetType(), "script", "$('#commentsModal').modal('show');triggerCommentsCodeBehind('" + questionReference + "');", true);

    }



    private bool ReturnIfSignOffRequired(string type)
    {

        bool returnValue = false;

        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_DSEV10.dbo.Assessment_CustomCommands", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@recordreference", _recordReference));
                cmd.Parameters.Add(new SqlParameter("@customReference", DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@type", "ISSIGNOFFREQUIRED"));
                cmd.Parameters.Add(new SqlParameter("@sectionReference", DBNull.Value));
                cmd.Parameters.Add(new SqlParameter("@questionReference", DBNull.Value));

                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();

                    if (type == "RequiresSignOff")
                    {
                        returnValue = Convert.ToBoolean(objrs["requiresSignOff"]);
                    }
                    else if (type == "SendToSupervisor")
                    {
                        returnValue = Convert.ToBoolean(objrs["send_to_superviser"]);
                    }

                }


                cmd.Dispose();
                objrs.Dispose();

            }
            catch (Exception)
            {
                throw;
            }
        }

        return returnValue;
    }



    //Other

    //StackOverflow reference https://stackoverflow.com/questions/3419159/how-to-get-all-child-controls-of-a-windows-forms-form-of-a-specific-type-button
    //This returns all controls that are a child of the parent control of a specific type. Does this recursively too!!
    public IEnumerable<Control> GetAllControls(Control control, Type type)
    {
        var controls = control.Controls.Cast<Control>();

        return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                  .Concat(controls)
                                  .Where(c => c.GetType() == type);
    }
    private Control ReturnRequiredComment(Type type, string currentControlID, string commentRequired, string currentComment, bool currentNegativeAnswer)
    {
        StringBuilder script = new StringBuilder();
        LiteralControl startHtml = new LiteralControl();
        LiteralControl endHtml = new LiteralControl();
        PlaceHolder comment_PlaceHolder = new PlaceHolder();

        //comment_PlaceHolder.Controls.Add(new LiteralControl(commentRequired));

        // We know that a comment is required, and if they have a current comment.
        // If there is a current comment, we show the comment regardless.
        try
        {
            if (commentRequired == "1" || commentRequired == "2" || currentComment.Length > 0 || commentRequired == "3")
            {

                // 1 : Force Comment - always visible
                // 2 : Comment only shows on negative answer - only visible when answered negatively


                //TextBox comment_TextBox = new TextBox();
                //comment_TextBox.ID = "control_" + currentControlID + "_comment";
                //comment_TextBox.Attributes.Add("placeholder", "Please enter a comment...");
                //comment_TextBox.CssClass = "form-control";
                //comment_TextBox.TextMode = TextBoxMode.MultiLine;
                //comment_TextBox.Rows = 5;
                //comment_TextBox.Text = currentComment;

                if (commentRequired == "2" || commentRequired == "3")
                {
                    //Hide and show.
                    if (currentNegativeAnswer)
                    {
                        //Show!
                        //startHtml.Text = "<div class='comment'><h5>Required Comments:</h5>";
                    }
                    else if (commentRequired == "2")
                    {
                        //Do not show. It will only show when a person explicitally selects a negative answer.
                        //startHtml.Text = "<div class='comment' style='display:none'><h5>Required Comments:</h5>";
                    }
                    else if (commentRequired == "3")
                    {
                        //Do not show. It will only show when a person explicitally selects a negative answer.
                        if (currentComment.Length > 0)
                        {
                            //startHtml.Text = "<div class='comment'><h5>Comments:</h5>";
                        }
                        else
                        {
                            //startHtml.Text = "<div class='comment' style='display:none'><h5>Comments:</h5>";
                        }


                    }

                }
                else
                {
                    //startHtml.Text = "<div class='comment'><h5>Required Comments:</h5>";
                }


                endHtml.Text = "</div>";

                //comment_PlaceHolder.Controls.Add(startHtml);
                //comment_PlaceHolder.Controls.Add(comment_TextBox);
                //comment_PlaceHolder.Controls.Add(endHtml);

            }
        }
        catch
        {
            throw;
        }



        return comment_PlaceHolder;
    }

    private string[] StringToArray(string input, char deliminator)
    {
        string[] returnArray = input.Split(deliminator);

        return returnArray;

    }


    //Enums
    enum QuestionType
    {
        TextBox_Single = 1,
        TextBox_Multi = 2,
        CheckBox = 3,
        Radio_YNNA = 4,
        Custom = 5,
        Radio_YN = 6


    };

    enum CustomQuestionType
    {
        Radio = 2,
        DropDown = 1,
        Checkbox = 3

    };

    public enum Status
    {
        Edit = 0,
        Report = 1
    }

}

