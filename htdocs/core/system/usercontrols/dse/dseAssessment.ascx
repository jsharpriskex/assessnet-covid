﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="dseAssessment.ascx.cs" Inherits="dseAssessment_UserControl" %>


<asp:HiddenField ID="currentIDOfdseQuestion" runat="server" />

<asp:UpdatePanel ID="dseAssessment_UpdatePanel" runat="server" UpdateMode="Conditional" ViewStateMode="Enabled">
    <ContentTemplate>

        <asp:HiddenField ID="sectionReference_HiddenField_v2" value="" runat="server"></asp:HiddenField>
        <asp:Label ID="sectionReference_Label_v2" runat="server" Visible="False"></asp:Label>

        <div class="row">
            <div class="col-xs-12">
                    <asp:CustomValidator runat="server" ID="dseAssessment_Validator" ValidationGroup="dseAssessment" ClientValidationFunction="ValidateAssessmentControl"></asp:CustomValidator>
                    <asp:PlaceHolder ID="dseAssessment_PlaceHolder" runat="server">

                    </asp:PlaceHolder>
            </div>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>


<!-- alert -->
<div class="modal fade" id="alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="">Invalid Data Entry</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        Please enter valid information within the fields that have been highlighted in <span style="color: #c00c00; font-weight: bold;">red</span>. This includes question answers and the following icons.<br /><div class="btn btn-danger"><i class='fa fa-paperclip' style='text-align:left;'></i></div>&nbsp;<div class="btn btn-danger"><i class='fa fa-comments' style='text-align:left;'></i></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- End of alert Modal -->



<script type="text/javascript">

   




</script>
