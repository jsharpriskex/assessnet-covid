﻿
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Your Session has been timed out</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>

    <link href="version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />

</head>

<body>
    <form id="pageform" runat="server" action="#">


    <div class="container">
    <div class="row">

        <div class="col-md-12 text-center" style="padding-top: 160px;">

            <img src="Combine-CREATORS-OfAssess-300x78.png" style="width:400px;" />

        </div>


    </div>

    <div class="row">

        <div class="col-md-8 col-md-offset-2 text-center align-self-center">

            <h3>Your session has be idle and has been ended for security reasons</h3>

            <p>&nbsp;</p>

            <div class="row">
                <div class="col-xs-12 text-center">
                    <a href="https://covid.riskex.co.uk/admin/" target="_parent" class="btn btn-lg btn-danger">Admin Login</a>
                </div>
            </div>

        </div>
        </div>

    </form>


</body>
</html>


