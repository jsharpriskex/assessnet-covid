﻿
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Opps! - An error has occured</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>

    <link href="version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />

</head>

<body>
    <form id="pageform" runat="server" action="#">


    <div class="container">
    <div class="row">

        <div class="col-md-12 text-center" style="padding-top: 40px;">

            <img src="error-image.PNG" />

        </div>


    </div>

    <div class="row">

        <div class="col-md-8 col-md-offset-2 text-center align-self-center">

            <h1>Oops! - An error has occured</h1>
            <p >This error has created a report that has been logged in our system if further investigation is required.</p>


            <div style="margin-left:40px;margin-right:40px;margin-top:25px;">
            <p>Please use the back button on your browser to go back to the previous page and try again. If the problem persists, then please contact our support team via the help tab or via <strong><a href="mailto:support@assessnet.co.uk">support@assessnet.co.uk</a></strong>.</p>


             <div class="alert alert-success text-left" role="alert">
                <div class="media">
                  <div class="media-left">
                      <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                    <strong>Useful Tip</strong><br />Sometimes it helps to close down your browser and/or clear all your cache before you login and try again. We do love browsers, but they sometimes get confused and need a fresh start.
                  </div>
                </div>
                </div>

            </div>

        </div>
        </div>

    </form>


</body>
</html>
