﻿
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Oops - Page can not be found</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>

    <link href="version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />

</head>

<body>
    <form id="pageform" runat="server" action="#">


    <div class="container">
    <div class="row">

        <div class="col-md-12 text-center" style="padding-top: 40px;">

            <img src="404-image.png" />

        </div>


    </div>

    <div class="row">

        <div class="col-md-8 col-md-offset-2 text-center align-self-center">

            <h1>Oops, sorry we can't find that page!</h1>
            <p>This error has been sent to support so we can track down why the page is not found.</p>


            <div style="margin-left:40px;margin-right:40px;margin-top:25px;">
            <p>AssessNET comprises of 1000's of pages and at times links to those pages may be incorrect.  If this keeps occurring, please contact our support team via the help tab or via <strong><a href="mailto:support@assessnet.co.uk">support@assessnet.co.uk</a></strong> and provide details as to where you were in the system prior to this error.</p>


             <div class="alert alert-success text-left" role="alert">
                <div class="media">
                  <div class="media-left">
                      <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                    <strong>Useful Tip</strong><br />On occasion pages may move and you may get this error because of a link you saved previous.  If so, we recommend you remove your shortcut, login and find the page you require and create a new link or browser favourite.
                  </div>
                </div>
                </div>

            </div>

        </div>
        </div>

    </form>


</body>
</html>


