<%
Option Explicit


Const lngMaxFormBytes = 200

Dim objASPError, blnErrorWritten, strServername, strServerIP, strRemoteIP
Dim strMethod, lngPos, datNow, strQueryString, strURL

If Response.Buffer Then
    Response.Clear
    Response.Status = "500 Internal Server Error"
    Response.ContentType = "text/html"
    Response.Expires = 0
End If


' ------------------------------------------------------
' --------------- COMPILE ERROR DETAILS ----------------

Set objASPError = Server.GetLastError

Dim bakCodepage, errorBody, errorTitle, shortError, errorBrowser, errorPage, errorPost
On Error resume Next
    bakCodepage = Session.Codepage
    Session.Codepage = 1252
On Error goto 0


If objASPError.ASPCode > "" Then
    errorBody = errorBody & Server.HTMLEncode(", " & objASPError.ASPCode)
End if

errorBody = errorBody & Server.HTMLEncode(" (0x" & Hex(objASPError.Number) & ")" ) & "<br />"

If objASPError.ASPDescription > "" Then
    errorBody = errorBody & Server.HTMLEncode(objASPError.ASPDescription) & "<br />"
ElseIf (objASPError.Description > "") Then
    errorBody = errorBody & Server.HTMLEncode(objASPError.Description) & "<br />"
End if

shortError = errorBody

blnErrorWritten = False

' Only show the Source if it is available and the request is from the same machine as IIS
If objASPError.Source > "" Then
    strServername = LCase(Request.ServerVariables("SERVER_NAME"))
    strServerIP = Request.ServerVariables("LOCAL_ADDR")
    strRemoteIP =  Request.ServerVariables("REMOTE_ADDR")
    If (strServername = "localhost" Or strServerIP = strRemoteIP) And objASPError.File <> "?" Then
        errorBody = errorBody & Server.HTMLEncode(objASPError.File)
        If objASPError.Line > 0 Then errorBody = errorBody & ", line " & objASPError.Line
        If objASPError.Column > 0 Then
            errorBody = errorBody & ", column " & objASPError.Column
        End if
        errorBody = errorBody & "<br>"
        errorBody = errorBody & "<font style=""COLOR:000000; FONT: 8pt/11pt courier new""><b>"
        errorBody = errorBody & Server.HTMLEncode(objASPError.Source) & "<br />"

        If objASPError.Column > 0 Then
            errorBody = errorBody & String((objASPError.Column - 1), "-") & "<br />"
        End if
        errorBody = errorBody & "</b></font>"
        blnErrorWritten = True
    End If
End If

If Not blnErrorWritten And objASPError.File <> "?" Then
    errorTitle = Server.HTMLEncode(objASPError.File)
    If objASPError.Line > 0 Then
        errorTitle = errorTitle & Server.HTMLEncode(", line " & objASPError.Line)
    End if
    If objASPError.Column > 0 Then
        errorTitle = errorTitle & ", column " & objASPError.Column
    End if
    errorTitle = errorTitle
End If
thisFingerPrint = errorTitle

errorBrowser = "<span title='" & Server.HTMLEncode(Request.ServerVariables("HTTP_USER_AGENT")) & "'>[ UA ]</span>"

errorPage = "<span title='"

strMethod = Request.ServerVariables("REQUEST_METHOD")
errorPage = errorPage & strMethod & " "

If strMethod = "POST" Then
    errorPage = errorPage & Request.TotalBytes & " bytes to "
End If
errorPage = errorPage & Request.ServerVariables("SCRIPT_NAME")

lngPos = InStr(Request.QueryString, "|")

If lngPos > 1 Then
    errorPage = errorPage & "?" & Server.HTMLEncode(Left(Request.QueryString, (lngPos - 1)))
End If

errorPage = errorPage & "'>[ Pg ]</span>"




' ----------------- END ERROR DETAILS ------------------
' ------------------------------------------------------

Dim ObjSend, mailFrom, mailTo, mailSubject, mailBody

thisCreator = Session("YOUR_ACCOUNT_ID")
thisDescription = thisFingerPrint & _
                  "<p>" & errorPage & " " & errorBrowser & " <span title='" & Request.ServerVariables("HTTP_REFERER") & "'>[ Rf ]</span><br />" & _
                  "<p>" & errorPost & " " & _
                  "<p>" & errorBody & "</p>" & _
                  "<p><b>Requested URL:</b> " & request.QueryString & _
                   "<br /><b>HTTP Header:</b> " & Request.ServerVariables("ALL_HTTP")  & _
                  "</p><h3>Session details</h3>" & _
                  "<b>Corp. code:</b> " & Session("CORP_CODE") & _
                  "<br /><b>Group:</b> " & Session("YOUR_GROUP") & _
                  "<br /><b>Company:</b> " & Session("YOUR_COMPANY_NAME") & _
                  "<br /><b>Location:</b> " & Session("YOUR_LOCATION_NAME") & _
                  "<br /><b>Department:</b> " & Session("YOUR_DEPARTMENT_NAME") & _
                  "<br /><b>Person:</b> " & Session("YOUR_FIRSTNAME") & " " & Session("YOUR_SURNAME") & _
                  "<br /><b>Username:</b> " & Session("YOUR_USERNAME") & _
                  "<br /><b>E-mail:</b> " & Session("YOUR_EMAIL")
thisDescription = Replace(thisDescription,"'","''")
thisStatus = 0

%>
<!-- #INCLUDE FILE="old/dbconnections.inc" -->
<!-- #INCLUDE FILE="old/func_random.asp" -->
<%
Dim objRs, objConn, objCommand
Dim thisRef, thisCreator, thisDescription, thisStatus, thisFingerPrint, sqlErrorNotify, allowed_ref,checkRS


Call startConnections()

' -------------------  Add to DB  ------------------------

objCommand.CommandText = "SELECT * FROM " & Application("DBTABLE_DEV_DATA") & " " & _
                         "WHERE error_fingerprint='" & Replace(thisFingerPrint,"'","''") & "'" & _
                         "  AND status < 6"

Set objRs = objCommand.Execute

If objRs.EOF Then

    'generate unique ref

    allowed_ref = 0
    while allowed_ref = 0

        thisRef = generateRandom(12)
        objCommand.CommandText = "SELECT id FROM " & Application("DBTABLE_DEV_DATA") & " where ref= '" & thisRef & "' "
        set checkRS = objcommand.execute

        if checkRS.eof then
           allowed_ref = 1
        end if

    wend




    objCommand.CommandText = "INSERT INTO " & Application("DBTABLE_DEV_DATA") & _
                             "  (REF,CREATOR,DATE_CREATED,CORP_CODE,category,description,status,priority,error_fingerprint,error_notify) " & _
                             "VALUES" &  _
                             "  ('" & thisRef & "','" & thisCreator & "',GETDATE(),'" & Session("CORP_CODE") & "'," & _
                             "   'error','" & thisDescription & "','" & thisStatus & "'," & _
                             "   '5','" & thisFingerPrint & "','" & Session("YOUR_EMAIL") & "')"
    objCommand.Execute

Else
    If InStr("" & objRs("error_notify"), Session("YOUR_EMAIL")) Then
        sqlErrorNotify = ""
    Else
        sqlErrorNotify = ", error_notify = error_notify + ', " & Session("YOUR_EMAIL") & "'"
    End if

    objCommand.CommandText = "UPDATE " & Application("DBTABLE_DEV_DATA") & _
                             "  SET error_count = error_count + 1" & sqlErrorNotify & " " & _
                             "WHERE error_fingerprint = '" & thisFingerPrint & "'"

    objCommand.Execute

End if

Call endConnections()

' -------------------- SEND EMAIL -------------------------

MailFrom = "Error Page <error.page@assessnet.co.uk>"
MailTo = "Development <development@assessnet.co.uk>"
MailSubject = "Error Page - " & Date()
MailBody = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN""><html><head></head><body>" & _
           thisDescription & _
           "</body></html>"

Set objSend = Server.CreateObject("CDONTS.Newmail")

objSend.Importance = 1  '0 - Low, 1 - Norm, 2 - High
objSend.BodyFormat = 0  '0 for HTML , 1 for Plain text
objSend.MailFormat = 0
objSend.From = mailFrom
objSend.To = mailTo
objSend.Subject = mailSubject
objSend.body = mailBody
objSend.send

Set objSend = Nothing

%>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Oops! - An error has occured</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>


</head>

<body>
    <form id="pageform" runat="server" action="#">


    <div class="container">
    <div class="row">

        <div class="col-md-12 text-center" style="padding-top: 40px;">

            <img src="error-image.PNG" />

        </div>


    </div>

    <div class="row">

        <div class="col-md-8 col-md-offset-2 text-center align-self-center">

            <h1>Oops! - An error has occured</h1>
            <p >This error has created a report and sent it over for further diagnostics.</p>


            <div style="margin-left:40px;margin-right:40px;margin-top:25px;">
            <p>Please use the back button on your browser to go back to the previous page and try again. If the problem persists, then please contact our support team via the help tab or via <strong><a href="mailto:support@assessnet.co.uk">support@assessnet.co.uk</a></strong>.</p>


             <div class="alert alert-success text-left" role="alert">
                <div class="media">
                  <div class="media-left">
                      <i class="fa fa-lightbulb-o fa-4x" aria-hidden="true"></i>
                  </div>
                  <div class="media-body">
                    <strong>Useful Tip</strong><br />Sometimes it helps to close down your browser and/or clear all your cache before you login and try again. We do love browsers, but they sometimes get confused and need a fresh start.
                  </div>
                </div>
                </div>

            </div>

        </div>
        </div>

    </form>


</body>
</html>

