<%
Randomize()

Function generateRandom(strLen)
    Dim strCount, intRnd, listRnd
    
    strCount = 0
    Do
        ' Get random number between 48 & 122 (0 - Z)
        intRnd = int(75 * Rnd() + 48)
        
        ' Limit string to 0-9, a-z, A-Z
        If (intRnd < 57 OR intRnd > 65) AND (intRnd < 90 OR intRnd > 97) then
             listRnd = listRnd & chr(intRnd)
             strCount = strCount + 1
        End if
    Loop until strCount = strLen
    
    generateRandom = listRnd
End function
%>