﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboard.aspx.cs" Inherits="responseDashboard" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Dashboard</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>


    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />





    <style type="text/css">
        .postby {float: right; color: Gray; font-size: 0.8em;}

    .tile-heading {
    padding: 5px 8px;
    text-transform: uppercase;
    background-color: #186f9d;
    font-size:15px;
    color: #FFF;
    }

        .tile-body {
            padding: 10px;
            color: #FFFFFF;
            line-height: 40px;
        }

    .fa-user:before {
    content: "\f007";

}

    .tile {
    margin-bottom: 15px;
    border-radius: 3px;
    background-color: #279FE0;
    color: #FFFFFF;
    transition: all 1s;
}
    .tile-footer {
    padding: 5px 8px;
    background-color: #186f9d;
    text-align:right;
    font-size:16px;
}

    .tile a {
    color: #FFFFFF;
}


    .tile .tile-body i {
    font-size: 40px;
    opacity: 0.7;
    transition: all 1s;
    padding-top: 10px;
}

    .tile .tile-body h2 {
    font-size: 37px;
    margin: 5px;
    padding: 5px;
}

    </style>






</head>
<body runat="server" id="pageBody">
    <form id="pageform" runat="server">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


    <div id='downloadPopup' class="modal fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Export File Generating</h4>
                </div>
                <div class="modal-body">
                    <p style="font-size: 14px;">The export file has been requested and may take several minutes to generate. You will be able to continue once the file is available for download. Please do not navigate away from this page whilst the export is generated.</p>

                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-success progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <div class="container-fluid">
    <div class="row">

    <asp:UpdatePanel id="UpdatePanelStats" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <div runAt="server" id="reportMenu" class="subpageNavListMargin" style="padding-top: 10px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header">
            Navigation
          </li>

          <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" Text="Back to main menu" OnClick="goToMainMenu_Click" />
          <asp:Button ID="goToSearch" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to search" OnClick="goToSearch_Click" />

          <a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/risk-assessment-software/" target="_blank">
          <img src="img/safe2daybanneradvert.png" style="padding-top:10px;" />

          <img src="setup/img/Fit2Work-300dpi-ReMake.jpg" style="margin-top:5px;" />

          </a>


        </ul>
    </div>

    </ContentTemplate>
    </asp:UpdatePanel>


    <div runAt="server" id="reportContent" class="mainpageNavList"> <!-- Main Col Start -->

        <div class="container-fluid">
            <div class="row" style="padding-bottom:20px;">
            <div class="col-xs-8">
                    <h1 style="margin:0px;padding-top:15px;padding-bottom:3px;font-size:29px;">Health Assessment (COVID-19) Dashboard</h1>
                    <span style="font-size:26px;">For <asp:Literal runat="server" ID="litCompanyName" /></span>
            </div>
            <div class="col-xs-4 text-right" align="right">
                 <img src="Combine-CREATORS-OfAssess-300x78.png" />
                 <div class="sharethis-inline-follow-buttons"></div>
             </div>
            </div>
        </div>



    <div class="subpageNavListMobile" style="display:none;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Navigation & Options <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>

    <div class="row">
    <div class="col-md-8">


        <asp:UpdatePanel ID="UpdatePanelDashboard" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <div class="row">
        <div class="col-md-6">


            <div class="tile">
               <div class="tile-heading" style="background-color:#038a5d !important;">Feeling Fit 2 work<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#5DBE9E !important;"><i class="fas fa-user-hard-hat"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleFTW" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#038a5d !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardPeopleFTW_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=4&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>">Show...</a></div>
            </div>



        </div>
        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#b71f1f !important;">Not feeling fit 2 work<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#E06767 !important;"><i class="fas fa-user-alt"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleNotFTW" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#b71f1f !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardPeopleNotFTW_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=9&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>">Show...</a></div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr />
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#b71f1f !important;">Tested positive for COVID -19 (NHS)<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#E06767 !important;"><i class="fa fa-thermometer-full" aria-hidden="true"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardConfirmedInfections" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#b71f1f !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardConfirmedInfections_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=1&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>">Show...</a></div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#b71f1f !important;">Self reported symptoms of COVID-19<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#E06767 !important;"><i class="fa fa-thermometer-quarter" aria-hidden="true"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPossibleInfections" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#b71f1f !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardPossibleInfections_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=2&rStatus=E&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>">Show...</a></div>
            </div>

        </div>
    </div>
    <div class="row">
        
        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#e15c1a!important;">People at risk of infection<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#ef9466 !important;"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleInfectionRisk" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#e15c1a !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardPeopleInfectionRisk_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=5&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>"> Show...</a></div>
            </div>

          </div>

          <div class="col-md-6">


            <div class="tile">
               <div class="tile-heading">With symptoms but not called NHS 111<span class="pull-right"></span></div>
                   <div class="tile-body"><i class="fas fa-phone-slash"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardSymptomsNotCalledNHS" Text="0" /></h2>
                   </div>
               <div class="tile-footer"><span class="pull-left"><asp:Literal runat="server" ID="dashboardSymptomsNotCalledNHS_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=6&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>"> Show...</a></div>
            </div>

          </div>

    </div>
    <div class="row">

        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#e15c1a !important;">Home alone - has symptoms<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#ef9466 !important;"><i class="fas fa-house-user"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardSymptomsHomeAlone" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#e15c1a !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardSymptomsHomeAlone_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=7&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>"> Show...</a></div>
            </div>

        </div>

        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading">People with Underlying Health Issues<span class="pull-right"></span></div>
                   <div class="tile-body"><i class="fas fa-heartbeat"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardUnderlyingIssues" Text="0" /></h2>
                   </div>
                  <div class="tile-footer"><span class="pull-left"><asp:Literal runat="server" ID="dashboardUnderlyingIssues_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=8&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>"> Show...</a></div>

            </div>

        </div>

    </div>
    <div class="row">

        <div class="col-md-6">

            <div class="tile">
               <div class="tile-heading" style="background-color:#e15c1a !important;">People in Isolation<span class="pull-right"></span></div>
                   <div class="tile-body" style="background-color:#ef9466 !important;"><i class="fas fa-user-lock"></i>
                   <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleInIsolation" Text="0" /></h2>
                   </div>
               <div class="tile-footer" style="background-color:#e15c1a  !important;"><span class="pull-left"><asp:Literal runat="server" ID="dashboardPeopleInIsolation_per" Text="0" />% of users</span><a href="responseSearch.aspx?filter=Y&cStatus=3&rStatus=&tier2=<% =tier2_value %>&tier3=<% =tier3_value %>&tier4=<% =tier4_value %>&tier5=<% =tier5_value %>&tier6=<% =tier6_value %>"> Show...</a></div>
            </div>

        </div>
        <div class="col-md-6" >





        </div>
    </div>



    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="clearDashboardFilter" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="setDashboardFilter" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>


    </div>
    <div class="col-md-4">

           <div class="panel panel-default" runat="server" visible="true">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Options</h3>
                  </div>
                  <div class="panel-body">

                                       <asp:UpdatePanel ID="UpdatePanel_structure" runat="server" UpdateMode="Conditional">
                                       <ContentTemplate>

                                       <asp:Label runat="server" ID="testing" />

                                        <div class="form-group">

                                           <table class="table table-bordered table-searchstyle01">
                                           <tr><th>Filter to specific area</th></tr>
                                           <tr><td>

                                            <!-- Will only show for new setups -->
                                           <asp:DropDownList runat="server" ID="srch_structureTierSolo" Visible="false" class="form-control" Style="width: 99% !important; display: inline;"></asp:DropDownList>
                                           <!-- Will only show for AssessNET clients -->

                                           <asp:DropDownList runat="server" ID="srch_structureTier1" class="form-control" OnSelectedIndexChanged="structureTier1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                           <asp:DropDownList runat="server" ID="srch_structureTier2" class="form-control" style="margin-top:3px !important;" OnSelectedIndexChanged="structureTier2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                           <asp:DropDownList runat="server" ID="srch_structureTier3" class="form-control" style="margin-top:3px !important;" OnSelectedIndexChanged="structureTier3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                           <asp:DropDownList runat="server" ID="srch_structureTier4" class="form-control" style="margin-top:3px !important;" OnSelectedIndexChanged="structureTier4_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                           <asp:DropDownList runat="server" ID="srch_structureTier5" class="form-control" style="margin-top:3px !important;"></asp:DropDownList>
                                           </td></tr></table>

                                        </div>

                                       </ContentTemplate>
                                            <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="srch_structureTier1" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="srch_structureTier2" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="srch_structureTier3" EventName="SelectedIndexChanged" />
                                            <asp:AsyncPostBackTrigger ControlID="srch_structureTier4" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                       </asp:UpdatePanel>

                      <div class="row">
                          <div class="col-md-6">
                              <asp:Button ID="clearDashboardFilter" runat="server" class="btn btn-danger btn btn-block" Text="Clear" OnClick="clearDashboardFilter_Click" />
                          </div>
                          <div class="col-md-6">
                              <asp:Button ID="setDashboardFilter" runat="server" class="btn btn-success btn btn-block" Text="Filter Results" OnClick="setDashboardFilter_Click" />
                          </div>
                      </div>


                  </div>
            </div>


              <div class="row">
                <div class="col-md-12">

                    <div class="alert alert-success" role="alert">

                    <div class="media">
                          <div class="media-left">
                              <i class="fas fa-3x fa-history"></i>
                          </div>
                          <div class="media-body">
                            <p><strong>Auto-refresh</strong> if you leave this dashboard open, it will refresh the statistics every 5 minutes.</p>
                          </div>
                        </div>
                    </div>

                </div>
                </div>

               <div class="row">
                <div class="col-md-12">

                    <div class="alert alert-info" role="alert" runat="server" id="S2D_areaExport" visible="true">
                    
                    <div class="media">
                          <div class="media-left">
                              <i class="fa fa-3x fa-file-excel-o" aria-hidden="true"></i>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">Export COVID Data</h4>
                            Download assessment raw data
                          </div>
                          <div class="media-left">
                              <a href="#" class="btn btn-primary btn btn-block" onclick="exportButtonClick()">Download</a>
                          </div>
                        </div>
                    </div>

                </div>
            </div>


    </div>

    </div>


    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->


        <script type="text/javascript">

            function exportButtonClick() {

                var cookiesEnabled = ("cookie" in document && (document.cookie.length > 0 || (document.cookie = "test").indexOf.call(document.cookie, "test") > -1));

                if (cookiesEnabled == true) {
                    var cookieId = createID();
                    //blockUIForDownload(cookieId);
                }

                window.location.href = '../../../version3.2/data/documents/excel_centre/ham_export/ham_export.aspx?ccode=<%=var_corp_code%>&a=<%=dataSecurityCode%>&outputto=screen&cookie=' + cookieId;
                //&sdate=01/01/1900&edate=31/12/2021

                return false;
            }

            function createID() {
                //return guid();
                return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                });
            }

            function blockUIForDownload(cookieId) {
                var token = cookieId;

                $('#downloadPopup').modal('show');

                fileDownloadCheckTimer = window.setInterval(function () {
                    var cookieValue = $.cookie('fileDownloadToken');
                    if (cookieValue == token)
                        finishDownload();
                }, 1000);
            }

            function finishDownload() {
                window.clearInterval(fileDownloadCheckTimer);
                $.removeCookie('fileDownloadToken', { path: '/' }); //clears this cookie value

                //alert("window has been unblocked!");
                $('#downloadPopup').modal('hide');
            }

        </script>





    </form>
    <br />
    <br />

</body>
</html>
