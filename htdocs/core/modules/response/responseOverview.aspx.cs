﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Linq;
using ASNETNSP;

public partial class responseOverview : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string tier2;
    public string tier3;
    public string tier4;
    public string tier5;
    public string tier6;
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;
    public string tier2Name;
    public string tier3Name;
    public string tier4Name;
    public string yourAccessLevel;

    string emailFirstName;
    string companyURLaccess;
    string emailAddress;

    // Search Specific Vars
    public string ssRef;
    public string runSearch;
    public string searchType;
    public int pageNumber;
    public int pageSelection;
    public string uniqueSearchCode;
    public string uniqueHistoryCode;
    public string uniqueSearchTableCode;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }


        //***************************
        // CHECK THE SESSION STATUS

        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        uniqueSearchCode = "CASEOV" + yourAccRef.Replace("-","") + var_corp_code;
        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");

        if (!IsPostBack)
        {

            runSearch = "Y";
            returnResults();
            displayPagingCheck("0");

        }

    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }

    protected void returnSelectedPageTop(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigationTop.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void returnSelectedPage(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigation.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void nextPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) + 1;
        returnResultsPage(pageNumber);
        previousPageButton.Enabled = true;
        previousPageButtonTop.Enabled = true;

    }

    protected void previousPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) - 1;
        returnResultsPage(pageNumber);
        nextPageButtonTop.Enabled = true;
        nextPageButton.Enabled = true;

    }

    public void returnResultsPage(int pageNumber)
    {

        string pageSize = "15";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string sqlText = "execute Module_HA.dbo.caseSearch_Page '" + uniqueSearchCode + "', '" + pageNumber + "', '" + pageSize + "'";

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            //Set the table info
            tableContentResults.DataSource = objrs;
            tableContentResults.DataBind();

            currenPageNumberHidden.Value = pageNumber.ToString();


            sqlComm.Dispose();
            objrs.Close();

            displayPagingCheck("0");
        }
    }

    private void displayPagingCheck(string commandType)
    {
        string sqlText;
        string scriptString;
        string results_addTheS = "s";
        pageNavigationTop.Items.Clear();
        pageNavigation.Items.Clear();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here1');", true);
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            sqlText = "SELECT COUNT(*) as totalrecords FROM TempSearchDB.dbo." + uniqueSearchCode;

            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here2');", true);

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                int totalRecords = Int32.Parse(objrs["totalrecords"].ToString());

                if (totalRecords == 1) { results_addTheS = ""; }


                results_count.Text = totalRecords.ToString() + " result" + results_addTheS + " found";


                results_count.Visible = true;
                updateRecordTotal.Update();

                if (totalRecords > 15)
                {
                    int pageCount = totalRecords / 15;
                    int remainder = totalRecords % 15;
                    if (remainder > 0) { pageCount++; }

                    pageNavigationTop.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    pageNavigation.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    maxPageNumberHidden.Value = pageCount.ToString();

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == 1)
                    {
                        previousPageButtonTop.Enabled = false;
                        previousPageButton.Enabled = false;
                    }
                    else
                    {
                        previousPageButtonTop.Enabled = true;
                        previousPageButton.Enabled = true;
                    }

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == Convert.ToInt32(maxPageNumberHidden.Value))
                    {
                        nextPageButtonTop.Enabled = false;
                        nextPageButton.Enabled = false;
                    }
                    else
                    {
                        nextPageButtonTop.Enabled = true;
                        nextPageButton.Enabled = true;
                    }

                    pageNumber = Convert.ToInt32(currenPageNumberHidden.Value);
                    pageNavigationTop.SelectedValue = pageNumber.ToString();
                    pageNavigation.SelectedValue = pageNumber.ToString();



                    scriptString = "$('.pagingSection').show();$('.pagingSectionTop').show();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";




                }
                else
                {
                    scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";
                }

            }
            else
            {
                results_count.Visible = false;
                results_count.Text = "0";

                scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.DisplayResults').show();";
            }


            //switch (commandType)
            //{
            //    case "OpenInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('show');";
            //        break;
            //    case "CloseInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('hide');";
            //        break;
            //    case "CloseLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('hide');";
            //        break;
            //    case "OpenLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('show');";
            //        break;

            //}

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", scriptString, true);

            sqlComm.Dispose();
            objrs.Close();

        }

        //return sqlText;
    }

    public void returnResults()
    {
        string sqlText = "";
        try
        {

            string pageSize = "15";

            if (pageNumber == 0)
            {
                pageNumber = 1;
            }

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "execute Module_HA.dbo.adminClientOverview '" + pageNumber + "', '" + pageSize + "', '" + uniqueSearchCode + "','0'";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

                tableContentResults.DataSource = objrs;
                tableContentResults.DataBind();

                currenPageNumberHidden.Value = "1";

                previousPageButtonTop.Enabled = false;
                previousPageButton.Enabled = false;

                objrs.Close();
                sqlComm.Dispose();
            }



        }
        catch (Exception)
        {
            throw;
        }


    }

    protected string countAssessments()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.statsCountAssessments", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return objrs["countAssessments"].ToString();
            }

            cmd.Dispose();

        }

        return "0";

    }

    protected string countAssessments(string _corpCode)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.statsCountAssessments", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return objrs["count"].ToString();
            }

            cmd.Dispose();

        }

        return "0";

    }


    protected string countUsers(string _corpCode)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.statsCountUsers", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return objrs["count"].ToString();
            }

            cmd.Dispose();

        }

        return "0";

    }







    protected void tableContentResults_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        string contactNumber = DataBinder.Eval(e.Item.DataItem, "contactNumber").ToString();
        string contactEmail = DataBinder.Eval(e.Item.DataItem, "contactEmail").ToString();
        string contactName = DataBinder.Eval(e.Item.DataItem, "contactName").ToString();
        string _corpCode = DataBinder.Eval(e.Item.DataItem, "corpCode").ToString();
        string terms_agreed = DataBinder.Eval(e.Item.DataItem, "terms_agreed").ToString();
        string approved = DataBinder.Eval(e.Item.DataItem, "approved").ToString();

        Literal client = (Literal)e.Item.FindControl("client");
        Literal users = (Literal)e.Item.FindControl("users");
        Literal assessments = (Literal)e.Item.FindControl("assessments");

        Button CommandOpt = (Button)e.Item.FindControl("commandOpt");

        HtmlGenericControl setupStatus = (HtmlGenericControl)e.Item.FindControl("setupStatus");
        HtmlGenericControl setupTerms = (HtmlGenericControl)e.Item.FindControl("setupTerms");
        HtmlGenericControl setupVal = (HtmlGenericControl)e.Item.FindControl("setupVal");
        HtmlGenericControl setupComp = (HtmlGenericControl)e.Item.FindControl("setupComp");


        if (approved == "False" && _corpCode.Length > 0)
        {
            CommandOpt.Visible = true;
        }



        if (contactName.Length > 0) {
            client.Text = contactName + "<br/>" + contactEmail + " | " + contactNumber;
        } else
        {
            client.Text = "AssessNET Client";
        }

        if (_corpCode.Length == 0)
        {
            setupStatus.Visible = true ;

            if (terms_agreed.Length == 0)
            {
                setupTerms.Visible = true;
            } else
            {
                setupVal.Visible = true;
            }

        } else
        {

            setupComp.Visible = true;
        }


        assessments.Text = countAssessments(_corpCode);
        users.Text = countUsers(_corpCode);



    }






    protected void tableContentResults_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string recID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "Approve":
                approveClient(recID);

                runSearch = "Y";
                returnResults();
                displayPagingCheck("0");


                break;

        }

    }



    // This is for all the approval end

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "Riskex Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }

            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }


    protected void approveClient(string _ref)
    {

        string emailSubject;
        string emailTitle;
        string emailBody;
        string _setupEmail;
        string access_link;

        // Get Details
        getUserDetails(_ref);


        //emailAddress = "j.sharp@riskex.co.uk";

        Boolean emailSent;

        access_link = "<a href='" + companyURLaccess + "'>" + companyURLaccess + "</a>";

        _setupEmail = emailAddress;


        // Send Admin email with setup details
        emailSubject = "System Details - COVID-19 Assessment Platform";
        emailTitle = "COVID-19 Health Assessment Service - System Details";
        emailBody = "<p>Hi " + emailFirstName + ",</p><p>Welcome to AssessNET, the home of Online Health and Safety Management.  Your system is all set up and ready to go.</p><p>Use the link below, which is unique to your organisation – </p><ul><li>Email this link to your employees so they can register and complete their assessment.  We have sent you another email containing a template, if you wish to use it.<p>" + access_link + "</p></li></ul><p>Use this link to login to your Dashboard and Admin Panel, using your email and password.</p><p><a href='https://covid.riskex.co.uk/admin/'>https://covid.riskex.co.uk/admin/</a></p><p><strong>IMPORTANT</strong></p><p>As the lead administrator and primary license holder for your company, you have main access to the data.  You can sign up additional administrators from within the Admin Panel, so they can use reports and view dashboard statistics.</p><p>Please note that your company is responsible for managing the data received to the dashboard.  The information provided to you is strictly confidential to your company. No others, apart from third parties to whom you provide access, will have access to this data.</p><p>If you provide a dashboard to a consultant or any third parties, you do so at your own risk and no liability rests with Riskex or our sponsors. </p><p>Please also note that this service is provided up until the 30th October 2020 and/or in accordance with the Terms and Conditions.</p><p>Please respect the lockdown conditions and take care of your health and wellbeing.</p><p>Best Regards</p><p>AssessNET Team</p>";

        emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);


        // Send template email to admin
        emailSubject = "Employee Access Template - COVID-19 Assessment Platform";
        emailTitle = "Health Assessment (COVID-19) <br/> Employee Access Template";
        emailBody = "<p>Welcome to AssessNET, <strong>COPY AND PASTE</strong> the email below to all your employees and volunteers:</p><p>Hi,</p><p>The link below will take you to our cloud-based COVID-19 Health Assessment.</p><p>" + access_link + "</p><p>As a valued employee / volunteer we request that you click the link and complete the simple health assessment.  It will take just 30 seconds.  We hope that you are well, and this assessment can be repeated and updated at any time, if your health status changes.</p><p>The COVID-19 crisis is presenting a lot of challenges.  As your employer, we are using the Riskex Health Assessment module to keep a check on your health status.</p><p>Your contact details and the information you supply are protected by encryption and will not be used for any purpose other than to contact you during this crisis.</p><p>Please respect the lockdown conditions and take care of your health and wellbeing.</p><p>Best regards</p><p>&nbsp;</p>";

        emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);


        // Set as approved
        if (emailSent)
        {
            setApproved(_ref);
        }




    }


    public void getUserDetails(string _ref)
    {
  

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupGetCompanyAccessDetailsByRef", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@ref", _ref));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                companyURLaccess = "https://covid.riskex.co.uk/?a=" + objrs["uniqueLink"].ToString() + objrs["uniqueSalt"].ToString();
                //companyValidationNumber.Text = objrs["validation_code"].ToString();
                emailAddress = objrs["emailaddress"].ToString();
                emailFirstName = objrs["setupFirstName"].ToString();

            }
        }


    }

    protected void setApproved(string _ref)
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminSetApproval", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@ref", _ref));
            cmd.ExecuteNonQuery();


            cmd.Dispose();

        }


    }



}

