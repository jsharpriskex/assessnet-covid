﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using System.Globalization;
using FarPoint.Excel.EntityClassLibrary.DrawingVML;

public partial class responseDashboard : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string tier2;
    public string tier3;
    public string tier4;
    public string tier5;
    public string tier6;
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;
    public string tier2Name;
    public string tier3Name;
    public string tier4Name;
    public string yourAccessLevel;
    public string MonthName;

    public string dataSecurityCode;

    public string tier2_value;
    public string tier3_value;
    public string tier4_value;
    public string tier5_value;
    public string tier6_value;

    public int _populationTotal = 0;


    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }


        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");
        tier2 = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_COMPANY", "user");
        tier3 = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_LOCATION", "user");
        tier4 = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_DEPARTMENT", "user");
        tier5 = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_TIER5", "user");
        tier6 = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_TIER6", "user");

        dataSecurityCode = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "DATA_EXPORT_SECURITY_CODE", "pref");

        // refresh page over intervals
        Response.AddHeader("Refresh", "300");

        if (tier2 == "-1")
        {
            tier2 = "0";
        }

        if (tier3 == "-1")
        {
            tier3 = "0";
        }

        if (tier4 == "-1")
        {
            tier4 = "0";
        }

        if (tier5 == "-1")
        {
            tier5 = "0";
        }

        if (tier6 == "-1")
        {
            tier6 = "0";
        }



        if (!IsPostBack)
        {

            getUserDetails(yourAccRef);

            // Hide the structure elements
            srch_structureTier2.Visible = false;
            srch_structureTier3.Visible = false;
            srch_structureTier4.Visible = false;
            srch_structureTier5.Visible = false;

            // Advanced Criteria Options
            populateSoloStructure();
            structureTier_populate(0);
            countStats();

            if (Session["rspDashboardSearch"] == "Y")
            {
                // ok we have filters in the session so add them in before the dashboard runs
                tier2 = Session["tier2"].ToString();
                tier3 = Session["tier3"].ToString();
                tier4 = Session["tier4"].ToString();
                tier5 = Session["tier5"].ToString();
                tier6 = Session["tier6"].ToString();
            }



            if (srch_structureTier1.Items.FindByValue(tier2) != null)
            {
                srch_structureTier1.SelectedValue = tier2;
                srch_structureTier2.Visible = true;
                getTier2StructureInformation();
                tier2_value = tier2;
            }

            if (srch_structureTier2.Items.FindByValue(tier3) != null)
            {
                srch_structureTier2.SelectedValue = tier3;
                srch_structureTier3.Visible = true;
                getTier3StructureInformation();
                tier3_value = tier3;
            }

            if (srch_structureTier3.Items.FindByValue(tier4) != null)
            {
                srch_structureTier3.SelectedValue = tier4;
                srch_structureTier4.Visible = true;
                getTier4StructureInformation();
                tier4_value = tier4;
            }

            if (srch_structureTier4.Items.FindByValue(tier5) != null)
            {
                srch_structureTier4.SelectedValue = tier5;
                srch_structureTier5.Visible = true;
                getTier5StructureInformation();
                tier5_value = tier5;
            }

            if (srch_structureTier5.Items.FindByValue(tier6) != null)
            {
                srch_structureTier5.SelectedValue = tier6;
                tier6_value = tier6;

            }

            //Move to shared components at some point
            MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(DateTime.Today.Month);
            dashboardStats();

        }




    }

    public void getUserDetails(string _yourAccRef)
    {

        // Kick out if the user account has needed more than 2 tries



        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", _yourAccRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                litCompanyName.Text = objrs["setupCompanyName"].ToString();

            }

            cmd.Dispose();
            objrs.Dispose();

        }



    }




    public string getPercentage(string input, int total)
    {

        // convert input
        int _input = Convert.ToInt32(input);

        int _percentage = (int)Math.Round((double)(100 * _input) / total);

        return _percentage.ToString();

    }



    public void dashboardStats()
    {

        string tier2Selection = "";

        if (srch_structureTierSolo.Visible)
        {
            tier2Selection = (srch_structureTierSolo.SelectedValue.Length < 5) ? srch_structureTierSolo.SelectedValue : "0";
        }
        else
        {
            tier2Selection = (srch_structureTier1.SelectedValue != "") ? srch_structureTier1.SelectedValue : "0";
        }

        string tier3Selection = (srch_structureTier2.SelectedValue != "") ? srch_structureTier2.SelectedValue : "0";
        string tier4Selection = (srch_structureTier3.SelectedValue != "") ? srch_structureTier3.SelectedValue : "0";
        string tier5Selection = (srch_structureTier4.SelectedValue != "") ? srch_structureTier4.SelectedValue : "0";
        string tier6Selection = (srch_structureTier5.SelectedValue != "") ? srch_structureTier5.SelectedValue : "0";




            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.dashboardPopulationTotal", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    _populationTotal = Convert.ToInt32(objrs["countvalue"].ToString());

                }


                objrs.Close();

                cmd = new SqlCommand("Module_HA.dbo.dashboardConfirmedInfections", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardConfirmedInfections.Text = objrs["countvalue"].ToString();
                    dashboardConfirmedInfections_per.Text = getPercentage(dashboardConfirmedInfections.Text, _populationTotal);

                }

                objrs.Close();



                cmd = new SqlCommand("Module_HA.dbo.dashboardPossibleInfections", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPossibleInfections.Text = objrs["countvalue"].ToString();
                    dashboardPossibleInfections_per.Text = getPercentage(dashboardPossibleInfections.Text, _populationTotal);
                }



                objrs.Close();

                cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleInIsolation", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleInIsolation.Text = objrs["countvalue"].ToString();
                    dashboardPeopleInIsolation_per.Text = getPercentage(dashboardPeopleInIsolation.Text, _populationTotal);
                }

                objrs.Close();



                cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleFTW", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleFTW.Text = objrs["countvalue"].ToString();
                    dashboardPeopleFTW_per.Text = getPercentage(dashboardPeopleFTW.Text, _populationTotal);
                }

                objrs.Close();


                cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleNotFTW", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleNotFTW.Text = objrs["countvalue"].ToString();
                    dashboardPeopleNotFTW_per.Text = getPercentage(dashboardPeopleNotFTW.Text, _populationTotal);
                }

                objrs.Close();



                cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleInfectionRisk", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleInfectionRisk.Text = objrs["countvalue"].ToString();
                    dashboardPeopleInfectionRisk_per.Text = getPercentage(dashboardPeopleInfectionRisk.Text, _populationTotal);
                }

                objrs.Close();



                cmd = new SqlCommand("Module_HA.dbo.dashboardSymptomsHomeAlone", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardSymptomsHomeAlone.Text = objrs["countvalue"].ToString();
                    dashboardSymptomsHomeAlone_per.Text = getPercentage(dashboardSymptomsHomeAlone.Text, _populationTotal);
                }

                objrs.Close();


                cmd = new SqlCommand("Module_HA.dbo.dashboardSymptomsNotCalledNHS", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardSymptomsNotCalledNHS.Text = objrs["countvalue"].ToString();
                    dashboardSymptomsNotCalledNHS_per.Text = getPercentage(dashboardSymptomsNotCalledNHS.Text, _populationTotal);
                }

                objrs.Close();


                cmd = new SqlCommand("Module_HA.dbo.dashboardUnderlyingIssues", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardUnderlyingIssues.Text = objrs["countvalue"].ToString();
                    dashboardUnderlyingIssues_per.Text = getPercentage(dashboardUnderlyingIssues.Text, _populationTotal);
                }

                objrs.Close();


            }




    }

    public void countStats()
    {

        //string sqlText = "";
        //try
        //{

        //    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        //    {
        //        sqlText = "execute Module_TMX.dbo.countCourses '" + var_corp_code + "'";
        //        SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
        //        SqlDataReader objrs = sqlComm.ExecuteReader();

        //        if (objrs.HasRows)
        //        {
        //            objrs.Read();
        //            courseCountBadge.Text = objrs["countvalue"].ToString();

        //        }

        //        objrs.Close();
        //        sqlComm.Dispose();

        //        sqlText = "execute Module_TMX.dbo.countProviders '" + var_corp_code + "'";
        //        SqlCommand sqlComm2 = new SqlCommand(sqlText, dbConn);
        //        SqlDataReader objrs2 = sqlComm2.ExecuteReader();

        //        if (objrs2.HasRows)
        //        {
        //            objrs2.Read();
        //            providerCountBadge.Text = objrs2["countvalue"].ToString();

        //        }

        //        objrs2.Close();
        //        sqlComm2.Dispose();

        //        sqlText = "execute Module_TMX.dbo.countRoles '" + var_corp_code + "'";
        //        SqlCommand sqlComm3 = new SqlCommand(sqlText, dbConn);
        //        SqlDataReader objrs3 = sqlComm3.ExecuteReader();

        //        if (objrs3.HasRows)
        //        {
        //            objrs3.Read();
        //            roleCountBadge.Text = objrs3["countvalue"].ToString();

        //        }

        //        objrs3.Close();
        //        sqlComm3.Dispose();

        //        sqlText = "execute Module_TMX.dbo.countPeople '" + var_corp_code + "'";
        //        SqlCommand sqlComm4 = new SqlCommand(sqlText, dbConn);
        //        SqlDataReader objrs4 = sqlComm4.ExecuteReader();

        //        if (objrs4.HasRows)
        //        {
        //            objrs4.Read();
        //            peopleCountBadge.Text = objrs4["countvalue"].ToString();
        //        }

        //        objrs4.Close();
        //        sqlComm4.Dispose();





        //        UpdatePanelStats.Update();

        //    }

        //} catch (Exception)
        //{
        //    throw;
        //}


    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }

    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }

    public void populateSoloStructure()
    {

        srch_structureTierSolo.Visible = true;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetStructure", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            // populate the drop down
            srch_structureTierSolo.DataSource = objrs;
            srch_structureTierSolo.DataValueField = "tierRef";
            srch_structureTierSolo.DataTextField = "struct_name";
            srch_structureTierSolo.DataBind();

            // put in a selectable option
            srch_structureTierSolo.Items.Insert(0, "All Locations");

            ListItem _item = new ListItem("(Location was not listed)", "-2");
            srch_structureTierSolo.Items.Add(_item);

            cmd.Dispose();

        }


    }





    // Structure Elements (main)

    public void structureTier_populate(Int32 refID)
    {

        srch_structureTier1.Items.Clear();

        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, "", tier2Ref, "SEARCH", yourAccessLevel, "", "2", ""))
            {
                srch_structureTier1.DataSource = returnedStructure;
                srch_structureTier1.DataValueField = "tierRef";
                srch_structureTier1.DataTextField = "struct_name";
                srch_structureTier1.DataBind();

                if (srch_structureTier1.Items.Count == 0)
                {
                    srch_structureTier1.Visible = false;
                    srch_structureTier1.Items.Clear();

                }


            }


        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void structureTier1_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier2.Visible = true;
        srch_structureTier3.Visible = false;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;
        srch_structureTier2.Items.Clear();
        srch_structureTier3.Items.Clear();
        srch_structureTier4.Items.Clear();
        srch_structureTier5.Items.Clear();
        getTier2StructureInformation();



    }

    public void structureTier2_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier3.Visible = true;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;
        srch_structureTier3.Items.Clear();
        srch_structureTier4.Items.Clear();
        srch_structureTier5.Items.Clear();
        getTier3StructureInformation();



    }

    public void structureTier3_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier4.Visible = true;
        srch_structureTier5.Visible = false;
        srch_structureTier4.Items.Clear();
        srch_structureTier5.Items.Clear();
        getTier4StructureInformation();



    }

    public void structureTier4_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier5.Visible = true;
        srch_structureTier5.Items.Clear();
        getTier5StructureInformation();



    }

    public void getTier2StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier1.SelectedValue != "")
            {
                currentSelection = srch_structureTier1.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier3Ref, "SEARCH", yourAccessLevel, "", "3", ""))
            {
                srch_structureTier2.DataSource = returnedStructure;
                srch_structureTier2.DataValueField = "tierRef";
                srch_structureTier2.DataTextField = "struct_name";
                srch_structureTier2.DataBind();

                if (srch_structureTier2.Items.Count == 0)
                {
                    srch_structureTier2.Visible = false;
                    srch_structureTier2.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            testing.Text = ex.ToString();

        }

    }

    public void getTier3StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier2.SelectedValue != "")
            {
                currentSelection = srch_structureTier2.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier4Ref, "SEARCH", yourAccessLevel, "", "4", ""))
            {
                srch_structureTier3.DataSource = returnedStructure;
                srch_structureTier3.DataValueField = "tierRef";
                srch_structureTier3.DataTextField = "struct_name";
                srch_structureTier3.DataBind();

                if (srch_structureTier3.Items.Count == 0)
                {
                    srch_structureTier3.Visible = false;
                    srch_structureTier3.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier4StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier3.SelectedValue != "")
            {
                currentSelection = srch_structureTier3.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier5Ref, "SEARCH", yourAccessLevel, "", "5", ""))
            {
                srch_structureTier4.DataSource = returnedStructure;
                srch_structureTier4.DataValueField = "tierRef";
                srch_structureTier4.DataTextField = "struct_name";
                srch_structureTier4.DataBind();

                if (srch_structureTier4.Items.Count == 0)
                {
                    srch_structureTier4.Visible = false;
                    srch_structureTier4.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier5StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier4.SelectedValue != "")
            {
                currentSelection = srch_structureTier4.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier6Ref, "SEARCH", yourAccessLevel, "", "6", ""))
            {
                srch_structureTier5.DataSource = returnedStructure;
                srch_structureTier5.DataValueField = "tierRef";
                srch_structureTier5.DataTextField = "struct_name";
                srch_structureTier5.DataBind();

                if (srch_structureTier5.Items.Count == 0)
                {
                    srch_structureTier5.Visible = false;
                    srch_structureTier5.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    // Structure Elements Main End

    public void setDashboardFilter_Click(object sender, EventArgs e)
    {

        Session["tier2"] = null;
        Session["tier3"] = null;
        Session["tier4"] = null;
        Session["tier5"] = null;
        Session["tier6"] = null;

        if (srch_structureTierSolo.Visible)
        {
            tier2_value = (srch_structureTierSolo.SelectedValue.Length < 5) ? srch_structureTierSolo.SelectedValue : "";
        }
        else
        {
            tier2_value = srch_structureTier1.SelectedValue;
        }

        tier3_value = srch_structureTier2.SelectedValue;
        tier4_value = srch_structureTier3.SelectedValue;
        tier5_value = srch_structureTier4.SelectedValue;
        tier6_value = srch_structureTier5.SelectedValue;


        Session["tmxDashboardSearch"] = "Y";
        Session["tier2"] = tier2_value;
        Session["tier3"] = tier3_value;
        Session["tier4"] = tier4_value;
        Session["tier5"] = tier5_value;
        Session["tier6"] = tier6_value;

        dashboardStats();

    }

    protected void clearDashboardFilter_Click(object sender, EventArgs e)
    {


        srch_structureTier2.Visible = false;
        srch_structureTier3.Visible = false;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;


        if (srch_structureTier1.Items.FindByValue(tier2) != null)
        {
            srch_structureTier1.SelectedValue = tier2;
            srch_structureTier2.Visible = true;
            getTier2StructureInformation();
            tier2_value = tier2;
        }

        if (srch_structureTier2.Items.FindByValue(tier3) != null)
        {
            srch_structureTier2.SelectedValue = tier3;
            srch_structureTier3.Visible = true;
            getTier3StructureInformation();
            tier3_value = tier3;
        }

        if (srch_structureTier3.Items.FindByValue(tier4) != null)
        {
            srch_structureTier3.SelectedValue = tier4;
            srch_structureTier4.Visible = true;
            getTier4StructureInformation();
            tier4_value = tier4;
        }

        if (srch_structureTier4.Items.FindByValue(tier5) != null)
        {
            srch_structureTier4.SelectedValue = tier5;
            srch_structureTier5.Visible = true;
            getTier5StructureInformation();
            tier5_value = tier5;
        }

        if (srch_structureTier5.Items.FindByValue(tier6) != null)
        {
            srch_structureTier5.SelectedValue = tier6;
            tier6_value = tier6;

        }

        if (srch_structureTierSolo.Visible)
        {
           srch_structureTierSolo.SelectedIndex = 0;
        }


        Session["rspDashboardSearch"] = null;
        Session["tier2"] = null;
        Session["tier3"] = null;
        Session["tier4"] = null;
        Session["tier5"] = null;
        Session["tier6"] = null;

        dashboardStats();
        UpdatePanel_structure.Update();

    }

}

