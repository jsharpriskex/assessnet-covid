﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using System.Globalization;
using Telerik.Windows.Documents.Spreadsheet.Model;
using FarPoint.Excel;

public partial class responseControl : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string yourAccessLevel;


    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }

        //***************************
        // CHECK THE SESSION STATUS
        // SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");




        if (!IsPostBack)
        {

            showReminders(); 
            showLocations();
            showUsers();

        }




    }

    protected void ddl_remindersHours_SelectedIndexChanged(object sender, EventArgs e)
    {

        string _hours = ddl_remindersHours.SelectedValue;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminSetReminder", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@hours", _hours));
            cmd.ExecuteNonQuery();

        }

    }


    protected void showReminders()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminGetReminder", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                ddl_remindersHours.SelectedValue = objrs["hours"].ToString();

            }


        }



    }

    protected void setupAdminAccounts()
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupSendEntryAdminEmails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.ExecuteReader();

        }

    }

    protected void addNewAdminAccount(string _password)
    {

        string _accRef = Guid.NewGuid().ToString();
        string _salt = ASNETNSP.Authentication.setSalt(512);
        string _firstname = firstname_admin.Text;
        string _lastname = surname_admin.Text;
        string _phone = contactNumber_admin.Text;
        string _role = role_admin.SelectedValue;
        string _email = emailAddress_admin.Text;
        string _isValid = "NO";

        string _passwordHash = ASNETNSP.Authentication.setSHA512Hash(_password, _salt);



            // Set the password and create the admin account whilst there.
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupAddManager", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@password", _passwordHash));
                cmd.Parameters.Add(new SqlParameter("@salt", _salt));
                cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
                cmd.Parameters.Add(new SqlParameter("@corp_email", _email));
                cmd.Parameters.Add(new SqlParameter("@fname", _firstname));
                cmd.Parameters.Add(new SqlParameter("@sname", _lastname));
                cmd.Parameters.Add(new SqlParameter("@corp_jobtitle", _role));
                cmd.Parameters.Add(new SqlParameter("@corp_phone", _phone));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    _isValid = objrs["isValid"].ToString();
                }

            }


        if (_isValid == "YES")
        {
            createAdminAccessLink(_accRef, var_corp_code, _email);
            setupAdminAccounts();
            clearForm();
            pwAlertArea.Visible = false;
            showUsers();
            UpdatePanelUsers.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#addUserModal').modal('hide');", true);


        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#notValidModal').modal('show');", true);
        }


    }

    protected void clearForm()
    {

        firstname_admin.Text = "";
        surname_admin.Text = "";
        contactNumber_admin.Text = "";
        role_admin.SelectedIndex = 0;
        emailAddress_admin.Text = "";

    }

    public int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public void createAdminAccessLink(string _acc_ref, string _corp_code, string _emailAddress)
    {

        // New unique ID with some salt
        string _adminAccessLink = Guid.NewGuid().ToString();
        string _adminAccessSalt = RandomNumber(100000, 999999).ToString();
        _adminAccessLink = _adminAccessLink.Replace("-", "");

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCreateAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@ip_address", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@access_link", _adminAccessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _adminAccessSalt));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }


    }

    protected void saveUserDetails_Click(object sender, EventArgs e)
    {

        string _emailAddress = emailAddress_admin.Text;
        string _firstname = firstname_admin.Text;
        string _surname = surname_admin.Text;
        string _number = contactNumber_admin.Text;
        string _role = role_admin.SelectedValue.ToString();
        bool formIsValid = false;

        if (_emailAddress.Length > 1 && _role.Length > 2 && _number.Length > 7 && _firstname.Length > 1 && _surname.Length > 1) { formIsValid = true; }

        if (formIsValid)
        {

            if (IsValidEmail(_emailAddress))
            {

                addNewAdminAccount("wk38fkWDk2dkd@@wdi2");

            }
            else
            {

                pwAlertArea.Visible = true;
                pwAlert.Attributes.Add("Class", "alert alert-danger");
                pwMsg.Text = "<strong>Alert: </strong>The email you have entered is not valid, please check.";

            }

        } else
        {

            pwAlertArea.Visible = true;
            pwAlert.Attributes.Add("Class", "alert alert-danger");
            pwMsg.Text = "<strong>Alert: </strong>Please ensure all items are completed and valid";

        }
    }

    protected void showUsers()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminShowAdminUsers", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", yourAccRef));
            objrs = cmd.ExecuteReader();


            tableContentUsers.DataSource = objrs;
            tableContentUsers.DataBind();

        }

    }

    protected void showLocations()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupShowLocation", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            locationList.DataSource = objrs;
            locationList.DataBind();

        }

    }

    protected void saveLocation_Click(object sender, EventArgs e)
    {
        string _location = addLocationText.Text;

        if (_location.Length > 0)
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupAddLocation", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@location", _location));
                cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
                cmd.ExecuteNonQuery();

                addLocationText.Text = "";
            }

            addLocationText.Text = "";

            showLocations();
            UpdatePanelLocations.Update();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#addLocModal').modal('hide');", true);

        }

    }

    protected bool checkLocationIsEmpty(string _tier)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCheckForLocation", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@tier_ref", _tier));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                if (Convert.ToInt32(objrs["RecordCount"].ToString()) > 0) {
                    // Dont show remove, it has locations
                    return false;
                } else
                {
                    return true;
                }

            } else
            {
                return true;
            }

        }


    }

    public void locationList_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        string tier_ref = DataBinder.Eval(e.Item.DataItem, "tier2_ref").ToString();
        HtmlGenericControl RemoveLocationDiv = (HtmlGenericControl)e.Item.FindControl("RemoveLocationDiv");

        RemoveLocationDiv.Visible = checkLocationIsEmpty(tier_ref);

    }

    protected void tableContentUsers_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        string memInfo = DataBinder.Eval(e.Item.DataItem, "acc_mquestion").ToString();
        string userdis = DataBinder.Eval(e.Item.DataItem, "acc_status").ToString();
        string userlvl = DataBinder.Eval(e.Item.DataItem, "acc_level").ToString();

        HtmlGenericControl UserStatusNotUsed = (HtmlGenericControl)e.Item.FindControl("UserStatusNotUsed");
        HtmlGenericControl UserStatusDisabled = (HtmlGenericControl)e.Item.FindControl("UserStatusDisabled");
        HtmlGenericControl UserStatusActive = (HtmlGenericControl)e.Item.FindControl("UserStatusActive");

        HtmlGenericControl enableOptionLi = (HtmlGenericControl)e.Item.FindControl("enableOptionLi");
        HtmlGenericControl disableOptionli = (HtmlGenericControl)e.Item.FindControl("disableOptionli");

        if (userdis == "D" && userlvl == "1")
        {
            UserStatusDisabled.Visible = true;
            enableOptionLi.Visible = true;
            disableOptionli.Visible = false;

        }
        else
        {
            if (memInfo.Length > 0) {
                UserStatusActive.Visible = true;
            } else
            {
                UserStatusNotUsed.Visible = true;
            }
        }



    }

    protected void toggleUser(string _acc_ref,string _toggle)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminDisableUser", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@switch", _toggle));
            cmd.ExecuteNonQuery();

            showUsers();
            UpdatePanelUsers.Update();

        }

    }

    protected void removeLocation(string _locID)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminRemoveLocation", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@tier_ref", _locID));
            cmd.ExecuteNonQuery();

            showLocations();

        }


    }

    public void locationList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string locID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "locRemove":
                removeLocation(locID);
                break;

        }


    }

    public void tableContentUsers_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string _acc_ref = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemDisable":
                toggleUser(_acc_ref,"D");
                break;
            case "ItemEnable":
                toggleUser(_acc_ref, "");
                break;

        }

    }

    public static bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }

    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }

    protected void goToDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }



}

