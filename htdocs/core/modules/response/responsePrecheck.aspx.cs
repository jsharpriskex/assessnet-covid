﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ASNETNSP;
using Telerik.Web.UI;
using Convert = System.Convert;

public partial class responsePreCheck : System.Web.UI.Page
{

    public string corpCode;
    public string yourAccRef;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }

        corpCode = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        migrateUserDetails();
    }

    protected void migrateUserDetails()
    {

        string _isValid = "";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseMigrateUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", yourAccRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                _isValid = objrs["Valid"].ToString();
            }

            if (_isValid == "YES")
            {
                Response.Redirect("~/core/modules/response/responseMain.aspx");
                Response.End();
            } else
            {
                Response.Redirect("~/core/modules/response/layout/module_page.aspx");
                Response.End();
            }


            cmd.Dispose();

        }




    }

}


