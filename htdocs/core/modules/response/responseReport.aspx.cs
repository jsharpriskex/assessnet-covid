﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ASNETNSP;
using Telerik.Web.UI;


public partial class responseReport : System.Web.UI.Page
{
    public string corpCode;
    public string yourAccRef;
    public string accLang;
    public string recordRef;
    public string _q1 = "", _q2 = "", _q3 = "", _q4 = "", _q5 = "", _q6 = "", _q7 = "", _q8 = "", _q9 = "", _q10 = "", _q11 = "";
    public string _caseFirstName;
    public string _caseSurName;
    public string _caseEmail;
    public string _caseNumber;
    public string _caseJobTitle;
    public string caseReference;
    public string _assessmentComments;
    public string adminUser;

    //Get permissions
    public string globalUserPerms;
    public string currentUserAccess;
    public bool modulePermission;
    public string recordPermission;
    public string hazReportAnon;

    //Structure variables
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;

    //Get user location
    public string company;
    public string location;
    public string department;
    public string tier5;
    public string tier6;

    //Portal specific
    public int portalReadOnly;
    public string valPortalAccount;

    public string emailAddress;
    public string _accessLink = "";
    public string _accessSalt = "";
    public string _quickLink = "";
    public string _corpcode = "";
    public string _accRef = "";
    public string _emailFirstname = "";

    public TranslationServices ts;

    protected void Page_Load(object sender, EventArgs e)
    {


        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session.aspx"); Response.End(); }

        Session.Timeout = 90;

        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        corpCode = Context.Session["CORP_CODE"].ToString();
        adminUser = Context.Session["YOUR_ACCESS"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();
        //portalReadOnly = (int)Context.Session["PTL_READ_ONLY"];
        //END OF SESSION STATUS CHECK
        //***************************

        //***************************
        // INITIATE AND SET TRANSLATIONS
        ts = new TranslationServices(corpCode, accLang, "hazardreporting");
        // END TRANSLATIONS
        //***************************

        goToMainMenu.Text = ts.GetResource("back_to_menu");
        goToSearch.Text = ts.GetResource("back_search");

        valPortalAccount = portalReadOnly == 0 ? "N" : "Y";

        globalUserPerms = SharedComponants.getPreferenceValue(corpCode, yourAccRef, "GLOBAL_USR_PERM", "pref");

        recordRef = "CASE01";


        if (adminUser == "1" || adminUser == "0")
        {
            goToMainMenu.Visible = true;
            goToDashboard.Visible = true;
            goToSearch.Visible = true;
            divAssessmentComplete.Visible = false;
            li_header.Visible = true;
            //li_item1.Visible = true;
            //li_item2.Visible = true;

        }


        if (!IsPostBack)
        {

            if (Session["SETUP_ADMIN"] != null)
            {
                if (Session["SETUP_ADMIN"].ToString() == "Y")
                {
                    divAssessmentCompleteSetup.Visible = true;
                    goToDashboard.Visible = false;
                    goToMainMenu.Visible = false;
                    goToSearch.Visible = false;


                }
            }

            // Ok lets see if this is a new assessment or not and create a new reference
            if (!String.IsNullOrEmpty(Request.QueryString["ref"]))
            {
                recordRef = Request.QueryString["ref"].ToString();
                //As part of edit process, we now need to display all sections and return all info
            }
            else
            {

                //recordRef = Refererences.GenerateRecordReference("HAZR", corpCode);

                SharedComponants.insertHistory(corpCode, recordRef, yourAccRef, "Created ", "HAZR");

            }

            ReturnAssessmentData();

        }


    }
    public override void Dispose()
    {
        if (ts != null)
        {
            ts.Dispose();
        }
        base.Dispose();
    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect(valPortalAccount == "Y" ? "../../../version3.2/modules/acentre/" : "layout/module_page.aspx");
    }
    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }
    protected void goToDash_Click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }


    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "Riskex Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.Priority = MailPriority.High;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }


            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }




    public void showAlerts()
    {

        // based on the data, show key alerts
        if (_q1 == "YES") { alert01.Visible = true; q1his.Visible = true; }
        if (_q2 == "YES") { alert02.Visible = true; q2his.Visible = true; }
        if (_q3 == "YES") { alert08.Visible = true; }
        if (_q6 == "YES") { alert04.Visible = true; }
        if (_q4 == "NO") { alert03.Visible = true; q4his.Visible = true; }

        if ((_q1 == "YES" || _q2 == "YES") && _q6 == "NO") { q6his.Visible = true; }

        if (_q6 == "NO")
        {

            // We only need this warning if the symptoms are within 2 weeks.
            bool overTwoWeeks = false;
            DateTime symptomDate;
            if (DateTime.TryParse(q5_ans.Text, out symptomDate))
            {
                overTwoWeeks = (symptomDate - DateTime.Now).TotalDays < 14;
            }

            if (!overTwoWeeks && _q3 != "YES")
            {
                alert07.Visible = true;
            }
            else
            {
                q6his.Visible = false;
            }


        }
        if (_q8 == "YES" && _q3 != "YES") { alert05.Visible = true; q8his.Visible = true; }
        if (_q10 == "YES") { alert06.Visible = true; }
        if (_q10 == "NO") { alert10.Visible = true; q10his.Visible = true; }
        if (_q11 == "YES") { alert09.Visible = true; q11his.Visible = true; }


        if (_q7 == "0") { q7his.Visible = true; }
        if (_q9 == "NO" && _q3 != "YES") { q9his.Visible = true; }



    }



    public void showQuestions()
    {

        if (_q1.Length >= 1) { q1row.Visible = true; q1_ans.Text = _q1; }
        if (_q2.Length >= 1) { q2row.Visible = true; q2_ans.Text = _q2; }
        if (_q3.Length >= 1) { q3row.Visible = true; q3_ans.Text = _q3; }
        if (_q4.Length >= 1) { q4row.Visible = true; q4_ans.Text = _q4; }
        if (_q5.Length >= 1) { q5row.Visible = true; q5_ans.Text = _q5; }
        if (_q6.Length >= 1) { q6row.Visible = true; q6_ans.Text = _q6;  }
        if (_q7.Length >= 1) { q7row.Visible = true; q7_ans.Text = _q7;  }
        if (_q8.Length >= 1) { q8row.Visible = true; q8_ans.Text = _q8;  }
        if (_q9.Length >= 1) { q9row.Visible = true; q9_ans.Text = _q9;  }
        if (_q10.Length >= 1) { q10row.Visible = true; q10_ans.Text = _q10; }
        if (_q11.Length >= 1) { q11row.Visible = true; q11_ans.Text = _q11; }
        if (_assessmentComments.Length >=1 ) { assessmentCommentsDiv.Visible = true; assessmentComments.Text = _assessmentComments; }


    }



    public void ReturnAssessmentData()
    {
        bool isItFirstAssessment = false;
        string _acc_ref = "";
        string[] ages = new string[5];
        ages[0] = "Not Specified";
        ages[1] = "Less than 20";
        ages[2] = "20 to 39";
        ages[3] = "40 to 59";
        ages[4] = "60 or older";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseReadAssessment", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@case_reference", recordRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                litCompanyName.Text = objrs["setupCompanyName"].ToString();
                lit_person.Text = objrs["acc_firstname"].ToString() + " " + objrs["acc_surname"].ToString();
                _emailFirstname = objrs["acc_firstname"].ToString();
                _acc_ref = objrs["acc_ref"].ToString();
                lit_datetime.Text = objrs["gendatetime"].ToString();
                lit_dateduration.Text = objrs["duration"].ToString(); ;
                lit_jobtitle.Text = objrs["acc_jobtitle"].ToString();
                lit_contactphone.Text = objrs["acc_contactNumber"].ToString();
                lit_contactemail.Text = objrs["acc_contactEmail"].ToString();
                emailAddress = objrs["acc_contactEmail"].ToString();
                lit_agerange.Text = ages[Convert.ToInt32(objrs["acc_agerange"].ToString())];
                tier2Ref = objrs["tier2"].ToString();
                tier3Ref = objrs["tier3"].ToString();
                tier4Ref = objrs["tier4"].ToString();
                tier5Ref = objrs["tier5"].ToString();
                tier6Ref = objrs["tier6"].ToString();

                _q1 = objrs["q1"].ToString();
                _q2 = objrs["q2"].ToString();
                _q3 = objrs["q3"].ToString();
                _q4 = objrs["q4"].ToString();
                _q5 = objrs["q5"].ToString();
                _q6 = objrs["q6"].ToString();
                _q7 = objrs["q7"].ToString();
                _q8 = objrs["q8"].ToString();
                _q9 = objrs["q9"].ToString();
                _q10 = objrs["q10"].ToString();
                _q11 = objrs["q11"].ToString();
                _assessmentComments = objrs["case_comments"].ToString();

                showQuestions();
                showAlerts();

            }

            cmd.Dispose();
            objrs.Dispose();

        }


        if (isItTheFirstAssessment(_acc_ref, corpCode))
        {

            string emailSubject;
            string emailTitle;
            string emailBody;
            string emailLink;
            string access_link;


            //This will grab there normal link
            createAccessLink(_acc_ref, corpCode, emailAddress);

            access_link = Application["CONFIG_PROVIDER_SECURE"] + "/login/?t=HAM&u=" + _accessLink + _accessSalt;

            emailLink = "<a href='" + access_link + "'>" + access_link + "</a>";
            emailSubject = "RISKEX COVID-19 Health Assessment";
            emailTitle = "What to do if your health changes (COVID-19)";
            emailBody = "<p>Hi " + _emailFirstname + ",</p><p>If your health or symptoms change in any way, use this link to update your assessment:</p><p>link: " + emailLink + "</p><p>&nbsp;</p><p>It is your duty to keep us informed about your current health status, using the Riskex health assessment tool.  Please help us beat COVID-19.</p><p>You will receive a reminder to repeat the assessment after 72 hours, even if nothing has changed.  Please log in and do this, to keep your record up to date</p><h2>Thank you</h2>";

            Boolean emailSent;
            emailSent = SendEmail(emailAddress, emailSubject, emailTitle, emailBody, "template", true);

        }



    }

    protected bool isItTheFirstAssessment(string acc_ref, string corp_code)
    {

        string assessmentCount = "0";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCountAssessments", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", acc_ref));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                assessmentCount = objrs["countAssessments"].ToString();
            }

            cmd.Dispose();
            objrs.Dispose();

        }

        if (Convert.ToInt32(assessmentCount) == 1)
        {
            return true;
        } else
        {
            return false;
        }


    }


    public int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }


    public void createAccessLink(string _acc_ref, string _corp_code, string _emailAddress)
    {

        // New unique ID with some salt
        _accessLink = Guid.NewGuid().ToString();
        _accessSalt = RandomNumber(100000, 999999).ToString();
        _accessLink = _accessLink.Replace("-", "");

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseProvideAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corp_code));
            cmd.Parameters.Add(new SqlParameter("@ip_address", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _accessSalt));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                _accessLink = objrs["access_link"].ToString();
                _accessSalt = objrs["access_salt"].ToString();

            }

            cmd.Dispose();

        }


    }


}

