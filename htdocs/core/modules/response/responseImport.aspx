﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responseImport.aspx.cs" Inherits="responseImport" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Dashboard</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>


    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />





    <style type="text/css">
        .postby {float: right; color: Gray; font-size: 0.8em;}

    .tile-heading {
    padding: 5px 8px;
    text-transform: uppercase;
    background-color: #1E91CF;
    color: #FFF;
    }

        .tile-body {
            padding: 15px;
            color: #FFFFFF;
            line-height: 48px;
        }

    .fa-user:before {
    content: "\f007";

}

    .tile {
    margin-bottom: 15px;
    border-radius: 3px;
    background-color: #279FE0;
    color: #FFFFFF;
    transition: all 1s;
}
    .tile-footer {
    padding: 5px 8px;
    background-color: #3DA9E3;
}

    .tile a {
    color: #FFFFFF;
}


    .tile .tile-body i {
    font-size: 50px;
    opacity: 0.3;
    transition: all 1s;
}

    .tile .tile-body h2 {
    font-size: 27px;
    margin: 5px;
    padding: 5px;
}

    </style>






</head>
<body runat="server" id="pageBody">
    <form id="pageform" runat="server">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(CheckSessionStatus);

        function CheckSessionStatus(sender, args) {
            $.ajax({
                url: "<% =Application["CONFIG_PROVIDER"] + "/core/system/sessions/ajaxSessions.ashx" %>",
                type: "POST",
                async: false,
                success: function (data) { if (data != "") { window.parent.location = data; } },
                error: function () { alert("An error occured retrieving your user session. Please contact support"); }
            });
        }



    </script>






    <div class="container-fluid">
    <div class="row">

    <asp:UpdatePanel id="UpdatePanelStats" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <div runAt="server" id="reportMenu" class="subpageNavListMargin" style="padding-top: 10px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header">
            Navigation & Statistics
          </li>

          <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" Text="Back to main menu" OnClick="goToMainMenu_Click" />
          <asp:Button ID="goToSearch" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to search" OnClick="goToSearch_Click" />
          <asp:Button ID="goToDashboard" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to Dashboard" OnClick="goToDashboard_Click" />

       </ul>
    </div>

    </ContentTemplate>
    </asp:UpdatePanel>


    <div runAt="server" id="reportContent" class="mainpageNavList"> <!-- Main Col Start -->

        <div class="container-fluid">
            <div class="row">
                <div runat="server" id="corporateLogo" class="corporate_logo col-md-3">
                    <img src='../../../version3.2/data/documents/pdf_centre/img/corp_logos/<%=var_corp_code %>.jpg'  />
                </div>
                <div runat="server" id="pageTitle" class="page_title col-md-5">
                    <h1>Health Assessment ( COVID-19 ) User Import Tool</h1>
                    <span>Information valid as of <%=DateTime.Now %></span>
                </div>
            </div>
        </div>



    <div class="subpageNavListMobile" style="display:none;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Navigation & Options <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>

    <div class="row">
    <div class="col-md-7">


    <asp:UpdatePanel ID="UpdatePanelImport" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Import Email Addresses</h3>
            </div>
            <div class="panel-body">

                <asp:TextBox id="caseUserList" runat="server" Rows="25" style="width:100%;" TextMode="MultiLine" Text="" placeholder="Copy / Paste Email Address(s), based on the seperator you choose." />





            </div>
        </div>




    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="importsubmit" EventName="Click"  />
        </Triggers>
    </asp:UpdatePanel>


    </div>
    <div class="col-md-5">


    <asp:UpdatePanel ID="UpdatePanelControls" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Options / Info</h3>
            </div>
            <div class="panel-body">

                <p>This import tool is to help with adding emails on bulk for users who do not have a corporate email account, such as home workers.  Users are only able to register themselves if they either have an account on the system already or use an email which matches one of your registered domains.</p>
                <p>For those who are having to use their personal email accounts, you can add them here on bulk.  This will then register that email account to your licence and enable them to start the assessment process.</p>

                <div class="alert alert-info">
                Please list the email addresses you wish to create accounts for and select the seperator you are using between each email from the drop down below.
                </div>

                </p>





                <asp:Literal runat="server" ID="caseUserListImportTotalTaken" Visible="false" />
                <asp:Literal runat="server" ID="caseUserListImportTotal" visible="false"/>

                <table class="table table-bordered table-searchstyle01">
                    <tbody>
                        <tr>
                            <th class="text-left" style="width:180px;">Seperation Method</th>
                            <td>
                                <asp:DropDownList runat="server" CssClass="form-control" ID="importSeperation" >
                                    <asp:ListItem Value="newLine">New Line</asp:ListItem>
                                    <asp:ListItem Value="comma">Comma (,)</asp:ListItem>
                                    <asp:ListItem Value="colon">Semi-colon (;)</asp:ListItem>
                                </asp:DropDownList>


                            </td>
                        </tr>
                    </tbody>
                </table>

                <asp:Button runat="server" ID="importsubmit" CssClass="btn btn-lg btn-block btn-success" Text="Import Email Addresses" OnClick="importUsers_Click" />



            </div>
        </div>




    </ContentTemplate>
    </asp:UpdatePanel>


    </div>
    </div>


    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

</body>
</html>
