﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responseMain.aspx.cs" Inherits="responseMain"  MaintainScrollPositionOnPostback="false" EnableEventValidation="false" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Form</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />


    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>


    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?22" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />
    <script src="../../framework/js/assessnet.js"></script>
    <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-13178301-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-13178301-5');
    </script>

    <style>

       html,body {

           margin:10px;
           padding:0px;

            background: url('background-covid-v3-trans.png') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/

       }

     .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                display: none;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            content: "NO";
            background-color: #5cb85c;
        }

        input:focus + .slider {
            content: "NO";
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }


        .radioGroup {
        /*margin:4px;*/
        overflow: auto;
        /*display:inline-block;*/
        width:auto;
        }

        .parentRadioGroup {
            overflow:auto;
        }

        .radioGroup label {
            /*float:left;*/
            min-width:75px;
            /*margin:4px;*/
            background-color:white;
            border-radius:4px;
            border:1px solid #D0D0D0;
            overflow:auto;
            /*padding: 2px;*/


        }

        .radioGroup label span {
            text-align:center;
            font-size: 13px;
            padding:6px 12px;
            display:block;
            font-weight: 100;
        }

        .radioGroup label input {

            display:none;
        }

        /*.radioGroup input:checked + span {
            background-color:#5cb85c;
            color:#F7F7F7;
        }*/

        .radioGroup .green input:checked + span {
            background-color:#5cb85c;
            color:#F7F7F7;
        }

        .radioGroup .orange input:checked + span {
            background-color:#e27036;
            color:#F7F7F7;
        }

        .radioGroup .red input:checked + span {
            background-color:#d9534f;
            color:#F7F7F7;
        }

        .radioGroup .greyRB input:checked + span {
            background-color:#8c8b8b;
            color:#F7F7F7;
        }


        .radioGroup .* {
            background-color:white;
            color:black;
        }

         .btn-extramargin{
             margin:5px;
             min-width: 125px;
         }

         .controlRow{
             padding-bottom:20px;
             border: solid 1px #dddddd;
	        border-radius: 5px
         }

         .inputError{
             background-color:#fdc8c8 !important;
         }


         .comment{
             margin-top:10px;
         }

         .buttonGroup{
             /*border: solid 1px #dddddd;
             border-radius:3px;*/
             overflow: auto;
             float:right;

             -webkit-margin-before: 1.33em;
             padding:10px;

             margin-bottom:10px;
         }

         .buttonGroup > div > a{
             margin: 0 5px !important;
             width: 150px !important;
             text-align:center;
         }

         .buttonGroup > div > a:first-child{
             margin: 0 0px !important;
             width: 150px !important;
             text-align:center;
         }



         .inlineDropDown{
             display: inline-block;
            margin-bottom: 0px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            user-select: none;
            background-image: none;
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            border-image: initial;
            border-radius: 4px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
            height:34px;
            width:90px;
         }


          .lowRisk {
            background-color:#5cb85c;
            color:#F7F7F7;
        }

        .mediumRisk {
            background-color:#e27036;
            color:#F7F7F7;
        }

        .highRisk {
            background-color:#d9534f;
            color:#F7F7F7;
        }

        .naRisk {
            background-color:#8c8b8b;
            color:#F7F7F7;
        }

        .inlineButtonNotButton{
             display: inline-block;
            margin-bottom: 0px;
            font-size: 15px;
            font-weight: 600;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            user-select: none;
            background-image: none;
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            border-image: initial;
            border-radius: 4px;
            border-color: #ccc;
            height:34px;
            width:90px;
         }


        .riskSummaryCount{

            height:35px;
            border: 1px solid white;
            border-radius: 5px;
            font-size:20px;
            font-weight: 600;
            padding-top: 2px;
        }

        .st-left {display:none !important;}
        .sharethis-inline-follow-buttons {padding-right:5px !important;}

    </style>

</head>

<body>
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>

    <script type="text/javascript" language="javascript">


        function ShowDatePopup2() {
            $find("<%= srch_assessmentDateStartSel.ClientID %>").showPopup();
                }
    </script>

        <!-- Secure Info Modal -->
        <div class="modal fade" id="secureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="secureModalLabel">Peace of mind - Security is in our DNA at Riskex</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12">


                        <div class="media">
                          <div class="media-left">


                    <%--            <a href="#">
                                  <img src="img/secure-ency-logo.jpg" />
                                </a>--%>

                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">We ensure your data is safe, secure and handled correctly (GDPR).</h4>
                                <ul style="padding-top:10px;font-size:17px;">
                                    <li>Our service uses our core AssessNET platform, a cloud H&S service for 20 years, based around robust architecture and security measures.</li>
                                    <li>We are audited and certified by BSI to ISO 27001 Information Security Management, along with ISO 9001 in Quality Systems and Cyber Essentials.</li>
                                    <li>Our UK hosting provider Rackspace has ISO 27001 and PCI DSS, SOC 1, 2, 3 level security.</li>
                                    <li>Personal data for the COVID tool is totally encrypted, and the tool provides a secure connection at all times.</li>
                                    <li>We request minimal data from the user required for assessment and analytical purposes of COVID-19 at organisational level.</li>
                                    <li>Data from assessments and users is accessible by the organisation (you) by way of administrator permissions only.</li>
                                    <li>If a full deletion is requested, it will be done on request.  At the end of the scheme (31st October 2020) all data will be available to the organisation to export in full, prior to full deletion.</li>
                                    <li>Data is backed up to our UK Servers, held encrypted at all times and no backup data will be kept after the 31st October 2020, or if deletion is requested prior.</li>
                                    <li>Your data will not be shared with any third-party at any time.</li>
                                </ul>
                          </div>
                        </div>



                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->



                     <!-- Welcome Modal -->
                    <div class="modal fade" id="AdminWelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">


                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="AdminWelModalLabel">Welcome to your COVID-19 Health Assessment</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                <div class="col-xs-12 text-left">

                                <p style="font-size:17px;">Before we can introduce you to your company dashboard, please take just 30 seconds of your time to undertake your own COVID-19 Health Assessment.</p>

                                <p style="font-size:17px;">Not only will this kick start your service, but also allow you to fully understand the assessment 2-step process your employees will undertake.</p>

                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Start Assessment</button>
                                </div>
                            </div>
                            </div>


                        </div>
                        </div>
                    </div>
                    <!-- End of Modal -->


                    <!-- Remove Modal -->
                    <div class="modal fade" id="valModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">


                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="runRemoveModalLabel">Assessment Incomplete</h4>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                <div class="col-xs-12 text-left">
                                    Nearly there! Before your assessment can be submitted, please ensure you have filled in <strong>ALL</strong> your details in the top section.
                                </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                                </div>
                            </div>
                          </div>


                        </div>
                      </div>
                    </div>
                    <!-- End of Remove Modal -->


        
                    <!-- Remove Modal -->
                    <div class="modal fade" id="exitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">


                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exitModalLabel">Assessment Incomplete</h4>
                          </div>
                          <div class="modal-body">
                              <div class="row">
                                <div class="col-xs-12 text-left">
                                    <strong>Are you sure you want to cancel?</strong>: Your assessment is not complete, it takes just 30 seconds.  Please help to save lives.
                                </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">

                                <asp:Button runat="server" class="btn btn-danger" ID="buttonConfirmCancel" OnClick="cancelAssessment_Click" Text="Cancel Assessment and Exit" />&nbsp;
                                <button type="button" class="btn btn-success" data-dismiss="modal">Continue Assessment</button>

                                </div>
                            </div>
                          </div>


                        </div>
                      </div>
                    </div>
                    <!-- End of Remove Modal -->


    <div class="container-fluid">
    <div class="row">


    <div class="subpageNavListMargin" style="padding-top: 19px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header" runat="server" id="li_header" visible="false">
            Navigation
          </li>
            <li class="list-group-item" runat="server" id="li_item1" visible="false">
                <a href="#anchorGeneralDetails">Your Details</a>
            </li>
            <li class="list-group-item" runat="server" id="li_item2" visible="false">
                <a href="#anchorAssessment">Assessment</a>
            </li>
         <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" Text="" OnClick="goToMainMenu_Click" visible="false" />


        <div style="background-color:#ffffff;padding:15px;border:solid 1px #ddd;margin-top:10px;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:4px;border-bottom-right-radius: 4px;" class="text-center">

           <a href="https://www.riskex.co.uk" target="_blank"><img src="img/AssessNET-Web2016.png" /></a>

            <div class="media" onclick="$('#secureModal').modal('show');" style="padding-bottom:10px;cursor:pointer;">
                <div class="media-left">

                <%--     <a href="#">
                        <img src="secure-ency-logo.jpg" />
                    </a>--%>

                </div>
                <div class="media-body">
                <h4 class="media-heading"><i class="fas fa-lock"></i> Peace of mind<br />your security</h4>
                Learn how we ensure your data is safe and secure.

                </div>
            </div>


           <p>AssessNET contains 20 additional Health and Safety Modules.</p>
               
               <uL class="text-left" style="margin-left:15px;">
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/risk-assessment-software/" target="_blank" style="color:#c00c00;">Risk Assessment</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/hazard-reporting/" target="_blank" style="color:#c00c00;">Hazard Reporting</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/dse-assessment/" target="_blank" style="color:#c00c00;">DSE Assessment</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/safety-inspection-online-software/" target="_blank" style="color:#c00c00;">Safety Inspection</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/" target="_blank" style="color:#c00c00;">More...</a></li>
               </uL>
             
           <img src="img/crown-commercial-services-logo.png" style="margin-top:5px;padding:10px;" />
           <img src="setup/img/Fit2Work-300dpi-ReMake.jpg" style="margin-top:5px;" />

        </div>

        </ul>





    </div>

    <div class="mainpageNavList"> <!-- Main Col Start -->

        <div class="row" style="padding-bottom:10px;">
        <div class="col-xs-8">
                <h1 style="padding-top:5px;padding-bottom:5px;font-size:30px;">Welcome <asp:Literal runat="server" ID="litFirstName" /> <br /><span style="font-size:26px">Your Health Assessment (COVID-19)</span></h1>
                <span style="font-size:26px;">For <asp:Literal runat="server" ID="litCompanyName" /></span>
        </div>
        <div class="col-xs-4 text-right" align="right">
             <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" style="width:235px;"/></a>
             <div class="sharethis-inline-follow-buttons"></div>
         </div>
        </div>



        <div runat="server" id="section_1" class="row section_1">
            <div class="col-xs-12" id="pros">

                            <asp:UpdatePanel ID="UpdateGeneralDetails" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                            <asp:HiddenField runat="server" ID="isManualEntry_HiddenValue" Value="1" />

                            <asp:Button runat="server" id="btnSelectUserHidden" style="display: none;" />
                            <asp:Button runat="server" id="btnManualDetailsHidden" style="display: none;" />

                            <div id="reportedByDetails" runat="server">

                                <div class="row">
                                    <div class="col-xs-6" id="enterdetails">

                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                            <div class="panel panel-default panel-subpanel" id="headingOneContainer" runat="server">
                                                <div class="panel-heading click" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" OnClick="headingOne_clicked();">
                                                    <h4 class="panel-title">
                                                        <a role="button" aria-expanded="true" aria-controls="collapseOne">
                                                            <i class="fa fa-bars" aria-hidden="true"></i>Confirm your details <a href="#" id="anchorGeneralDetails"></a>

                                                        </a>
                                                    </h4>
                                                </div>
                                                <div runat="server" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">

                                                        <div class="row" runat="server" id="caseNameRow" visible="false">
                                                            <div class="col-xs-6">
                                                                <div class="form-group">
                                                                    <label for="hazFirstName">First Name</label>
                                                                    <span class="form-control"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="form-group">
                                                                    <label for="hazSurname">Surname</label>
                                                                    <span class="form-control"></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="hazJobTitle">Position / Job Title</label>
                                                                    <asp:TextBox runat="server" class="form-control" id="caseJobTitle"  aria-describedby="jobTitle"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="form-group">
                                                                    <label for="hazContactEmail">Contact Number</label>
                                                                    <asp:TextBox runat="server" class="form-control" id="caseContactNumber" aria-describedby="contactNumber"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6" runat="server" id="emailarea" visible="false">
                                                                <div class="form-group">
                                                                    <label for="hazContactEmail">Contact Email</label>
                                                                    <span class="form-control"><asp:Literal runat="server" id="caseContactEmail" ></asp:Literal></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6" runat="server" id="ageArea">
                                                                <div class="form-group">
                                                                    <label for="hazContactEmail">Your Age Range</label>
                                                                    <asp:DropDownList runat="server" ID="caseAgeRange" class="form-control" >
                                                                        <asp:ListItem Value="0">-- Select Range --</asp:ListItem>
                                                                        <asp:ListItem Value="1">Less than 20</asp:ListItem>
                                                                        <asp:ListItem Value="2">20 - 39</asp:ListItem>
                                                                        <asp:ListItem Value="3">40 - 59</asp:ListItem>
                                                                        <asp:ListItem Value="4">60 or older</asp:ListItem>
                                                                    </asp:DropDownList>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="col-xs-6" >

                                      <div class="panel panel-default" runat="server" >
                                      <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-xs-10">
                                                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Confirm your general office location </h3>
                                                </div>
                                                <div class="col-xs-2 text-right">

                                                </div>
                                            </div>
                                      </div>

                                      <div class="panel-body">

                                        <div class="row">
                                        <div class="col-xs-12">
                                        <div class="form-group" id="step5_GenDetails">

                                            <asp:UpdatePanel ID="UpdatePanel_structure" runat="server">
                                                <ContentTemplate>

                                                    <div class="form-group">

                                                        <table class="table table-bordered table-searchstyle01" runat="server">
                                                            <tr>
                                                                <th>Current locations available</th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <!-- Will only show for new setups -->
                                                                    <asp:DropDownList runat="server" ID="structureTierSolo" Visible="false" class="form-control" Style="width: 99% !important; display: inline;"></asp:DropDownList>
                                                                    <!-- Will only show for AssessNET clients -->
                                                                    <asp:DropDownList runat="server" ID="structureTier1" class="form-control" OnSelectedIndexChanged="structureTier1_SelectedIndexChanged" AutoPostBack="true" Style="width: 95% !important; display: inline;"></asp:DropDownList><asp:CompareValidator ID="structureTier1_Validator" ValidationGroup='Group1' runat="server" ControlToValidate="structureTier1" ErrorMessage=" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>" Operator="NotEqual" ValueToCompare="0" ForeColor="#c00c00" Display="Dynamic" />
                                                                    <asp:DropDownList runat="server" ID="structureTier2" class="form-control" Style="margin-top: 3px !important; width: 95% !important; display: inline;" OnSelectedIndexChanged="structureTier2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><asp:CompareValidator ID="structureTier2_Validator" ValidationGroup='Group1' runat="server" ControlToValidate="structureTier2" ErrorMessage=" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>" Operator="NotEqual" ValueToCompare="0" ForeColor="#c00c00" Display="Dynamic" />
                                                                    <asp:DropDownList runat="server" ID="structureTier3" class="form-control" Style="margin-top: 3px !important; width: 95% !important; display: inline;" OnSelectedIndexChanged="structureTier3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><asp:CompareValidator ID="structureTier3_Validator" ValidationGroup='Group1' runat="server" ControlToValidate="structureTier3" ErrorMessage=" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>" Operator="NotEqual" ValueToCompare="0" ForeColor="#c00c00" Display="Dynamic" />
                                                                    <asp:DropDownList runat="server" ID="structureTier4" class="form-control" Style="margin-top: 3px !important; width: 95% !important; display: inline;" OnSelectedIndexChanged="structureTier4_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList><asp:CompareValidator ID="structureTier4_Validator" ValidationGroup='Group1' runat="server" ControlToValidate="structureTier4" ErrorMessage=" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>" Operator="NotEqual" ValueToCompare="0" ForeColor="#c00c00" Display="Dynamic" />
                                                                    <asp:DropDownList runat="server" ID="structureTier5" class="form-control" Style="margin-top: 3px !important; width: 95% !important; display: inline;"></asp:DropDownList><asp:CompareValidator ID="structureTier5_Validator" ValidationGroup='Group1' runat="server" ControlToValidate="structureTier5" ErrorMessage=" <i class='fa fa-exclamation-triangle' aria-hidden='true'></i>" Operator="NotEqual" ValueToCompare="0" ForeColor="#c00c00" Display="Dynamic" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Label ID="structureDebug" runat="server"></asp:Label>

                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="structureTier1" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="structureTier2" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="structureTier3" EventName="SelectedIndexChanged" />
                                                    <asp:AsyncPostBackTrigger ControlID="structureTier4" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>


                                       </div>
                                       </div>
                                       </div>

                                     </div>
                                     </div>


                                    </div>
                                </div>

                            </div>

                            </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel runat="server" ID="updatePanel01" UpdateMode="Conditional">
                            <ContentTemplate>

                            <div class="row">
                            <div class="col-xs-12">

                            <div class="panel panel-default">
                              <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Assessment <a href="#" id="anchorAssessment"></a></h3>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                    </div>
                              </div>

                            <div class="panel-body">


                            <div class="table">
                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Question</th>
                                        <th colspan="2">Response</th>
                                    </tr>
                                </thead>
                                <tbody>
                                          <tr id="q1row"  class="questionRow" runat="server" >
                                            <td>Have you officially tested positive for COVID-19

                                            </td>
                                            <td width="10px" id="q1control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" OnCheckedChanged="runAssessment" AutoPostBack="true" ID="q1_yes" GroupName='q1' />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q1control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" OnCheckedChanged="runAssessment" AutoPostBack="true" ID="q1_no" GroupName='q1' />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr id="q2row"  class="questionRow" runat="server" visible="false">
                                            <td>Do you currently have any of the following symptoms:

                                                <p>
                                                <ul>

                                                    <li><strong>high temperature</strong>:<br />this means you feel hot to touch on your chest or back (you do not need to measure your temperature)</li>
                                                    <li><strong>new, continuous cough</strong>:<br />this means coughing a lot for more than an hour, or 3 or more coughing episodes in 24 hours (if you usually have a cough, it may be worse than usual)</li>
                                                    <li><strong>loss or change to your sense of smell or taste</strong>:<br />this means you've noticed you cannot smell or taste anything, or things smell or taste different to normal</li>

                                                    <asp:Literal runat="server" ID="test" />

                                                </ul>
                                                </p>

                                            </td>
                                            <td width="10px" id="q2control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q2_yes" OnCheckedChanged="runAssessment" AutoPostBack="true" GroupName='q2' />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q2control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q2_no" OnCheckedChanged="runAssessment" AutoPostBack="true" GroupName='q2' />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr id="q3row"  class="questionRow" runat="server" visible="false">
                                            <td>Have you previously had the above symptoms and recovered?

                                                <asp:HiddenField ID="HiddenField8" Value='111' runat="server" />
                                            </td>
                                            <td width="10px" id="q3control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q3_yes" OnCheckedChanged="runAssessment" AutoPostBack="true" GroupName='q3' />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q3control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q3_no" OnCheckedChanged="runAssessment" AutoPostBack="true" GroupName='q3' />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                             </tbody>
                           </table>
                       </div>

                            <div class="alert alert-danger" role="alert" id="Inf_warning01" runat="server" visible="false">

                                     <div class="media">
                                      <div class="media-left">
                                          <i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i>
                                      </div>
                                      <div class="media-body">
                                        <h4 class="media-heading">Infection Notice</h4>

                                          <p>You could have contracted COVID-19, it is important you self-isolate from others.</p>

                                      </div>
                                    </div>

                             </div>

                            <div class="table" runat="server" visible="false" id="q4table">
                                <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left;">Question</th>
                                            <th colspan="2">Response</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                              <tr id="q4row"  class="questionRow" runat="server" visible="false">
                                                <td>Have you contacted NHS 111 since developing symptoms?

                                                </td>
                                                <td width="10px" id="q4control_ans1" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q4_yes" GroupName='q4' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                            <span>Yes</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10px" id="q4control_ans2" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q4_no" GroupName='q4' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                            <span>No</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                  </tr>

                                 </tbody>
                               </table>
                             </div>

                            <div class="alert alert-info" role="alert" id="Inf_warning02" runat="server" visible="false">

                            <div class="media">
                                <div class="media-left">
                                      <i class="fa fa-3x fa-info-circle" aria-hidden="true"></i>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">Notice</h4>

                                      <p>As you are showing possible symptoms of COVID-19, it is important that you use the NHS Coronavirus website for more advice - visit  <a href='https://www.111.nhs.uk' target="_parent">www.111.nhs.uk</a>. If you are unable to use the online service, call the NHS on 111.</p>

                                  </div>
                                </div>


                            </div>

                            <div class="table" runat="server" visible="false" id="q5table">
                                <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left;">Question</th>
                                            <th width="100px" colspan="2">Response</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                <tr id="q11row"  class="questionRow" runat="server" visible="false">
                                                <td>Do you suffer from any underlying health conditions?

                                                </td>
                                                <td width="10px" id="q11control_ans1" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q11_yes" GroupName='q11' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                            <span>Yes</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10px" id="q11control_ans2" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q11_no" GroupName='q11' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                            <span>No</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                  </tr>

                                             <tr id="q5row"  class="questionRow" runat="server" visible="false">
                                                <td>On what date did symptoms first occur?

                                                </td>
                                                <td width="10px" id="q5control_ans1" runat="server" colspan="2">

                                                    <div class="input-group datepick"  id="assessmentDateStart" style="width:100%">
                                                        <telerik:RadDatePicker runat="server" id="srch_assessmentDateStartSel" OnSelectedDateChanged="runAssessment" AutoPostBack="true" width="165px" Skin="Bootstrap" >
                                                            <DateInput>
                                                                <ClientEvents OnFocus="ShowDatePopup2" />
                                                            </DateInput>
                                                            <Calendar runat="server">
                                                                <SpecialDays>
                                                                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-BackColor="Silver">
                                                                    </telerik:RadCalendarDay>
                                                                </SpecialDays>
                                                            </Calendar>
                                                        </telerik:RadDatePicker>
                                                    </div>


                                                </td>

                                            </tr>

                                            <tr id="q6row"  class="questionRow" runat="server" visible="false">
                                                <td>Are you currently self-isolating from others?


                                                </td>
                                                <td width="10px" id="q6control_ans1" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q6_yes" GroupName='q6' OnCheckedChanged="runAssessment" AutoPostBack="true"  />
                                                            <span>Yes</span>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td width="10px" id="q6control_ans2" runat="server">
                                                    <div class="parentRadioGroup text-center">
                                                        <div class="radioGroup">
                                                            <label class="green white">
                                                            <asp:RadioButton runat="server" ID="q6_no" GroupName='q6' OnCheckedChanged="runAssessment" AutoPostBack="true"  />
                                                            <span>No</span>

                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>


                                 </tbody>
                               </table>
                           </div>

                            <div class="alert alert-danger" role="alert" id="Inf_warning03" runat="server" visible="false">
                                <div class="media">
                                  <div class="media-left">
                                      <i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">Infection Notice</h4>

                                      <p>You should self-isolate from others to prevent cross-infection immediately. <a href='https://www.nhs.uk/conditions/coronavirus-covid-19/self-isolation-advice/' target="_blank">Click Here</a> for information and guidance with regards to self-isolation.</p>

                                  </div>
                                </div>
                            </div>

                            <div class="table" runat="server" visible="false" id="q7table">
                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Question</th>
                                        <th colspan="2">Response</th>
                                    </tr>
                                </thead>
                                <tbody>

                                        <tr id="q7row"  class="questionRow"  runat="server" visible="false">
                                            <td>How many people live in your house / apartment with you?


                                            </td>
                                            <td id="q7control_ans1" runat="server" colspan="2">

                                                <asp:DropDownList runat="server" ID="q7_ans" CssClass="form-control" OnSelectedIndexChanged="runAssessment" AutoPostBack="true"  >
                                                    <asp:ListItem>Select Amount</asp:ListItem>
                                                    <asp:ListItem>0</asp:ListItem>
                                                    <asp:ListItem>1</asp:ListItem>
                                                    <asp:ListItem>2</asp:ListItem>
                                                    <asp:ListItem>3</asp:ListItem>
                                                    <asp:ListItem>4</asp:ListItem>
                                                    <asp:ListItem>5 or more</asp:ListItem>
                                                </asp:DropDownList>


                                            </td>

                                        </tr>
                                        <tr id="q8row"  class="questionRow"  runat="server" visible="false">
                                            <td>Are any of the people in your household showing the symptoms above?

                                            </td>
                                            <td width="10px" id="q8control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q8_yes" GroupName='q8' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q7control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q8_no" GroupName='q8' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        <tr id="q9row"  class="questionRow" runat="server" visible="false">
                                            <td>Are those showing symptoms currently self-isolated from you?


                                            </td>
                                            <td width="10px" id="q9control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q9_yes" GroupName='q9' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q8control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q9_no" GroupName='q9' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>

                                        <tr id="q10row"  class="questionRow" runat="server" visible="false">
                                            <td>Are you feeling well enough to work?


                                            </td>
                                            <td width="10px" id="q10control_ans1" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q10_yes" GroupName='q10' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>Yes</span>

                                                    </div>
                                                </div>
                                            </td>
                                            <td width="10px" id="q10control_ans2" runat="server">
                                                <div class="parentRadioGroup text-center">
                                                    <div class="radioGroup">
                                                        <label class="green white">
                                                        <asp:RadioButton runat="server" ID="q10_no" GroupName='q10' OnCheckedChanged="runAssessment" AutoPostBack="true" />
                                                        <span>No</span>

                                                    </div>
                                                </div>
                                            </td>

                                        </tr>

                             </tbody>
                           </table>
                       </div>


                            <div class="alert alert-info" role="alert" id="successAlert" runat="server" visible="false">

                                <div class="media">
                                  <div class="media-left">
                                      <i class="fa fa-3x fa-check" aria-hidden="true"></i>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading">Assessment Complete</h4>

                                      <p>Thank you for taking the time to complete this assessment. To share any other information that you think is important for us to know, such as your mental health, stress or changes in your personal circumstances, please use the text area below.</p>
                                      <p>Once ready, please click on <strong>"Submit Assessment"</strong> to finalise and notify management of your results.</p>

                                  </div>
                                </div>
                            </div>


                            <div class="table" runat="server" visible="false" id="assessmentCommentsDiv">

                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Additional Comments - report temperature plus other symptoms and concerns</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><asp:Textbox runat="server" TextMode="MultiLine" Rows="3" ID="assessmentComments" CssClass="form-control" placeholder="Are there any additional comments you would like to add?" /></td>
                                    </tr>
                                </tbody>
                            </table>

                            </div>



                            </div>

                            <div class="panel-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">

                                    <asp:Button runat="server" CssClass="btn btn-danger"  ID="assessmentCancel" Text="Cancel Assessment and Exit" OnClientClick="$('#exitModal').modal('show');" Visible="false" />
                                    <asp:Button runat="server" CssClass="btn btn-success"  ID="assessmentSubmit" Visible="false" Text="Submit Assessment" OnClick="submitAssessment_Click" />

                                </div>
                            </div>
                            </div>

                            </div>

                            </div>
                            </div>

                            </ContentTemplate>
                            </asp:UpdatePanel>

                            <a href="#assessmentAnchor" id="assessmentAnchor" class="assessmentAnchor" >&nbsp;</a>














            </div>
        </div>

    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    <br/><br />

    </form>
    <%--script src="dropzone/dropzone.js" type="text/javascript"></script>--%>
       <script type="text/javascript" src="../../framework/js/intro.js"></script>
    <script type="text/javascript">


        function headingTwo_clicked() {
            var btnHidden = $('#<%= btnSelectUserHidden.ClientID %>');
            if (btnHidden != null) {
                btnHidden.click();
            }
        }

        function headingOne_clicked() {
            var btnHidden = $('#<%= btnManualDetailsHidden.ClientID %>');
            if (btnHidden != null) {
                btnHidden.click();
            }
        }

        function BindControlEvents() {

            $(function () {
                $('#datetimepicker8').datetimepicker({
                    maxDate: moment(),
                    format: 'DD/MM/YYYY HH:mm',
                    sideBySide: true,
                    allowInputToggle: true,
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
            });




        }

        //Initial bind
        $(document).ready(function(){

            BindControlEvents();

        });

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function() {
            BindControlEvents();
        });


    </script>



</body>
</html>
