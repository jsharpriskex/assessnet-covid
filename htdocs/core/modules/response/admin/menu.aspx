﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="menu.aspx.cs" Inherits="responseHeaderMenu" validateRequest="false" enableEventValidation="false" %>
<%@ Import Namespace="ASNETNSP" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head runat="server">
    <title>Health Assessment ( COVID-19 ) - Request Access Menu</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

            <meta name="robots" content="noindex" />
        <meta property="og:title" content="AssessNET: COVID-19 Assessment Service" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://www.assessweb.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1600" />
        <meta property="og:image:height" content="900" />
        <meta property="og:url" content="https://www.assessweb.co.uk/" />
        <meta name="twitter:card" content="summary_large_image" />


    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">

    <link href="../../../framework/css/custom.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="../../../framework/js/assessnet.js"></script>
    <link href="../../../framework/css/custom.css" rel="stylesheet" />
    <asp:Literal runat="server" ID="cssLinkClientStylesheet" Text="" />

      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
    <form id="pageform" runat="server">
        <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <div id="top_menu">
            <ul>
                <li class="active"><a href="../../../../version3.2/data/functions/func_exit.asp" target='_parent'><i class="fa fa-fw fa-sign-out" aria-hidden="true"></i> Log out</a></li>
            </ul>
            <div style="position:absolute; right:0; top:-5px;">
                <img id="menuSwish" src="../../../media/images/core/AssessNETMenuSwishv4.png" style="position:relative; right:0; top:0;" />
                <img id="corpLogo" runat="server" src="../../../media/images/core/assessnet-logo.png" style="position:absolute; right:12px; top:6px; z-index:99; max-height: 37px;" />
            </div>

        </div>

    </form>
</body>
</html>
