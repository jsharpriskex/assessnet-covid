﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class ReponseAuthAdmin : System.Web.UI.Page
{

    public string _accessLink;

    protected void Page_Load(object sender, EventArgs e) {

        if (!IsPostBack)
        {

            if (Session["AdminLink"] != null)
            {
                _accessLink = Session["AdminLink"].ToString();
                getAdminUserDetails(_accessLink);
            } else
            {
                Response.Redirect("~/core/modules/response/admin/nolink.aspx");
                Response.End();
            }

        }

    }

    public void getAdminUserDetails(string _access_Link)
    {

        SqlDataReader objrs = null;
        SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCheckAdminRequiresPassword", GetDatabaseConnection());
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@access_link", _access_Link));
        objrs = cmd.ExecuteReader();

        if (objrs.HasRows)
        {
            objrs.Read();

            Session["CORP_CODE"] = objrs["corp_code"].ToString();
            Session["YOUR_ACCOUNT_ID"] = objrs["acc_ref"].ToString();
            Session["YOUR_EMAIL"] = objrs["acc_email"].ToString();

        }

        if (Session["CORP_CODE"].ToString().Length < 10)
        {
            // no record, its not valid, password has already been changed
            Response.Redirect("~/core/modules/response/admin/expiredlink.aspx");
            Response.End();
        }

        cmd.Dispose();
        objrs.Dispose();

    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }

    public bool DoPasswordsMatch(string _password1, string _password2)
    {
        // Clean the data before compare

        string password1 = Regex.Replace(_password1, @"\s", "");
        string password2 = Regex.Replace(_password2, @"\s", "");

        bool result = password1.Equals(password2, StringComparison.Ordinal);

        return result;

    }

    protected void setPassword(string _password)
    {

        string _salt = ASNETNSP.Authentication.setSalt(512);
        string _passwordHash = ASNETNSP.Authentication.setSHA512Hash(_password, _salt);
        string _corpCode = Session.Contents["Corp_Code"].ToString();
        string _accRef = Session.Contents["YOUR_ACCOUNT_ID"].ToString();

        //string _salt = ASNETNSP.Authentication.setSalt(512);
        //string _passwordHash = ASNETNSP.Authentication.setSHA512Hash(_password, _salt);

        // Set the password and create the admin account whilst there.
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminSetPassword", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@password", _passwordHash));
            cmd.Parameters.Add(new SqlParameter("@salt", _salt));
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
            cmd.ExecuteNonQuery();
        }

    }

    protected void buttonVerify_Click(object sender, EventArgs e)
    {


        string _password1 = authPass1.Text;
        string _password2 = authPass2.Text;

        authPass1.Text = "";
        authPass2.Text = "";

        if (_password1.Length > 7)
        {
            if (_password1.Any(char.IsUpper))
            {
                if (DoPasswordsMatch(_password1, _password2))
                {

                    setPassword(_password1);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "window.parent.location = 'http://covid.riskex.co.uk/admin/'", true);


                }
                else
                {
                    alert.Visible = true;
                    alertarea.Attributes.Add("Class", "alert alert-danger");
                    msg.Text = "<strong>Alert: </strong>The passwords you entered do not match, please try again. Remember, passwords are CaSe sensitive";
                }

            }
            else
            {
                alert.Visible = true;
                alertarea.Attributes.Add("Class", "alert alert-danger");
                msg.Text = "<strong>Alert: </strong>The password is the right length, but it does not contain a combination of lower and UPPER case letters.";
            }

        }
        else
        {
            alert.Visible = true;
            alertarea.Attributes.Add("Class", "alert alert-danger");
            msg.Text = "<strong>Alert: </strong>The password you selected is too short, please ensure it is at least 8 characters long";
        }


    }



}

