﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using System.Globalization;

public partial class responseAdmin : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string yourAccessLevel;
    int _accUserCountTotalInput = 0;
    int _accUserCountTotalInputTaken = 0;


    protected void Page_Load(object sender, EventArgs e)
    {

        //***************************
        // CHECK THE SESSION STATUS
        SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");


        if (!IsPostBack)
        {



        }


    }



    public static bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }


    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("../layout/module_page.aspx");
    }

    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("../responseSearch.aspx");
    }

    protected void goToDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("../dashboard.aspx");
    }

}

