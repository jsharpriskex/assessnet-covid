﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="reset.aspx.cs" Inherits="resetAdmin" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Riskex - Health Assessment (COVID-19) Platform</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

        <meta name="robots" content="noindex" />
        <meta property="og:title" content="AssessNET: COVID-19 Assessment Service" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://www.assessweb.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1600" />
        <meta property="og:image:height" content="900" />
        <meta property="og:url" content="https://www.riskex.co.uk/" />
        <meta name="twitter:card" content="summary_large_image" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />


    <style>



       body p {

            font-size:17px;

        }

       html,body {
           margin:10px;
           padding:0px;

            background: url('../layout/background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

       .textInput {
            height: 45px;
            font-size: 30px;
            text-align: center;
            background:#efefef;

        }

    </style>


</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
<div id="backgroundIMG">


    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


    <div class="container">
    <div class="row">





     <div class="row">
            <div class="col-xs-12 text-center">

                    <h2>Reset your Dashboard Admin Password<br /> <asp:Literal runat="server" ID="companyTitleName" /></h2>

            </div>
        </div>


        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;color:#000000"><i class="fa fa-bars" aria-hidden="true"></i>Validate your email to request password</h3>
                  </div>
                  <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="../layout/Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>




                    <asp:UpdatePanel ID="UpdatePanelInfoArea" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                    <asp:HiddenField runat="server" id="refcode" />

                    <div class="row">
                    <div class="col-xs-12" align="center" class="text-center">
                    <div class="form-group">


                              <div id="emailArea" runat="server" style="padding-top:15px;">

                                  <table width="650px">
                                  <tbody>
                                  <tr><td>

                                  <div class="form-group">

                                  </div>


                                        <div class="row" runat="server" id="emailAreaBox">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="caseSurname" style="font-size:17px;color:#000000">Email Address: </label>
                                                    <asp:Textbox ID="userEmail" Runat="server" autocomplete="off" style="width: 100%;" CssClass="textInput form-control email-input" placeholder="ENTER YOUR EMAIL ADDRESS" />
                                                </div>
                                            </div>
                                        </div>

                                  </td></tr>
                                  </tbody>
                                  </table>


                              </div>

                              <div id="ConfirmMessage" runat="server" style="padding-top:25px;" visible="false">

                                  <h1>Password reset email has been sent to</h1>
                                  <h2 style="color:#c00c00;"><asp:Literal runat="server" ID="emailAddressOutput" /></h2>


                                    <div class="alert alert-success text-left" style="margin-top:30px;">

                                        <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                      <i class="fas fa-envelope-open-text fa-5x" style="color:#357234;padding-top:10px;"></i>
                                                    </a>
                                                  </div>
                                              <div class="media-body">
                                                <div class="col-md-12" style="font-size:17px;">

                                                <p style="font-size:17px;">Once you receive the email, click the link to reset your password. Please make sure you check JUNK/SPAM folders and you may need to wait a few minutes for it arrive.</p>
                                                <p style="font-size:17px;"><strong>If the email address above is incorrect, please use the button below to start over</strong></p>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                              </div>

                    </div>
                    </div>
                    </div>

                            <asp:Literal runat="server" id="test2" />

                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alertArea" visible="false">
                                 <p><asp:Literal runat="server" ID="alertAreaMsg" /></p>
                            </div>


                    </ContentTemplate>
                       <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="buttonAccessEmail" EventName="Click"  />
                       </Triggers>
                    </asp:UpdatePanel>


                    </div>





                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:updatepanel id="buttonUpdate" runat="server" UpdateMode="conditional">
                                <ContentTemplate>

                                    <asp:Button ID="buttonAccessReset" runat="server" Visible="false" class="btn btn-lg btn-danger" Text="Re-Enter Email and Start Again" OnClick="buttonAccessReset_Click"  />
                                    <asp:Button ID="buttonAccessEmail" runat="server" class="btn btn-lg btn-success"  Text="Continue" OnClick="buttonAccessEmail_Click"  />

                                </ContentTemplate>
                            </asp:updatepanel>

                        </div>
                    </div>
                    </div>



                </div>



        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->


    <script type="text/javascript">




        $(document).ready(function () {

            BindControlEvents();



        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });



    </script>



    </form>
    <br />
    <br />




</div>
</body>
</html>
