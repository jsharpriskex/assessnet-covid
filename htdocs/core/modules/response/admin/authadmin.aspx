﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="authadmin.aspx.cs" Inherits="ReponseAuthAdmin" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Riskex - Health Assessment (COVID-19) Platform</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

     <style>
     body p {

            font-size:16px;

        }
    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>




    <div class="container">
    <div class="row">



     <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Health Assessment ( COVID-19 )</h1>

            </div>
        </div>


        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Dashboard User Registration</h3>
                  </div>
                  <div class="panel-body">

                    <asp:UpdatePanel ID="UpdatePanelInfoArea" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>


                    <div class="row">
                        <div class="col-md-12">

                            <h1>Secure your dashboard account</h1>

                            <p>Please set your password with a minimum of 8 characters and containing at least one number and Upper / Lowercase characters.</p>

                            <p>&nbsp;</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">


                                  <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Set a password</h3>
                                  </div>
                                  <div class="panel-body">


                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="caseFirstName">Create a Password</label>
                                                    <asp:TextBox runat="server" class="form-control" id="authPass1"  TextMode="password"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label for="caseSurname">Confirm your Password</label>
                                                    <asp:TextBox runat="server" class="form-control" id="authPass2" TextMode="password" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>


                                  </div>
                                  </div>


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <div class="alert alert-warning">
                                <p>Once your password is set, you will be redirected to the login page: <strong>https://covid.riskex.co.uk/admin/</strong></p>
                            </div>

                        </div>
                    </div>

                    <div class="row" runat="server" id="alert" visible="false">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="alert alert-warning" role="alert" runat="server" id="alertarea">
                            <p><asp:Literal runat="server" ID="msg" /></p>

                            </div>
                        </div>
                    </div>




                           <asp:Literal runat="server" ID="test" />
                    </ContentTemplate>
                       <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="buttonVerify" EventName="Click"  />
                       </Triggers>
                    </asp:UpdatePanel>


                    </div>


                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button ID="buttonVerify" runat="server" class="btn btn-success"  Text="Save Password and Login" onClick="buttonVerify_Click"  />

                        </div>
                    </div>
                    </div>



                </div>

        </div>
        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
