﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responseControl.aspx.cs" Inherits="responseControl" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Dashboard</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>


    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />


    <style type="text/css">

 .bizLocation {
    padding-bottom:10px;
    padding-top:10px;
    margin-top:5px;
    margin-bottom:5px;
    font-size:16px;
}


    </style>


</head>
<body runat="server" id="pageBody">
    <form id="pageform" runat="server">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>

    <div class="container-fluid">
    <div class="row">


        <!-- Add Location Modal -->
        <div class="modal fade" id="addLocModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addLocModalLabel">Add New Location</h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel id="UDP_LocationModal" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                    <div class="row">
                    <div class="col-xs-12">

                        <div class="alert alert-warning" style="margin-bottom:5px;">
                            <asp:TextBox runat="server" ID="addLocationText" class="form-control" MaxLength="50"  />
                        </div>

                    </div>
                    </div>

                    </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="saveLocation" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>


                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close with saving</button>
                    <asp:Button runat="server" ID="saveLocation" class="btn btn-success" text="Save Location" OnClick="saveLocation_Click" />
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Add User Modal -->
        <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addUserModalLabel">Add New User</h4>
                </div>
                <div class="modal-body">

                    <asp:UpdatePanel id="UDP_UserModal" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:HiddenField runat="server" ID="userUpdateType" Value="Add" />

                    <div class="row">
                    <div class="col-xs-12">

                        <div class="alert alert-warning" style="font-size:16px;">
                            <p>Enter the details below of the new dashboard user you wish to add.  Once the user has been added, they will receive an email giving them instructions to set a password and access this admin panel</p>
                        </div>

                        <div class="form-group">
                        <label for="setupCompanyName">First Name</label>
                            <asp:Textbox runat="server" ID="firstname_admin" autocomplete="off" class="form-control" Text="" />
                        </div>

                        <div class="form-group">
                        <label for="setupCompanyName">Surname</label>
                            <asp:Textbox runat="server" ID="surname_admin" autocomplete="off" class="form-control" Text="" />
                        </div>

                        <div class="form-group">
                        <label for="setupCompanyName">Email Address (Username)</label>
                            <asp:Textbox runat="server" ID="emailAddress_admin" autocomplete="off" class="form-control" Text="" />
                        </div>

                        <div class="form-group">
                        <label for="setupCompanyName">Contact Number</label>
                            <asp:Textbox runat="server" ID="contactNumber_admin" autocomplete="off" class="form-control" Text="" />
                        </div>

                        <div class="form-group">
                        <label for="setupCompanyName">Job Role</label>

                        <asp:DropDownList runat="server" ID="role_admin" class="form-control" >
                            <asp:ListItem Value="NA" Text="-- Select Role --" />
                            <asp:ListItem Value="Administrator" Text="Administrator" />
                            <asp:ListItem Value="Company Director" Text="Company Director" />
                            <asp:ListItem Value="Executive Manager" Text="Executive Manager" />
                            <asp:ListItem Value="Health and Safety Manager" Text="Health and Safety Manager" />
                            <asp:ListItem Value="Health and Safety Officer" Text="Health and Safety Officer" />
                            <asp:ListItem Value="HR Manager" Text="HR Manager" />
                            <asp:ListItem Value="Line Manager" Text="Line Manager" />
                            <asp:ListItem Value="Occupational Health" Text="Occupational Health" />
                            <asp:ListItem Value="Other" Text="Other" />
                        </asp:DropDownList>
                        </div>


                        <div runat="server" id="pwAlertArea" visible="false">
                                <div class="alert alert-success" runat="server" id="pwAlert" style="margin-bottom:0px;" >
                                    <asp:Literal runat="server" ID="pwMsg" />
                                </div>
                        </div>


                        </div>
                        </div>

                        </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="addUser" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>

                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close with saving</button>
                    <asp:Button runat="server" ID="addUser" class="btn btn-success" text="Save User" OnClick="saveUserDetails_Click" />
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->


    <asp:UpdatePanel id="UpdatePanelStats" runat="server" UpdateMode="Conditional">
    <ContentTemplate>

    <div runAt="server" id="reportMenu" class="subpageNavListMargin" style="padding-top: 10px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header">
            Navigation & Statistics
          </li>

          <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" Text="Back to main menu" OnClick="goToMainMenu_Click" />
          <asp:Button ID="goToSearch" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to search" OnClick="goToSearch_Click" />
          <asp:Button ID="goToDashboard" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to Dashboard" OnClick="goToDashboard_Click" />

       </ul>
    </div>

    </ContentTemplate>
    </asp:UpdatePanel>


    <div runAt="server" id="reportContent" class="mainpageNavList"> <!-- Main Col Start -->

        <div class="container-fluid">
            <div class="row">
                <div runat="server" id="pageTitle" class="page_title col-md-5">
                    <h1>Health Assessment ( COVID-19 ) Configuration</h1>
                    <span>Information valid as of <%=DateTime.Now %></span>
                </div>
            </div>
        </div>

    <div class="subpageNavListMobile" style="display:none;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Navigation & Options <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>

    <div class="row">
    <div class="col-md-7">


        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Additional User Accounts</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-3 text-center" runat="server" >
                        <ASP:Button id="addUserAccount" style="margin-top:5px;" runat="server" OnClientClick="$('#addUserModal').modal('show');" class="btn btn-primary" Text="Add a new user" />
                    </div>
                    <div class="col-xs-9">
                        <p>You can use this control to manage the current dashboard users you have setup.  Please note, as the superuser, only you will be able to access this configuration page.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">

                <div id="searchResultsArea" class="searchResultsArea" runat="server">
                <div runat="server" id="DisplayResults" class="DisplayResults">

                <asp:UpdatePanel ID="UpdatePanelUsers" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                            <asp:ListView ID="tableContentUsers" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnItemCommand="tableContentUsers_ItemCommand" OnItemDataBound="tableContentUsers_ItemDataBound">
                                <LayoutTemplate>

                                    <div class="table">
                                    <table class="table table-striped table-bordered table-liststyle01 searchResults">

                                        <thead>
                                            <tr>
                                                <th width="220px">Name</th>
                                                <th>Email Address</th>
                                                <th width="160px">Status</th>
                                                <th width="110px">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                        </tbody>

                                    </table>
                                    </div>



                                </LayoutTemplate>
                                <ItemTemplate>

                                <tr>
                                    <td class="text-center"><%# Eval("per_fname").ToString() %> <%# Eval("per_sname").ToString() %></td>
                                    <td class="text-center"><%# Eval("corp_email").ToString() %></td>
                                    <td class="text-center">
                                        <div runat="server" visible="false" id="UserStatusActive" class="text-center list-group-item list-group-item-success">ACTIVE IN USE</div>
                                        <div runat="server" visible="false" id="UserStatusDisabled" class="text-center list-group-item list-group-item-warning">DISABLED</div>
                                        <div runat="server" visible="false" id="UserStatusNotUsed" class="text-center list-group-item list-group-item-danger">NOT USED YET</div>
                                    </td>
                                    <td class="text-center">

                                        <!-- Complete options -->

                                        <div class="btn-group" id="optionsComplete" runat="server">
                                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Options
                                                <span class="caret"></span>
                                                <span class="sr-only">Toggle Dropdown</span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li runat="server" id="disableOptionli"><asp:LinkButton ID="disableOption" runat="server" CommandName="ItemDisable" CommandArgument='<%# Eval("acc_ref").ToString() %>'>Disabled Account</asp:LinkButton></li>
                                                <li runat="server" id="enableOptionLi" visible="false"><asp:LinkButton ID="enableOption" runat="server" CommandName="ItemEnable" CommandArgument='<%# Eval("acc_ref").ToString() %>'>Enable Account</asp:LinkButton></li>
                                            </ul>
                                        </div>

                                        <!-- End of complete options -->

                                    </td>
                                </tr>


                                </ItemTemplate>
                                <EmptyDataTemplate>

                                    <div class="alert alert-danger" style="margin-bottom:0px;margin-top:10px;font-size:16px;">
                                        <p><strong>Notice</strong>: You have no additional dashboard users associated to your company.</p>
                                    </div>

                                </EmptyDataTemplate>
                                </asp:ListView>


                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="addUserAccount" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                </div>
                </div>

                    </div>
                </div>

            </div>
        </div>


    </div>
    <div class="col-md-5">

        <asp:UpdatePanel ID="UDP_Reminders" runat="server" UpdateMode="Conditional" Visible="false">
        <ContentTemplate>

        <div class="alert alert-warning" >
            <div class="row">
                <div class="col-md-9">
                <strong>Assessment Reminders</strong><br />
                   By default for this FREE edition, reminders can not be changed.  To have a wider range of reminder durations, contact our sales department via <strong>sales@riskex.co.uk</strong> about our Safe2Day package.
                </div>
                <div class="col-md-3 text-center">
                    <asp:DropDownList runat="server" ID="ddl_remindersHours" Enabled="false" CssClass="form-control" style="margin-top:25px;" OnSelectedIndexChanged="ddl_remindersHours_SelectedIndexChanged">
                        <asp:ListItem Value="24">1 Day</asp:ListItem>
                        <asp:ListItem value="48">2 Days</asp:ListItem>
                        <asp:ListItem Value="72">3 Days</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>



        </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddl_remindersHours" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>




        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Locations</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-4 text-center">
                        <ASP:Button id="addLocationModal" style="margin-top:5px;" runat="server" class="btn btn-primary" Text="Add Location" OnClientClick="$('#addLocModal').modal('show');" />
                    </div>
                    <div class="col-xs-8">
                        <p>Below is a list of locations currently assigned to your organisation. You can remove unused locations (those which have no records associated with them already) by clicking <strong>Remove</strong>.</p>
                    </div>
                </div>

                <asp:UpdatePanel ID="UpdatePanelLocations" runat="server" UpdateMode="Conditional">
                <ContentTemplate>


                            <asp:ListView runat="server" ID="locationList" OnItemCommand="locationList_ItemCommand" OnItemDataBound="locationList_ItemDataBound">

                                <ItemTemplate>

                                    <div class="alert alert-success bizLocation">
                                    <i class="fa fa-building-o" aria-hidden="true"></i> <%#Eval("struct_name") %> <div runat="server" visible="false" id="RemoveLocationDiv" class="pull-right"><asp:Button runat="server" CssClass="btn btn-sm btn-danger pull-right" style="padding-top:2px;padding-bottom:2px;" ID="locationRemove" Text="REMOVE" CommandName="locRemove" CommandArgument='<%# Eval("tier2_ref").ToString() %>' /></div>
                                    </div>

                                </ItemTemplate>
                                <EmptyDataTemplate>

                                    <div class="alert alert-danger" style="margin-bottom:0px;margin-top:10px;font-size:16px;">
                                        <p><strong>Notice</strong>:  No locations configured, add one or more.</p>
                                    </div>

                                </EmptyDataTemplate>

                            </asp:ListView>


                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="addLocationModal" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>


    </div>
    </div>


    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

</body>
</html>
