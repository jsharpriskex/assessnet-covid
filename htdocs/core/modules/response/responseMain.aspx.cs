﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ASNETNSP;
using Telerik.Web.UI;
using Convert = System.Convert;


public partial class responseMain : System.Web.UI.Page
{
    public string corpCode;
    public string yourAccRef;
    public string accLang;
    public string recordRef;
    public string _q1 = "", _q2 = "", _q3 = "", _q4 = "", _q5 = "", _q6 = "", _q7 = "", _q8 = "", _q9 = "", _q10 = "", _q11 = "";
    public string _caseFirstName;
    public string _caseSurName;
    public string _caseEmail;
    public string _caseNumber;
    public string _caseJobTitle;
    public string _casecomment;
    public string _caseAgeRange;
    public string adminUser;

    //Get permissions
    public string globalUserPerms;
    public string currentUserAccess;
    public bool modulePermission;
    public string recordPermission;
    public string hazReportAnon;

    //Structure variables
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;

    //Get user location
    public string tier2;
    public string tier3;
    public string tier4;
    public string tier5;
    public string tier6;
    public string _tier2 = "0";
    public string _tier3 = "0";
    public string _tier4 = "0";
    public string _tier5 = "0";
    public string _tier6 = "0";

    //Portal specific
    public int portalReadOnly;
    public string valPortalAccount;

    public TranslationServices ts;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session.aspx"); Response.End(); }

        Session.Timeout = 60;

        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        adminUser = Context.Session["YOUR_ACCESS"].ToString();
        corpCode = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();
        //portalReadOnly = (int)Context.Session["PTL_READ_ONLY"];
        //END OF SESSION STATUS CHECK
        //***************************


        //***************************
        // INITIATE AND SET TRANSLATIONS
        ts = new TranslationServices(corpCode, accLang, "hazardreporting");
        // END TRANSLATIONS
        //***************************

        goToMainMenu.Text = ts.GetResource("back_to_menu");

        valPortalAccount = portalReadOnly == 0 ? "N" : "Y";


        globalUserPerms = SharedComponants.getPreferenceValue(corpCode, yourAccRef, "GLOBAL_USR_PERM", "pref");
        currentUserAccess = "0";

        recordRef = "CASE01";
        srch_assessmentDateStartSel.MaxDate = DateTime.Now;
        srch_assessmentDateStartSel.DateInput.ReadOnly = true;

        if (adminUser == "1" || adminUser == "0")
        {
            goToMainMenu.Visible = true;
            li_header.Visible = true;
            //li_item1.Visible = true;
            //li_item2.Visible = true;
        }


        if (!IsPostBack)
        {

            if (Session["SETUP_ADMIN"] != null)
            {
                if (Session["SETUP_ADMIN"].ToString() == "Y")
                {
                    assessmentCancel.Visible = false;
                    goToMainMenu.Visible = false;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#AdminWelModal').modal('show');", true);

                }
            }

            // Hide the structure elements
            structureTier2.Visible = false;
           structureTier3.Visible = false;
           structureTier4.Visible = false;
           structureTier5.Visible = false;

           structureTier1_populate(0);
           populateSoloStructure();



            //************************
            // GET USER DETAILS BASED ON ACC REF
            // ONLY NEED THIS ONCE, NOT USING SESSION FOR SECURITY
            getUserDetails(yourAccRef,true);
            //************************

            structureTierSolo.SelectedValue = tier2;


        }


    }

    public override void Dispose()
    {
        if (ts != null)
        {
            ts.Dispose();
        }
        base.Dispose();
    }

    protected void cancelAssessment_Click(object sender, EventArgs e)
    {
        if (adminUser == "0" || adminUser == "1")
        {
            Response.Redirect("~/core/modules/response/layout/module_page.aspx");
        }
        else
        {
            Response.Redirect("~/core/modules/response/layout/responseAccess.aspx");
        }
    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect(valPortalAccount == "Y" ? "../../../version3.2/modules/acentre/" : "layout/module_page.aspx");
    }
    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }

    public void getUserDetails(string _yourAccRef, bool retry)
    {

        // Kick out if the user account has needed more than 2 tries



        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", _yourAccRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                litCompanyName.Text = objrs["setupCompanyName"].ToString();
                caseJobTitle.Text = objrs["acc_jobtitle"].ToString();
                litFirstName.Text = objrs["acc_firstname"].ToString();
                caseContactNumber.Text = objrs["acc_contactNumber"].ToString();
                caseContactEmail.Text = objrs["acc_contactEmail"].ToString();
                caseAgeRange.SelectedValue = objrs["acc_agerange"].ToString();
                tier2 = objrs["tier2"].ToString();
                tier3 = objrs["tier3"].ToString();
                tier4 = objrs["tier4"].ToString();
                tier5 = objrs["tier5"].ToString();
                tier6 = objrs["tier6"].ToString();


            }

            cmd.Dispose();
            objrs.Dispose();

        }


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetUserSymptoms", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", _yourAccRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                DateTime symptomDate;
                if (DateTime.TryParse(objrs["q5"].ToString(), out symptomDate))
                {
                    srch_assessmentDateStartSel.SelectedDate = DateTime.Parse(objrs["q5"].ToString());
                }

            }

            cmd.Dispose();
            objrs.Dispose();

        }


    }

    protected void archiveAssessment(string _yourAccRef)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseArchiveAssessment", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _yourAccRef));
            cmd.ExecuteNonQuery();
            cmd.Dispose();



        }



    }

    protected void migrateUserDetails(string _yourAccRef)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseMigrateUserDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", _yourAccRef));
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            // Now get the user details again, but only try one more time before you kick out.
            getUserDetails(_yourAccRef,false);

        }



    }

    public void submitAssessment_Click(Object sender, EventArgs e)
    {

        _caseNumber = caseContactNumber.Text;
        _caseJobTitle = caseJobTitle.Text;
        _casecomment = assessmentComments.Text;
        _caseAgeRange = caseAgeRange.SelectedValue;

        bool _formValid = false;

        // Is this one structure or multiple
        if (structureTierSolo.Visible)
        {
            if (structureTierSolo.SelectedValue.Length > 0) { _tier2 = structureTierSolo.SelectedValue; }
            if (_caseJobTitle.Length > 1 && _caseNumber.Length > 1 && _caseAgeRange != "0" && structureTierSolo.SelectedIndex > 0) { _formValid = true; }

        }
        else
        {

            if (structureTier1.SelectedValue.Length > 0) { _tier2 = structureTier1.SelectedValue; }
            if (structureTier2.SelectedValue.Length > 0) { _tier3 = structureTier2.SelectedValue; }
            if (structureTier3.SelectedValue.Length > 0) { _tier4 = structureTier3.SelectedValue; }
            if (structureTier4.SelectedValue.Length > 0) { _tier5 = structureTier4.SelectedValue; }
            if (structureTier5.SelectedValue.Length > 0) { _tier6 = structureTier5.SelectedValue; }
            if (_caseJobTitle.Length > 1 && _caseNumber.Length > 1 && _caseAgeRange != "0") { _formValid = true; }

        }





        // We need to check the main form is completed
        if (!_formValid)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#valModal').modal('show');", true);


        }
        else
        {

            // Firstly lets save only the questions which are visible

            if (q1row.Visible) { if (q1_yes.Checked) { _q1 = "YES"; } else { _q1 = "NO"; } }
            if (q2row.Visible) { if (q2_yes.Checked) { _q2 = "YES"; } else { _q2 = "NO"; } }
            if (q3row.Visible) { if (q3_yes.Checked) { _q3 = "YES"; } else { _q3 = "NO"; } }
            if (q4row.Visible) { if (q4_yes.Checked) { _q4 = "YES"; } else { _q4 = "NO"; } }
            if (q5row.Visible) { _q5 = srch_assessmentDateStartSel.SelectedDate.ToString(); } else { _q5 = ""; }
            if (q6row.Visible) { if (q6_yes.Checked) { _q6 = "YES"; } else { _q6 = "NO"; } }
            if (q7row.Visible) { _q7 = q7_ans.SelectedValue.ToString(); } else { _q7 = ""; }
            if (q8row.Visible) { if (q8_yes.Checked) { _q8 = "YES"; } else { _q8 = "NO"; } }
            if (q9row.Visible) { if (q9_yes.Checked) { _q9 = "YES"; } else { _q9 = "NO"; } }
            if (q10row.Visible) { if (q10_yes.Checked) { _q10 = "YES"; } else { _q10 = "NO"; } }
            if (q11row.Visible) { if (q11_yes.Checked) { _q11 = "YES"; } else { _q11 = "NO"; } }

            // Go ahead and save it all

            saveUserDetails();
            archiveAssessment(yourAccRef);
            saveAssessmentDetails();

        }

    }

    public void saveUserDetails()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string _caseReference = Guid.NewGuid().ToString();

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseUpdateUser", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", yourAccRef));
            cmd.Parameters.Add(new SqlParameter("@acc_contactnumber", _caseNumber));
            cmd.Parameters.Add(new SqlParameter("@acc_Jobtitle", _caseJobTitle));
            cmd.Parameters.Add(new SqlParameter("@acc_agegroup", _caseAgeRange));
            cmd.Parameters.Add(new SqlParameter("@tier2", _tier2));
            cmd.Parameters.Add(new SqlParameter("@tier3", _tier3));
            cmd.Parameters.Add(new SqlParameter("@tier4", _tier4));
            cmd.Parameters.Add(new SqlParameter("@tier5", _tier5));
            cmd.Parameters.Add(new SqlParameter("@tier6", _tier6));


            cmd.ExecuteNonQuery();


            cmd.Dispose();


        }


    }

    public void saveAssessmentDetails()
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string _caseReference = Guid.NewGuid().ToString();

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCreateAssessment", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@case_reference", _caseReference));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", yourAccRef));
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@q1", _q1));
            cmd.Parameters.Add(new SqlParameter("@q2", _q2));
            cmd.Parameters.Add(new SqlParameter("@q3", _q3));
            cmd.Parameters.Add(new SqlParameter("@q4", _q4));
            cmd.Parameters.Add(new SqlParameter("@q5", _q5));
            cmd.Parameters.Add(new SqlParameter("@q6", _q6));
            cmd.Parameters.Add(new SqlParameter("@q7", _q7));
            cmd.Parameters.Add(new SqlParameter("@q8", _q8));
            cmd.Parameters.Add(new SqlParameter("@q9", _q9));
            cmd.Parameters.Add(new SqlParameter("@q10", _q10));
            cmd.Parameters.Add(new SqlParameter("@q11", _q11));
            cmd.Parameters.Add(new SqlParameter("@case_comments", _casecomment));
            cmd.ExecuteNonQuery();


            cmd.Dispose();

            // Go to report
            Response.Redirect("responseReport.aspx?ref=" + _caseReference);
            Response.End();

        }


    }

    public void runAssessment(object sender, EventArgs e)
    {


        resetVis();


        if (q1_no.Checked && q1row.Visible)
        {

            q2row.Visible = true;

        }
        else if (q1_yes.Checked && q1row.Visible)
        {
            q5table.Visible = true;
            q11row.Visible = true;
        }

        if (q2_no.Checked && q2row.Visible)
        {
            q3row.Visible = true;

        }
        else if (q2_yes.Checked && q2row.Visible)
        {
            Inf_warning01.Visible = true;
            q4table.Visible = true;
            q4row.Visible = true;

        }


        if (q3_no.Checked && q3row.Visible)
        {

            q5table.Visible = true;
            q11row.Visible = true;

        }
        else if (q3_yes.Checked && q3row.Visible)
        {
            q5table.Visible = true;
            q11row.Visible = true;

        }

        if (q4_no.Checked && q4row.Visible)
        {
            Inf_warning02.Visible = true;
            q5table.Visible = true;
            q11row.Visible = true;

        }
        else if (q4_yes.Checked && q4row.Visible)
        {
            q5table.Visible = true;
            q11row.Visible = true;

        }


        if (q11_no.Checked && q3row.Visible || q11_yes.Checked && q3row.Visible)
        {
            q7table.Visible = true;
            q7row.Visible = true;
        }
        else if (q11_no.Checked && q11row.Visible && !q3row.Visible)
        {
            q5table.Visible = true;
            q5row.Visible = true;

        }
        else if (q11_yes.Checked && q11row.Visible && !q3row.Visible)
        {
            q5table.Visible = true;
            q5row.Visible = true;

        }



        DateTime value;
        if (DateTime.TryParse(srch_assessmentDateStartSel.SelectedDate.ToString(), out value))
        {
            q6row.Visible = true;

        }

        if (q6_no.Checked && q6row.Visible)
        {

            // We only need this warning if the symptoms are within 2 weeks.
            bool overTwoWeeks = false;
            DateTime symptomDate;
            if (DateTime.TryParse(srch_assessmentDateStartSel.SelectedDate.ToString(), out symptomDate))
            {
                overTwoWeeks = (DateTime.Now - symptomDate).TotalDays < 14;
            }

            if (overTwoWeeks) { Inf_warning03.Visible = true; }

            q7table.Visible = true;
            q7row.Visible = true;

        }
        else if (q6_yes.Checked && q6row.Visible)
        {
            q7table.Visible = true;
            q7row.Visible = true;
        }


        if (q7_ans.SelectedIndex > 1)
        {
            q8row.Visible = true;

        }
        else if (q7_ans.SelectedIndex == 1)
        {
            q10row.Visible = true;
        }

        if (q8_no.Checked && q8row.Visible)
        {
            q10row.Visible = true;
        }
        else if (q8_yes.Checked && q8row.Visible)
        {
            q9row.Visible = true;
        }


        if (q9_no.Checked && q9row.Visible)
        {
            q10row.Visible = true;
        }
        else if (q9_yes.Checked && q9row.Visible)
        {
            q10row.Visible = true;
        }

        if (q10_no.Checked && q10row.Visible)
        {
            successAlert.Visible = true;
            assessmentSubmit.Visible = true;
            assessmentCommentsDiv.Visible = true;
        }
        else if (q10_yes.Checked && q10row.Visible)
        {
            successAlert.Visible = true;
            assessmentSubmit.Visible = true;
            assessmentCommentsDiv.Visible = true;
        }

        clearNonVisibleOptions();

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('html, body').animate({ scrollTop: $('.assessmentAnchor').offset().top}, 1);", true);


    }

    public void resetVis()
    {

        // Set everything to vis = false
        q2row.Visible = false;
        q3row.Visible = false;
        q4row.Visible = false;
        q5row.Visible = false;
        q6row.Visible = false;
        q7row.Visible = false;
        q8row.Visible = false;
        q9row.Visible = false;
        q10row.Visible = false;
        q11row.Visible = false;

        q4table.Visible = false;
        q5table.Visible = false;
        q7table.Visible = false;

        Inf_warning01.Visible = false;
        Inf_warning02.Visible = false;
        Inf_warning03.Visible = false;
        successAlert.Visible = false;
        assessmentSubmit.Visible = false;
        assessmentCommentsDiv.Visible = false;

    }

    public void clearNonVisibleOptions()
    {
        // Clears all the checkboxes on non visible items (results integrity)
        if (!q2row.Visible) { q2_no.Checked = false; q2_yes.Checked = false; }
        if (!q3row.Visible) { q3_no.Checked = false; q3_yes.Checked = false; }
        if (!q4row.Visible) { q4_no.Checked = false; q4_yes.Checked = false; }
        if (!q6row.Visible) { q6_no.Checked = false; q6_yes.Checked = false; }
        if (!q7row.Visible) { q7_ans.SelectedIndex = 0; }
        if (!q8row.Visible) { q8_no.Checked = false; q8_yes.Checked = false; }
        if (!q9row.Visible) { q9_no.Checked = false; q9_yes.Checked = false; }
        if (!q10row.Visible) { q10_no.Checked = false; q10_yes.Checked = false; }
        if (!q11row.Visible) { q11_no.Checked = false; q11_yes.Checked = false; }

    }

    public void populateSoloStructure()
    {

        structureTierSolo.Visible = true;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetStructure", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();

            // populate the drop down
            structureTierSolo.DataSource = objrs;
            structureTierSolo.DataValueField = "tierRef";
            structureTierSolo.DataTextField = "struct_name";
            structureTierSolo.DataBind();

            // put in a selectable option
            structureTierSolo.Items.Insert(0, "--- Select Option ---");

            ListItem _item = new ListItem("(My Location is not listed)", "-2");
            structureTierSolo.Items.Add(_item);

            cmd.Dispose();

        }


    }



    // Structure Elements

    public void structureTier1_populate(Int32 refID)
    {

        structureTier1.Items.Clear();

        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(corpCode, yourAccRef, "", tier2Ref, "", currentUserAccess, "", "2", ""))
            {
                structureTier1.DataSource = returnedStructure;
                structureTier1.DataValueField = "tierRef";
                structureTier1.DataTextField = "struct_name";
                structureTier1.DataBind();



                if (structureTier1.Items.Count == 0)
                {
                    structureTier1.Visible = false;
                    structureTier1.Items.Clear();

                }
                else
                {

                    //Remove All - for ocado
                    structureTier1.Items.Remove(structureTier1.Items.FindByValue("-1"));

                    if (structureTier1.Items.Count == 0)
                    {
                        structureTier1.Visible = false;
                        structureTier1.Items.Clear();

                    }

                }


                if (structureTier1.Items.Count == 1)
                {
                    structureTier1.Visible = false;
                    structureTier1.Items.Clear();
                }




            }


        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void structureTier1_SelectedIndexChanged(object sender, EventArgs e)
    {

        structureTier2.Visible = true;
        structureTier3.Visible = false;
        structureTier4.Visible = false;
        structureTier5.Visible = false;
        structureTier2.Items.Clear();
        structureTier3.Items.Clear();
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier2StructureInformation();

    }

    public void structureTier2_SelectedIndexChanged(object sender, EventArgs e)
    {

        structureTier3.Visible = true;
        structureTier4.Visible = false;
        structureTier5.Visible = false;
        structureTier3.Items.Clear();
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier3StructureInformation();

    }

    public void structureTier3_SelectedIndexChanged(object sender, EventArgs e)
    {

        structureTier4.Visible = true;
        structureTier5.Visible = false;
        structureTier4.Items.Clear();
        structureTier5.Items.Clear();

        getTier4StructureInformation();

    }

    public void structureTier4_SelectedIndexChanged(object sender, EventArgs e)
    {

        structureTier5.Visible = true;
        structureTier5.Items.Clear();

        getTier5StructureInformation();

    }

    public void getTier2StructureInformation()
    {
        try
        {



            string currentSelection = "0";

            if (structureTier1.SelectedValue != "")
            {
                currentSelection = structureTier1.SelectedValue;
            }


            using (DataTable returnedStructure = Structure.ReturnStructure(corpCode, yourAccRef, currentSelection, tier3Ref, "", currentUserAccess, "", "3", ""))
            {
                structureTier2.DataSource = returnedStructure;
                structureTier2.DataValueField = "tierRef";
                structureTier2.DataTextField = "struct_name";
                structureTier2.DataBind();

                if (structureTier2.Items.Count == 0)
                {
                    structureTier2.Visible = false;
                    structureTier2.Items.Clear();
                }
                else
                {
                    //Remove All - for ocado
                    structureTier2.Items.Remove(structureTier2.Items.FindByValue("-1"));

                    if (structureTier2.Items.Count == 0)
                    {
                        structureTier2.Visible = false;
                        structureTier2.Items.Clear();
                    }
                }


                if (structureTier2.Items.Count == 1)
                {
                    structureTier2.Visible = false;
                    structureTier2.Items.Clear();
                }

            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }
    }

    public void getTier3StructureInformation()
    {

        try
        {
            string currentSelection = "0";

            if (structureTier2.SelectedValue != "")
            {
                currentSelection = structureTier2.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(corpCode, yourAccRef, currentSelection, tier4Ref, "", currentUserAccess, "", "4", ""))
            {
                structureTier3.DataSource = returnedStructure;
                structureTier3.DataValueField = "tierRef";
                structureTier3.DataTextField = "struct_name";
                structureTier3.DataBind();

                if (structureTier3.Items.Count == 0)
                {
                    structureTier3.Visible = false;
                    structureTier3.Items.Clear();
                }
                else
                {
                    //Remove All - for ocado
                    structureTier3.Items.Remove(structureTier3.Items.FindByValue("-1"));

                    if (structureTier3.Items.Count == 0)
                    {
                        structureTier3.Visible = false;
                        structureTier3.Items.Clear();
                    }
                }


                if (structureTier3.Items.Count == 1)
                {
                    structureTier3.Visible = false;
                    structureTier3.Items.Clear();
                }


            }


        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier4StructureInformation()
    {


        string currentSelection = "0";

        if (structureTier3.SelectedValue != "")
        {
            currentSelection = structureTier3.SelectedValue;
        }


        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(corpCode, yourAccRef, currentSelection, tier5Ref, "", currentUserAccess, "", "5", ""))
            {
                structureTier4.DataSource = returnedStructure;
                structureTier4.DataValueField = "tierRef";
                structureTier4.DataTextField = "struct_name";
                structureTier4.DataBind();

                if (structureTier4.Items.Count == 0)
                {
                    structureTier4.Visible = false;
                    structureTier4.Items.Clear();
                }
                else
                {

                    //Remove All - for ocado
                    structureTier4.Items.Remove(structureTier4.Items.FindByValue("-1"));

                    if (structureTier4.Items.Count == 0)
                    {
                        structureTier4.Visible = false;
                        structureTier4.Items.Clear();
                    }
                }


                if (structureTier4.Items.Count == 1)
                {
                    structureTier4.Visible = false;
                    structureTier4.Items.Clear();
                }

            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier5StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (structureTier4.SelectedValue != "")
            {
                currentSelection = structureTier4.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(corpCode, yourAccRef, currentSelection, tier6Ref, "", currentUserAccess, "", "6", ""))
            {
                structureTier5.DataSource = returnedStructure;
                structureTier5.DataValueField = "tierRef";
                structureTier5.DataTextField = "struct_name";
                structureTier5.DataBind();

                if (structureTier5.Items.Count == 0)
                {
                    structureTier5.Visible = false;
                    structureTier5.Items.Clear();
                }
                else
                {
                    //Remove All - for ocado
                    structureTier5.Items.Remove(structureTier5.Items.FindByValue("-1"));


                    if (structureTier5.Items.Count == 0)
                    {
                        structureTier5.Visible = false;
                        structureTier5.Items.Clear();
                    }


                    if (structureTier5.Items.Count == 1)
                    {
                        structureTier5.Visible = false;
                        structureTier5.Items.Clear();
                    }

                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }


    //Structure Elements End


}


