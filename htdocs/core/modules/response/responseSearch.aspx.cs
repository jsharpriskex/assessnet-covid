﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class responseSearch : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string tier2;
    public string tier3;
    public string tier4;
    public string tier5;
    public string tier6;
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;
    public string tier2Name;
    public string tier3Name;
    public string tier4Name;
    public string yourAccessLevel;

    // Search Specific Vars
    public string ssRef;
    public string runSearch;
    public string searchType;
    public int pageNumber;
    public int pageSelection;
    public string uniqueSearchCode;
    public string uniqueHistoryCode;
    public string uniqueSearchTableCode;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }

        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        uniqueSearchCode = "CASE" + yourAccRef.Replace("-","") + var_corp_code;
        uniqueHistoryCode = "CASEH" + yourAccRef.Replace("-","") + var_corp_code;
        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");



        if (!IsPostBack)
        {

            // Hide the structure elements
            srch_structureTier2.Visible = false;
            srch_structureTier3.Visible = false;
            srch_structureTier4.Visible = false;
            srch_structureTier5.Visible = false;

            populateSoloStructure();
            structureTier_populate(0);
            populateQuestions("COVID");

            if (srch_structureTier1.Items.FindByValue(tier2) != null)
            {
                srch_structureTier1.SelectedValue = tier2;
                srch_structureTier2.Visible = true;
                getTier2StructureInformation();
            }

            if (srch_structureTier2.Items.FindByValue(tier3) != null)
            {
                srch_structureTier2.SelectedValue = tier3;
                srch_structureTier3.Visible = true;
                getTier3StructureInformation();
            }

            if (srch_structureTier3.Items.FindByValue(tier4) != null)
            {
                srch_structureTier3.SelectedValue = tier4;
                srch_structureTier4.Visible = true;
                getTier4StructureInformation();
            }

            if (srch_structureTier4.Items.FindByValue(tier5) != null)
            {
                srch_structureTier4.SelectedValue = tier5;
                srch_structureTier5.Visible = true;
                getTier5StructureInformation();
            }

            if (srch_structureTier5.Items.FindByValue(tier6) != null)
            {
                srch_structureTier5.SelectedValue = tier6;
            }


            if (!string.IsNullOrEmpty(Request.QueryString["cStatus"]))
            {
                // Then we have something
                string cStatus = Request.QueryString["cStatus"];
                string _tier2 = Request.QueryString["tier2"];

                if (_tier2 != null)
                {
                    srch_structureTierSolo.SelectedValue = _tier2;
                }

                if (cStatus == "1")
                {
                    srch_question.SelectedValue = "1";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "2")
                {
                    srch_question.SelectedValue = "2";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "3")
                {
                    srch_question.SelectedValue = "6";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "4")
                {
                    srch_question.SelectedValue = "10";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "5")
                {

                    srch_question.SelectedValue = "8";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "6")
                {

                    srch_HasSymNotNHS.Value = "TRUE";
                    srch_question.SelectedValue = "4";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "NO";


                }

                if (cStatus == "7")
                {

                    srch_HomeAlone.Value = "TRUE";

                }

                if (cStatus == "8")
                {

                    srch_question.SelectedValue = "11";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "Yes";

                }

                if (cStatus == "9")
                {

                    srch_question.SelectedValue = "10";
                    questionResponseTR.Visible = true;
                    srch_questionResponse.SelectedValue = "NO";

                }



                Search_Click(null, EventArgs.Empty);


            }

        }

    }


    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }


    // Search Controls Start

    // Both page selection dropdowns grouped together
    protected void returnSelectedPageTop(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigationTop.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void returnSelectedPage(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigation.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void nextPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) + 1;
        returnResultsPage(pageNumber);
        previousPageButton.Enabled = true;
        previousPageButtonTop.Enabled = true;

    }

    protected void previousPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) - 1;
        returnResultsPage(pageNumber);
        nextPageButtonTop.Enabled = true;
        nextPageButton.Enabled = true;

    }

    public void questionList_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (srch_question.SelectedIndex == 0)
        {
            questionResponseTR.Visible = false;
            srch_questionResponse.SelectedIndex = 0;
        }
        else
        {
            questionResponseTR.Visible = true;
        }

        UpdatePanel1.Update();


    }

    public void populateQuestions(string _template)
    {


        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {


            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseTemplateQuestions", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@template_ref", _template));

            objrs = cmd.ExecuteReader();

            srch_question.DataSource = objrs;
            srch_question.DataValueField = "db_col_ref";
            srch_question.DataTextField = "template_question";
            srch_question.DataBind();

            srch_question.Items.Insert(0, "-- Select Question --");


            cmd.Dispose();
            objrs.Dispose();


        }



    }

    public void returnResultsPage(int pageNumber)
    {

        string pageSize = "15";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string sqlText = "execute Module_HA.dbo.caseSearch_Page '" + uniqueSearchCode + "', '" + pageNumber + "', '" + pageSize + "'";

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            //Set the table info
            tableContentResults.DataSource = objrs;
            tableContentResults.DataBind();

            currenPageNumberHidden.Value = pageNumber.ToString();


            sqlComm.Dispose();
            objrs.Close();

            displayPagingCheck("0");
        }
    }

    private void displayPagingCheck(string commandType)
    {
        string sqlText;
        string scriptString;
        string results_addTheS = "s";
        pageNavigationTop.Items.Clear();
        pageNavigation.Items.Clear();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here1');", true);
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            sqlText = "SELECT COUNT(*) as totalrecords FROM TempSearchDB.dbo." + uniqueSearchCode;

            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here2');", true);

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                int totalRecords = Int32.Parse(objrs["totalrecords"].ToString());

                if (totalRecords == 1) { results_addTheS = ""; }


                results_count.Text = totalRecords.ToString() + " result" + results_addTheS + " found";


                results_count.Visible = true;
                updateRecordTotal.Update();

                if (totalRecords > 15)
                {
                    int pageCount = totalRecords / 15;
                    int remainder = totalRecords % 15;
                    if (remainder > 0) { pageCount++; }

                    pageNavigationTop.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    pageNavigation.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    maxPageNumberHidden.Value = pageCount.ToString();

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == 1)
                    {
                        previousPageButtonTop.Enabled = false;
                        previousPageButton.Enabled = false;
                    }
                    else
                    {
                        previousPageButtonTop.Enabled = true;
                        previousPageButton.Enabled = true;
                    }

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == Convert.ToInt32(maxPageNumberHidden.Value))
                    {
                        nextPageButtonTop.Enabled = false;
                        nextPageButton.Enabled = false;
                    }
                    else
                    {
                        nextPageButtonTop.Enabled = true;
                        nextPageButton.Enabled = true;
                    }

                    pageNumber = Convert.ToInt32(currenPageNumberHidden.Value);
                    pageNavigationTop.SelectedValue = pageNumber.ToString();
                    pageNavigation.SelectedValue = pageNumber.ToString();



                    scriptString = "$('.pagingSection').show();$('.pagingSectionTop').show();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";




                }
                else
                {
                    scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";
                }

            }
            else
            {
                results_count.Visible = false;
                results_count.Text = "0";

                scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.DisplayResults').show();";
            }


            //switch (commandType)
            //{
            //    case "OpenInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('show');";
            //        break;
            //    case "CloseInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('hide');";
            //        break;
            //    case "CloseLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('hide');";
            //        break;
            //    case "OpenLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('show');";
            //        break;

            //}

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", scriptString, true);

            sqlComm.Dispose();
            objrs.Close();

        }

        //return sqlText;
    }

    public void returnResults()
    {
        string sqlText = "";
        try
        {

            string Keyword = srch_Keyword.Text;
            string _case_status = srch_show.SelectedValue;
            string _dateRangeType = srch_dateRangeType.SelectedValue;
            string _casedate = srch_DateSel.SelectedDate.ToString();
            string _casedateStart = srch_DateSel.SelectedDate.ToString();
            string _casedateEnd = srch_DateEndSel.SelectedDate.ToString();
            string _question = srch_question.SelectedValue;
            string _question_response = srch_questionResponse.SelectedValue;

            string _HomeAlone = srch_HomeAlone.Value;
            string _HasSymNotNHS = srch_HasSymNotNHS.Value;

            string tier2Selection = "";

            if (srch_structureTierSolo.Visible)
            {
                tier2Selection = (srch_structureTierSolo.SelectedValue.Length < 5) ? srch_structureTierSolo.SelectedValue : "0";
            }
            else
            {
                tier2Selection = (srch_structureTier1.SelectedValue != "") ? srch_structureTier1.SelectedValue : "0";
            }

            string tier3Selection = (srch_structureTier2.SelectedValue != "") ? srch_structureTier2.SelectedValue : "0";
            string tier4Selection = (srch_structureTier3.SelectedValue != "") ? srch_structureTier3.SelectedValue : "0";
            string tier5Selection = (srch_structureTier4.SelectedValue != "") ? srch_structureTier4.SelectedValue : "0";
            string tier6Selection = (srch_structureTier5.SelectedValue != "") ? srch_structureTier5.SelectedValue : "0";

            string pageSize = "15";
            string Tempvar_corp_code = var_corp_code;




            if (pageNumber == 0)
            {
                pageNumber = 1;
            }

            string sortResultsBySelection = "person";
            string sortResultsByOrder = "asc";

            string yourAccountID = yourAccRef; //former session
            string yourCompany = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_COMPANY", "user");
            string yourLocation = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_LOCATION", "user");
            string yourDepartment = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_DEPARTMENT", "user");
            string userPermsActive = (SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref") == "Y") ? "1" : "0";


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "execute Module_HA.dbo.caseSearch '" + Tempvar_corp_code + "', '" + _HomeAlone + "', '" + _HasSymNotNHS + "','" + Keyword + "', '" + _dateRangeType + "','" + _casedate + "','" + _casedateStart + "', '" + _casedateEnd + "', '" + _case_status + "','" + _question + "','" + _question_response + "','" + tier2Selection + "', '" + tier3Selection + "', '" + tier4Selection + "', '" + tier5Selection + "', '" + tier6Selection + "', '" + sortResultsBySelection + "', '" + sortResultsByOrder + "', '" + yourAccessLevel + "', '" + yourAccountID + "', '" + yourCompany + "', '" + yourLocation + "', '" + yourDepartment + "', '" + pageNumber + "', '" + pageSize + "', '" + uniqueSearchCode + "','0'";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

                tableContentResults.DataSource = objrs;
                tableContentResults.DataBind();

                currenPageNumberHidden.Value = "1";

                previousPageButtonTop.Enabled = false;
                previousPageButton.Enabled = false;

                objrs.Close();
                sqlComm.Dispose();
            }



        }
        catch (Exception)
        {
            throw;
        }


    }

    protected void restoreRecord(string recID)
    {

        string sqlText = "0";

        try
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "UPDATE Module_HA.dbo.HAM_CaseEntries " +
                          "SET case_status = '1' " +
                          "Where corp_code = '" + var_corp_code + "' and case_reference = '" + recID + "'";

                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                sqlComm.ExecuteNonQuery();

                // Refresh the results
                returnResults();
                displayPagingCheck("0");
                UpdateSearchResults.Update();


            }


        }
        catch (Exception ex)
        {
            throw ex;
        }


    }

    protected void removeRecord(string recID)
    {

        string sqlText = "0";

        try
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "UPDATE Module_HA.dbo.HAM_CaseEntries " +
                          "SET case_status = '0' " +
                          "Where corp_code = '" + var_corp_code + "' and case_reference = '" + recID + "'";

                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                sqlComm.ExecuteNonQuery();

                // Refresh the results
                returnResults();
                displayPagingCheck("0");
                UpdateSearchResults.Update();


            }


        }
        catch (Exception ex)
        {
            throw ex;
        }


    }

    protected void showHistory(string recID)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseShowHistory", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", recID));
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            LV_History.DataSource = objrs;
            LV_History.DataBind();


        }

    }

    public void tableContentHistory_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string recID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemReport":
                Response.Redirect("responseReport.aspx?ref=" + recID);
                break;

        }

    }

    protected void tableContentHistory_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        string symptomsDate = DataBinder.Eval(e.Item.DataItem, "q5").ToString();
        string covidconfirmed = DataBinder.Eval(e.Item.DataItem, "q1").ToString();
        string covidSymptoms = DataBinder.Eval(e.Item.DataItem, "q2").ToString();
        string covidRecovered = DataBinder.Eval(e.Item.DataItem, "q3").ToString();
        string riskofinfection = DataBinder.Eval(e.Item.DataItem, "q8").ToString();
        string nhsNotInformed = DataBinder.Eval(e.Item.DataItem, "q4").ToString();
        string riskofspread = DataBinder.Eval(e.Item.DataItem, "q6").ToString();
        string fitforwork = DataBinder.Eval(e.Item.DataItem, "q10").ToString();
        string healthIssues = DataBinder.Eval(e.Item.DataItem, "q11").ToString();

        HtmlGenericControl viewOptionli = (HtmlGenericControl)e.Item.FindControl("viewOptionli");

        HtmlGenericControl infectionStatusConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusConfirmed");
        HtmlGenericControl infectionStatusNoneConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusNoneConfirmed");
        HtmlGenericControl infectionStatusRecovered = (HtmlGenericControl)e.Item.FindControl("infectionStatusRecovered");
        HtmlGenericControl infectionStatusNone = (HtmlGenericControl)e.Item.FindControl("infectionStatusNone");


        Literal symptomsDateInfo = (Literal)e.Item.FindControl("symptomsDateInfo");

        if (symptomsDate.Length > 0) { symptomsDateInfo.Text = symptomsDate; }

        if (covidconfirmed == "YES")
        {
            infectionStatusConfirmed.Visible = true;
        }
        else if (covidSymptoms == "YES")
        {
            infectionStatusNoneConfirmed.Visible = true;
        }
        else if (covidRecovered == "YES")
        {
            infectionStatusRecovered.Visible = true;
        }
        else
        {
            infectionStatusNone.Visible = true;
        }



    }

    public void tableContentResults_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string recID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemReport":
                Response.Redirect("responseReport.aspx?ref=" + recID);
                break;
            case "ItemRemove":
                removeRecord(recID);
                break;
            case "ItemRestore":
                restoreRecord(recID);
                break;
            case "ItemHistory":
                LV_History.Visible = true;
                showHistory(recID);
                udp_history.Update();
                break;

        }

    }

    protected void tableContentResults_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        string symptomsDate = DataBinder.Eval(e.Item.DataItem, "q5").ToString();
        string jobtitle = DataBinder.Eval(e.Item.DataItem, "job_title").ToString();
        string phoneNumber = DataBinder.Eval(e.Item.DataItem, "acc_contactNumber").ToString();
        string covidconfirmed = DataBinder.Eval(e.Item.DataItem, "q1").ToString();
        string covidSymptoms = DataBinder.Eval(e.Item.DataItem, "q2").ToString();
        string covidRecovered = DataBinder.Eval(e.Item.DataItem, "q3").ToString();
        string riskofinfection = DataBinder.Eval(e.Item.DataItem, "q8").ToString();
        string nhsNotInformed = DataBinder.Eval(e.Item.DataItem, "q4").ToString();
        string riskofspread = DataBinder.Eval(e.Item.DataItem, "q6").ToString();
        string fitforwork = DataBinder.Eval(e.Item.DataItem, "q10").ToString();
        string healthIssues = DataBinder.Eval(e.Item.DataItem, "q11").ToString();
        string historical = DataBinder.Eval(e.Item.DataItem, "historical").ToString();

        HtmlGenericControl viewOptionli = (HtmlGenericControl)e.Item.FindControl("viewOptionli");
        HtmlGenericControl restoreOptionli = (HtmlGenericControl)e.Item.FindControl("restoreOptionli");
        HtmlGenericControl removeOptionli = (HtmlGenericControl)e.Item.FindControl("removeOptionli");
        HtmlGenericControl editOptionli = (HtmlGenericControl)e.Item.FindControl("editOptionli");
        HtmlGenericControl showHistoryli = (HtmlGenericControl)e.Item.FindControl("showHistoryli");
        HtmlGenericControl ItemSeparator = (HtmlGenericControl)e.Item.FindControl("ItemSeparator");
        HtmlGenericControl itemHistoryBadge = (HtmlGenericControl)e.Item.FindControl("itemHistoryBadge");

        HtmlGenericControl infectionStatusConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusConfirmed");
        HtmlGenericControl infectionStatusNoneConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusNoneConfirmed");
        HtmlGenericControl infectionStatusRecovered = (HtmlGenericControl)e.Item.FindControl("infectionStatusRecovered");
        HtmlGenericControl infectionStatusNone = (HtmlGenericControl)e.Item.FindControl("infectionStatusNone");

        HtmlGenericControl statusIsolated = (HtmlGenericControl)e.Item.FindControl("statusIsolated");
        HtmlGenericControl statusNotisolated = (HtmlGenericControl)e.Item.FindControl("statusNotisolated");
        HtmlGenericControl statusRiskofinfection = (HtmlGenericControl)e.Item.FindControl("statusRiskofinfection");
        HtmlGenericControl statusNHSNotInformed = (HtmlGenericControl)e.Item.FindControl("statusNHSNotInformed");
        HtmlGenericControl statusRiskofspread = (HtmlGenericControl)e.Item.FindControl("statusRiskofspread");
        HtmlGenericControl statusFitforwork = (HtmlGenericControl)e.Item.FindControl("statusFitforwork");
        HtmlGenericControl statusNotFitforwork = (HtmlGenericControl)e.Item.FindControl("statusNotFitforwork");
        HtmlGenericControl statusHealthIssues = (HtmlGenericControl)e.Item.FindControl("statusHealthIssues");


        Literal symptomsDateInfo = (Literal)e.Item.FindControl("symptomsDateInfo");
        Literal jobtitleData = (Literal)e.Item.FindControl("jobtitleData");
        Literal contactNumber = (Literal)e.Item.FindControl("contactNumber");

        // We only need this warning if the symptoms are within 2 weeks.
        bool overTwoWeeks = false;
        DateTime symptomDateVal;
        if (DateTime.TryParse(symptomsDate, out symptomDateVal))
        {
            overTwoWeeks = (symptomDateVal - DateTime.Now).TotalDays < 14;

        }

        if (historical == "True")
        {
            //itemHistoryBadge.Visible = true;
            showHistoryli.Visible = true;
            ItemSeparator.Visible = true;
        }

        if (symptomsDate.Length > 0) { symptomsDateInfo.Text = symptomsDate; }
        if (jobtitle.Length > 0) { jobtitleData.Text =  jobtitle; }
        if (phoneNumber.Length > 0) { contactNumber.Text = " | Tel :" + phoneNumber; }

        if (healthIssues == "YES") { statusHealthIssues.Visible = true; }


        if (covidconfirmed == "YES") {
            infectionStatusConfirmed.Visible = true;
        } else if (covidSymptoms == "YES")
        {
            infectionStatusNoneConfirmed.Visible = true;
        } else if (covidRecovered == "YES")
        {
            infectionStatusRecovered.Visible = true;
        } else {
            infectionStatusNone.Visible = true;
        }

        if (nhsNotInformed == "NO") { statusNHSNotInformed.Visible = true; }
        if (riskofinfection == "YES" && covidRecovered != "YES") { statusRiskofinfection.Visible = true; }
        if (riskofspread == "NO" && covidRecovered != "YES" && overTwoWeeks != true) {
            statusRiskofspread.Visible = true;
            statusNotisolated.Visible = true;
        } else {

            if ((covidconfirmed == "YES" || covidSymptoms == "YES") && covidRecovered != "YES" && overTwoWeeks != true)
            {
                statusIsolated.Visible = true;
            }

        }
        if (fitforwork == "YES") { statusFitforwork.Visible = true; } else { statusNotFitforwork.Visible = true;  }









    }

    protected void Search_Click(object sender, EventArgs e)
    {
        runSearch = "Y";
        saveSearchCriteriaData(runSearch);
        returnResults();
        displayPagingCheck("0");




    }

    public void saveSearchCriteriaData(string runSearch)
    {


        ssRef = savedSearchRefHiddenValue.Value;
        searchType = HiddenFieldServerSearchType.Value.ToString();

        if (searchType == "true")
        {
            searchType = "A";
        }
        else
        {
            searchType = "B";
        }
        uniqueSearchTableCode = "TempSearchDB.dbo." + uniqueSearchCode + "Search";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            //create our temp table for a standard search
            if (runSearch == "Y")
            {
                string sqlText4 = "execute Module_GLOBAL.dbo.CreateTempSearchTable '" + uniqueSearchTableCode + "'";

                SqlCommand sqlComm5 = new SqlCommand(sqlText4, dbConn);
                sqlComm5.ExecuteReader();

                sqlComm5.Dispose();

            }

            //Loop through the form and add each item to the the relevant table
            foreach (string filter in Request.Form)
            {
                if (!filter.StartsWith("srch_")) continue;

                if (runSearch == "Y") //Pass Y through the standard search, pass N through the saved search
                {
                    //Save the search criteria to the temp table, we will then pull that data out again and rerun the search
                    string sqlText = "insert into " + uniqueSearchTableCode + " (corp_code, session_name, session_value) " +
                             " values ('" + var_corp_code + "', '" + filter.ToString() + "', '" + Request.Form[filter] + "') ";

                    SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                    sqlComm.ExecuteReader();

                    sqlComm.Dispose();
                }
                else
                {
                    //We are saving a search to the Saved search table
                    string sqlText = "insert into " + Application["DBTABLE_SAVED_SEARCH_DATA"] + " (corp_code, search_reference, session_name, session_value) " +
                             " values ('" + var_corp_code + "', '" + ssRef + "', '" + filter.ToString() + "', '" + Request.Form[filter] + "') ";

                    SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                    sqlComm.ExecuteReader();

                    sqlComm.Dispose();

                }
            }


        }

    }
    // Search Controls End

    protected void clearSearch_Click(object sender, EventArgs e)
    {

        if (srch_structureTierSolo.Visible) { srch_structureTierSolo.SelectedIndex = 0; }

        srch_structureTier1.SelectedValue = "0";
        srch_structureTier2.SelectedValue = "0";
        srch_structureTier3.SelectedValue = "0";
        srch_structureTier4.SelectedValue = "0";
        srch_structureTier5.SelectedValue = "0";
        srch_structureTier2.Visible = false;
        srch_structureTier3.Visible = false;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;

        srch_Keyword.Text = "";
        srch_question.SelectedIndex = 0;
        srch_show.SelectedIndex = 1;
        questionResponseTR.Visible = false;
        srch_dateRangeType.SelectedIndex = 0;


    }

    public void populateSoloStructure()
    {

        srch_structureTierSolo.Visible = true;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetStructure", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            // populate the drop down
            srch_structureTierSolo.DataSource = objrs;
            srch_structureTierSolo.DataValueField = "tierRef";
            srch_structureTierSolo.DataTextField = "struct_name";
            srch_structureTierSolo.DataBind();

            // put in a selectable option
            srch_structureTierSolo.Items.Insert(0, "All Locations");

            ListItem _item = new ListItem("(My Locations is not listed)", "-2");
            srch_structureTierSolo.Items.Add(_item);

            cmd.Dispose();

        }


    }



    // Structure Elements (main)


    public void structureTier_populate(Int32 refID)
    {

        srch_structureTier1.Items.Clear();

        try
        {

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, "", tier2Ref, "SEARCH", yourAccessLevel, "", "2", ""))
            {
                srch_structureTier1.DataSource = returnedStructure;
                srch_structureTier1.DataValueField = "tierRef";
                srch_structureTier1.DataTextField = "struct_name";
                srch_structureTier1.DataBind();

                if (srch_structureTier1.Items.Count == 0)
                {
                    srch_structureTier1.Visible = false;
                    srch_structureTier1.Items.Clear();

                }


            }


        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void structureTier1_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier2.Visible = true;
        srch_structureTier3.Visible = false;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;
        srch_structureTier2.Items.Clear();

        getTier2StructureInformation();



    }

    public void structureTier2_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier3.Visible = true;
        srch_structureTier4.Visible = false;
        srch_structureTier5.Visible = false;
        srch_structureTier3.Items.Clear();

        getTier3StructureInformation();



    }

    public void structureTier3_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier4.Visible = true;
        srch_structureTier5.Visible = false;
        srch_structureTier4.Items.Clear();

        getTier4StructureInformation();



    }

    public void structureTier4_SelectedIndexChanged(object sender, EventArgs e)
    {

        srch_structureTier5.Visible = true;
        srch_structureTier5.Items.Clear();

        getTier5StructureInformation();



    }

    public void getTier2StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier1.SelectedValue != "")
            {
                currentSelection = srch_structureTier1.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier3Ref, "SEARCH", yourAccessLevel, "", "3", ""))
            {
                srch_structureTier2.DataSource = returnedStructure;
                srch_structureTier2.DataValueField = "tierRef";
                srch_structureTier2.DataTextField = "struct_name";
                srch_structureTier2.DataBind();



                if (srch_structureTier2.Items.Count == 0)
                {
                    srch_structureTier2.Visible = false;
                    srch_structureTier2.Items.Clear();
                }


            }




        }

        catch (Exception ex)
        {

            testing.Text = ex.ToString();

        }

    }

    public void getTier3StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier2.SelectedValue != "")
            {
                currentSelection = srch_structureTier2.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier4Ref, "SEARCH", yourAccessLevel, "", "4", ""))
            {
                srch_structureTier3.DataSource = returnedStructure;
                srch_structureTier3.DataValueField = "tierRef";
                srch_structureTier3.DataTextField = "struct_name";
                srch_structureTier3.DataBind();

                if (srch_structureTier3.Items.Count == 0)
                {
                    srch_structureTier3.Visible = false;
                    srch_structureTier3.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier4StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier3.SelectedValue != "")
            {
                currentSelection = srch_structureTier3.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier5Ref, "SEARCH", yourAccessLevel, "", "5", ""))
            {
                srch_structureTier4.DataSource = returnedStructure;
                srch_structureTier4.DataValueField = "tierRef";
                srch_structureTier4.DataTextField = "struct_name";
                srch_structureTier4.DataBind();

                if (srch_structureTier4.Items.Count == 0)
                {
                    srch_structureTier4.Visible = false;
                    srch_structureTier4.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }

    public void getTier5StructureInformation()
    {

        try
        {

            string currentSelection = "0";

            if (srch_structureTier4.SelectedValue != "")
            {
                currentSelection = srch_structureTier4.SelectedValue;
            }

            using (DataTable returnedStructure = Structure.ReturnStructure(var_corp_code, yourAccRef, currentSelection, tier6Ref, "SEARCH", yourAccessLevel, "", "6", ""))
            {
                srch_structureTier5.DataSource = returnedStructure;
                srch_structureTier5.DataValueField = "tierRef";
                srch_structureTier5.DataTextField = "struct_name";
                srch_structureTier5.DataBind();

                if (srch_structureTier5.Items.Count == 0)
                {
                    srch_structureTier5.Visible = false;
                    srch_structureTier5.Items.Clear();
                }
            }

        }

        catch (Exception ex)
        {

            Response.Write(ex);

        }

    }


    // Structure Elements Main End



    protected void goToDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }



}

