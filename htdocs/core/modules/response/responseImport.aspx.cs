﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using System.Globalization;

public partial class responseImport : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string yourAccessLevel;
    int _accUserCountTotalInput = 0;
    int _accUserCountTotalInputTaken = 0;


    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session.aspx"); Response.End(); }

        //***************************
        // CHECK THE SESSION STATUS
        SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");


        if (!IsPostBack)
        {



        }


    }



    public static bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }


    public void addUserAccount(string _emailaddress)
    {
        // Create a new acc ref
        string _acc_ref = "";
        string _emailInUse = "";

        _acc_ref = Guid.NewGuid().ToString();

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            // See if the email is in the system already
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCreateUser", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@emailAddress", _emailaddress));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                _emailInUse = objrs["AccountInUse"].ToString();

            }

            objrs.Dispose();
            cmd.Dispose();

            if (_emailInUse == "TRUE") { _accUserCountTotalInputTaken++; }

            _accUserCountTotalInput++;


        }


    }


    public void importUsers_Click(object sender, EventArgs e)
    {
        // Grab the data
        string emailList = caseUserList.Text;
        string emailsNotValid = "";

        // If it has no real length then dont bother
        if (emailList.Length > 5) {
            string[] lst = emailList.Split(new Char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);

            // Now lets run through the users
            foreach (string Email in lst)
            {

                // ok lets check they are valid emails, if not, throw it back
                if (IsValidEmail(Email)) {

                    addUserAccount(Email);


                } else
                {
                   emailsNotValid = emailsNotValid + Email + " - (not valid email address)\n";
                }



            }

            caseUserList.Text = emailsNotValid;
            caseUserListImportTotalTaken.Text = _accUserCountTotalInputTaken.ToString();
            caseUserListImportTotal.Text = _accUserCountTotalInput.ToString();


        } else
        {
            // No data

        }


        UpdatePanelControls.Update();





    }




    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }

    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }

    protected void goToDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }

}

