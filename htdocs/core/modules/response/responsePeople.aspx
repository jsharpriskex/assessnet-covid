﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responsePeople.aspx.cs" Inherits="responsePeople" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Search</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />

</head>
<body runat="server" id="pageBody">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true" >
    </asp:ScriptManager>


    <script type="text/javascript" language="javascript">


        function showLoadingDiv(extraTimer) {
            $('#progressPopUp').fadeIn(1500);
        }

    </script>

    <asp:updateprogress id="progressPopUp" runat="server" dynamiclayout="true" DisplayAfter="1500">
        <progresstemplate>

            <%=ASNETNSP.SharedComponants.ReturnProgressDiv() %>

        </progresstemplate>
    </asp:updateprogress>

    <div class="container-fluid">
    <div class="row">

            <!-- Records Modal -->
            <div class="modal fade" id="HistoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">


                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalLabel">Assessments</h4>
                    </div>
                    <div class="modal-body">

                       <asp:UpdatePanel runat="server" id="udp_history" ChildrenAsTriggers="false" UpdateMode="conditional" >
                           <ContentTemplate>

                                <div class="table">
                                    <table class="table table-striped table-bordered table-liststyle01 searchResults">

                                       <asp:ListView ID="LV_History" Visible="false" runat="server" ItemPlaceholderID="itemPlaceHolderHistory" OnItemCommand="tableContentHistory_ItemCommand" OnItemDataBound="tableContentHistory_ItemDataBound">
                                            <LayoutTemplate>

                                                <thead>
                                                    <tr>
                                                        <th style="width: 150px;">Case Date/Time</th>
                                                        <th style="width: 150px;">Case Reference</th>
                                                        <th style="width: 200px;">Infection</th>
                                                        <th style="width: 150px;">Symptoms Start</th>
                                                        <th style="width: 100px;">Options</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolderHistory"></asp:PlaceHolder>
                                                </tbody>

                                            </LayoutTemplate>

                                            <ItemTemplate>

                                                <tr>
                                                    <td class="text-center"><%# Eval("gendatetime").ToString() %></td>
                                                    <td class="text-center"><%# Eval("reference").ToString() %></td>
                                                    <td class="text-center">
                                                         <div runat="server" visible="false" id="infectionStatusConfirmed" class="text-center list-group-item list-group-item-danger">Confirmed</div>
                                                         <div runat="server" visible="false" id="infectionStatusNoneConfirmed" class="text-center list-group-item list-group-item-warning">Symptoms</div>
                                                         <div runat="server" visible="false" id="infectionStatusRecovered" class="text-center list-group-item list-group-item-success">Recovered</div>
                                                         <div runat="server" visible="false" id="infectionStatusNone" class="text-center list-group-item list-group-item-success">None</div>
                                                    </td>

                                                    <td class="text-center">
                                                        <asp:Literal runat="server" ID="symptomsDateInfo" Text="Not Applicable" />
                                                    </td>
                                                    <td class="text-center">

                                                       <!-- Complete options -->

                                                        <asp:LinkButton ID="viewOption" runat="server" class="btn btn-primary" CommandName="ItemReport" CommandArgument='<%# Eval("case_reference") %>'>View Report</asp:LinkButton>

                                                        <!-- End of complete options -->


                                                    </td>
                                                </tr>

                                            </ItemTemplate>
                                            <EmptyDataTemplate>

                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <div class="alert alert-warning">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>NOTE</strong><br />
                                                                    There are currently no assessments for this person.
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </EmptyDataTemplate>
                                        </asp:ListView>

                                    </table>
                                </div>

                           </ContentTemplate>
                       </asp:UpdatePanel>

                    </div>
                    <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    </div>


                </div>
                </div>
            </div>
            <!-- End of Records Modal -->


    <div runAt="server" id="reportMenu" class="subpageNavListMargin" style="padding-top: 10px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header">
            Navigation & Statistics
          </li>

          <asp:Button ID="goToSearch" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to Asessment Search" OnClick="goToSearch_Click" />
          <asp:Button ID="goToDashboard" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to Dashboard" OnClick="goToDashboard_Click" />
          <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to main menu" OnClick="goToMainMenu_Click" />
        </ul>
    </div>



    <div runAt="server" id="reportContent" class="mainpageNavList"> <!-- Main Col Start -->

        <div class="container-fluid">
            <div class="row" style="padding-bottom:20px;">
            <div class="col-xs-8">
                    <h1 style="padding-top:28px;">Health Assessment ( COVID-19 )</h1>
                    <span>Provide Rapid Response to COVID Cases</span>
            </div>
            <div class="col-xs-4 text-right" align="right">
                 <img src="Combine-CREATORS-OfAssess-300x78.png" />
                 <div class="sharethis-inline-follow-buttons"></div>
             </div>
            </div>
        </div>



    <div class="subpageNavListMobile" style="display:none;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Navigation & Options <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>



<!-- ******************************** -->
<!-- **** Search criteria begins **** -->
<!-- ******************************** -->

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-12">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Filters</h3>
                  </div>
                  <div class="panel-body">

                      <asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                      <ContentTemplate>

                     <div class="well">
                      <div class="row">
                      <div class="col-xs-7">


                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">


                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                                <tr>
                                                    <th class="text-left" style="width:180px;">Person</th>
                                                    <td ><asp:Textbox ID="srch_pepKeyword" Runat="server" style="width: 100%;" CssClass="form-control" /></td>
                                                </tr>
                                                 <tr>
                                                    <th style="width: 150px;">Show</th>
                                                    <td>
                                                    <asp:DropDownList ID="srch_pepshow" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="All" Value="1"/>
                                                        <asp:ListItem Text="Not Fully Registered" Value="2"/>
                                                        <asp:ListItem Text="Register with no Assessments" Value="3"/>
                                                        <asp:ListItem Text="Register with Assessments" Value="4"/>
                                                    </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <th style="width: 150px;">Order By</th>
                                                    <td>

                                                    <div class="row">
                                                        <div class="col-xs-8">

                                                            <asp:DropDownList ID="srch_orderby" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="Registration Date" Value="gendatetime"/>
                                                                <asp:ListItem Text="Name" Value="acc_firstname"/>
                                                            </asp:DropDownList>

                                                        </div>
                                                        <div class="col-xs-4">

                                                            <asp:DropDownList ID="srch_sortOrder" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="Descending" Value="desc"/>
                                                                <asp:ListItem Text="Ascending" Value="asc"/>
                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>


                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="col-xs-5">

                            <div class="row" runat="server" visible="true">
                                 <div class="col-xs-12">
                                    <div class="form-group">

                                       <asp:UpdatePanel ID="UpdatePanel_structure" runat="server" UpdateMode="Conditional">
                                       <ContentTemplate>

                                       <asp:Label runat="server" ID="testing" />

                                        <div class="form-group">

                                           <table class="table table-bordered table-searchstyle01">
                                           <tr><th>Filter to specific area</th></tr>
                                           <tr><td>

                                           <asp:DropDownList runat="server" ID="srch_structureTierSolo" Visible="false" class="form-control" Style="width: 99% !important; display: inline;"></asp:DropDownList>

                                           </td></tr>
                                           </table>


                                        </div>

                                       </ContentTemplate>
                                       </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>

                       </div>


                      </div>
                     </div>



                      </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="clearSearch" />
                              <asp:AsyncPostBackTrigger ControlID="Search" />
                          </Triggers>
                     </asp:UpdatePanel>

                    </div> <!-- End of Panel Body -->

                      <div class="panel-footer">
                      <div class="row">
                          <div class="col-xs-12 text-center">
                              <asp:Button ID="clearSearch" runat="server" class="btn btn-danger " OnClick="clearSearch_Click" Text="Clear" OnClientClick="$(section_2).hide();" />
                              &nbsp;<asp:Button ID="Search" runat="server" class="btn btn-success " OnClick="Search_Click" Text="Search"  />
                          </div>
                      </div>
                      </div>

            </div>

        </div>
        </div>





<!-- ****************************** -->
<!-- **** Search criteria ends **** -->
<!-- ****************************** -->


<!-- ***************************** -->
<!-- *** Search Results begins *** -->
<!-- ***************************** -->


        <a href="#searchareaAnchor" id="searchareaAnchor" class="searchareaAnchor" >&nbsp;</a>

        <div runat="server" id="section_2" class="row section_2" style="display:none;">

            <div class="col-xs-12">
                <div class="panel panel-default">

                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Results

                    <asp:UpdatePanel id="updateRecordTotal" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Label runat="server" id="results_count" CssClass="badge pull-right" Visible="False"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    </h3>

                    </div>
                    <!-- Search results panel begins -->
                    <div class="panel-body"  id="section2Panel" runat="server">



                    <asp:UpdatePanel id="UpdateSearchResults" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:HiddenField ID="savedSearchRefHiddenValue" runat="server" />
                    <asp:HiddenField ID="HiddenFieldServerSearchType" runat="server" />
                    <asp:HiddenField ID="HiddenFieldRemoveRef" runat="server" />


                      <div id="searchResultsArea" class="searchResultsArea" runat="server">

                      <div class="row">
                          <div class="col-xs-12">


                            <div runat="server" id="DisplayResults" class="DisplayResults">


                                <div class="table"> <!-- responsive removed by ML 27/03/2017 to fix issue with the options button dropdown -->
                                    <table class="table table-striped table-bordered table-liststyle01 searchResults">

                                    <div class="pagingSectionTopKEK" id="pagingSectionTopKEK"  >
                                        <div class="row">

                                            <!-- Search Key -->
                                            <div class="col-xs-8">
                                                <div class="iconTextBlockStyleContainer01">

                                                 </div>
                                            </div>
                                            <!-- End of Search Key -->

                                            <!-- Top Paging Section -->
                                            <div class="col-xs-4 text-right pagingSectionTop" id="pagingSectionTop" style="display:none;" >
                                                <div class="btn-group" role="group" aria-label="...">
                                                    <asp:Button ID="previousPageButtonTop" class="btn btn-primary previousPageButtonTop" runat="server" Text="&laquo;" OnClick="previousPage_Click" />
                                                    <div class="btn-group">
                                                        <asp:DropDownList ID="pageNavigationTop" runat="server" class="form-control" OnSelectedIndexChanged="returnSelectedPageTop" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                    <asp:Button ID="nextPageButtonTop" class="btn btn-primary nextPageButtonTop" runat="server" Text="&raquo;" OnClick="nextPage_Click" />
                                                </div>
                                            </div>
                                            <!-- End of Top Paging Section -->

                                         </div>
                                    </div>
                                    <br />

                                        <asp:ListView ID="tableContentResults" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnItemCommand="tableContentResults_ItemCommand" OnItemDataBound="tableContentResults_ItemDataBound">
                                            <LayoutTemplate>

                                                <thead>
                                                    <tr>
                                                        <th style="width: 150px;">Registered</th>
                                                        <th style="width: 220px;">Email Address</th>
                                                        <th>Person</th>
                                                        <th style="width: 180px;">Status</th>
                                                        <th style="width: 150px;">Assessments</th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                </tbody>

                                            </LayoutTemplate>

                                            <ItemTemplate>

                                                <tr>
                                                    <td class="text-center"><%# Eval("gendatetime").ToString() %></td>
                                                    <td class="text-center"><%# Eval("acc_contactemail").ToString() %></td>
                                                    <td class="searchDescriptionCell">
                                                        <div runat="server" id="iconPerson" visible="true"><i class='fa fa-user fa-2x' aria-hidden='true'></i></div>

                                                        <h3 style="font-size:17px; font-weight:normal;text-transform:capitalize;"><asp:Literal runat="server" id="fullname" Text="Registration Started" /></h3>
                                                        <p>Role: <asp:Literal runat="server" ID="jobtitleData" Text="No job title specified" />&nbsp;&nbsp:Tel: <asp:Literal runat="server" ID="contactNumber" Text="Not Specified" /></p>
                                                    </td>
                                                    <td class="text-center">
                                                        <div runat="server" visible="false" id="peopleStatusRegFull" class="text-center list-group-item list-group-item-success">Fully Registered</div>
                                                        <div runat="server" visible="false" id="peopleStatusRegPart" class="text-center list-group-item list-group-item-warning">Part Registered</div>
                                                    </td>
                                                    <td class="text-center" style="font-size:30px;"><asp:LinkButton ID="showHistory"  runat="server" CommandName="ItemHistory" OnClientClick="$('#HistoryModal').modal('show');" CommandArgument='<%# Eval("acc_ref").ToString() %>'><asp:Literal runat="server" ID="assessments" Text="0" /></asp:LinkButton></td>
                                                 </tr>

                                            </ItemTemplate>
                                            <EmptyDataTemplate>

                                                <div class="row">
                                                    <div class="col-xs-12">

                                                        <div class="alert alert-warning">
                                                            <div class="row">
                                                                <div class="col-xs-12">
                                                                    <strong>NOTE</strong><br />
                                                                    There are currently no search results displayed. Please use the menus above to select the criteria then click "Search" to continue. Remember, selecting as many criteria as possible will produce more refined search results.
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </EmptyDataTemplate>
                                        </asp:ListView>


                                    </table>



                                <!-- Bottom Paging Section -->
                                <div class="col-xs-12 text-right pagingSection" id="pagingSection" style="display:none;padding-right:0;margin-right:0;" >
                                    <asp:HiddenField ID="currenPageNumberHidden" runat="server" />
                                    <asp:HiddenField ID="maxPageNumberHidden" runat="server" />

                                    <div class="btn-group" role="group" aria-label="...">
                                        <asp:Button ID="previousPageButton" class="btn btn-primary previousPageButton" runat="server" Text="&laquo;" OnClick="previousPage_Click" />
                                        <div class="btn-group">
                                            <asp:DropDownList ID="pageNavigation" runat="server" class="form-control" OnSelectedIndexChanged="returnSelectedPage" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <asp:Button ID="nextPageButton" class="btn btn-primary nextPageButton" runat="server" Text="&raquo;" OnClick="nextPage_Click" />
                                    </div>
                                </div>
                                <!-- End of Bottom Paging Section -->

                                </div>




                            </div> <!-- display results end -->



                          </div>
                       </div>

                 </div>

                </ContentTemplate>
                <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="Search" EventName="Click" />
                </Triggers>
                </asp:UpdatePanel>

                   </div> <!-- end of search results panel body -->

                    <div class="panel-footer">
                      <div class="row">
                          <div class="col-xs-6 text-left">
                              <!-- Total Assessments -->
                          </div>


                      </div>
                    </div>

                 </div> <!-- end of search results panel -->
              </div> <!-- end of search results main column -->
          </div> <!-- end of search results row -->



<!-- ***************************** -->
<!-- **** Search Results Ends **** -->
<!-- ***************************** -->






    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />



    <script type="text/javascript">

        function BindControlEvents() {


            $('#pageform').keypress(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#searchAssessment').focus();
                    $('#searchAssessment').click();
                }
            });


            $('#pageform').keypress(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $('#Search').focus();
                    $('#Search').click();
                }
            });

        }

        $(document).ready(function () {

            BindControlEvents();

            $(function () {
                $('#assessment_date').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm',
                    sideBySide: true,
                    widgetPositioning: { horizontal: 'right', vertical: 'auto' }
                });
            });

        });

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });

    </script>


</body>
</html>
