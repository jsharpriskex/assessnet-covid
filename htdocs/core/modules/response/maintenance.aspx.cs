﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class ReponseMaint : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

        }

    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }


}

