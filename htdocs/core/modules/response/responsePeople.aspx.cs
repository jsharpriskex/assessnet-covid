﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class responsePeople : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string globalUserPerms;
    public string recordRef;
    public string tier2;
    public string tier3;
    public string tier4;
    public string tier5;
    public string tier6;
    public string tier2Ref;
    public string tier3Ref;
    public string tier4Ref;
    public string tier5Ref;
    public string tier6Ref;
    public string tier2Name;
    public string tier3Name;
    public string tier4Name;
    public string yourAccessLevel;

    // Search Specific Vars
    public string ssRef;
    public string runSearch;
    public string searchType;
    public int pageNumber;
    public int pageSelection;
    public string uniqueSearchCode;
    public string uniqueHistoryCode;
    public string uniqueSearchTableCode;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }


        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        uniqueSearchCode = "CASEPEP" + yourAccRef.Replace("-","") + var_corp_code;
        globalUserPerms = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref");
        yourAccessLevel = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_ACCESS", "user");



        if (!IsPostBack)
        {

            populateSoloStructure();

        }

    }

    protected void goToDashboard_Click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }

    protected void goToMainMenu_Click(object sender, EventArgs e)
    {
        Response.Redirect("layout/module_page.aspx");
    }

    protected void goToSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("responseSearch.aspx");
    }

    // Search Controls Start

    // Both page selection dropdowns grouped together
    protected void returnSelectedPageTop(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigationTop.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void returnSelectedPage(object sender, EventArgs e)
    {
        pageSelection = Int32.Parse(pageNavigation.SelectedItem.Value);
        returnResultsPage(pageSelection);
    }

    protected void nextPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) + 1;
        returnResultsPage(pageNumber);
        previousPageButton.Enabled = true;
        previousPageButtonTop.Enabled = true;

    }

    protected void previousPage_Click(object sender, EventArgs e)
    {

        pageNumber = Int32.Parse(currenPageNumberHidden.Value) - 1;
        returnResultsPage(pageNumber);
        nextPageButtonTop.Enabled = true;
        nextPageButton.Enabled = true;

    }

    public void returnResultsPage(int pageNumber)
    {

        string pageSize = "15";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string sqlText = "execute Module_HA.dbo.caseSearch_Page '" + uniqueSearchCode + "', '" + pageNumber + "', '" + pageSize + "'";

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            //Set the table info
            tableContentResults.DataSource = objrs;
            tableContentResults.DataBind();

            currenPageNumberHidden.Value = pageNumber.ToString();


            sqlComm.Dispose();
            objrs.Close();

            displayPagingCheck("0");
        }
    }

    private void displayPagingCheck(string commandType)
    {
        string sqlText;
        string scriptString;
        string results_addTheS = "s";
        pageNavigationTop.Items.Clear();
        pageNavigation.Items.Clear();
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here1');", true);
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            sqlText = "SELECT COUNT(*) as totalrecords FROM TempSearchDB.dbo." + uniqueSearchCode;

            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('here2');", true);

            SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
            SqlDataReader objrs = sqlComm.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                int totalRecords = Int32.Parse(objrs["totalrecords"].ToString());

                if (totalRecords == 1) { results_addTheS = ""; }


                results_count.Text = totalRecords.ToString() + " result" + results_addTheS + " found";


                results_count.Visible = true;
                updateRecordTotal.Update();

                if (totalRecords > 15)
                {
                    int pageCount = totalRecords / 15;
                    int remainder = totalRecords % 15;
                    if (remainder > 0) { pageCount++; }

                    pageNavigationTop.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    pageNavigation.Items.AddRange(Enumerable.Range(1, pageCount).Select(e => new ListItem(e.ToString())).ToArray());
                    maxPageNumberHidden.Value = pageCount.ToString();

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == 1)
                    {
                        previousPageButtonTop.Enabled = false;
                        previousPageButton.Enabled = false;
                    }
                    else
                    {
                        previousPageButtonTop.Enabled = true;
                        previousPageButton.Enabled = true;
                    }

                    if (Convert.ToInt32(currenPageNumberHidden.Value) == Convert.ToInt32(maxPageNumberHidden.Value))
                    {
                        nextPageButtonTop.Enabled = false;
                        nextPageButton.Enabled = false;
                    }
                    else
                    {
                        nextPageButtonTop.Enabled = true;
                        nextPageButton.Enabled = true;
                    }

                    pageNumber = Convert.ToInt32(currenPageNumberHidden.Value);
                    pageNavigationTop.SelectedValue = pageNumber.ToString();
                    pageNavigation.SelectedValue = pageNumber.ToString();



                    scriptString = "$('.pagingSection').show();$('.pagingSectionTop').show();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";




                }
                else
                {
                    scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.section_2').css('display', 'block');$('.DisplayResults').show();$('html, body').animate({scrollTop: $('.searchareaAnchor').offset().top}, 10);";
                }

            }
            else
            {
                results_count.Visible = false;
                results_count.Text = "0";

                scriptString = "$('.pagingSection').hide();$('.pagingSectionTop').hide();$('.DisplayResults').show();";
            }


            //switch (commandType)
            //{
            //    case "OpenInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('show');";
            //        break;
            //    case "CloseInfoModal":
            //        scriptString = scriptString + "$('#InfoModal').modal('hide');";
            //        break;
            //    case "CloseLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('hide');";
            //        break;
            //    case "OpenLinkModal":
            //        scriptString = scriptString + "$('#LinkModal').modal('show');";
            //        break;

            //}

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", scriptString, true);

            sqlComm.Dispose();
            objrs.Close();

        }

        //return sqlText;
    }

    public void returnResults()
    {
        string sqlText = "";
        try
        {

            string Keyword = srch_pepKeyword.Text;
            string status = srch_pepshow.SelectedValue;

            string tier2Selection = (srch_structureTierSolo.SelectedValue.Length < 5) ? srch_structureTierSolo.SelectedValue : "0";
            string tier3Selection = "0";
            string tier4Selection = "0";
            string tier5Selection = "0";
            string tier6Selection = "0";

            string pageSize = "15";
            string Tempvar_corp_code = var_corp_code;


            if (pageNumber == 0)
            {
                pageNumber = 1;
            }

            string sortResultsBySelection = srch_orderby.SelectedValue;
            string sortResultsByOrder = srch_sortOrder.SelectedValue;

            string yourAccountID = yourAccRef; //former session
            string yourCompany = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_COMPANY", "user");
            string yourLocation = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_LOCATION", "user");
            string yourDepartment = SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "YOUR_DEPARTMENT", "user");
            string userPermsActive = (SharedComponants.getPreferenceValue(var_corp_code, yourAccRef, "GLOBAL_USR_PERM", "pref") == "Y") ? "1" : "0";


            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                sqlText = "execute Module_HA.dbo.casePeopleSearch '" + Tempvar_corp_code + "','" + Keyword + "', '" + status + "','" + tier2Selection + "', '" + tier3Selection + "', '" + tier4Selection + "', '" + tier5Selection + "', '" + tier6Selection + "', '" + sortResultsBySelection + "', '" + sortResultsByOrder + "', '" + yourAccessLevel + "', '" + yourAccountID + "', '" + yourCompany + "', '" + yourLocation + "', '" + yourDepartment + "', '" + pageNumber + "', '" + pageSize + "', '" + uniqueSearchCode + "','0'";
                SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                SqlDataReader objrs = sqlComm.ExecuteReader();

                tableContentResults.DataSource = objrs;
                tableContentResults.DataBind();

                currenPageNumberHidden.Value = "1";

                previousPageButtonTop.Enabled = false;
                previousPageButton.Enabled = false;

                objrs.Close();
                sqlComm.Dispose();
            }



        }
        catch (Exception)
        {
            throw;
        }


    }

    protected void showRecords(string recID)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseShowAllRecords", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@var_acc_ref", recID));
            cmd.Parameters.Add(new SqlParameter("@var_corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            LV_History.DataSource = objrs;
            LV_History.DataBind();


        }

    }

    public void tableContentHistory_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string recID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemReport":
                Response.Redirect("responseReport.aspx?ref=" + recID);
                break;

        }

    }

    protected void tableContentHistory_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        string symptomsDate = DataBinder.Eval(e.Item.DataItem, "q5").ToString();
        string covidconfirmed = DataBinder.Eval(e.Item.DataItem, "q1").ToString();
        string covidSymptoms = DataBinder.Eval(e.Item.DataItem, "q2").ToString();
        string covidRecovered = DataBinder.Eval(e.Item.DataItem, "q3").ToString();
        string riskofinfection = DataBinder.Eval(e.Item.DataItem, "q8").ToString();
        string nhsNotInformed = DataBinder.Eval(e.Item.DataItem, "q4").ToString();
        string riskofspread = DataBinder.Eval(e.Item.DataItem, "q6").ToString();
        string fitforwork = DataBinder.Eval(e.Item.DataItem, "q10").ToString();
        string healthIssues = DataBinder.Eval(e.Item.DataItem, "q11").ToString();

        HtmlGenericControl viewOptionli = (HtmlGenericControl)e.Item.FindControl("viewOptionli");

        HtmlGenericControl infectionStatusConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusConfirmed");
        HtmlGenericControl infectionStatusNoneConfirmed = (HtmlGenericControl)e.Item.FindControl("infectionStatusNoneConfirmed");
        HtmlGenericControl infectionStatusRecovered = (HtmlGenericControl)e.Item.FindControl("infectionStatusRecovered");
        HtmlGenericControl infectionStatusNone = (HtmlGenericControl)e.Item.FindControl("infectionStatusNone");


        Literal symptomsDateInfo = (Literal)e.Item.FindControl("symptomsDateInfo");

        if (symptomsDate.Length > 0) { symptomsDateInfo.Text = symptomsDate; }

        if (covidconfirmed == "YES")
        {
            infectionStatusConfirmed.Visible = true;
        }
        else if (covidSymptoms == "YES")
        {
            infectionStatusNoneConfirmed.Visible = true;
        }
        else if (covidRecovered == "YES")
        {
            infectionStatusRecovered.Visible = true;
        }
        else
        {
            infectionStatusNone.Visible = true;
        }



    }

    public void tableContentResults_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string recID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemHistory":
                LV_History.Visible = true;
                showRecords(recID);
                udp_history.Update();
                break;

        }

    }

    protected string countAssessments(string _accRef)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCountAssessments", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return objrs["countAssessments"].ToString();
            }

            cmd.Dispose();

        }

        return "0";

    }

    protected void tableContentResults_ItemDataBound(object sender, ListViewItemEventArgs e)
    {

        string accRef = DataBinder.Eval(e.Item.DataItem, "acc_ref").ToString();
        string firstname = DataBinder.Eval(e.Item.DataItem, "acc_firstname").ToString();
        string surname = DataBinder.Eval(e.Item.DataItem, "acc_surname").ToString();
        string job = DataBinder.Eval(e.Item.DataItem, "acc_jobtitle").ToString();
        string phone = DataBinder.Eval(e.Item.DataItem, "acc_contactnumber").ToString();

        HtmlGenericControl peopleStatusRegPart = (HtmlGenericControl)e.Item.FindControl("peopleStatusRegPart");
        HtmlGenericControl peopleStatusRegFull = (HtmlGenericControl)e.Item.FindControl("peopleStatusRegFull");

        Literal fullname = (Literal)e.Item.FindControl("fullname");
        Literal jobtitleData = (Literal)e.Item.FindControl("jobtitleData");
        Literal contactNumber = (Literal)e.Item.FindControl("contactNumber");
        Literal assessments = (Literal)e.Item.FindControl("assessments");

        if (firstname.Length > 0)
        {
            peopleStatusRegFull.Visible = true;
            fullname.Text = firstname + ' ' + surname;

            if (job.Length > 0) { jobtitleData.Text = job; }
            if (phone.Length > 0) { contactNumber.Text = phone; }

            assessments.Text = countAssessments(accRef);

        } else
        {
            peopleStatusRegPart.Visible = true;
        }



    }

    protected void Search_Click(object sender, EventArgs e)
    {
        runSearch = "Y";
        saveSearchCriteriaData(runSearch);
        returnResults();
        displayPagingCheck("0");

    }

    public void saveSearchCriteriaData(string runSearch)
    {


        ssRef = savedSearchRefHiddenValue.Value;
        searchType = HiddenFieldServerSearchType.Value.ToString();

        if (searchType == "true")
        {
            searchType = "A";
        }
        else
        {
            searchType = "B";
        }
        uniqueSearchTableCode = "TempSearchDB.dbo." + uniqueSearchCode + "Search";

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            //create our temp table for a standard search
            if (runSearch == "Y")
            {
                string sqlText4 = "execute Module_GLOBAL.dbo.CreateTempSearchTable '" + uniqueSearchTableCode + "'";

                SqlCommand sqlComm5 = new SqlCommand(sqlText4, dbConn);
                sqlComm5.ExecuteReader();

                sqlComm5.Dispose();

            }

            //Loop through the form and add each item to the the relevant table
            foreach (string filter in Request.Form)
            {
                if (!filter.StartsWith("srch_")) continue;

                if (runSearch == "Y") //Pass Y through the standard search, pass N through the saved search
                {
                    //Save the search criteria to the temp table, we will then pull that data out again and rerun the search
                    string sqlText = "insert into " + uniqueSearchTableCode + " (corp_code, session_name, session_value) " +
                             " values ('" + var_corp_code + "', '" + filter.ToString() + "', '" + Request.Form[filter] + "') ";

                    SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                    sqlComm.ExecuteReader();

                    sqlComm.Dispose();
                }
                else
                {
                    //We are saving a search to the Saved search table
                    string sqlText = "insert into " + Application["DBTABLE_SAVED_SEARCH_DATA"] + " (corp_code, search_reference, session_name, session_value) " +
                             " values ('" + var_corp_code + "', '" + ssRef + "', '" + filter.ToString() + "', '" + Request.Form[filter] + "') ";

                    SqlCommand sqlComm = new SqlCommand(sqlText, dbConn);
                    sqlComm.ExecuteReader();

                    sqlComm.Dispose();

                }
            }


        }

    }
    // Search Controls End

    protected void clearSearch_Click(object sender, EventArgs e)
    {

        if (srch_structureTierSolo.Visible) { srch_structureTierSolo.SelectedIndex = 0; }

        srch_pepKeyword.Text = "";
        srch_pepshow.SelectedIndex = 0;
        srch_orderby.SelectedIndex = 0;
        srch_sortOrder.SelectedIndex = 0;

    }

    public void populateSoloStructure()
    {

        srch_structureTierSolo.Visible = true;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetStructure", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", var_corp_code));
            objrs = cmd.ExecuteReader();

            // populate the drop down
            srch_structureTierSolo.DataSource = objrs;
            srch_structureTierSolo.DataValueField = "tierRef";
            srch_structureTierSolo.DataTextField = "struct_name";
            srch_structureTierSolo.DataBind();

            // put in a selectable option
            srch_structureTierSolo.Items.Insert(0, "All Locations");

            ListItem _item = new ListItem("(My Locations is not listed)", "-2");
            srch_structureTierSolo.Items.Add(_item);

            cmd.Dispose();

        }


    }



}

