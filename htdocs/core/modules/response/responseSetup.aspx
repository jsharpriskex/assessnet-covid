﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="responseSetup.aspx.cs" Inherits="ResponseSetup" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Training Management Module</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />


    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-13178301-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-13178301-5');
    </script>

    <style>

    <style>

       body p {

            font-size:17px;

        }

       html,body {
           margin:10px;
           padding:0px;

            background: url('background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

       .textInput {
            height: 45px;
            font-size: 30px;
            text-align: center;
            background:#efefef;
            text-transform:uppercase;
        }

    </style>


</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


    <div class="container">
    <div class="row">


        <div class="row">
            <div class="col-xs-12 text-center">

                    <h2>Set up your Health Assessment (COVID-19)</h2>

            </div>
        </div>

         <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1"">

                    <asp:UpdatePanel ID="UpdatePanelInfoArea" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>

                        <asp:HiddenField runat="server" ID="acc" />
                        <asp:HiddenField runat="server" ID="cc" />
                        <asp:HiddenField runat="server" ID="email" />


                            <div class="row">
                                    <div class="col-xs-12" id="enterdetails">

                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                            <div class="panel panel-default panel-subpanel" id="headingOneContainer" runat="server">
                                                <div class="panel-heading click" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" data-target="#collapseOne" OnClick="headingOne_clicked();">
                                                    <h4 class="panel-title">
                                                        <a role="button" aria-expanded="true" aria-controls="collapseOne">
                                                            <i class="fa fa-bars" aria-hidden="true"></i>Enter your details

                                                        </a>
                                                    </h4>
                                                </div>
                                                <div runat="server" id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">


                                                   <div class="row">
                                                        <div class="col-xs-3">
                                                            <img src="Combine-CREATORS-OfAssess-300x78.png" />
                                                        </div>
                                                        <div class="col-xs-9" style="padding-top:5px;">
                                                            <div class="sharethis-inline-follow-buttons"></div>
                                                        </div>
                                                    </div>


                                                       <div class="row" style="padding-top:10px;">
                                                            <div  class="col-xs-10 col-xs-offset-1">

                                                                <div class="alert alert-warning" role="alert">
                                                                    <div class="media">
                                                                          <div class="media-body" style="font-size:17px;">
                                                                            Please enter the information in the boxes below.  You will only have to do this once.
                                                                          </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-5 col-xs-offset-1">

                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="caseFirstName" style="font-size:17px;">Your First Name</label>
                                                                            <asp:TextBox runat="server" class="form-control" id="caseFirstName"  aria-describedby="firstname"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="caseSurname" style="font-size:17px;">Your Surname</label>
                                                                            <asp:TextBox runat="server" class="form-control" id="caseSurname"  aria-describedby="surname"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-xs-5">

                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div class="form-group">
                                                                                <label for="caseJobTitle" style="font-size:17px;">Your Position / Job Title </label>
                                                                                <asp:TextBox runat="server" class="form-control" id="caseJobTitle"  aria-describedby="jobTitle"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div class="form-group">
                                                                                <label for="caseContactNumber" style="font-size:17px;">Your Contact Number</label>
                                                                                <asp:TextBox runat="server" class="form-control" id="caseContactNumber" aria-describedby="contactNumber"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div  class="col-xs-10 col-xs-offset-1">

                                                                <div class="alert alert-danger" role="alert" runat="server" id="alertArea" visible="false">
                                                                   <div class="media">
                                                                        <div class="media-body">
                                                                          <p><asp:Literal runat="server" ID="alertAreaMsg" /></p>
                                                                        </div>
                                                                   </div>
                                                                </div>

                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>

                                                <div class="panel-footer">
                                            <div class="row">
                                                <div class="col-xs-12 text-center">

                                                    <asp:Button ID="buttonAccess" runat="server" class="btn btn-success"  Text="Continue to Assessment" OnClick="buttonAccess_Click"  />

                                                </div>
                                            </div>
                                            </div>
                                            </div>

                                        </div>

                                    </div>

                            </div>


                    </ContentTemplate>
                       <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="buttonAccess" EventName="Click"  />
                       </Triggers>
                    </asp:UpdatePanel>

            </div>
        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
