﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responseOverview.aspx.cs" Inherits="responseOverview" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Search</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>



    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <link href="response.css?333" rel="stylesheet" />

</head>
<body runat="server" id="pageBody">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true" >
    </asp:ScriptManager>


    <script type="text/javascript" language="javascript">

        function showLoadingDiv(extraTimer) {
            $('#progressPopUp').fadeIn(1500);
        }

    </script>

    <asp:updateprogress id="progressPopUp" runat="server" dynamiclayout="true" DisplayAfter="1500">
        <progresstemplate>

            <%=ASNETNSP.SharedComponants.ReturnProgressDiv() %>

        </progresstemplate>
    </asp:updateprogress>



    <div class="container-fluid">
    <div class="row">



    <div runAt="server" id="reportMenu" class="subpageNavListMargin" style="padding-top: 10px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header">
            Navigation & Statistics
          </li>

          <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to main menu" OnClick="goToMainMenu_Click" />
        </ul>
    </div>



    <div runAt="server" id="reportContent" class="mainpageNavList"> <!-- Main Col Start -->

        <div class="container-fluid">
            <div class="row">
                <div runat="server" id="pageTitle" class="page_title col-md-5">
                    <h1>Health Assessment ( COVID-19 ) Client Overview</h1>
                    <span>Information valid as of <%=DateTime.Now %></span>
                </div>
            </div>
        </div>



    <div class="subpageNavListMobile" style="display:none;">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars" aria-hidden="true"></i> Navigation & Options <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                            </ul>
                        </li>
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>




<!-- ***************************** -->
<!-- *** Search Results begins *** -->
<!-- ***************************** -->


        <a href="#searchareaAnchor" id="searchareaAnchor" class="searchareaAnchor" >&nbsp;</a>

        <div runat="server" id="section_2" class="row section_2" style="display:none;">

            <div class="col-xs-12">
                <div class="panel panel-default">

                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Non AssessNET Client List

                    <asp:UpdatePanel id="updateRecordTotal" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                        <ContentTemplate>
                            <asp:Label runat="server" id="results_count" CssClass="badge pull-right" Visible="False"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    </h3>

                    </div>
                    <!-- Search results panel begins -->
                    <div class="panel-body"  id="section2Panel" runat="server">


                    <asp:UpdatePanel id="UpdateSearchResults" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                    <asp:HiddenField ID="savedSearchRefHiddenValue" runat="server" />
                    <asp:HiddenField ID="HiddenFieldServerSearchType" runat="server" />
                    <asp:HiddenField ID="HiddenFieldRemoveRef" runat="server" />


                      <div id="searchResultsArea" class="searchResultsArea" runat="server">

                      <div class="row">
                          <div class="col-xs-12">


                            <div runat="server" id="DisplayResults" class="DisplayResults">


                                <div class="table"> <!-- responsive removed by ML 27/03/2017 to fix issue with the options button dropdown -->
                                    <table class="table table-striped table-bordered table-liststyle01 searchResults">

                                    <div class="pagingSectionTopKEK" id="pagingSectionTopKEK"  >
                                        <div class="row">

                                            <!-- Search Key -->
                                            <div class="col-xs-8">
                                                <div class="iconTextBlockStyleContainer01">

                                                 </div>
                                            </div>
                                            <!-- End of Search Key -->

                                            <!-- Top Paging Section -->
                                            <div class="col-xs-4 text-right pagingSectionTop" id="pagingSectionTop" style="display:none;" >
                                                <div class="btn-group" role="group" aria-label="...">
                                                    <asp:Button ID="previousPageButtonTop" class="btn btn-primary previousPageButtonTop" runat="server" Text="&laquo;" OnClick="previousPage_Click" />
                                                    <div class="btn-group">
                                                        <asp:DropDownList ID="pageNavigationTop" runat="server" class="form-control" OnSelectedIndexChanged="returnSelectedPageTop" AutoPostBack="true"></asp:DropDownList>
                                                    </div>
                                                    <asp:Button ID="nextPageButtonTop" class="btn btn-primary nextPageButtonTop" runat="server" Text="&raquo;" OnClick="nextPage_Click" />
                                                </div>
                                            </div>
                                            <!-- End of Top Paging Section -->

                                         </div>
                                    </div>
                                    <br />

                                        <asp:ListView ID="tableContentResults" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnItemCommand="tableContentResults_ItemCommand" OnItemDataBound="tableContentResults_ItemDataBound">
                                            <LayoutTemplate>

                                                <thead>
                                                    <tr>
                                                        <th style="text-align:left;">Company Name</th>
                                                        <th width="150px">Status</th>
                                                        <th width="360px">Main Contact</th>
                                                        <th width="150px">People Registered</th>
                                                        <th width="150px">Assessments</th>
                                                        <th width="90px">Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                                </tbody>

                                            </LayoutTemplate>
                                            <ItemTemplate>

                                                <tr>
                                                    <td><%# Eval("clientName").ToString() %><br /><strong>Date:</strong> <%# Eval("gendatetime").ToString() %>&nbsp;<strong>Size:</strong> <%# Eval("Emp").ToString() %></td>
                                                    <td class="text-center"><h3><span class="label label-success pull-right btn-block" runat="server" style="margin-bottom:2px;" visible="false" id="setupComp">COMPLETE</span><span class="label label-danger btn-block" runat="server"  style="padding-bottom:2px;" visible="false" id="setupStatus">INCOMPLETE</span></h3><h3><span class="label label-warning" runat="server"   visible="false" id="setupTerms">TERMS</span></h3><h3><span class="label label-warning btn-block" runat="server"  visible="false" id="setupVal">VALIDATION</span></h3></td>
                                                    <td><asp:Literal runat="server" ID="client" /></td>
                                                    <td class="text-center"><div class="btn btn-block btn-info"><asp:Literal runat="server" ID="users" Text="0" /></div></td>
                                                    <td class="text-center"><div class="btn btn-block btn-warning"><asp:Literal runat="server" ID="assessments" Text="0" /></div></td>
                                                    <td><asp:Button runat="server" ID="commandOpt" CssClass="btn btn-success" CommandName="Approve" CommandArgument='<%# Eval("setup_reference") %>' Text="Approve" visible="false" /></td>
                                                </tr>

                                            </ItemTemplate>
                                        </asp:ListView>


                                    </table>



                                <!-- Bottom Paging Section -->
                                <div class="col-xs-12 text-right pagingSection" id="pagingSection" style="display:none;padding-right:0;margin-right:0;" >
                                    <asp:HiddenField ID="currenPageNumberHidden" runat="server" />
                                    <asp:HiddenField ID="maxPageNumberHidden" runat="server" />

                                    <div class="btn-group" role="group" aria-label="...">
                                        <asp:Button ID="previousPageButton" class="btn btn-primary previousPageButton" runat="server" Text="&laquo;" OnClick="previousPage_Click" />
                                        <div class="btn-group">
                                            <asp:DropDownList ID="pageNavigation" runat="server" class="form-control" OnSelectedIndexChanged="returnSelectedPage" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                        <asp:Button ID="nextPageButton" class="btn btn-primary nextPageButton" runat="server" Text="&raquo;" OnClick="nextPage_Click" />
                                    </div>
                                </div>
                                <!-- End of Bottom Paging Section -->

                                </div>




                            </div> <!-- display results end -->



                          </div>
                       </div>

                 </div>

                </ContentTemplate>
                </asp:UpdatePanel>

                   </div> <!-- end of search results panel body -->

                    <div class="panel-footer">
                      <div class="row">
                          <div class="col-xs-6 text-left">
                              <!-- Total Assessments -->
                          </div>
                      </div>
                    </div>

                 </div> <!-- end of search results panel -->
              </div> <!-- end of search results main column -->
          </div> <!-- end of search results row -->



<!-- ***************************** -->
<!-- **** Search Results Ends **** -->
<!-- ***************************** -->



    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />


    <script type="text/javascript">

        function BindControlEvents() {


        }

        $(document).ready(function () {

            BindControlEvents();

        });

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });

    </script>


</body>
</html>
