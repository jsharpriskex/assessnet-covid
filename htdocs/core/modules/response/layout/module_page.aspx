﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_page.aspx.cs" Inherits="responseMenu" validateRequest="false" enableEventValidation="false" %>
<%@ Import Namespace="ASNETNSP" %>
<%@ Register Src="~/core/system/usercontrols/Global/ModuleTraining/ModuleTraining.ascx" TagPrefix="TrainingWebControl" TagName="ModuleTraining" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Response Module</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../../framework/js/assessnet.js"></script>

        <script type="text/javascript" src="/core/framework/fusioncharts/licence/fusioncharts.js"></script>
    <script type="text/javascript" src="/core/framework/fusioncharts/licence/themes/fusioncharts.theme.fusion.js"></script>

     <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>
    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />
    <link href="../../../framework/css/custom.css?22222" rel="stylesheet" />
    <asp:Literal runat="server" ID="cssLinkClientStylesheet" Text="" />
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>


        <style type="text/css">
        .postby {float: right; color: Gray; font-size: 0.8em;}

    .tile-heading {
    padding: 5px 8px;
    text-transform: uppercase;
    background-color: #186f9d;
    font-size:15px;
    color: #FFF;
    }

        .tile-body {
            padding: 10px;
            color: #FFFFFF;
            line-height: 40px;
        }

    .fa-user:before {
    content: "\f007";

}

    .tile {
    margin-bottom: 15px;
    border-radius: 3px;
    background-color: #279FE0;
    color: #FFFFFF;
    transition: all 1s;
}
    .tile-footer {
    padding: 5px 8px;
    background-color: #186f9d;
    text-align:right;
}

    .tile a {
    color: #FFFFFF;
}


    .tile .tile-body i {
    font-size: 50px;
    opacity: 0.7;
    transition: all 1s;
    padding-top: 10px;
}

    .tile .tile-body h2 {
    font-size: 37px;
    margin: 5px;
    padding: 5px;
}

    </style>

<script>

   // var colourArray = ["#4473c5", "#00af50", "#fe0000", "#ffc000", "#ffc000"];


    function ColourGraphs() {

        var i = 0
        var colourArray = ["#4473c5", "#00af50", "#fe0000", "#ffc000", "#ffc000"];

        $("[class*=plot-group] rect").each(function (index) {
            $(this).attr("fill", colourArray[i]);
            i++;
        });



    }

    function test() {
        alert("done")
    }


    $(document).ready(function () {
        //ColourGraphs();
    });

</script>

</head>


<body>
    <form id="pageform" runat="server">



        <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>

        <script type="text/javascript" language="javascript">

            function showLoadingDiv(extraTimer) {
                $('#progressPopUp').fadeIn(3000);
            }

        </script>

        <div id="progressPopUp" style="display: none;">
            <div id='loading' class='updateMain' style='display: block !important;'>
                <div class='updateContent'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='updateContentBox'>
                                <div class='row'>
                                    <div class='updateSwirlyContainer' style='width: 90%;'>
                                        <div style='width: 100%; text-align: left !important;'>
                                            <div style='display: block; float: left;'>
                                                <img src='/core/media/images/core/loading.gif' width='40' />
                                            </div>
                                            <div class='h3-Processing-Block'>
                                                <h3 class='h3-Processing'><% =ts.GetResource("popup_processing")%></h3>
                                            </div>
                                            <div style='clear: both;'></div>
                                        </div>
                                    </div>
                                    <div class='text-center' style=''><% =ts.GetResource("popup_processing_description")%></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="container-fluid">

         <!-- Secure Info Modal -->
        <div class="modal fade" id="secureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="secureModalLabel">Peace of mind - Security is in our DNA at Riskex</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12">


                        <div class="media">
                          <div class="media-left">


                    <%--            <a href="#">
                                  <img src="img/secure-ency-logo.jpg" />
                                </a>--%>

                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">We ensure your data is safe, secure and handled correctly (GDPR).</h4>
                                <ul style="padding-top:10px;font-size:17px;">
                                    <li>Our service uses our core AssessNET platform, a cloud H&S service for 20 years, based around robust architecture and security measures.</li>
                                    <li>We are audited and certified by BSI to ISO 27001 Information Security Management, along with ISO 9001 in Quality Systems and Cyber Essentials.</li>
                                    <li>Our UK hosting provider Rackspace has ISO 27001 and PCI DSS, SOC 1, 2, 3 level security.</li>
                                    <li>Personal data for the COVID tool is totally encrypted, and the tool provides a secure connection at all times.</li>
                                    <li>We request minimal data from the user required for assessment and analytical purposes of COVID-19 at organisational level.</li>
                                    <li>Data from assessments and users is accessible by the organisation (you) by way of administrator permissions only.</li>
                                    <li>If a full deletion is requested, it will be done on request.  At the end of the scheme (31st October 2020) all data will be available to the organisation to export in full, prior to full deletion.</li>
                                    <li>Data is backed up to our UK Servers, held encrypted at all times and no backup data will be kept after the 31st October 2020, or if deletion is requested prior.</li>
                                    <li>Your data will not be shared with any third-party at any time.</li>
                                </ul>
                          </div>
                        </div>



                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->


                     <!-- Advert Modal -->
                    <div class="modal fade" id="IntroModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">


                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="IntroModalLabel">Learn to use this tool</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                <div class="col-xs-12 text-center">


                                    <iframe class="embeddedObject shadow resizable" name="embedded_content" scrolling="no" frameborder="0" type="text/html" style="overflow:hidden;" src="https://www.screencast.com/users/Riskex/folders/COVID-19/media/13b8e5ec-2aab-49e4-94db-d9cb0db2305d/embed" height="450" width="800"></iframe>
                               
                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-danger" id="closeVideo2" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>


                        </div>
                        </div>
                    </div>
                    <!-- End of Modal -->



                     <!-- Advert Modal -->
                    <div class="modal fade" id="UpgradeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">


                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="UpgradeModalLabel">Upgrade to Safe2Day!</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                <div class="col-xs-12 text-center">
                                        <iframe class="embeddedObject shadow resizable" name="embedded_content" scrolling="no" frameborder="0" type="text/html" style="overflow:hidden;" src="https://www.screencast.com/users/Riskex/folders/COVID-19/media/0a07a6c5-f69c-41e2-81a1-025e5c601e22/embed" height="450" width="800" ></iframe>

                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-danger" id="closeVideo" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>


                        </div>
                        </div>
                    </div>
                    <!-- End of Modal -->


                     <!-- Welcome Modal -->
                    <div class="modal fade" id="AdminWelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">


                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="AdminWelModalLabel">Welcome to your COVID-19 admin panel</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                <div class="col-xs-12 text-left" style="font-size:17px;">

                                    <p>You are all set!</p>
                                    <p>Using this admin panel, you can view assessments as they are completed, filter results, review real-time statistics, gain an overview of your current population, and change system configuration.</p>
                                    <p><strong>To access this dashboard in future, bookmark this address:  https://covid.riskex.co.uk/admin/ </strong></p>
                                    <p>Then enter the email address and password you used during your set up.</p>
                                    <div class="alert alert-success" style="margin-bottom:0px !important;margin-top:20px !important;">
                                    <p><strong>Next Step:</strong> Email the unique link to all your employees, so they can register and complete their assessments.</p>
                                     <p>You can copy the link from the next page, or by using the emails you were sent during your set up.</p>
                                    </div>

                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Continue</button>
                                </div>
                            </div>
                            </div>


                        </div>
                        </div>
                    </div>
                    <!-- End of Modal -->



                     <!-- Advert Modal -->
                    <div class="modal fade" id="advertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">


                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="advertModalLabel">Upgrade Offer  - Safe2day</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                <div class="col-xs-12 text-left" style="font-size:19px;">

                                    <p>Upgrade to our safe2day offer which will give you our enhanced edition of this tool.</p>
                                    <p>Contact our sales team today to discuss this offer in more detail at <strong>sales@riskex.co.uk</strong> or sign up to one of our weekly webinar sessions to learn more - <a href='https://www.riskex.co.uk/webinar/' target="_blank">Register Webinar Place</a></p>

                                </div>
                            </div>
                            </div>
                            <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-12 text-center">
                                <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            </div>


                        </div>
                        </div>
                    </div>
                    <!-- End of Modal -->



        <div class="row" style="padding-bottom:20px;padding-top:5px;">
        <div class="col-xs-8" style="padding-left:37px;">

                            <h1 style="margin:0px;padding-top:15px;padding-bottom:10px;font-size:30px;">Welcome <asp:Literal runat="server" ID="litAdminFirstName" /> <br /><span style="font-size:24px">Your Health Assessment (COVID-19) Admin Panel for <span  style="color:#c00c00;"><asp:Literal runat="server" ID="litCompanyName" /></span></span></h1>
        </div>
        <div class="col-xs-4 text-right" align="right">
             <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" style="width:235px;"/></a>
             <div class="sharethis-inline-follow-buttons"></div>
         </div>
        </div>

            <div class="row" runat="server" id="showLicenceAlert" visible="false">

                <div class="col-md-6">
                        <div class="alert alert-danger" role="alert">

                             <div class="media">
                                <div class="media-left">
                                    <i class="fa fa-3x fa-exclamation-triangle" aria-hidden="true"></i>
                                </div>
                                <div class="media-body">
                                <h4 class="media-heading">Licence not valid</h4>

                                    <p>Please contact our sales department on sales@riskex.co.uk to discuss enabling this module for your organisation FREE OF CHARGE</p>

                                </div>
                            </div>

                        </div>
                </div>

            </div>


            <div class="row" runat="server" id="hidControls">
                <div class="col-xs-4">

                    <div id="ReportResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="ReportResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="ReportResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="ReportResponseH1" runat="server"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h1>
                                                <h3 id="ReportResponseH3" runat="server">Your COVID-19 Health Assessment</h3>
                                                <p id="ReportResponseP" runat="server">Undertake a quick assesment to keep your workplace informed of your current situation</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="SearchResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="SearchResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="SearchResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="SearchResponseH1" runat="server"><i class="fa fa-search" aria-hidden="true"></i></h1>
                                                <h3 id="SearchResponseH3" runat="server">Search COVID-19 Health Assessments</h3>
                                                <p id="SearchResponseP1" runat="server">Use multiple criteria to drill down on cases along with overlay statuses</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                

                    <div id="PeopleResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="PeopleResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="PeopleResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="PeopleResponseH1" runat="server"><i class="fas fa-user-friends"></i></h1>
                                                <h3 id="PeopleResponseH3" runat="server">Search your population</h3>
                                                <p id="PeopleResponseP1" runat="server">Use multiple criteria to drill down on people and view their status</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div id="DashResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="DashResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="DashResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="DashResponseH1" runat="server"><i class="fa fa-bar-chart" aria-hidden="true"></i></h1>
                                                <h3 id="DashResponseH3" runat="server">Dashboard Statistics</h3>
                                                <p id="DashResponseP" runat="server">Get a real-time reivew of the current COVID-19 Health Assessments</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                   <div id="InputResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper" visible="false">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="InputResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="InputResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="InputResponseH1" runat="server"><i class="fa fa-users" aria-hidden="true"></i></h1>
                                                <h3 id="InputResponseH2" runat="server">Import Users</h3>
                                                <p id="InputResponseP" runat="server">Input non-standard Email Address on bulk to create accounts.</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="AdminResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper" visible="false">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="AdminResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="AdminResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="AdminResponseH1" runat="server"><i class="fas fa-wrench" aria-hidden="true"></i></h1>
                                                <h3 id="AdminResponseH2" runat="server">Add managers and locations</h3>
                                                <p id="AdminResponseP" runat="server">Administer your locations and add other users to this management console.</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="OverviewResponseContainer" runat="server" class="panel_wrapper panel-menu-item-wrapper" visible="false">
                        <div class="panel panel-default panel-menu-item menu-item ">
                            <div class="panel-body panel-menu-item-body">
                                <div id="OverviewResponseWrapper" runat="server" class="item_wrapper">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            <a id="OverviewResponseLink" runat="server" href="#" onclick="showLoadingDiv(1)">
                                                <h1 id="OverviewResponseH1" runat="server"><i class="fas fa-sitemap"></i></h1>
                                                <h3 id="OverviewResponseH2" runat="server">Current Clients</h3>
                                                <p id="OverviewResponseP" runat="server">Show details on current clients using this service</p>
                                            </a>
                                        </div>
                                        <div class="col-xs-1 item_options">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    <div class="row" runat="server" id="ELearningWrapper" onclick="$('#IntroModal').modal('show');" >
                        <div class="col-md-12">

                            <div class="alert alert-success" role="alert" style="margin-bottom:10px;margin-left:20px;margin-right:20px;padding-right:5px;">
                    
                            <div class="media">
                                  <div class="media-left">
                                      <i class="fas fa-2x fa-graduation-cap" style="margin-top:7px;" aria-hidden="true"></i>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading" style="margin-bottom:0px;margin-top:2px;">Learn to use this tool</h4>
                                    Get familiar with this module and its features.
                                  </div>
                                  <div class="media-left">
                                    <a href="#" class="btn btn-success btn btn-block" onclick="$('#IntroModal').modal('show');">Show Video</a>
                                  </div>
                                </div>
                            </div>

                        </div>
                    </div>

                     <div class="row" runat="server" id="Div2" onclick="$('#secureModal').modal('show');" >
                        <div class="col-md-12">

                            <div class="alert alert-warning" role="alert" style="margin-top:0px;margin-left:20px;margin-right:20px;padding-right:5px;">
                    
                            <div class="media">
                                  <div class="media-left">
                                      <i class="fas fa-2x fa-lock" style="margin-top:7px;" aria-hidden="true"></i>
                                  </div>
                                  <div class="media-body">
                                    <h4 class="media-heading" style="margin-bottom:0px;margin-top:2px;">Peace of mind, your security</h4>
                                    Learn how we ensure your data is safe and secure.
                                  </div>
                                <div class="media-left">
                                    <a href="#" class="btn btn-warning btn btn-block" onclick="$('#secureModal').modal('show');">Learn more</a>
                                  </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col-xs-6">

                    <TrainingWebControl:ModuleTraining ID="Training" runat="server" mod="RAPID" />

                        <div class="row">
                            <div class="col-md-12">

                                 <div class="panel panel-default" id="panel_config" runat="server" visible="true" style="border:5px solid !important; border-color:#c00c00 !important;">
                                            <div class="panel-heading">
                                            <h3 class="panel-title" style="font-size:18px; font-weight:bolder;"><i class="fa fa-bars" aria-hidden="true"></i>Email the link below to everyone in your team</h3>
                                            </div>
                                            <div class="panel-body" >

                                                <style>

                                                    .CompanyURL {
                                                        font-size: 18px;
                                                        color:#c00c00;
                                                        font-weight:bold;
                                                    }

                                                    .validationCode {
                                                        font-size: 35px;
                                                        text-align:center;
                                                        font-weight:bold;
                                                    }

                                                </style>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <p>This unique link will enable everyone you wish to undertake a COVID-19 Health Assessment to access the system at any time.</p>

                                                                <div class="alert alert-success CompanyURL" style="margin:0px;">

                                                                  <asp:Literal runat="server" ID="companyURLaccess" />

                                                                </div>

                                                            </div>


                                                        </div>

                                            </div>
                                            </div>


                            </div>
                        </div>


                       <div class="row" style="display:none;">
                            <div class="col-md-6">

                                <div class="tile">
                                   <div class="tile-heading" style="background-color:#b71f1f !important;">People with symptoms<span class="pull-right"></span></div>
                                       <div class="tile-body" style="background-color:#E06767 !important;"><i class="fa fa-thermometer-full" aria-hidden="true"></i>
                                       <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleSymptomsAll" Text="0" /></h2>
                                       </div>
                                   <div class="tile-footer" style="background-color:#b71f1f !important;">Company Wide</div>
                                </div>

                            </div>

                            <div class="col-md-6">
                                <div class="tile">
                                   <div class="tile-heading" style="background-color:#038a5d !important;">People fit to work<span class="pull-right"></span></div>
                                       <div class="tile-body" style="background-color:#5DBE9E !important;"><i class="fas fa-user-hard-hat"></i>
                                       <h2 class="pull-right"><asp:Literal runat="server" ID="dashboardPeopleFTW" Text="0" /></h2>
                                       </div>
                                   <div class="tile-footer" style="background-color:#038a5d !important;">Company Wide</div>
                                </div>

                            </div>
                        </div>

                                <div class="row" style="display:none;">
                            <div class="col-md-12">

                                <div class="row">

                                    <div class="col-md-3">
                                        <a href="https://www.riskex.co.uk" target="_blank"><img src="../img/AssessNET-PromoCovid.jpg" style="height:180px;"/></a>
                                    </div>
                                    <div class="col-md-9" style="padding-top:20px;">
                                        <h3>Class-Leading Health &amp; Safety Solution</h3>
                                        <p style="font-size:17px;">20 years of service, built by safety professionals.</p>
                                        <ul>
                                            <li><a style="color:#c00c00;" href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/risk-assessment-software/" target="_blank">Risk Assessment</a></li>
                                            <li><a style="color:#c00c00;" href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/dse-assessment/" target="_blank">Display Screen Equipment</a></li>
                                            <li><a style="color:#c00c00;" href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/hazard-reporting/" target="_blank">Hazard Reporting</a></li>
                                            <li><a style="color:#c00c00;" href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/" target="_blank">over 19 Health and Safety Modules</a></li>
                                        </ul>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                    

                      <div class="row" runat="server" id="statistics_Div" Visible="True">
                        <div class="col-md-12">
                        <div class="panel panel-default" id="Div1" runat="server">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i>Statistics</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">

                                        <asp:UpdatePanel runat="server" id="statistics_UpdatePanel" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="col-md-12" runat="server" id="graphingcontrols" visible="false" >
                                                    
                                                    <asp:LinkButton runat="server" id="Daily_LinkButton" CssClass="btn btn-default" CommandArgument="TODAY" OnCommand="statLinkButton_OnCommand">Today</asp:LinkButton>
                                                    <asp:LinkButton runat="server" id="sevenDay_LinkButton" CssClass="btn btn-primary" CommandArgument="SEVEN" OnCommand="statLinkButton_OnCommand">Week</asp:LinkButton>
                                                    <asp:LinkButton runat="server" id="month_LinkButton" CssClass="btn btn-default" CommandArgument="MONTH" OnCommand="statLinkButton_OnCommand">Month</asp:LinkButton>
                                                    <asp:LinkButton runat="server" id="year_LinkButton" CssClass="btn btn-default" CommandArgument="YEAR" OnCommand="statLinkButton_OnCommand">Year</asp:LinkButton>


                                                </div>
                                                <div class="col-md-12 text-center" style="padding-left:50px;padding-right:50px;">

                                                    <asp:Literal runat="server" id="stat_Literal" Visible="false"></asp:Literal>
                                                    <img src="F2W-GraphSample.jpg" style="cursor: pointer;width:100%" onclick="$('#UpgradeModal').modal('show');" />

                                                </div>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>


                                </div>

                            </div>
                            </div>
                        </div>

                        
                        



                </div>



                </div>

                <div class="col-xs-2 text-center">

                   <img src="../setup/img/Fit2Work-300dpi-ReMake.jpg" />
        
                  <img src="../img/safe2daybanneradvert.png" style="cursor: pointer;" onclick="$('#UpgradeModal').modal('show');"  />
             
                  

                </div>

            </div>

        </div>


<script>

    $(document).ready(function () {

        $("#UpgradeModal").modal({
            show: false,
            backdrop: 'static'
        });

        $("#IntroModal").modal({
            show: false,
            backdrop: 'static'
        });

        $('#closeVideo').click(function () {
            location.reload();
        });

        $('#IntroModal').click(function () {
            location.reload();
        });
        
    });

</script>



    </form>

</body>
</html>
