﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="auth.aspx.cs" Inherits="ReponseAuth" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Training Management Module</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

        <meta name="robots" content="noindex" />
        <meta property="og:title" content="AssessNET: COVID-19 Assessment Service" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://www.assessweb.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1600" />
        <meta property="og:image:height" content="900" />
        <meta property="og:url" content="https://www.assessweb.co.uk/" />
        <meta name="twitter:card" content="summary_large_image" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

     <style>

       body p {

            font-size:17px;

        }

       html,body {
           margin:10px;
           padding:0px;

            background: url('background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

       .textInput {
            height: 45px;
            font-size: 30px;
            text-align: center;
            background:#efefef;
            text-transform:uppercase;
        }

    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>




    <div class="container">
    <div class="row">



     <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Secure your account - Health Assessment ( COVID-19 )</h1>

            </div>
        </div>


        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">


                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;color:#000000"><i class="fa fa-bars" aria-hidden="true"></i> Complete Registration</h3>
                  </div>
                  <div class="panel-body">


                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanelInfoArea" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>


                    <div class="row" style="padding-top:10px;">
                        <div class="col-md-12">

                            <p>Please create your password to secure your records:</p>
                            <ul>
                                <li>Minimum of 8 characters</li>
                                <li>At least 1 number</li>
                                <li>Upper / Lowercase</li>
                            </ul>
                            <p>&nbsp;</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">


                                  <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h3 class="panel-title" style="font-size:17px;color:#000000"><i class="fa fa-bars" aria-hidden="true"></i> Set a password</h3>
                                  </div>
                                  <div class="panel-body">

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="caseFirstName" style="font-size:17px;color:#000000">Create a Password</label>
                                                    <asp:TextBox runat="server" class="form-control" id="authPass1"  TextMode="password"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="caseSurname" style="font-size:17px;color:#000000">Confirm your Password</label>
                                                    <asp:TextBox runat="server" class="form-control" id="authPass2" TextMode="password" ></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                  </div>
                                  </div>


                        </div>
                    </div>




                    <div class="row" runat="server" id="alert" visible="false">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="alert alert-warning" role="alert" runat="server" id="alertarea">
                            <p><asp:Literal runat="server" ID="msg" /></p>

                            </div>
                        </div>
                    </div>




                           <asp:Literal runat="server" ID="test" />
                    </ContentTemplate>
                       <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="buttonVerify" EventName="Click"  />
                       </Triggers>
                    </asp:UpdatePanel>


                    </div>


                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button ID="buttonVerify" runat="server" class="btn btn-success"  Text="Save Password and Continue" onClick="buttonVerify_Click"  />

                        </div>
                    </div>
                    </div>



                </div>

        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
