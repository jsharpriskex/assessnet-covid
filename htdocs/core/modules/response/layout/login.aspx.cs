﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class ReponseLogin : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session.aspx"); Response.End(); }


        if (!IsPostBack)
        {
            SetInputFocus();
        }

    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }

    protected bool passwordValid(string _authPassword, string _emailAddress)
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckPassword", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@auth", _authPassword));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                Session["CORP_CODE"] = objrs["corp_code"].ToString();
                Session["YOUR_ACCOUNT_ID"] = objrs["acc_ref"].ToString();
                Session["YOUR_EMAIL"] = objrs["acc_contactEmail"].ToString();
                Session["YOUR_LANGUAGE"] = "en-gb";
                Session["YOUR_ACCESS"] = "6";
                return true;

            }
            else
            {
                return false;
            }


        }


    }

    public void SetInputFocus()
    {
        TextBox tbox = this.pageform.FindControl("authPass") as TextBox;
        if (tbox != null)
        {
            ScriptManager.GetCurrent(this.Page).SetFocus(tbox);
        }
    }
    protected void resetPassword(string _emailAddress, string _corpCode)
    {

        // all we will do is remove their password, and send out a relink, this wil force a new password to be set on entry
        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseResetPassword", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corpcode", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.ExecuteReader();


        }

    }

    protected void sendAccessLink(string _emailAddress, string _corpCode)
    {

        string _accessLink;
        string _accessSalt;

        // all we will do is remove their password, and send out a relink, this wil force a new password to be set on entry
        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseGetUserAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                _accessLink = objrs["uniqueLink"].ToString();
                _accessSalt = objrs["uniqueSalt"].ToString();

                string emailSubject;
                string emailTitle;
                string emailBody;
                string emailLink;
                string access_link;

                access_link = Application["CONFIG_PROVIDER_SECURE"] + "/login/?t=HAM&u=" + _accessLink + _accessSalt;
                //test2.Text = access_link;

                emailLink = "<a href='" + access_link + "'>" + access_link + "</a>";
                emailSubject = "Password Reset - Health Assessment (COVID-19)";
                emailTitle = "Health Assessment (COVID-19)";
                emailBody = "<p>Your password has been reset, ready for you to set a new one.</p><p>By clicking the link below, you will be asked to set a new password and access the assessment process.</p><p><strong>Do NOT share this link, its private to your account.</strong><p><p>" + emailLink + "</p><p>&nbsp;</p><p><strong>How to undertake a re-assessment later</strong></p><p>Once your password has been set, you will no longer need to use any unique links.</p><p>Simply visit <a href='" + Application["CONFIG_PROVIDER_SECURE"] + "'>" + Application["CONFIG_PROVIDER_SECURE"] + "</a> and enter your email address and password as instructed to undergo another assessment.</p><p>&nbsp;</p><p><strong>Problems using the link?</strong></p><p>If you have any issue accessing your record using the link above, please contact your Health and Safety department.</p>";

                Boolean emailSent;
                emailSent = SendEmail(_emailAddress, emailSubject, emailTitle, emailBody, "template", true);

            }




        }

    }

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "AssessNET Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.Priority = MailPriority.High;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }


            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }


    protected void buttonVerify_Click(object sender, EventArgs e)
    {

        string emailAddress = Session["YOUR_EMAIL"].ToString();

        alertArea.Visible = false;
        alertArea.Attributes.Add("class", "alert alert-danger");

        string _authPassword = authPass.Text;

        if (passwordValid(_authPassword, emailAddress))
        {

            Response.Redirect("~/core/modules/response/responseMain.aspx");
            Response.End();

        }
        else
        {

            alertArea.Visible = true;
            alertArea.Attributes.Add("class", "alert alert-danger");
            alertAreaMsg.Text = "The password you entered was incorrect, please try again.";

        }


    }

    protected void buttonReset_Click(object sender, EventArgs e)
    {

        string emailAddress = Session["YOUR_EMAIL"].ToString();
        string corpCode = Session["CORP_CODE"].ToString();

        resetPassword(emailAddress, corpCode);
        sendAccessLink(emailAddress, corpCode);

        buttonReset.Visible = false;
        buttonVerify.Visible = false;
        passwordArea.Visible = false;
        passwordResetMsg.Visible = true;


        UpdatePanelInfoArea.Update();
        UDP_controls.Update();

        Session.Clear();
        Session.Abandon();


    }



}

