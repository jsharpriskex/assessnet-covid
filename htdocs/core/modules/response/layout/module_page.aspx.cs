﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using System.Globalization;
using FusionCharts.Charts;
using FusionCharts.DataEngine;
using FusionCharts.Visualization;
using Telerik.Web.UI;
using DataModel = FusionCharts.DataEngine.DataModel;
using ListItem = System.Web.UI.WebControls.ListItem;
using System.Runtime.Remoting;
using System.ComponentModel.Design;
using FarPoint.Excel;

public partial class responseMenu : System.Web.UI.Page
{

    public string corpCode;
    public string accRef;
    public string adminUser;
    public string accLang;
    public string yourAccessLevel;
    public string yourFirstName;
    public string permissionsActive;
    public bool superUSER;

    public string moduleCode;

    public TranslationServices ts;


    public Graphing currentDayGraph;
    public Graphing currentWeeklyGraph;

    protected void Page_Load(object sender, EventArgs e)
    {

        // Session Check
        if (Session["CORP_CODE"] == null) { Response.Redirect("~/core/system/general/session_admin.aspx"); Response.End(); }
        // Permission Check
        if (Context.Session["YOUR_ACCESS"] == null) { Response.End(); }


        Session.Contents["YOUR_LANGUAGE"] = "en-gb";

        //***************************
        // CHECK THE SESSION STATUS
        //SessionState.checkSession(Page.ClientScript);
        Session.Timeout = 60;
        adminUser = Context.Session["YOUR_ACCESS"].ToString();
        corpCode = Context.Session["CORP_CODE"].ToString();
        accRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        accLang = Context.Session["YOUR_LANGUAGE"].ToString();

        if (Session["SETUP_ADMIN"] != null)
        {
            if (Session["SETUP_ADMIN"].ToString() == "Y")
            {

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#AdminWelModal').modal('show');", true);
                Session["SETUP_ADMIN"] = "N";
            }
        }


        permissionsActive = (SharedComponants.getPreferenceValue(corpCode, accRef, "USER_PERMS_ACTIVE", "pref") == "Y") ? "1" : "0";
        //END OF SESSION STATUS CHECK
        //***************************

        //***************************
        // INITIATE AND SET TRANSLATIONS
        ts = new TranslationServices(corpCode, accLang, "menus");
        // END TRANSLATIONS
        //***************************

        yourFirstName = SharedComponants.getPreferenceValue(corpCode, accRef, "YOUR_FIRSTNAME", "user");
        yourAccessLevel = SharedComponants.getPreferenceValue(corpCode, accRef, "YOUR_ACCESS", "user");
        moduleCode = "Response";

        CheckLicence();
        CreateHazard();
        SearchHazard();
        People();
        Dashboard();
        dashboardStats();
        loadClientConfig();

        // Show Admin Panel
        if (adminUser == "0") { Admin(); AdminResponseContainer.Visible = true; }
        if (corpCode == "1000000006") { Overview(); OverviewResponseContainer.Visible = true; }

        if (!Page.IsPostBack)
        {
           // GenerateGraphs("TODAY");
        }

    }

    private void GenerateGraphs(string type)
    {

        switch (type)
        {
            case "SEVEN":
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#advertModal').modal('show');", true);
                //GenerateWeeklyRollingDayGraph();
                break;
            case "TODAY":
                GenerateCurrentDayGraph();
                break;
            case "MONTH":
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#advertModal').modal('show');", true);
                //GenerateCurrentMonthGraph();
                break;
            case "YEAR":
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#advertModal').modal('show');", true);
                //GenerateCurrentYearGraph();
                break;
        }

        statistics_UpdatePanel.Update();
        GenerateJavascriptForUpdatePanel();

    }

    private void GenerateJavascriptForUpdatePanel()
    {
        string dataString = stat_Literal.Text;
        string toFind = "javascript";
        if (dataString.Contains("javascript"))
        {
            var code = dataString.Split(new[] { "<script type=\"text/javascript\">" }, StringSplitOptions.None)[1].Replace("</script>", "");

            if (currentDayGraph != null)
            {
                code += ";ColourGraphs();";
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "Test", code, true);
        }
    }

    private void GenerateCurrentDayGraph()
    {

        sevenDay_LinkButton.CssClass = "btn btn-default";
        Daily_LinkButton.CssClass = "btn btn-primary";


        currentDayGraph = new Graphing(corpCode, ts);
        currentDayGraph.ChartTitle = Formatting.FormatTextInput(DateTime.Now.DayOfWeek.ToString()) + " - " + litCompanyName.Text;
        currentDayGraph.ChartSubtitle = Formatting.FormatTextInput("Statistics for today");

        currentDayGraph.ChartFontSize = "12";
        currentDayGraph.ChartType = Graphing.GraphType.Column;
        currentDayGraph.GraphLiteral = stat_Literal;

        currentDayGraph.ChartValue = Graphing.GraphValue.Daily;
        currentDayGraph.XAxisTitle = "";
        currentDayGraph.YAxisTitle = "People";


        currentDayGraph.GenerateGraph("currentDay");

    }

    private void GenerateWeeklyRollingDayGraph()
    {

        sevenDay_LinkButton.CssClass = "btn btn-primary";
        Daily_LinkButton.CssClass = "btn btn-default";

        currentWeeklyGraph = new Graphing(corpCode, ts);
        currentWeeklyGraph.ChartTitle = Formatting.FormatTextInput("5 Day Rolling");
        currentWeeklyGraph.ChartSubtitle = Formatting.FormatTextInput("Statistics for the last 5 days");

        currentWeeklyGraph.ChartFontSize = "12";
        currentWeeklyGraph.ChartType = Graphing.GraphType.Column;
        currentWeeklyGraph.GraphLiteral = stat_Literal;

        currentWeeklyGraph.ChartValue = Graphing.GraphValue.Weekly;
        currentWeeklyGraph.XAxisTitle = "";
        currentWeeklyGraph.YAxisTitle = "People";


        currentWeeklyGraph.GenerateGraph("weeklyChart");

    }

    protected void setActivated()
    {
        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupSetActivated", dbConn) { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.ExecuteNonQuery();

        }


    }


    protected void loadClientConfig()
    {

        bool _activated = false;

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupGetCompanyAccessDetails", dbConn) {CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                companyURLaccess.Text = Application["CONFIG_PROVIDER_SECURE"].ToString() + "/?a=" + objrs["uniqueLink"].ToString() + objrs["uniqueSalt"].ToString();
                litAdminFirstName.Text = yourFirstName;
                litCompanyName.Text = objrs["setupCompanyName"].ToString();
                _activated = Convert.ToBoolean(objrs["activated"].ToString());

                if (!_activated)
                {
                    litCompanyName.Text = litCompanyName.Text + " --";
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#IntroModal').modal('show');", true);
                    setActivated();
                }


            }
        }


    }



    public override void Dispose()
    {
        if (ts != null)
        {
            ts.Dispose();
        }
        base.Dispose();
    }


    public void CheckLicence()
    {

        string IsLicenceValid = "FALSE";

        try
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {


                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckLicence", dbConn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    IsLicenceValid = objrs["validLicence"].ToString();

                }

                objrs.Close();

                if (IsLicenceValid == "FALSE")
                {

                    showLicenceAlert.Visible = true;
                    hidControls.Visible = false;

                }



            }

        }
        catch (Exception)
        {
            throw;
        }



    }



    public void dashboardStats()
    {

        string tier2Selection = "0";
        string tier3Selection = "0";
        string tier4Selection = "0";
        string tier5Selection = "0";
        string tier6Selection = "0";

        try
        {

            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {


                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleFTW", dbConn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleFTW.Text = objrs["countvalue"].ToString();

                }

                objrs.Close();


                cmd = new SqlCommand("Module_HA.dbo.dashboardPeopleSymptomsAny", dbConn) { CommandType = CommandType.StoredProcedure };
                cmd.Parameters.Add(new SqlParameter("@var_corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bu", tier2Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bl", tier3Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_bd", tier4Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier5", tier5Selection));
                cmd.Parameters.Add(new SqlParameter("@var_srch_tier6", tier6Selection));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    dashboardPeopleSymptomsAll.Text = objrs["countvalue"].ToString();

                }

                objrs.Close();




            }

        }
        catch (Exception)
        {
            throw;
        }


    }



    public void CreateHazard()
    {
        string moduleCode = "ReportResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");


            SetLink(moduleLink, "../responsePrecheck.aspx", "");

    }

    public void SearchHazard()
    {
        string moduleCode = "SearchResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

        SetLink(moduleLink, "../responseSearch.aspx", "");

        //HideModule(moduleCode, moduleLink);
    }

    public void People()
    {
        string moduleCode = "PeopleResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

        SetLink(moduleLink, "../responsePeople.aspx", "");

        //HideModule(moduleCode, moduleLink);
    }

    public void Dashboard()
    {
        string moduleCode = "DashResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

        SetLink(moduleLink, "../dashboard.aspx", "");

    }

    public void Import()
    {
        string moduleCode = "InputResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

        if (yourAccessLevel == "0")
        {
            SetLink(moduleLink, "../responseImport.aspx", "");
        }
        else
        {
            HideModule(moduleCode, moduleLink);
        }
    }

    public void Admin()
    {
        string moduleCode = "AdminResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

            SetLink(moduleLink, "../responseControl.aspx", "");

    }

    public void Overview()
    {
        string moduleCode = "OverviewResponse";
        HtmlAnchor moduleLink = (HtmlAnchor)FindControl(moduleCode + "Link");

        SetLink(moduleLink, "../responseOverview.aspx", "");

    }

    public void HideModule(string moduleCode, HtmlAnchor moduleLink)
    {
        SetModuleAccess(moduleCode, moduleLink, "#");

        HtmlGenericControl container = (HtmlGenericControl)FindControl(moduleCode + "Container");
        container.Visible = false;
    }

    public void SetModuleAccess(string moduleCode, HtmlAnchor link, string failLinkAddress)
    {
        HtmlGenericControl container = (HtmlGenericControl)FindControl(moduleCode + "Container");
        HtmlGenericControl wrapper = (HtmlGenericControl)FindControl(moduleCode + "Wrapper");
        HtmlGenericControl h1 = (HtmlGenericControl)FindControl(moduleCode + "H1");
        HtmlGenericControl h3 = (HtmlGenericControl)FindControl(moduleCode + "H3");
        HtmlGenericControl p = (HtmlGenericControl)FindControl(moduleCode + "P");

        // Add a class
        AppendCss(container, "disabled");
        AppendCss(wrapper, "disabled");
        AppendCss(h1, "disabled");
        AppendCss(h3, "disabled");
        AppendCss(p, "disabled");
        SetLink(link, failLinkAddress, "disabled");
    }

    public static void SetLink(HtmlAnchor control, string link, string cssClass)
    {
        control.HRef = link;

        if (cssClass.Length > 0)
        {
            control.Attributes["class"] = cssClass;
        }
    }

    public static void AppendCss(HtmlGenericControl control, string cssClass)
    {
        // Ensure CSS class is definied
        if (string.IsNullOrEmpty(cssClass)) return;

        // Append CSS class
        if (string.IsNullOrEmpty(control.Attributes["class"]))
        {
            // Set our CSS Class as only one
            control.Attributes["class"] = cssClass;
        }
        else
        {
            // Append new CSS class with space as seprator
            control.Attributes["class"] += (" " + cssClass);
        }
    }


    protected void statLinkButton_OnCommand(object sender, CommandEventArgs e)
    {
        string arg = e.CommandArgument.ToString();

        GenerateGraphs(arg);

    }
}


public class Graphing
{


    private readonly string _corpCode;//

    public TranslationServices ts;


    public string ChartTitle;//
    public string ChartSubtitle;//
    public string XAxisTitle;//
    public string YAxisTitle;//
    public string ChartFontSize;//

    public GraphType ChartType;//
    public GraphValue ChartValue;


    public string GraphOrder;

    public DataTable GraphDataTable;
    private DataView GraphDataView;

    public DateTime? DateStart; //
    public DateTime? DateEnd; //

    public bool BreakdownByQuarter; //
    public bool BreakdownByYear; //

    private bool _isMultiDate;
    private bool _isMultiType;

    public Literal GraphLiteral;



    // To do: Incident Options


    // Basic constructor, only really has data in it when the client has clicked generate chart.
    public Graphing(string corpCode, TranslationServices _ts)
    {

        _corpCode = corpCode;
        GraphDataTable = new DataTable();

        ts = _ts;

    }



    // Function for when the Chart is ran on button click.
    public void GenerateGraph(string chartID)
    {

        // Pass the data into the sproc to generate results.
        // Return as DataTable.
        // Then we define what chart to show based on the users criteria.


        using (SqlConnection dbConn = ASNETNSP.ConnectionManager.GetDatabaseConnection())
        {
            try
            {

                SqlDataReader objrs = null;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.[dashboardGraphing_HomePage]", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
                cmd.Parameters.Add(new SqlParameter("@type", ChartValue.ToString()));
                cmd.Parameters.Add(new SqlParameter("@weekEndDate", DateTime.Now));



                objrs = cmd.ExecuteReader();

                GraphDataTable.Load(objrs);

                cmd.Dispose();
                objrs.Dispose();

                switch (GraphOrder)
                {
                    case "ASC":
                        GraphDataView = GraphDataTable.DefaultView;
                        GraphDataView.Sort = "Frequency ASC";
                        GraphDataTable = GraphDataView.ToTable();
                        break;

                    case "DESC":
                        GraphDataView = GraphDataTable.DefaultView;
                        GraphDataView.Sort = "Frequency DESC";
                        GraphDataTable = GraphDataView.ToTable();
                        break;
                }



            }
            catch (Exception e)
            {
                throw e.GetBaseException();
            }
        }


        // Replace any nulls with 0
        foreach (DataColumn col in GraphDataTable.Columns) col.ReadOnly = false;

        int miscCounter = 0;

        for (int i = 0; i < GraphDataTable.Rows.Count; i++)
        {
            for (int j = 0; j < GraphDataTable.Columns.Count; j++)
            {
                if (string.IsNullOrEmpty(GraphDataTable.Rows[i][j].ToString()))
                {

                    GraphDataTable.Rows[i][j] = "0";
                }
                else
                {
                    GraphDataTable.Rows[i][j] = Formatting.FormatTextInputField(GraphDataTable.Rows[i][j].ToString());
                }
            }



        }


        // Chop the position off the daily type
        if (ChartValue == GraphValue.Daily)
        {
            GraphDataTable.Columns.RemoveAt(GraphDataTable.Columns.Count - 1);
        }


        // Decide what graph type to show
        switch (ChartType)
        {
            case GraphType.Column:
                GenerateColumnChart(chartID);
                break;
            case GraphType.Doughnut:
                GenerateDoughnutChart();
                break;
            case GraphType.Line:
                GenerateLineChart();
                break;
            case GraphType.Pie:
                GeneratePieChart();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }


        GraphDataTable.Dispose();


    }

    private void GenerateColumnChart(string chartID)
    {
        //Initiate the Chart
        Charts.ColumnChart primaryChart = new Charts.ColumnChart("chart");


        StaticSource source = new StaticSource(GraphDataTable);
        DataModel primaryData = new DataModel();


        primaryData.DataSources.Add(source);

        primaryChart.Width.Percentage(100);
        primaryChart.Height.Pixel(350);
        primaryChart.Data.Source = primaryData;
        primaryChart.Caption.Text = ChartTitle;
        primaryChart.SubCaption.Text = ChartSubtitle;
        primaryChart.Values.Show = true;
        primaryChart.ThemeName = FusionChartsTheme.ThemeName.FUSION;
        primaryChart.Scrollable = false;
        //primaryChart.Legend.Show = false;

        //primaryChart.Labels.Rotate = true;
        primaryChart.ThreeD = false;
        primaryChart.XAxis.Text = XAxisTitle;
        primaryChart.YAxis.Text = YAxisTitle;

        primaryChart.YAxis.FontSize = Convert.ToInt32(ChartFontSize);
        primaryChart.XAxis.FontSize = Convert.ToInt32(ChartFontSize);


        primaryChart.Export.Enabled = true;
        primaryChart.Export.ExportedFileName = "AssessNET_Graph";
        primaryChart.Export.TargetWindow = Exporter.ExportTargetWindow.BLANK;
        primaryChart.Export.Action = Exporter.ExportAction.DOWNLOAD;

        // Font size
        primaryChart.Caption.FontSize = 23;
        primaryChart.SubCaption.FontSize = 19;
        primaryChart.Legend.FontSize = 18;
        primaryChart.XAxis.FontSize = 18;
        primaryChart.YAxis.FontSize = 18;
        primaryChart.Data.Categories.FontSize = 18;
        primaryChart.Values.PlaceInside = false;

        string[] colours = { "#5d62b5", "#afafaf", "#f2726", "#ffc533", "#62b58f", "#afafaf" };

        //primaryChart.PaletteColors("#5d62b5", "#29c3be", "#f2726f");



        if (GraphDataTable.Rows.Count > 0)
        {

            switch (ChartValue)
            {
                case GraphValue.Weekly:
                    break;
                case GraphValue.Daily:
                    primaryChart.Legend.Show = false;
                    primaryChart.Events.AttachRenderedEvents(FusionChartsEvents.RenderedEvents.RENDERCOMPLETE, "ColourGraphs");
                    primaryChart.Events.AttachRenderedEvents(FusionChartsEvents.RenderedEvents.CHARTUPDATED, "ColourGraphs");
                    primaryChart.Events.AttachGenericEvents(FusionChartsEvents.GenericEvents.DATAPLOTCLICK, "ColourGraphs");
                    primaryChart.Events.AttachGenericEvents(FusionChartsEvents.GenericEvents.CHARTMOUSEMOVE, "ColourGraphs");
                    primaryChart.Events.AttachGenericEvents(FusionChartsEvents.GenericEvents.DATAPLOTROLLOVER, "ColourGraphs");
                    primaryChart.Events.AttachGenericEvents(FusionChartsEvents.GenericEvents.DATAPLOTCLICK, "ColourGraphs");
                    primaryChart.Events.AttachGenericEvents(FusionChartsEvents.GenericEvents.DATAPLOTROLLOUT, "ColourGraphs");
                    break;
            }

            //renderString = renderString.Replace("script type", "");

            GraphLiteral.Text = primaryChart.Render();
        }
        else
        {
            GraphLiteral.Text = ts.GetResource("lbl_nodatafound");
        }
    }

    private void GenerateDoughnutChart()
    {
        //Initiate the Chart
        Charts.DoughnutChart primaryChart = new Charts.DoughnutChart("chart");



        StaticSource source = new StaticSource(GraphDataTable);
        DataModel primaryData = new DataModel();
        primaryData.DataSources.Add(source);

        primaryChart.Width.Percentage(100);
        primaryChart.Height.Pixel(500);
        primaryChart.Data.Source = primaryData;
        primaryChart.Caption.Text = ChartTitle;
        primaryChart.SubCaption.Text = ChartSubtitle;
        primaryChart.Values.Show = true;
        primaryChart.ThemeName = FusionChartsTheme.ThemeName.FUSION;

        primaryChart.Labels.Rotate = true;

        primaryChart.Export.Enabled = true;
        primaryChart.Export.ExportedFileName = "AssessNET_Graphh";
        primaryChart.Export.TargetWindow = Exporter.ExportTargetWindow.BLANK;
        primaryChart.Export.Action = Exporter.ExportAction.DOWNLOAD;



        if (GraphDataTable.Rows.Count > 0)
        {
            GraphLiteral.Text = primaryChart.Render();
        }
        else
        {
            GraphLiteral.Text = ts.GetResource("lbl_nodatafound");
        }
    }

    private void GenerateLineChart()
    {
        //Initiate the Chart
        Charts.LineChart primaryChart = new Charts.LineChart("chart");



        StaticSource source = new StaticSource(GraphDataTable);
        DataModel primaryData = new DataModel();
        primaryData.DataSources.Add(source);

        primaryChart.Width.Percentage(100);
        primaryChart.Height.Pixel(500);
        primaryChart.Data.Source = primaryData;
        primaryChart.Caption.Text = ChartTitle;
        primaryChart.SubCaption.Text = ChartSubtitle;
        primaryChart.Values.Show = true;
        primaryChart.ThemeName = FusionChartsTheme.ThemeName.FUSION;

        primaryChart.Labels.Rotate = true;
        primaryChart.XAxis.Text = XAxisTitle;
        primaryChart.YAxis.Text = YAxisTitle;

        primaryChart.Export.Enabled = true;
        primaryChart.Export.ExportedFileName = "AssessNET_Graph";
        primaryChart.Export.TargetWindow = Exporter.ExportTargetWindow.BLANK;
        primaryChart.Export.Action = Exporter.ExportAction.DOWNLOAD;

        primaryChart.YAxis.FontSize = Convert.ToInt32(ChartFontSize);
        primaryChart.XAxis.FontSize = Convert.ToInt32(ChartFontSize);



        if (GraphDataTable.Rows.Count > 0)
        {
            GraphLiteral.Text = primaryChart.Render();

        }
        else
        {
            GraphLiteral.Text = "No Data Found";
        }
    }

    private void GeneratePieChart()
    {
        //Initiate the Chart
        Charts.PieChart primaryChart = new Charts.PieChart("chart");


        StaticSource source = new StaticSource(GraphDataTable);
        DataModel primaryData = new DataModel();
        primaryData.DataSources.Add(source);

        primaryChart.Width.Percentage(100);
        primaryChart.Height.Pixel(500);
        primaryChart.Data.Source = primaryData;
        primaryChart.Caption.Text = ChartTitle;
        primaryChart.SubCaption.Text = ChartSubtitle;
        primaryChart.Values.Show = true;
        primaryChart.ThemeName = FusionChartsTheme.ThemeName.FUSION;

        primaryChart.Labels.Rotate = true;
        primaryChart.Export.Enabled = true;
        primaryChart.Export.ExportedFileName = "AssessNET_Graph";
        primaryChart.Export.TargetWindow = Exporter.ExportTargetWindow.BLANK;
        primaryChart.Export.Action = Exporter.ExportAction.DOWNLOAD;


        if (GraphDataTable.Rows.Count > 0)
        {
            GraphLiteral.Text = primaryChart.Render();

        }
        else
        {
            GraphLiteral.Text = ts.GetResource("lbl_nodatafound");
        }

    }



    public string DeliminateString(List<string> input)
    {
        return string.Join(",", input.ToArray()); ;
    }



    // The choice of graph (bar, pie, column, etc).
    public enum GraphType
    {
        Column = 1,
        Doughnut = 2,
        Line = 3,
        Pie = 4
    }


    public enum GraphValue
    {
        Daily = 1,
        Weekly = 2
    }


}






