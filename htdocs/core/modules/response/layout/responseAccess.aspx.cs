﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;
using FarPoint.Excel;
using System.Threading;
using System.Text.RegularExpressions;
using System.Linq;
using Telerik.Windows.Documents.Spreadsheet.Model;

public partial class ResponseAccess : System.Web.UI.Page
{

    public string _accessLink = "";
    public string _accessSalt = "";
    public string _quickLink = "";
    public string _corpcode = "";
    public string _accRef = "";
    public string _companyName = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            // Do we have a quick link
            if (Session["quickLink"] != null)
            {

                if (Session["quicklink"].ToString().Length > 10)
                {
                    _quickLink = Session["quickLink"].ToString();
                    // Extract corp code from the link, we wont need to check the user.
                    getCorpCode();

                    refcode.Value = _corpcode;
                }

                //test3.Text = _quickLink;

            }


        }

    }

    protected void getCorpCode()
    {
        // We have a link, so grab a corp code
        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckCompanyAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@access_link", _quickLink));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                _corpcode = objrs["corp_code"].ToString();
                _companyName = objrs["company_name"].ToString();

                companyTitleName.Text = "For " + _companyName;

            }


        }


    }

    public int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    protected void logEmailSent(string _accessLink, string _emailAddress)
    {

        // log that email was sent with datatime
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseLogEmail", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
            cmd.Parameters.Add(new SqlParameter("@email_address", _emailAddress));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }

    }

    public static bool IsValidEmail(string email)
    {

        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "Riskex Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.Priority = MailPriority.High;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }


            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }

    public void createAccessLink(string _acc_ref, string _corp_code, string _emailAddress)
    {

        // New unique ID with some salt
        _accessLink = Guid.NewGuid().ToString();
        _accessSalt = RandomNumber(100000, 999999).ToString();
        _accessLink = _accessLink.Replace("-", "");


        // This needs to be a trigger at some point
        // Clean out any links currently in the system

        // Removed for now
        //////////using (SqlConnection dbConn = GetDatabaseConnection())
        //////////{
        //////////    SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCleanLinks", dbConn);
        //////////    cmd.CommandType = CommandType.StoredProcedure;
        //////////    cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
        //////////    cmd.Parameters.Add(new SqlParameter("@corp_code", _corp_code));
        //////////    cmd.ExecuteNonQuery();

        //////////    cmd.Dispose();

        //////////}

        // Register the new link
        ////using (SqlConnection dbConn = GetDatabaseConnection())
        ////{
        ////    SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCreateAccessLink", dbConn);
        ////    cmd.CommandType = CommandType.StoredProcedure;
        ////    cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
        ////    cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
        ////    cmd.Parameters.Add(new SqlParameter("@corp_code", _corp_code));
        ////    cmd.Parameters.Add(new SqlParameter("@ip_address", getIPAddress()));
        ////    cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
        ////    cmd.Parameters.Add(new SqlParameter("@access_salt", _accessSalt));
        ////    cmd.ExecuteNonQuery();

        ////    cmd.Dispose();

        ////}

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseProvideAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corp_code));
            cmd.Parameters.Add(new SqlParameter("@ip_address", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _accessSalt));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                _accessLink = objrs["access_link"].ToString();
                _accessSalt = objrs["access_salt"].ToString();

            }

            cmd.Dispose();

        }


    }

    protected bool passwordValid(string _authPassword)
    {
        string _emailAddress = userEmail.Text;

        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckPassword", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@auth", _authPassword));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                    Session["CORP_CODE"] = objrs["corp_code"].ToString();
                    Session["YOUR_ACCOUNT_ID"] = objrs["acc_ref"].ToString();
                    Session["YOUR_EMAIL"] = objrs["acc_contactEmail"].ToString();
                    Session["YOUR_LANGUAGE"] = "en-gb";
                    return true;

            } else
            {
                return false;
            }


        }


    }


    protected void buttonAccessReset_Click(object sender, EventArgs e)
    {

        Response.Redirect("~/core/modules/response/layout/");

    }

    protected void buttonAccessEmail_Click(object sender, EventArgs e)
    {

        string _acc_ref = "";
        string _corp_code = "";
        string emailDomain = "";
        string _acc_multiples = "YES";
        string _acc_valid_client = "NO";
        string _domain_Count = "";
        string _email_Count = "";
        string _multiple_clients = "";
        string _acc_hasAccount = "";
        string _acc_passwordCount = "";

        bool validClient = true;
        bool emailFound = false;
        bool hasAccount = false;
        bool emailConfirmed = false;
        bool multipleEmail = false;
        string emailAddress = userEmail.Text.Replace(" ", "");

        alertArea.Visible = false;
        alertArea.Attributes.Add("class", "alert alert-danger");

        // Is this a password check?
        if (!emailAreaBox.Visible && IsValidEmail(emailAddress))
        {

            string _authPassword = authPass.Text;

            if (passwordValid(_authPassword)) {

                Session["AUTH"] = "Y";
                Session["YOUR_ACCESS"] = "3";
                Response.Redirect("~/core/modules/response/responseMain.aspx");
                Response.End();

            } else
            {

                alertArea.Visible = true;
                alertArea.Attributes.Add("class", "alert alert-danger");
                alertAreaMsg.Text = "The password you entered was incorrect, please try again.";

            }


        }
        else
        {


            // Lets check if the email is valid, wont get to this point if at password check
            if (IsValidEmail(emailAddress))
            {


                // Just get the domain
                string address = emailAddress;
                string host;
                host = address.Split('@')[1];

                _corpcode = refcode.Value;

                using (SqlConnection dbConn = GetDatabaseConnection())
                {

                    SqlDataReader objrs = null;
                    SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckUser", dbConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@emailAddress", emailAddress));
                    cmd.Parameters.Add(new SqlParameter("@EmailAddressDomain", host));
                    cmd.Parameters.Add(new SqlParameter("@corpcode", _corpcode));
                    objrs = cmd.ExecuteReader();

                    if (objrs.HasRows)
                    {
                        objrs.Read();

                        _acc_ref = objrs["acc_ref"].ToString();
                        _corp_code = objrs["corp_code"].ToString();
                        _acc_multiples = objrs["acc_multiples"].ToString();
                        _acc_valid_client = objrs["valid_client"].ToString();
                        _domain_Count = objrs["domain_counts"].ToString();
                        _email_Count = objrs["email_count"].ToString();
                        _acc_hasAccount = objrs["has_password"].ToString();
                        _acc_passwordCount = objrs["password_count"].ToString();
                        _multiple_clients = objrs["multiple_clients"].ToString();

                    }

                    cmd.Dispose();
                    objrs.Dispose();


                    if (_acc_multiples == "NO" && _acc_valid_client == "YES") { emailFound = true; }
                    if (_acc_valid_client == "NO") { validClient = false; }
                    if (_acc_multiples == "YES") { multipleEmail = true; }
                    if (_acc_hasAccount == "YES") { hasAccount = true; }

                }



                //test2.Text = "<strong>DEBUG OUTPUT</strong> (ignore during test period) <br/>Password Count: " + _acc_passwordCount + " Has Password: " + _acc_hasAccount + " Multiple Clients: " + _multiple_clients + " Email Counts:" + _email_Count + " Email: " + emailAddress + " Email domain:" + host + " _acc_ref: " + _acc_ref + " _corp_code: " + _corp_code + " _acc_multiples: " + _acc_multiples + " _acc_valid_client: " + _acc_valid_client;

                // Do they already have an account with valid password
                if (hasAccount)
                {

                    emailAreaBox.Visible = false;
                    passwordArea.Visible = true;
                    buttonAccessReset.Visible = true;

                }

                // Before we go further, if they have no account and corp code is TEMP, we need to tell them to get a link
                if (!hasAccount && _corp_code == "TEMP")
                {

                    Response.Redirect("~/core/modules/response/layout/nolink.aspx");
                    Response.End();

                }


                if (emailFound && !hasAccount)
                {


                    // Ok so we can create the link and get going
                    createAccessLink(_acc_ref, _corp_code, emailAddress);


                    string emailSubject;
                    string emailTitle;
                    string emailBody;
                    string emailLink;
                    string access_link;

                    access_link = Application["CONFIG_PROVIDER_SECURE"] + "/login/?t=HAM&u=" + _accessLink + _accessSalt;
                    //test2.Text = access_link;

                    emailLink = "<a href='" + access_link + "'>" + access_link + "</a>";
                    emailSubject = "RISKEX COVID-19 Health Assessment";
                    emailTitle = "Health Assessment ( COVID-19 )";
                    emailBody = "<p>Thank you for requesting your Health Assessment (COVID-19) access.</p><p>To start your assessment, click this link:</p><p>link: " + emailLink + "</p><p>&nbsp;</p><p>You will be asked for some basics details and to set a password to keep your assessment safe and secure.</p>";

                    Boolean emailSent;
                    emailSent = SendEmail(emailAddress, emailSubject, emailTitle, emailBody, "template", true);

                    if (emailSent)
                    {
                        logEmailSent(_accessLink + _accessSalt, emailAddress);

                        emailArea.Visible = false;
                        buttonAccessEmail.Visible = false;
                        buttonAccessReset.Visible = true;
                        ConfirmMessage.Visible = true;
                        emailAddressOutput.Text = emailAddress;

                    }
                    else
                    {
                        alertArea.Visible = true;
                        alertArea.Attributes.Add("class", "alert alert-danger");
                        alertAreaMsg.Text = "There has been an issue with sending the email, is your email address correct?";


                    }



                }


                if (!validClient)
                {
                    alertArea.Visible = true;
                    alertAreaMsg.Text = "We can't find a valid licence associated to your email address, please contact your H&S department.";
                }

                if (multipleEmail)
                {
                    alertArea.Visible = true;
                    alertAreaMsg.Text = "Your email address is used across multiple clients, you are unable to use this service.";
                }



            }
            else
            {
                alertArea.Visible = true;
                alertAreaMsg.Text = "The email address you provided is not a valid format, please check and try again.";
            }

        }

    }

}