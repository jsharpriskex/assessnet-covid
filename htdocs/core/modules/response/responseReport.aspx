﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="responseReport.aspx.cs" Inherits="responseReport"  MaintainScrollPositionOnPostback="false" EnableEventValidation="false" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapid Response Form</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css">
   <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="../../framework/datepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />


    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="../../framework/datepicker/js/moment.js"></script>
    <script src="../../framework/datepicker/js/bootstrap-datetimepicker.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap2-toggle.min.js"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>


    <link href="../../../version3.2/layout/upgrade/css/newlayout.css" rel="stylesheet" />
    <link href="response.css" rel="stylesheet" />
    <script src="../../framework/js/assessnet.js"></script>
    <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-13178301-5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-13178301-5');
    </script>

    <style>


    <style>

        .alert {
            padding:8px;
        }

        .media {
            padding-top: 4px;
        }

        .media-heading {
            margin-bottom:0px;
        }

        .st-left {display:none !important;}
        .sharethis-inline-follow-buttons {padding-right:5px !important;}


               html,body {

           margin:10px;
           padding:0px;

            background: url('background-covid-v3-trans.png') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/

       }


    </style>


</head>

<body>
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


        <!-- Secure Info Modal -->
        <div class="modal fade" id="secureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="secureModalLabel">Peace of mind - Security is in our DNA at Riskex</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12">


                        <div class="media">
                          <div class="media-left">


                    <%--            <a href="#">
                                  <img src="img/secure-ency-logo.jpg" />
                                </a>--%>

                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">We ensure your data is safe, secure and handled correctly.</h4>
                                <ul style="padding-top:10px;font-size:17px;">
                                    <li>Our service uses our core AssessNET platform, a cloud H&S service for 20 years.  Robust architecture and security measures.</li>
                                    <li>We are audited and certified by BSI to ISO 27001 Information Security Management along with ISO 9001 in Quality Systems and Cyber Essentials.</li>
                                    <li>Our hosting provider Rackspace of which our servers are hosted here in the UK is also ISO 27001 along with PCI DSS, SOC 1,2, 3 level security.</li>
                                    <li>Personal data for the COVID tool is encrypted at all times, it also provides a secure connection at all times to the end-user.</li>
                                    <li>We only request the minimal of data from the user required for assessment and analytical purpose of COVID-19 at organisational level.</li>
                                    <li>Data of assessments and users is only accessible by the organisation (you) by way of administrator users only.</li>
                                    <li>If a full deletion is requested, it will be done on request.  At the end of the scheme (31st October 2020) all data will be available to the organisation to export in full prior to full deletion.</li>
                                    <li>Data is backed up to our UK Servers, held encrypted at all times and no backup data will be kept after the 31st October 2020 or if deletion is requested prior.</li>
                                    <li>Your data will not be shared with any third-party at any time.</li>
                                </ul>
                          </div>
                        </div>



                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

    <div class="container-fluid">
    <div class="row">


    <div class="subpageNavListMargin" style="padding-top: 19px;">
        <ul class="list-group subpageNavList">
          <li class="list-group-item header" runat="server" id="li_header" visible="false">
            Navigation
          </li>
            <li class="list-group-item" runat="server" id="li_item1" visible="false">
                <a href="#anchorGeneralDetails">General Details</a>
            </li>
            <li class="list-group-item" runat="server" id="li_item2" visible="false">
                <a href="#anchorResponses">Responses</a>
            </li>
            <asp:Button ID="goToMainMenu" runat="server" class="btn btn-danger btn btn-block" Text="" OnClick="goToMainMenu_Click" visible="false" />
            <asp:Button ID="goToDashboard" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="Back to Dashboard" OnClick="goToDash_Click"  visible="false"  />
            <asp:Button ID="goToSearch" runat="server" class="btn btn-danger btn btn-block" style="margin-top:0px;" Text="" OnClick="goToSearch_Click"  visible="false"  />

        <div style="background-color:#ffffff;padding:15px;border:solid 1px #ddd;margin-top:10px;border-top-left-radius:4px;border-top-right-radius:4px;border-bottom-left-radius:4px;border-bottom-right-radius: 4px;" class="text-center">

           <a href="https://www.riskex.co.uk" target="_blank"><img src="img/AssessNET-Web2016.png" /></a>

            <div class="media" onclick="$('#secureModal').modal('show');" style="padding-bottom:10px;cursor:pointer;">
                <div class="media-left">

                <%--     <a href="#">
                        <img src="secure-ency-logo.jpg" />
                    </a>--%>

                </div>
                <div class="media-body">
                <h4 class="media-heading"><i class="fas fa-lock"></i> Peace of mind<br />your security</h4>
                Learn how we ensure your data is safe and secure.

                </div>
            </div>


           <p>AssessNET contains 20 additional Health and Safety Modules.</p>
               
               <uL class="text-left" style="margin-left:15px;">
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/risk-assessment-software/" target="_blank" style="color:#c00c00;">Risk Assessment</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/hazard-reporting/" target="_blank" style="color:#c00c00;">Hazard Reporting</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/dse-assessment/" target="_blank" style="color:#c00c00;">DSE Assessment</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/safety-inspection-online-software/" target="_blank" style="color:#c00c00;">Safety Inspection</a></li>
                   <li><a href="https://www.riskex.co.uk/ehs-health-and-safety-software-modules/" target="_blank" style="color:#c00c00;">More...</a></li>
               </uL>
             
           <img src="img/crown-commercial-services-logo.png" style="margin-top:5px;padding:10px;" />
           <img src="setup/img/Fit2Work-300dpi-ReMake.jpg" style="margin-top:5px;" />

        </div>


        </ul>



    </div>

    <div class="mainpageNavList"> <!-- Main Col Start -->

        <div class="row" style="padding-bottom:15px;">
        <div class="col-xs-8">
                <h1 style="padding-top:5px;font-size:30px;">Health Assessment (COVID-19)</h1>
                <span style="font-size:28px;">For <asp:Literal runat="server" ID="litCompanyName" /></span>
        </div>
        <div class="col-xs-4 text-right" align="right">
             <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" style="width:235px;"/></a>
             <div class="sharethis-inline-follow-buttons"></div>
         </div>
        </div>

         <div id="divAssessmentCompleteSetup" class="alert alert-success" runat="server" visible="false">
            <div class="row">
                <div class="col-xs-9" style="padding-top:15px;">
                    <h3><strong>Assessment Complete - Thank You: stay alert, control the virus, save lives.</strong></h3>
                    <p style="font-size:17px;">Your Health Assessment is complete, thank you. Now that you have completed your own assessment, you can access your admin panel and kick start the process to your entire workforce </p>
                </div>
                <div class="col-xs-3 text-center" style="margin-top:30px;">
                    <div class="form-group">
                        <a href="../../navigation/" target="_parent">
                            <button type="button" class="btn btn-lg btn-success">Go to Admin Panel</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="divAssessmentComplete" class="alert alert-success" runat="server" >
            <div class="row">
                <div class="col-xs-12">
                    <h3><strong>Assessment Complete - Thank You: stay alert, control the virus, save lives.</strong></h3>
                    <p style="font-size:17px;">Your Health Assessment is complete, thank you. This service is in place to help your employer understand the full impact of COVID-19 and take action if necessary. Please ensure you complete another Assessment if your situation changes, or if prompted by AssessNET.</p>
                </div>
<%--                <div class="col-xs-3 text-center" style="margin-top:30px;">
                    <div class="form-group">
                        <a href="<% Response.Write(Application["CONFIG_PROVIDER_SECURE"].ToString()); %>" target="_parent">
                            <button type="button" class="btn btn-lg btn-success">Close and Finish</button>
                        </a>
                    </div>
                </div>--%>
            </div>
        </div>


        <div runat="server" id="section_1" class="row section_1">
            <div class="col-xs-12" id="pros">


                            <div class="panel panel-default">
                              <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> General Details <a href="#" id="anchorGeneralDetails"></a></h3>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                    </div>
                              </div>

                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-xs-6">

                                        <table class="table table-bordered table-searchstyle01">
                                            <tbody>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Date / Time</th>
                                                    <td><asp:Literal runat="server" ID="lit_datetime" /> - (<asp:Literal runat="server" ID="lit_dateduration" /> Days ago)</td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Person</th>
                                                    <td><asp:Literal runat="server" ID="lit_person" /> </td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Job Title</th>
                                                    <td><asp:Literal runat="server" ID="lit_jobtitle" /> </td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Contact Phone</th>
                                                    <td><asp:Literal runat="server" ID="lit_contactphone" /></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Contact Email</th>
                                                    <td><asp:Literal runat="server" ID="lit_contactemail" /></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Age Range</th>
                                                    <td><asp:Literal runat="server" ID="lit_agerange" /></td>
                                                </tr>
                                                <tr>
                                                    <th class="text-left" style="width:130px;">Location</th>
                                                    <td>
                                                        <%=ASNETNSP.SharedComponants.getStructureElementName(tier2Ref, 1, corpCode) %>
                                                        <%=ASNETNSP.SharedComponants.getStructureElementName(tier3Ref, 2, corpCode) %>
                                                        <%=ASNETNSP.SharedComponants.getStructureElementName(tier4Ref, 3, corpCode) %>
                                                        <%=ASNETNSP.SharedComponants.getStructureElementName(tier5Ref, 4, corpCode) %>
                                                        <%=ASNETNSP.SharedComponants.getStructureElementName(tier6Ref, 5, corpCode) %>


                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>





                                    </div>
                                    <div class="col-xs-6 CaseAlerts">

                                        <h1 style="font-size:25px;">Key Factors</h1>
                                        <hr style="margin-top:10px;" />

                                        <div class="alert alert-success" role="alert" id="alert08" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-check" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Has recovered</h4>
                                             Has had COVID-19 or associated symptoms and is now recovered?
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-danger" role="alert" id="alert01" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fas fa-2x fa-virus"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Infection Notice</h4>
                                             Has a confirmed case of COVID-19
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-danger" role="alert" id="alert02" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fas fa-2x fa-virus"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Infection Notice</h4>
                                             Currently has symptoms associated to COVID-19
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-warning" role="alert" id="alert09" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Has Underlying Health Conditions</h4>
                                             Higher risk of serious complications if contracted
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-warning" role="alert" id="alert07" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Risk of Spread</h4>
                                             Currently has symptoms or has had a case of COVID-19 but is not isolated
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-warning" role="alert" id="alert05" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Risk of Infection</h4>
                                             Currently has members in the household with symptoms
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-warning" role="alert" id="alert04" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Self-Isolation Warning</h4>
                                             Currently under Self-Isolation
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-info" role="alert" id="alert03" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">NHS not Informed</h4>
                                             The NHS have not been contacted via 111, so unaware of this case
                                             </div>
                                        </div>
                                        </div>


                                        <div class="alert alert-success" role="alert" id="alert06" runat="server" visible="false">
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-check" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Fit for Work</h4>
                                             Currently feels well enough to work
                                             </div>
                                        </div>
                                        </div>

                                        <div class="alert alert-danger" role="alert" id="alert10" runat="server" visible="false" >
                                        <div class="media">
                                             <div class="media-left"><i class="fa fa-2x fa-exclamation-triangle" aria-hidden="true"></i></div>
                                             <div class="media-body">
                                             <h4 class="media-heading">Not Fit for Work</h4>
                                             Currently does not feel well enough to work
                                             </div>
                                        </div>
                                        </div>







                                    </div>
                                </div>

                            </div>
                            </div>



                            <div class="panel panel-default">
                              <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-10">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Responses <a href="#" id="anchorResponses"></a></h3>
                                        </div>
                                        <div class="col-xs-2 text-right">

                                        </div>
                                    </div>
                              </div>

                            <div class="panel-body">


                            <div class="table">
                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Question</th>
                                        <th width="150px">Response</th>
                                    </tr>
                                </thead>
                                <tbody>
                                          <tr id="q1row"  class="questionRow" runat="server" visible="false" >
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q1his">CONCERN</span></h3>
                                                Have you been officially tested positive for COVID-19

                                            </td>
                                            <td width="10px" id="q1control" runat="server" style="font-size:20px;" class="text-center">
                                                <asp:Literal runat="server" ID="q1_ans"  />
                                            </td>

                                        </tr>
                                        <tr id="q2row"  class="questionRow" runat="server" visible="false" >
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q2his">CONCERN</span></h3>
                                                Do you currently have any of the following symptoms?:

                                                <p>
                                                <ul>
                                                    <li><strong>high temperature</strong>:<br />this means you feel hot to touch on your chest or back (you do not need to measure your temperature)</li>
                                                    <li><strong>new, continuous cough</strong>:<br />this means coughing a lot for more than an hour, or 3 or more coughing episodes in 24 hours (if you usually have a cough, it may be worse than usual)</li>
                                                    <li><strong>loss or change to your sense of smell or taste</strong>:<br />this means you've noticed you cannot smell or taste anything, or things smell or taste different to normal</li>

                                                </ul>
                                                </p>

                                            </td>
                                            <td width="10px" id="q2control" runat="server" style="font-size:20px;" class="text-center">

                                                <asp:Literal runat="server"  ID="q2_ans"  />
                                            </td>

                                        </tr>
                                        <tr id="q3row"  class="questionRow" runat="server" visible="false" >
                                            <td>Have you previously had the above symptoms and recovered?

                                                <asp:HiddenField ID="HiddenField8" Value='111' runat="server" />
                                            </td>
                                            <td width="10px" id="q3control_" runat="server" style="font-size:20px;" class="text-center">
                                                <asp:Literal runat="server"  ID="q3_ans" />
                                            </td>

                                        </tr>

                                              <tr id="q4row"  class="questionRow" runat="server" visible="false">
                                                <td>
                                                    <h3><span class="label label-info pull-right" runat="server" visible="false" id="q4his">NOTED</span></h3>
                                                    Have you contacted NHS 111 since developing symptoms?

                                                </td>
                                                <td width="10px" id="q4control" runat="server" style="font-size:20px;" class="text-center">

                                                    <asp:Literal runat="server"  ID="q4_ans" />
                                                </td>
                                             </tr>

                                            <tr id="q11row"  class="questionRow" runat="server" visible="false">
                                                <td>
                                                    <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q11his">CONCERN</span></h3>
                                                    Do you suffer from any underlying health conditions?

                                                </td>
                                                <td width="10px" id="q11control" runat="server" style="font-size:20px;" class="text-center">

                                                    <asp:Literal runat="server" ID="q11_ans" />
                                                </td>

                                             </tr>


                                             <tr id="q5row"  class="questionRow" runat="server" visible="false">
                                                <td>On what date did symptoms first occur?

                                                </td>
                                                <td width="10px" id="q5control" runat="server" style="font-size:20px;" class="text-center" >
                                                    <asp:Literal runat="server" ID="q5_ans" />
                                                </td>

                                            </tr>

                                            <tr id="q6row"  class="questionRow" runat="server" visible="false">
                                                <td>
                                                    <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q6his">CONCERN</span></h3>
                                                    Are you currently under isolation from others?


                                                </td>
                                                <td width="10px" id="q6control" runat="server" style="font-size:20px;" class="text-center">
                                                    <asp:Literal runat="server"  ID="q6_ans"  />
                                                </td>

                                            </tr>


                                        <tr id="q7row"  class="questionRow"  runat="server" visible="false">
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q7his">CONCERN</span></h3>
                                                How many people live in your house / apartment with you?


                                            </td>
                                            <td width="10px" id="q7control" runat="server" style="font-size:20px;" class="text-center">
                                                <asp:Literal runat="server"  ID="q7_ans"  />
                                            </td>

                                        </tr>
                                        <tr id="q8row"  class="questionRow"  runat="server" visible="false">
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q8his">CONCERN</span></h3>
                                                Are any of the people in your household showing the symptoms above?

                                            </td>
                                            <td width="10px" id="q8control" runat="server" style="font-size:20px;" class="text-center">

                                                <asp:Literal runat="server" ID="q8_ans" />
                                            </td>

                                        </tr>
                                        <tr id="q9row"  class="questionRow" runat="server" visible="false">
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q9his">CONCERN</span></h3>
                                                Are those showing symptoms currently self-isolated from you?


                                            </td>
                                            <td width="10px" id="q9control" runat="server" style="font-size:20px;" class="text-center">

                                                <asp:Literal runat="server"  ID="q9_ans"  />
                                            </td>

                                        </tr>

                                        <tr id="q10row"  class="questionRow" runat="server" visible="false">
                                            <td>
                                                <h3><span class="label label-danger pull-right" runat="server" visible="false" id="q10his">CONCERN</span></h3>
                                                Are you feeling well enough to work?


                                            </td>
                                            <td width="10px" id="q10control" runat="server" style="font-size:20px;" class="text-center">

                                                <asp:Literal runat="server"  ID="q10_ans" />
                                            </td>

                                        </tr>

                             </tbody>
                           </table>


                            <div class="table" runat="server" visible="false" id="assessmentCommentsDiv">

                            <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <thead>
                                    <tr>
                                        <th style="text-align: left;">Additional Comments</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><asp:Literal runat="server" ID="assessmentComments" /></td>
                                    </tr>
                                </tbody>
                            </table>

                            </div>


                       </div>




                            </div>
                           </div>


            <a href="#assessmentAnchor" id="assessmentAnchor" class="assessmentAnchor" >&nbsp;</a>



            </div>
        </div>

    </div> <!-- Main Col End -->
    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    <br/><br />

    </form>
    <%--script src="dropzone/dropzone.js" type="text/javascript"></script>--%>
       <script type="text/javascript" src="../../framework/js/intro.js"></script>
    <script type="text/javascript">



        function BindControlEvents() {

            $(function () {
                $('#datetimepicker8').datetimepicker({
                    maxDate: moment(),
                    format: 'DD/MM/YYYY HH:mm',
                    sideBySide: true,
                    allowInputToggle: true,
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    }
                });
            });




        }

        //Initial bind
        $(document).ready(function(){

            BindControlEvents();

        });

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function() {
            BindControlEvents();
        });


    </script>



</body>
</html>
