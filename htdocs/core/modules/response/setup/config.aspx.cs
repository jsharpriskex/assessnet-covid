﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Linq;
using ASNETNSP;


public partial class SetupConfig : System.Web.UI.Page
{
    public string corpCode;
    public string setupReference;

    protected void Page_Load(object sender, EventArgs e)
    {

        Session.Timeout = 90;
        corpCode = Session["corpCode"].ToString();

        if (!IsPostBack)
        {
            loadClientConfig();
            showLocations();

        }

    }

    protected void loadClientConfig()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupGetCompanyAccessDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                companyURLaccess.Text = "https://covid.riskex.co.uk/?a=" + objrs["uniqueLink"].ToString() + objrs["uniqueSalt"].ToString();
                //companyValidationNumber.Text = objrs["validation_code"].ToString();
                userEmail.Text = objrs["emailaddress"].ToString();
                hidFirstName.Value = objrs["setupFirstName"].ToString();

            }
        }


    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public bool DoPasswordsMatch(string _password1, string _password2)
    {
        // Clean the data before compare

        string password1 = Regex.Replace(_password1, @"\s", "");
        string password2 = Regex.Replace(_password2, @"\s", "");

        bool result = password1.Equals(password2, StringComparison.Ordinal);

        return result;

    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMSetup"]));
        connection.Open();

        return connection;
    }

    protected void removeLocation(string _locID)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminRemoveLocation", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@tier_ref", _locID));
            cmd.ExecuteNonQuery();

            showLocations();

        }


    }


    public void locationList_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string locID = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "locRemove":
                removeLocation(locID);
                UDP_Locations.Update();
                break;

        }


    }

    protected void setPassword(string _password)
    {

        string _accRef = Guid.NewGuid().ToString();
        string _salt = ASNETNSP.Authentication.setSalt(512);

        string _passwordHash = ASNETNSP.Authentication.setSHA512Hash(_password, _salt);

        // Set the password and create the admin account whilst there.
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupAddAdmin", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@password", _passwordHash));
            cmd.Parameters.Add(new SqlParameter("@salt", _salt));
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
            cmd.ExecuteNonQuery();
        }

        // Need this for login once setup
        Session.Contents["YOUR_ACCOUNT_ID"] = _accRef;

    }

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "Riskex Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }

            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }

    protected void savePassword_Click(object sender, EventArgs e)
    {

        string _password1 = passW1.Text;
        string _password2 = passW2.Text;

        passW1.Text = "";
        passW1.Text = "";

        if (_password1.Length > 7)
        {

            if (_password1.Any(char.IsUpper))
            {

                if (_password1.Any(char.IsNumber))
                {



                    if (DoPasswordsMatch(_password1, _password2))
                    {
                        setPassword(_password1);

                        passwordArea.Visible = false;
                        pwAlertArea.Visible = false;
                        passwordAreaAlert.Visible = true;
                        passW1.ReadOnly = true;
                        passW2.ReadOnly = true;
                        savePassword.Visible = false;
                        pwLabel_attention.Visible = false;

                    }
                    else
                    {
                        pwAlertArea.Visible = true;
                        pwAlert.Attributes.Add("Class", "alert alert-danger");
                        pwMsg.Text = "<strong>Alert: </strong>The passwords you entered do not match, please try again. Remember, passwords are CaSe sensitive";
                    }




                }
                else
                {
                    pwAlertArea.Visible = true;
                    pwAlert.Attributes.Add("Class", "alert alert-danger");
                    pwMsg.Text = "<strong>Alert: </strong>The password does not contiain any numbers, it must include at least one.";
                }


            } else
            {
                pwAlertArea.Visible = true;
                pwAlert.Attributes.Add("Class", "alert alert-danger");
                pwMsg.Text = "<strong>Alert: </strong>The password is the right length, but it does not contain a combination of lower and UPPER case letters.";
            }

        } else
        {
            pwAlertArea.Visible = true;
            pwAlert.Attributes.Add("Class", "alert alert-danger");
            pwMsg.Text = "<strong>Alert: </strong>The password you selected is too short, please ensure it is at least 8 characters long";
        }


    }

    protected void showLocations()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupShowLocation", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();

            locationList.DataSource = objrs;
            locationList.DataBind();

            if (objrs.HasRows) { locLabel_attention.Visible = false; }


        }

    }

    protected void addLocation_Click(object sender, EventArgs e)
    {
        string _location = addLocationText.Text;

        if (_location.Length > 0) {

            using (SqlConnection dbConn = GetDatabaseConnection())
            {
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupAddLocation", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@location", _location));
                cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                cmd.ExecuteNonQuery();

                addLocationText.Text = "";
            }

            showLocations();

        }

    }

   // protected void setupAdminAccounts()
   // {


   //     using (SqlConnection dbConn = GetDatabaseConnection())
   //     {
   //         SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupSendEntryAdminEmails", dbConn);
   //         cmd.CommandType = CommandType.StoredProcedure;
   //         cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
   //         cmd.ExecuteReader();

   //     }

   // }

    protected void closeFinish_Click(object sender, EventArgs e)
    {
        // Ok before we go, lets check all is done
        if (locLabel_attention.Visible == false && pwLabel_attention.Visible == false)
        {
            // So all is done on the page, confirm it all
            Session.Contents["setupComplate"] = "true";
            // Send email with main details

            string emailSubject;
            string emailTitle;
            string emailBody;
            string _setupEmail;
            string access_link;
            string emailFirstName = hidFirstName.Value;

            Boolean emailSent;

            access_link = "<a href='" + companyURLaccess.Text + "'>" + companyURLaccess.Text + "</a>";

            _setupEmail = userEmail.Text;

            // Send Admin email with setup details
            emailSubject = "System Details - COVID-19 Assessment Platform";
            emailTitle = "COVID-19 Health Assessment Service - System Details";
            emailBody = "<p>Hi " + emailFirstName + ",</p><p>Welcome to AssessNET, the home of Online Health and Safety Management.  Your system is all set up and ready to go.</p><p>Use the link below, which is unique to your organisation – </p><ul><li>Email this link to your employees so they can register and complete their assessment.  We have sent you another email containing a template, if you wish to use it.<p>" + access_link + "</p></li></ul><p>Use this link to login to your Dashboard and Admin Panel, using your email and password.</p><p><a href='https://covid.riskex.co.uk/admin/'>https://covid.riskex.co.uk/admin/</a></p><p><strong>IMPORTANT</strong></p><p>As the lead administrator and primary license holder for your company, you have main access to the data.  You can sign up additional administrators from within the Admin Panel, so they can use reports and view dashboard statistics.</p><p>Please note that your company is responsible for managing the data received to the dashboard.  The information provided to you is strictly confidential to your company. No others, apart from third parties to whom you provide access, will have access to this data.</p><p>If you provide a dashboard to a consultant or any third parties, you do so at your own risk and no liability rests with Riskex or our sponsors. </p><p>Please also note that this service is provided up until the 30th October 2020 and/or in accordance with the Terms and Conditions.</p><p>Please respect the lockdown conditions and take care of your health and wellbeing.</p><p>Best Regards</p><p>AssessNET Team</p>";

            //emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);


            // Send template email to admin
            emailSubject = "Employee Access Template - COVID-19 Assessment Platform";
            emailTitle = "Health Assessment (COVID-19) <br/> Employee Access Template";
            emailBody = "<p>Welcome to AssessNET, <strong>COPY AND PASTE</strong> the email below to all your employees and volunteers:</p><p>Hi,</p><p>The link below will take you to our cloud-based COVID-19 Health Assessment.</p><p>" + access_link + "</p><p>As a valued employee / volunteer we request that you click the link and complete the simple health assessment.  It will take just 30 seconds.  We hope that you are well, and this assessment can be repeated and updated at any time, if your health status changes.</p><p>The COVID-19 crisis is presenting a lot of challenges.  As your employer, we are using the Riskex Health Assessment module to keep a check on your health status.</p><p>Your contact details and the information you supply are protected by encryption and will not be used for any purpose other than to contact you during this crisis.</p><p>Please respect the lockdown conditions and take care of your health and wellbeing.</p><p>Best regards</p><p>&nbsp;</p>";

            //emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);

            // Send template email to admin
            emailSubject = "COVID-19 System Setup - Approval";
            emailTitle = "Health Assessment (COVID-19) <br/> System Awaiting Approval";
            emailBody = "<p>Heads up! we have a new sign up waiting for approval, please check the client list from the admin panel.</p>";

            emailSent = SendEmail("sales@riskex.co.uk", emailSubject, emailTitle, emailBody, "template", true);

            // Setup Admin Accounts Removed, now internal
            //setupAdminAccounts();


            Response.Redirect("approval.aspx");
            Response.End();

        } else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#valModal').modal('show');", true);
        }





    }





}

