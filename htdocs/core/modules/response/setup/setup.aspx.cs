﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Linq;
using Telerik.Windows.Documents.Spreadsheet.Model;

public partial class Setup : System.Web.UI.Page
{
    public string _accessLink = "";
    public string _accessSalt = "";
    public string accessCode;
    private static Random random = new Random();

    protected void Page_Load(object sender, EventArgs e)
    {

        Session.Timeout = 90;

        if (Session["SETUP_CONTINUE"] != null) {

            if (Session["SETUP_CONTINUE"].ToString() == "Y")
            {

                Response.Redirect("verify.aspx");
                Response.End();

            }

        }

        if (!IsPostBack)
        {
            if (setupStage1.Visible) { showIndustries(); }
        }

    }

    public string RandomCode(int _length, string _type)
    {

        string chars = "";

        if (_type == "alpha")
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        } else if (_type == "num")
        {
            chars = "0123456789";
        } else
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }

        return new string(Enumerable.Range(1, _length).Select(_ => chars[random.Next(chars.Length)]).ToArray());

    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static bool IsValidEmail(string email)
    {
        try
        {
            email = Regex.Replace(email, @"\s", "");

            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMSetup"]));
        connection.Open();

        return connection;
    }

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk", "Riskex Notification");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }

            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }

    public void createAccessCode(string _setup_reference, string _emailAddress)
    {

        // New unique ID with some salt
        string _accessCode;
        _accessCode = RandomCode(6,"");
        accessCode = _accessCode;

        // This needs to be a trigger at some point
        // Clean out any links currently in the system
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCleanCodes", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }

        // Register the new link
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCreateAccessCode", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setup_reference));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.Parameters.Add(new SqlParameter("@access_code", _accessCode));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }


    }

    public bool isValidClient(string _emailaddress)
    {

        // We need to see if the person is setting up this new licence with a domain already in our DB, needs to redirect.

        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            string isValidClient = "False";

            // Just get the domain
            string address = _emailaddress;
            string host;
            host = address.Split('@')[1];

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseCheckSetupDomain", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@EmailAddressDomain", host));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                isValidClient = objrs["valid_client"].ToString();
            }

            if (isValidClient.ToUpper() == "TRUE")
            {
                return true;
            } else
            {
                return false;
            }


            cmd.Dispose();
            objrs.Dispose();

        }


    }

    public bool DoEmailsMatch(string _email1,string _email2)
    {
        // Clean the data before compare

        string emailmatch1 = Regex.Replace(_email1, @"\s", "");
        string emailmatch2 = Regex.Replace(_email2, @"\s", "");

        bool result = emailmatch1.Equals(emailmatch2, StringComparison.OrdinalIgnoreCase);

        return result;

    }

    protected void showStage2()
    {
        // Ok we can continue
        setupStage1.Visible = false;
        setupStage2.Visible = true;

    }

    protected void showStage3()
    {
        // Ok we can continue
        setupStage1.Visible = false;
        setupStage2.Visible = false;
        setupStage3.Visible = true;


    }

    protected void checkTerms(object sender, EventArgs e)
    {
        if (yes.Checked) {termsAcceptance.Visible = false; termsConfirmation.Visible = true; }
    }

    protected void processStage1(string _setupCompanyName, string _setupCompanyAddress, string _setupCompanyPostCode, string _setupFirstName, string _setupSurname, string _setupJobTitle, string _setupEmail, string _setupPhone, string _setupEmployees, string _setupIndustries)
    {

        string _setupReference = Guid.NewGuid().ToString();
        // Want to put a check here also that checks to see if the reference is used

        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupRegisterCompany", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            cmd.Parameters.Add(new SqlParameter("@companyName", _setupCompanyName));
            cmd.Parameters.Add(new SqlParameter("@companyAddress", _setupCompanyAddress));
            cmd.Parameters.Add(new SqlParameter("@companyPostCode", _setupCompanyPostCode));
            cmd.Parameters.Add(new SqlParameter("@FirstName", _setupFirstName));
            cmd.Parameters.Add(new SqlParameter("@Surname", _setupSurname));
            cmd.Parameters.Add(new SqlParameter("@JobTitle", _setupJobTitle));
            cmd.Parameters.Add(new SqlParameter("@EmailAddress", _setupEmail));
            cmd.Parameters.Add(new SqlParameter("@PhoneNumber", _setupPhone));
            cmd.Parameters.Add(new SqlParameter("@Industry", _setupIndustries));
            cmd.Parameters.Add(new SqlParameter("@employees", _setupEmployees));
            cmd.Parameters.Add(new SqlParameter("@ipAddress", getIPAddress()));
            cmd.ExecuteNonQuery();

            // OK success, lets move on.  Set the setup reference
            setupReference.Value = _setupReference;
            setupTermsCompanyName.Text = _setupCompanyName;

            showStage2();

        }

    }

    protected void processStage2(string _setupEmail)
    {

        // We need to now add terms datatime date
        // Create the access links
        // Send out email

        string _setupReference = setupReference.Value;
        // Want to put a check here also that checks to see if the reference is used

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupConfirmRegistration", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            cmd.Parameters.Add(new SqlParameter("@ipAddress", getIPAddress()));
            cmd.ExecuteNonQuery();

        }

        createAccessCode(_setupReference, _setupEmail);

        string emailSubject;
        string emailTitle;
        string emailBody;
        string _accessLink;
        string _emailFirstName = setupFirstName.Text;

        verifyEmailAddress.Text = _setupEmail;

        _accessLink = Application["CONFIG_PROVIDER_SECURE"] + "/login/?t=SV&u=" + _setupReference + "&c=" + accessCode;
        test.Text = _accessLink;


        emailSubject = "Verify your email address - COVID Health Assessment Service";
        emailTitle = "Health Assessment ( COVID-19 )<br/>Verify Your Email";
        emailBody = "<p>Hi " + _emailFirstName + ",</p><p>Thank you for registering your organisation for AssessNET’s Health Assessment Tool.</p><p>Please click the link below to verify your email address and continue the setup process.</p><p><a href='" + _accessLink + "'>" + _accessLink + "</a></p><p> </p>";

        Boolean emailSent;
        emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);
        alertArea.Visible = true;

        showStage3();


    }

    protected void resendCode_Click(object sender, EventArgs e)
    {

        string _setupReference = setupReference.Value;

        // Create a new code
        accessCode = RandomCode(6,"");

        // Get the code and email from the current setup reference (just as a double check).
        using (SqlConnection dbConn = GetDatabaseConnection())
        {

            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupResendCode", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            cmd.Parameters.Add(new SqlParameter("@access_code", accessCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                string _setupEmail = objrs["emailAddress"].ToString();


                string emailSubject;
                string emailTitle;
                string emailBody;
                string _emailFirstName = setupFirstName.Text;

                _accessLink = Application["CONFIG_PROVIDER_SECURE"] + "/login/?t=SV&u=" + _setupReference + "&c=" + accessCode;
                test.Text = _accessLink;


                emailSubject = "Verify your email address - COVID Health Assessment Service";
                emailTitle = "Health Assessment ( COVID-19 )<br/>Verify Your Email";
                emailBody = "<p>Hi " + _emailFirstName + ",</p><p>Thank you for registering your organisation for AssessNET’s Health Assessment Tool.</p><p>Please click the link below to verify your email address and continue the setup process.</p><p><a href='" + _accessLink + "'>" + _accessLink + "</a></p><p> </p>";


                Boolean emailSent;
                emailSent = SendEmail(_setupEmail, emailSubject, emailTitle, emailBody, "template", true);
                alertArea.Visible = true;
                alertArea.Attributes.Add("class", "alert alert-success");
                alertAreaMsg.Text = "An access email has been sent, please check your junk folders and follow the instructions.";

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#codeResentModal').modal('show');", true);


            }

        }


    }

    protected bool checkValidationCode(string _code)
    {

        // This will also log the attempt
        string _setupReference = setupReference.Value;

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCheckCode", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            cmd.Parameters.Add(new SqlParameter("@ipAddress", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@code", _code));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return Convert.ToBoolean(objrs["valid"].ToString());

            }

        }

        return false;
    }

    protected void setupNewClient(string _setupReference)
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupNewClient", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                Session.Contents["CorpCode"] = objrs["corpCode"].ToString();
                Session.Contents["Email"] = objrs["Email"].ToString();

            }


        }

    }

    public void createCompanyAccessCode(string _corpCode)
    {

        // Create new access link and Unique Code
        string _verificationCode;
        string _codeP1,_codeP2,_codeP3;
        _codeP1 = RandomCode(3,"alpha");
        _codeP2 = RandomCode(3,"num");
        _codeP3 = RandomCode(3,"num");

        _verificationCode = _codeP1 + '-' + _codeP2 + '-' + _codeP3;

        // New unique ID with some salt
        _accessLink = Guid.NewGuid().ToString();
        _accessSalt = RandomCode(6,"num");
        _accessLink = _accessLink.Replace("-", "");

        // Register the new link
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCreateCompanyAccessCodes", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@verication_code", _verificationCode));
            cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _accessSalt));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }


    }

    public void showIndustries()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupShowIndustries", dbConn);
            objrs = cmd.ExecuteReader();

            setupCompanyIndustry.DataSource = objrs;
            setupCompanyIndustry.DataValueField = "id";
            setupCompanyIndustry.DataTextField = "ind_type";
            setupCompanyIndustry.DataBind();

            setupCompanyIndustry.Items.Insert(0, "-- Select Option --");
            setupCompanyIndustry.Items.Insert(setupCompanyIndustry.Items.Count, "Other");


            cmd.Dispose();

        }


    }

    // Form control methods

    protected void setupStage1_Click(object sender, EventArgs e)
    {

        string _setupCompanyName = setupCompanyName.Text;
        string _setupCompanyAddress = setupCompanyAddress.Text;
        string _setupCompanyPostCode = "XXXXXX";
        string _setupFirstName = setupFirstName.Text;
        string _setupSurname = setupSurname.Text;
        string _setupJobTitle = setupJobTitle.SelectedValue;
        string _setupEmail = setupEmail.Text;
        string _setupEmailConfirm = setupEmailConfirm.Text;
        string _setupPhone = setupPhone.Text;
        string _setupEmployees = setupCompanyEmployees.SelectedValue;
        string _setupIndustries = setupCompanyIndustry.SelectedValue;

       // if (setupJobTitle.SelectedValue == "Other") { _setupJobTitle = setupOtherJobTitle.Text; }

        bool _formValid = false;

        if (_setupCompanyName.Length > 2 &&
            _setupCompanyAddress.Length > 5 &&
            _setupCompanyPostCode.Length > 5 &&
            _setupFirstName.Length > 1 &&
            _setupSurname.Length > 1 &&
            _setupEmail.Length > 5 &&
            _setupPhone.Length > 6 &&
            _setupJobTitle != "NA" &&
            _setupJobTitle.Length > 0 &&
            setupCompanyEmployees.SelectedIndex != 0 &&
            setupCompanyIndustry.SelectedIndex != 0 &&
            IsValidEmail(_setupEmail) == true &&
            IsValidEmail(_setupEmailConfirm) == true &&
            DoEmailsMatch(_setupEmail, _setupEmailConfirm) == true )
        { _formValid = true; }


        // We need to check the main form is completed
        if (!_formValid)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#valModal').modal('show');", true);

        }
        else {

                // redirect if they are a client of AssessNET
                if (isValidClient(_setupEmail)) {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#clientModal').modal('show');", true);
                } else {
                // Ok we can process this registration to the next stage
                processStage1(_setupCompanyName,
                              _setupCompanyAddress,
                              _setupCompanyPostCode,
                              _setupFirstName,
                              _setupSurname,
                              _setupJobTitle,
                              _setupEmail,
                              _setupPhone,
                              _setupEmployees,
                              _setupIndustries);
                }



        }



    }

    protected void setupStage2_Click(object sender, EventArgs e)
    {
        bool _termsYes = yes.Checked;
        string _confirmEmail = setupEmailConfirm.Text;
        bool _formValid = false;

        if (_termsYes && _confirmEmail.Length > 3 && setupReference.Value.Length > 5) { _formValid = true; }


        // Stage needs to be completed
        if (!_formValid)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#valTermModal').modal('show');", true);

        } else
        {

            processStage2(_confirmEmail);

        }


    }

    protected void setupStage3_Click(object sender, EventArgs e)
    {
        // Ok we now need to check the validation code, if it works, then we are good to go.

        string _code = validationCode.Text;
        if (checkValidationCode(_code))
        {

            string _setupReference = setupReference.Value;
            // Create a corp code and main account
            setupNewClient(_setupReference);

            string _corpCode = Session["CorpCode"].ToString();
            // Create Access Codes for Client
            createCompanyAccessCode(_corpCode);

            // OK Lets Continue to the final setup, this is now valid.
            Session.Timeout = 60;
            Context.Session["setupReference"] = setupReference.Value;
            Response.Redirect("~/core/modules/response/setup/config.aspx");
            Response.End();

        }
        else {

            validationCode.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#codeWrongModal').modal('show');", true);

        }



    }

    protected void setupJobtitle_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (setupJobTitle.SelectedValue == "Other")
        {
            jobtitle_otherarea.Visible = true;

        } else
        {
            jobtitle_otherarea.Visible = false;
        }

        UpdatePanelSetup.Update();

    }


}

