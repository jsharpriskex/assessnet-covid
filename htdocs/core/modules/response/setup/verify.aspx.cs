﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class ReponseNoLink : System.Web.UI.Page
{
    public string _accessLink = "";
    public string _accessSalt = "";
    public string accessCode;
    private static Random random = new Random();

    public string _code;
    public string _setupReference;

    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Timeout = 90;

        Session["SETUP_CONTINUE"] = "N";

        _code = Session["setupCode"].ToString();
        _setupReference = Session["setupReference"].ToString();


        if (checkValidationCode())
        {

            // Create a corp code and main account
            setupNewClient(_setupReference);

            string _corpCode = Session["CorpCode"].ToString();
            // Create Access Codes for Client
            createCompanyAccessCode(_corpCode);

            // OK Lets Continue to the final setup, this is now valid.
            Session.Timeout = 60;
            Response.Redirect("~/core/modules/response/setup/config.aspx");
            Response.End();

        }


    }

    public string RandomCode(int _length, string _type)
    {

        string chars = "";

        if (_type == "alpha")
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }
        else if (_type == "num")
        {
            chars = "0123456789";
        }
        else
        {
            chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }

        return new string(Enumerable.Range(1, _length).Select(_ => chars[random.Next(chars.Length)]).ToArray());

    }

    protected void setupNewClient(string _setupReference)
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupNewClient", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                Session.Contents["CorpCode"] = objrs["corpCode"].ToString();
                Session.Contents["Email"] = objrs["Email"].ToString();


            }


        }

    }

    public void createCompanyAccessCode(string _corpCode)
    {

        // Create new access link and Unique Code
        string _verificationCode;
        string _codeP1, _codeP2, _codeP3;
        _codeP1 = RandomCode(3, "alpha");
        _codeP2 = RandomCode(3, "num");
        _codeP3 = RandomCode(3, "num");

        _verificationCode = _codeP1 + '-' + _codeP2 + '-' + _codeP3;

        // New unique ID with some salt
        _accessLink = Guid.NewGuid().ToString();
        _accessSalt = RandomCode(6, "num");
        _accessLink = _accessLink.Replace("-", "");

        // Register the new link
        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCreateCompanyAccessCodes", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@verication_code", _verificationCode));
            cmd.Parameters.Add(new SqlParameter("@access_link", _accessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _accessSalt));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }


    }

    protected bool checkValidationCode()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs = null;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupCheckCode", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@setup_reference", _setupReference));
            cmd.Parameters.Add(new SqlParameter("@ipAddress", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@code", _code));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();
                return Convert.ToBoolean(objrs["valid"].ToString());

            }

        }

        return false;
    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }


}

