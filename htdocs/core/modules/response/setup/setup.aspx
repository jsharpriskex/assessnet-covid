﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="setup.aspx.cs" Inherits="Setup" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Health Assessment ( COVID-19 ) - Setup</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

        <meta name="robots" content="noindex" />
        <meta property="og:title" content="AssessNET: COVID-19 Assessment Service" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://www.assessweb.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1600" />
        <meta property="og:image:height" content="900" />
        <meta property="og:url" content="https://www.assessweb.co.uk/" />
        <meta name="twitter:card" content="summary_large_image" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />


<!-- Google Tag Manager -->
<script>(function (w, d, s, l, i) {
    w[l] = w[l] || []; w[l].push({
        'gtm.start':
            new Date().getTime(), event: 'gtm.js'
    }); var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-N4B4S4T');</script>
<!-- End Google Tag Manager -->



    <style>

               html,body {
           margin:10px;
           padding:0px;

            background: url('background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

     .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

            .switch input {
                display: none;
            }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

            .slider:before {
                position: absolute;
                content: "";
                height: 26px;
                width: 26px;
                left: 4px;
                bottom: 4px;
                background-color: white;
                -webkit-transition: .4s;
                transition: .4s;
            }

        input:checked + .slider {
            content: "NO";
            background-color: #5cb85c;
        }

        input:focus + .slider {
            content: "NO";
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }


        .radioGroup {
        /*margin:4px;*/
        overflow: auto;
        /*display:inline-block;*/
        width:auto;
        }

        .parentRadioGroup {
            overflow:auto;
        }

        .radioGroup label {
            /*float:left;*/
            min-width:75px;
            /*margin:4px;*/
            background-color:white;
            border-radius:4px;
            border:1px solid #D0D0D0;
            overflow:auto;
            /*padding: 2px;*/


        }

        .radioGroup label span {
            text-align:center;
            font-size: 13px;
            padding:6px 12px;
            display:block;
            font-weight: 100;
        }

        .radioGroup label input {

            display:none;
        }

        /*.radioGroup input:checked + span {
            background-color:#5cb85c;
            color:#F7F7F7;
        }*/

        .radioGroup .green input:checked + span {
            background-color:#5cb85c;
            color:#F7F7F7;
        }

        .radioGroup .orange input:checked + span {
            background-color:#e27036;
            color:#F7F7F7;
        }

        .radioGroup .red input:checked + span {
            background-color:#d9534f;
            color:#F7F7F7;
        }

        .radioGroup .greyRB input:checked + span {
            background-color:#8c8b8b;
            color:#F7F7F7;
        }


        .radioGroup .* {
            background-color:white;
            color:black;
        }

         .btn-extramargin{
             margin:5px;
             min-width: 125px;
         }

         .controlRow{
             padding-bottom:20px;
             border: solid 1px #dddddd;
	        border-radius: 5px
         }

         .inputError{
             background-color:#fdc8c8 !important;
         }


         .comment{
             margin-top:10px;
         }

         .buttonGroup{
             /*border: solid 1px #dddddd;
             border-radius:3px;*/
             overflow: auto;
             float:right;

             -webkit-margin-before: 1.33em;
             padding:10px;

             margin-bottom:10px;
         }

         .buttonGroup > div > a{
             margin: 0 5px !important;
             width: 150px !important;
             text-align:center;
         }

         .buttonGroup > div > a:first-child{
             margin: 0 0px !important;
             width: 150px !important;
             text-align:center;
         }



         .inlineDropDown{
             display: inline-block;
            margin-bottom: 0px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            touch-action: manipulation;
            cursor: pointer;
            user-select: none;
            background-image: none;
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            border-image: initial;
            border-radius: 4px;
            color: #333;
            background-color: #fff;
            border-color: #ccc;
            height:34px;
            width:90px;
         }


          .lowRisk {
            background-color:#5cb85c;
            color:#F7F7F7;
        }

        .mediumRisk {
            background-color:#e27036;
            color:#F7F7F7;
        }

        .highRisk {
            background-color:#d9534f;
            color:#F7F7F7;
        }

        .naRisk {
            background-color:#8c8b8b;
            color:#F7F7F7;
        }

        .inlineButtonNotButton{
             display: inline-block;
            margin-bottom: 0px;
            font-size: 15px;
            font-weight: 600;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            user-select: none;
            background-image: none;
            padding: 6px 12px;
            border-width: 1px;
            border-style: solid;
            border-color: transparent;
            border-image: initial;
            border-radius: 4px;
            border-color: #ccc;
            height:34px;
            width:90px;
         }


        .riskSummaryCount{

            height:35px;
            border: 1px solid white;
            border-radius: 5px;
            font-size:20px;
            font-weight: 600;
            padding-top: 2px;
        }

        body p {

            font-size:16px;

        }


    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4B4S4T" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


        <!-- Validation Modal -->
        <div class="modal fade" id="valModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runRemoveModalLabel">Unable to continue</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        Nearly there! Before you can continue this registration, please ensure you have filled in <strong>ALL</strong> the details within the form correctly.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

       <!-- Validation Terms Modal -->
        <div class="modal fade" id="valTermModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runTermsModalLabel">Unable to continue</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        Nearly there! We are unable to create your account until you have agreed to the Terms of Service.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->


        <!-- Secure Info Modal -->
        <div class="modal fade" id="secureModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="secureModalLabel">Peace of mind - Security is in our DNA at Riskex</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12">


                        <div class="media">
                          <div class="media-left">


                                <a href="#">
                                  <img src="img/secure-ency-logo.jpg" />
                                </a>

                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">We ensure your data is safe, secure and handled correctly (GDPR).</h4>
                                <ul style="padding-top:10px;font-size:17px;">
                                    <li>Our service uses our core AssessNET platform, a cloud H&S service for 20 years, based around robust architecture and security measures.</li>
                                    <li>We are audited and certified by BSI to ISO 27001 Information Security Management, along with ISO 9001 in Quality Systems and Cyber Essentials.</li>
                                    <li>Our UK hosting provider Rackspace has ISO 27001 and PCI DSS, SOC 1, 2, 3 level security.</li>
                                    <li>Personal data for the COVID tool is totally encrypted, and the tool provides a secure connection at all times.</li>
                                    <li>We request minimal data from the user required for assessment and analytical purposes of COVID-19 at organisational level.</li>
                                    <li>Data from assessments and users is accessible by the organisation (you) by way of administrator permissions only.</li>
                                    <li>If a full deletion is requested, it will be done on request.  At the end of the scheme (31st October 2020) all data will be available to the organisation to export in full, prior to full deletion.</li>
                                    <li>Data is backed up to our UK Servers, held encrypted at all times and no backup data will be kept after the 31st October 2020, or if deletion is requested prior.</li>
                                    <li>Your data will not be shared with any third-party at any time.</li>
                                </ul>
                          </div>
                        </div>



                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->




        <!-- Resend Code Modal -->
        <div class="modal fade" id="codeResentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="resendCodeModalLabel">Validation Link Sent</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        A new validation link has been generated and sent to your email address.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Code Wrong Modal -->
        <div class="modal fade" id="codeWrongModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="wrongCodeModalLabel">Validation Error</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        The validation code you have entered is not correct, please check and try again.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Client Modal -->
        <div class="modal fade" id="clientModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runClientModalLabel">Already a client of AssessNET</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        We have be able to establish tha your organisation is already a registered user of AssessNET, you do not need to sign up.  You can visit the assessment page directly and being the process using your business email.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->


    <div class="container">
    <div class="row">

        <br />

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Health Assessment ( COVID-19 ) - FREE Licence Setup</h1>

            </div>
        </div>

        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">

                <asp:UpdatePanel ID="UpdatePanelSetup" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>

                <asp:HiddenField runat="server" ID="setupReference" />

                <!-- Stage 1 -->
                <div class="panel panel-default" id="setupStage1" runat="server" visible="true">

                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;"><i class="fa fa-bars" aria-hidden="true"></i> Registration</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>


                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">

                            <p><strong>Welcome to Riskex’s COVID-19 Health Assessment Tool for Companies and Organisations.</strong></p>
                            <p>This service is FREE OF CHARGE and is funded by Riskex and our sponsors to help protect the UK's workforce and volunteers.</p>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6">

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupCompanyName">Company Name</label>
                                            <asp:TextBox runat="server" ID="setupCompanyName" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupCompanyAddress">Company Postcode</label>
                                            <asp:TextBox runat="server" ID="setupCompanyAddress" class="form-control" placeholder="UK postcode only" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupCompanyIndustry">Industry Type</label>
                                            <asp:DropDownList runat="server" ID="setupCompanyIndustry" class="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupCompanyEmployees">Employee Range</label>
                                            <asp:DropDownList runat="server" ID="setupCompanyEmployees" class="form-control">
                                                  <asp:ListItem Value="NA" Text="-- Select Option --" />
                                                  <asp:ListItem Value="Less than 50" Text="Less than 50" />
                                                  <asp:ListItem Value="50 to 99" Text="50 to 99" />
                                                  <asp:ListItem Value="100 to 199" Text="100 to 199" />
                                                  <asp:ListItem Value="200 to 399" Text="200 to 399" />
                                                  <asp:ListItem Value="400 to 1000" Text="400 to 1000" />
                                                  <asp:ListItem Value="More than 1000" Text="More than 1000" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">


                        <div class="media" onclick="$('#secureModal').modal('show');" style="padding-top:10px;cursor:pointer;">
                          <div class="media-left">

                           <%--     <a href="#">
                                  <img src="secure-ency-logo.jpg" />
                                </a>--%>

                          </div>
                          <div class="media-body">
                            <h4 class="media-heading">Peace of mind</h4>
                            Click here to learn how we ensure your data is safe and secure.

                          </div>
                        </div>

                                    </div>
                                </div>


                        </div>
                        <div class="col-xs-6">

                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                        <label for="setupFirstName">Your First Name</label>
                                            <asp:TextBox runat="server" ID="setupFirstName" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                        <label for="setupSurname">Your Surname</label>
                                            <asp:TextBox runat="server" ID="setupSurname" class="form-control" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupJobTitle">Your Job Role</label>

                                            <asp:DropDownList runat="server" ID="setupJobTitle" AutoPostBack="True" class="form-control"   >
                                                <asp:ListItem Value="NA" Text="-- Select Role --" />
                                                <asp:ListItem Value="Health and Safety" Text="Health and Safety" />
                                                <asp:ListItem Value="Health and Wellbeing" Text="Health and Wellbeing" />
                                                <asp:ListItem Value="Human Resources" Text="Human Resources" />
                                                <asp:ListItem Value="Risk/Compliance" Text="Risk/Compliance" />
                                                <asp:ListItem Value="Senior Management" Text="Senior Management" />
                                                <asp:ListItem Value="Line Manager" Text="Line Manager" />
                                                <asp:ListItem Value="Other" Text="Other" />
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                </div>

                                <div class="row" runat="server" id="jobtitle_otherarea" visible="false">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupOtherJobTitle">Specify Job Title</label>

                                            <asp:TextBox runat="server" ID="setupOtherJobTitle" class="form-control" autocompletetype="Disabled"  />

                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupEmail">Your Business Email Address</label>
                                            <asp:TextBox runat="server" ID="setupEmail" class="form-control" autocompletetype="Disabled"  />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupEmailConfirm">Confirm Your Business Email Address</label>
                                            <asp:TextBox runat="server" ID="setupEmailConfirm" class="form-control" autocompletetype="Disabled" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                        <label for="setupNumber">Your Mobile Number</label>
                                            <asp:TextBox runat="server" ID="setupPhone" class="form-control" />
                                        </div>
                                    </div>
                                </div>


                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alertArea" visible="false">
                                <p><asp:Literal runat="server" ID="alertAreaMsg" /></p>
                            </div>
                        </div>
                    </div>


                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <a href="https://www.riskex.co.uk/" target="_parent" class="btn btn-danger">Cancel and Exit</a>
                            <asp:Button ID="buttonRegister" runat="server" class="btn btn-success"  Text="Continue" OnClick="setupStage1_Click"   />

                        </div>
                    </div>
                    </div>

                </div>
                <!-- End of Stage 1 -->


                <!-- Stage 2 -->
                <div class="panel panel-default" id="setupStage2" runat="server" visible="false">

                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;"><i class="fa fa-bars" aria-hidden="true"></i> Terms and Conditions</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-md-12">

                            <iframe id="inlineFrameExample" title="Inline Frame Example" width="100%" height="340" src="documents/termsCOVID.pdf"></iframe>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">

                            <asp:UpdatePanel ID="UpdatePanelTerms" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <div class="table" style="margin-top:5px;" runat="server" id="termsAcceptance">
                                <table class="table table-striped table-bordered table-liststyle01 searchResults">
                                <tbody>

                                    <tr>

                                        <td>I <strong>accept</strong> the terms of service for the FREE AssessNET License for the COVID-19 Health Assessment Tool<strong> <asp:Literal runat="server" Visible="false" ID="setupTermsCompanyName" /></strong></td>
                                        <td width="10px" id="terms_ans1" runat="server">
                                            <div class="parentRadioGroup text-center">
                                                <div class="radioGroup">
                                                    <label class="green white">
                                                    <asp:RadioButton runat="server" ID="yes" OnCheckedChanged="checkTerms" AutoPostBack="true" GroupName='terms' />
                                                    <span>Yes</span>

                                                </div>
                                            </div>
                                        </td>
                                        <td width="10px" id="terms_ans2" runat="server"  Visible="false">
                                            <div class="parentRadioGroup text-center" >
                                                <div class="radioGroup">
                                                    <label class="red white">
                                                    <asp:RadioButton runat="server" ID="no" OnCheckedChanged="checkTerms" AutoPostBack="true" GroupName='terms' />
                                                    <span>No</span>

                                                </div>
                                            </div>
                                        </td>



                                    </tr>

                                </tbody>
                                </table>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="alert alert-warning"  role="alert">
                                            <p><strong>PLEASE NOTE:</strong><br />The FREE license is offered to UK registered organisations until 30th October 2020, when the sponsorship programme with Rackspace will terminate.  Any organisation wishing to continue with the service after this date will need to contact Riskex prior to 30th September 2020 to arrange for your license to be transferred to our Client Servers.</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" runat="server" style="margin-top:5px;" id="termsConfirmation" visible="false">
                                    <div class="col-xs-12">
                                        <div class="alert alert-success" role="alert" runat="server" id="setupEmailConfirmAlert" visible="true">
                                            Thank you for accepting the Terms of Service. Please click <strong>Continue</strong> to conclude this sign-up and send out a verification email.
                                        </div>
                                    </div>
                                </div>

                            </ContentTemplate>
                            </asp:UpdatePanel>


                        </div>
                    </div>






                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <a href="https://www.riskex.co.uk/" target="_parent" class="btn btn-danger">Cancel and Exit</a>
                            <asp:Button ID="buttonTermsStage" runat="server" class="btn btn-success"  Text="Continue" OnClick="setupStage2_Click"   />

                        </div>
                    </div>
                    </div>

                </div>
                <!-- End of Stage 2 -->

                <!-- Stage 3 -->
                <div class="panel panel-default" id="setupStage3" runat="server" visible="false">

                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;"><i class="fa fa-bars" aria-hidden="true"></i> Validation</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center">

                              <div id="ConfirmMessage" runat="server" style="padding-top:25px;" visible="true">

                                  <h1>Check your Email - Verification link sent!</h1>
                                  <h2 style="color:#c00c00;"><asp:Literal runat="server" ID="verifyEmailAddress" /></h2>


                                    <div class="alert alert-success text-left" style="margin-top:30px;">

                                        <div class="media">
                                                <div class="media-left">
                                                    <a href="#">
                                                      <i class="fas fa-envelope-open-text fa-5x" style="color:#357234;padding-top:10px;"></i>
                                                    </a>
                                                  </div>
                                              <div class="media-body">
                                                <div class="col-md-12" style="font-size:17px;">

                                                <p>To help us verify your email address and continue the setup process; please click the link that has just been sent to you at <asp:Literal runat="server" ID="Literal1" />. Remember to check your Junk and Spam folders.</p>
                                                <p><strong>If you do not receive an email within the next few minutes, please click "Resend Link" below.</strong></p>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                              </div>


                        </div>
                    </div>

                    <div class="row" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:TextBox runat="server"  ID="validationCode" MaxLength="6" class="form-control codeInput" placeholder="Code" />
                            <p>&nbsp;</p>
                        </div>
                    </div>

                    <div class="row" runat="server" visible="false">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="alert alert-warning" role="alert">
                            <p>On entering your code, click <strong>"Continue"</strong> below to verify. Alternatively, if you have not received the email, or the code has expired, click on <strong>"Resend Code"</strong>.</p>

                            </div>
                        </div>
                    </div>


                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button ID="buttonResend" runat="server" class="btn btn-primary"  Text="Resend Link"  OnClick="resendCode_Click"  />
                            <asp:Button ID="buttonCloseFinish" runat="server" class="btn btn-success" Visible="false"  Text="Continue" OnClick="setupStage3_Click"   />

                        </div>
                    </div>
                    </div>



                </div>
                <!-- End of Stage 3 -->

                  <p><asp:Literal runat="server" ID="test" Visible="false"  /> </p>
                  <p><!-- <asp:Literal runat="server" ID="test2" /> --></p>

                </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="buttonResend" EventName="Click"  />
                        <asp:AsyncPostBackTrigger ControlID="buttonRegister" EventName="Click"  />
                        <asp:AsyncPostBackTrigger ControlID="buttonTermsStage" EventName="Click"  />
                        <asp:AsyncPostBackTrigger ControlID="buttonCloseFinish" EventName="Click"  />
                    </Triggers>
                </asp:UpdatePanel>

        <div class="row">
            <div class="col-xs-4">
                <img src="img/Fit2Work-300dpi-ReMake.jpg" alt="Riskex Fit 2 Work" />
            </div>
            <div class="col-xs-4" style="padding-top:7px;">
                <p style="font-size:14px;">Copyright &copy; 2020 Riskex Ltd. Hosting for this service has been provided by Rackspace as part of their COVID-19 support scheme.</p>
            </div>

            <div class="col-xs-4">
                <img src="img/bsi-CovidList.png" alt="BSI / UKAS Certified ISO Standards" onclick="$('#secureModal').modal('show');" />
            </div>

        </div>

        </div>
        </div>



    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
