﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Linq;
using ASNETNSP;


public partial class SetupConfirm : System.Web.UI.Page
{
    public string corpCode;

    protected void Page_Load(object sender, EventArgs e)
    {

        Session.Timeout = 90;

        corpCode = Session["corpCode"].ToString();
        loadClientConfig();

    }

    protected void loadClientConfig()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupGetCompanyAccessDetails", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();

            if (objrs.HasRows)
            {
                objrs.Read();

                companyURLaccess.Text = "https://covid.riskex.co.uk/?a=" + objrs["uniqueLink"].ToString() + objrs["uniqueSalt"].ToString();
                confCompanyName.Text = "<strong>" + objrs["setupCompanyName"].ToString() + "</strong>";


            }
        }


    }

    protected void buttonActiveSystem_Click(object sender, EventArgs e)
    {

        Session["SETUP_ADMIN"] = "Y";
        Session["CORP_CODE"] = corpCode;
        Session["YOUR_LANGUAGE"] = "en-gb";
        Session["YOUR_ACCESS"] = "0";

        Response.Redirect("../responsePrecheck.aspx");

}

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMSetup"]));
        connection.Open();

        return connection;
    }


}

