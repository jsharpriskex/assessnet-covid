<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"
           "http://www.w3.org/TR/xhtml1/DTD/xhtml1-Frameset.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Health Assessment ( COVID-19 ) - Request Access Menu</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="robots" content="noindex" />
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
        <meta property="og:title" content="AssessNET: Health Assessment ( COVID-19 )" />
        <meta property="og:description" content="Class-Leading Cloud Based Health and Safety Management Platform" />
        <meta property="og:image" content="https://www.assessweb.co.uk/version3.2/security/login/img/assessnet-social.png" />
        <meta property="og:image:width" content="1600" />
        <meta property="og:image:height" content="900" />
        <meta property="og:url" content="https://portal.assessweb.co.uk/" />
        <meta name="twitter:card" content="summary_large_image" />

         <!--  Non-Essential, But Recommended -->

        <meta property="og:site_name" content="Riskex Limted - AssessNET">
        <meta name="twitter:image:alt" content="Class-Leading Cloud Based Health and Safety Management Platform">


        <!--  Non-Essential, But Required for Analytics -->

        <meta name="twitter:site" content="@AssessNET">


</head>

<frameset rows="*">
    <frame frameborder='0' name='main' scrolling='yes' noresize='noresize' src='setup.aspx' />
</frameset>

</html>
