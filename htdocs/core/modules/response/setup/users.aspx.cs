﻿using System;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Linq;
using ASNETNSP;


public partial class SetupUsers : System.Web.UI.Page
{
    public string corpCode;

    protected void Page_Load(object sender, EventArgs e)
    {

        corpCode = Session["corpCode"].ToString();
        showUsers();

    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMSetup"]));
        connection.Open();

        return connection;
    }

    public static bool SendEmail(string emailAddress, string emailSubject, string emailTitle, string emailBody, string emailTemplate, Boolean html)
    {

        // This will send back true or false based on success of email.
        // It must be run in such a way its waiting for a return value, not as a void.
        // emailTemplate will switch designs (not implimented as only 1 design currently) -- future proof

        try
        {

            // Server config
            string smtpAddress = "riskex-co-uk.mail.protection.outlook.com";
            int portNumber = 587;

            // Credentials
            string from = "notifications@riskexltd.onmicrosoft.com";
            string password = "rX46812@";

            string template01 = "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>" +
                                "<html xmlns='http://www.w3.org/1999/xhtml' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:v='urn:schemas-microsoft-com:vml'>" +
                                "<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge'><meta name='viewport' content='width=device-width, initial-scale=1'>" +
                                "<meta name='apple-mobile-web-app-capable' content='yes'><meta name='apple-mobile-web-app-status-bar-style' content='black'><meta name='format-detection' content='telephone=no'>" +
                                "<style type='text/css'>html { -webkit-font-smoothing: antialiased; }body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}img{-ms-interpolation-mode:bicubic;}body {background-color: #e0e0e0 !important;font-family: Arial,Helvetica,sans-serif;color: #333333;font-size: 14px;font-weight: normal;}a {color: #d60808; text-decoration: none;}</style>" +
                                "<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]--></head>" +
                                "<body bgcolor='#ffffff' style='margin: 0px; padding:0px; -webkit-text-size-adjust:none;' yahoo='fix'><br/>" +
                                "<table align='center' bgcolor='#ffffff' border='0' cellpadding='15' cellspacing='0' style='margin: 10px; width:654px; background-color:#ffffff;' width='654'>" +
                                "    <tbody>" +
                                "        <tr><td style='background-color:#ff4747; border-bottom:solid 10px #d60808; padding-right:15px; text-align:left; height:60px' valign='top' align='right'> " +
                                "        <P style='margin-top:17px; padding-bottom:0px !important; line-height: normal; margin-bottom:0px !important; font-size:50px; font-weight:bold; color:#ffffff;'>RISKEX</P>" +
                                "        <P style='margin-top:0px; padding-top:0px; font-size:18px; color:#ffffff;'>delivering AssessNET</P>" +
                                "        </td></tr>" +
                                "        <tr>" +
                                "           <td style='padding-bottom:25px;' valign='top'>" +
                                "               <h1 style='text-align:center'>" + emailTitle + "</h1>" +
                                "               <p>" + emailBody + "</p>" +
                                "           </td>" +
                                "        </tr>" +
                                "        <tr>" +
                                "        <td style='background-color:#a5a0a0; border-top:solid 5px #939292; text-align:right;'>" +
                                "        <p style='text-align:left; margin-top:5px; font-size: 12px; color:#efefef;'><strong>Notice:</strong> If you are not the intended recipient of this email then please contact your Health and Safety team who will be able to remove you from this service.&nbsp;&nbsp;This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed.</p>" +
                                "        <span style='font-size: 12px; color:#efefef;'> &copy; Copyright Riskex Ltd " + DateTime.Today.Year.ToString() + "</span>" +
                                "</td></tr></tbody></table></body></html>";


            // Email data
            string to = emailAddress;
            string subject = emailSubject;
            string body = template01;


            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress("notifications@assessnet.co.uk");
                mail.To.Add(to);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = html;

                using (SmtpClient smtp = new SmtpClient(smtpAddress))
                {
                    smtp.EnableSsl = true;
                    smtp.Timeout = 5000;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(from, password);
                    smtp.Send(mail);
                }

            }

            // Emailed sent
            return true;

        }
        catch
        {

            // Email failed
            return false;

        }


    }

    public void tableContentUsers_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

        string _acc_ref = Convert.ToString(e.CommandArgument);

        switch (e.CommandName)
        {
            case "ItemRemove":
                removeUser(_acc_ref);
                break;

        }

    }

    protected void addNewAdminAccount(string _password)
    {

        string _accRef = Guid.NewGuid().ToString();
        string _salt = ASNETNSP.Authentication.setSalt(512);
        string _firstname = usr_fname.Text;
        string _lastname = usr_lname.Text;
        string _phone = usr_phone.Text;
        string _role = usr_role.SelectedValue;
        string _email = usr_email.Text;
        string _isValid = "NO";

        string _passwordHash = ASNETNSP.Authentication.setSHA512Hash(_password, _salt);


        if (_email != Session["Email"].ToString()) {

            // Set the password and create the admin account whilst there.
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {

                SqlDataReader objrs;
                SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupAddManager", dbConn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@password", _passwordHash));
                cmd.Parameters.Add(new SqlParameter("@salt", _salt));
                cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
                cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
                cmd.Parameters.Add(new SqlParameter("@corp_email", _email));
                cmd.Parameters.Add(new SqlParameter("@fname", _firstname));
                cmd.Parameters.Add(new SqlParameter("@sname", _lastname));
                cmd.Parameters.Add(new SqlParameter("@corp_jobtitle", _role));
                cmd.Parameters.Add(new SqlParameter("@corp_phone", _phone));
                objrs = cmd.ExecuteReader();

                if (objrs.HasRows)
                {
                    objrs.Read();
                    _isValid = objrs["isValid"].ToString();
                }

            }

        }

        if (_isValid == "YES") {
            createAdminAccessLink(_accRef, corpCode, _email);
            clearForm();
            showUsers();
            UpdatePanelUsers.Update();
        } else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#notValidModal').modal('show');", true);
        }


    }

    public int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public void createAdminAccessLink(string _acc_ref, string _corp_code, string _emailAddress)
    {

        // New unique ID with some salt
        string _adminAccessLink = Guid.NewGuid().ToString();
        string _adminAccessSalt = RandomNumber(100000, 999999).ToString();
        _adminAccessLink = _adminAccessLink.Replace("-", "");

        using (SqlConnection dbConn = GetDatabaseConnection())
        {


            SqlCommand cmd = new SqlCommand("Module_HA.dbo.adminCreateAccessLink", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _acc_ref));
            cmd.Parameters.Add(new SqlParameter("@emailaddress", _emailAddress));
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corp_code));
            cmd.Parameters.Add(new SqlParameter("@ip_address", getIPAddress()));
            cmd.Parameters.Add(new SqlParameter("@access_link", _adminAccessLink));
            cmd.Parameters.Add(new SqlParameter("@access_salt", _adminAccessSalt));
            cmd.ExecuteNonQuery();

            cmd.Dispose();

        }


    }

    protected void clearForm()
    {

        usr_fname.Text = "";
        usr_lname.Text = "";
        usr_phone.Text = "";
        usr_role.SelectedIndex = 0;
        usr_email.Text = "";

    }

    public static bool IsValidEmail(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            return false;
        }
    }

    protected void addUser_Click(object sender, EventArgs e)
    {

        string _firstname = usr_fname.Text;
        string _lastname = usr_lname.Text;
        string _phone = usr_phone.Text;
        string _role = usr_role.SelectedValue;
        string _email = usr_email.Text;
        bool formValid = false;

        if (_firstname.Length > 1 && _lastname.Length > 1 && _phone.Length > 6 && _email.Length > 5 && _role.Length > 2 && IsValidEmail(_email)) { formValid = true; }

        if (formValid)
        {

            addNewAdminAccount("wk38fkWDk2dkd@@wdi2");

        } else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#valModal').modal('show');", true);
        }


    }

    protected void removeUser(string _accRef)
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupRemoveUser", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _accRef));
            cmd.ExecuteNonQuery();

            showUsers();
            UpdatePanelUsers.Update();

        }

    }

    protected void showUsers()
    {

        using (SqlConnection dbConn = GetDatabaseConnection())
        {
            SqlDataReader objrs;
            SqlCommand cmd = new SqlCommand("Module_HA.dbo.setupShowAdminUsers", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", corpCode));
            objrs = cmd.ExecuteReader();


            tableContentUsers.DataSource = objrs;
            tableContentUsers.DataBind();

        }

    }

    protected void continueWithoutUsers_Click(object sender, EventArgs e)
    {
        Response.Redirect("config.aspx");
        Response.End();
    }

    protected void buttonGoToConfig_Click(object sender, EventArgs e)
    {

        if (tableContentUsers.Items.Count > 0)
        {

            Response.Redirect("config.aspx");
            Response.End();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "$('#noticeModal').modal('show');", true);
        }
    }

}

