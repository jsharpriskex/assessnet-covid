﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="config.aspx.cs" Inherits="SetupConfig"  %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Health Assessment ( COVID-19 ) - Setup</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>


    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

    <script type="text/javascript">

        function ClientSideClick(myButton) {
            // Client side validation
            if (typeof (Page_ClientValidate) == 'function') {
                if (Page_ClientValidate() == false) { return false; }
            }

            //make sure the button is not of type "submit" but "button"
            if (myButton.getAttribute('type') == 'button') {
                // disable the button
                myButton.disabled = true;
                myButton.className = "btn btn-default";
                myButton.value = "processing...";
            }
            return true;
        }

    </script>


    <style>

       body p {

            font-size:16px;

        }

              html,body {
           margin:10px;
           padding:0px;

            background: url('background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


        <!-- Validation Modal -->
        <div class="modal fade" id="valModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runRemoveModalLabel">Unable to continue</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        Nearly there! Before you can complete the setup, please ensure you have added some locations and set your password for your admin account (Step 1 and 2).
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->



    <div class="container">
    <div class="row">

        <br />

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Health Assessment ( COVID-19 ) - New Licence Setup</h1>

            </div>
        </div>

        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">

                <asp:UpdatePanel ID="UpdatePanelSetup" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                <ContentTemplate>

                <asp:HiddenField runat="server" ID="hidFirstName" />

                <!-- Stage 3 -->
                <div class="panel panel-default" id="setupFinal" runat="server" visible="true">

                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;"><i class="fa fa-bars" aria-hidden="true"></i> Final Configuration</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>

                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">

                            <p>Your system is now almost ready.  We just need a little more information so that users can start to process their assessments.</p>

                                          <asp:UpdatePanel ID="UDP_Locations" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                          <ContentTemplate>


                                          <div class="panel panel-default" id="panel_locations" runat="server" visible="true">
                                          <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Step 1 of 2 - Add Business Locations <span runat="server" id="locLabel_attention" class="label label-danger pull-right">Needs Attention</span></h3>
                                          </div>
                                          <div class="panel-body">

                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            <style>
                                                                .bizLocation {
                                                                    padding-bottom:8px;
                                                                    padding-top:8px;
                                                                    margin-top:5px;
                                                                    margin-bottom:5px;
                                                                }


                                                            </style>

                                                        <asp:ListView runat="server" ID="locationList" OnItemCommand="locationList_ItemCommand">

                                                            <ItemTemplate>

                                                                <div class="alert alert-success bizLocation">
                                                                <i class="fa fa-building-o" aria-hidden="true"></i> <%#Eval("struct_name") %> <div runat="server" id="locRemove" class="pull-right"><asp:Button runat="server" CssClass="btn btn-sm btn-danger pull-right" style="padding-top:2px;padding-bottom:2px;" ID="locRemoveBut" Text="REMOVE" CommandName="locRemove" CommandArgument='<%# Eval("tier2_ref").ToString() %>' /></div>
                                                                </div>


                                                            </ItemTemplate>
                                                            <EmptyDataTemplate>

                                                                <div class="alert alert-danger ">
                                                                <i class="fa fa-ban" aria-hidden="true"></i> No locations currently configured, add one or more.
                                                                </div>

                                                            </EmptyDataTemplate>

                                                        </asp:ListView>


                                                        </div>
                                                        <div class="col-md-6">

                                                            <div class="alert alert-warning" style="margin-bottom:5px;">
                                                              <asp:TextBox runat="server" ID="addLocationText" class="form-control" MaxLength="50"  />
                                                              <asp:Button runat="server" style="margin-top:5px;" ID="addLocation" class="btn btn-block btn-warning" Text="Add Location" OnClick="addLocation_Click" />
                                                            </div>

                                                        </div>
                                                    </div>

                                            </div>
                                            </div>

                                            </ContentTemplate>
                                                  <Triggers>
                                                      <asp:AsyncPostBackTrigger ControlID="addLocation" EventName="Click" />
                                                  </Triggers>
                                            </asp:UpdatePanel>



                                            <asp:UpdatePanel ID="UDP_Password" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
                                            <ContentTemplate>

                                            <div class="panel panel-default" id="panel_password" runat="server" visible="true">
                                            <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Step 2 of 2 - Set your password  <span class="label label-danger pull-right" runat="server" id="pwLabel_attention">Needs Attention</span></h3>
                                            </div>
                                            <div class="panel-body">

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="col-xs-4">
                                                                    <div class="form-group">
                                                                    <label for="setupCompanyName">Your Email Address (Username)</label>
                                                                        <asp:Textbox runat="server" ID="userEmail" class="form-control" Text="" ReadOnly="true" />
                                                                    </div>
                                                                </div>

                                                                <div id="passwordArea" runat="server">
                                                                    <div class="col-xs-3">
                                                                            <div class="form-group">
                                                                            <label for="setupCompanyName">Set Password</label>
                                                                                <asp:TextBox runat="server" ID="passW1" TextMode="password" MaxLength="15" class="form-control" />
                                                                            </div>
                                                                    </div>

                                                                    <div class="col-xs-3">
                                                                            <div class="form-group">
                                                                            <label for="setupCompanyName">Confirm Password</label>
                                                                                <asp:TextBox runat="server" ID="passW2" TextMode="password" MaxLength="15" class="form-control" />
                                                                            </div>
                                                                     </div>


                                                                    <div class="col-xs-2">
                                                                            <div class="form-group">
                                                                            <asp:Button runat="server" ID="savePassword" Class="btn btn-block btn-success" Text="SAVE" OnClick="savePassword_Click" />
                                                                            </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-8" runat="server" id="passwordAreaAlert" visible="false">

                                                                    <div class="alert alert-success" style="padding-top:20px;padding-bottom:20px;margin-bottom:0px;margin-left:15px;margin-right:12px;" >
                                                                        <strong>Success: </strong>Password has been set and assigned to your account. Please keep it safe.
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="row" runat="server" id="pwAlertArea">
                                                            <div class="alert alert-success" runat="server" id="pwAlert" style="margin-bottom:0px;margin-left:15px;margin-right:12px;" >
                                                               <asp:Literal runat="server" ID="pwMsg">Your password should be at least 8 characters long and include both lower- and UPPER-case letters and at least one number.</asp:Literal>
                                                            </div>
                                                        </div>


                                            </div>
                                            </div>

                                            </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="savePassword" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel>



                                            <div class="panel panel-default" id="panel_config" runat="server" visible="false">
                                            <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Step 3 of 3 - Employee Access Details</h3>
                                            </div>
                                            <div class="panel-body">


                                                <style>

                                                    .CompanyURL {
                                                        font-size: 23px;
                                                    }

                                                    .validationCode {
                                                        font-size: 45px;
                                                        text-align:center;
                                                        font-weight:bold;
                                                    }

                                                </style>

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <p>Your unique link to send to any employees that will need to undertake an assessment, they will only need this to register.  From that point on they will be able to access the system using just their email, no link required.</p>

                                                                <div class="alert alert-success CompanyURL">

                                                                  <asp:Literal runat="server" ID="companyURLaccess" />

                                                                </div>

                                                            </div>
                                                        </div>

                                            </div>
                                            </div>





                        </div>
                    </div>


                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button ID="buttonCloseFinish" runat="server" class="btn btn-success"   Text="Finish and Send Email Confirmation" OnClick="closeFinish_Click"    />

                        </div>
                    </div>
                    </div>


                </div>
                <!-- End of Stage 3 -->

                  <p><asp:Literal runat="server" ID="test" /></p>
                  <p><asp:Literal runat="server" ID="test2" /></p>

                </ContentTemplate>
                    <Triggers>

                    </Triggers>
                </asp:UpdatePanel>

        <div class="row">
            <div class="col-xs-4">
                <img src="img/Fit2Work-300dpi-ReMake.jpg" alt="Riskex Fit 2 Work" />
            </div>
            <div class="col-xs-4" style="padding-top:7px;">
                <p style="font-size:14px;">Copyright &copy; 2020 Riskex Ltd. Hosting for this service has been provided by Rackspace as part of their COVID-19 support scheme.</p>
            </div>

            <div class="col-xs-4">
                <img src="img/bsi-CovidList.png" alt="BSI / UKAS Certified ISO Standards" onclick="$('#secureModal').modal('show');" />
            </div>

        </div>

        </div>
        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
