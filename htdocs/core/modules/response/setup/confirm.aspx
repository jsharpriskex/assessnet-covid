﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="confirm.aspx.cs" Inherits="SetupConfirm" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Health Assessment ( COVID-19 ) - Setup</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ea4b370d716680012d495b6&product=inline-share-buttons' async='async'></script>

    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

    <style>

      body p {

            font-size:17px;

        }

      .CompanyURL {
            font-size: 23px;
        }

      .table th, td {

            font-size:20px;
            font-weight:normal;
            padding-left:20px !important;
            padding-right:20px !important;
            vertical-align:middle;

        }

             html,body {
           margin:10px;
           padding:0px;

            background: url('background-covid-v3.jpg') no-repeat center center fixed;
            webkit-background-size: cover; /* For WebKit*/
            moz-background-size: cover;    /* Mozilla*/
            o-background-size: cover;      /* Opera*/
            background-size: cover;         /* Generic*/


       }

    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


    <div class="container">
    <div class="row">

        <br />

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Your Health Assessment Tool is now ready for use</h1>

            </div>
        </div>

        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">


                <!-- Stage Confirm -->
                <div class="panel panel-default" id="setupFinal" runat="server" visible="true">

                  <div class="panel-heading">
                    <h3 class="panel-title" style="font-size:17px;"><i class="fa fa-bars" aria-hidden="true"></i> Setup Complete</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="https://www.riskex.co.uk" target="_blank"><img src="Combine-CREATORS-OfAssess-300x78.png" /></a>
                        </div>
                        <div class="col-xs-9" style="padding-top:5px;">
                            <div class="sharethis-inline-follow-buttons"></div>
                        </div>
                    </div>


                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">

                            <p>We’ve sent you an email confirming all the details, and a reminder of the links you will need to use this service in future. </p>
                            <p><strong>What you need to do:</strong></p>
                            <p>Copy and email the link below, which is unique to <asp:Literal runat="server" ID="confCompanyName" Text="your organisation" />, and send it to your employees. They can then register and complete assessments as necessary.</p>

                            <div class="row">
                                <div class="col-md-12">

                                            <div class="panel panel-default" id="panel_config" runat="server" visible="true">
                                            <div class="panel-heading">
                                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i>Employee Access Link</h3>
                                            </div>
                                            <div class="panel-body">

                                                        <div class="row">
                                                            <div class="col-md-12">

                                                                <div class="alert alert-success CompanyURL">

                                                                  <asp:Literal runat="server" ID="companyURLaccess" />

                                                                </div>

                                                            </div>

                                                        </div>

                                            </div>
                                            </div>


                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th class="warning" width="350px" style="font-size:23px;padding-top:15px;">
                                                    https://covid.riskex.co.uk/admin/

                                                    </th>
                                                    <td>
                                                        This is the address you will need to access the admin system, view assessments and dashboard statistics later.

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>



                               </div>
                            </div>



                        </div>
                    </div>


                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button runat="server" ID="buttonActiveSystem" class="btn btn-lg btn-success" Text="Activate your system" OnClick="buttonActiveSystem_Click" />

                        </div>
                    </div>
                    </div>



                </div>
                <!-- End of Stage Confirm -->

        <div class="row">
            <div class="col-xs-4">
                <img src="img/Fit2Work-300dpi-ReMake.jpg" alt="Riskex Fit 2 Work" />
            </div>
            <div class="col-xs-4" style="padding-top:7px;">
                <p style="font-size:14px;">Copyright &copy; 2020 Riskex Ltd. Hosting for this service has been provided by Rackspace as part of their COVID-19 support scheme.</p>
            </div>

            <div class="col-xs-4">
                <img src="img/bsi-CovidList.png" alt="BSI / UKAS Certified ISO Standards" onclick="$('#secureModal').modal('show');" />
            </div>

        </div>



        </div>
        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
