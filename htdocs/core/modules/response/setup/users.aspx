﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="users.aspx.cs" Inherits="SetupUsers" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8" />

    <title>Health Assessment ( COVID-19 ) - Setup</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/5.0.0/normalize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7f83c477f2.js" crossorigin="anonymous"></script>


    <link href="../../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />

    <script type="text/javascript">

        function ClientSideClick(myButton) {
            // Client side validation
            if (typeof (Page_ClientValidate) == 'function') {
                if (Page_ClientValidate() == false) { return false; }
            }

            //make sure the button is not of type "submit" but "button"
            if (myButton.getAttribute('type') == 'button') {
                // disable the button
                myButton.disabled = true;
                myButton.className = "btn btn-default";
                myButton.value = "processing...";
            }
            return true;
        }

    </script>


    <style>

       body p {

            font-size:16px;

        }


    </style>

</head>
<body runat="server" id="pageBody" style="padding-top:10px;">
    <form id="pageform" runat="server" action="#">

    <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
    </asp:ScriptManager>


        <!-- Validation Modal -->
        <div class="modal fade" id="notValidModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runnotValidModalLabel">Unable to add user</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        The email address you have entered is already registered on our system, we are unable to add them as an additional dashboard user.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Validation Modal -->
        <div class="modal fade" id="valModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runRemoveModalLabel">Unable to add user</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        Please ensure you fill out all the required fields and that the users email address is valid.
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- No Users Modal -->
        <div class="modal fade" id="noticeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
            <div class="modal-content">


                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="runNoiceModalLabel">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                    <div class="col-xs-12 text-left">
                        You have not added any other contacts in your organisation to your dashboard, only you will have access to the reports and real-time reporting data.
                        <p>&nbsp;</p>
                        <p class="text-center"><strong>Are you sure you want to contnue?</strong></p>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12 text-center">
                    <button type="button" class="btn btn-success" data-dismiss="modal">NO - Remain and add users</button>
                    <asp:Button runat="server" class="btn btn-danger" ID="continueWithoutUsers" OnClick="continueWithoutUsers_Click" Text="YES - Continue without users" />
                    </div>
                </div>
                </div>


            </div>
            </div>
        </div>
        <!-- End of Modal -->


    <div class="container">
    <div class="row">

        <br />

        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 text-center">

                    <h1>Health Assessment ( COVID-19 ) - New Licence Setup</h1>

            </div>
        </div>

        <br />

        <div runat="server" id="section_1" class="row">
            <div class="col-xs-10 col-xs-offset-1">



                <!-- Stage Users -->
                <div class="panel panel-default" id="setupFinal" runat="server" visible="true">

                  <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Company Key Contacts</h3>
                  </div>
                  <div class="panel-body">
                  <!-- Body Start -->

                    <div class="row">
                        <div class="col-md-12">

                            <h1>Key Contacts - Dashboard Users</h1>
                            <p>Before we finalise the configuration of your setup and provide you with your own dashboard access, we need you to define other individuals in your organisation who will require assess to the assessment reports and statistical analysis.</p>
                            <p><strong>Please provide details of key individuals who will require dashboard access:  For example, Head of HR, Head of Safety etc</strong></p>

                            <p>&nbsp;</p>

                            <asp:UpdatePanel ID="UpdatePanelAddUser" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">

                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                <label for="setupCompanyName">First Name</label>
                                                    <asp:TextBox runat="server" ID="usr_fname" MaxLength="20" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                <label for="setupCompanyName">Last Name</label>
                                                    <asp:TextBox runat="server" ID="usr_lname" MaxLength="20" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-group">
                                                <label for="setupCompanyName">Mobile</label>
                                                    <asp:TextBox runat="server" ID="usr_phone"  MaxLength="15" class="form-control" />
                                                </div>
                                            </div>

                                    </div>
                                    <div class="row">

                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                <label for="setupCompanyName">Email Address</label>
                                                    <asp:TextBox runat="server" ID="usr_email" MaxLength="40" class="form-control" />
                                                </div>
                                            </div>

                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                <label for="setupCompanyName">Role</label>

                                                    <asp:DropDownList runat="server" ID="usr_role" class="form-control" >
                                                        <asp:ListItem Value="NA" Text="-- Select Role --" />
                                                        <asp:ListItem Value="Administrator" Text="Administrator" />
                                                        <asp:ListItem Value="Company Director" Text="Company Director" />
                                                        <asp:ListItem Value="Executive Manager" Text="Executive Manager" />
                                                        <asp:ListItem Value="Health and Safety Manager" Text="Health and Safety Manager" />
                                                        <asp:ListItem Value="Health and Safety Officer" Text="Health and Safety Officer" />
                                                        <asp:ListItem Value="HR Manager" Text="HR Manager" />
                                                        <asp:ListItem Value="Line Manager" Text="Line Manager" />
                                                        <asp:ListItem Value="Occupational Health" Text="Occupational Health" />
                                                        <asp:ListItem Value="Other" Text="Other" />
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <asp:Button runat="server" ID="addUser" Style="margin-top:45px;" CssClass="btn btn-lg btn-success" Text="Add User" OnClick="addUser_Click" />
                                </div>
                            </div>

                            </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="addUser" />
                                </Triggers>
                            </asp:UpdatePanel>



                            <asp:UpdatePanel ID="UpdatePanelUsers" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                            <div class="panel panel-default" id="panelusers" runat="server" visible="true">
                            <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bars" aria-hidden="true"></i> Dashboard Users</h3>
                            </div>
                            <div class="panel-body">


                             <asp:ListView ID="tableContentUsers" runat="server" ItemPlaceholderID="itemPlaceHolder1" OnItemCommand="tableContentUsers_ItemCommand" >
                                <LayoutTemplate>

                                    <div class="table" style="padding:0px;margin:0px;">
                                        <table class="table table-striped table-bordered table-liststyle01 searchResults" style="padding:0px;margin:0px;">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th width="270px">Role</th>
                                                    <th width="290px">Email</th>
                                                    <th width="130px">Phone</th>
                                                    <th width="50px" class="text-center"><i class="fas fa-cogs"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                    <asp:PlaceHolder runat="server" ID="itemPlaceHolder1"></asp:PlaceHolder>
                                            </tbody>
                                        </table>
                                    </div>

                                </LayoutTemplate>
                                <ItemTemplate>

                                                <tr>
                                                    <td><%# Eval("per_fname").ToString() %> <%# Eval("per_sname").ToString() %></td>
                                                    <td><%# Eval("Per_job_title").ToString() %></td>
                                                    <td><%# Eval("corp_email").ToString() %></td>
                                                    <td class="text-center"><%# Eval("corp_phone").ToString() %></td>
                                                    <td class="text-center"><asp:Linkbutton runat="server" CssClass="btn btn-danger" ID="removeUser" CommandName="ItemRemove" CommandArgument='<%# Eval("acc_ref").ToString() %>'><i class="fas fa-trash-alt"></i></asp:Linkbutton></td>
                                                </tr>

                                </ItemTemplate>
                                <EmptyDataTemplate>

                                    <div class="alert alert-danger" style="margin-bottom:0px;margin-top:10px;font-size:16px;">
                                        <p><strong>Notice</strong>: You currenty have no additional dashboard users added.</p>
                                    </div>

                                </EmptyDataTemplate>
                                </asp:ListView>



                            </div>
                            </div>

                            </ContentTemplate>
                            </asp:UpdatePanel>

                            <p><strong>What happens next?</strong> On completion of this setup, the people listed above will be sent an email with instructions on how to set a password and access the dashboard. You will set your own password during the next and final stage.</p>

                        </div>
                    </div>


                    <!-- Body End -->
                    </div>

                    <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-12 text-center">

                            <asp:Button ID="buttonGoToConfig" runat="server" OnClick="buttonGoToConfig_Click" onclientclick="ClientSideClick(this)"  class="btn btn-success" UseSubmitBehavior="False"  Text="Continue Setup"    />

                        </div>
                    </div>
                    </div>


                </div>
                <!-- End of Stage 3 -->

                  <p><asp:Literal runat="server" ID="test" /></p>
                  <p><asp:Literal runat="server" ID="test2" /></p>


        <table cellpadding="0" cellspacing="0" border="0" class='nostyle noborder' width="100%">
        <tr>
            <td align='right'><img style="padding-top:15px;" src="img/riskex-logo-2012.png" alt="Riskex" /></td><td align='right'><img src="img/bsi-logos.png" alt="BSI / UKAS Certified ISO Standards" /></td>
        </tr>
        </table>

        </div>


    </div> <!-- Main Row End -->
    </div> <!-- Container End -->

    </form>
    <br />
    <br />

    <script type="text/javascript">


        $(document).ready(function () {

            BindControlEvents();

        });

        function BindControlEvents() {

        }

        //Re-bind for callbacks
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            BindControlEvents();
        });


    </script>


</body>
</html>
