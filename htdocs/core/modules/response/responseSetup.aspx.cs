﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Net;
using System.Net.Mail;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using ASNETNSP;
using Microsoft.VisualBasic;

public partial class ResponseSetup : System.Web.UI.Page
{
    public string corpCode;
    public string needSetup;
    public string yourAccRef;
    public string yourEmail;
    public string hasPassword;

    public string currentUserAccess;

    public string _caseFirstName;
    public string _caseSurname;
    public string _caseNumber;
    public string _caseJobTitle;

    protected void Page_Load(object sender, EventArgs e)
    {

        string access_Link = Context.Session["AccessLink"].ToString();

        currentUserAccess = "0";

        if (!IsPostBack)
        {

            getUserDetails(access_Link);

        }

    }

    public void getUserDetails(string _access_Link)
    {

        SqlDataReader objrs = null;
        SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseLoginUser", GetDatabaseConnection());
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add(new SqlParameter("@access_link", _access_Link));
        objrs = cmd.ExecuteReader();

        if (objrs.HasRows)
        {
            objrs.Read();

            yourAccRef = objrs["acc_ref"].ToString();
            corpCode = objrs["corp_code"].ToString();
            yourEmail = objrs["acc_email"].ToString();
            needSetup = objrs["need_setup"].ToString();
            hasPassword = objrs["has_password"].ToString();

            Session["CORP_CODE"] = corpCode;
            Session["YOUR_ACCOUNT_ID"] = yourAccRef;
            Session["YOUR_EMAIL"] = yourEmail;

            cc.Value = corpCode;
            acc.Value = yourAccRef;
            email.Value = yourEmail;



        } else
        {

            Response.Redirect("~/core/modules/response/layout/?err=1");
            Response.End();

        }


        cmd.Dispose();
        objrs.Dispose();

        // If there is no valid corp code, it needs a validation code
        if (corpCode.Length < 6)
        {
            Response.Redirect("~/core/modules/response/errorhandler.aspx?err=2");
            Response.End();
        }


        // Ok are we dealing with a user already? If so, just go straight to assessment
        if (needSetup == "NO")
        {

            if (hasPassword == "NO")
            {

                // Ok we need to check their password, if they dont have one, it will set one.
                Response.Redirect("~/core/modules/response/layout/auth.aspx");
                Response.End();
            }
            else
            {
                Session["AUTH"] = "Y";
                // If they do have a password, we need to ask them for it
                Response.Redirect("~/core/modules/response/layout/login.aspx");
                Response.End();
            }


        }



    }

    protected string getIPAddress()
    {
        System.Web.HttpContext context = System.Web.HttpContext.Current;
        string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return context.Request.ServerVariables["REMOTE_ADDR"];
    }

    public static SqlConnection GetDatabaseConnection()
    {

        SqlConnection connection = new SqlConnection
        (Convert.ToString(ConfigurationManager.ConnectionStrings["HAMUser"]));
        connection.Open();

        return connection;
    }

    public void saveUserDetails()
    {

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {

            string _corpCode = cc.Value;
            string _yourAccRef = acc.Value;
            string _yourEmail = email.Value;
            string _caseReference = Guid.NewGuid().ToString();

            SqlCommand cmd = new SqlCommand("Module_HA.dbo.caseUserSetup", dbConn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@corp_code", _corpCode));
            cmd.Parameters.Add(new SqlParameter("@acc_ref", _yourAccRef));
            cmd.Parameters.Add(new SqlParameter("@first_name", _caseFirstName));
            cmd.Parameters.Add(new SqlParameter("@last_name", _caseSurname));
            cmd.Parameters.Add(new SqlParameter("@acc_contactnumber", _caseNumber));
            cmd.Parameters.Add(new SqlParameter("@acc_Jobtitle", _caseJobTitle));
            cmd.Parameters.Add(new SqlParameter("@acc_email", _yourEmail));
            //  cmd.Parameters.Add(new SqlParameter("@tier2", _tier2));
            //  cmd.Parameters.Add(new SqlParameter("@tier3", _tier3));
            //  cmd.Parameters.Add(new SqlParameter("@tier4", _tier4));
            //  cmd.Parameters.Add(new SqlParameter("@tier5", _tier5));
            //  cmd.Parameters.Add(new SqlParameter("@tier6", _tier6));

            //test.Text = "acc:" + _yourAccRef +" cc:" + _corpCode +" email:" + _yourEmail;

            cmd.ExecuteNonQuery();


            cmd.Dispose();


        }


    }

    public void buttonAccess_Click(object sender, EventArgs e)
    {
        _caseFirstName = caseFirstName.Text;
        _caseSurname = caseSurname.Text;
        _caseNumber = caseContactNumber.Text;
        _caseJobTitle = caseJobTitle.Text;

        bool _formValid = false;

        if (_caseJobTitle.Length > 1 && _caseNumber.Length > 1 && _caseFirstName.Length > 1 && _caseSurname.Length > 1) { _formValid = true; }

        if (_formValid)
        {

            saveUserDetails();
            // We need to check or set password
            Response.Redirect("~/core/modules/response/layout/auth.aspx");
            Response.End();

            //Response.Redirect("responseMain.aspx");

        } else
        {

            alertArea.Visible = true;
            alertAreaMsg.Text = "<strong>Alert</strong> Please ensure you complete every field in the form below before you continue.";

        }



    }


}

