

function OpenAttach(ref) {
    var ref

    attachwindow = window.open("/version3.2/modules/btools/upload_agent.asp?reference=" + ref, "mywindow", "location=0,status=0,scrollbars=1,width=800,height=500");
    attachwindow.moveTo(0, 0);
    attachwindow.focus();
}

function validateCombo(sender, args) {
    args.IsValid = false;
    var combo = $find(sender.controltovalidate);

    var text = combo.get_text();
    if (text.length < 1) {
        args.IsValid = false;
    }
    var node = combo.findItemByText(text);
    if (node) {
        var value = node.get_value();
        if (value.length > 0 && (value != 0 || text == "Any User")) {
            args.IsValid = true;
        }
    }
    else {
        args.IsValid = false;
    }



    //console.log(node.value);
}


function validateCombo_NoBlank(sender, args) {

    args.IsValid = false;
    var combo = $find(sender.controltovalidate);

    var text = combo.get_text();


    if (text === "Please type a name or select a user") {
        args.IsValid = true;
    } else {
        var node = combo.findItemByText(text);
        if (node) {
            var value = node.get_value();
            if (value.length > 0 && (value != 0 || text == "Any User")) {
                args.IsValid = true;
            } else {
                args.IsValid = false;
            }
        }

    }

}