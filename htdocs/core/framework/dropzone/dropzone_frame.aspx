﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dropzone_frame.aspx.cs" Inherits="dropzone_frame" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset = "utf-8">

    <title>Document Uploader</title>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="dropzone.css" rel="stylesheet" type="text/css" />
    <link href="assessnet_dropzone.css" rel="stylesheet" type="text/css" />

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="../../framework/js/assessnet.js"></script>

    <link href="../../../version3.2/layout/upgrade/css/newlayout.css?444" rel="stylesheet" />
    <asp:Literal runat="server" id="cssLinkClientStylesheet" Text="" />


    <style>

        #dZUpload {
            max-height: 300px !important;
        }

        .fileupload_process {
            max-width: 300px;
        }

    </style>

</head>
<body style="width:745px;">


    <form id="pageform" runat="server" action="#">
    <div class="container-fluid">
    <div class="row">
        
        <asp:ScriptManager id="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
    

        <script type="text/javascript" language="javascript">
            var dropZoneRestricted = false; //View permission used with file upload and notes

            Sys.WebForms.PageRequestManager.getInstance().add_initializeRequest(CheckSessionStatus);

            function CheckSessionStatus(sender, args) {
                $.ajax({
                    url: "<% =Application["CONFIG_PROVIDER"] + "/core/system/sessions/ajaxSessions.ashx" %>",
                    type: "POST",
                    async: false,
                    success: function (data) { if (data != "") { window.parent.location = data; } },
                    error: function () { alert("An error occured retrieving your user session. Please contact support"); }
                });
            }
        </script>











        <!-- Gallery Modal -->
        <div id="dZUploadGallery">
            <div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="galleryModalLabel"><i class="fa fa-file-text" aria-hidden="true"></i> Image Preview</h4>
                        </div>
                        <div class="modal-body">
                            <div><img id="galleryImage" src="" width="25%"/></div>
                            <div><span id="galleryImageTitle"></span></div>
                        </div>
                        <div class="modal-footer">
                            <button id="galleryPrevious" type="button" class="btn btn-default">Previous</button>
                            <button id="galleryNext" type="button" class="btn btn-default">Next</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Ends -->

        <div id="dZUploadContainer" runat="server">
            <div id="file_list" runat="server"><%=relatedFilesList%></div>

            <div id="dZActions" class="row" runat="server">
                <div style="width:300px; padding-left:20px; float:left;">
                    <div id="dZUploadMessageSmall"><i class="fa fa-cloud-upload purple-colour" aria-hidden="true"></i> Drop files below to attach...</div>
                    <span class="fileupload-process">
                        <div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                        </div>
                    </span>
                </div>
                <div style="text-align: right; float:right; padding-right:20px; width:200px;">
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Add files manually</span>
                    </span>
                </div>
            </div>

            <div id="dZUpload">
                <div id="dZUploadMessageLarge"><i class="fa fa-cloud-upload purple-colour" aria-hidden="true"></i> Drop files here...</div>
                <div class="table table-striped" class="files" id="previews"> 
                    <div id="dZtemplate" class="file-row"> 
                        <!-- This is used as the file preview template --> 
                        <div> 
                            <span data-dz-preview class="preview"><img data-dz-thumbnail /></span> 
                        </div> 
                        <div> 
                            <p class="name" data-dz-name></p> 
                            <strong class="error text-danger" data-dz-errormessage></strong> 
                        </div> 
                        <div> 
                            <p class="size" data-dz-size></p> 
                            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"></div> 
                            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div> 
                        </div> 
                        <div id="dZcommands" style="width:120px;"> 
                            <button data-dz-remove class="btn btn-warning cancel" title="cancel"> 
                                <i class="glyphicon glyphicon-ban-circle"></i> 
                                <span>Cancel</span> 
                            </button> 
                            <button data-dz-remove class="btn btn-danger delete dZfloated" title="remove" id="dZRemove" runat="server"> 
                                <i class="glyphicon glyphicon-trash"></i> 
                            </button> 
                            <a data-dz-download class="btn btn-warning download dZfloated" target="_blank" title="download">
                                <i class='glyphicon glyphicon-download-alt'></i> 
                            </a>
                        </div> 
                    </div> 
                </div> 
            </div>

        </div>

<%--        <div id="dZPrintContainer" style="display:none;" runat="server">
            <asp:Repeater id="filePrintImageRepeater" runat="server"> 
                <ItemTemplate>
                    <div style="float:left;">
                        <img src="<%#relatedFilesRoot + Eval("filepath") + Eval("filename").ToString() %>" class="imagePrint" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>--%>

        <div id="dZNoFiles" style="display:none;" class="alert alert-warning">
            <div class="row">
                <div class="col-xs-9">
                    <strong>NOTICE</strong><br />There are currently no files attached to this assessment.
                </div>
            </div>
        </div>

    </div>
    </div>
    </form>

    <script src="dropzone.js" type="text/javascript"></script>
    <script src="assessnet_dropzone.js" type="text/javascript"></script>

    <script type="text/javascript">

        createDropzone("", "<%=recordModule %>", "<%=recordReference %>", "<%=recordReference2 %>", "<%=relatedFilesRoot %>", dropZoneRestricted);

    </script>

</body>
</html>
