﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using ASNETNSP;

public partial class dropzone_frame : System.Web.UI.Page
{
    public string var_corp_code;
    public string yourAccRef;
    public string recordReference;
    public string recordReference2;
    public string recordModule;

    public string relatedFilesList;

    public const string relatedFilesRoot = "../../../uploads";

    protected void Page_Load(object sender, EventArgs e)
    {

        //***************************
        // CHECK THE SESSION STATUS
        SessionState.checkSession(Page.ClientScript);
        var_corp_code = Context.Session["CORP_CODE"].ToString();
        yourAccRef = Context.Session["YOUR_ACCOUNT_ID"].ToString();
        //END OF SESSION STATUS CHECK
        //***************************

        cssLinkClientStylesheet.Text = "<link href='../css/customs/" + var_corp_code + ".css' rel='stylesheet' />";


        //pick module
        //pick reference
        //pick reference2

        recordReference = Request["ref"];
        recordReference2 = Request["ref2"];

        if (recordReference2.Length > 0)
        {
            recordModule = "ACC";
        }
        else
        {
            recordModule = Regex.Replace(recordReference, @"[\d-]", string.Empty);
        }

        relatedFilesList = SharedComponants.getFilesForDropzone(var_corp_code, recordReference, recordReference2, "all");

    }
}