﻿<%@ WebHandler Language="C#" Class="FileHandler_detach" %>

using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using ASNETNSP;

public class FileHandler_detach : IHttpHandler, IRequiresSessionState {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string reference = context.Request.QueryString["ref"];
        string reference2 = context.Request.QueryString["ref2"];
        string fileName = HttpUtility.HtmlDecode(HttpUtility.UrlDecode(context.Request.QueryString["file"]));
        string module = context.Request.QueryString["module"];

        string corp_code = context.Session["CORP_CODE"].ToString();
        string yourAccRef = context.Session["YOUR_ACCOUNT_ID"].ToString();


        

        //sql to remove the file from the record in the database

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            string sql_text = "EXEC Module_GLOBAL.dbo.usp_file_detach '" + corp_code + "', '" + reference + "', '" + reference2 + "', '" + fileName + "', '" + yourAccRef + "'";
            SqlCommand cmd = new SqlCommand(sql_text, dbConn);
            cmd.CommandType = CommandType.Text;

                context.Response.Write(sql_text);

            cmd.ExecuteNonQuery();

            cmd.Dispose();
        }

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            string sql_text = "EXEC Module_GLOBAL.dbo.usp_file_detach '" + corp_code + "', '" + reference + "', '" + reference2 + "', '" + Formatting.FormatTextInput(fileName) + "', '" + yourAccRef + "'";
            SqlCommand cmd = new SqlCommand(sql_text, dbConn);
            cmd.CommandType = CommandType.Text;

            context.Response.Write(sql_text);

            cmd.ExecuteNonQuery();

            cmd.Dispose();
        }

        if (module != "TM")
        {
            SharedComponants.insertHistory(corp_code, reference, yourAccRef, "Detached File: " + fileName, module, reference2);
        }
        else
        {
            using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
            {
                try
                {
                    //This script removes any previous default searches for the selected module
                    string sql_text = "INSERT INTO " + context.Application["DBTABLE_TASK_HISTORY"] + " " +
                                        "(corp_code,task_id,task_history_by,task_history,gendatetime, identifier) " +
                                        "VALUES('" + corp_code + "','" + reference + "','" + yourAccRef + "','Detached File: " + fileName + "', GETDATE(), '')";
                    SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                    sqlComm.ExecuteReader();

                    sqlComm.Dispose();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        context.Response.Write("ok");

    }

    public bool IsReusable {
        get {
            return false;
        }
    }



}