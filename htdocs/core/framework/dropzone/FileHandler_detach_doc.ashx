﻿<%@ WebHandler Language="C#" Class="FileHandler_detach_doc" %>

using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using ASNETNSP;

public class FileHandler_detach_doc : IHttpHandler, IRequiresSessionState {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string reference = context.Request.QueryString["ref"];
        string reference2 = context.Request.QueryString["ref2"];
        string fileName = context.Request.QueryString["file"];
        string module = context.Request.QueryString["module"];

        string corp_code = context.Session["CORP_CODE"].ToString();
        string yourAccRef = context.Session["YOUR_ACCOUNT_ID"].ToString();


        //sql to remove the file from the record in the database

        using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
        {
            string sql_text = "EXEC Module_DOC.dbo.removeFile '" + corp_code + "', '" + reference + "', '" + reference2 + "', '" + fileName + "', '" + yourAccRef + "', '" + module + "' ";
            SqlCommand cmd = new SqlCommand(sql_text, dbConn);
            cmd.CommandType = CommandType.Text;

            cmd.ExecuteNonQuery();

            cmd.Dispose();
        }

        
        context.Response.Write("ok");

    }

    public bool IsReusable {
        get {
            return false;
        }
    }



}