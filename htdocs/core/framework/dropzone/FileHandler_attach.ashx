﻿<%@ WebHandler Language="C#" Class="FileHandler_attach" %>

using System;
using System.Web;
using System.Web.UI;
using System.Web.SessionState;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using ASNETNSP;

public class FileHandler_attach : IHttpHandler, IRequiresSessionState {

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";

        string reference = context.Request.QueryString["ref"];
        string reference2 = context.Request.QueryString["ref2"];
        string reference3 = context.Request.QueryString["ref3"];
        string module = context.Request.QueryString["module"];

        string corp_code = context.Session["CORP_CODE"].ToString();
        string yourAccRef = context.Session["YOUR_ACCOUNT_ID"].ToString();

        string str_image = "";

        foreach (string s in context.Request.Files)
        {
            HttpPostedFile file = context.Request.Files[s];
            string fileName = file.FileName;
            string fileExtension = file.ContentType;
            int filesize = file.ContentLength;

            fileName = fileName.Replace("'", "`");

            if (!string.IsNullOrEmpty(fileName))
            {

                // Add the file to an attachments data so we have record of file and record

                Guid GuidReference = Guid.NewGuid();
                string file_reference = GuidReference.ToString();



                fileExtension = Path.GetExtension(fileName);

                str_image = file_reference + fileExtension;
                string vpath = "/" + corp_code + "/" + yourAccRef + "/";   // "/FileStore/222999/";
                string path = Path.Combine(context.Application["DOC_UPLOAD_PATH"].ToString(), corp_code, yourAccRef) + "\\";      //HttpContext.Current.Server.MapPath("/FileStore/222999/");

                Directory.CreateDirectory(path);

                string pathToSave = path + str_image;

                using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                {
                    string sql_text = "EXEC Module_GLOBAL.dbo.usp_file_attach '" + corp_code + "', '" + reference + "', '" + reference2 + "', '" + str_image + "', '" + fileName + "', '" + fileExtension + "', '" + path + "', '" + vpath + "', '" + filesize + "', '" + yourAccRef + "', '" + reference3 + "' ";
                    SqlCommand cmd = new SqlCommand(sql_text, dbConn);
                    cmd.CommandType = CommandType.Text;

                    cmd.ExecuteNonQuery();

                    cmd.Dispose();
                }

                file.SaveAs(pathToSave);


                if (module != "TM")
                {
                    SharedComponants.insertHistory(corp_code, reference, yourAccRef, "Attached File: " + fileName, module, reference2);
                }
                else
                {
                    using (SqlConnection dbConn = ConnectionManager.GetDatabaseConnection())
                    {
                        try
                        {
                            //This script removes any previous default searches for the selected module
                            string sql_text = "INSERT INTO " + context.Application["DBTABLE_TASK_HISTORY"] + " " +
                                              "(corp_code,task_id,task_history_by,task_history,gendatetime, identifier) " +
                                              "VALUES('" + corp_code + "','" + reference + "','" + yourAccRef + "','Attached File: " + fileName + "', GETDATE(), '')";
                            SqlCommand sqlComm = new SqlCommand(sql_text, dbConn);
                            sqlComm.ExecuteReader();

                            sqlComm.Dispose();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }


                str_image = vpath + str_image;
            }
        }
        context.Response.Write(str_image);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }



}