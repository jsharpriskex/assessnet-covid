﻿

var previewFileList = [];
var previewFileTitle = [];

Dropzone.autoDiscover = false;

// Get the template HTML and remove it from the doument
var previewNode = document.querySelector("#dZtemplate");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);

var myDropzone = new Dropzone(document.getElementById("dZUpload"), { // Make a specific area a dropzone
    url: dropZoneAttachHandler, // Set the url
    thumbnailWidth: 50,
    thumbnailHeight: 50,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: true, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button",  // Define the element that should be used as click trigger to select files.
    acceptedFiles: "image/*, application/pdf, .doc, .docx, .xls, .xlsx, .ppt, .pptx"
});

// Update the total progress bar
myDropzone.on("totaluploadprogress", function (progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
});

myDropzone.on("sending", function (file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1";
});

// Hide the total progress bar when nothing's uploading anymore
myDropzone.on("queuecomplete", function (progress) {
    document.querySelector("#total-progress").style.opacity = "0";
});

myDropzone.on("removedfile", function (file) {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", dropZoneDetachHandler + file.name, false);
    xmlhttp.send();

    //remove file from the imagepreview array
    if (file.fname) {
        var index = previewFileList.indexOf(file.url + file.fname)
        previewFileList.splice(index, 1);
        previewFileTitle.splice(index, 1);
    }

    //decrease file count badge
    var fileCount = $("#fileCounter").html();
    fileCount--;
    $("#fileCounter").html(fileCount);

    tidyDropZone();
});

myDropzone.on("success", function (file) {
    if (file.fname) {
        file.previewTemplate.querySelector("[data-dz-download]").setAttribute('href', dropZoneFileRoot + file.url + file.fname);

        var fileName = file.fname.toLowerCase();
        if (fileName.indexOf(".jpg") > 0 || fileName.indexOf(".jpeg") > 0 || fileName.indexOf(".png") > 0 || fileName.indexOf(".bmp") > 0 || fileName.indexOf(".gif") > 0) {
            previewFileList.push(file.url + file.fname);
            previewFileTitle.push(file.name);
        }
    } else {
        file.previewTemplate.querySelector("[data-dz-download]").setAttribute('style', "display: none;");
        //increase file count badge
        var fileCount = $("#fileCounter").html();
        fileCount++;
        $("#fileCounter").html(fileCount);
    }

    tidyDropZone();
});

myDropzone.on("addedfile", function (file) {
    if (file.fname) {
        file.previewTemplate.querySelector("[data-dz-thumbnail]").addEventListener("click", function () {
            //trigger modal to open showing the larger image
            $("#galleryImage").attr("src", dropZoneFileRoot + file.url + file.fname);
            $("#galleryImageTitle").html(file.name);
            $("#galleryModal").modal("show");
        });
    }

    var fileName = file.name.toLowerCase();
    if (fileName.indexOf(".jpg") == -1 && fileName.indexOf(".jpeg") == -1 && fileName.indexOf(".png") == -1 && fileName.indexOf(".bmp") == -1 && fileName.indexOf(".gif") == -1) {
        file.previewTemplate.querySelector("[data-dz-thumbnail]").setAttribute('style', "display: none;");
        file.previewTemplate.querySelector("[data-dz-preview]").setAttribute('class', "fa fa-file fa-4x green-colour");
    }
});

myDropzone.on("dragover", function () {
    $("#previews").css('opacity', '0.2');
    //$("#dZUploadMessage").css('display', 'block');
});

myDropzone.on("dragleave", function () {
    $("#previews").css('opacity', '1');
});

myDropzone.on("drop", function () {
    $("#previews").css('opacity', '1');
    //$("#dZUploadMessage").css('display', 'none');
});


//recall previously uploaded files
var json_insert = $("#file_list").html();
$("#file_list").remove();
var files = JSON.parse(json_insert);
for (var i = 0; i < files.length; i++) {

    var mockFile = {
        url: files[i].filepath,
        size: files[i].filesize,
        name: files[i].friendlyname,
        fname: files[i].filename
    };

    // Call the default addedfile event handler
    myDropzone.emit("addedfile", mockFile);
    myDropzone.createThumbnailFromUrl(mockFile, dropZoneFileRoot + mockFile.url + mockFile.fname);

    // Make sure that there is no progress bar, etc...
    myDropzone.emit("success", mockFile);

}





//gallery navigation
$("#galleryNext").click(function () {

    var currentFile = $("#galleryImage").attr("src");

    for (var i = 0; i < previewFileList.length; i++) {
        if (dropZoneFileRoot + previewFileList[i] == currentFile) {
            i++;
            if (previewFileList[i]) {
                $("#galleryImage").attr("src", dropZoneFileRoot + previewFileList[i]);
                $("#galleryImageTitle").html(previewFileTitle[i]);
                return false;
            }
        }
    }
});
$("#galleryPrevious").click(function () {

    var currentFile = $("#galleryImage").attr("src");

    for (var i = previewFileList.length; i > 0; i--) {
        if (dropZoneFileRoot + previewFileList[i] == currentFile) {
            i--;
            if (previewFileList[i]) {
                $("#galleryImage").attr("src", dropZoneFileRoot + previewFileList[i]);
                $("#galleryImageTitle").html(previewFileTitle[i]);
                return false;
            }
        }
    }
});





if (checkDropzonePermissions == true) {
    myDropzone.removeEventListeners();
    document.getElementById("dZActions").style.display = "none";
    $("#dZUpload").css('border', '0px');
    $("#dZUploadMessage").css('display', 'none');
}



function tidyDropZone() {

    var fileCount = $("#fileCounter").html();
    if (fileCount == 0) {
        $("#dZUploadMessage").css('font-size', '3em');
        $("#dZUploadMessage").css('padding-top', '75px');
        $("#dZUploadMessage").css('height', '200px');
        $("#dZUploadMessage").css('color', '#cccccc');
    } else {
        $("#dZUploadMessage").css('font-size', '1.5em');
        $("#dZUploadMessage").css('padding-top', '0px');
        $("#dZUploadMessage").css('height', '0px');
        $("#dZUploadMessage").css('color', '#9e9e9e');
    }

}

tidyDropZone();