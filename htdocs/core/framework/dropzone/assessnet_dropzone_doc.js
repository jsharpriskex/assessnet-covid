﻿

var previewFileList = [];
var previewFileTitle = [];

var dropZoneAttachHandler
var dropZoneDetachHandler
var dropZoneFileRoot

Dropzone.autoDiscover = false;

function createDropzone(DropzoneId, module, reference, reference2, fileRoot, dropZoneRestricted) {

    var handlerLocation = fileRoot.replace('../uploads', '');

    dropZoneAttachHandler = handlerLocation + "framework/dropzone/FileHandler_attach_doc.ashx?ref=" + reference + "&ref2=" + reference2 + "&module=" + module;
    dropZoneDetachHandler = handlerLocation + "framework/dropzone/FileHandler_detach_doc.ashx?ref=" + reference + "&ref2=" + reference2 + "&module=" + module + "&file=";
    dropZoneFileRoot = fileRoot;

    var newDropzone = initDropzone(DropzoneId)
    getDropzoneFiles(newDropzone, DropzoneId);
    setDropzonePermissions(newDropzone, DropzoneId, dropZoneRestricted);
    tidyDropZone(DropzoneId);

}



function initDropzone(DropzoneId) {
    // Get the template HTML and remove it from the doument
    var previewNode = document.querySelector("#dZtemplate" + DropzoneId);
    previewNode.id = "";
    var previewTemplate = previewNode.parentNode.innerHTML;
    previewNode.parentNode.removeChild(previewNode);

    myDropzone = new Dropzone(document.getElementById("dZUpload" + DropzoneId), { // Make a specific area a dropzone
        url: dropZoneAttachHandler, // Set the url
	uploadMultiple: false,
        thumbnailWidth: 50,
        thumbnailHeight: 50,
        parallelUploads: 1,
        previewTemplate: previewTemplate,
        autoQueue: true,
        previewsContainer: "#previews" + DropzoneId,
        clickable: ".fileinput-button",
        acceptedFiles: "image/*, application/pdf, .doc, .docx, .xls, .xlsx, .xlsm, .ppt, .pptx, .eml, .msg, .wmv, .avi, .mp4",
        maxFileSize: 100, //mb
	timeout: 90000,
        init: function () {
            this.on('error', function (file, errorMessage) {
                if (errorMessage.indexOf('Error 404') !== -1) {
                    var errorDisplay = document.querySelectorAll('[data-dz-errormessage]');
                    errorDisplay[errorDisplay.length - 1].innerHTML = 'Error 404: The upload page was not found on the server';
                }
            });
            this.on("totaluploadprogress", function (progress) {
                // Update the total progress bar
                document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
            });
            this.on("sending", function (file) {
                // Show the total progress bar when upload starts
                $("#dZUploadMessageSmall" + DropzoneId).css('display', 'none');
                document.querySelector("#total-progress" + DropzoneId).style.opacity = "1";
            });
            this.on("queuecomplete", function (progress) {
                // Hide the total progress bar when nothing's uploading anymore
                document.querySelector("#total-progress" + DropzoneId).style.opacity = "0";
                $("#dZUploadMessageSmall" + DropzoneId).css('display', 'block');
            });
            this.on("removedfile", function (file) {
                xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", dropZoneDetachHandler + file.name, false);
                xmlhttp.send();

                //remove file from the imagepreview array
                if (file.fname) {
                    var index = previewFileList.indexOf(file.url + file.fname)
                    previewFileList.splice(index, 1);
                    previewFileTitle.splice(index, 1);
                }

                //decrease file count badge
                if ($("#fileCounter" + DropzoneId)) {
                    var fileCount = $("#fileCounter" + DropzoneId).html();
                    fileCount--;
                    $("#fileCounter" + DropzoneId).html(fileCount);

                    tidyDropZone(DropzoneId);
                }

            });
            this.on("success", function (file, response) {
                var increaseBadgeCounter = false;
                if (!file.fname) {
                    file.url = response.substring(0, response.lastIndexOf("/") + 1);
                    file.fname = response.substring(response.lastIndexOf("/") + 1, response.length);
                    increaseBadgeCounter = true;
                }

                file.previewTemplate.querySelector("[data-dz-download]").setAttribute('href', dropZoneFileRoot + file.url + file.fname);

                var fileName = file.fname.toLowerCase();
                if (fileName.indexOf(".jpg") > 0 || fileName.indexOf(".jpeg") > 0 || fileName.indexOf(".png") > 0 || fileName.indexOf(".bmp") > 0 || fileName.indexOf(".gif") > 0) {
                    previewFileList.push(file.url + file.fname);
                    previewFileTitle.push(file.name);
                }

                file.previewTemplate.querySelector("[data-dz-thumbnail]").addEventListener("click", function () {
                    //trigger modal to open showing the larger image
                    $("#galleryImage").attr("src", dropZoneFileRoot + file.url + file.fname);
                    $("#galleryImageTitle").html(file.name);
                    $("#galleryModal").modal("show");
                });

                //increase file count badge
                if (increaseBadgeCounter == true && $("#fileCounter" + DropzoneId)) {
                    var fileCount = $("#fileCounter" + DropzoneId).html();
                    fileCount++;
                    $("#fileCounter" + DropzoneId).html(fileCount);
                }

                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    tidyDropZone(DropzoneId);
                }
            });
            this.on("addedfile", function (file) {
                var fileName = file.name.toLowerCase();
                if (fileName.indexOf(".jpg") == -1 && fileName.indexOf(".jpeg") == -1 && fileName.indexOf(".png") == -1 && fileName.indexOf(".bmp") == -1 && fileName.indexOf(".gif") == -1) {
                    file.previewTemplate.querySelector("[data-dz-thumbnail]").setAttribute('style', "display: none;");
                    file.previewTemplate.querySelector("[data-dz-preview]").setAttribute('class', "fa fa-file fa-4x green-colour");
                }
            });
            this.on("dragover", function () {
                $("#previews").css('opacity', '0.2');
            });
            this.on("dragleave", function () {
                $("#previews").css('opacity', '1');
            });
            this.on("drop", function () {
                $("#previews").css('opacity', '1');
            });

        }

    });

    return myDropzone;
}




function getDropzoneFiles(myDropzone, DropzoneId) {
    //recall previously uploaded files
    var json_insert = $("#file_list" + DropzoneId).html();
    $("#file_list" + DropzoneId).remove();

    var files = JSON.parse(json_insert);
    for (var i = 0; i < files.length; i++) {

        if (files[i].filepath.substring(0, 1) != "/") { files[i].filepath = "/" + files[i].filepath; }

        var mockFile = {
            url: files[i].filepath,
            size: files[i].filesize,
            name: files[i].friendlyname,
            fname: files[i].filename
        };

        // Call the default addedfile event handler
        myDropzone.emit("addedfile", mockFile, mockFile.url + mockFile.fname);
        myDropzone.createThumbnailFromUrl(mockFile, dropZoneFileRoot + mockFile.url + mockFile.fname);

        // Make sure that there is no progress bar, etc...
        myDropzone.emit("success", mockFile);
    }
}



function setDropzonePermissions(myDropzone, DropzoneId, userPermission) {
    if (userPermission == true) {
        myDropzone.removeEventListeners();
        document.getElementById("dZActions" + DropzoneId).style.display = "none";
        $("#dZUpload" + DropzoneId).css('border', '0px');
        $("#dZUploadMessage" + DropzoneId).css('display', 'none');

        var fileCount = $("#fileCounter").html();
        if (fileCount == 0) {
            $("#dZUpload" + DropzoneId).css('display', 'none');
            $("#dZNoFiles" + DropzoneId).css('display', 'block');
        }
    }
}


function tidyDropZone(DropzoneId) {

    var fileCount = $("#fileCounter" + DropzoneId).html();
    if (fileCount == 0) {
        $("#dZUploadMessageLarge" + DropzoneId).css('display', 'block');
        $("#dZUploadMessageSmall" + DropzoneId).css('display', 'none');
    } else {
        $("#dZUploadMessageLarge" + DropzoneId).css('display', 'none');
        $("#dZUploadMessageSmall" + DropzoneId).css('display', 'block');
    }

}






//gallery navigation
$("#galleryNext").click(function () {

    var currentFile = $("#galleryImage").attr("src");

    for (var i = 0; i < previewFileList.length; i++) {
        if (dropZoneFileRoot + previewFileList[i] == currentFile) {
            i++;
            if (previewFileList[i]) {
                $("#galleryImage").attr("src", dropZoneFileRoot + previewFileList[i]);
                $("#galleryImageTitle").html(previewFileTitle[i]);
                return false;
            }
        }
    }
});
$("#galleryPrevious").click(function () {

    var currentFile = $("#galleryImage").attr("src");

    for (var i = previewFileList.length; i > 0; i--) {
        if (dropZoneFileRoot + previewFileList[i] == currentFile) {
            i--;
            if (previewFileList[i]) {
                $("#galleryImage").attr("src", dropZoneFileRoot + previewFileList[i]);
                $("#galleryImageTitle").html(previewFileTitle[i]);
                return false;
            }
        }
    }
});
